/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.hibernate.util;

import dk.tigerteam.trimm.mdsd.runtime.RuntimeMeta;
import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaProperty;
import dk.tigerteam.trimm.persistence.util.InitializerStrategy;
import dk.tigerteam.trimm.util.ReflectUtils;
import org.hibernate.Hibernate;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Helper class which perform initialization of Entities using an {@link dk.tigerteam.trimm.persistence.util.InitializerStrategy}
 * @author Jeppe Cramon
 */
public class HibernateEntityInitializer {

	/**
	 * Default strategy which involves calling all property read methods. This should be used together
	 * with an Entity strategy in TRIMM which handles unproxyfication and initialization for properties through their
	 * property read methods (such as the HibernateAssociationUnproxyListener). 
	 */
	public static final InitializerStrategy HibernatePropertyCallingInitializer = new InitializerStrategy() {
		
		public <T extends RuntimeMeta> T initializeEntity(T entity,	EntityInitializationInfo initializationInfo) {
			if (initializationInfo.shouldInitialize(EntityInitializationInfo.Initialize.OneToOne)) {
				initializeAssociations(entity, initializationInfo.getOneToOneAssociations());
			}
			if (initializationInfo.shouldInitialize(EntityInitializationInfo.Initialize.ManyToOne)) {
				initializeAssociations(entity, initializationInfo.getManyToOneAssociations());
			}
			if (initializationInfo.shouldInitialize(EntityInitializationInfo.Initialize.OneToMany)) {
				initializeAssociations(entity, initializationInfo.getOneToManyAssociations());
			}
			if (initializationInfo.shouldInitialize(EntityInitializationInfo.Initialize.ManyToMany)) {
				initializeAssociations(entity, initializationInfo.getManyToManyAssociations());
			}
			return entity;
		}
		
		private void initializeAssociations(Object entity, Set<RuntimeMetaProperty> associations) {
			for (RuntimeMetaProperty property : associations) {
				Method readMethod = property.getPropertyDescriptor().getReadMethod();
				if (readMethod != null) {
					Object result = ReflectUtils.invokeMethod(readMethod, entity, ReflectUtils.NO_METHOD_CALL_ARGS);
					if (result instanceof Collection || result instanceof Map) {
						Hibernate.initialize(result);
					}
				}
			}
		}
	};
	
	/**
	 * Initializes the entity using the {@link #HibernatePropertyCallingInitializer}
	 * @param <T> The type of the entity
	 * @param entity The entity to initialize
	 * @param initializations The initializations to use. If left unspecified, then {@link dk.tigerteam.trimm.persistence.util.InitializerStrategy.EntityInitializationInfo.Initialize#All} is assumed
	 * @return The initialized entity - It's advisable to use the return value instead of the method parameter, as some {@link InitializerStrategy}'s might resort to
	 * for instance prefetch queries
	 */
	public static <T extends RuntimeMeta> T initializeEntity (T entity, InitializerStrategy.EntityInitializationInfo.Initialize...initializations) {
		return initializeEntity(entity, HibernatePropertyCallingInitializer, initializations);
	}
	
	/**
	 * Initializes the entity using the strategy provided
	 * @param <T> The type of the entity
	 * @param entity The entity to initialize
	 * @param initializations The initializations to use. If left unspecified, then {@link dk.tigerteam.trimm.persistence.util.InitializerStrategy.EntityInitializationInfo.Initialize#All} is assumed
	 * @return The initialized entity - It's advisable to use the return value instead of the method parameter, as some {@link InitializerStrategy}'s might resort to
	 * for instance prefetch queries
	 */
	public static <T extends RuntimeMeta> T initializeEntity (T entity, InitializerStrategy initializer, InitializerStrategy.EntityInitializationInfo.Initialize...initializations) {
		if (initializer == null) {
			throw new IllegalArgumentException("No InitializerStrategy was provided");
		}
    InitializerStrategy.EntityInitializationInfo initializationInfo = InitializerStrategy.EntityInitializationInfo.populateEntityIntializationInfo(entity, initializations);
		return initializer.initializeEntity(entity, initializationInfo);
	}
	

}
