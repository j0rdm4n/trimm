/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.hibernate.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.hibernate.stat.CollectionStatistics;
import org.hibernate.stat.EntityStatistics;
import org.hibernate.stat.QueryStatistics;
import org.hibernate.stat.Statistics;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Simple Hibernate statistisc class, only local development use....
 * Not Completed
 * 
 * @author Lasse Cramon
 */
public class HibernateRuntimeStatistics {

	private static final Log log = LogFactory.getLog(HibernateRuntimeStatistics.class);
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final String newLine = System.getProperty("line.separator");
	private Statistics statistics;

	public HibernateRuntimeStatistics(Statistics statistics) {
		super();
		this.statistics = statistics;
		if (statistics == null) {
			throw new RuntimeException("Statistics is null probably because it is not enabled in the configuration");
		}
	}

	public HibernateRuntimeStatistics(EntityManager entityManager) {
		super();
		HibernateEntityManager hibernateEntityManager = (HibernateEntityManager) entityManager;
		this.statistics = hibernateEntityManager.getSession().getSessionFactory().getStatistics();
		if (this.statistics == null) {
			throw new RuntimeException("Statistics is null probably because it is not enabled in the configuration");
		}
	}

	public HibernateRuntimeStatistics(EntityManagerFactory entityManagerFactory) {
		super();
		HibernateEntityManagerFactory factory = (HibernateEntityManagerFactory) entityManagerFactory;
		this.statistics = factory.getSessionFactory().getStatistics();
		if (this.statistics == null) {
			throw new RuntimeException("Statistics is null probably because it is not enabled in the configuration");
		}
	}

	public Statistics getStatistics() {
		return statistics;
	}

	private String generateEntityTextReport(String entityName, EntityStatistics statistics) {
		final StringBuilder builder = new StringBuilder();
		builder.append("####################################################################");
		builder.append(newLine);
		builder.append("Statistics for entity: " + entityName);
		builder.append(newLine);
		builder.append("Load count:   " + statistics.getLoadCount());
		builder.append(newLine);
		builder.append("Fetch count:  " + statistics.getFetchCount());
		builder.append(newLine);
		builder.append("Insert count: " + statistics.getInsertCount());
		builder.append(newLine);
		builder.append("Update count: " + statistics.getUpdateCount());
		builder.append(newLine);
		builder.append("Delete count: " + statistics.getDeleteCount());
		builder.append(newLine);
		builder.append("Optimisticfailure count: " + statistics.getOptimisticFailureCount());
		builder.append(newLine);
		builder.append("####################################################################");
		builder.append(newLine);
		return builder.toString();
	}

	private String generateEntityTextReport(String entityName, EntityStatistics statistics, Collection<Field> collectionFields) {
		final StringBuilder builder = new StringBuilder();
		builder.append("####################################################################");
		builder.append(newLine);
		builder.append("Statistics for entity: " + entityName);
		builder.append(newLine);
		builder.append("Load count:   " + statistics.getLoadCount());
		builder.append(newLine);
		builder.append("Fetch count:  " + statistics.getFetchCount());
		builder.append(newLine);
		builder.append("Insert count: " + statistics.getInsertCount());
		builder.append(newLine);
		builder.append("Update count: " + statistics.getUpdateCount());
		builder.append(newLine);
		builder.append("Delete count: " + statistics.getDeleteCount());
		builder.append(newLine);
		builder.append("Optimisticfailure count: " + statistics.getOptimisticFailureCount());
		builder.append(newLine);
		for (Field field : collectionFields) {
			CollectionStatistics collectionStatistics = this.statistics.getCollectionStatistics(entityName + "." + field.getName());
			if (collectionStatistics != null) {
				builder.append("Statistics for collection: " + field.getName());
				builder.append(newLine);
				builder.append("Fetch count:     " + collectionStatistics.getFetchCount());
				builder.append(newLine);
				builder.append("Load count:      " + collectionStatistics.getLoadCount());
				builder.append(newLine);
				builder.append("Recreate count:  " + collectionStatistics.getRecreateCount());
				builder.append(newLine);
				builder.append("Remove count:    " + collectionStatistics.getRemoveCount());
				builder.append(newLine);
				builder.append("Update count:    " + collectionStatistics.getUpdateCount());
				builder.append(newLine);
			}
		}
		builder.append("####################################################################");
		builder.append(newLine);
		return builder.toString();
	}

	private String generateQueryTextReport(String queryName, QueryStatistics statistics) {
		final StringBuilder builder = new StringBuilder();
		builder.append("####################################################################");
		builder.append(newLine);
		builder.append("Statistics for query: " + queryName);
		builder.append(newLine);
		builder.append("Execution avg. time: " + statistics.getExecutionAvgTime());
		builder.append(newLine);
		builder.append("Execution count:     " + statistics.getExecutionCount());
		builder.append(newLine);
		builder.append("Execution max time:  " + statistics.getExecutionMaxTime());
		builder.append(newLine);
		builder.append("Execution min time:  " + statistics.getExecutionMinTime());
		builder.append(newLine);
		builder.append("Execution row count: " + statistics.getExecutionRowCount());
		builder.append(newLine);
		builder.append("####################################################################");
		builder.append(newLine);
		return builder.toString();
	}

	@SuppressWarnings("unused")
	private String generateCollectionTextReport(String collectionName, CollectionStatistics statistics) {
		final StringBuilder builder = new StringBuilder();
		builder.append("####################################################################");
		builder.append(newLine);
		builder.append("Statistics for collection: " + collectionName);
		builder.append(newLine);
		builder.append("Fetch count: " + statistics.getFetchCount());
		builder.append(newLine);
		builder.append("Load count:     " + statistics.getLoadCount());
		builder.append(newLine);
		builder.append("Recreate count:  " + statistics.getRecreateCount());
		builder.append(newLine);
		builder.append("Remove count:  " + statistics.getRemoveCount());
		builder.append(newLine);
		builder.append("Update count: " + statistics.getUpdateCount());
		builder.append(newLine);
		builder.append("####################################################################");
		builder.append(newLine);
		return builder.toString();
	}

	private String generateOverallTextReport() {
		final StringBuilder builder = new StringBuilder();
		builder.append("####################################################################");
		builder.append(newLine);
		builder.append("Overall statistics starttime: " + new Date(statistics.getStartTime()));
		builder.append(newLine);
		builder.append("Total Load count:   " + statistics.getEntityLoadCount());
		builder.append(newLine);
		builder.append("Total Fetch count:  " + statistics.getEntityFetchCount());
		builder.append(newLine);
		builder.append("Total Insert count: " + statistics.getEntityInsertCount());
		builder.append(newLine);
		builder.append("Total Update count: " + statistics.getEntityUpdateCount());
		builder.append(newLine);
		builder.append("Total Delete count: " + statistics.getEntityDeleteCount());
		builder.append(newLine);
		builder.append("Total Optimisticfailure count: " + statistics.getOptimisticFailureCount());
		builder.append(newLine);
		builder.append("Total Connection count: " + statistics.getConnectCount());
		builder.append(newLine);
		builder.append("Total Session open count: " + statistics.getSessionOpenCount());
		builder.append(newLine);
		builder.append("Total Session close count: " + statistics.getSessionCloseCount());
		builder.append(newLine);
		builder.append("Total Flush count: " + statistics.getFlushCount());
		builder.append(newLine);
		builder.append("Total PreparedStatement count: " + statistics.getPrepareStatementCount());
		builder.append(newLine);
		builder.append("Total Close count: " + statistics.getCloseStatementCount());
		builder.append(newLine);
		builder.append("Total Transaction count: " + statistics.getTransactionCount());
		builder.append(newLine);
		builder.append("Total Successful Transaction count: " + statistics.getSuccessfulTransactionCount());
		builder.append(newLine);
		builder.append("Total Collection Load count: " + statistics.getCollectionLoadCount());
		builder.append(newLine);
		builder.append("Total Collection Fetch count: " + statistics.getCollectionFetchCount());
		builder.append(newLine);
		builder.append("Total Collection Recreate count: " + statistics.getCollectionRecreateCount());
		builder.append(newLine);
		builder.append("Total Collection Remove count: " + statistics.getCollectionRemoveCount());
		builder.append(newLine);
		builder.append("Total Collection Update count: " + statistics.getCollectionUpdateCount());
		builder.append(newLine);
		builder.append("Total Query execution  count: " + statistics.getQueryExecutionCount());
		builder.append(newLine);
		builder.append("Query max execution time: " + statistics.getQueryExecutionMaxTime() + "ms ("
				+ (statistics.getQueryExecutionMaxTime() / 1000) + "s)");
		builder.append(newLine);
		builder.append("Query max execution time query: " + statistics.getQueryExecutionMaxTimeQueryString());
		builder.append(newLine);
		builder.append("####################################################################");
		builder.append(newLine);
		return builder.toString();
	}

	public String generateTextReportAllMappedEntities() {
		final StringBuilder builder = new StringBuilder();
		for (String entityName : this.statistics.getEntityNames()) {
			EntityStatistics statistics = this.statistics.getEntityStatistics(entityName);
			if (statistics != null) {
				builder.append(generateEntityTextReport(entityName, statistics));
				builder.append(newLine);
			}
		}
		return builder.toString();
	}

	public String generateTextReportAllMappedQueries() {
		final StringBuilder builder = new StringBuilder();
		for (String queryName : this.statistics.getQueries()) {
			QueryStatistics statistics = this.statistics.getQueryStatistics(queryName);
			if (statistics != null) {
				builder.append(generateQueryTextReport(queryName, statistics));
				builder.append(newLine);
			}
		}
		return builder.toString();
	}

	public void printAllMappedEntitiesStdOut() {
		System.out.println(generateTextReportAllMappedEntities());
	}

	public void printAllMappedQueriesStdOut() {
		System.out.println(generateTextReportAllMappedQueries());
	}

	public void printOverallTextReportStdOut() {
		System.out.println(generateOverallTextReport());
	}

	public void generateTextFileReport(String generateToDirectory, List<String> content) throws IOException {
		File generatorDirectory = new File(generateToDirectory);
		if (!generatorDirectory.exists()) {
			generatorDirectory.mkdirs();
		}
		String timestamp = formatter.format(System.currentTimeMillis());
		String fileName = "HibernateRuntimeStatisticsTextReport-" + timestamp + ".txt";
		fileName = fileName.replace(":", ".");
		File file = new File(generatorDirectory.getAbsolutePath()
				+ (generatorDirectory.getAbsolutePath().endsWith(File.separator) ? "" : File.separator) + fileName);
		FileUtils.writeLines(file, "utf-8", content);
	}

	public void generateOverallTextFileReport(String generateToDirectory) throws IOException {
		File generatorDirectory = new File(generateToDirectory);
		if (!generatorDirectory.exists()) {
			generatorDirectory.mkdirs();
		}
		final StringBuilder builder = new StringBuilder();
		builder.append(generateOverallTextReport());
		String timestamp = formatter.format(System.currentTimeMillis());
		String fileName = "HibernateRuntimeStatisticsTextReport-" + timestamp + ".txt";
		fileName = fileName.replace(":", ".");
		File file = new File(generatorDirectory.getAbsolutePath()
				+ (generatorDirectory.getAbsolutePath().endsWith(File.separator) ? "" : File.separator) + fileName);
		FileUtils.writeStringToFile(file, builder.toString(), "utf-8");
	}

//	private ClasspathScanner classpathScanner = new ClasspathScanner(new ClassFilter() {
//		public boolean isOk(Class<?> clazz) {
//			return ReflectUtils.hasAnnotation(clazz, Entity.class) || ReflectUtils.hasAnnotation(clazz, MappedSuperclass.class);
//		}
//	});

	/**
	 * Will serach the classpath of classes containing {@link Entity} or {@link MappedSuperClass} annotation and generate statistics for them,
	 * as well as overall and query reports
	 * 
	 * @throws IOException
	 */
//	@SuppressWarnings("unchecked")
//	public void generateFullTextFileReport(String includeSearchInPackage, String generateToDirectory) throws IOException {
//		log.debug("Generating full text file report");
//		final StringBuilder builder = new StringBuilder();
//		builder.append(generateOverallTextReport());
//		builder.append(generateTextReportAllMappedQueries());
//		Set<Class> entities = classpathScanner.scan(includeSearchInPackage);
//		log.debug("Found '" + entities.size() + "' with Entity or MappedSuperClass annotations in '" + includeSearchInPackage + "'");
//		for (Class clazz : entities) {
//			Collection<Field> collectionFields = ReflectUtils.getAllFieldsWithAnnotations(clazz, OneToMany.class, ManyToMany.class);
//			log.trace("Found '" + collectionFields.size() + "' collection field with OneToMany or ManyToMany annotations in  clazz'"
//					+ clazz.getSimpleName() + "'");
//			EntityStatistics entityStatistics = this.statistics.getEntityStatistics(clazz.getName());
//			if (entityStatistics != null) {
//				builder.append(generateEntityTextReport(clazz.getName(), entityStatistics, collectionFields));
//			}
//		}
//		File generatorDirectory = new File(generateToDirectory);
//		if (!generatorDirectory.exists()) {
//			generatorDirectory.mkdirs();
//		}
//		String timestamp = formatter.format(System.currentTimeMillis());
//		String fileName = "HibernateRuntimeStatisticsTextReport-" + timestamp + ".txt";
//		fileName = fileName.replace(":", ".");
//		File file = new File(generatorDirectory.getAbsolutePath()
//				+ (generatorDirectory.getAbsolutePath().endsWith(File.separator) ? "" : File.separator) + fileName);
//		FileUtils.writeStringToFile(file, builder.toString(), "utf-8");
//		log.debug("Done generating full text file report");
//	}

}
