/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.hibernate.bean;

import dk.tigerteam.trimm.collection.WrappedSet;
import dk.tigerteam.trimm.persistence.hibernate.util.HibernateUtils;

import java.util.Iterator;
import java.util.Set;

public class UnproxyingWrappedSet<T> extends WrappedSet<T> {

  private static final long serialVersionUID = -5892206170991119165L;

  public UnproxyingWrappedSet(Set<T> wrappedSet) {
    super(wrappedSet);
  }
  
  @Override
  public Iterator<T> iterator() {
    return new WrappedIterator(getWrappedSet().iterator());
  }
  
  private final class WrappedIterator implements Iterator<T> {
    
    public WrappedIterator(Iterator<T> wrapped) {
      this.wrapped = wrapped;
    }
    
    Iterator<T> wrapped;

    public boolean hasNext() {
      return wrapped.hasNext();
    }

    @SuppressWarnings("unchecked")
    public T next() {
      return (T) HibernateUtils.unproxyAndReassociateIfNecessary(wrapped.next());
    }

    public void remove() {
      wrapped.remove();
    }
  }
  
}
