/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.hibernate.util;

import org.hibernate.Hibernate;
import org.hibernate.impl.SessionImpl;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.HibernateProxyHelper;

import java.util.Collection;

public class HibernateUtils {

	public static Object unproxyAndReassociateIfNecessary(Object in) {
		if (in instanceof HibernateProxy) {
			try {
				return ((SessionImpl)((HibernateProxy) in).getHibernateLazyInitializer().getSession()
					.getPersistenceContext().unproxyAndReassociate(in));
			} catch (Exception e) {
				// Just return the proxy
			}
		}
		return in;
	}
	
	public static Object unproxyAndReassociate(Object in) {
		try {
			return ((SessionImpl)((HibernateProxy) in).getHibernateLazyInitializer().getSession()
					.getPersistenceContext().unproxyAndReassociate(in));
		} catch (Exception e) {
			// Just return the proxy
		}
		return in;
	}

	public static boolean isProxy(Object in) {
		return in instanceof HibernateProxy;
	}

	public static Object unproxyIfNecessary(Object in) {
		if (in instanceof HibernateProxy) {
			try {
				return ((SessionImpl)((HibernateProxy) in).getHibernateLazyInitializer().getSession()
						.getPersistenceContext().unproxy(in));
			} catch (Exception e) {
				// Just return the proxy
			}
		}
		return in;
	}
	
	public static Class<?> getClassWithoutInitializingProxy(Object in) {
		return HibernateProxyHelper.getClassWithoutInitializingProxy(in);
	}
	
	public static void initialize(Object in) {
		Hibernate.initialize(in);
	}
	
	public static boolean isInitialized(Object in) {
		return Hibernate.isInitialized(in);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> Collection<T> unproxyAndReassociateCollection(Collection<T> collection) {
		for (T t : collection) {
			t = (T) unproxyAndReassociateIfNecessary(t);
		}
		return collection;
	}
}
