/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.hibernate.util;

import dk.tigerteam.trimm.persistence.util.ProxyHandler;
import org.hibernate.impl.SessionImpl;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.HibernateProxyHelper;

import javax.persistence.EntityManager;


public class HibernateProxyHandler implements ProxyHandler {

	private EntityManager entityManager;
  
  public HibernateProxyHandler(EntityManager entityManager) {
	  super();
	  this.entityManager = entityManager;
  }
	
	@SuppressWarnings("unchecked")
	public Object unproxyAndReassociateIfNecessary(Object in) {
		if (in instanceof HibernateProxy) {
      HibernateUtils.initialize(in);
      return ((SessionImpl)entityManager.getDelegate()).
      		getPersistenceContext().unproxyAndReassociate(in);
		} else {
      return in;
		}
	}

	public boolean isProxy(Object in) {
		return in instanceof HibernateProxy;
	}

	@SuppressWarnings("unchecked")
	public Object unproxyIfNecessary(Object in) {
		if (in instanceof HibernateProxy) {
      HibernateUtils.initialize(in);
			return ((SessionImpl)entityManager.getDelegate()).
					getPersistenceContext().unproxy(in);
		}
		return in;
	}
	
	public Class<?> getClassWithoutInitializingProxy(Object in) {
		return HibernateProxyHelper.getClassWithoutInitializingProxy(in);
	}

}
