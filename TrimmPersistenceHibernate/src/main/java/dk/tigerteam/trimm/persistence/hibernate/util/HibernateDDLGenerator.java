/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.hibernate.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.ejb.Ejb3Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class to call Hibernate ddl infrastructure
 * 
 * @author Lasse Cramon
 */
public class HibernateDDLGenerator {

	private static final Log logger = LogFactory.getLog(HibernateDDLGenerator.class);

	public static String generate(String persistenceUnitName, String generateToDirectory, String ddlFileName, Map<String, String> properties,
			boolean format, boolean outputToConsole) {
		File generatorDirectory = new File(generateToDirectory);
		if (!generatorDirectory.exists()) {
			generatorDirectory.mkdirs();
		}
		String fullPath = generatorDirectory.getAbsolutePath()
				+ (generatorDirectory.getAbsolutePath().endsWith(File.separator) ? "" : File.separator) + ddlFileName;
		logger.debug("Generating DDL file '" + ddlFileName + "'");
		Ejb3Configuration configuration = new Ejb3Configuration();
		configuration.configure(persistenceUnitName, properties);
		SchemaExport schemaExport = new SchemaExport(configuration.getHibernateConfiguration());
		schemaExport.setFormat(format);
		schemaExport.setDelimiter(";");
		schemaExport.setOutputFile(fullPath);
		schemaExport.create(outputToConsole, false);
		logger.debug("Done generating DDL file '" + ddlFileName + "'");
		return fullPath;
	}

	public static void main(String[] args) {
		if (args.length != 4) {
			System.out.println("Usage: persistenceUnitName hibernateDialectClassName generateToDirectory ddlFileName");
			System.out.println("Hibernate dialects (Name - ClassName)");
			System.out.println("H2 org.hibernate.dialect.H2Dialect");
			System.out.println("DB2	org.hibernate.dialect.DB2Dialect");
			System.out.println("DB2 AS/400	org.hibernate.dialect.DB2400Dialect");
			System.out.println("DB2 OS390	org.hibernate.dialect.DB2390Dialect");
			System.out.println("PostgreSQL	org.hibernate.dialect.PostgreSQLDialect");
			System.out.println("MySQL	org.hibernate.dialect.MySQLDialect");
			System.out.println("MySQL with InnoDB	org.hibernate.dialect.MySQLInnoDBDialect");
			System.out.println("MySQL with MyISAM	org.hibernate.dialect.MySQLMyISAMDialect");
			System.out.println("Oracle (any version)	org.hibernate.dialect.OracleDialect");
			System.out.println("Oracle 9i	org.hibernate.dialect.Oracle9iDialect");
			System.out.println("Oracle 10g	org.hibernate.dialect.Oracle10gDialect");
			System.out.println("Sybase	org.hibernate.dialect.SybaseDialect");
			System.out.println("Sybase Anywhere	org.hibernate.dialect.SybaseAnywhereDialect");
			System.out.println("Microsoft SQL Server	org.hibernate.dialect.SQLServerDialect");
			System.out.println("SAP DB	org.hibernate.dialect.SAPDBDialect");
			System.out.println("Informix	org.hibernate.dialect.InformixDialect");
			System.out.println("HypersonicSQL	org.hibernate.dialect.HSQLDialect");
			System.out.println("Ingres	org.hibernate.dialect.IngresDialect");
			System.out.println("Progress	org.hibernate.dialect.ProgressDialect");
			System.out.println("Mckoi SQL	org.hibernate.dialect.MckoiDialect");
			System.out.println("Interbase	org.hibernate.dialect.InterbaseDialect");
			System.out.println("Pointbase	org.hibernate.dialect.PointbaseDialect");
			System.out.println("FrontBase	org.hibernate.dialect.FrontbaseDialect");
			System.out.println("Firebird	org.hibernate.dialect.FirebirdDialect");
			return;
		}
		String persistenceUnitName = args[0];
		String hibernateDialect = args[1];
		String generateToDirectory = args[2];
		String ddlFileName = args[3];

		Map<String, String> properties = new HashMap<String, String>();
		properties.put("hibernate.dialect", hibernateDialect);
		generate(persistenceUnitName, generateToDirectory, ddlFileName, properties, true, true);
	}
}
