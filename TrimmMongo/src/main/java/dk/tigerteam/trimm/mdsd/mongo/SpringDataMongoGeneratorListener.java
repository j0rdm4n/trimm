/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.mongo;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ManyToManyAssociationEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.ManyToOneAssociationEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.OneToManyAssociationEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.OneToOneAssociationEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.SimplePropertyEvent;

/**
 * Listener that adds Spring Data mongo meta mapping annotation on model. <br />
 * 
 * Supports: <br />
 * 
 * Clazz: <br />
 *				{@link Document} <br/>
 *  			{@link CompoundIndex} as StereoType COMPOUNDINDEXES and the CompoundIndex as tagvalue name/value
 *  			<pre>@CompoundIndex(name = "age_idx", def = "{'lastName': 1, 'age': -1}")</pre>
 * Fields: <br />
 * 				{@link Id} StereoType ID <br/>
 * 				{@link Indexed} StereoType INDEX <br/>
 *        {@link Indexed} unique=true StereoType UNIQUEINDEX <br/>
 *        {@link Transient} StereoType TRANSIENT <br/>
 * Association: <br />
 * 				{@link DBRef} StereoType DBREF     
 * <br />
 * @see http://docs.spring.io/spring-data/data-mongo/docs/1.3.1.RELEASE/reference/html
 * <br />
 * 
 * @author Lasse Cramon
 */
public class SpringDataMongoGeneratorListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

	private static final Log logger = LogFactory.getLog(SpringDataMongoGeneratorListener.class);
	
	public static final String DOCUMENT_STEREOTYPE = "DOCUMENT";
	public static final String ID_STEREOTYPE = "DOCUMENT_ID";
	public static final String INDEX_STEREOTYPE = "INDEX";
	public static final String COMPOUND_INDEXS_STEREOTYPE = "COMPOUNDINDEXES";
	public static final String UNIQUE_INDEX_STEREOTYPE = "UNIQUEINDEX";
	public static final String FIELD_STEREOTYPE = "FIELD";
	public static final String TRANSIENT_STEREOTYPE = "TRANSIENT";
	public static final String DBREF_STEREOTYPE = "DBREF";
	public static final String PERSISTENCECONSTRUCTOR_STEREOTYPE = "PERSISTENCECONSTRUCTOR";

	@Override
	protected boolean handleClazzEvent(ClazzEvent event) {
		if(event.getClazz().isInterface() || event.getClazz().isEnumeration()) {
			return true;
		}
		if(event.getMetaClazz().hasStereoType(DOCUMENT_STEREOTYPE)) {
			logger.debug("Adding Mongo @Document to clazz '" + event.getClazz().getFQCN() + "'");
//			Annotation documentAnnotation = new Annotation(Document.class);
//			documentAnnotation.addAttribute("collection", Utils.uncapitalize(event.getClazz().getName())); // default
			event.getClazz().addAnnotations(new Annotation(Document.class));
//			if(event.getMetaClazz().hasStereoType(COMPOUND_INDEXS_STEREOTYPE)) {
//				if(!event.getMetaClazz().getTaggedValues().isEmpty()) { // ?
//					Annotation compoundIndexsAnnotation = new Annotation(CompoundIndexes.class);
//					for (Map.Entry<String, String> entry : event.getMetaClazz().getTaggedValues().entrySet()) {
//						if(entry.getKey().equals("COMPOUNDINDEX")) {
//							Annotation compoundIndexAnnotation = new Annotation(CompoundIndex.class);
//							// ? parse value string name:def
//							compoundIndexAnnotation.addAttribute("name", entry.getKey()).addAttribute("def", entry.getValue());
//							compoundIndexsAnnotation.addAnnotations(compoundIndexAnnotation);
//						}
//					}
//					event.getClazz().addAnnotations(compoundIndexsAnnotation);
//				}
//			}
		}
		return true;
	}
	
	@Override
	protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
		if(event.getMetaProperty().hasStereoType(ID_STEREOTYPE)) {
			logger.trace("Adding @Id to property '" + event.getProperty().getField().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Id.class));
		}
		if(event.getMetaProperty().hasStereoType(INDEX_STEREOTYPE)) {
			logger.trace("Adding @Indexed to property '" + event.getProperty().getField().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Indexed.class));
		}
		if(event.getMetaProperty().hasStereoType(UNIQUE_INDEX_STEREOTYPE)) {
			logger.trace("Adding @Unique index to property '" + event.getProperty().getField().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Indexed.class).addAnnotationAttribute("unique", true));
		}
		if(event.getMetaProperty().hasStereoType(TRANSIENT_STEREOTYPE)) {
			logger.trace("Adding @Transient to property '" + event.getProperty().getField().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Transient.class));
		}
		if(event.getMetaProperty().hasStereoType(FIELD_STEREOTYPE)) {
			String fieldValue = null;
			for (Map.Entry<String, String> entry : event.getMetaProperty().getTaggedValues().entrySet()) {
				if(entry.getKey().equals(FIELD_STEREOTYPE)) {
					fieldValue = entry.getValue();
				}
			}
			if(fieldValue != null) {
				logger.trace("Adding Field to property '" + event.getProperty().getField().getName() + "'");
				Annotation annotation = new Annotation(Field.class);
				annotation.addAttribute("value", fieldValue);
				event.getProperty().getField().addAnnotations();
			}
		}
		
		return true;
	}

	@Override
	protected boolean handleManyToManyAssociationEvent(ManyToManyAssociationEvent event) {
		if(event.getMetaProperty().hasStereoType(DBREF_STEREOTYPE) || event.getMetaProperty().getAssociation().hasStereoType(DBREF_STEREOTYPE)) {
			logger.trace("Adding @DBRef to property '" + event.getProperty().getField().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(DBRef.class));
		}
		return true;
	}

	@Override
	protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
		if(event.getMetaProperty().hasStereoType(DBREF_STEREOTYPE) || event.getMetaProperty().getAssociation().hasStereoType(DBREF_STEREOTYPE)) {
			logger.trace("Adding @DBRef to property '" + event.getProperty().getField().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(DBRef.class));
		}
		return true;
	}

	@Override
	protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
		if(event.getMetaProperty().hasStereoType(DBREF_STEREOTYPE) || event.getMetaProperty().getAssociation().hasStereoType(DBREF_STEREOTYPE)) {
			logger.trace("Adding @DBRef to property '" + event.getProperty().getField().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(DBRef.class));
		}
		return true;
	}

	@Override
	protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
		if(event.getMetaProperty().hasStereoType(DBREF_STEREOTYPE) || event.getMetaProperty().getAssociation().hasStereoType(DBREF_STEREOTYPE)) {
			logger.trace("Adding @DBRef to property '" + event.getProperty().getField().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(DBRef.class));
		}
		return true;
	}
	
}
