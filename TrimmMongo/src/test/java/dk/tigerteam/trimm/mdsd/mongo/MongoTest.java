package dk.tigerteam.trimm.mdsd.mongo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.Mongo;

/**
 * Require that you have a Mongo instance running on local machine
 *
 * @author Lasse Cramon
 */
public class MongoTest {

    @SuppressWarnings("unused")
		private static final Log logger = LogFactory.getLog(MongoTest.class);

		@SuppressWarnings("unused")
		private static MongoOperations mongoOps;
    
    @BeforeClass
    public static void setup() throws Exception {
    	mongoOps = new MongoTemplate(new SimpleMongoDbFactory(new Mongo(), "testing"));
    }
    
    public static void teardown() throws Exception {
    	mongoOps = null;
    }
    
    @Test
    public void testGeneratedMongoMappings() {
//    	 String id = UUID.randomUUID().toString();
//    	 Customer customer = new Customer();
//    	 customer.setId(id);
//    	 customer.setUsername("Hansi");
//    	 customer.setFirstName("Hans");
//    	 customer.setLastName("Hansen");
//    	 Address address = new Address();
//    	 address.setStreet("Street");
//    	 address.setCity("City");
//    	 customer.setAddress(address);
//    	 
//    	 Order order = new Order();
//    	 order.setId(UUID.randomUUID().toString());
//    	 order.setOrderDate(new Date());
//    	 
//    	 order.setCustomer(customer);
//    	 customer.getOrder().add(order);
//    	 
//    	 mongoOps.insert(customer);
//    	 mongoOps.insert(order);
//    	 
//    	 Customer c = mongoOps.findById(id, Customer.class);
//    	 System.out.println(c);
//    	 System.out.println(c.getOrder().iterator().next());
    }
    
}
