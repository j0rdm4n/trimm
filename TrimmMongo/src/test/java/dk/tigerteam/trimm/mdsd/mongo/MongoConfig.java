package dk.tigerteam.trimm.mdsd.mongo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.mapping.event.LoggingEventListener;

import com.mongodb.Mongo;

@Configuration
public class MongoConfig extends AbstractMongoConfiguration {

	@Override
	public String getDatabaseName() {
		return "mdsdTest";
	}

	@Override
	public Mongo mongo() throws Exception {
		return new Mongo("localhost", 27017);
	}
	
	@Bean
  public LoggingEventListener mappingEventsListener() {
    return new LoggingEventListener();
  }

}
