package dk.tigerteam.trimm.mdsd.mongo;

import java.io.File;

import org.junit.Test;

import dk.tigerteam.trimm.mdsd.java.configuration.EventListenerConfiguration;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaConfiguration;
import dk.tigerteam.trimm.mdsd.java.configuration.PojoConfigurationBasedCodeGenerator;
import dk.tigerteam.trimm.mdsd.java.generator.extension.HashCodeAndEqualsListener;
import dk.tigerteam.trimm.mdsd.java.generator.extension.ToStringListener;

public class SpringDataMongoGeneratorListenerTest {

    @Test
    public void test() {
    	JavaConfiguration javaConfiguration = new JavaConfiguration();
      javaConfiguration.setUmlTool("EA");
      javaConfiguration.setXmiModelPath("src/test/resources/trimm-mongo-exa-model.xml");
      javaConfiguration.setUseImports(true);
      javaConfiguration.setBasePackageName("dk.tigerteam.mongo");
      javaConfiguration.setGenerateExtensionClasses(false);
      javaConfiguration.setAddExtensionAndBaseClassAnnotations(false);
      javaConfiguration.setGenerateBaseClassesToPath("target/generated-sources/mongo-base");
      javaConfiguration.setGenerateExtensionClassesToPath("target/generated-sources/mongo-extension");

      javaConfiguration.getEventListeners().add(new EventListenerConfiguration(SpringDataMongoGeneratorListener.class));
      javaConfiguration.getEventListeners().add(new EventListenerConfiguration(HashCodeAndEqualsListener.class));
      javaConfiguration.getEventListeners().add(new EventListenerConfiguration(ToStringListener.class));

      PojoConfigurationBasedCodeGenerator codeGenerator = new PojoConfigurationBasedCodeGenerator(javaConfiguration, new File(""), new File(""), null);
      codeGenerator.generate();
    }

}
