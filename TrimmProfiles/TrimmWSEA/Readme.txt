Introduction
------------
This zip file contains the full set of files needed to model, generate, install the TigerTeam WebService MDG for Enterprise Architect.
The MDG is used for modeling WebServices for use with the TigerTeam TRIMM Model Generator. 

Release Notes:
--------------

1.2:
- Changed the Technology name in the "Select from:" list in the "New Diagram" Dialog from "TrimmWS" to "Trimm WebService"
- TigerTeam Logo added

1.1:
- Changed product name from TigerMDSD to TRIMM

1.0:
- Initial Release


Files
-----
TigerTeam TrimmWS.eap		The EA model containing the MDG elements 
TigerTeam TrimmWS MDG.mts	The EA MTS file used when generating the MDG file
TigerTeam TrimmWS Profile.xml	The elements used for modeling WebServices exported from the EA model
TigerTeam TrimmWS Toolbox.xml	The toolbox containing the WebServices elements exported from the EA model
TigerTeam TrimmWS Diagram.xml	The Diagram used for modeling WebServices exported from the EA model
TigerTeam TrimmWS.xml		The generated MDG file
Readme.txt			This file

Reguirements
------------
This MDG is tested with Enterprise Architect version 9.x and version 10.x
Tested with TigerTeam TRIMM version 1.x

Installing the MDG
------------------
The MDG is contained in the "TigerTeam TrimmWS.xml" file. 

Information on the differernt ways to install MDG's in Enerprise Architect is found here: 

	http://trimm.tigerteam.dk/how-to-install-a-trimm-mdg-under-enterprise-architect-from-sparx-systems/


More information
----------------
More information about the TigerTeam TRIMM model generator and how to develop MDG's can be found on the TigerTeam TRIMM website:

	http://trimm.tigerteam.dk

If you find any bugs, have request for new features or just want to share tips and tricks, please visit our support site:

	http://trimmsupport.tigerteam.dk


Regards

The TRIMM Team
