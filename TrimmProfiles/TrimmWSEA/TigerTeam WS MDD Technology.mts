<MDG.Selections model="\\psf\TigerMDSDWS\TigerTeam WS MDD Model.eap">
	<Technology id="TigerMDDWS" name="TigerTeam WebService MDD" version="1.0" notes="TigerTeam Model Driven WebServices Profile for use with TigerMDSD" filename="\\psf\TigerMDSDWS\TigerTeam WS MDD.xml" infoURI="www.tigerteam.dk"/>
	<Profiles directory="W:\" files="TigerTeam WS MDD Profile.xml"/>
	<TagVals tags="Fault Messages,WSNamespace"/>
	<CodeMods>
		<CodeMod product="TT WSDL" codetypes="1" codetemplates="0" grammar="" codeoptions=""/>
	</CodeMods>
	<DiagramProfile directory="W:\" files="TigerTeam WS MDD Diagrams.xml"/>
	<UIToolboxes directory="W:\" files="TigerTeam WS MDD Toolbox.xml"/>
</MDG.Selections>
