<MDG.Selections model="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmWSEA\TigerTeam TrimmWS.eap">
	<Technology id="TrimmWS" name="Trimm WebService" version="1.3" notes="TigerTeam Model Driven WebServices Profile for use with the TigerTeam Trimm Model Generator" filename="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmWSEA\TigerTeam TrimmWS.xml" infoURI="www.tigerteam.dk" supportURI="+45 7021 7170" icon="\\psf\Home\Documents\git\trimm\TrimmProfiles\Icons and Bitmaps\Tigerteam Favicon.bmp" logo="\\psf\Home\Documents\git\trimm\TrimmProfiles\Icons and Bitmaps\Tigerteam icon 64x64.bmp"/>
	<Profiles directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmWSEA" files="TigerTeam TrimmWS Profile.xml"/>
	<TagVals tags="faultMessages,namespace"/>
	<CodeMods>
		<CodeMod product="TrimmWS" codetypes="1" codetemplates="0" grammar="" codeoptions=""/>
	</CodeMods>
	<DiagramProfile directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmWSEA" files="TigerTeam TrimmWS Diagram.xml"/>
	<UIToolboxes directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmWSEA" files="TigerTeam TrimmWS Toolbox.xml"/>
</MDG.Selections>
