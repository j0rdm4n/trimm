<MDG.Selections model="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmHibernateEA\TigerTeam TrimmHibernate .eap">
	<Technology id="TrimmHbnt" name="Trimm Hibernate" version="1.1" notes="TigerTeam Model Driven Hibernate and JPA Profile for use with the TigerTeam TRIMM Model Generator" filename="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmHibernateEA\TigerTeam TrimmHibernate .xml" infoURI="www.tigerteam.dk" supportURI="+45 7021 7170 " icon="\\psf\Home\Documents\git\trimm\TrimmProfiles\Icons and Bitmaps\Tigerteam Favicon.bmp" logo="\\psf\Home\Documents\git\trimm\TrimmProfiles\Icons and Bitmaps\Tigerteam icon 64x64.bmp"/>
	<Profiles directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmHibernateEA" files="TigerTeam TrimmHibernate Profile.xml"/>
	<TagVals tags="BatchSize,FetchMode,Index,Unique_index"/>
	<DiagramProfile directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmHibernateEA" files="TigerTeam TrimmHibernate Diagram.xml"/>
	<UIToolboxes directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmHibernateEA" files="TigerTeam TrimmHibernate Toolbox.xml"/>
</MDG.Selections>
