Introduction
------------
This zip file contains the full set of files needed to model, generate, install the TigerTeam JAVA MDG for Enterprise Architect.
The MDG is used for modeling Java for use with the TigerTeam TRIMM Model Generator. 

Release Notes:
--------------

1.1:
- Fixed bug that made the Tagged Values from other MDG's show up in this one.

1.0:
- Initial Release


Files
-----
TigerTeam TrimmJAVA.eap			The EA model containing the MDG elements 
TigerTeam TrimmJAVA MDG.mts		The EA MTS file used when generating the MDG file
TigerTeam TrimmJAVA Profile.xml		The elements used for modeling JAVA exported from the EA model
TigerTeam TrimmJAVA Toolbox.xml		The toolbox containing the JAVA elements exported from the EA model
TigerTeam TrimmJAVA Diagram.xml		The Diagram used for modeling JAVA exported from the EA model
TigerTeam TrimmJAVA Wizard.xml		The Java Diagram Wizard exported from the EA model
TigerTeam TrimmJAVA.xml			The generated MDG file
Readme.txt				This file


Reguirements
------------
This MDG is tested with Enterprise Architect version 9.x and version 10.x
Tested with TigerTeam TRIMM version 1.x

Installing the MDG
------------------
The MDG is contained in the "TigerTeam TrimmJAVA.xml" file. 
The MDG includes Diagram Wizards contained in the "TigerTeam TrimmJAVA Wizards.xml" file. 

Information on the differernt ways to install MDG's in Enerprise Architect is found here: 

	http://trimm.tigerteam.dk/how-to-install-a-trimm-mdg-under-enterprise-architect-from-sparx-systems/


More information
----------------
More information about the TigerTeam TRIMM model generator and how to develop MDG's can be found on the TigerTeam TRIMM website:

	http://trimm.tigerteam.dk

If you find any bugs, have request for new features or just want to share tips and tricks, please visit our support site:

	http://trimmsupport.tigerteam.dk


Regards

The TRIMM Team
