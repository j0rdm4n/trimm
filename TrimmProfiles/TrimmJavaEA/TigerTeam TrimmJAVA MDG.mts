<MDG.Selections model="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmJavaEA\TigerTeam TrimmJAVA.eap">
	<Technology id="TrimmJAVA" name="Trimm JAVA" version="1.1" notes="TigerTeam Model Driven Java Profile for use with the TigerTeam TRIMM Model Generator" filename="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmJavaEA\TigerTeam TrimmJAVA.xml" infoURI="www.tigerteam.dk" supportURI="+45 7021 7170 " icon="\\psf\Home\Documents\git\trimm\TrimmProfiles\Icons and Bitmaps\Tigerteam Favicon.bmp" logo="\\psf\Home\Documents\git\trimm\TrimmProfiles\Icons and Bitmaps\Tigerteam icon 64x64.bmp"/>
	<Profiles directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmJavaEA" files="TigerTeam TrimmJAVA Profile.xml"/>
	<TagVals tags="ConstructorBody,LiteralInitValue"/>
	<DiagramProfile directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmJavaEA" files="TigerTeam TrimmJAVA Diagram.xml"/>
	<UIToolboxes directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmJavaEA" files="TigerTeam TrimmJAVA Toolbox.xml"/>
	<ModelTemplates>
		<Model name="Trimm JAVA" description="Modeling JAVA applications for use with the TigerTeam TRIMM model generator" location="TigerTeam TrimmJAVA Wizard.xml" default="yes" icon="31" filter="Trimm"/>
	</ModelTemplates>
</MDG.Selections>
