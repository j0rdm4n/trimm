<MDG.Selections model="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmJpaEA\TigerTeam TrimmJPA.eap">
	<Technology id="TrimmJPA" name="Trimm JPA" version="1.1" notes="TigerTeam Model Driven JPA Profile for use with the TigerTeam TRIMM Model Generator" filename="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmJpaEA\TigerTeam TrimmJPA.xml" infoURI="www.tigerteam.dk" supportURI="+45 7021 7170 " icon="\\psf\Home\Documents\git\trimm\TrimmProfiles\Icons and Bitmaps\Tigerteam Favicon.bmp" logo="\\psf\Home\Documents\git\trimm\TrimmProfiles\Icons and Bitmaps\Tigerteam icon 64x64.bmp"/>
	<Profiles directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmJpaEA" files="TigerTeam TrimmJPA Profile.xml"/>
	<TagVals tags="Cascade,OrderBy"/>
	<DiagramProfile directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmJpaEA" files="TigerTeam TrimmJPA Diagram.xml"/>
	<UIToolboxes directory="\\psf\Home\Documents\git\trimm\TrimmProfiles\TrimmJpaEA" files="TigerTeam TrimmJPA Toolbox.xml"/>
</MDG.Selections>
