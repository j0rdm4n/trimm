/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.groovy.configuration.configuration;

import dk.tigerteam.trimm.mdsd.groovy.configuration.GroovyConfigurationBasedCodeGenerator;
import org.junit.Test;

import java.io.File;

/**
 */
public class GormTestModelTest {
  @Test
  public void testMddExampleGeneration() {
    GroovyConfigurationBasedCodeGenerator.generate(new File("src/test/resources/gormTestModel.yml"), new File(""), new File(""), false, null);
  }
}
