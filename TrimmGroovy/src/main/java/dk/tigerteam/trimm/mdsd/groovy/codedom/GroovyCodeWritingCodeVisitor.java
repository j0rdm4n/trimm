/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.groovy.codedom;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.codedom.Enumeration;
import dk.tigerteam.trimm.mdsd.java.codedom.GenericType.Specialization;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.PackageRewriter;
import dk.tigerteam.trimm.util.Utils;

import java.util.*;

/**
 * Code visitor that can output Groovy code to a {@link CodeWriter} - supports Package rewriting
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class GroovyCodeWritingCodeVisitor extends AbstractCodeWritingVisitor implements GroovyCodeDomVisitor {

  public GroovyCodeWritingCodeVisitor(CodeWriter writer, PackageRewriter packageRewriter, boolean useImports) {
    super(writer, packageRewriter, useImports);
  }

  // ----------------------- Helper methods ------------------------
  /**
   * Print out all the elements {@link dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier}' in order they were specified
   *
   * @param element The element to write out the {@link CodeElement#getAccessModifiers()} for
   * @return This {@link CodeWriter} instance
   */
  public CodeWriter printAccessModifiers(CodeElement element) {
    for (CodeElement.AccessModifier modifier : (Set<AccessModifier>)element.getAccessModifiers()) {
      // In groovy public is the default, so let's skip it to make the code more terse
      if ("public".equalsIgnoreCase(modifier.name())) continue;
      writer.print(modifier.name().toLowerCase()).print(" ");
    }
    return writer;
  }

  // ---------------------------------------------------------------

  @Override
  public void handle(Annotation annotation) {
  	if(isUseImports()) {
  		writer.print("@").print(annotation.getSimpleName());
  	} else {
  		writer.print("@").print(annotation.getName());
  	}
    if (annotation.hasChildren()) {
      writer.print("(");
      printCodeElementCollection(annotation.getChildren());
      writer.print(")");
    }
  }

  @Override
  public void handle(AnnotationAttribute annotationAttribute) {
    if (annotationAttribute.getName() != null && !annotationAttribute.getName().equals(AnnotationAttribute.DEFAULT_VALUE_ATTRIBUTE_NAME)) {
      writer.print(annotationAttribute.getName()).print("=");
    }
    if (annotationAttribute.hasMultipleValues()) {
      writer.print("[");
    }
    int count = 0;
    for (Object value : annotationAttribute.getValues()) {
      if (count > 0) {
        writer.print(", ");
      }
      if (value instanceof CodeElement) {
        ((CodeElement) value).visit(this);
      } else if (value instanceof Enum) {
        Enum<?> enumValue = (Enum<?>) value;
        if(isUseImports()) {
        	writer.print(enumValue.getDeclaringClass().getSimpleName() + "." + enumValue.name());
        } else {
        	writer.print(enumValue.getDeclaringClass().getName() + "." + enumValue.name());
        }
      } else if (value instanceof String) {
        writer.print(Utils.quotedString((String) value));
      } else {
        writer.print(value.toString());
      }
      count++;
    }
    if (annotationAttribute.hasMultipleValues()) {
      writer.print("]");
    }
  }

  @Override
  public void handle(AnnotationDefinition annotationDefinition) {
    // TODO: Complete
  }

  @Override
  public void handle(Clazz clazz) {
    CodePackage codePackage = clazz.getPackage();
    if (codePackage != null && clazz.getClazzStyle() == Clazz.ClazzStyle.Normal) {
      writer.print("package ").print(codePackage.getName()).println();
    }
    if(!clazz.getImports().isEmpty()) {
    	writer.println();
    }
    for (Import import_ : clazz.getImports()) {
			import_.visit(this);
		}
    writer.println();
    printJavaDoc(clazz);

    printAnnotations(clazz);
    printAccessModifiers(clazz);
    writer.print("class ");
    writer.print(clazz.getName());

    if (clazz.getGenerics().size() > 0) {
      writer.print("<");
      printCodeElementCollection(clazz.getGenerics());
      writer.print(">");
    }

    writer.print(" ");
    if (clazz.getExtendsType() != null) {
      writer.print("extends ");
      clazz.getExtendsType().visit(this);
      writer.print(" ");
    }
    if (clazz.getImplementsTypes().size() > 0) {
      writer.print("implements ");
      printCodeElementCollection(clazz.getImplementsTypes());
    }

    writer.println("{");
    writer.println();
    // Write out statics first
    for (CodeElement child : clazz.getChildren()) {
      boolean isStaticProperty = child instanceof Property && ((Property)child).hasAccessModifier(AccessModifier.Static);
      if (isStaticProperty) {
        child.visit(this);
        writer.println();
      }
    }

    // TODO: handle Gorm Embeddable inner class
    for (CodeElement child : clazz.getChildren()) {
      boolean sameFileClazz = child instanceof Clazz && ((Clazz)child).getClazzStyle() == Clazz.ClazzStyle.SameFileClazz;
      boolean isStaticProperty = child instanceof Property && ((Property)child).hasAccessModifier(AccessModifier.Static);
      if (!sameFileClazz && !isStaticProperty) {
        child.visit(this);
        writer.println();
      }
    }

    writer.println("}");

    for (CodeElement child : clazz.getChildren()) {
      boolean sameFileClazz = child instanceof Clazz && ((Clazz)child).getClazzStyle() == Clazz.ClazzStyle.SameFileClazz;
      if (sameFileClazz) {
        child.visit(this);
      }
    }
  }

  @Override
  public void handle(CodePackage codePackage) {
    // Do nothing so far
  }

  @Override
  public void handle(Constructor constructor) {
    Clazz definedInClazz = constructor.findOwnerOfType(Clazz.class);
    if (definedInClazz != null && !constructor.hasAccessModifier(AccessModifier.Static)) {
      // Use the Clazz' name as our constructor name
      constructor.setName(definedInClazz.getName());
    }
    handle((Method) constructor);
  }

  @Override
  public void handle(Enumeration enumeration) {
    CodePackage codePackage = enumeration.getPackage();
    if (codePackage != null) {
      writer.print("package ").print(codePackage.getName()).println();
    }
    if(!enumeration.getImports().isEmpty()) {
    	writer.println();
    }
    for (Import import_ : enumeration.getImports()) {
			import_.visit(this);
		}
    writer.println();
    printJavaDoc(enumeration);
    printAnnotations(enumeration);
    printAccessModifiers(enumeration);
    writer.print("enum ");
    writer.print(enumeration.getName());
    writer.print(" ");
    if (enumeration.getImplementsTypes().size() > 0) {
      writer.print("implements ");
      printCodeElementCollection(enumeration.getImplementsTypes());
    }
    writer.println(" {");
    List<EnumLiteralField> enumValues = enumeration.getEnumValues();
    printCodeElementCollection(enumValues);
    writer.println(";");

    // Write out the rest of the children if any (its late, perhaps there's a better way to sort this stuff out)
    List<CodeElement> remainingChildren = new LinkedList<CodeElement>();
    for (CodeElement child : enumeration.getChildren()) {
      if (!enumValues.contains(child)) {
        remainingChildren.add(child);
      }
    }

    for (CodeElement child : remainingChildren) {
      child.visit(this);
    }

    writer.println("}");
  }

  @Override
  public void handle(EnumLiteralField enumLiteralField) {
    if (enumLiteralField.getClazzOrInterfaceOwner().isInterface()) {
      return;
    }

    printJavaDoc(enumLiteralField);
    printAnnotations(enumLiteralField);

    writer.print(" ");
    writer.print(enumLiteralField.getName());
    if (enumLiteralField.getInitializerStatement() != null) {
      writer.print("(");
      enumLiteralField.getInitializerStatement().visit(this);
      writer.print(")");
    }
  }

  @Override
  public void handle(Field field) {
    if (field.getClazzOrInterfaceOwner().isInterface() && !field.hasAccessModifier(AccessModifier.Static)) {
      return;
    }

    printJavaDoc(field);
    printAnnotations(field);
    printAccessModifiers(field);
    if (field.isDynamicallyTyped()) {
      writer.print("def");
    } else {
      field.getType().visit(this);
    }
    writer.print(" ");
    writer.print(field.getName());
    if (field.getInitializerStatement() != null) {
      writer.print(" = ");
      field.getInitializerStatement().visit(this);
    }
    writer.println();
  }

  @Override
  public void handle(FQCN fqcn) {
    if (fqcn.isGenerateQuoted()) {
    	if(isUseImports() && !fqcn.isOverrideImports()) {
    		writer.print(Utils.quotedString(fqcn.getClazzOrInterface().getName()));
    	}	else if(isUseImports() && fqcn.isOverrideImports()) {
    		writer.print(Utils.quotedString(fqcn.getClazzOrInterface().getFQCN()));
    	} else {
    		writer.print(Utils.quotedString(fqcn.getClazzOrInterface().getFQCN()));
    	}
    } else {
    	if(isUseImports() && !fqcn.isOverrideImports()) {
    		writer.print(fqcn.getClazzOrInterface().getName());
    	}	else if(isUseImports() && fqcn.isOverrideImports()) {
    		writer.print(fqcn.getClazzOrInterface().getFQCN());
    	} else {
    		writer.print(fqcn.getClazzOrInterface().getFQCN());    		
    	}
    }
  }

  @Override
  public void handle(GenericType genericType) {
    if (genericType.hasSymbol()) {
      writer.print(genericType.getSymbol());
    }
    if (genericType.getSpecialization() != Specialization.None) {
      writer.print(genericType.getSpecialization().toString().toLowerCase());
    }
    if (genericType.getName() != null) {
      handle((Type) genericType);
    }
  }

  @Override
  public void handle(GetterMethod getterMethod) {
    handle((Method) getterMethod);
  }

  @Override
  public void handle(SetterMethod setterMethod) {
    handle((Method) setterMethod);
  }

  @Override
  public void handle(Import _import) {
  	writer.print("import ").print(_import.getName()).println();
  }

  @Override
  public void handle(Interface _interface) {
    CodePackage codePackage = _interface.getPackage();
    if (codePackage != null) {
      writer.print("package ").print(codePackage.getName()).println();
    }
    if(!_interface.getImports().isEmpty()) {
    	writer.println();
    }
    for (Import import_ : _interface.getImports()) {
			import_.visit(this);
		}
    writer.println();
    printJavaDoc(_interface);
    printAnnotations(_interface);
    printAccessModifiers(_interface);
    writer.print("interface ");
    writer.print(_interface.getName());

    if (_interface.getGenerics().size() > 0) {
      writer.print("<");
      printCodeElementCollection(_interface.getGenerics());
      writer.print(">");
    }

    writer.print(" ");
    if (_interface.getExtendsTypes().size() > 0) {
      writer.print("extends ");
      printCodeElementCollection(_interface.getExtendsTypes());
    }
    writer.println("{");

    for (CodeElement child : _interface.getChildren()) {
      child.visit(this);
    }

    writer.print("}");
  }

  @Override
  public void handle(Method method) {
    printJavaDoc(method);
    printAnnotations(method);
    if (!(method instanceof Constructor) && method.isDynamicallyTyped()) {
      writer.print("def ");
    }
    printAccessModifiers(method);
    method.getReturnType().visit(this);
    // Support static constructors without name nor arguments
    if (method.getName() != null) {
      writer.print(" ").print(method.getName()).print("(");
      printCodeElementCollection(method.getParameters());
      writer.print(") ");
      if (method.getThrowsExceptions().size() > 0) {
        writer.print("throws ");
        printCodeElementCollection(method.getThrowsExceptions());
      }
    }
    if (method.getClazzOrInterfaceOwner().isInterface() || method.hasAccessModifier(AccessModifier.Abstract)) {
      //writer.println(";");
    } else {
      writer.println(" {");
      writeMethodBodyContent(method);
      writer.println("}");
    }
  }

  @Override
  public void handle(PackageFQN packageFQN) {
    if (packageFQN.isGenerateQuoted()) {
      writer.print(Utils.quotedString(packageFQN.getCodePackage().getName()));
    } else {
      writer.print(packageFQN.getCodePackage().getName());
    }
  }

  @Override
  public void handle(Parameter parameter) {
    if (!parameter.isDynamicallyTyped()) {
      parameter.getType().visit(this);
    }
    if (parameter.isVariableArgument()) {
      writer.print("... ");
    } else {
      writer.print(" ");
    }
    writer.print(parameter.getName());

    if (parameter.getDefaultValue() != null) {
      writer.print(" = ");
      if (parameter.getDefaultValue() instanceof Enum) {
        Enum<?> enumValue = (Enum<?>) parameter.getDefaultValue();
        writer.print(enumValue.getDeclaringClass().getName() + "." + enumValue.name());
      } else if (parameter.getDefaultValue() instanceof String) {
        writer.print(Utils.quotedString((String) parameter.getDefaultValue()));
      } else {
        writer.print(parameter.getDefaultValue().toString());
      }
    }
  }
  
  @Override
  public void handle(Property property) {
    if (!property.getOwner().isInterface()) {
      printJavaDoc(property);
      printAnnotations(property);
      printAccessModifiers(property);
      if(!property.hasAccessModifier(AccessModifier.Static)) {
	      if (property.isDynamicallyTyped()) {
	        writer.print("def");
	      } else {
	        property.getType().visit(this);
	      }
	      writer.print(" ");
    	}
      writer.print(property.getName());
      if (property.getInitializerStatement() != null) {
        writer.print(" = ");
        property.getInitializerStatement().visit(this);
      }
      writer.println();
    }

    for (CodeElement child : property.getChildren()) {
      child.visit(this);
    }
//    writer.println();
//    if (!property.hasAccessModifier(AccessModifier.Static)) {
//      // GORM HACK!!!! - GET METHOD
//      for (Annotation annotation : property.getAnnotations()) {
//        handle(annotation);
//      }
//      writer.println();
////      if (property.isDynamicallyTyped())
//        writer.print("def");
////      else
////        handle(property.getType());
//
//      writer.println(" get" + StringUtils.capitalize(property.getName()) + "() {");
//      writer.println("   return " + property.getName());
//      writer.println("}");
//      writer.println();
//      // GORM HACK!!!! - SET METHOD
////      if (property.isDynamicallyTyped())
//        writer.print("def");
////      else
////        writer.print("void");
//
//      writer.print(" set" + StringUtils.capitalize(property.getName()) + "(");
//      if (!property.isDynamicallyTyped()) handle(property.getType());
//      writer.println(" " + property.getName() + ") {");
//      writer.println("   this." + property.getName() + " = " + property.getName()) ;
//      writer.println("}");
//    }
  }

  @Override
  public void handle(Type type) {
    List<? extends Type> generics = type.getGenerics();
    if (type.getClazz() != null) {
    	if(isUseImports()) {
    		writer.print(type.getClazz().getName());
    	} else {
    		if (hasPackageRewriter()) {
    			getPackageRewriter().rewrite(type.getClazz());
    			type.setName(type.getClazz().getFQCN());
    		}
    		writer.print(type.getClazz().getFQCN());
    	}
      if (type.getClazz().getGenerics().size() > 0) {
        generics = type.getClazz().getGenerics();
      }
    } else if (type.getInterface() != null) {
    	if(isUseImports()) {
    		writer.print(type.getInterface().getName());
    	} else {
    		if (hasPackageRewriter()) {
    			getPackageRewriter().rewrite(type.getInterface());
    			type.setName(type.getInterface().getFQCN());
    		}
    		writer.print(type.getInterface().getFQCN());
    	}
      if (type.getInterface().getGenerics().size() > 0) {
        generics = type.getInterface().getGenerics();
      }
    } else {
      if (hasPackageRewriter()) {
        type.setName(getPackageRewriter().rewrite(type.getName()));
      }
      writer.print(type.getName());
    }

    if (generics.size() > 0) {
      writer.print("<");
      printCodeElementCollection(generics);
      writer.print(">");
    }

    if (type.isReference()) {
      writer.print(".class");
    }
  }

  @Override
  public void handle(CodeModel codeModel) {
    // Do nothing so far
  }


  @Override
  public void handle(Closure closure) {
    printCodeElementCollection(closure.getParameters());
    writer.print(" -> ");
    writer.println("{");
    writeMethodBodyContent(closure);
    writer.println("}");
  }

  @Override
  public void handle(GroovyMap groovyMap) {
    writer.println("[");
    int count = 0;
    for (Map.Entry<String, CodeElement> entry: groovyMap.getEntries()) {
      if (count++ > 0) writer.println(", ");
      writer.print(entry.getKey());
      writer.print(": ");
      entry.getValue().visit(this);
    }
    writer.print("]");
  }

  @Override
  public void handle(GroovyList groovyList) {
    writer.println("[");
    int count = 0;
    for (CodeElement entry: groovyList) {
      if (count++ > 0) writer.println(", ");
      entry.visit(this);
    }
    writer.print("]");
  }

	@Override
	public void handle(GormDslCodeSnippet codeSnippet) {
		if(!codeSnippet.getConstraintsEntries().isEmpty()) {
			writer.println("{");
				for (SortedMap.Entry<String, SortedMap<String, String>> contraints : codeSnippet.getConstraintsEntries()) {
					writer.print(contraints.getKey()  + " "/* + "("*/);
					int count = 0;
					for (Map.Entry<String, String> entry : contraints.getValue().entrySet()) {
						if (count++ > 0) writer.print(", ");
						writer.print(entry.getKey() + ": " + entry.getValue());
					}
//					writer.println(")");
					writer.println();
				}
			writer.print("}");
		}
		if(codeSnippet.getMappingSnippet() != null) {
			// TODO: 
		}
		
	}
  
  
  
}