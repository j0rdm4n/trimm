/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.groovy.codedom;

import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeSnippet;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeVisitor;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * A groovy Map that contains key/value pairs
 * 
 * @author Lasse Cramon
 * @author Jeppe Cramon
 */
public class GroovyMap extends CodeSnippet {
	
	private static final long serialVersionUID = 1L;
	
	private Map<String, CodeElement> map = new LinkedHashMap<String, CodeElement>();
  
  @Override
  public void visit(CodeVisitor codeVisitor) {
    if (!(codeVisitor instanceof GroovyCodeDomVisitor)) throw new IllegalArgumentException("Visitor " + codeVisitor.getClass() + " must be of type " + GroovyCodeDomVisitor.class);
    ((GroovyCodeDomVisitor)codeVisitor).handle(this);
  }
  
  public GroovyMap put(String key, String value) {
    map.put(key, new CodeSnippet(value));
    return this;
  }
  
  public GroovyMap put(String key, CodeElement value) {
    map.put(key, value);
    return this;
  }

  public Set<Map.Entry<String, CodeElement>> getEntries() {
    return map.entrySet();
  }
}
