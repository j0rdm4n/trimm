/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.groovy.configuration;

import com.beust.jcommander.JCommander;
import dk.tigerteam.trimm.mdsd.groovy.generator.GroovyGenerator;
import dk.tigerteam.trimm.mdsd.groovy.generator.GroovyGeneratorContext;
import dk.tigerteam.trimm.mdsd.groovy.generator.codewriter.EclipseFormattingCodeWriter;
import dk.tigerteam.trimm.mdsd.groovy.generator.codewriter.GroovyCodeWriter;
import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;
import dk.tigerteam.trimm.mdsd.java.configuration.AbstractConfigurationBasedCodeGenerator;
import dk.tigerteam.trimm.mdsd.java.configuration.EclipseFormatterConfig;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaConfiguration;
import dk.tigerteam.trimm.mdsd.java.configuration.PojoConfigurationBasedCodeGenerator;
import dk.tigerteam.trimm.mdsd.java.generator.DefaultJavaInheritanceAndClazzesStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.RuntimeMetaDataCreator;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.AbstractCodeWriter;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.CodeWritingStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.EclipseCodeFormatter;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.FileSystemCodeWritingStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.event.GeneratorEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.GeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.extension.ImportListener;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

/**
 * Yaml configuration for generating code to Groovy
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class GroovyConfigurationBasedCodeGenerator extends AbstractConfigurationBasedCodeGenerator<GroovyConfiguration, GroovyGeneratorContext> {

  private static final Log log = LogFactory.getLog(GroovyConfigurationBasedCodeGenerator.class);

  // --------------------------------------------------------------------------------------------------------------------------------------------------------
  public static void main(String args[]) {
      System.out.println("*****************************************");
      System.out.println("* TRIMM Groovy code generator           *");
      System.out.println("*****************************************");

      PojoConfigurationBasedCodeGenerator.JavaGeneratorCommandLineArguments generatorArgs = new PojoConfigurationBasedCodeGenerator.JavaGeneratorCommandLineArguments();
       JCommander jCommander = new JCommander(generatorArgs, args);
       if (generatorArgs.help) {
           jCommander.usage();
           System.exit(-1);
       }

       if (generatorArgs.baseDir == null || !generatorArgs.baseDir.exists()) {
           System.err.println("Can't find Base directory '" + generatorArgs.baseDir + "'");
           System.exit(-1);
       }
       File outputDir = generatorArgs.baseDir;
       if (generatorArgs.outputDir != null) {
           outputDir = new File(generatorArgs.baseDir, generatorArgs.outputDir)            ;
       }
       File yamlFile = new File(generatorArgs.baseDir, generatorArgs.yamlFile);

       System.out.println("Using YAML configuration file: '" + yamlFile.getAbsolutePath() + "'");
       System.out.println("Using Base dir               : '" + generatorArgs.baseDir.getAbsolutePath() + "'");
       System.out.println("Using Output dir             : '" + outputDir.getAbsolutePath() + "'");
       if (generatorArgs.forceOverwriteGeneratedExtensionClasses) {
           System.out.println("FORCEFULLY overwriting generated Extension classes!!!");
       }
      GroovyConfigurationBasedCodeGenerator.generate(yamlFile, generatorArgs.baseDir, outputDir, generatorArgs.forceOverwriteGeneratedExtensionClasses, PojoConfigurationBasedCodeGenerator.class.getClassLoader());
   }


  /**
   * Generate code based on input
   *
   * @param yamlConfigurationFile The file pointing to the YAML configuration file
   * @param baseDir               the base dir that this configuration is run in (used for calculating absolute path's)
   * @param baseOutputDir         The directory to use for output files
   * @param forceOverwriteGeneratedExtensionClasses
   *                              Extra option to allow removing ALL generated extension classes (use with care)
   * @param classLoader           The classloader to use (may be null - in which case the {@link dk.tigerteam.trimm.mdsd.java.configuration.AbstractConfigurationBasedCodeGenerator#getClassLoader()} defaults apply
   *
   * @return The classes and interfaces generated
   */
  public static List<ClazzOrInterface> generate(final File yamlConfigurationFile, final File baseDir, final File baseOutputDir, boolean forceOverwriteGeneratedExtensionClasses, final ClassLoader classLoader) {
    GroovyConfiguration groovyConfiguration = null;
    try {
    	groovyConfiguration = parseGroovyConfiguration(yamlConfigurationFile, forceOverwriteGeneratedExtensionClasses);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return generate(groovyConfiguration, yamlConfigurationFile, baseDir, baseOutputDir, classLoader);
  }

  /**
   * Generate code based on input
   *
   * @param groovyConfiguration    The Groovy generator configuration - can be parsed using {@link #parseGroovyConfiguration(java.io.File, boolean)}
   * @param yamlConfigurationFile The file pointing to the YAML configuration file
   * @param baseDir               the base dir that this configuration is run in (used for calculating absolute path's)
   * @param baseOutputDir         The directory to use for output files
   * @param classLoader           The classloader to use (may be null - in which case the {@link dk.tigerteam.trimm.mdsd.java.configuration.AbstractConfigurationBasedCodeGenerator#getClassLoader()} defaults apply
   *
   * @return The classes and interfaces generated
   */
  public static List<ClazzOrInterface> generate(GroovyConfiguration groovyConfiguration, final File yamlConfigurationFile, final File baseDir, final File baseOutputDir, final ClassLoader classLoader) {
    try {
      if (PojoConfigurationBasedCodeGenerator.shouldGenerate(yamlConfigurationFile, baseDir, baseOutputDir, groovyConfiguration)) {
        return new GroovyConfigurationBasedCodeGenerator(groovyConfiguration, baseDir, baseOutputDir, classLoader).generate();
      } else {
        log.info("All resource files are up to date nothing to generate");
        return null;
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Parse the {@link JavaConfiguration} from the yaml file
   *
   * @param yamlConfigurationFile The file pointing to the YAML configuration file
   * @param forceOverwriteGeneratedExtensionClasses
   *                              Extra option to allow removing ALL generated extension classes (use with care)
   * @return The Java codegeneration configuration
   * @throws FileNotFoundException
   */
  public static GroovyConfiguration parseGroovyConfiguration(final File yamlConfigurationFile, boolean forceOverwriteGeneratedExtensionClasses) throws FileNotFoundException {
    FileReader reader = null;
    try {
      reader = new FileReader(yamlConfigurationFile);
      GroovyConfiguration configuration = new Yaml().loadAs(reader, GroovyConfiguration.class);
      configuration.setForceOverwriteGeneratedExtensionClasses(forceOverwriteGeneratedExtensionClasses);
      return configuration;
    } finally {
      IOUtils.closeQuietly(reader);
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Create a new configured generator, call {@link #generate()} to execute the generation
   *
   * @param groovyConfiguration The configuration
   * @param baseDir           the base dir that this configuration is run in (used for calculating absolute path'es)
   * @param baseOutputDir     The directory to use for output files
   */
  public GroovyConfigurationBasedCodeGenerator(GroovyConfiguration groovyConfiguration, File baseDir, File baseOutputDir) {
    super(groovyConfiguration, baseDir, baseOutputDir);
  }

  /**
   * Create a new configured generator, call {@link #generate()} to execute the generation
   *
   * @param groovyConfiguration The configuration
   * @param baseDir           the base dir that this configuration is run in (used for calculating absolute path'es)
   * @param baseOutputDir     The directory to use for output files
   * @param classLoader       The classloader to use
   */
  public GroovyConfigurationBasedCodeGenerator(GroovyConfiguration groovyConfiguration, File baseDir, File baseOutputDir, ClassLoader classLoader) {
    super(groovyConfiguration, baseDir, baseOutputDir, classLoader);
  }

  @Override
  protected GroovyGeneratorContext createGeneratorContext(MetaModel metaModel) {
    return new GroovyGeneratorContext(metaModel);
  }

  @Override
  protected List<ClazzOrInterface> generateInternal(GroovyGeneratorContext generatorContext) {
    GroovyGenerator javaGenerator = new GroovyGenerator();

    if (getConfiguration().getCreateRuntimeMetaModel() != null) {
      generatorContext.addEventListeners(new GeneratorEventListener() {
        @Override
        public void generationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
          new RuntimeMetaDataCreator(
              getConfiguration().getCreateRuntimeMetaModel().getPackageName()
          ).create(allGeneratedClazzes);
        }

        @Override
        public boolean handle(GeneratorEvent event) {
          return true;
        }

        @Override
        public void generationCommenced() {
        }
      });
    }
    
    if (getConfiguration().getRootSuperClass() != null) {
      generatorContext.setInheritanceStrategy(new DefaultJavaInheritanceAndClazzesStrategy() {
        @Override
        public Type resolveSuperClazzType(Clazz clazz) {
          return new Type(getConfiguration().getRootSuperClass());
        }
      });
    } else {
      generatorContext.setInheritanceStrategy(new DefaultJavaInheritanceAndClazzesStrategy());
    }
    if(getConfiguration().getGormSetup() != null) {
    	generatorContext.setExcludeGormMappedProperties(getConfiguration().getGormSetup().isExcludeGormMappedProperties());
    	if(getConfiguration().getGormSetup().getMixinSuperClass() != null) {
    		generatorContext.setMixinSuperClass(getConfiguration().getGormSetup().getMixinSuperClass());
    	}
    }
 
    EclipseCodeFormatter eclipseCodeFormatter = null;
    if(getConfiguration().getEclipseFormatterConfig() != null) {
    	// Unfortunately it wont format groovy source files :-(
    	EclipseFormatterConfig eclipseFormatterConfig = getConfiguration().getEclipseFormatterConfig();
    	eclipseCodeFormatter = new EclipseCodeFormatter(eclipseFormatterConfig.getEclipsePath(),
    			eclipseFormatterConfig.getJavaVmPath(), toAbsolutePath(getBaseDir(), eclipseFormatterConfig.getConfigFilePath()),
    			toAbsolutePath(getBaseDir(), getConfiguration().getGenerateBaseClassesToPath()),
    			toAbsolutePath(getBaseDir(), getConfiguration().getGenerateExtensionClassesToPath()),
    			toAbsolutePath(getBaseDir(), getConfiguration().getGenerateInterfacesToPath()),
    			toAbsolutePath(getBaseDir(), getConfiguration().getGenerateTestClassesToPath()));
    }
    
    if(getConfiguration().isUseImports()) {
    	generatorContext.addAsLastEventListener(new ImportListener(getConfiguration().getBasePackageName()));
    }
    
    AbstractCodeWriter codeWriter = createCodeWriter();
    CodeWritingStrategy codeWritingStrategy = null;
    if(codeWriter instanceof EclipseFormattingCodeWriter && eclipseCodeFormatter != null) {
    	EclipseFormattingCodeWriter eclipseFormattingCodeWriter = (EclipseFormattingCodeWriter)codeWriter;
    	eclipseFormattingCodeWriter.setEclipseCodeFormatter(eclipseCodeFormatter);
    	codeWritingStrategy = new FileSystemCodeWritingStrategy(eclipseFormattingCodeWriter);
    } else {
    	codeWritingStrategy = new FileSystemCodeWritingStrategy((GroovyCodeWriter)codeWriter);
    }
    return javaGenerator.transformAndWriteCode(generatorContext, codeWritingStrategy);
  }


  @SuppressWarnings("unchecked")
	@Override
  protected <T extends AbstractCodeWriter> Class<T> getDefaultCodeWriter() {
    return (Class<T>) GroovyCodeWriter.class;
  }
}
