/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.groovy.generator.codewriter;

import dk.tigerteam.trimm.mdsd.groovy.codedom.GroovyCodeWritingCodeVisitor;
import dk.tigerteam.trimm.mdsd.java.codedom.AbstractCodeWritingVisitor;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeWriter;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaCodeGenerationPaths;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.EclipseCodeFormatter;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.GeneratorSummary.UpdateType;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.JavaCodeWriter;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.PackageRewriter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Uses Eclipse JavaCodeFormatter commandline tool to format source, unfortunately it can't format .groovy files :-(
 * 
 * @author Lasse Cramon
 */
public class EclipseFormattingCodeWriter extends JavaCodeWriter {
	
	private static final Log logger = LogFactory.getLog(EclipseFormattingCodeWriter.class);
	private EclipseCodeFormatter eclipseCodeFormatter;

	public EclipseFormattingCodeWriter(JavaCodeGenerationPaths javaCodeGenerationPaths, PackageRewriter packageRewriter, String rootDirectory, String... excludeFormattingForPackages) {
    super(javaCodeGenerationPaths, packageRewriter, rootDirectory, excludeFormattingForPackages);
    setFileExtension(".groovy");
  }
	
  public EclipseFormattingCodeWriter(JavaCodeGenerationPaths javaCodeGenerationPaths, PackageRewriter packageRewriter, String rootDirectory,
                                    Collection<String> excludeFormattingForPackages) {
    super(javaCodeGenerationPaths, packageRewriter, rootDirectory, excludeFormattingForPackages);
    setFileExtension(".groovy");
  }
  
  public void setEclipseCodeFormatter(EclipseCodeFormatter eclipseCodeFormatter) {
		this.eclipseCodeFormatter = eclipseCodeFormatter;
	}

	@Override
  public List<File> writeCode(Collection<ClazzOrInterface> clazzesToWrite) throws IOException {
    logger.debug("Writing '" + clazzesToWrite.size() + "' artifacts with code writer '" +
        this.getClass().getSimpleName() + "' using file extension '" + getFileExtension() + "'");
    getSummary().setNumberOfGeneratedArtifacts(clazzesToWrite.size());
    // Ensure that all packages have been rewritten prior to writing out ANY code!
    rewritePackageNames(clazzesToWrite);

    List<File> generatedFiles = new LinkedList<File>();

    for (ClazzOrInterface clazz : clazzesToWrite) {
      if (!shouldGenerateSourceFileForClazz(clazz)) {
        // Don't generate
        logger.trace("DontGenerate stereotype found for clazz '" + clazz + "'");
        getSummary().addToNumberOfDontGenerateClasses();
        continue;
      }
      String fqcn = clazz.getFQCN();
      String fqcnPath = generateFqcnPath(fqcn, getFileExtension());

      String destinationFolder = resolveDestionationFolder(clazz);

      File destinationFolderFile = createDestinationFolder(destinationFolder);

      String fullPath = createPackageFolders(fqcnPath, destinationFolderFile);

      File clazzFile = new File(fullPath);
      if (clazzFile.exists()) {
        updateFile(clazzFile, clazz, generatedFiles);
      } else {
        logger.debug("Adding artifact '" + clazz.getName() + "' to new file '" + clazzFile.getAbsolutePath() + "'");
        writeFile(clazz, clazzFile);
        updateSummary(clazz, UpdateType.New);
        generatedFiles.add(clazzFile);
      }
    }
    
    // Format 
    eclipseCodeFormatter.formatCode();
    
    return generatedFiles;
  }
	
	@Override
  protected AbstractCodeWritingVisitor createCodeWritingVisitor(CodeWriter codeWriter)  {
    return new GroovyCodeWritingCodeVisitor(codeWriter, getPackageRewriter(), isUseImports());
  }
}
