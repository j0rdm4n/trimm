/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.groovy.codedom;

import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeSnippet;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeVisitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A groovy List style CodeSnippet
 * 
 * @author Lasse Cramon
 * @author Jeppe Cramon
 */
public class GroovyList extends CodeSnippet implements Iterable<CodeElement> {
	
	private static final long serialVersionUID = 1L;
	
	private List<CodeElement> list = new ArrayList<CodeElement>();
  
  @Override
  public void visit(CodeVisitor codeVisitor) {
    if (!(codeVisitor instanceof GroovyCodeDomVisitor)) throw new IllegalArgumentException("Visitor " + codeVisitor.getClass() + " must be of type " + GroovyCodeDomVisitor.class);
    ((GroovyCodeDomVisitor)codeVisitor).handle(this);
  }
  
  public GroovyList add(String value) {
    if(!list.contains(value)) {
    	list.add(new CodeSnippet(value));
    }
    return this;
  }
  
  public GroovyList add(CodeElement value) {
  	if(!list.contains(value)) {
  		list.add(value);
  	}
    return this;
  }

  @Override
  public Iterator<CodeElement> iterator() {
    return list.iterator();
  }
}
