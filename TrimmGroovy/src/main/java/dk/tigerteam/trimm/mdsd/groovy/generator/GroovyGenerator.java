/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.groovy.generator;

import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.generator.JavaCodeGenerationStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator;
import dk.tigerteam.trimm.mdsd.java.generator.event.PropertyEvent;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;

/**
 * Groovy specialization of {@link JavaGenerator}
 * 
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class GroovyGenerator extends JavaGenerator {
  public GroovyGenerator() {
    super(new GroovyCodeGenerationStrategy());
  }

  public GroovyGenerator(JavaCodeGenerationStrategy javaCodeGenerationStrategy) {
    super(javaCodeGenerationStrategy);
  }

  @Override
  protected PropertyEvent createPropertyEvent(ClazzOrInterface clazz, MetaProperty thisMetaProperty) {
    PropertyEvent propertyEvent = super.createPropertyEvent(clazz, thisMetaProperty);

    // Groovy is intelligent about properties, so we don't need to create field, getter and setter methods
    propertyEvent.setCreateAccessorMethodsForProperty(false);
    propertyEvent.setCreateFieldForProperty(false);

    return propertyEvent;
  }
}
