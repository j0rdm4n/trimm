/*
 * Copyright 2012-2013 Tigerteam Aps.
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.groovy.generator.gorm;

import dk.tigerteam.trimm.mdsd.groovy.codedom.GormDslCodeSnippet;
import dk.tigerteam.trimm.mdsd.groovy.codedom.GroovyList;
import dk.tigerteam.trimm.mdsd.groovy.codedom.GroovyMap;
import dk.tigerteam.trimm.mdsd.groovy.generator.GroovyGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.java.generator.extension.ImportListener;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import dk.tigerteam.trimm.util.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.serial.io.JBossObjectOutputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Groovy Eventlistener that add Grails GORM orm mappings.<br/>
 * <br />
 * See {@link http://grails.org/doc/latest/guide/GORM.html}
 * 
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class GormGeneratorEventListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {
	
	private static final Log logger = LogFactory.getLog(GormGeneratorEventListener.class);
	
  private JBossObjectOutputStream jbossSerializer;
  public static final String EMBEDDED_CLAZZ = "embedded";
  public static final String TRANSIENT = "transient";
  public static String BELONGS_TO = "belongsTo";
  public static String HAS_ONE = "hasOne";
  public static String HAS_MANY = "hasMany";
  public static String MAPPED_BY = "mappedBy";
  public static String MAPPING = "mapping";
  public static String EMBEDDED = "embedded";
  public static String CONSTRAINTS = "constraints";
  public static String TRANSIENTS = "transients";

  public GormGeneratorEventListener() {
    try {
      this.jbossSerializer = new JBossObjectOutputStream(null);
    } catch (IOException e) {
      throw new RuntimeException("Can't configure JBossObjectOutputStream - can't clone and hence can't handle same file classes");
    }
  }
  
  @SuppressWarnings("unchecked")
	protected <T> T smartClone(T objToClone) {
    try {
      return (T) jbossSerializer.smartClone(objToClone);
    } catch (IOException e) {
      throw new RuntimeException("Can't smart-clone " + objToClone.toString(), e);
    }
  }

  @Override
  protected boolean handleClazzEvent(final ClazzEvent event) {
    logger.debug("Handling Gorm mappings for clazz  '" + event.getClazz().getFQCN() + "'");
    if (event.getClazz().isInterface()) {
      return true;
    }
    // TODO: Make configurable ?
//    if (isEmbeddableClazz(event.getClazz())) {
//      logger.debug(event.getClazz().getFQCN() + " is embeddable");
//      event.getClazz().setClazzStyle(ClazzStyle.SameFileClazz);
//
//      GroovyGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {
//        @Override
//        public void execute() {
//          Set<Clazz> alreadyAddedToClazz = new HashSet<Clazz>();
//          CodeModel codeModel = GroovyGeneratorContext.getContext().getCodeModel();
//
//          CodeComposite clazzOwner = event.getClazz().getOwner();
//          event.getClazz().setOwner(null);
//          recursivelyClearCodeModelReference(event.getClazz());
//          for (MetaProperty metaProperty : event.getMetaClazz().getPropertiesOfThisType()) {
//
//            Property property = codeModel.findByMetaTypeId(Property.class, metaProperty.getId());
//            if (property != null) {
//            	if (metaProperty.getCardinalityType() == CardinalityType.Many || metaProperty.getType().isArray()) {
//            		logger.warn(metaProperty.getName() + " in '" + metaProperty.getOwnerClazz().getFQCN() +"' is a many/collection of embeddables which" +
//            				" is not supported by Gorm");
//            	}
//
//              // Change property Type to pretend to be built in - so the FQCN matches
//              property.setType(new Type(event.getClazz().getName()));
//
//              MetaClazz ownerMetaClazz = metaProperty.getOwnerClazz();
//              Clazz ownerClazz = codeModel.getClazzByMetaTypeId(ownerMetaClazz.getId(), Filter.ExcludeExtensionClazzes);
//              Property embeddedProperty = getOrCreateStaticProperty(ownerClazz, EMBEDDED);
//              getOrCreateInitializerStatementAsGroovyList(embeddedProperty).add(Utils.singleQuotedString(property.getName()));
//
//              if (!alreadyAddedToClazz.contains(ownerClazz)) {
//                logger.debug("Adding embeddable clazz '" + event.getClazz().getFQCN() + "' as same file class" +
//                    " in '" + ownerClazz.getFQCN() + "'");
//
//                Clazz sameFileClazzClone = smartClone(event.getClazz());
//                sameFileClazzClone.setOwner(clazzOwner);
//                ownerClazz.add(sameFileClazzClone);
//                alreadyAddedToClazz.add(ownerClazz);
//              }
//            }
//          }
//          recursivelySetCodeModelReference(event.getClazz(), codeModel);
//          event.getClazz().setOwner(clazzOwner);
//        }
//
//        private void recursivelySetCodeModelReference(CodeElement element, CodeModel codeModel) {
//          element.setCodeModel(codeModel);
//          if (element instanceof CodeComposite) {
//            for (CodeElement o : ((CodeComposite) element).getChildrenAsXmiElements()) {
//              recursivelyClearCodeModelReference(o);
//            }
//          }
//
//        }
//
//        private void recursivelyClearCodeModelReference(CodeElement element) {
//          element.setCodeModel(null);
//          if (element instanceof CodeComposite) {
//            for (CodeElement o : ((CodeComposite) element).getChildrenAsXmiElements()) {
//              recursivelyClearCodeModelReference(o);
//            }
//          }
//        }
//
//        @Override
//        public String getDescription() {
//          return "Handling all references to embedded clazz " + event.getMetaClazz().toString();
//        }
//      });
//    }
    return true;
  }

  @Override
  protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
    logger.debug("Handling Gorm mappings for property  '" + event.getProperty().getName() + "'");
    if(event.getMetaProperty().hasStereoType(TRANSIENT)) {
    	Property transientProperty = getOrCreateStaticProperty(event.getClazz(), TRANSIENTS);
    	getOrCreateInitializerStatementAsGroovyList(transientProperty).add(Utils.singleQuotedString(event.getResolvedJavaFieldName()));
    	return true;
    }
    if (event.getMetaProperty().getCardinalityType() == CardinalityType.Many || event.getMetaProperty().getType().isArray()) {
    	if(isEnumerationClazz(event.getProperty().getType())) {
    		Property basicHasManyProperty = getOrCreateStaticProperty(event.getClazz(), HAS_MANY);
        getOrCreateInitializerStatementAsGroovyMap(basicHasManyProperty).put(event.getResolvedJavaFieldName(), event.getProperty().getType().getGenerics().get(0));
    	}
    	if(isEmbeddableClazz(event.getProperty().getType())) {
    		// Handled in ClazzEvent does not seem to be supported by Gorm
    		return true;
    	} else {
    		// Collection of Basic
    		Property basicHasManyProperty = getOrCreateStaticProperty(event.getClazz(), HAS_MANY);
    		getOrCreateInitializerStatementAsGroovyMap(basicHasManyProperty).put(event.getResolvedJavaFieldName(), event.getProperty().getType().getGenerics().get(0));
    	}
    } else {
    	if (event.getMetaProperty().getCardinalityType() == CardinalityType.SingleOptional) {
    		Property constraintsProperty = getOrCreateStaticProperty(event.getClazz(), CONSTRAINTS);
    		GormDslCodeSnippet gormDslSnippet = getOrCreateInitializerStatementAsGormDslSnippet(constraintsProperty);
    		gormDslSnippet.putConstraint(event.getResolvedJavaFieldName(), "nullable", "true");
    	} else if(event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired) {
    		Property constraintsProperty = getOrCreateStaticProperty(event.getClazz(), CONSTRAINTS);
    		GormDslCodeSnippet gormDslSnippet = getOrCreateInitializerStatementAsGormDslSnippet(constraintsProperty);
    		if(event.getProperty().getType() != null && (event.getProperty().getType().getName().equals(String.class.getName()) ||
    				event.getProperty().getType().getName().equalsIgnoreCase("string"))) {
    			gormDslSnippet.putConstraint(event.getResolvedJavaFieldName(), "blank", "false");
    		} else {
    			gormDslSnippet.putConstraint(event.getResolvedJavaFieldName(), "nullable", "false");
    		}
    	}
    	// TODO: minSize & maxSize should properly only apply to String types!
    	if(event.getMetaProperty().getLowerBound() != null) {
    		Property constraintsProperty = getOrCreateStaticProperty(event.getClazz(), CONSTRAINTS);
    		GormDslCodeSnippet gormDslSnippet = getOrCreateInitializerStatementAsGormDslSnippet(constraintsProperty);
    		gormDslSnippet.putConstraint(event.getResolvedJavaFieldName(), "minSize", event.getMetaProperty().getLowerBound().toString());
    	}
    	if(event.getMetaProperty().getUpperBound() != null) {
    		Property constraintsProperty = getOrCreateStaticProperty(event.getClazz(), CONSTRAINTS);
    		GormDslCodeSnippet gormDslSnippet = getOrCreateInitializerStatementAsGormDslSnippet(constraintsProperty);
    		gormDslSnippet.putConstraint(event.getResolvedJavaFieldName(), "maxSize", event.getMetaProperty().getUpperBound().toString());
    	}
    }
    
    addConstraints(event);

    return true;
  }

  @Override
  protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
    logger.debug("Handling Gorm mappings for OneToOneAssociationEvent on property '" + event.getProperty().getName() + "' of type '" +
        event.getProperty().getType().getClazz().getFQCN() + "'");
    if (event.getClazzOrInterface().isInterface() || isEmbeddableClazz(event.getProperty().getType())) {
      return true;
    }

    boolean isBidirectional = event.getMetaProperty().isPartInAnAssociation() && event.getMetaProperty().getAssociation().isBidirectional();
    boolean ownerOfAssociation = event.getMetaProperty().isOwnerOfAssociation();
    logger.trace("Bidirectional '" + isBidirectional + "' and ownerOfAssociation '" + ownerOfAssociation + "'");
    if (isBidirectional && ownerOfAssociation) {
      Property property = getOrCreateStaticProperty(event.getClazz(), HAS_ONE);
      getOrCreateInitializerStatementAsGroovyMap(property).put(event.getResolvedJavaFieldName(), event.getProperty().getType());
      Property constraintsProperty = getOrCreateStaticProperty(event.getClazz(), CONSTRAINTS);
  		GormDslCodeSnippet gormDslSnippet = getOrCreateInitializerStatementAsGormDslSnippet(constraintsProperty);
  		gormDslSnippet.putConstraint(event.getResolvedJavaFieldName(), "unique", "true");
  		removeGormMappedProperty(event);
    } else if(!isEnumerationClazz(event.getProperty().getType())){
      Property property = getOrCreateStaticProperty(event.getClazz(), BELONGS_TO);
      getOrCreateInitializerStatementAsGroovyList(property).add(event.getProperty().getType());
    }
    
    addCardinalityConstraints(event);
    addConstraints(event);
    
    return true;
  }

	private void addCardinalityConstraints(AssociationEvent event) {
		Property constraintsProperty = getOrCreateStaticProperty(event.getClazz(), CONSTRAINTS);
		GormDslCodeSnippet gormDslSnippet = getOrCreateInitializerStatementAsGormDslSnippet(constraintsProperty);
		if(event.getMetaProperty().getCardinalityType() == CardinalityType.SingleOptional) {
			gormDslSnippet.putConstraint(event.getResolvedJavaFieldName(), "nullable", "true");
			
		} else if(event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired) {
			// TODO: if they have superclazz and strategy is tablePerHierarchy = false then it has to be optional
			gormDslSnippet.putConstraint(event.getResolvedJavaFieldName(), "nullable", "false");
		}
	}

  @Override
  protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
    logger.debug("Handling Gorm mappings for ManyToOneAssociationEvent on property '" + event.getProperty().getName() + "' of type '" +
        event.getProperty().getType().getClazz().getFQCN() + "'");
    if (event.getClazzOrInterface().isInterface() || isEmbeddableClazz(event.getProperty().getType())) {
      return true;
    }
    boolean isBidirectional = event.getMetaProperty().getAssociation().isBidirectional();
    boolean ownerOfAssociation = event.getMetaProperty().isOwnerOfAssociation();
    logger.trace("Bidirectional '" + isBidirectional + "' and ownerOfAssociation '" + ownerOfAssociation + "'");
    
    if (isBidirectional /*&& !ownerOfAssociation*/ && !isEnumerationClazz(event.getProperty().getType())) {
      Property property = getOrCreateStaticProperty(event.getClazz(), BELONGS_TO);
      getOrCreateInitializerStatementAsGroovyList(property).add(event.getProperty().getType());
    }
    addCardinalityConstraints(event);
    addConstraints(event);

    return true;
  }

  @Override
  protected boolean handleManyToManyAssociationEvent(ManyToManyAssociationEvent event) {
    logger.debug("Handling Gorm mappings for ManyToManyAssociationEvent on property '" + event.getProperty().getName() + "' of type '" +
        event.getProperty().getType().getName() + "'");
    if (event.getClazzOrInterface().isInterface() || isEmbeddableClazz(event.getProperty().getType())) {
      return true;
    }
    boolean isBidirectional = event.getMetaProperty().getAssociation().isBidirectional();
    boolean ownerOfAssociation = event.getMetaProperty().isOwnerOfAssociation();
    logger.trace("Bidirectional '" + isBidirectional + "' and ownerOfAssociation '" + ownerOfAssociation + "'");

    Property hasManyProperty = getOrCreateStaticProperty(event.getClazz(), HAS_MANY);
    getOrCreateInitializerStatementAsGroovyMap(hasManyProperty).put(event.getResolvedJavaFieldName(), event.getProperty().getType().getGenerics().get(0));
    if (!ownerOfAssociation) {
      Property belongsToProperty = getOrCreateStaticProperty(event.getClazz(), BELONGS_TO);
      getOrCreateInitializerStatementAsGroovyList(belongsToProperty).add(event.getProperty().getType().getGenerics().get(0));
      // Seems Gorm wants the all recursively subclazz to have the belongsTo as well!
      List<Clazz> allSubClazz = findAllSubClazzRecursively(event.getClazz());
      for (Clazz clazz : allSubClazz) {
      	belongsToProperty = getOrCreateStaticProperty(clazz, BELONGS_TO);
  			getOrCreateInitializerStatementAsGroovyList(belongsToProperty).add(event.getProperty().getType().getGenerics().get(0));
  			ImportListener.addImport(clazz, event.getProperty().getType().getGenerics().get(0));
			}
    }
    
    removeGormMappedProperty(event);
    addConstraints(event);

    return true;
  }

  @Override
  protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
    logger.debug("Handling Gorm mappings for OneToManyAssociationEvent on property '" + event.getProperty().getName() + "' of type '" +
        event.getProperty().getType().getName() + "'");
    if (event.getClazzOrInterface().isInterface() || isEmbeddableClazz(event.getProperty().getType().getGenerics().get(0))) {
      return true;
    }
    boolean isBidirectional = event.getMetaProperty().getAssociation().isBidirectional();
    boolean ownerOfAssociation = event.getMetaProperty().isOwnerOfAssociation();
    logger.trace("Bidirectional '" + isBidirectional + "' and ownerOfAssociation '" + ownerOfAssociation + "'");

    if (ownerOfAssociation) {
      // TODO: JoinTable
    }
    Property hasManyProperty = getOrCreateStaticProperty(event.getClazz(), HAS_MANY);
    getOrCreateInitializerStatementAsGroovyMap(hasManyProperty).put(event.getResolvedJavaFieldName(), event.getProperty().getType().getGenerics().get(0));
    if(isBidirectional && checkIfThereIsMoreThanTwoOnManySide(event)) {
    	Property mappedByProperty = getOrCreateStaticProperty(event.getClazz(), MAPPED_BY);
    	getOrCreateInitializerStatementAsGroovyMap(mappedByProperty).put(event.getResolvedJavaFieldName(), Utils.singleQuotedString(event.getResolvedOppositeMetaPropertyName()));
    }
    
    removeGormMappedProperty(event);
    addConstraints(event);

    return true;
  }
  
  private GroovyMap getOrCreateInitializerStatementAsGroovyMap(Property property) {
    GroovyMap groovyMap = (GroovyMap) property.getInitializerStatement();
    if (groovyMap == null) {
      groovyMap = new GroovyMap();
      property.setInitializerStatement(groovyMap);
    }
    return groovyMap;
  }

  private GroovyList getOrCreateInitializerStatementAsGroovyList(Property property) {
  	GroovyList groovyList = (GroovyList) property.getInitializerStatement();
    if (groovyList == null) {
      groovyList = new GroovyList();
      property.setInitializerStatement(groovyList);
    }
    return groovyList;
  }

  private GormDslCodeSnippet getOrCreateInitializerStatementAsGormDslSnippet(Property property) {
  	GormDslCodeSnippet codeSnippet = (GormDslCodeSnippet) property.getInitializerStatement();
    if (codeSnippet == null) {
    	codeSnippet = new GormDslCodeSnippet();
      property.setInitializerStatement(codeSnippet);
    }
    return codeSnippet;
  }

  private Property getOrCreateStaticProperty(Clazz ownerClazz, String propertyName) {
    Property property = ownerClazz.getPropertyByName(propertyName);
    if (property == null) {
      property = new Property(ownerClazz, AccessModifier.Static);
      property.setName(propertyName);
      ownerClazz.addChildren(property);
    }
    return property;
  }
  
  private void addConstraints(PropertyEvent event) {
		if(event.getMetaProperty().hasStereoType(CONSTRAINTS)) {
    	Map<String, String> taggedValues = event.getMetaProperty().getTaggedValues();
    	if(!taggedValues.isEmpty()) {
    		Property constraintsProperty = getOrCreateStaticProperty(event.getClazz(), CONSTRAINTS);
    		GormDslCodeSnippet gormDslSnippet = getOrCreateInitializerStatementAsGormDslSnippet(constraintsProperty);
    		for (Entry<String, String> entry : taggedValues.entrySet()) {
					Map<String, String> propertyConstraints = gormDslSnippet.getPropertyConstraints(event.getResolvedJavaFieldName());
						propertyConstraints.put(entry.getKey(), entry.getValue());
				}
    	}
    }
	}
  
  private void removeGormMappedProperty(AssociationEvent event) {
		if(GroovyGeneratorContext.getContext().isExcludeGormMappedProperties() &&
				!GroovyGeneratorContext.getContext().getCollectionTypeResolver().isBag(event.getMetaProperty()) &&
				!GroovyGeneratorContext.getContext().getCollectionTypeResolver().isOrdered(event.getMetaProperty()) &&
				!GroovyGeneratorContext.getContext().getCollectionTypeResolver().isSorted(event.getMetaProperty())) {
			// Remove Property from owner clazz
			Clazz owner = (Clazz)event.getProperty().getOwner();
			if(owner.getChildren().contains(event.getProperty())) {
				ImportListener.addImport(owner, event.getProperty().getType()); // yikes
				owner.getChildren().remove(event.getProperty());
			}
		}
	}

  private boolean checkIfThereIsMoreThanTwoOnManySide(OneToManyAssociationEvent event) {
  	Clazz clazz = event.getClazz();
  	if(clazz.isBaseClazz()) {
  		clazz = GroovyGeneratorContext.getContext().getExtensionClazzForBaseClazz(clazz);
  	}
  	Type type = new Type(clazz);
  	ClazzOrInterface clazzOrInterface = GroovyGeneratorContext.getContext().getClazzFromMetaClazz(event.getOppositeMetaProperty().getOwnerClazz());
  	if(clazzOrInterface != null && clazzOrInterface.isClazz()) {
  		int typeIndex = 0;
  		Clazz oppositeClazz = (Clazz) clazzOrInterface;
  		List<Property> properties = oppositeClazz.getProperties();
  		for (Property property : properties) {
  			if((property.getType() != null && property.getType().equals(type)) || 
  					(property.getType() != null && property.getType().getClazz() != null && (type.getClazz().isSubClassOfClazz(property.getType().getClazz()) ||
  							property.getType().getClazz().isSubClassOfClazz(type.getClazz())))) {
					typeIndex++;
					if(typeIndex >= 2) {
						return true;
					}
				}
			}
  	}
  	return false;
	}
  
  private List<Clazz> findAllSubClazzRecursively(Clazz clazz) {
  	List<Clazz> result = new ArrayList<Clazz>();
		List<Clazz> subclazzes = clazz.getCodeModel().findAllDirectSubClazzesOfClazz(clazz);
		for (Clazz subclazz : subclazzes) {
			result.add(subclazz);
			result.addAll(findAllSubClazzRecursively(subclazz));
		}
		return result;
  }

	private boolean isEmbeddableClazz(Type type) {
    if (type == null)
      return false;
    return isEmbeddableClazz(type.getClazz());
  }

  private boolean isEmbeddableClazz(Clazz clazz) {
    if (clazz == null)
      return false;
    return isEmbeddableClazz(clazz.getMetaType());
  }

  private boolean isEmbeddableClazz(MetaClazz metaClazz) {
    if (metaClazz == null)
      return false;
    return metaClazz.hasStereoType(EMBEDDED_CLAZZ);
  }
  
  public boolean isEnumerationClazz(Clazz clazz) {
		return clazz instanceof Enumeration;
	}

	public boolean isEnumerationClazz(Type type) {
		return type.getClazz() instanceof Enumeration;
	}

}