/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.groovy.generator.extensions;

import dk.tigerteam.trimm.mdsd.groovy.generator.GroovyGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.FQCN;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import groovy.lang.Category;
import groovy.lang.Mixin;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Creates a companion Mixin class for each Clazz in the model. The original Clazz is marked with the {@link Mixin} annotation which points
 * to the created Mixin clazz<br/>
 * The Mixin class is marked as Extension clazz so it will be generated to the extension source folder (so we don't overwriter it)
 *
 * @author Jeppe Cramon
 */
public class GroovyMixinEventListener extends AbstractGeneratorEventListener {

	private static final Log logger = LogFactory.getLog(GroovyMixinEventListener.class);
	
  @Override
  protected boolean handleClazzEvent(ClazzEvent event) {
    if (!event.getClazz().isClazz() || event.getClazz().isEnumeration()) return true;
    logger.debug("Adding Mixin extension clazz for '" + event.getClazz().getFQCN() + "'");
    Clazz mixinClazz = new Clazz(event.getClazz().getName() + getMixinClazzNameSuffix());
    mixinClazz.addAnnotations(new Annotation(Category.class).withDefaultValueAnnotationAttribute(event.getClazz().asFQCN()));
    mixinClazz.setMetaType(event.getMetaClazz());
    // Mark it as extension class so it will be generated to the extension source folder (so we don't overwriter it)
    mixinClazz.setExtensionClazz(true);
    event.getClazz().getPackage().add(mixinClazz);
    String mixinSuperClass = GroovyGeneratorContext.getContext().getMixinSuperClass();
    if(mixinSuperClass != null) {
    	event.getClazz().addAnnotations(new Annotation(Mixin.class).withDefaultValueAnnotationAttribute(mixinClazz.asFQCN(), new FQCN(new Clazz(mixinSuperClass))));
    } else {
    	event.getClazz().addAnnotations(new Annotation(Mixin.class).withDefaultValueAnnotationAttribute(mixinClazz.asFQCN()));
    }

    return true;
  }

  /**
   * Default value "Mixin"
   */
  protected String getMixinClazzNameSuffix() {
    return "Mixin";
  }
}
