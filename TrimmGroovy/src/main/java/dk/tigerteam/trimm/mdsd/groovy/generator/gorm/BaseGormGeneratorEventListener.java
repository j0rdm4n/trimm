/*
<<<<<<< HEAD
 * Copyright 2012-2013 Tigerteam Aps.
=======
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
>>>>>>> branch 'master' of https://tigerteam@bitbucket.org/tigerteam/trimm.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.groovy.generator.gorm;

import dk.tigerteam.trimm.mdsd.java.generator.event.*;

public abstract class BaseGormGeneratorEventListener extends AbstractGeneratorEventListener {

	@Override
	protected boolean handleMethodEvent(MethodEvent event) {
		return true;
	}

	@Override
	protected boolean handleInterfaceEvent(InterfaceEvent event) {
		return true;
	}

	@Override
	protected boolean handleManyToManyAssociationEvent(ManyToManyAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
		return true;
	}

	@Override
	protected boolean handleEnumeratorEvent(EnumeratorLiteralEvent literalEvent) {
		return true;
	}

	@Override
	protected boolean handleClazzEvent(ClazzEvent event) {
		return true;
	}

	

}
