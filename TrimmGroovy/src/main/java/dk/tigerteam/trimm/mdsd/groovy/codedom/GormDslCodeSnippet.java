/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.groovy.codedom;

import dk.tigerteam.trimm.mdsd.java.codedom.CodeSnippet;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeVisitor;

import java.util.*;

/**
 *  Snippet to handle specific Gorm DSL structure
 * <br/>
 * <code>
 * static mapping = { <br/>
 *		table 'user_' <br/>
 *		version true <br/>
 *		id column: 'user_pk' <br/> 
 *		tablePerHierarchy false <br/>
 *		columns { <br/>
 *			userId column: 'userid', index:'userid_idx', cache: false <br/>
 *			password column: 'password' <br/>
 *			email column: 'email' <br/>
 * 			veterinarian lazy:true, column: 'veterinarian' <br/>
 *			roles column: 'user_role_Id' // uni one-to-many without jointable <br/>
 *			nicknames joinTable: [name: 'user_mf_nicknames', key: 'user_id', column: 'nickname', type: "text"] <br/>
 *		} <br/>
 *	} <br/>
 *	<br/>
 *   static constraints = { <br/>
 *		userId(blank:false,unique:true,nullable:false,matches:'[0-9]{8}[A-Za-z]') <br/>
 *		password(blank:false,nullable:false,size: 6..12, <br/>
 *			validator: { passwd, user -> <br/>
 *				return passwd != user.userId }) <br/>
 *		email(email:true,nullable:true,blank:true) <br/>
 *		veterinarian(nullable:true,blank:true) <br/>
 *   } <br/>
 *	</code>
 * 
 * @author Lasse Cramon
 */
public class GormDslCodeSnippet extends CodeSnippet {

	private static final long serialVersionUID = 1L;

	private MappingSnippet mappingSnippet;
	private SortedMap<String, SortedMap<String, String>> constraintsSnippet = new TreeMap<String, SortedMap<String,String>>();
	
	public MappingSnippet getOrCreateMappingSnippet() {
		if(mappingSnippet == null) {
			this.mappingSnippet = new MappingSnippet();
		}
		return mappingSnippet;
	}
	
	public MappingSnippet getMappingSnippet() {
		return mappingSnippet;
	}

	public void setMappingSnippet(MappingSnippet mappingSnippet) {
		this.mappingSnippet = mappingSnippet;
	}

	public SortedMap<String, SortedMap<String, String>> getConstraintsSnippet() {
		return constraintsSnippet;
	}

	public void setConstraintsSnippet(SortedMap<String, SortedMap<String, String>> constraintsSnippet) {
		this.constraintsSnippet = constraintsSnippet;
	}
	
	public Set<SortedMap.Entry<String, SortedMap<String, String>>> getConstraintsEntries() {
    return constraintsSnippet.entrySet();
  }
	
	public SortedMap<String, String> getPropertyConstraints(String propertyName) {
		SortedMap<String, String> constraints = constraintsSnippet.get(propertyName);
		if(constraints == null) {
			constraints = new TreeMap<String, String>();
			constraintsSnippet.put(propertyName, constraints);
		}
		return constraints;
	}
	
	public void putConstraint(String propertyName, String name, String value) {
		SortedMap<String, String> constraints = constraintsSnippet.get(propertyName);
		if(constraints == null) {
			constraints = new TreeMap<String, String>();
			constraintsSnippet.put(propertyName, constraints);
		}
		constraints.put(name, value);
	}

	public class MappingSnippet {
		
		private String tableName;
		private String version = "true";
		private String idColumnName;
		private String tablePerHierarchy = "true";
		private String tablePerSubclass = "false";
		private String type;
		private String cache = "false";
		private String cascade;
		private String fetch;
		private String lazy;
		private String order;
		private String sort;
		// propertyName column : value, index: ....
		private Map<String, Map<String, String>> columns = new HashMap<String, Map<String,String>>();
		// joinTable  name: value, ....
		private Map<String, Set<JoinTable>> joinTables = new HashMap<String, Set<JoinTable>>();
		
		public String getTableName() {
			return tableName;
		}
		public void setTableName(String tableName) {
			this.tableName = tableName;
		}
		public String getVersion() {
			return version;
		}
		public void setVersion(String version) {
			this.version = version;
		}
		public String getIdColumnName() {
			return idColumnName;
		}
		public void setIdColumnName(String idColumnName) {
			this.idColumnName = idColumnName;
		}
		public String getTablePerHierarchy() {
			return tablePerHierarchy;
		}
		public void setTablePerHierarchy(String tablePerHierarchy) {
			this.tablePerHierarchy = tablePerHierarchy;
		}
		public Map<String, Map<String, String>> getColumns() {
			return columns;
		}
		public void setColumns(Map<String, Map<String, String>> columns) {
			this.columns = columns;
		}
		public String getTablePerSubclass() {
			return tablePerSubclass;
		}
		public void setTablePerSubclass(String tablePerSubclass) {
			this.tablePerSubclass = tablePerSubclass;
		}
		public Map<String, Set<JoinTable>> getJoinTables() {
			return joinTables;
		}
		public void setJoinTables(Map<String, Set<JoinTable>> joinTables) {
			this.joinTables = joinTables;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getCache() {
			return cache;
		}
		public void setCache(String cache) {
			this.cache = cache;
		}
		public String getCascade() {
			return cascade;
		}
		public void setCascade(String cascade) {
			this.cascade = cascade;
		}
		public String getFetch() {
			return fetch;
		}
		public void setFetch(String fetch) {
			this.fetch = fetch;
		}
		public String getLazy() {
			return lazy;
		}
		public void setLazy(String lazy) {
			this.lazy = lazy;
		}
		public String getOrder() {
			return order;
		}
		public void setOrder(String order) {
			this.order = order;
		}
		public String getSort() {
			return sort;
		}
		public void setSort(String sort) {
			this.sort = sort;
		}
	}
	
	public class JoinTable {
		private String name;
		private String key;
		private String column;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public String getColumn() {
			return column;
		}
		public void setColumn(String column) {
			this.column = column;
		}
		public String toString() {
			return "joinTable: [" + name != null ? "name: '" + name + "'" : "" +  key != null ? ", key: '" + key + "'" : "" + column != null ? ", column: '" + column + "'" : "" + "]";
		}
	}
	
	@Override
  public void visit(CodeVisitor codeVisitor) {
    if (!(codeVisitor instanceof GroovyCodeDomVisitor)) throw new IllegalArgumentException("Visitor " + codeVisitor.getClass() + " must be of type " + GroovyCodeDomVisitor.class);
    ((GroovyCodeDomVisitor)codeVisitor).handle(this);
  }
	
}
