/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.groovy.generator;

import dk.tigerteam.trimm.mdsd.java.codedom.CodeModel;
import dk.tigerteam.trimm.mdsd.java.generator.CollectionTypeResolver;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.JavaInheritanceAndClazzesStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.event.GeneratorEventListener;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;

/**
 * Groovy specialization of {@link JavaGeneratorContext}
 * 
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class GroovyGeneratorContext extends JavaGeneratorContext {

	private boolean excludeGormMappedProperties;
	private String mixinSuperClass;
	
  /**
   * Get the currently active {@link GroovyGeneratorContext} execution (is multithread safe)
   */
  public static GroovyGeneratorContext getContext() {
    return getContext(GroovyGeneratorContext.class);
  }

  public GroovyGeneratorContext(MetaModel metaModel) {
    super(metaModel);
  }

  public GroovyGeneratorContext(MetaModel metaModel, CodeModel codeModel, CollectionTypeResolver collectionTypeResolver,
                                JavaInheritanceAndClazzesStrategy inheritanceStrategy, boolean createExtensionClazzes,
                                boolean addExtensionAndBaseClazzAnnotations, String collectionPropertyNameSuffix,
                                boolean addDefaultConstructor, GeneratorEventListener... eventListeners) {
    super(metaModel, codeModel, collectionTypeResolver, inheritanceStrategy, createExtensionClazzes, addExtensionAndBaseClazzAnnotations,
        collectionPropertyNameSuffix, addDefaultConstructor, eventListeners);
  }

	public boolean isExcludeGormMappedProperties() {
		return excludeGormMappedProperties;
	}

	public void setExcludeGormMappedProperties(boolean excludeGormMappedFields) {
		this.excludeGormMappedProperties = excludeGormMappedFields;
	}

	public String getMixinSuperClass() {
		return mixinSuperClass;
	}

	public void setMixinSuperClass(String mixinSuperClass) {
		this.mixinSuperClass = mixinSuperClass;
	}
}
