/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.groovy.generator.codewriter;

import dk.tigerteam.trimm.mdsd.groovy.codedom.GroovyCodeWritingCodeVisitor;
import dk.tigerteam.trimm.mdsd.java.codedom.AbstractCodeWritingVisitor;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeWriter;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaCodeGenerationPaths;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.JavaCodeWriter;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.PackageRewriter;

import java.util.Collection;

/**
 * The Groovy Code Writer
 *
 * @author Lasse Cramon
 * @author Jeppe Cramon
 */
public class GroovyCodeWriter extends JavaCodeWriter {

  public GroovyCodeWriter(JavaCodeGenerationPaths javaCodeGenerationPaths, PackageRewriter packageRewriter, String rootDirectory, String... excludeFormattingForPackages) {
    super(javaCodeGenerationPaths, packageRewriter, rootDirectory, excludeFormattingForPackages);
    setFileExtension(".groovy");
  }

  public GroovyCodeWriter(JavaCodeGenerationPaths javaCodeGenerationPaths, PackageRewriter packageRewriter, String rootDirectory, Collection<String> excludeFormattingForPackages) {
    super(javaCodeGenerationPaths, packageRewriter, rootDirectory, excludeFormattingForPackages);
    setFileExtension(".groovy");
  }

  @Override
  protected AbstractCodeWritingVisitor createCodeWritingVisitor(CodeWriter codeWriter)  {
    return new GroovyCodeWritingCodeVisitor(codeWriter, getPackageRewriter(), isUseImports());
  }
}
