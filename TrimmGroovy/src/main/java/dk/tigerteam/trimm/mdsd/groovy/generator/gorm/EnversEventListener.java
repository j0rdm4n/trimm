/*
 * Copyright 2012-2013 Tigerteam Aps.
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.groovy.generator.gorm;

import dk.tigerteam.trimm.mdsd.groovy.generator.GroovyGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.java.util.MdsdJavaUtil;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.util.List;

public class EnversEventListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

	private static final Log logger = LogFactory.getLog(EnversEventListener.class);
	
	public static final String HISTORY_STEROETYPE_NAME = "history";
	public static final String AUDIT_STEROETYPE_NAME = "audit";
	public static final String NO_HISTORY_STEROETYPE_NAME = "nohistory";
	public static final String NO_AUDIT_STEROETYPE_NAME = "noaudit";
	
	@Override
	protected boolean handleClazzEvent(ClazzEvent event) {
		if(!event.getClazz().isEnumeration() && !event.getClazz().isExtensionClazz()) {
			logger.debug("Adding Audit on clazz '" + event.getClazz().getFQCN() + "'");
			event.getClazz().addAnnotations(new Annotation(Audited.class));
		}
//		if(hasHistoryStereotype(event.getMetaClazz())) {
//			Clazz clazz = event.getClazz();
//			logger.debug("Clazz '" + clazz.getFQCN() + "' has stereotype History or Audit ");
//			Annotation annotation = new Annotation(Audited.class);
//			List<Clazz> allSuperClazzes = clazz.getAllSuperClazzes();
//			if(!allSuperClazzes.isEmpty()) {
//				logger.warn("Clazz '" + clazz.getFQCN() + "' has Audit annotation and has super clazzes, Envers requires them to have Audit annotation also ... they be added if they not present");
//			}
//			for (Clazz superClazz : allSuperClazzes) {
//				boolean clazzHasAuditedAnnotation = hasAuditedAnnotation(superClazz) || hasHistoryStereotype(superClazz.getMetaType());
//				if(!clazzHasAuditedAnnotation) {
//					superClazz.addAnnotations(new Annotation(Audited.class));
//					handleRelationTarget(superClazz);
//				}
//			}
//			clazz.addAnnotations(annotation);
//			handleRelationTarget(clazz);
//		}
		return true;
	}
	
	@Override
	protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
		if(hasNoHistoryStereotype(event.getMetaProperty())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' has stereotype NO History or Audit");
			property.addAnnotations(new Annotation(NotAudited.class));
			return true;
		}
		if(hasHistoryStereotype(event.getMetaProperty())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' has stereotype History or Audit");
			boolean clazzHasAuditedAnnotation = doesPropertyClazzHaveHistoryStereotypeOrAuditedAnnotation(property);
			if(!clazzHasAuditedAnnotation) {
				property.addAnnotations(new Annotation(Audited.class));
			}
		}
		return true;
	}
	
	private boolean doesPropertyClazzHaveHistoryStereotypeOrAuditedAnnotation(Property property) {
		boolean isClazz = property.getOwner().isClazz();
		boolean clazzHasAuditedAnnotation = false;
		if(isClazz) {
			Clazz clazz = (Clazz) property.getOwner();
			clazzHasAuditedAnnotation = hasAuditedAnnotation(clazz) || hasHistoryStereotype(clazz.getMetaType());
			logger.debug("Property '" + property.getName() + "' belonging to clazz '" + clazz.getName() + "' has" +
					" already got audited annotation or history stereotype '" + clazzHasAuditedAnnotation + "'");
		}
		return clazzHasAuditedAnnotation;
	}
	
	@Override
	protected boolean handleManyToManyAssociationEvent(ManyToManyAssociationEvent event) {
		if(hasNoHistoryStereotype(event.getMetaProperty())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' has stereotype NO History or Audit");
			property.addAnnotations(new Annotation(NotAudited.class));
			return true;
		}
		
		return true;
	}

	@Override
	protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
		if(hasNoHistoryStereotype(event.getMetaProperty())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' has stereotype NO History or Audit");
			property.addAnnotations(new Annotation(NotAudited.class));
			return true;
		}
		
		return true;
	}

	@Override
	protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
		if(hasNoHistoryStereotype(event.getMetaProperty())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' has stereotype NO History or Audit");
			property.addAnnotations(new Annotation(NotAudited.class));
			return true;
		}
		
		return true;
	}

	@Override
	protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
		if(hasNoHistoryStereotype(event.getMetaProperty())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' has stereotype NO History or Audit");
			property.addAnnotations(new Annotation(NotAudited.class));
			return true;
		}
		
		return true;
	}
	
	@SuppressWarnings("unused")
	private void handleRelationTarget(Clazz clazz) {
		List<Property> properties = clazz.getProperties();
		for (Property property : properties) {
			if(property.getType() != null) {
				Clazz propClazz = null;
				if(property.getType().getClazz() != null) {
					propClazz = property.getType().getClazz();
				} else if(MdsdJavaUtil.isPropertyCollection(property)) {
					propClazz = property.getType().getGenerics().get(0).getClazz();
				}
				if(propClazz != null && !propClazz.isEnumeration() && (!hasHistoryStereotype(propClazz.getMetaType()) ||
					!hasAuditedAnnotation(propClazz))) {
					Annotation annotation = new Annotation(Audited.class);
					annotation.addAnnotationAttribute("targetAuditMode", RelationTargetAuditMode.NOT_AUDITED);
					property.addAnnotations(annotation);
				}
			}
		}
	}



//	private void handleRelationTarget(AssociationEvent event) {
//		Property property = findPropertyFromMetaClazzByPropertyName(event.getOppositeMetaProperty().getOwnerClazz(), event.getResolvedOppositeMetaPropertyName());
//		boolean hasNoHistoryStereotype = hasNoHistoryStereotype(event.getOppositeMetaProperty().getAssociation());
//		boolean hasHistoryStereotype = !event.getOppositeMetaProperty().getOwnerClazz().isEnumerationClazz() && hasHistoryStereotype(event.getOppositeMetaProperty().getOwnerClazz());
//		boolean hasClazzAuditAnnotation = !event.getOppositeMetaProperty().getOwnerClazz().isEnumerationClazz() && hasClazzAuditAnnotation(event.getOppositeMetaProperty().getOwnerClazz());
//		boolean hasPropertyAuditAnnotation = property != null && (hasAuditedAnnotation(property) || hasAuditedAnnotation(property));
//		boolean hasPropertyNotAuditAnnotation = property != null && (hasNotAuditedAnnotation(property) || hasNotAuditedAnnotation(property));
//		if(event.getMetaProperty().getAssociation().isBidirectional() || hasPropertyAuditAnnotation ) {
//			return;
//		} else if(hasPropertyNotAuditAnnotation ||
//				 !hasClazzAuditAnnotation ||
//				 !hasHistoryStereotype ||
//				 hasNoHistoryStereotype) { 
//			logger.debug("Adding RelationTargetAuditMode.NOT_AUDITED for assocation oppositeside property '" + event.getResolvedOppositeMetaPropertyName() + "'");
//			Annotation annotation = new Annotation(Audited.class);
//			annotation.addAnnotationAttribute("targetAuditMode", RelationTargetAuditMode.NOT_AUDITED);
//			event.getProperty().addAnnotations(annotation);
//		}
//	}
	
	private boolean hasHistoryStereotype(MetaType metaType) {
		if(metaType != null && (metaType.hasStereoType(HISTORY_STEROETYPE_NAME) ||
				metaType.hasStereoType(AUDIT_STEROETYPE_NAME))) {
			return true;
		}
		return false;
	}
	
	private boolean hasNoHistoryStereotype(MetaType metaType) {
		if(metaType != null && (metaType.hasStereoType(NO_HISTORY_STEROETYPE_NAME) ||
				metaType.hasStereoType(NO_AUDIT_STEROETYPE_NAME))) {
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unused")
	private boolean hasClazzAuditAnnotation(MetaClazz metaClazz) {
		ClazzOrInterface clazzOrInterface = GroovyGeneratorContext.getContext().getClazzFromMetaClazz(metaClazz);
		if(clazzOrInterface != null && clazzOrInterface.isClazz()) {
			return clazzOrInterface.hasAnnotation(Audited.class);
		}
		return false;
	}
	
	@SuppressWarnings("unused")
	private Property findPropertyFromMetaClazzByPropertyName(MetaClazz metaClazz, String propertyName) {
		ClazzOrInterface clazzOrInterface = GroovyGeneratorContext.getContext().getClazzFromMetaClazz(metaClazz);
		if(clazzOrInterface != null && clazzOrInterface.isClazz()) {
			Clazz clazz = (Clazz) clazzOrInterface;
			Property property = clazz.getPropertyByName(propertyName);
//			logger.debug("Found property '" + property != null ? property.getName() : "null " + " serching in clazz '" 
//					+ clazz != null ? clazz.getName() : "null " + " with property search name '" + propertyName + "'");
			return property;
		}
		return null;
	}
	
	private boolean hasAuditedAnnotation(CodeElement element) {
		return element.hasAnnotation(Audited.class);
	}
	
	protected boolean hasNotAuditedAnnotation(CodeElement element) {
		return element.hasAnnotation(NotAudited.class);
	}

}
