/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.openjpa.util;

import dk.tigerteam.trimm.persistence.util.ProxyHandler;

import javax.persistence.EntityManager;

// TODO:
public class OpenJpaProxyHandler implements ProxyHandler {

	@SuppressWarnings("unused")
	private EntityManager entityManager;
	
	public OpenJpaProxyHandler(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	
	@Override
	public boolean isProxy(Object in) {
		return false;
	}

	@Override
	public <T> T unproxyAndReassociateIfNecessary(T in) {
		return in;
	}

	@Override
	public <T> T unproxyIfNecessary(T in) {
		return in;
	}

	@Override
	public Class<?> getClassWithoutInitializingProxy(Object in) {
		return in.getClass();
	}

}
