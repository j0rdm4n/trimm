/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.util;

import dk.tigerteam.trimm.util.IndentPrintWriter;
import dk.tigerteam.trimm.util.ReflectUtils;
import dk.tigerteam.trimm.util.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.*;

public class ObjectGraphUtil {
	
	private static final Log logger = LogFactory.getLog(ObjectGraphUtil.class);
	
    public static void printGraphRecursively(Object rootObject, Writer writer, ObjectPrinter... objectPrinters) {
            IndentPrintWriter indentPrintWriter = new IndentPrintWriter(writer, true);
            Set<Object> printedObjects = new HashSet<Object>();
            internalRecursivelyPrintGraph(rootObject, indentPrintWriter, printedObjects, objectPrinters);
        }

        private static void internalRecursivelyPrintGraph(final Object object, final IndentPrintWriter indentPrintWriter, final Set<Object> printedObjects,
                                                          final ObjectPrinter... objectPrinters) {
            if (object == null) {
                indentPrintWriter.println("<null>");
                return;
            }
            if (printedObjects.contains(object)) {
                indentPrintWriter.println(object.getClass().getName() + "@" + System.identityHashCode(object));
            } else if (object instanceof Collection) {
                Collection<?> collection = (Collection<?>) object;
                indentPrintWriter.println("[");
                indentPrintWriter.indent();
                int index = 0;
                for (Object subObject : collection) {
                    if (index > 0) {
                        indentPrintWriter.print(", ");
                    }
                    internalRecursivelyPrintGraph(subObject, indentPrintWriter, printedObjects, objectPrinters);
                    index++;
                }
                indentPrintWriter.outdent();
                indentPrintWriter.println("]");
            } else if (object instanceof Map) {
                Map<?, ?> map = (Map<?, ?>) object;
                indentPrintWriter.println("[");
                indentPrintWriter.indent();
                int index = 0;
                for (Object key : map.keySet()) {
                    if (index > 0) {
                        indentPrintWriter.print(", ");
                    }
                    internalRecursivelyPrintGraph(key, indentPrintWriter, printedObjects, objectPrinters);
                    indentPrintWriter.print(" => ");
                    internalRecursivelyPrintGraph(map.get(key), indentPrintWriter, printedObjects, objectPrinters);
                    index++;
                }
                indentPrintWriter.outdent();
                indentPrintWriter.println("]");
            } else if (object.getClass().isArray()) {
                List<?> collection = Utils.arrayObjectToList(object);
                indentPrintWriter.println("[");
                indentPrintWriter.indent();
                int index = 0;
                for (Object subObject : collection) {
                    if (index > 0) {
                        indentPrintWriter.print(", ");
                    }
                    internalRecursivelyPrintGraph(subObject, indentPrintWriter, printedObjects, objectPrinters);
                    index++;
                }
                indentPrintWriter.outdent();
                indentPrintWriter.println("]");
            } else if (object instanceof Enum) {
                indentPrintWriter.println(object.getClass().getName() + "." + object.toString());
            } else if (object.getClass().getName().startsWith("java.") || object.getClass().getName().startsWith("javax.")) {
                indentPrintWriter.println(object.getClass().getName() + " -> " + object.toString());
            } else {
                executeMatchingObjectPrinterOrPerformDefaultObjectPrinter(object, printedObjects, indentPrintWriter, objectPrinters,
                        new ObjectPrinter() {

                            @Override
                            public boolean support(Object object) {
                                return true;
                            }

                            @Override
                            public void print(Object object, Set<Object> printedObjects, IndentPrintWriter indentPrintWriter) {
                                indentPrintWriter.println(object.getClass().getName() + "@" + System.identityHashCode(object) + " {");
                                printedObjects.add(object);
                                indentPrintWriter.indent();

                                Collection<Field> fields = ReflectUtils.getFields(object.getClass());
                                for (Field field : fields) {
                                    if (!Modifier.isStatic(field.getModifiers()) && !Modifier.isTransient(field.getModifiers())) {
                                        ReflectUtils.makeAccessible(field);
                                        indentPrintWriter.print(field.getName() + ": ");
                                        try {
                                            Object fieldValue = ReflectUtils.getValueUsingReadMethodForField(object, field);
                                            internalRecursivelyPrintGraph(fieldValue, indentPrintWriter, printedObjects, objectPrinters);
                                        } catch (RuntimeException e) {
                                            if (e.getCause() != null && e.getCause().getCause() instanceof NoSuchMethodException) {
                                                // Ignore, not all fields are exposed
                                                Object fieldValue = ReflectUtils.getFieldValue(field, object);
                                                internalRecursivelyPrintGraph(fieldValue, indentPrintWriter, printedObjects, objectPrinters);
                                            } else {
                                                throw e;
                                            }
                                        }
                                    }
                                }
                                indentPrintWriter.outdent();
                                indentPrintWriter.println("}");
                            }
                        });
            }
        }

        public static void executeMatchingObjectPrinterOrPerformDefaultObjectPrinter(final Object object, final Set<Object> printedObjects,
                                                                                     final IndentPrintWriter indentPrintWriter, final ObjectPrinter[] objectPrinters,
                                                                                     final ObjectPrinter defaultObjectPrinter) {
            if (objectPrinters != null) {
                for (ObjectPrinter objectPrinter : objectPrinters) {
                    if (objectPrinter.support(object)) {
                        objectPrinter.print(object, printedObjects, indentPrintWriter);
                        return;
                    }
                }
            }
            defaultObjectPrinter.print(object, printedObjects, indentPrintWriter);
        }

	
	public static boolean areTheGraphsEqual(Object graph1, Object graph2, ProxyHandler proxyHandler, Field...ignoredFields) {
		Set<Object> checkedObjectsInGraph1 = new HashSet<Object>(); 
		Set<Field> ignoredFieldsSet = new HashSet<Field>(Arrays.asList(ignoredFields));
		Stack<Object> debugStack = new Stack<Object>();
		return internalRecursivelyCompareGraphs(graph1, graph2, proxyHandler, checkedObjectsInGraph1, ignoredFieldsSet, debugStack, null);
	}

	@SuppressWarnings("deprecation")
	private static boolean internalRecursivelyCompareGraphs(Object graph1, Object graph2, ProxyHandler proxyHandler, Set<Object> checkedObjectsInGraph1, Set<Field> ignoredFieldsSet, Stack<Object> debugStack, Field fieldBeingProcessed) {
		if (graph1 == null && graph2 == null) {
			return true;
		} else if (graph1 != null && graph2 == null) {
			if (graph1.getClass().isArray() && Utils.arrayObjectToList(graph1).size() == 0) {
				return true; 
			} else {
				if (logger.isDebugEnabled()) {
					logger.debug("Object 1 has value " + graph1 + " but Object 2's is null. Callstack: " + debugStack.toString());
				}
				return false;
			}
		} else if (graph1 == null && graph2 != null) {
			if (graph2.getClass().isArray() && Utils.arrayObjectToList(graph2).size() == 0) {
				return true;
			} else {
				if (logger.isDebugEnabled()) {
					logger.debug("Object 1 is null, but Object 2 has value " + graph2 + ". Callstack: " + debugStack.toString());
				}
				return false;
			}
		}
		
		if(proxyHandler != null) {
			graph1 = proxyHandler.unproxyIfNecessary(graph1);
			graph2 = proxyHandler.unproxyIfNecessary(graph2);
		}
		
		if (!graph1.getClass().equals(graph2.getClass())) {
			boolean dateExceptionFromRule = graph1 instanceof Date && graph2 instanceof Date;
			if (!dateExceptionFromRule) {
				if (logger.isDebugEnabled()) {
					logger.debug("Object 1 of type " + graph1.getClass() + " didn't compare to Object 2's type " + graph2.getClass() + ". Callstack: " + debugStack.toString());
				}
				return false;
			}
		}
		
		if (checkedObjectsInGraph1.contains(graph1)) {
			return true;
		} else {
			if (graph1 instanceof Collection) {
				Collection<?> graph1Collection = (Collection<?>) graph1;
				Collection<?> graph2Collection = (Collection<?>) graph2;
				if(proxyHandler != null) {
					graph1 = proxyHandler.unproxyIfNecessary(graph1Collection);
					graph2 = proxyHandler.unproxyIfNecessary(graph2Collection);
				}
				if (graph1Collection.size() != graph2Collection.size()) {
					if (logger.isDebugEnabled()) {
						logger.debug("Object 1 of Collection size " + graph1Collection.size() + " didn't compare to Object 2's collection size " + graph2Collection.size() + ". Callstack: " + debugStack.toString());
					}
					return false;
				}
				
				Iterator<?> graph2Iterator = graph2Collection.iterator();
				int index = 0;
				for (Object subObject1 : graph1Collection) {
					Object subObject2 = graph2Iterator.next();
					debugStack.push("[" + index + "]");
					if (!internalRecursivelyCompareGraphs(subObject1, subObject2, proxyHandler, checkedObjectsInGraph1, ignoredFieldsSet, debugStack, null)) {
						return false;
					}
					index++;
					debugStack.pop();
				}
			} else if (graph1 instanceof Map) {
				Map<?, ?> map1 = (Map<?, ?>) graph1;
				Map<?, ?> map2 = (Map<?, ?>) graph2;
				
				if (map1.size() != map2.size()) {
					return false;
				}
				
				for (Object key1 : map1.keySet()) {
					Object key2 = map2.get(key1);
					if (key2 == null) {
						if (logger.isDebugEnabled()) {
							logger.debug("Object2 map didn't contain key '" + key1 + "'. Callstack: " + debugStack.toString());
						}
						return false;
					}
					
					Object value1 = map1.get(key1);
					Object value2 = map2.get(key2);
					debugStack.push("[" + key1 + "]");
					if (!internalRecursivelyCompareGraphs(value1, value2, proxyHandler, checkedObjectsInGraph1, ignoredFieldsSet, debugStack, null)) {
						return false;
					}
					debugStack.pop();
				}
			} else if (graph1.getClass().isArray()) {
				List<?> graph1Collection = Utils.arrayObjectToList(graph1);
				List<?> graph2Collection = Utils.arrayObjectToList(graph2);
				for (int i = 0; i < graph1Collection.size(); i++) {
					Object subObject1 = graph1Collection.get(i);
					Object subObject2 = graph2Collection.get(i);
					debugStack.push("[" + i + "]");
					if (!internalRecursivelyCompareGraphs(subObject1, subObject2, proxyHandler, checkedObjectsInGraph1, ignoredFieldsSet, debugStack, null)) {
						return false;
					}
					debugStack.pop();
				}
			} else if (graph1 instanceof Date) { 
				// Check for JPA temporal type (so that we only compare the values that would be returned after a roundtrip to the db)
				// This allows us to compare saved and unsaved entities
				Date date1 = (Date)graph1;
				Date date2 = (Date)graph2;
				boolean equal = true;
				if (fieldBeingProcessed != null && fieldBeingProcessed.getAnnotation(Temporal.class) != null) {
					Temporal temporal = fieldBeingProcessed.getAnnotation(Temporal.class);
					if (temporal.value() == TemporalType.DATE) {
						equal = date1.getDay() == date2.getDay() && date1.getMonth() == date2.getMonth() && date1.getYear() == date2.getYear();
					} else if (temporal.value() == TemporalType.TIME) {
						equal = date1.getHours() == date2.getHours() && date1.getMinutes() == date2.getMinutes() && date1.getSeconds() == date2.getSeconds();
					} if (temporal.value() == TemporalType.TIMESTAMP) {
						equal = date1.getTime() == date2.getTime();
					}
				} else {
					equal = date1.getTime() == date2.getTime();
				}
				
				if (!equal) {
					if (logger.isDebugEnabled()) {
						logger.debug("Object 1 date '" + graph1 + "' wasn't the same as Object 2' date '" + graph2 + "'. Callstack: " + debugStack.toString());
					}
					return false;
				}
			} else if (graph1 instanceof BigDecimal) { 
				BigDecimal bigDecimal1 = (BigDecimal) graph1;
				BigDecimal bigDecimal2 = (BigDecimal) graph2;
				if (bigDecimal1.scale() < bigDecimal2.scale()) {
					bigDecimal1 = bigDecimal1.setScale(bigDecimal2.scale());
				} else if (bigDecimal1.scale() > bigDecimal2.scale()) {
					bigDecimal2 = bigDecimal2.setScale(bigDecimal1.scale());
				} 
				if (!bigDecimal1.equals(bigDecimal2)) {
					if (logger.isDebugEnabled()) {
						logger.debug("Object 1 value '" + graph1 + "' wasn't the same as Object 2' value '" + graph2 + "'. Callstack: " + debugStack.toString());
					}
					return false;
				}
			} else if (graph1.getClass().getName().startsWith("java.") || graph1.getClass().getName().startsWith("javax.") || graph1.getClass().getName().startsWith("org.joda")) {
				if (!graph1.equals(graph2)) {
					if (logger.isDebugEnabled()) {
						logger.debug("Object 1 value '" + graph1 + "' wasn't the same as Object 2' value '" + graph2 + "'. Callstack: " + debugStack.toString());
					}
					return false;
				}
      } else if (graph1.getClass().isEnum()) {
        return graph1 == graph2;
			} else {
				checkedObjectsInGraph1.add(graph1);
				Collection<Field> fields = ReflectUtils.getFields(graph1.getClass());
				for (Field field : fields) {
					if (!Modifier.isStatic(field.getModifiers()) && !Modifier.isTransient(field.getModifiers()) && !ignoredFieldsSet.contains(field)) {
						ReflectUtils.makeAccessible(field);
						// If possible use the Read method
						Object fieldValueInGraph1;
						Object fieldValueInGraph2;
						try {
							fieldValueInGraph1 = ReflectUtils.getValueUsingReadMethodForField(graph1, field);
							fieldValueInGraph2 = ReflectUtils.getValueUsingReadMethodForField(graph2, field);
						} catch (Exception e) {
							fieldValueInGraph1 = ReflectUtils.getFieldValue(field, graph1);
							fieldValueInGraph2 = ReflectUtils.getFieldValue(field, graph2);
						}
						
						debugStack.push(field);
						if (!internalRecursivelyCompareGraphs(fieldValueInGraph1, fieldValueInGraph2, proxyHandler, checkedObjectsInGraph1, ignoredFieldsSet, debugStack, field)) {
							return false;
						}
						debugStack.pop();
					}
				}
			}
			return true;
		}
	}
	
	public static void codifyGraphRecursively(Object rootObject, Writer writer, Field...ignoredFields) {
		IndentPrintWriter indentPrintWriter = new IndentPrintWriter(writer, true);
		// Object as key and variable name as value
		Map<Object, String> allInstances = new HashMap<Object, String>();
		Set<Field> ignoredFieldsSet = new HashSet<Field>(Arrays.asList(ignoredFields));
		internalRecursivelyCodifyInstantiationsGraph(rootObject, indentPrintWriter, allInstances, new HashMap<Class<?>, Integer>(), ignoredFieldsSet);	
		internalRecursivelyCodifyGraphReferences(rootObject, indentPrintWriter, allInstances, new HashSet<Object>(), ignoredFieldsSet, null);
		indentPrintWriter.println(";");
	}
	
	private static void internalRecursivelyCodifyGraphReferences(
			Object object, IndentPrintWriter writer,
			Map<Object, String> allInstances, Set<Object> processedObjects, Set<Field> ignoredFieldsSet, Field fieldBeingProcessed) {
		if (object == null) {
			return;
		}
		
		if (object instanceof Collection) {
			Collection<?> collection = (Collection<?>) object;
			
			if (collection.size() > 0) {
				writer.println(".addTo" + Utils.capitalize(fieldBeingProcessed.getName()) + "(");
				writer.indent();
				
				int index = 0;
				for (Object subObject : collection) {
					if (index > 0) {
						writer.println(", ");
					}
					internalRecursivelyCodifyGraphReferences(subObject, writer, allInstances, processedObjects, ignoredFieldsSet, null);
					index++;
				}
				writer.outdent();
				writer.println(")");
			}
		} else if (object instanceof Map) {
//			Map<?, ?> map = (Map<?, ?>) object;
//			for (Object key : map.keySet()) {
//				internalRecursivelyCodifyGraphReferences(key, writer, allInstances, null);
//				internalRecursivelyCodifyGraphReferences(map.get(key), writer, allInstances, null);
//			}
		} else if (object.getClass().isArray()) {
			List<?> collection = Utils.arrayObjectToList(object);
			if (collection.size() > 0) {
				writer.println(".with" + Utils.capitalize(fieldBeingProcessed.getName()) + "(");
				writer.indent();
				
				writer.print("new " + object.getClass().getComponentType().getName() + "[] {");
				int index = 0;
				for (Object subObject : collection) {
					if (index > 0) {
						writer.println(", ");
					}
					internalRecursivelyCodifyGraphReferences(subObject, writer, allInstances, processedObjects, ignoredFieldsSet, null);
					index++;
				}
				writer.print("}");
				writer.outdent();
				writer.println(")");
			}
		} else if (object instanceof Enum) {
			if (fieldBeingProcessed != null) {
				writer.print(".with" + Utils.capitalize(fieldBeingProcessed.getName()) + "(");
			}
			writer.print(object.getClass().getName() + "." + object.toString());
			if (fieldBeingProcessed != null) {
				writer.println(")");
			}
		} else if (object.getClass().getName().startsWith("java.") || object.getClass().getName().startsWith("javax.")) {
			if (fieldBeingProcessed != null) {
				writer.print(".with" + Utils.capitalize(fieldBeingProcessed.getName()) + "(");
			}
			Class<?> _class = object.getClass();
			if (_class.equals(String.class)) {
				writer.print("\"" + object + "\"");
			} else if (_class.equals(Long.class) || _class.equals(long.class)) {
				writer.print(Long.toString((Long)object));
				writer.print("L");
			} else if (_class.equals(Short.class) || _class.equals(short.class)) {
				writer.print(Short.toString((Short)object));
			} else if (_class.equals(Integer.class) || _class.equals(int.class)) {
				writer.print(Integer.toString((Integer)object));
			} else if (_class.equals(Byte.class) || _class.equals(byte.class)) {
				writer.print("(byte)" + Byte.toString((Byte)object));
			} else if (_class.equals(Character.class) || _class.equals(char.class)) {
				writer.print("'" + (Character)object + "'");
			} else if (_class.equals(Double.class) || _class.equals(double.class)) {
				writer.print(Double.toString((Double)object) + "D");
			} else if (_class.equals(Float.class) || _class.equals(float.class)) {
				writer.print(Float.toString((Float)object));
			} else if (_class.equals(Boolean.class) || _class.equals(boolean.class)) {
				writer.print(Boolean.toString((Boolean)object));
			} else if (_class.equals(BigDecimal.class)) {
				BigDecimal bigDecimal = (BigDecimal) object;
				writer.print("new " + BigDecimal.class.getName() + "(" + bigDecimal.toPlainString() + ")");
			} else if (_class.equals(BigInteger.class)) {
				BigInteger bigInteger = (BigInteger) object;
				writer.print("new " + BigInteger.class.getName() + "(" + bigInteger.toString() + ")");
			} else if (_class.equals(Date.class)) {
				Date date = (Date)object;
				writer.print("new " + Date.class.getName() + "(" + date.getTime() + "L)");
			} else if (_class.equals(Timestamp.class)) {
				Timestamp date = (Timestamp)object;
				writer.print("new " + Timestamp.class.getName() + "(" + date.getTime() + "L)");
			} /*else if (_class.equals(Calendar.class)) {
				writer.print(_class.getName()".getInstance()");
			}*/ else {
				throw new IllegalArgumentException("Unhandled type " + object.getClass().getName() + " with value '" + object + "'");
			}
			if (fieldBeingProcessed != null) {
				writer.println(")");
			}
		} else {
			
			String variableName = allInstances.get(object);
			if (fieldBeingProcessed == null) {
				writer.println(variableName);
			} else {
				writer.print(".with" + Utils.capitalize(fieldBeingProcessed.getName()) + "(" + variableName);
			}
			if (!processedObjects.contains(object)) {
				processedObjects.add(object);
				writer.indent();
				Collection<Field> fields = ReflectUtils.getFields(object.getClass());
				for (Field field : fields) {
					if (!Modifier.isStatic(field.getModifiers()) && !ignoredFieldsSet.contains(field)) {
						ReflectUtils.makeAccessible(field);
						Object fieldValue = ReflectUtils.getValueUsingReadMethodForField(object, field);
						internalRecursivelyCodifyGraphReferences(fieldValue, writer, allInstances, processedObjects, ignoredFieldsSet, field);
					}
				}
				writer.outdent();
			}
			if (fieldBeingProcessed != null) {
				writer.println(")");
			}
		}
	}

	private static void internalRecursivelyCodifyInstantiationsGraph(Object object,
			IndentPrintWriter indentPrintWriter, Map<Object, String> allInstances, Map<Class<?>, Integer> typeVariableCountMap, Set<Field> ignoredFieldsSet) {
		if (object == null || allInstances.containsKey(object)) {
			return;
		}
		
		if (object instanceof Collection) {
			Collection<?> collection = (Collection<?>) object;
			for (Object subObject : collection) {
				internalRecursivelyCodifyInstantiationsGraph(subObject, indentPrintWriter, allInstances, typeVariableCountMap, ignoredFieldsSet);
			}
		} else if (object instanceof Map) {
			Map<?, ?> map = (Map<?, ?>) object;
			for (Object key : map.keySet()) {
				internalRecursivelyCodifyInstantiationsGraph(key, indentPrintWriter, allInstances, typeVariableCountMap, ignoredFieldsSet);
				internalRecursivelyCodifyInstantiationsGraph(map.get(key), indentPrintWriter, allInstances, typeVariableCountMap, ignoredFieldsSet);
			}
		} else if (object.getClass().isArray()) {
			if (!object.getClass().getComponentType().isPrimitive()) {
				Object[] collection = (Object[]) object;
				for (Object subObject : collection) {
					internalRecursivelyCodifyInstantiationsGraph(subObject, indentPrintWriter, allInstances, typeVariableCountMap, ignoredFieldsSet);
				}
			}
		} else if (object.getClass().getName().startsWith("java.") || object.getClass().getName().startsWith("javax.")) {
			// Ignore for now ??
			// TODO: Handle Calendar here
		} else if (object instanceof Enum) {
			// Ignore for now ??
		} else {
			// Decide on a variable name
			Integer count = typeVariableCountMap.get(object.getClass());
			if (count == null) {
				count = Integer.valueOf(1);
				typeVariableCountMap.put(object.getClass(), count);
			} else {
				count++;
				typeVariableCountMap.put(object.getClass(), count);
			}
			
			String variableName = Utils.uncapitalize(object.getClass().getSimpleName()) + count;
			allInstances.put(object, variableName);
			indentPrintWriter.println(object.getClass().getName() + " " + variableName + " = new " + object.getClass().getName() + "();");
			
			Collection<Field> fields = ReflectUtils.getFields(object.getClass());
			for (Field field : fields) {
				if (!Modifier.isStatic(field.getModifiers()) && !ignoredFieldsSet.contains(field)) {
					ReflectUtils.makeAccessible(field);
					Object fieldValue = ReflectUtils.getValueUsingReadMethodForField(object, field);
					internalRecursivelyCodifyInstantiationsGraph(fieldValue, indentPrintWriter, allInstances, typeVariableCountMap, ignoredFieldsSet);
				}
			}
		}
	}
	
	
}
