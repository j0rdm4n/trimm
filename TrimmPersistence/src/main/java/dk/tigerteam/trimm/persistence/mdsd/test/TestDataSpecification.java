/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.mdsd.test;

import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaClazz;
import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaProperty;

/**
 * Test Data specification. Defines how large a graph should be created during {@link JpaUnitTest} processing.
 * Is used by {@link RuntimeMetaTestDataCreator}. 
 * @author Jeppe Cramon
 */
public class TestDataSpecification {
	/**
	 * Should fill this optional property with test data?
	 * @param metaProperty
	 * @return
	 */
	public boolean createForOptionalProperty(RuntimeMetaProperty metaProperty) {
		return true;
	}
	
	/**
	 * Should we fill this required property with test data?
	 * @param metaProperty
	 * @return
	 */
	public boolean createForRequiredProperty(RuntimeMetaProperty metaProperty) {
		return true;
	}
	
	/**
	 * How many instances should we put into this xToMany property?
	 * @param metaProperty
	 * @return
	 */
	public int allowedInstancesInToManyProperty(RuntimeMetaProperty metaProperty) {
		return 1;
	}

	/**
	 * How many concurrent instances should we allow of the given class (set as low as possible
	 * to allow for reuse of clazz instances as much as possible). 
	 * @param metaClazz
	 * @return
	 */
	public int allowedInstancesOfClazz(RuntimeMetaClazz metaClazz) {
		return 1;
	}
	
	/**
	 * Should we create a minimal or maximal object graph.
	 * @param metaClazz
	 * @return
	 */
	public boolean shouldCreateLargestPossibleGraph(RuntimeMetaClazz metaClazz) {
		return false;
	}
}
