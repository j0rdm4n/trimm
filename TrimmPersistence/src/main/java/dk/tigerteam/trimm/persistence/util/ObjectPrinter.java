/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.util;

import dk.tigerteam.trimm.util.IndentPrintWriter;

import java.util.Set;

/**
 * The Object Printer is used in combination with {@link ObjectGraphUtil#printGraphRecursively(Object, java.io.Writer, ObjectPrinter...)} to
 * print out an object graph
 * @author Jeppe Cramon
 */
public interface ObjectPrinter {
    /**
     * Is the supplied object supported by this ObjectPrinter?
     * @param object the supplied object
     * @return true if it's supported
     */
	boolean support(Object object);

    /**
     * Print the object to the printWriter
     * @param object The object to print
     * @param printedObjects Set of Objects that have already been printed. This can be used so you
     * only print out the details of an object once and for the rest of the cases e.g. just print the
     * <code>&lt;instance-class-name>{@literal@}&lt;instance-hashcode></code> - <i>Note: The hashcode can be calculated
     * by System.identityHashCode()</i>
     * @param printWriter The indenting print writer that the object must be written using
     */
	void print(Object object, Set<Object> printedObjects, IndentPrintWriter printWriter);
}
