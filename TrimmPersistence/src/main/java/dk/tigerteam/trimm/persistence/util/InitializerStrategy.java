/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.util;

import dk.tigerteam.trimm.mdsd.runtime.RuntimeMeta;
import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaClazz;
import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaProperty;

import java.util.*;

/**
 * Strategy interface which allows for variation of how data in a entity is prefetched 
 * @author Jeppe Cramon
 */
public interface InitializerStrategy {
	/**
	 * Initialize the entity
	 * @param <T> The type of entity
	 * @param entity The entity instance
	 * @param initializationInfo The info about the different associations and hwich we want to initialize
	 * @return The initialized entity - It's advisable to use the return value instead of the method parameter, as some {@link InitializerStrategy}'s might resort to
	 * for instance prefetch queries
	 */
	<T extends RuntimeMeta> T initializeEntity(T entity, EntityInitializationInfo initializationInfo);



  /**
   * Info about all the possible association initializations for an Entity
   * @author Jeppe Cramon
   */
  public static class EntityInitializationInfo {
    public enum Initialize { All, AllToOne, AllToMany, OneToOne, ManyToOne, OneToMany, ManyToMany }
    private List<Initialize> initializations;
    private Set<RuntimeMetaProperty> oneToOneAssociations = new HashSet<RuntimeMetaProperty>();
    private Set<RuntimeMetaProperty> manyToOneAssociations = new HashSet<RuntimeMetaProperty>();
    private Set<RuntimeMetaProperty> oneToManyAssociations = new HashSet<RuntimeMetaProperty>();
    private Set<RuntimeMetaProperty> manyToManyAssociations = new HashSet<RuntimeMetaProperty>();

    /**
     * Helper method to populate information about Associations in an entity (only for framework and unit test usage)
     * @param entity The runtime meta representation of a TRIMM generated Java class
     * @param initializations The intitializations
     */
    public static InitializerStrategy.EntityInitializationInfo populateEntityIntializationInfo(RuntimeMeta entity, InitializerStrategy.EntityInitializationInfo.Initialize...initializations) {
      InitializerStrategy.EntityInitializationInfo initializationInfo = new InitializerStrategy.EntityInitializationInfo();
      if (initializations != null) {
        initializationInfo.initializations = Arrays.asList(initializations);
      } else {
        initializationInfo.initializations = new LinkedList<Initialize>();
        initializationInfo.initializations.add(InitializerStrategy.EntityInitializationInfo.Initialize.All);
      }

      RuntimeMetaClazz metaClazz = entity.getMetaType();
      for (RuntimeMetaProperty metaProperty : metaClazz.getAllPropertiesIncludingSuperClazzProperties()) {
        if (metaProperty.isPartInAnAssociation() && metaProperty.isHasCorrespondingJavaProperty()) {
          switch (metaProperty.getAssociationType()) {
            case OneToOne:
              initializationInfo.oneToOneAssociations.add(metaProperty);
              break;
            case ManyToOne:
              initializationInfo.manyToOneAssociations.add(metaProperty);
              break;
            case OneToMany:
              initializationInfo.oneToManyAssociations.add(metaProperty);
              break;
            case ManyToMany:
              initializationInfo.manyToManyAssociations.add(metaProperty);
              break;
          }
        }
      }
      return initializationInfo;
    }


    public List<Initialize> getInitializations() {
      return initializations;
    }

    public void setInitializations(List<Initialize> initializations) {
      this.initializations = initializations;
    }

    public Set<RuntimeMetaProperty> getOneToOneAssociations() {
      return oneToOneAssociations;
    }

    public void setOneToOneAssociations(
        Set<RuntimeMetaProperty> oneToOneAssociations) {
      this.oneToOneAssociations = oneToOneAssociations;
    }

    public Set<RuntimeMetaProperty> getManyToOneAssociations() {
      return manyToOneAssociations;
    }

    public void setManyToOneAssociations(
        Set<RuntimeMetaProperty> manyToOneAssociations) {
      this.manyToOneAssociations = manyToOneAssociations;
    }

    public Set<RuntimeMetaProperty> getOneToManyAssociations() {
      return oneToManyAssociations;
    }

    public void setOneToManyAssociations(
        Set<RuntimeMetaProperty> oneToManyAssociations) {
      this.oneToManyAssociations = oneToManyAssociations;
    }

    public Set<RuntimeMetaProperty> getManyToManyAssociations() {
      return manyToManyAssociations;
    }

    public void setManyToManyAssociations(
        Set<RuntimeMetaProperty> manyToManyAssociations) {
      this.manyToManyAssociations = manyToManyAssociations;
    }

    /**
     * Test if we should initialize OneToOne, OneToMany, ManyToOne or ManyToMany based on the values in the
     * {@link #getInitializations()}
     * @param initialize The initialize value (OneToOne, OneToMany, ManyToOne or ManyToMany) to test against the {@link #getInitializations()} values
     * @return true if the given association type should be initialized, otherwise false
     */
    public boolean shouldInitialize(Initialize initialize) {
      switch (initialize) {
        case OneToOne:
          return initializations.contains(Initialize.All) || initializations.contains(Initialize.AllToOne)
              || initializations.contains(Initialize.OneToOne);
        case ManyToOne:
          return initializations.contains(Initialize.All) || initializations.contains(Initialize.AllToOne)
              || initializations.contains(Initialize.ManyToOne);
        case OneToMany:
          return initializations.contains(Initialize.All) || initializations.contains(Initialize.AllToMany)
              || initializations.contains(Initialize.OneToMany);
        case ManyToMany:
          return initializations.contains(Initialize.All) || initializations.contains(Initialize.AllToMany)
              || initializations.contains(Initialize.ManyToMany);
        default:
          throw new IllegalStateException("Unhandled Initialize value '" + initialize + "'");
      }
    }
  }
}
