/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.mdsd.test;

import dk.tigerteam.trimm.persistence.util.ObjectPrinter;
import dk.tigerteam.trimm.persistence.util.ProxyHandler;

import javax.persistence.EntityManager;
import java.lang.reflect.Field;

/**
 * Base test class for generated Test cases.
 * If you let your Generated JPA unit test class (generated using <code>JpaUnittestCreator</code>) inherit from this class
 * you also need to generate a {@link dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaModel} using the
 * <code>RuntimeMetaDataCreator</code>
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public abstract class AbstractModelTest implements JpaUnitTest {

	public RuntimeMetaTestDataCreator getDataCreator() {
		RuntimeMetaTestDataCreator runtimeMetaTestDataCreator = lazyCreateDataCreator();
		return runtimeMetaTestDataCreator;
	}

	protected RuntimeMetaTestDataCreator lazyCreateDataCreator() {
		return new RuntimeMetaTestDataCreator();
	}

	public TestDataSpecification getTestDataSpecification(Class<?> classToTest) {
		return new TestDataSpecification();
	}

  public abstract ProxyHandler getProxyHandler(EntityManager entityManager);

    /**
     * Informs the test comparer about which {@link Field}'s to ignore during graph comparison
     * Example: <pre>
     * {@literal @Override}
     * public Field[] getIgnoreFieldsDuringGraphCompare(Class&lt;?> classBeingCompared) {
     *     return new Field[] {
     *         dk.tigerteam.foundation.util.ReflectUtils.getField(
     *             dk.tigerteam.mddexample.model.AbstractEntity.class,
     *             "created"
     *         ),
     *         dk.tigerteam.foundation.util.ReflectUtils.getField(
     *            dk.tigerteam.mddexample.model.AbstractEntity.class,
     *            "lastUpdated"
     *         )
     *     };
     * }
     * </pre>
     * @param classBeingCompared The class that's being compared and who may have fields that should be ignored during the comparison
     * @return the fields or an <b>empty</b> Field[] array in case no fields should be ignored
     */
    public abstract Field[] getIgnoreFieldsDuringGraphCompare(Class<?> classBeingCompared);

    /**
     * Specify extra ObjectPrinters which allows Test errors to print a nice object graph.
     * Example which registers Object Printers for Joda Times LocalDate and DateTime:
     * <pre>
     * public abstract class BaseJpaModelTest extends AbstractModelTest {

           private static ObjectPrinter[] objectPrinters;

           // Static initialization of ObjectPrinters
           {
                      objectPrinters = new ObjectPrinter[] { new ObjectPrinter() {
                                 {@literal @Override}
                                 public boolean support(Object object) {
                                            return object instanceof LocalDate;
                                 }

                                 {@literal @Override}
                                 public void print(Object object, Set&lt;Object> printedObjects, IndentPrintWriter printWriter) {
                                            printWriter.print(((LocalDate) object).toString());
                                 }
                      }, new ObjectPrinter() {

                                 {@literal @Override}
                                 public boolean support(Object object) {
                                            return object instanceof DateTime;
                                 }

                                 {@literal @Override}
                                 public void print(Object object, Set&lt;Object> printedObjects, IndentPrintWriter printWriter) {
                                            printWriter.print(((DateTime) object).toString());
                                 }
                      } };
           }



           {@literal @Override}
           public ObjectPrinter[] getObjectPrinters() {
                      return objectPrinters;
           }

           // Other test methods
            …
            …
            …

      }
     * </pre>
     *
     * @return The ObjectPrinters or <code>null</code>
     */
    public abstract ObjectPrinter[] getObjectPrinters();
}
