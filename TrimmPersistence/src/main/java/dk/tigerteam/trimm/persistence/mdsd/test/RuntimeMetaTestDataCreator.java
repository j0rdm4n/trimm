/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package dk.tigerteam.trimm.persistence.mdsd.test;

import dk.tigerteam.trimm.mdsd.BaseClazz;
import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaClazz;
import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaEnumeration;
import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaModel;
import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaProperty;
import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaProperty.CardinalityType;
import dk.tigerteam.trimm.util.ReflectUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * {@link TestDataCreator} which uses the {@link RuntimeMetaModel} information stored in each class to create a test data graph
 *  
 * @author Jeppe Cramon
 */
public class RuntimeMetaTestDataCreator implements TestDataCreator {
	
	private static final Log logger = LogFactory.getLog(RuntimeMetaTestDataCreator.class);
	private ConcurrentMap<Class<?>, RuntimeMetaClazz> classToRuntimeMetaClazzCache = new ConcurrentHashMap<Class<?>, RuntimeMetaClazz>();
	private Random random = new Random();
	
	@SuppressWarnings("unchecked")
	public <T> T createTestData(Class<T> tClass,
			TestDataSpecification testDataSpecification) {
		RuntimeMetaClazz metaClazz = getMetaClazzFromClass(tClass);
		return (T) createAndInitializeClass(metaClazz, testDataSpecification, new ObjectInstancesStack(), null); 
	}
	
	@SuppressWarnings("unchecked")
	protected Object createAndInitializeClass(RuntimeMetaClazz metaClazz, TestDataSpecification testDataSpecification, 
			                                  ObjectInstancesStack objectInstancesStack, RuntimeMetaProperty metaPropertyToFill) {
		Object instance = null;
		if (metaClazz.isBuiltInType() || metaClazz instanceof RuntimeMetaEnumeration) {
			trace(objectInstancesStack, "Create instance ", metaClazz);
			instance = newInstance(metaClazz, objectInstancesStack, metaPropertyToFill);
			return instance; 
		}
		
		int activeInstancesOfClazz = objectInstancesStack.getActiveInstancesOfClass(metaClazz.getJavaClass(), metaPropertyToFill);
		if (testDataSpecification.shouldCreateLargestPossibleGraph(metaClazz) && activeInstancesOfClazz >= testDataSpecification.allowedInstancesOfClazz(metaClazz)) {
			// Use of from the objectInstancesStack (if possible)
			instance = objectInstancesStack.getFromActiveInstances(metaClazz.getJavaClass(), metaPropertyToFill);
			if (metaPropertyToFill.isPartInAnAssociation() && metaPropertyToFill.getAssociation().isBidirectional() && metaPropertyToFill.getCardinalityType() != CardinalityType.Many) {
				// Check if the opposite property of this object already is filled, if so, then we can't reuse it
				RuntimeMetaProperty oppositeProperty = metaPropertyToFill.getAssociation().getOppositeProperty(metaPropertyToFill);
				boolean isInitialized = ReflectUtils.invokeMethod(oppositeProperty.getPropertyDescriptor().getReadMethod(), instance, new Object[0]) != null;
				if (isInitialized) {
					instance = null;
				}
			} else {
				trace(objectInstancesStack, "Reusing instance ", metaClazz);
				return instance;
			}
		} else if (!testDataSpecification.shouldCreateLargestPossibleGraph(metaClazz) && activeInstancesOfClazz > 0) {
			// Use of from the objectInstancesStack (if possible)
			instance = objectInstancesStack.getFromActiveInstances(metaClazz.getJavaClass(), metaPropertyToFill);
			if (metaPropertyToFill.isPartInAnAssociation() && metaPropertyToFill.getAssociation().isBidirectional() && metaPropertyToFill.getCardinalityType() != CardinalityType.Many) {
				// Check if the opposite property of this object already is filled, if so, then we can't reuse it
				RuntimeMetaProperty oppositeProperty = metaPropertyToFill.getAssociation().getOppositeProperty(metaPropertyToFill);
				boolean isInitialized = ReflectUtils.invokeMethod(oppositeProperty.getPropertyDescriptor().getReadMethod(), instance, new Object[0]) != null;
				if (isInitialized) {
					instance = null;
				} else {
					trace(objectInstancesStack, "Reusing instance ", metaClazz);
					return instance;
				}
			} else {
				trace(objectInstancesStack, "Reusing instance ", metaClazz);
				return instance;
			}
		}
			
		if (instance == null) {
			trace(objectInstancesStack, "Create instance ", metaClazz);
			instance = newInstance(metaClazz, objectInstancesStack, metaPropertyToFill);
			objectInstancesStack.push(instance);
		}
		
		// Correct the MetaClazz in case we chose a subclass of it
		metaClazz = getMetaClazzFromClass(instance.getClass());
		
		boolean metaClazzIsEmbeddable = metaClazz.getJavaClass().getAnnotation(Embeddable.class) != null;
		boolean filledAPropertyForMetaClazz = false;
		List<RuntimeMetaProperty> allPropertiesIncludingSuperClazzProperties = metaClazz.getAllPropertiesIncludingSuperClazzProperties();
		for (RuntimeMetaProperty metaProperty : allPropertiesIncludingSuperClazzProperties) {
			if (!metaProperty.isHasCorrespondingJavaProperty()) {
				continue;
			}
			
			boolean shouldCreate = true;
			if (!metaClazzIsEmbeddable && metaProperty.getMinCardinality() == 0 && !metaProperty.getJavaField().isAnnotationPresent(NotNull.class)) {
				shouldCreate = testDataSpecification.createForOptionalProperty(metaProperty);
				if (shouldCreate && !testDataSpecification.shouldCreateLargestPossibleGraph(metaClazz)) {
					if (instance != objectInstancesStack.getRootObject()) {
						shouldCreate = false;
					} 
				}
			} else {
				shouldCreate = testDataSpecification.createForRequiredProperty(metaProperty);
			}
			
			
			if (shouldCreate) {
				if (metaProperty.isPartInAnAssociation()) {
					RuntimeMetaProperty oppositeMetaProperty = metaProperty.getAssociation().getOppositeProperty(metaProperty);
					if (oppositeMetaProperty.equals(metaPropertyToFill)) {
						// Ignore, because this property is just the opposite end of the same association for which we're currently generating
						continue;
					}
				}
				
				trace(objectInstancesStack, "Handling Property ", metaProperty.getOwnerClazz().getName(), "#", metaProperty);
				
				if (metaProperty.getCardinalityType() == CardinalityType.Many) {
					int allowedInstancesInToManyProperty = testDataSpecification.allowedInstancesInToManyProperty(metaProperty);
					if (allowedInstancesInToManyProperty <  metaProperty.getMinCardinality()) {
						allowedInstancesInToManyProperty = metaProperty.getMinCardinality();
					}
					Collection propertyCollection = (Collection) ReflectUtils.invokeMethod(metaProperty.getPropertyDescriptor().getReadMethod(), instance);
					boolean isNotInitialized = propertyCollection.size() == 0;
					boolean isSelfReferencing = metaProperty.equals(metaPropertyToFill);
					if (isNotInitialized && !isSelfReferencing) {
						for (int i = 0; i < allowedInstancesInToManyProperty; i++) {
							filledAPropertyForMetaClazz = true;
							Object propertyValue = createAndInitializeClass(metaProperty.getType(), 
																			testDataSpecification, objectInstancesStack, metaProperty);
							propertyCollection.add(propertyValue);
						}
					}
				} else {
					boolean isNotInitialized = ReflectUtils.invokeMethod(metaProperty.getPropertyDescriptor().getReadMethod(), instance, new Object[0]) == null;
					boolean isSelfReferencing = metaProperty.equals(metaPropertyToFill);
					if (isNotInitialized && !isSelfReferencing) {
						filledAPropertyForMetaClazz = true;
						Object propertyValue = createAndInitializeClass(metaProperty.getType(), 
																		testDataSpecification, objectInstancesStack, metaProperty);
						ReflectUtils.invokeMethod(metaProperty.getPropertyDescriptor().getWriteMethod(), instance, propertyValue);
					}
				}
			}
		}
		if (metaClazzIsEmbeddable && !filledAPropertyForMetaClazz) {
			// This is no good, we have to fill a Property of Metatype embeddable, but the type itself didn't require any properties set
			// To let the test pass, we need to persist at least a single property (otherwise Hibernate will return a null property
			// when the object is fetched again)
			
			// Lets select a random one
			while (!filledAPropertyForMetaClazz) {
				RuntimeMetaProperty metaProperty = allPropertiesIncludingSuperClazzProperties.get(random.nextInt(allPropertiesIncludingSuperClazzProperties.size()));
				if (!metaProperty.isHasCorrespondingJavaProperty()) {
					continue;
				}
				if (metaProperty.getCardinalityType() == CardinalityType.Many) {
					int allowedInstancesInToManyProperty = testDataSpecification.allowedInstancesInToManyProperty(metaProperty);
					if (allowedInstancesInToManyProperty <  metaProperty.getMinCardinality()) {
						allowedInstancesInToManyProperty = metaProperty.getMinCardinality();
					}
					Collection propertyCollection = (Collection) ReflectUtils.invokeMethod(metaProperty.getPropertyDescriptor().getReadMethod(), instance);
					boolean isNotInitialized = propertyCollection.size() == 0;
					boolean isSelfReferencing = metaProperty.equals(metaPropertyToFill);
					if (isNotInitialized && !isSelfReferencing) {
						for (int i = 0; i < allowedInstancesInToManyProperty; i++) {
							filledAPropertyForMetaClazz = true;
							Object propertyValue = createAndInitializeClass(metaProperty.getType(), 
																			testDataSpecification, objectInstancesStack, metaProperty);
							propertyCollection.add(propertyValue);
						}
					}
				} else {
					boolean isNotInitialized = ReflectUtils.invokeMethod(metaProperty.getPropertyDescriptor().getReadMethod(), instance, new Object[0]) == null;
					boolean isSelfReferencing = metaProperty.equals(metaPropertyToFill);
					if (!isNotInitialized && !isSelfReferencing && metaProperty.getJavaField().getType().isPrimitive()) {
						isNotInitialized = true;
					}
					if (isNotInitialized && !isSelfReferencing) {
						filledAPropertyForMetaClazz = true;
						Object propertyValue = createAndInitializeClass(metaProperty.getType(), 
																		testDataSpecification, objectInstancesStack, metaProperty);
						ReflectUtils.invokeMethod(metaProperty.getPropertyDescriptor().getWriteMethod(), instance, propertyValue);
					}
				}
			}
			
		}
//		Object pop = objectInstancesStack.pop();
//		if (pop != instance) {
//			throw new IllegalStateException("The last popped object should be our instance '" + instance + "', but was '" + pop + "'");
//		}
		return instance;
	}
	
	@SuppressWarnings("unchecked")
	private Class<?> pickASubClazz(RuntimeMetaClazz metaClazz) {
		RuntimeMetaClazz[] subClazzes = metaClazz.getAllSubClazzes().toArray(new RuntimeMetaClazz[0]);
		RuntimeMetaClazz selectedMetaClazz = subClazzes[random.nextInt(metaClazz.getSubClazzes().size())];
		int numOfLoops = 0;
		while (selectedMetaClazz.hasStereoType("abstract") || Modifier.isAbstract(selectedMetaClazz.getJavaClass().getModifiers())) {
			selectedMetaClazz = subClazzes[random.nextInt(subClazzes.length)];
			if (selectedMetaClazz.getJavaClass().isAnnotationPresent(BaseClazz.class)) {
				BaseClazz baseClazz = (BaseClazz) selectedMetaClazz.getJavaClass().getAnnotation(BaseClazz.class);
				Class<?> extensionClazz = baseClazz.extensionClazz();
				if (!Modifier.isAbstract(extensionClazz.getModifiers())) {
					return extensionClazz;
				}
			}
			numOfLoops++;
			if (numOfLoops > 10000) {
				throw new IllegalStateException("Caught in endless loop while looking for a subclazzz for " + metaClazz);
			}
		}
		return selectedMetaClazz.getJavaClass();
	}

	protected RuntimeMetaClazz getMetaClazzFromClass(Class<?> clazz) {
		if (classToRuntimeMetaClazzCache.containsKey(clazz)) {
			return classToRuntimeMetaClazzCache.get(clazz);
		} else {
			RuntimeMetaClazz metaClazz = (RuntimeMetaClazz) ReflectUtils.getFieldValue(ReflectUtils.getField(clazz, "metaType"), null);
			classToRuntimeMetaClazzCache.put(clazz, metaClazz);
			return metaClazz;
		}
	}

	protected Object newInstance(RuntimeMetaClazz metaClazz, ObjectInstancesStack objectInstancesStack, RuntimeMetaProperty metaProperty) {
		if (metaClazz instanceof RuntimeMetaEnumeration) {
			return chooseEnumValue((RuntimeMetaEnumeration)metaClazz);
		} else if (metaClazz.isBuiltInType()) {
			Class<?> _class = metaClazz.getJavaClass();
			if (_class.equals(String.class)) {
				return RandomStringUtils.randomAlphabetic(metaProperty.getUpperBound() != null ? metaProperty.getUpperBound().intValue() : 25);
			} else if (_class.equals(Long.class) || _class.equals(long.class)) {
				return random.nextLong(); 
			} else if (_class.equals(Short.class) || _class.equals(short.class)) {
				return (short)random.nextInt();
			} else if (_class.equals(Integer.class) || _class.equals(int.class)) {
				return random.nextInt();
			} else if (_class.equals(Byte.class) || _class.equals(byte.class)) {
				return (byte)random.nextInt(127);
			} else if (_class.equals(Character.class) || _class.equals(char.class)) {
				return RandomStringUtils.randomAlphabetic(25).charAt(random.nextInt(25));
			} else if (_class.equals(Double.class) || _class.equals(double.class)) {
				return random.nextDouble();
			} else if (_class.equals(Float.class) || _class.equals(float.class)) {
				return random.nextFloat();
			} else if (_class.equals(Boolean.class) || _class.equals(boolean.class)) {
				return random.nextBoolean();
			} else if (_class.equals(BigDecimal.class)) {
				return new BigDecimal(Integer.toString(random.nextInt()));
			} else if (_class.equals(BigInteger.class)) {
				return new BigInteger(Integer.toString(random.nextInt()));
			} else if (_class.equals(Date.class)) {
				return new Date();
			} else if (_class.equals(Timestamp.class)) {
				return new Timestamp(System.currentTimeMillis());
			} else if (_class.equals(Calendar.class)) {
				return Calendar.getInstance();
			} else if (_class.isArray()) {
				Class<?>  arrayType = _class.getComponentType();
				if (arrayType.equals(byte.class) || arrayType.equals(Byte.class) ) {
					return RandomStringUtils.randomAlphabetic(25).getBytes();
				} else if (arrayType.equals(char.class) || arrayType.equals(Character.class) ) {
					return RandomStringUtils.randomAlphabetic(25).toCharArray();
				} else if (arrayType.equals(int.class)) {
					int[] result = new int[25];
					for (int i = 0; i < result.length; i++) {
						result[i] = random.nextInt();
					}
					return result;
				} else if (arrayType.equals(long.class)) {
					long[] result = new long[25];
					for (int i = 0; i < result.length; i++) {
						result[i] = (long)random.nextInt();
					}
					return result;
				} else if (arrayType.equals(short.class)) {
					short[] result = new short[25];
					for (int i = 0; i < result.length; i++) {
						result[i] = (short)random.nextInt();
					}
					return result;
				} else if (arrayType.equals(double.class)) {
					double[] result = new double[25];
					for (int i = 0; i < result.length; i++) {
						result[i] = random.nextDouble();
					}
					return result;
				} else if (arrayType.equals(float.class)) {
					float[] result = new float[25];
					for (int i = 0; i < result.length; i++) {
						result[i] = random.nextFloat();
					}
					return result;
				} else if (arrayType.equals(String.class)) {
					String[] result = new String[25];
					for (int i = 0; i < result.length; i++) {
						result[i] = RandomStringUtils.randomAlphabetic(metaProperty.getUpperBound() != null ? metaProperty.getUpperBound().intValue() : 25);
					}
					return result;
				} else {
					throw new IllegalArgumentException("Can't handle arrays of type '" +  _class + "'");
				}
			} else {
				throw new IllegalArgumentException("Can't handle properties of type '" +  _class + "'");
			}
		} else {
			Class<?> instanceClass = metaClazz.getJavaClass();
			if (metaClazz.getSubClazzes().size() > 0) {
				instanceClass = pickASubClazz(metaClazz);
			} else if (Modifier.isAbstract(instanceClass.getModifiers())) {
				BaseClazz baseClazz = instanceClass.getAnnotation(BaseClazz.class);
				if (baseClazz != null) {
					instanceClass = baseClazz.extensionClazz();
				} else {
					throw new TestDataCreationException("Can't create instance of Abstract " + metaClazz.getJavaClass());
				}
			}
			try {
				Object newInstance = instanceClass.newInstance();
				return newInstance;
			} catch (Exception e) {
				throw new TestDataCreationException("Failed to create a new instance of " + instanceClass, e);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	protected Object chooseEnumValue(RuntimeMetaEnumeration metaEnumeration) {
		List<RuntimeMetaProperty> enumValues = metaEnumeration.getEnumValues();
		String enumName = enumValues.get(random.nextInt(enumValues.size())).getName();
		return Enum.valueOf(metaEnumeration.getJavaClass(), enumName);
	}

	public static class ObjectInstancesStack {
		private Stack<Object> objectInstances = new Stack<Object>();
		
		public ObjectInstancesStack push(Object objectInstance) {
			objectInstances.push(objectInstance);
			return this;
		}
		
		public Object getRootObject() {
			return objectInstances.get(0);
		}

        public Object getFromActiveInstances(Class<?> clazz,
				RuntimeMetaProperty metaPropertyToFill) {
			if (metaPropertyToFill != null && metaPropertyToFill.getAggregation() == RuntimeMetaProperty.Aggregation.Shared) {
				for (Object instance : objectInstances) {
					if (clazz.isAssignableFrom(instance.getClass())) {
							return instance;
					}
				}
			}
			return null;
		}

		public int getActiveInstancesOfClass(Class<?> clazz, RuntimeMetaProperty metaPropertyToFill) {
			int count = 0;
			if (metaPropertyToFill != null && metaPropertyToFill.getAggregation() == RuntimeMetaProperty.Aggregation.Shared) {
				for (Object instance : objectInstances) {
					if (clazz.isAssignableFrom(instance.getClass())) {
						count++;
					}
				}
			}
			return count;
		}

		public Object pop() {
			return objectInstances.pop();
		}
		
		public Object peek() {
			return objectInstances.peek();
		}

		public int size() {
			return objectInstances.size();
		}
	}
	
	protected void trace(ObjectInstancesStack objectInstancesStack, Object...messages) {
		if (logger.isTraceEnabled() && messages != null) {
			StringBuilder stringBuilder = new StringBuilder();
			for (Object message : messages) {
				stringBuilder.append(message);
			}
			logger.trace(stringBuilder.toString());
		}
	}
}
