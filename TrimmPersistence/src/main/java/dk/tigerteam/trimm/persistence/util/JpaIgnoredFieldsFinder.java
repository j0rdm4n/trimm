package dk.tigerteam.trimm.persistence.util;

import dk.tigerteam.trimm.util.ReflectUtils;

import javax.persistence.Transient;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Is typically used by JPA model integration tests to ignore fields that cannot be persisted (e.g. transient fields)
 */
public class JpaIgnoredFieldsFinder {
    public static List<Field> findIgnoredFields(Class<?> jpaClass) {
        return ReflectUtils.getFieldsWithAnnotations(jpaClass, Transient.class);
    }
}
