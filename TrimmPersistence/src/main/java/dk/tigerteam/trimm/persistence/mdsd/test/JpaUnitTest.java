/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.mdsd.test;

import javax.persistence.EntityManager;

/**
 * Common interface for Running JPA Units driven by {@link TestDataCreator} 
 * @author Jeppe Cramon
 * @see RuntimeMetaTestDataCreator
 */
public interface JpaUnitTest {
	/**
	 * Get the data specification for the class we're going to test
	 * @param classToTest The class we're going to test
	 * @return The test data specification which defined how the graph surrounding classToTest should look like
	 */
	TestDataSpecification getTestDataSpecification(Class<?> classToTest);
	/**
	 * Get the Test data creator
	 */
	TestDataCreator getDataCreator();
	/**
	 * Get the entity manager for this test round 
	 */
	EntityManager getEntityManager();
}
