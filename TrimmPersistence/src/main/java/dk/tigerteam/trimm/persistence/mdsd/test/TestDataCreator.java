/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.mdsd.test;

/**
 * Test Data Creator factory interface. 
 * @author Jeppe Cramon
 */
public interface TestDataCreator {
	/**
	 * Create a test data object graph for tClass according to the specification in testDataSpecification
	 * @param <T> The type of test class
	 * @param tClass The class to create an object graph for
	 * @param testDataSpecification The specification for the test class object graph
	 * @return The test data object graph
	 */
	<T> T createTestData(Class<T> tClass, TestDataSpecification testDataSpecification);
	
}
