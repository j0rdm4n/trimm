/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.eclipselink.util;

import dk.tigerteam.trimm.persistence.util.ProxyHandler;
import org.eclipse.persistence.indirection.IndirectList;
import org.eclipse.persistence.indirection.IndirectMap;
import org.eclipse.persistence.indirection.IndirectSet;
import org.eclipse.persistence.indirection.ValueHolder;
import org.eclipse.persistence.internal.indirection.UnitOfWorkQueryValueHolder;
import org.eclipse.persistence.internal.indirection.UnitOfWorkValueHolder;
import org.eclipse.persistence.jpa.JpaEntityManager;

import javax.persistence.EntityManager;

public class EclipselinkProxyHandler implements ProxyHandler {

	private EntityManager entityManager;
	
	public EclipselinkProxyHandler(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	protected JpaEntityManager unwrap() {
		return entityManager.unwrap(JpaEntityManager.class);
	}
	
	@Override
	public boolean isProxy(Object in) {
		// Not sure this works in all cases
		return in instanceof IndirectList || in instanceof IndirectSet || in instanceof IndirectMap ||
				in instanceof ValueHolder || in instanceof UnitOfWorkValueHolder || in instanceof UnitOfWorkQueryValueHolder;
	}

	@SuppressWarnings("unchecked")
	public Object unproxyAndReassociateIfNecessary(Object in) {
		if(isProxy(in)) {
			if(in instanceof ValueHolder) {
//				if(!((ValueHolder)in).isInstantiated()) {
					return ((ValueHolder) in).getValue() != null ? ((ValueHolder) in).getValue() : in;
//				}
			} else if(in instanceof UnitOfWorkValueHolder) {
//				if(!((UnitOfWorkValueHolder) in).isInstantiated()) {
					return ((UnitOfWorkValueHolder) in).getValue() != null ? ((UnitOfWorkValueHolder) in).getValue() : in;
//				}
			} else if(in instanceof UnitOfWorkQueryValueHolder) {
//				if(!((UnitOfWorkQueryValueHolder) in).isInstantiated()) {
					return ((UnitOfWorkQueryValueHolder) in).getValue() != null ? ((UnitOfWorkQueryValueHolder) in).getValue() : in;
//				}
		 }	else if(in instanceof IndirectList) {
				if(!((IndirectList) in).isInstantiated()) {
					return ((IndirectList) in).getDelegateObject();
				}
			} else if(in instanceof IndirectSet) {
				if(!((IndirectSet) in).isInstantiated()) {
					return ((IndirectSet) in).getDelegateObject();
				}
			} else if(in instanceof IndirectMap) {
				if(!((IndirectMap) in).isInstantiated()) {
					return ((IndirectMap) in).getDelegateObject();
				}
			}
		}
		return in;
	}

	@SuppressWarnings("unchecked")
	public Object unproxyIfNecessary(Object in) {
		return unproxyAndReassociateIfNecessary(in);
	}

	@Override
	public Class<?> getClassWithoutInitializingProxy(Object in) {
		throw new UnsupportedOperationException("This is not supported by Eclipselink handling...");
	}

}
