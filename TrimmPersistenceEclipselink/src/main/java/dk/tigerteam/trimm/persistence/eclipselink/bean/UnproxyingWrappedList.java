/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.persistence.eclipselink.bean;

import dk.tigerteam.trimm.collection.WrappedList;
import dk.tigerteam.trimm.persistence.eclipselink.util.EclipselinkUtil;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class UnproxyingWrappedList<T> extends WrappedList<T> {

  private static final long serialVersionUID = 7368898300646710937L;

  public UnproxyingWrappedList(List<T> wrappedList) {
    super(wrappedList);
  }
  
  private final class WrappedListIterator implements ListIterator<T> {
    
    public WrappedListIterator(ListIterator<T> wrapped) {
      this.wrapped = wrapped;
    }
    
    ListIterator<T> wrapped;

    public void add(T o) {
      wrapped.add(o);
    }

    public boolean hasNext() {
      return wrapped.hasNext();
    }

    public boolean hasPrevious() {
      return wrapped.hasPrevious();
    }

    public T next() {
      return unproxyAndReplace(wrapped.next());
    }

    public int nextIndex() {
      return wrapped.nextIndex();
    }

    public T previous() {
      return unproxyAndReplace(wrapped.previous());
    }

    public int previousIndex() {
      return wrapped.previousIndex();
    }

    public void remove() {
      wrapped.remove();
    }

    public void set(T o) {
      wrapped.set(o);
    }
    
  }

  @Override
  public T get(int index) {
    T temp = getWrappedList().get(index);
    if (EclipselinkUtil.isProxy(temp)) {
      temp = unproxyAndReplace(index, temp);
    }
    return temp;
  }
  
  @Override
  public Iterator<T> iterator() {
    return listIterator();
  }
  
  @Override
  public ListIterator<T> listIterator() {
    return new WrappedListIterator(getWrappedList().listIterator());
  }

  @Override
  public ListIterator<T> listIterator(int index) {
    return new WrappedListIterator(getWrappedList().listIterator(index));
  }
  
  @SuppressWarnings("unchecked")
  protected T unproxyAndReplace(int pos, T obj) {
    if (!EclipselinkUtil.isProxy(obj)) { 
      return obj;
    }
    set(pos, obj = (T) EclipselinkUtil.unproxyAndReassociateIfNecessary(obj));
    return obj;
  }

  @SuppressWarnings("unchecked")
  protected T unproxyAndReplace(T obj) {
    if (!EclipselinkUtil.isProxy(obj)) {
      return obj;
    }
    set(indexOf(obj), obj = (T) EclipselinkUtil.unproxyAndReassociateIfNecessary(obj));
    return obj;
  }

  
}
