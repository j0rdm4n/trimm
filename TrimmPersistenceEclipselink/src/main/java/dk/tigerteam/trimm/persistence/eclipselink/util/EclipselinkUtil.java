/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.persistence.eclipselink.util;

import org.eclipse.persistence.indirection.ValueHolder;
import org.eclipse.persistence.internal.indirection.UnitOfWorkQueryValueHolder;
import org.eclipse.persistence.internal.indirection.UnitOfWorkValueHolder;

public class EclipselinkUtil {

	public static Object unproxyAndReassociateIfNecessary(Object in) {
		if (isProxy(in)) {
			try {
				if(in instanceof ValueHolder) {
//				if(!((ValueHolder)in).isInstantiated()) {
					return ((ValueHolder) in).getValue() != null ? ((ValueHolder) in).getValue() : in;
//				}
				} else if(in instanceof UnitOfWorkValueHolder) {
//				if(!((UnitOfWorkValueHolder) in).isInstantiated()) {
					return ((UnitOfWorkValueHolder) in).getValue() != null ? ((UnitOfWorkValueHolder) in).getValue() : in;
//				}
				} else if(in instanceof UnitOfWorkQueryValueHolder) {
//				if(!((UnitOfWorkQueryValueHolder) in).isInstantiated()) {
					return ((UnitOfWorkQueryValueHolder) in).getValue() != null ? ((UnitOfWorkQueryValueHolder) in).getValue() : in;
//				}
				}
				return in;
			} catch (Exception e) {
				// Just return the proxy
			}
		}
		return in;
	}


	// TODO: no idea yet if this works
	public static boolean isProxy(Object in) {
		return in instanceof ValueHolder || in instanceof UnitOfWorkValueHolder || in instanceof UnitOfWorkValueHolder;
	}
	
}
