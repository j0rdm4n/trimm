/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.ant;

import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.NameableMetaType;
import dk.tigerteam.trimm.mdsd.uml2.xmi.XmiReader;
import dk.tigerteam.trimm.mdsd.ws.NamespaceFileNameAndLocationResolver;
import dk.tigerteam.trimm.mdsd.ws.SoapHeaderHandler;
import dk.tigerteam.trimm.mdsd.ws.WsGenerationResult;
import dk.tigerteam.trimm.mdsd.ws.WsGenerator;
import dk.tigerteam.trimm.mdsd.ws.generator.*;
import dk.tigerteam.trimm.util.BuildUtils;
import groovy.lang.GroovyClassLoader;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.codehaus.groovy.control.CompilationFailedException;

import java.io.File;
import java.io.IOException;

public class TrimmWSAntTask extends Task {
    public static Character dirSeperator = File.separatorChar;
    private GroovyClassLoader groovyClassLoader;

    private String modelFilePath;
    private String generateToFolder;
    private boolean useAliasIfAvailable;
    private String namespaceAndFileLocationHandling;
    private boolean inlineReferencedSchemas;
    private boolean createWrapperSchema;
    private String wrapperElementNameSuffix;
    private boolean inlineWrapperSchema;
    private String soapHeaderNamespacePrefix = null;
    private String soapHeaderNamespace = null;
    private String soapHeaderLocation = null;
    private String soapHeaderWsdlMessageName = null;
    private String soapHeaderWsdlMessagePart = null;
    private String soapHeaderWsdlMessageElement = null;
    private String baseDir;
    private File outputDir;
    private Project project;

    public void setProject(Project proj) {
        project = proj;
    }

    public void setModelFilePath(String modelFilePath) {
        this.modelFilePath = modelFilePath;
    }

    public void setGenerateToFolder(String generateToFolder) {
        this.generateToFolder = generateToFolder;
    }

    public void setUseAliasIfAvailable(boolean useAliasIfAvailable) {
        this.useAliasIfAvailable = useAliasIfAvailable;
    }

    public void setNamespaceAndFileLocationHandling(String namespaceAndFileLocationHandling) {
        this.namespaceAndFileLocationHandling = namespaceAndFileLocationHandling;
    }

    public void setInlineReferencedSchemas(boolean inlineReferencedSchemas) {
        this.inlineReferencedSchemas = inlineReferencedSchemas;
    }

    public void setCreateWrapperSchema(boolean createWrapperSchema) {
        this.createWrapperSchema = createWrapperSchema;
    }

    public void setWrapperElementNameSuffix(String wrapperElementNameSuffix) {
        this.wrapperElementNameSuffix = wrapperElementNameSuffix;
    }

    public void setInlineWrapperSchema(boolean inlineWrapperSchema) {
        this.inlineWrapperSchema = inlineWrapperSchema;
    }

    public void setSoapHeaderNamespacePrefix(String soapHeaderNamespacePrefix) {
        this.soapHeaderNamespacePrefix = soapHeaderNamespacePrefix;
    }

    public void setSoapHeaderNamespace(String soapHeaderNamespace) {
        this.soapHeaderNamespace = soapHeaderNamespace;
    }

    public void setSoapHeaderLocation(String soapHeaderLocation) {
        this.soapHeaderLocation = soapHeaderLocation;
    }

    public void setSoapHeaderWsdlMessageName(String soapHeaderWsdlMessageName) {
        this.soapHeaderWsdlMessageName = soapHeaderWsdlMessageName;
    }

    public void setSoapHeaderWsdlMessagePart(String soapHeaderWsdlMessagePart) {
        this.soapHeaderWsdlMessagePart = soapHeaderWsdlMessagePart;
    }

    public void setSoapHeaderWsdlMessageElement(String soapHeaderWsdlMessageElement) {
        this.soapHeaderWsdlMessageElement = soapHeaderWsdlMessageElement;
    }

    public void setOutputDir(File outputDir) {
        this.outputDir = outputDir;
    }

    public void execute() {
        try {
            project.log("==== GENERATING WEBSERVICE FILES ====");

            baseDir = project.getBaseDir().getAbsolutePath() + File.separatorChar;
            project.log("BaseDir: '" + baseDir + "'");
            if (outputDir == null) {
                outputDir = new File(baseDir + "generated");
            }
            project.log("Output dir: '" + outputDir.getAbsolutePath() + "'");
            outputDir.mkdirs();

            NameableMetaType.UseAliasIfAvailable = useAliasIfAvailable;
            String fullModelPath = baseDir + modelFilePath;
            String fullGenerateToFolderPath = baseDir + generateToFolder;
            if (shouldGenerate(new File(fullGenerateToFolderPath), new File(fullModelPath), outputDir)) {

                SoapHeaderHandler soapHeaderHandler = new NoHeaderSoapHeaderHandler();
                if (soapHeaderNamespace != null) {
                    soapHeaderHandler = new OneHeaderSoapHeaderHandler(new OneHeaderSoapHeaderHandler.SoapHeaderConfiguration(soapHeaderNamespacePrefix, soapHeaderNamespace,
                            soapHeaderLocation, soapHeaderWsdlMessageName, soapHeaderWsdlMessagePart, soapHeaderWsdlMessageElement));
                }
                project.log("Using SOAP Header Handler " + soapHeaderHandler.getClass().getName());

                NamespaceFileNameAndLocationResolver namespaceFileNameAndLocationResolver;
                if (namespaceAndFileLocationHandling.equalsIgnoreCase("classic")) {
                    project.log("Using 'classic' namespaceAndFileLocationHandling");
                    namespaceFileNameAndLocationResolver = new ClassicNamespaceFileNameAndLocationResolver();
                } else if (namespaceAndFileLocationHandling.equalsIgnoreCase("default")) {
                    project.log("Using 'default' namespaceAndFileLocationHandling");
                    namespaceFileNameAndLocationResolver = new DefaultNamespaceFileNameAndLocationResolver();
                } else {
                    project.log("Loading NamespaceFileNameAndLocationResolver: '" + namespaceAndFileLocationHandling + "'");
                    namespaceFileNameAndLocationResolver = loadExtension(namespaceAndFileLocationHandling);
                }

                project.log("Reading from Model file '" + fullModelPath + "'");
                XmiReader xmiReader = WsGeneratorHelper.selectXmiReader(fullModelPath);
                final MetaModel metaModel = xmiReader.read(new File(fullModelPath));


                WsGenerator wsGenerator = new WsGenerator(inlineReferencedSchemas, createWrapperSchema, inlineWrapperSchema, wrapperElementNameSuffix,
                        new DefaultSimpleTypeResolver(), namespaceFileNameAndLocationResolver,
                        new DefaultSoapFaultHandler(), soapHeaderHandler);
                WsGenerationResult result = wsGenerator.generate(metaModel);
                project.log("Generating result to folder '" + fullGenerateToFolderPath + "'");
                WsGeneratorHelper.writeFiles(fullGenerateToFolderPath, result);
            } else {
                project.log("All resource files are up to date nothing to generate");
            }
        } catch (Exception e) {
            project.log("ERROR: ", e, Project.MSG_ERR);
            throw new RuntimeException("TrimmWSAntTask failed: " + e.getMessage(), e);
        }
    }

    public boolean shouldGenerate(File generateToFolder, File modelFile, File baseOutputDir) {
        if (!BuildUtils.doesOutputDirectoryExist(baseOutputDir.getAbsolutePath())
                || !BuildUtils.doesOutputDirectoryExist(generateToFolder.getAbsolutePath())) {
            project.log("One or more build output directory does not exists this means a clean was done," + " recommending generate proceed.", Project.MSG_DEBUG);
            return true;
        }
        if (BuildUtils.shouldGenerate(baseOutputDir.getAbsolutePath(), modelFile)) {
            project.log("A generate resource file has changed sine last generate," + " recommending generate proceed.", Project.MSG_DEBUG);
            return true;
        }
        return false;
    }

    /**
     * Load an extension <b>instance</b> - adheres to the following rules:
     * <ul>
     * <li>If it ends with .groovy is will be loaded as a Groovy Class by reading it in from the groovy script file using {@link #instantiateGroovyClassFromScriptFile(String)}</li>
     * <li>Otherwise it will be deferred to {@link #instantiateClass(String)}</li>
     * </ul>
     *
     * @param extensionValue Either a path to a .groovy script or the FQCN class name for an extension
     * @return The extension <b>instance</b>
     */
    protected <T> T loadExtension(String extensionValue) {
        T extension = null;
        if (extensionValue.endsWith(".groovy")) {
            extension = instantiateGroovyClassFromScriptFile(extensionValue);
        } else {
            extension = instantiateClass(extensionValue);
        }
        project.log("Loaded extension '" + extensionValue + "'", Project.MSG_DEBUG);

        return extension;
    }


    /**
     * Load a class from its Fully Qualified Class Name (FQCN)
     *
     * @param fqcn the class' Fully Qualified Class Name (FQCN)
     * @param <T>  The type of class
     * @return The class
     */
    protected <T> Class<T> loadClass(String fqcn) {
        fqcn = fqcn.trim();
        try {
            @SuppressWarnings("unchecked")
            Class<T> classLoaded = (Class<T>) getClassLoader().loadClass(fqcn);
            project.log("Loaded class from classpath: '" + fqcn + "'", Project.MSG_DEBUG);
            return classLoaded;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Couldn't find class '" + fqcn + "'", e);
        }
    }

    /**
     * Load a Groovy defined class from an <b>absolute</b> (or relative path which is accessible within the execution path of the generator)
     *
     * @param groovyScriptFilePath The groovy script file path
     * @param <T>                  The type of class to return
     * @return The groovy defined class
     */
    @SuppressWarnings("unchecked")
    protected <T> Class<T> loadGroovyClass(String groovyScriptFilePath) {
        groovyScriptFilePath = groovyScriptFilePath.trim();
        if (groovyClassLoader == null) {
            groovyClassLoader = new GroovyClassLoader(getClassLoader());
        }
        try {
            Class<T> classLoaded = groovyClassLoader.parseClass(new File(groovyScriptFilePath));
            project.log("Loaded Groovy class '" + classLoaded.getName() + "' from Groovy script file '" + groovyScriptFilePath + "'", Project.MSG_DEBUG);
            return classLoaded;
        } catch (CompilationFailedException e) {
            throw new RuntimeException("Failed to compile Groovy file '" + groovyScriptFilePath + "'", e);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read Groovy file '" + groovyScriptFilePath + "'", e);
        }
    }

    /**
     * The classloader to use for loading classes and groovy script defined classes.<br/>
     */
    protected ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }


    /**
     * Instantiate a Groovy defined class from a <b>relative</b> path
     *
     * @param relativeScriptPath The groovy script file path
     * @param <T>                The type of object to return
     * @return The groovy class <b>instance</b>
     */
    protected <T> T instantiateGroovyClassFromScriptFile(String relativeScriptPath) {
        String scriptPath = toAbsolutePath(relativeScriptPath.trim());
        Class<? extends T> classLoaded = null;
        try {
            classLoaded = loadGroovyClass(scriptPath);
            return (T) classLoaded.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Failed to load Groovy class from '" + scriptPath + "' " + e.getMessage(), e);
        }
    }

    /**
     * Instantiate a class from its Fully Qualified Class Name (FQCN)
     *
     * @param fqcn The class' Fully Qualified Class Name (FQCN)
     * @param <T>  The type of object to return
     * @return The class <b>instance</b>
     */
    protected <T> T instantiateClass(String fqcn) {
        Class<? extends T> classLoaded = null;
        try {
            classLoaded = loadClass(fqcn);
            return (T) classLoaded.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Failed to instantiate class from FQCN '" + fqcn + "'."
                    + (classLoaded != null ? " Class loaded: '" + classLoaded.getName() + "'" : ""), e);
        }
    }

    /**
     * Resolve a relative path to an absolute path but preappending the baseDir's absolute path<br/>
     * <b>Note: It is assumed that the path <b>is relative</b> - so if you specify an aboslute path the result will be wrong!
     *
     * @param relativePath The relative path (may start with an optional <code>/</code>)
     * @return the absolute path
     */
    public String toAbsolutePath(String relativePath) {
        String path = baseDir;
        if (!relativePath.startsWith(dirSeperator.toString())) {
            path = path + dirSeperator;
        }
        path = path + relativePath;
        return path;
    }

}
