/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.ws.maven;

import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.NameableMetaType;
import dk.tigerteam.trimm.mdsd.uml2.xmi.XmiReader;
import dk.tigerteam.trimm.mdsd.ws.NamespaceFileNameAndLocationResolver;
import dk.tigerteam.trimm.mdsd.ws.SoapHeaderHandler;
import dk.tigerteam.trimm.mdsd.ws.WsGenerationResult;
import dk.tigerteam.trimm.mdsd.ws.WsGenerator;
import dk.tigerteam.trimm.mdsd.ws.generator.*;
import dk.tigerteam.trimm.mdsd.ws.generator.OneHeaderSoapHeaderHandler.SoapHeaderConfiguration;
import dk.tigerteam.trimm.util.BuildUtils;
import groovy.lang.GroovyClassLoader;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.codehaus.groovy.control.CompilationFailedException;

import java.io.File;
import java.io.IOException;

/**
 * Configures the Maven plugin for generating WSDL/Xmlschemas based on an input UML model which describes a set of WebServices<br/>
 * If <b>soapHeaderNamespace</b> parameter is specified the {@link OneHeaderSoapHeaderHandler} will be used, otherwise the
 * {@link NoHeaderSoapHeaderHandler} will be used.
 * <p/>
 * <pre>
 &lt;plugin>
     &lt;groupId>dk.tigerteam&lt;/groupId>
     &lt;artifactId>TrimmWSMavenPlugin&lt;/artifactId>
     &lt;version>1.0.1&lt;/version>
     &lt;executions>
         &lt;!-- Generate for JAX-WS consumption, with XmlSchemas kept separate and not inlined -->
         &lt;execution>
             &lt;id>generate-for-backend&lt;/id>
             &lt;phase>generate-sources&lt;/phase>
             &lt;goals>
                 &lt;goal>generate&lt;/goal>
             &lt;/goals>
             &lt;configuration>
                 &lt;modelFilePath>model/wsmodel.xml&lt;/modelFilePath>
                 &lt;generateToFolder>target/generated-sources/wsdl&lt;/generateToFolder>
                 &lt;useAliasIfAvailable>true&lt;/useAliasIfAvailable>
                 &lt;namespaceAndFileLocationHandling>default&lt;/namespaceAndFileLocationHandling>
                 &lt;inlineReferencedSchemas>false&lt;/inlineReferencedSchemas>
                 &lt;createWrapperSchema>true&lt;/createWrapperSchema>
                 &lt;inlineWrapperSchema>true&lt;/inlineWrapperSchema>
                 &lt;soapHeaderNamespacePrefix>header&lt;/soapHeaderNamespacePrefix>
                 &lt;soapHeaderNamespace>http://bus.company.dk/iheader&lt;/soapHeaderNamespace>
                 &lt;soapHeaderLocation>header/header.xsd&lt;/soapHeaderLocation>
                 &lt;soapHeaderWsdlMessageName>OurHeaderMessage&lt;/soapHeaderWsdlMessageName>
                 &lt;soapHeaderWsdlMessagePart>iheader&lt;/soapHeaderWsdlMessagePart>
                 &lt;soapHeaderWsdlMessageElement>OurHeader&lt;/soapHeaderWsdlMessageElement>
             &lt;/configuration>
         &lt;/execution>
         &lt;!-- Generate for Oracle Service Bus (OSB) which prefers WSDL with as many XML Schemas as possible inlined -->
         &lt;execution>
             &lt;id>generate-for-osb&lt;/id>
             &lt;phase>generate-sources&lt;/phase>
             &lt;goals>
                 &lt;goal>generate&lt;/goal>
             &lt;/goals>
             &lt;configuration>
                 &lt;modelFilePath>model/wsmodel.xml&lt;/modelFilePath>
                 &lt;generateToFolder>target/generated-sources/wsdl-osb&lt;/generateToFolder>
                 &lt;useAliasIfAvailable>true&lt;/useAliasIfAvailable>
                 &lt;namespaceAndFileLocationHandling>classic&lt;/namespaceAndFileLocationHandling>
                 &lt;inlineReferencedSchemas>true&lt;/inlineReferencedSchemas>
                 &lt;createWrapperSchema>true&lt;/createWrapperSchema>
                 &lt;inlineWrapperSchema>true&lt;/inlineWrapperSchema>
                 &lt;soapHeaderNamespacePrefix>header&lt;/soapHeaderNamespacePrefix>
                 &lt;soapHeaderNamespace>http://bus.company.dk/iheader&lt;/soapHeaderNamespace>
                 &lt;soapHeaderLocation>header/header.xsd&lt;/soapHeaderLocation>
                 &lt;soapHeaderWsdlMessageName>OurHeaderMessage&lt;/soapHeaderWsdlMessageName>
                 &lt;soapHeaderWsdlMessagePart>iheader&lt;/soapHeaderWsdlMessagePart>
                 &lt;soapHeaderWsdlMessageElement>OurHeader&lt;/soapHeaderWsdlMessageElement>
             &lt;/configuration>
         &lt;/execution>
     &lt;/executions>
 &lt;/plugin>
 * </pre>
 * <code>* </code> NamespaceFileNameAndLocationResolver specifies which dk.tigerteam.trimm.mdsd.ws.NamespaceFileNameAndLocationResolver
 * implementation to use. The implementation name can either be a Fully Qualified Class Name (FQCN)
 * for a class on the classpath the implements NamespaceFileNameAndLocationResolver OR
 * it can be the relative path to a <code>.groovy</code> script which contains a class that implements NamespaceFileNameAndLocationResolver.
 * <p/>
 * <b>Note: Don't combine this plugin with <code>build-helper-maven-plugin</code> setup to add the same folders!
 *
 * @author Lasse Cramon
 * @author Jeppe Cramon
 * @goal generate
 * @phase generate-sources
 * @requiresDependencyResolution compile
 */
public class TrimmWSMojo extends AbstractMojo {
    public static Character dirSeperator = File.separatorChar;
    private GroovyClassLoader groovyClassLoader;

    /**
     * @parameter expression="${modelFilePath}"
     * @required
     */
    private String modelFilePath;
    /**
     * @parameter expression="${generateToFolder}"
     * @required
     */
    private String generateToFolder;
    /**
     * @parameter expression="${useAliasIfAvailable}"
     * @required
     */
    private boolean useAliasIfAvailable;
    /**
     * @parameter expression="${namespaceAndFileLocationHandling}"
     * @required
     */
    private String namespaceAndFileLocationHandling;
    /**
     * @parameter expression="${inlineReferencedSchemas}"
     */
    private boolean inlineReferencedSchemas;
    /**
     * @parameter expression="${createWrapperSchema}"
     */
    private boolean createWrapperSchema;
    /**
     * @parameter expression="${wrapperElementNameSuffix}"
     */
    private String wrapperElementNameSuffix;
    /**
     * @parameter expression="${inlineWrapperSchema}"
     */
    private boolean inlineWrapperSchema;
    /**
     * @parameter expression="${soapHeaderNamespacePrefix}"
     */
    private String soapHeaderNamespacePrefix = null;
    /**
     * @parameter expression="${soapHeaderNamespace}"
     */
    private String soapHeaderNamespace = null;
    /**
     * @parameter expression="${soapHeaderLocation}"
     */
    private String soapHeaderLocation = null;
    /**
     * @parameter expression="${soapHeaderWsdlMessageName}"
     */
    private String soapHeaderWsdlMessageName = null;
    /**
     * @parameter expression="${soapHeaderWsdlMessagePart}"
     */
    private String soapHeaderWsdlMessagePart = null;
    /**
     * @parameter expression="${soapHeaderWsdlMessageElement}"
     */
    private String soapHeaderWsdlMessageElement = null;

    /**
     * @parameter expression="${project}"
     * @required
     */
    private MavenProject project;
    private String baseDir;

    public TrimmWSMojo() {
    }

    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            getLog().info("==== GENERATING WEBSERVICE FILES ====");

            baseDir = project.getBasedir().getAbsolutePath() + File.separatorChar;
            NameableMetaType.UseAliasIfAvailable = useAliasIfAvailable;
            String fullModelPath = baseDir + modelFilePath;
            String fullGenerateToFolderPath = baseDir + generateToFolder;
            if (shouldGenerate(new File(fullGenerateToFolderPath), new File(fullModelPath), new File(project.getBuild()
                    .getDirectory()))) {

                SoapHeaderHandler soapHeaderHandler = new NoHeaderSoapHeaderHandler();
                if (soapHeaderNamespace != null) {
                    soapHeaderHandler = new OneHeaderSoapHeaderHandler(new SoapHeaderConfiguration(soapHeaderNamespacePrefix, soapHeaderNamespace,
                            soapHeaderLocation, soapHeaderWsdlMessageName, soapHeaderWsdlMessagePart, soapHeaderWsdlMessageElement));
                }
                getLog().info("Using SOAP Header Handler " + soapHeaderHandler.getClass().getName());

                NamespaceFileNameAndLocationResolver namespaceFileNameAndLocationResolver;
                if (namespaceAndFileLocationHandling.equalsIgnoreCase("classic")) {
                    getLog().info("Using 'classic' namespaceAndFileLocationHandling");
                    namespaceFileNameAndLocationResolver = new ClassicNamespaceFileNameAndLocationResolver();
                } else if (namespaceAndFileLocationHandling.equalsIgnoreCase("default")) {
                    getLog().info("Using 'default' namespaceAndFileLocationHandling");
                    namespaceFileNameAndLocationResolver = new DefaultNamespaceFileNameAndLocationResolver();
                } else {
                    getLog().info("Loading NamespaceFileNameAndLocationResolver: '" + namespaceAndFileLocationHandling + "'");
                    namespaceFileNameAndLocationResolver = loadExtension(namespaceAndFileLocationHandling);
                }

                getLog().info("Reading from Model file '" + fullModelPath + "'");
                XmiReader xmiReader = WsGeneratorHelper.selectXmiReader(fullModelPath);
                final MetaModel metaModel = xmiReader.read(new File(fullModelPath));


                WsGenerator wsGenerator = new WsGenerator(inlineReferencedSchemas, createWrapperSchema, inlineWrapperSchema, wrapperElementNameSuffix,
                        new DefaultSimpleTypeResolver(), namespaceFileNameAndLocationResolver,
                        new DefaultSoapFaultHandler(), soapHeaderHandler);
                WsGenerationResult result = wsGenerator.generate(metaModel);
                getLog().info("Generating result to folder '" + fullGenerateToFolderPath + "'");
                WsGeneratorHelper.writeFiles(fullGenerateToFolderPath, result);
            } else {
                getLog().info("All resource files are up to date nothing to generate");
            }
            Resource resource = new Resource();
            resource.setDirectory(fullGenerateToFolderPath);
            project.addResource(resource);
        } catch (Exception e) {
            getLog().error(e);
            throw new MojoExecutionException("TrimmWSMojo failed: " + e.getMessage(), e);
        }
    }

    public boolean shouldGenerate(File generateToFolder, File modelFile, File baseOutputDir) {
        if (!BuildUtils.doesOutputDirectoryExist(baseOutputDir.getAbsolutePath())
                || !BuildUtils.doesOutputDirectoryExist(generateToFolder.getAbsolutePath())) {
            getLog().debug("One or more build output directory does not exists this means a clean was done," + " recommending generate proceed.");
            return true;
        }
        if (BuildUtils.shouldGenerate(baseOutputDir.getAbsolutePath(), modelFile)) {
            getLog().debug("A generate resource file has changed sine last generate," + " recommending generate proceed.");
            return true;
        }
        return false;
    }

    /**
     * Load an extension <b>instance</b> - adheres to the following rules:
     * <ul>
     * <li>If it ends with .groovy is will be loaded as a Groovy Class by reading it in from the groovy script file using {@link #instantiateGroovyClassFromScriptFile(String)}</li>
     * <li>Otherwise it will be deferred to {@link #instantiateClass(String)}</li>
     * </ul>
     *
     * @param extensionValue Either a path to a .groovy script or the FQCN class name for an extension
     * @return The extension <b>instance</b>
     */
    protected <T> T loadExtension(String extensionValue) {
        T extension = null;
        if (extensionValue.endsWith(".groovy")) {
            extension = instantiateGroovyClassFromScriptFile(extensionValue);
        } else {
            extension = instantiateClass(extensionValue);
        }
        getLog().debug("Loaded extension '" + extensionValue + "'");

        return extension;
    }


    /**
     * Load a class from its Fully Qualified Class Name (FQCN)
     *
     * @param fqcn the class' Fully Qualified Class Name (FQCN)
     * @param <T>  The type of class
     * @return The class
     */
    protected <T> Class<T> loadClass(String fqcn) {
        fqcn = fqcn.trim();
        try {
            @SuppressWarnings("unchecked")
            Class<T> classLoaded = (Class<T>) getClassLoader().loadClass(fqcn);
            getLog().debug("Loaded class from classpath: '" + fqcn + "'");
            return classLoaded;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Couldn't find class '" + fqcn + "'", e);
        }
    }

    /**
     * Load a Groovy defined class from an <b>absolute</b> (or relative path which is accessible within the execution path of the generator)
     *
     * @param groovyScriptFilePath The groovy script file path
     * @param <T>                  The type of class to return
     * @return The groovy defined class
     */
    @SuppressWarnings("unchecked")
    protected <T> Class<T> loadGroovyClass(String groovyScriptFilePath) {
        groovyScriptFilePath = groovyScriptFilePath.trim();
        if (groovyClassLoader == null) {
            groovyClassLoader = new GroovyClassLoader(getClassLoader());
        }
        try {
            Class<T> classLoaded = groovyClassLoader.parseClass(new File(groovyScriptFilePath));
            getLog().debug("Loaded Groovy class '" + classLoaded.getName() + "' from Groovy script file '" + groovyScriptFilePath + "'");
            return classLoaded;
        } catch (CompilationFailedException e) {
            throw new RuntimeException("Failed to compile Groovy file '" + groovyScriptFilePath + "'", e);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read Groovy file '" + groovyScriptFilePath + "'", e);
        }
    }

    /**
     * The classloader to use for loading classes and groovy script defined classes.<br/>
     */
    protected ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }


    /**
     * Instantiate a Groovy defined class from a <b>relative</b> path
     *
     * @param relativeScriptPath The groovy script file path
     * @param <T>                The type of object to return
     * @return The groovy class <b>instance</b>
     */
    protected <T> T instantiateGroovyClassFromScriptFile(String relativeScriptPath) {
        String scriptPath = toAbsolutePath(relativeScriptPath.trim());
        Class<? extends T> classLoaded = null;
        try {
            classLoaded = loadGroovyClass(scriptPath);
            return (T) classLoaded.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Failed to load Groovy class from '" + scriptPath + "' " + e.getMessage(), e);
        }
    }

    /**
     * Instantiate a class from its Fully Qualified Class Name (FQCN)
     *
     * @param fqcn The class' Fully Qualified Class Name (FQCN)
     * @param <T>  The type of object to return
     * @return The class <b>instance</b>
     */
    protected <T> T instantiateClass(String fqcn) {
        Class<? extends T> classLoaded = null;
        try {
            classLoaded = loadClass(fqcn);
            return (T) classLoaded.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Failed to instantiate class from FQCN '" + fqcn + "'."
                    + (classLoaded != null ? " Class loaded: '" + classLoaded.getName() + "'" : ""), e);
        }
    }

    /**
     * Resolve a relative path to an absolute path but preappending the baseDir's absolute path<br/>
     * <b>Note: It is assumed that the path <b>is relative</b> - so if you specify an aboslute path the result will be wrong!
     *
     * @param relativePath The relative path (may start with an optional <code>/</code>)
     * @return the absolute path
     */
    public String toAbsolutePath(String relativePath) {
        String path = baseDir;
        if (!relativePath.startsWith(dirSeperator.toString())) {
            path = path + dirSeperator;
        }
        path = path + relativePath;
        return path;
    }

}
