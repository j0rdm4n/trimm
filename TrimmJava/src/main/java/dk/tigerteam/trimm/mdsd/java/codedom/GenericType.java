/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;

/**
 * A generic type, which support Javas parameterization of Classes and Interfaces.<br/>
 * Example:<br/>
 * T extends Person -> Symbol: T, Specialization: Extends, Clazz: Person<br/>
 * ? super Object -> Symbol: {@link #WILDCARD_SYMBOL}, Specialization: Super, Name/Class: {@link Object}<br/>
 * T -> Symbol: T, Specialization: None<br/>
 * Person -> Specialization: Super, Clazz: Person<br/>
 *
 * @author Jeppe Cramon
 */
public class GenericType extends Type {

    public static String WILDCARD_SYMBOL = "?";

    public enum Specialization {
        None, Extends, Super
    }

    private String symbol;
    private Specialization specialization = Specialization.None;

    public GenericType(MetaClazz metaClazz) {
        super(metaClazz);
    }

    public GenericType(ClazzOrInterface clazzOrInterface) {
        super(clazzOrInterface);
    }

    public GenericType(Class<?> _class) {
        super(_class);
    }

    public GenericType(String className) {
        super(className);
    }

    public GenericType(Clazz clazz) {
        super(clazz);
    }

    public GenericType(Interface _interface) {
        super(_interface);
    }

    public GenericType() {
    }

    /**
     * The generics symbol (if any):<br/>
     */
    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public boolean isWildcard() {
        return WILDCARD_SYMBOL.equals(symbol);
    }

    public boolean hasSymbol() {
        return symbol != null;
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		if (hasSymbol()) {
//			writer.print(symbol);
//		}
//		if (specialization != Specialization.None) {
//			writer.print(specialization.toString().toLowerCase());
//		}
//		if (getName() != null) {
//			super.generateCode(writer);
//		}
//	}

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
