/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;

import java.util.List;

/**
 * This adapter is responsible for automatically deferring all calls to the wrapped {@link GeneratorEventListener}
 *
 * @author Jeppe Cramon
 * @see JavaGenerator uses this internally to wrap all {@link GeneratorEventListener}' marked with the
 *      {@link DeferredGeneratorEventListener} marker interface
 */
public class DeferredEventHandlerAdapter implements GeneratorEventListener {

    private GeneratorEventListener wrappedListener;

    public DeferredEventHandlerAdapter(GeneratorEventListener wrappedListener) {
        super();
        this.wrappedListener = wrappedListener;
    }

    public boolean handle(final GeneratorEvent event) {
        JavaGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {
            public void execute() {
                wrappedListener.handle(event);
            }

            public String getDescription() {
                return "DeferredEventHandlerAdapter handling event " + event.getClass().getName() + " for "
                        + wrappedListener.getClass().getName();
            }
        });
        return true;
    }

    public void generationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
        wrappedListener.generationComplete(allGeneratedClazzes);
    }

    public void generationCommenced() {
        wrappedListener.generationCommenced();
    }

    @Override
    public String toString() {
        return wrappedListener.toString();
    }

}
