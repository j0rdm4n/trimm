/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

/**
 * An annotation definition.<br/>
 *
 * @author Jeppe Cramon
 * @see Annotation
 */
public class AnnotationDefinition extends Interface {

    public AnnotationDefinition(AccessModifier... accessModifiers) {
        super(accessModifiers);
    }

    public AnnotationDefinition(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
    }

    public AnnotationDefinition(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
