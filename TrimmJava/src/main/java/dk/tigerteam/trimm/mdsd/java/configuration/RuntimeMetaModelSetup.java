/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.configuration;

import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaModel;

/**
 * YAML specific configuration code (for setting up {@link RuntimeMetaModel} generation)
 *
 * @author Jeppe Cramon
 */
public class RuntimeMetaModelSetup {

    private String packageName;

    /**
     * Under which package name should the {@link RuntimeMetaModel} be placed=
     */
    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}