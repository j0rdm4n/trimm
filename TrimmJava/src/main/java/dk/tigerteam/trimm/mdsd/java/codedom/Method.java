/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.meta.MetaOperation;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a Java method.
 * <p/>
 * Default return type is {@link Type#VOID}
 *
 * @author Jeppe Cramon
 */
public class Method extends CodeComposite {
    private List<Parameter> parameters = new LinkedList<Parameter>();
    private Type returnType = Type.VOID;
    private List<Type> throwsExceptions = new LinkedList<Type>();

    public Method(AccessModifier... accessModifiers) {
        super(accessModifiers);
    }

    public Method(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
    }

    public Method(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

    public Method(String name, Clazz owner, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
        setOwner(owner);
    }

    public Method(String name, Interface owner, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
        setOwner(owner);
    }

    @Override
    public MetaOperation getMetaType() {
        return (MetaOperation) super.getMetaType();
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public Method setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
        return this;
    }

    public Method addParameters(Parameter... parameters) {
        getParameters().addAll(Arrays.asList(parameters));
        return this;
    }

    public List<Type> getThrowsExceptions() {
        return throwsExceptions;
    }

    public Method setThrowsExceptions(List<Type> throwsExceptions) {
        this.throwsExceptions = throwsExceptions;
        return this;
    }

    public Method addThrowsExceptions(Type... exceptions) {
        getThrowsExceptions().addAll(Arrays.asList(exceptions));
        return this;
    }

    public Method setReturnType(Type returnType) {
        this.returnType = returnType;
        return this;
    }

    public Method setReturnType(Class<?> returnType) {
        this.returnType = new Type(returnType);
        return this;
    }

    public Method setReturnType(Clazz returnType) {
        this.returnType = new Type(returnType);
        return this;
    }

    public Type getReturnType() {
        return returnType;
    }

    /**
     * Used for Dynamically types languages, like Groovy, where fields, variables, properties, etc. doesn't need types
     *
     * @return true if no {@link #getReturnType()} ()} has been specified
     */
    public boolean isDynamicallyTyped() {
        return returnType == null;
    }

    public ClazzOrInterface getClazzOrInterfaceOwner() {
        if (getOwner() == null) {
            return null;
        }
        if (getOwner() instanceof Property) {
            return ((Property) getOwner()).getOwner();
        } else if (getOwner() instanceof ClazzOrInterface) {
            return (ClazzOrInterface) getOwner();
        } else {
            throw new IllegalStateException("Didn't expect Method owner to be of type " + getOwner().getClass());
        }
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		writer.printJavaDoc(this);
//		writer.printAnnotations(this);
//		writer.printAccessModifiers(this);
//		getReturnType().generateCode(writer);
//		// Support static constructors without name nor arguments
//		if (getName() != null) {
//			writer.print(" ").print(getName()).print("(");
//			writer.printCodeElementCollection(getParameters());
//			writer.print(") ");
//			if (getThrowsExceptions().size() > 0) {
//				writer.print("throws ");
//				writer.printCodeElementCollection(getThrowsExceptions());
//			}
//		}
//		if (getClazzOrInterfaceOwner().isInterface() || hasAccessModifier(AccessModifier.Abstract)) {
//			writer.println(";");
//		} else {
//			writer.println(" {");
//			writeMethodBodyContent(writer);
//			writer.println("}");
//		}
//	}
//
//	@Deprecated
//	protected void writeMethodBodyContent(CodeWriter writer) {
//		for (CodeElement child : getChildrenAsXmiElements()) {
//			child.generateCode(writer);
//		}
//	}

    /**
     * Sugar method for adding CodeElement and the like to the method body
     *
     * @param elements
     * @return
     * @deprecated Use addCode instead :) (shorter name)
     */
    public Method addMethodBodyElements(CodeElement... elements) {
        addChildren(elements);
        return this;
    }

    /**
     * Sugar method for adding CodeElement and the like to the method body
     *
     * @param elements
     * @return
     */
    public Method addCode(CodeElement... elements) {
        addChildren(elements);
        return this;
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
