/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;

import java.util.Arrays;
import java.util.List;

/**
 * Represent a Type (either a class by its FQCN (Fully Qualified Class Name) or a {@link Clazz}
 * <p/>
 * The {@link #getName()} methods returns the textually FQCN. Use {@link #getClazz()} to check if we have a reference to a {@link Clazz} in
 * our own type hierarchy
 * <p/>
 * Supports <b>generics</b> (aka. parameterized types) through helper methods {@link #addGenerics(Type...)}, {@link #getGenerics()}.<br/>
 * In fact generics are just children of {@link Type}, so you can also manipulate generics using {@link #getChildren()}.
 * <p/>
 * <b>Note: In general, when sharing Types, it's a good idea to use {@link #clone()}, to keep the original type intact (in case you want to
 * change it somewhere).</b>
 *
 * @author Jeppe Cramon
 * @see #NONE Returntype for {@link Constructor}'
 * @see #VOID Default {@link Method} returntype
 * @see FQCN
 * @see Clazz
 */
public class Type extends CodeComposite implements Cloneable {
    /**
     * Represents a NO (return) type, which is used for {@link Constructor}'
     */
    public static final Type NONE = new Type("");
    /**
     * Represents a (return) type {@link Void} which is used by {@link Method}' and {@link SetterMethod}' by default
     */
    public static final Type VOID = new Type("void");

    private Clazz clazz;
    private Interface _interface;
    private Class<?> _class;
    /**
     * Is this type used as a Reference (i.e. FQCN.class instead of just FQCN)
     */
    private boolean isReference;

    /**
     * Sugar constructor for using built in types (will be transformed to a FQCN string)<br/>
     * The class parameter can be fetched using {@link #getWrappedJavaClass()}
     *
     * @param _class The Java type
     */
    public Type(Class<?> _class) {
        setWrappedJavaClass(_class);
    }

    /**
     * Sugar constructor for using built in types (will be transformed to a FQCN string)<br/>
     * The class parameter can be fetched using {@link #getWrappedJavaClass()}
     *
     * @param _class   The Java type
     * @param external Is it an external class out side of Trimm dependencies
     */
    public Type(Class<?> _class, boolean external) {
        setWrappedJavaClass(_class);
        setExternal(external);
    }

    public Type(String className) {
        super(className);
    }

    public Type(String className, boolean external) {
        super(className);
        setExternal(external);
    }

    /**
     * Is this type used as a Reference (i.e. FQCN.class instead of just FQCN)
     */
    public boolean isReference() {
        return isReference;
    }

    /**
     * Is this type used as a Reference (i.e. FQCN.class instead of just FQCN)
     */
    public Type setReference(boolean reference) {
        isReference = reference;
        return this;
    }

    /**
     * Sugar constructor
     *
     * @param clazzOrInterface
     */
    public Type(ClazzOrInterface clazzOrInterface) {
        if (clazzOrInterface == null) {
            throw new NullPointerException("Can't initialize a Type using a null ClazzOrInterface");
        }
        if (clazzOrInterface.isInterface()) {
            setInterface((Interface) clazzOrInterface);
        } else {
            setClazz((Clazz) clazzOrInterface);
        }
    }

    public Type(Clazz clazz) {
        super();
        if (clazz == null) {
            throw new NullPointerException("Can't initialize a Type using a null Clazz");
        }
        setClazz(clazz);
    }

    public Type(Interface _interface) {
        super();
        if (_interface == null) {
            throw new NullPointerException("Can't initialize a Type using a null Interface");
        }
        setInterface(_interface);
    }

    /**
     * Initialize based on the MetaClazz. Will try to look up corresponding {@link Clazz} using the {@link JavaGeneratorContext} and use this
     * Clazz as basis if it finds the Clazz.<br/>
     * <b>Note: If the Clazz returned from {@link JavaGeneratorContext} is Base class (returns true when calling {@link Clazz#isBaseClazz()}
     * then we'll use the Extension Clazz instead, by calling {@link JavaGeneratorContext#getExtensionClazzForBaseClazz(Clazz)}</b>
     *
     * @param metaClazz The meta clazz to base this Type on
     */
    public Type(MetaClazz metaClazz) {
        ClazzOrInterface typeClazz = JavaGeneratorContext.getContext().findClazzFromMetaClazz(metaClazz);
        if (typeClazz != null) {
            if (typeClazz.isInterface()) {
                setInterface((Interface) typeClazz);
            } else {
                Clazz clazz = (Clazz) typeClazz;
                if (clazz.isBaseClazz()) {
                    clazz = JavaGeneratorContext.getContext().getExtensionClazzForBaseClazz(clazz);
                }
                setClazz(clazz);
            }
        } else if (metaClazz.getWrappedJavaClass() != null) {
            setWrappedJavaClass(metaClazz.getWrappedJavaClass());
        }else {
            setName(metaClazz.getFQCN());
        }
    }

    Type() {
    }

    /**
     * Strips package from name
     *
     * @return simple annotation class name
     */
    public String getSimpleName() {
        int index = getName().lastIndexOf(".");
        if (index != -1) {
            return getName().substring(index + 1, getName().length());
        }
        return getName();
    }

    public ClazzOrInterface getClazzOrInterface() {
        if (clazz != null) {
            return clazz;
        } else if (_interface != null) {
            return _interface;
        }
        return null;
    }

    public Class<?> getWrappedJavaClass() {
        return _class;
    }

    public Type setWrappedJavaClass(Class<?> wrappedJavaClass) {
        this._class = wrappedJavaClass;
        setName(wrappedJavaClass.getName().replace('$', '.'));
        if (this._class.isArray()) {
            setName(_class.getCanonicalName());
        }
        return this;
    }

    public Clazz getClazz() {
        return clazz;
    }

    public void setClazz(Clazz clazz) {
        setName(clazz.getFQCN());
        this.clazz = clazz;
    }

    public Interface getInterface() {
        return _interface;
    }

    public void setInterface(Interface _interface) {
        setName(_interface.getFQCN());
        this._interface = _interface;
    }

    public List<Type> getGenerics() {
        return findInImmediateChildren(new Criteria<Type>(Type.class) {
            public boolean isOk(Type element) {
                return true;
            }
        });
    }

    public Type addGenerics(Type... generics) {
        getChildren().addAll(Arrays.asList(generics));
        return this;
    }

    public boolean isBooleanType() {
        if (getName() == null)
            return false;
        return (getName().equals("boolean") || isBooleanWrapperType());
    }

    public boolean isBooleanWrapperType() {
        return getName().equals("Boolean") || getName().equals("java.lang.Boolean");
    }

    public boolean isIntegerType() {
        if (getName() == null)
            return false;
        return (getName().equals("int") || isIntegerWrapperType());
    }

    public boolean isIntegerWrapperType() {
        return getName().equals("Integer") || getName().equals("java.lang.Integer");
    }

    public boolean isLongType() {
        if (getName() == null)
            return false;
        return (getName().equals("long") || isLongWrapperType());
    }

    public boolean isLongWrapperType() {
        return getName().equals("Long") || getName().equals("java.lang.Long");
    }

    public boolean isShortType() {
        if (getName() == null)
            return false;
        return (getName().equals("short") || isShortWrapperType());
    }

    public boolean isShortWrapperType() {
        return getName().equals("Short") || getName().equals("java.lang.Short");
    }

    public boolean isCharType() {
        if (getName() == null)
            return false;
        return (getName().equals("char") || isCharWrapperType());
    }

    public boolean isCharWrapperType() {
        return getName().equals("Char") || getName().equals("java.lang.Char");
    }

    public boolean isByteType() {
        if (getName() == null)
            return false;
        return (getName().equals("byte") || isByteWrapperType());
    }

    public boolean isByteWrapperType() {
        return getName().equals("Byte") || getName().equals("java.lang.Byte");
    }

    public boolean isFloatType() {
        if (getName() == null)
            return false;
        return (getName().equals("float") || isFloatWrapperType());
    }

    public boolean isFloatWrapperType() {
        return getName().equals("Float") || getName().equals("java.lang.Float");
    }

    public boolean isDoubleType() {
        if (getName() == null)
            return false;
        return (getName().equals("double") || isDoubleWrapperType());
    }

    public boolean isDoubleWrapperType() {
        return getName().equals("Double") || getName().equals("java.lang.Double");
    }

    public boolean isStringType() {
        if (getName() == null)
            return false;
        return (getName().equals("String") || getName().equals("java.lang.String"));
    }

    public boolean isVoidType() {
            if (getName() == null)
                return false;
            return (getName().equals("void") || getName().equals("java.lang.Void"));
        }

    public boolean isBuiltInType() {
        return isBooleanType() || isByteType() || isCharType() || isDoubleType() || isFloatType() || isIntegerType() || isLongType()
                || isShortType() || isStringType() || isVoidType();
    }

    /**
     * Useful for reuse of types, without modification affecting the original Type
     */
    public Type clone() {
        Type clone = new Type();
        clone.setAccessModifiers(getAccessModifiers());
        if (clazz != null)
            clone.setClazz(clazz);
        if (_interface != null)
            clone.setInterface(_interface);
        clone.setCodeDocumentation(getCodeDocumentation());
        clone.setCodeModel(getCodeModel());
        clone.setName(getName());
        for (Type generic : getGenerics()) {
            clone.addChildren(generic.clone());
        }
        clone.setExternal(isExternal());
        return clone;
    }

    /**
     * Specialization of hasAttribute which first checks this Type instances Attributes. If it doesn't find the key here, it will check an
     * eventual clazz instance for the attribute.<br/>
     * <b>Note: The other Attribute related method doesn't share this functionality!!</b>
     */
    @Override
    public boolean hasAttribute(String key) {
        if (super.hasAttribute(key))
            return true;
        if (clazz != null) {
            return clazz.hasAttribute(key);
        }
        return false;
    }

    @Override
    public String toString() {
        if (clazz != null) {
            return clazz.toString();
        } else {
            return getName();
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Type other = (Type) obj;
        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;
        return true;
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
