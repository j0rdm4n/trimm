/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

/**
 * Code visitor that allows the {@link CodeModel} to be written out differently depending on which {@link CodeVisitor} that
 * has been configured
 *
 * @author Jeppe Cramon
 */
public interface CodeVisitor {

    void handle(Annotation annotation);

    void handle(AnnotationAttribute annotationAttribute);

    void handle(AnnotationDefinition annotationDefinition);

    void handle(Clazz clazz);

    void handle(CodePackage codePackage);

    void handle(CodeSnippet codeSnippet);

    void handle(Constructor constructor);

    void handle(Enumeration enumeration);

    void handle(EnumLiteralField enumLiteralField);

    void handle(Field field);

    void handle(FQCN fqcn);

    void handle(GenericType genericType);

    void handle(GetterMethod getterMethod);

    void handle(SetterMethod setterMethod);

    void handle(Import _import);

    void handle(Interface _interface);

    void handle(Method method);

    void handle(PackageFQN packageFQN);

    void handle(Parameter parameter);

    void handle(Property property);

    void handle(Type type);

    void handle(CodeModel codeModel);

    void handle(Closure closure);
}
