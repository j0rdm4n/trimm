/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.Interface;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.InterfaceEvent;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaType;

/**
 * Documents the inheritance hierarchy as JavaDoc for a given Clazz/Interface
 * <p/>
 * This will for instance generate documentation such as this generated for a Clazz Veterinarian which inherits from User
 * together with another class called Customer
 * <p/>
 * <b>Example</b>
 * Hierarchy:
 * <ul>
 * <li>User</li>
 * <ul>
 * <li><b>Veterinarian</b></li>
 * <li>Customer</li>
 * </ul>
 * </ul>
 * @author Jeppe Cramon
 */
public class ClassHierarchyJavaDocListener extends
        AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

    @Override
    protected boolean handleClazzEvent(ClazzEvent event) {
        Clazz clazz = event.getClazz();
        MetaClazz metaClazz = clazz.getMetaType();
        // Doc
        String javaDoc = metaClazz.getDocumentation() != null ? metaClazz.getDocumentation() : "No Documentation";
        javaDoc += "<p/>" + MetaType.newline + "Hierarchy:" + MetaType.newline + JavaGeneratorContext.getContext().getHierarchyAsHtml(clazz);
        clazz.setCodeDocumentation(javaDoc);
        return true;
    }


    @Override
    protected boolean handleInterfaceEvent(InterfaceEvent event) {
        Interface _interface = event.getInterface();
        MetaClazz metaClazz = _interface.getMetaType();
        // Doc
        String javaDoc = metaClazz.getDocumentation() != null ? metaClazz.getDocumentation() : "No Documentation";
        javaDoc += "<p/>" + MetaType.newline + "Hierarchy:" + MetaType.newline + JavaGeneratorContext.getContext().getHierarchyAsHtml(_interface);
        _interface.setCodeDocumentation(javaDoc);
        return true;
    }

}
