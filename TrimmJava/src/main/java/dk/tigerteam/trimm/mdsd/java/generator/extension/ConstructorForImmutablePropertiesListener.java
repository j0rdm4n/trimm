/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.util.MdsdJavaUtil;

import java.util.LinkedList;
import java.util.List;

/**
 * Creates a constructor, which allows setting all immutable/readonly properties (i.e. properties which doesn't have a setter method)
 *
 * @author Jeppe Cramon
 */
public class ConstructorForImmutablePropertiesListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

    @Override
    protected boolean handleClazzEvent(ClazzEvent event) {
        List<Property> immuteableProperties = event.getClazz().getImmuteableProperties();
        List<Property> superClazzImmuteableProperties = new LinkedList<Property>();
        if (event.getClazz().extendsAGeneratedClazz()) {
            superClazzImmuteableProperties = event.getClazz().getExtendsType().getClazz().getImmuteableProperties();
        }

        if (immuteableProperties.size() > 0) {
            Constructor constructor = new Constructor(CodeElement.AccessModifier.Public);
            MdsdJavaUtil.sortPropertiesAlphabetical(superClazzImmuteableProperties);
            constructor.addCode(new CodeSnippet("super(", MdsdJavaUtil.propertiesAsCommaSeparatedString(superClazzImmuteableProperties), ");"));
            MdsdJavaUtil.sortPropertiesAlphabetical(immuteableProperties);
            for (Property immuteableProperty : immuteableProperties) {
                constructor.addParameters(new Parameter(immuteableProperty.getName(), immuteableProperty.getField().getType()));
                if (!superClazzImmuteableProperties.contains(immuteableProperty)) {
                    constructor.addCode(new CodeSnippet("this.", immuteableProperty.getField().getName(), " = ", immuteableProperty.getName(), ";"));
                }
            }
            event.getClazz().add(constructor);
        }

        return true;
    }

}
