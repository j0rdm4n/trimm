/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.util.MdsdJavaUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Implements a <code>toString</code> method with the properties from clazz.
 * You can specify if it should include superClassProperties, collections and sort the output according to the property name (ascending)
 *
 * @author Lasse Cramon
 */
public class ToStringListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

    private boolean includeSuperClassProperties = false;
    private boolean includeCollections = false;
    private boolean sort = true;

    @Override
    public void generationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
        for (ClazzOrInterface clazzOrInterface : allGeneratedClazzes) {
            if (clazzOrInterface.isClazz()) {
                Clazz clazz = (Clazz) clazzOrInterface;
                clazzToString(clazz);
            }
        }
    }

    protected boolean clazzToString(Clazz clazz) {
        // Only generate if there isn't already a toString() method
        if (clazz.getMethod("toString", Collections.<Parameter> emptyList()) != null) return true;

        List<Property> properties = new ArrayList<Property>();
        if (includeSuperClassProperties) {
            properties = clazz.getAllPropertiesIncludingSuperClazzProperties();
        } else {
            properties = clazz.getProperties();
        }
        if (sort) {
            MdsdJavaUtil.sortPropertiesAlphabetical(properties);
        }
        if (!properties.isEmpty()) {
            Method method = new Method("toString", AccessModifier.Public);
            method.setReturnType(String.class);
            StringBuilder builder = new StringBuilder();
            builder.append("return \"" + clazz.getName() + "[\"");
            int count = 0;
            for (Property property : properties) {
                if ((MdsdJavaUtil.isPropertyCollection(property) && includeCollections) || !MdsdJavaUtil.isPropertyCollection(property)) {
                    if (++count > 1) {
                        builder.append(" + \", " + property.getName() + ": \" +" + property.getName());
                    } else {
                        builder.append(" + \"" + property.getName() + ": \" +" + property.getName());
                    }
                }
            }
            builder.append(" + \"]\";");
            method.addCode(new CodeSnippet(builder.toString()));
            clazz.addChildren(method);
        }
//		}
        return true;
    }

    public ToStringListener setIncludeSuperClassProperties(boolean includeSuperClassProperties) {
        this.includeSuperClassProperties = includeSuperClassProperties;
        return this;
    }

    public ToStringListener setIncludeCollections(boolean includeCollections) {
        this.includeCollections = includeCollections;
        return this;
    }

    public ToStringListener setSort(boolean sort) {
        this.sort = sort;
        return this;
    }

}
