/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeModel;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaOperation;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;

import java.util.List;

/**
 * Code generation strategy that allows you to fine tune the Code Generation processing - see {@link DefaultJavaCodeGenerationStrategy} for inspiration
 *
 * @author Jeppe Cramon
 */
public interface JavaCodeGenerationStrategy {
    /**
     * Should the {@link dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator} generate an Extension Clazz for the ClazzOrInterface (following the Base/Extension class pattern) -
     * Default false for interfaces and enumerations and true for everything else
     *
     * @param clazz The clazz or interface in question
     */
    boolean shouldGenerateExtensionClazzFor(ClazzOrInterface clazz);

    /**
     * Post process Clazz' after that have been fully resolved and any listeners are done doing their job (this is after secondPassGeneration
     * is done but before code is written to the file system)
     *
     * @param clazzes All clazzes that have been generated
     * @return The clazzes to be written to the file system
     */
    List<ClazzOrInterface> postProcessClazzes(List<ClazzOrInterface> clazzes);

    /**
     * Callback method that allows for specialization of the base Clazz name (default is to preappend it with "Abstract")
     *
     * @param baseClazz      The base Clazz
     * @param extensionClazz The extension Clazz
     * @return The name to use for the base Clazz
     */
    String getBaseClazzName(Clazz baseClazz, Clazz extensionClazz);

    /**
     * Preprocess a Clazz before any properties and associations are resolved and processed.<br/>
     * Can be useful for processing Stereotypes, like JPA Embeddable cases, which changes a lot about how code later is generated.
     *
     * @param clazz     The clazz just created during firstPassGeneration
     * @param metaClazz The clazz' corresponding MetaClazz
     */
    void preProcessClazz(ClazzOrInterface clazz, MetaClazz metaClazz);

    /**
     * Change to fix and validate any MetaClazz's before generation starts. This is typically used to fix built in types, like the user writes
     * "Date", but means "java.util.Date". (however we recommend using {@link dk.tigerteam.trimm.mdsd.java.generator.extension.BuiltInTypesListener} for this purpose specifically)
     *
     * @param metaClazz The metaClazz to fix (if at all)
     */
    void preProcessMetaClazz(MetaClazz metaClazz);

    /**
     * Override this method to control which {@link dk.tigerteam.trimm.mdsd.meta.MetaClazz}' we generate {@link dk.tigerteam.trimm.mdsd.java.codedom.Clazz}'<br/>
     * Recommended rules for not allowing code generation:
     * <ul>
     * <li>If {@link dk.tigerteam.trimm.mdsd.meta.MetaClazz#isBuiltInType()} is <code>true</code></li>
     * <li>If the {@link dk.tigerteam.trimm.mdsd.meta.MetaClazz} or any owner (upwards recursively) has stereotype {@link dk.tigerteam.trimm.mdsd.java.generator.JavaStereotypes#DONT_GENERATE_STEREOTYPE_NAME} applied</li>
     * </ul>
     *
     * @param metaClazz The MetaClazz to test against
     * @return true if we should generate for the {@link dk.tigerteam.trimm.mdsd.meta.MetaClazz} otherwise false
     */
    boolean shouldGenerateForMetaClazz(MetaClazz metaClazz);

    /**
     * Guard method to turn of all method generation or just individual methods<br/>
     * Recommended rule for disallowing code generation:
     * <ul>
     * <li>The meta operation has the stereotype {@link dk.tigerteam.trimm.mdsd.java.generator.JavaStereotypes#DONT_GENERATE_STEREOTYPE_NAME} applied</li>
     * </ul>
     *
     * @param metaOperation The meta operation that we're processing
     * @return true if we should generate a {@link dk.tigerteam.trimm.mdsd.java.codedom.Method} for the {@link dk.tigerteam.trimm.mdsd.meta.MetaOperation}, otherwise false
     */
    boolean shouldGenerateForMetaOperation(MetaOperation metaOperation);

    /**
     * Guard method used by {@link dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator#generateEnumerators(dk.tigerteam.trimm.mdsd.java.codedom.Enumeration)}.<br/>
     * Determines if we should generate an {@link dk.tigerteam.trimm.mdsd.java.generator.event.EnumeratorLiteralEvent} for the given
     * {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty}.<br/>
     * Recommended rules for disallowing code generation:
     * <ul>
     * <li>The meta property has the stereotype {@link dk.tigerteam.trimm.mdsd.java.generator.JavaStereotypes#DONT_GENERATE_STEREOTYPE_NAME} applied</li>
     * <li>We don't generate {@link dk.tigerteam.trimm.mdsd.java.generator.event.EnumeratorLiteralEvent} for {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty}' that are
     * part in an Association (we don't want to generate properties in the enumeration class for the opposite side of an association to our
     * enumeration)
     * </li>
     * </ul>
     *
     * @param metaProperty The {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty}
     * @return true if we should generate an {@link dk.tigerteam.trimm.mdsd.java.generator.event.EnumeratorLiteralEvent} for the given
     *         {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty}, otherwise false.
     */
    boolean shouldGenerateForEnumeratorProperty(MetaProperty metaProperty);

    /**
     * Guard method that determines if we should generate code for the given {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty}.<br/>
     * Recommended for disallowing generation are:
     * <ul>
     * <li>For unidirectional associations, we only generate for the {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty} that is the owner of the Association (
     * {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty#isOwnerOfAssociation()})</li>
     * <li>If the meta property has the stereotype {@link dk.tigerteam.trimm.mdsd.java.generator.JavaStereotypes#DONT_GENERATE_STEREOTYPE_NAME}</li>
     * </ul>
     *
     * @param metaProperty The meta property that we are testing
     * @return true if we should generate for the given {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty}, otherwise false
     */
    boolean shouldGenerateForMetaProperty(MetaProperty metaProperty);

    /**
     * This method is used to determined how Many-to-Many bidirectionality is handled. In the {@link dk.tigerteam.trimm.mdsd.meta.MetaModel} a Many-to-Many
     * {@link dk.tigerteam.trimm.mdsd.meta.MetaAssociation} might not be modeled/marked as bidirectional, but logically it does make sense to code wise handle them as
     * bidirectional, which is what this method does by default.
     *
     * @param metaProperty the meta property to resolve many-to-many bidirectionality for
     */
    void resolveManyToManyBidirectionality(MetaProperty metaProperty);

    /**
     * Create an extension class for the metaClazz and it's matches clazz instance - the workings in here involves shifting the
     * clazz hierarchy around making the original clazz superclass of the generated extension class, where by the original clazz by our
     * definition becomes the base class. Base-class name is resolved using
     * {@link #getBaseClazzName(dk.tigerteam.trimm.mdsd.java.codedom.Clazz, dk.tigerteam.trimm.mdsd.java.codedom.Clazz)}.
     * Then the baseclass is made <code>abstract</code><p/>
     * <b>Finally the Base class and Extension class pair <u>MUST</u> be registered in the {@link JavaGeneratorContext} using
     * {@link JavaGeneratorContext#registerBaseClazzAndExtensionClazzPair(dk.tigerteam.trimm.mdsd.java.codedom.Clazz, dk.tigerteam.trimm.mdsd.java.codedom.Clazz)}</b>
     * <p/>
     * <i>Note: We recommend to support applying {@link dk.tigerteam.trimm.mdsd.ExtensionClazz} and {@link dk.tigerteam.trimm.mdsd.BaseClazz}
     * annotations on the Extension- and Base-classes if {@link dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext#isAddExtensionAndBaseClazzAnnotations()}
     * is set to <code>true</code>.</i>
     *
     * @param codeModel The code model
     * @param metaClazz The metaClazz
     * @param clazz     The clazz instance created based on the metaClazz
     */
    void createExtensionClazz(CodeModel codeModel, MetaClazz metaClazz, ClazzOrInterface clazz);

}
