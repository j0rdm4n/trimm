/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.configuration;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;
import dk.tigerteam.trimm.mdsd.java.generator.DefaultJavaInheritanceAndClazzesStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.RuntimeMetaDataCreator;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.CodeWritingStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.FileSystemCodeWritingStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.JavaCodeWriter;
import dk.tigerteam.trimm.mdsd.java.generator.event.GeneratorEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.GeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.extension.ImportListener;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.util.BuildUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

/**
 * Java POJO style code generator - Also support read its configuration from a YAML file by calling {@link #generate(java.io.File, java.io.File, java.io.File, boolean, ClassLoader)}
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class PojoConfigurationBasedCodeGenerator extends AbstractConfigurationBasedCodeGenerator<JavaConfiguration, JavaGeneratorContext> {
    private static final Log log = LogFactory.getLog(PojoConfigurationBasedCodeGenerator.class);

    public static void main(String args[]) {
        System.out.println("*****************************************");
        System.out.println("* TRIMM Java code generator             *");
        System.out.println("*****************************************");


        JavaGeneratorCommandLineArguments generatorArgs = new JavaGeneratorCommandLineArguments();
        JCommander jCommander = new JCommander(generatorArgs, args);
        if (generatorArgs.help) {
            jCommander.usage();
            System.exit(-1);
        }

        if (generatorArgs.baseDir == null || !generatorArgs.baseDir.exists()) {
            System.err.println("Can't find Base directory '" + generatorArgs.baseDir + "'");
            System.exit(-1);
        }
        File outputDir = generatorArgs.baseDir;
        if (generatorArgs.outputDir != null) {
            outputDir = new File(generatorArgs.baseDir, generatorArgs.outputDir)            ;
        }
        File yamlFile = new File(generatorArgs.baseDir, generatorArgs.yamlFile);

        System.out.println("Using YAML configuration file: '" + yamlFile.getAbsolutePath() + "'");
        System.out.println("Using Base dir               : '" + generatorArgs.baseDir.getAbsolutePath() + "'");
        System.out.println("Using Output dir             : '" + outputDir.getAbsolutePath() + "'");
        if (generatorArgs.forceOverwriteGeneratedExtensionClasses) {
            System.out.println("FORCEFULLY overwriting generated Extension classes!!!");
        }
        PojoConfigurationBasedCodeGenerator.generate(yamlFile, generatorArgs.baseDir, outputDir, generatorArgs.forceOverwriteGeneratedExtensionClasses, PojoConfigurationBasedCodeGenerator.class.getClassLoader());
    }

    // --------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Generate code based on input
     *
     * @param yamlConfigurationFile The file pointing to the YAML configuration file
     * @param baseDir               The base dir that this configuration is run in (used for calculating absolute path's in the YAML configuration file)
     * @param baseOutputDir         The directory to use for output files
     * @param forceOverwriteGeneratedExtensionClasses
     *                              Extra option to allow removing ALL generated extension classes (use with care)
     * @param classLoader           The classloader to use (may be null - in which case the {@link dk.tigerteam.trimm.mdsd.java.configuration.AbstractConfigurationBasedCodeGenerator#getClassLoader()} defaults apply
     * @return The classes and interfaces generated
     */
    public static List<ClazzOrInterface> generate(final File yamlConfigurationFile, final File baseDir, final File baseOutputDir, boolean forceOverwriteGeneratedExtensionClasses, final ClassLoader classLoader) {
        JavaConfiguration javaConfiguration = null;
        try {
            javaConfiguration = parseJavaConfiguration(yamlConfigurationFile, forceOverwriteGeneratedExtensionClasses);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return generate(javaConfiguration, yamlConfigurationFile, baseDir, baseOutputDir, classLoader);
    }

    /**
     * Generate code based on input
     *
     * @param javaConfiguration     The Java generator configuration - can be parsed using {@link #parseJavaConfiguration(java.io.File, boolean)}
     * @param yamlConfigurationFile The file pointing to the YAML configuration file
     * @param baseDir               the base dir that this configuration is run in (used for calculating absolute path's)
     * @param baseOutputDir         The directory to use for output files
     * @param classLoader           The classloader to use (may be null - in which case the {@link dk.tigerteam.trimm.mdsd.java.configuration.AbstractConfigurationBasedCodeGenerator#getClassLoader()} defaults apply
     * @return The classes and interfaces generated
     */
    public static List<ClazzOrInterface> generate(JavaConfiguration javaConfiguration, final File yamlConfigurationFile, final File baseDir, final File baseOutputDir, final ClassLoader classLoader) {
        try {
            if (shouldGenerate(yamlConfigurationFile, baseDir, baseOutputDir, javaConfiguration)) {
                return new PojoConfigurationBasedCodeGenerator(javaConfiguration, baseDir, baseOutputDir, classLoader).generate();
            } else {
                log.info("All resource files are up to date nothing to generate");
                return null;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Parse the {@link JavaConfiguration} from the yaml file
     *
     * @param yamlConfigurationFile The file pointing to the YAML configuration file
     * @param forceOverwriteGeneratedExtensionClasses
     *                              Extra option to allow removing ALL generated extension classes (use with care)
     * @return The Java codegeneration configuration
     * @throws FileNotFoundException
     */
    public static JavaConfiguration parseJavaConfiguration(final File yamlConfigurationFile, boolean forceOverwriteGeneratedExtensionClasses) throws FileNotFoundException {
        FileReader reader = null;
        try {
            reader = new FileReader(yamlConfigurationFile);
            JavaConfiguration configuration = new Yaml().loadAs(reader, JavaConfiguration.class);
            configuration.setForceOverwriteGeneratedExtensionClasses(forceOverwriteGeneratedExtensionClasses);
            return configuration;
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }

    /**
     * Guard method which inspects baseOutputDirectory, Xmi file (metamodel file) and Configuration file and determines if a regeneration is required
     *
     * @param configFile    The path to the (YAML) configuration file
     * @param baseDir       the base dir that this configuration is run in (used for calculating absolute path'es)
     * @param baseOutputDir The directory to use for output files
     * @param configuration The configuration instance (which contains the relative path to the Xmi/Model file
     * @return <code>true</code> if a regeneration is required, otherwise it returns <code>false</code>
     */
    public static boolean shouldGenerate(File configFile, File baseDir, File baseOutputDir, JavaConfiguration configuration) {
        if (!BuildUtils.doesOutputDirectoryExist(baseOutputDir.getAbsolutePath()) ||
                !BuildUtils.doesOutputDirectoriesExist(configuration.getGenerateToPaths())) {
            log.info("One or more build output directory does not exists this means a clean was done,"
                    + " recommending generate proceed.");
            return true;
        }
        File xmiFile = new File(configuration.getXmiModelPath());
        if (!xmiFile.isAbsolute() && baseDir != null) {
            xmiFile = new File(toAbsolutePath(baseDir, configuration.getXmiModelPath()));
        }
        if (BuildUtils.shouldGenerate(baseOutputDir.getAbsolutePath(), configFile, xmiFile)) {
            log.info("A generate resource file has changed sine last generate,"
                    + " recommending generate proceed.");
            return true;
        }
        return false;
    }

    // --------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Create a new configured generator, call {@link #generate()} to execute the generation
     *
     * @param javaConfiguration The configuration
     * @param baseDir           the base dir that this configuration is run in (used for calculating absolute path'es)
     * @param baseOutputDir     The directory to use for output files
     */
    public PojoConfigurationBasedCodeGenerator(JavaConfiguration javaConfiguration, File baseDir, File baseOutputDir) {
        super(javaConfiguration, baseDir, baseOutputDir);
    }

    /**
     * Create a new configured generator, call {@link #generate()} to execute the generation
     *
     * @param javaConfiguration The configuration
     * @param baseDir           the base dir that this configuration is run in (used for calculating absolute path'es)
     * @param baseOutputDir     The directory to use for output files
     * @param classLoader       The classloader to use
     */
    public PojoConfigurationBasedCodeGenerator(JavaConfiguration javaConfiguration, File baseDir, File baseOutputDir, ClassLoader classLoader) {
        super(javaConfiguration, baseDir, baseOutputDir, classLoader);
    }

    @Override
    protected JavaGeneratorContext createGeneratorContext(MetaModel metaModel) {
        return new JavaGeneratorContext(metaModel);
    }

    @Override
    protected List<ClazzOrInterface> generateInternal(JavaGeneratorContext generatorContext) {
        JavaGenerator javaGenerator = new JavaGenerator();

        if (getConfiguration().getCreateRuntimeMetaModel() != null) {
            generatorContext.addEventListeners(new GeneratorEventListener() {
                @Override
                public void generationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
                    new RuntimeMetaDataCreator(
                            getConfiguration().getCreateRuntimeMetaModel().getPackageName()
                    ).create(allGeneratedClazzes);
                }

                @Override
                public boolean handle(GeneratorEvent event) {
                    return true;
                }

                @Override
                public void generationCommenced() {
                }
            });
        }

        if (getConfiguration().getRootSuperClass() != null) {
            generatorContext.setInheritanceStrategy(new DefaultJavaInheritanceAndClazzesStrategy() {
                @Override
                public Type resolveSuperClazzType(Clazz clazz) {
                    return new Type(getConfiguration().getRootSuperClass());
                }
            });
        } else {
            generatorContext.setInheritanceStrategy(new DefaultJavaInheritanceAndClazzesStrategy());
        }

        if (getConfiguration().isUseImports()) {
            generatorContext.addAsLastEventListener(new ImportListener(getConfiguration().getBasePackageName()));
        }

        JavaCodeWriter codeWriter = createCodeWriter();
        CodeWritingStrategy codeWritingStrategy = new FileSystemCodeWritingStrategy(codeWriter);
        return javaGenerator.transformAndWriteCode(generatorContext, codeWritingStrategy);
    }

    public static class JavaGeneratorCommandLineArguments {
        @Parameter(names = "-yaml", description = "The YAML configuration file to use", required=true)
        public String yamlFile;

        @Parameter(names = "-base", description = "The base dir that this configuration is run in (used for calculating absolute path's in the YAML configuration file). Default is the current folder", converter = FileConverter.class)
        public File baseDir = new File(System.getProperty("user.dir"));

        @Parameter(names = "-output", description = "The directory to use for output files. Default is the current folder")
        public String outputDir;

        @Parameter(names = "-F", description = "Extra option to allow removing ALL generated extension classes (use with care)")
        public boolean forceOverwriteGeneratedExtensionClasses = false;

        @Parameter(names = { "-help", "-h", "-?"}, description = "Displays this help screen", help = true)
        public boolean help;

    }
}