/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.codewriter;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.CodePackage;


/**
 * A {@link PackageRewriter} is responsible for correcting package-names so they match the required naming scheme.<br/>
 * This could for instance be preprending a common packagename (like "com.mycompany.project") or removing
 * parts of a package name.
 *
 * @author Jeppe Cramon
 */
public interface PackageRewriter {
    /**
     * Rewrite the package name
     *
     * @param package_ The {@link CodePackage} to rewrite (will effect all nested elements of the package)
     */
    void rewrite(CodePackage package_);

    /**
     * Rewrite the given {@link Clazz}' package
     *
     * @param clazz The Clazz to rewrite
     */
    void rewrite(ClazzOrInterface clazz);

    /**
     * Rewrite the FQCN (Fully Qualified Class Name)
     *
     * @param fqcn the FQCN
     * @return The rewritten FQCN
     */
    String rewrite(String fqcn);
}
