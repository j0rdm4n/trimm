/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeSnippet;
import dk.tigerteam.trimm.mdsd.java.codedom.Method;
import dk.tigerteam.trimm.mdsd.java.codedom.Parameter;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.util.MdsdJavaUtil;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Generates a <code>hashCode</code> and <code>equals</code> method pair (using Commons.Lang's EqualsBuilder and HashCodeBuilder)
 * for either all properties in the class (if the <code>hashCodeAndEquals</code> <b>stereotype</b> is applied at class level) or
 * for the individual properties (if the <code>hashCodeAndEquals</code> <b>stereotype</b> applied at the property level)
 *
 * @author Lasse Cramon
 * @author Jeppe Cramon
 */
public class HashCodeAndEqualsListener extends AbstractGeneratorEventListener
        implements DeferredGeneratorEventListener {

    public static final String HASHCODE_AND_EQUALS_STEREOTYPE_NAME = "hashCodeAndEquals";

    @Override
    protected boolean handleClazzEvent(ClazzEvent event) {
        MetaClazz metaClazz = event.getMetaClazz();
        List<MetaProperty> excludeProperties = new ArrayList<MetaProperty>();

        boolean hasCandidateProperties = false;
        if (metaClazz.hasStereoType(HASHCODE_AND_EQUALS_STEREOTYPE_NAME)) {
            hasCandidateProperties = true;
        } else {
            for (MetaProperty property : metaClazz.getProperties()) {
                if (property.hasStereoType(HASHCODE_AND_EQUALS_STEREOTYPE_NAME)) {
                    hasCandidateProperties = true;
                } else if (property.getName() != null) {
                    excludeProperties.add(property);
                }
            }
        }

        if (hasCandidateProperties) {
            Method equalsMethod = new Method("equals", AccessModifier.Public);
            equalsMethod.setReturnType(boolean.class);
            equalsMethod.addParameters(new Parameter("rhs", Object.class));
            equalsMethod.addAnnotations(new Annotation(Override.class));
            CodeSnippet equalsCodeSnippet = new CodeSnippet("return ", EqualsBuilder.class, ".reflectionEquals(this, rhs, false, " + event.getClazz().asFQCN() + ".class, new String[]  {", MdsdJavaUtil.metaPropertiesAsQuotedCommaSeparatedString(excludeProperties), "});");
            equalsMethod.addCode(equalsCodeSnippet);
            event.getClazz().addChildren(equalsMethod);

            Method hashCodeMethod = new Method("hashCode", AccessModifier.Public);
            hashCodeMethod.setReturnType(int.class);
            hashCodeMethod.addAnnotations(new Annotation(Override.class));
            CodeSnippet hashCodeCodeSnippet = new CodeSnippet("return ", HashCodeBuilder.class, ".reflectionHashCode(17, 31, this, false, " + event.getClazz().asFQCN() + ".class, new String[]  {", MdsdJavaUtil.metaPropertiesAsQuotedCommaSeparatedString(excludeProperties), "});");
            hashCodeMethod.addCode(hashCodeCodeSnippet);
            event.getClazz().addChildren(hashCodeMethod);
        }
        return true;
    }

}
