/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.configuration;

import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;
import dk.tigerteam.trimm.mdsd.java.generator.DefaultModelPreprocessor;
import dk.tigerteam.trimm.mdsd.java.generator.JavaCollectionTypeResolver;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.ModelPreprocessor;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.AbstractCodeWriter;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.DefaultPackageRewriter;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.JalopyFormattingCodeWriter;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.PackageRewriter;
import dk.tigerteam.trimm.mdsd.java.generator.event.GeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.extension.BuiltInTypesListener;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.MetaModelTransformer;
import dk.tigerteam.trimm.mdsd.meta.NameableMetaType;
import dk.tigerteam.trimm.mdsd.uml2.xmi.XmiReader;
import dk.tigerteam.trimm.mdsd.uml2.xmi.ea.EAXmiReader;
import dk.tigerteam.trimm.mdsd.uml2.xmi.magicdraw.MagicDraw16XmiReader;
import dk.tigerteam.trimm.mdsd.uml2.xmi.magicdraw.MagicDraw17XmiReader;
import dk.tigerteam.trimm.util.ReflectUtils;
import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.groovy.control.CompilationFailedException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.*;

/**
 * Base implementation for all Java code Generators that adheres to the {@link CONFIGURATION} class hierarchy, and which parses the
 * {@link CONFIGURATION} from a Yaml configuration file.
 * <p/>
 * Example for full blown YAML configuration file:
 * <p/>
 * <pre>
 * # Setup
 * xmiModelPath: model/MDD Example.xml
 * umlTool: EA
 * basePackageName: dk.tigerteam.mddexample
 * collectionPropertyNameSuffix: Collection
 * generateExtensionClasses: false
 * # addExtensionAndBaseClassAnnotations is only valid if generateExtensionClasses is set to true
 * addExtensionAndBaseClassAnnotations: false
 * modelPreprocessingScript: model/model-preprocessing.groovy
 * sourceCodeFormatting:
 *     # implementation: dk.tigerteam.mdsd.java.generator.codewriter.JalopyFormattingCodeWriter
 *     # Note: Formatting exclusions can be specified even though implementation hasn't been specified (and the default JalopyFormattingCodeWriter is used)
 *     excludeFormattingForPackages:
 *         - dk.tigerteam.mddexample.meta
 *         - dk.tigerteam.mddexample.test
 * #excludePackageRewritingForPackages:
 * #    - some.package
 * #    - some.other.package
 * #stripPackagePrefix: __java
 *
 * # Paths generated-sources/
 * generateBaseClassesToPath: target/generated-sources/domainmodel
 * generateExtensionClassesToPath: src/main/domainmodel-extensions
 * generateInterfacesToPath: target/generated-sources/domainmodel
 * generateTestClassesToPath: target/generated-sources/generated-tests
 * generateResourcesToPath: target/generated-sources/generated-resources
 *
 * mapUmlPropertyTypes:
 *     - name: DateTime
 *       javaType: org.joda.time.DateTime
 *       jpaTypeConverterClass: dk.tigerteam.mddexample.hibernate.PersistentDateTime
 *     - name: LocalDate
 *       javaType: org.joda.time.LocalDate
 *       jpaTypeConverterClass: dk.tigerteam.mddexample.hibernate.PersistentLocalDate
 *
 * # JPA Setup
 * jpaSetup:
 *     addTableAndColumnNames: true
 *     # jpaNamedTablesAndColumnsNameResolver is only valid if addTableAndColumnNames is set to true
 *     jpaNamedTablesAndColumnsNameResolver: model/DatabaseTableNameResolver.groovy
 *     # resolveTableNames, resolveColumnNames and resolveAttributeOverrideColumnNames are only valid when jpaNamedTablesAndColumnsNameResolver ISN'T specified
 *     #resolveTableNames:
 *     #   - defaultTableName: User
 *     #     resolvedTableName: User_
 *     #   - defaultTableName: By
 *     #     resolvedTableName: By_
 *     #resolveColumnNames:
 *     #   - defaultColumnName: value
 *     #     resolvedColumnName: value_
 *     #   - defaultColumnName: type
 *     #     resolvedColumnName: type_
 *     #resolveAttributeOverrideColumnNames:
 *     #   - defaultColumnName: value_type_somethingelse
 *     #     resolvedColumnName: v_t_s
 *
 *     makeBaseClassesIntoMappedSuperClassesIfPossible: true
 *     generatePresentFieldInEmbeddables: false
 *     defaultToLazyFetchingForAllAssociations: true
 *     rootMappedSuperClass: dk.tigerteam.mddexample.model.AbstractEntity
 *     # inheritanceStrategy and idStrategy are only valid when rootMappedSuperClass ISN'T specified
 *     #inheritanceStrategy:
 *     #    implementation: dk.tigerteam.mdsd.jpa.DefaultJpaInheritanceAndClazzesStrategy
 *     #idStrategy:
 *     #    implementation: dk.tigerteam.mdsd.jpa.DefaultJpaIdStrategy
 *
 * # Extensions (note these listeners can also be groovy scripts containing a single class implement the GeneratorListener interface (or
 * # extend specializations thereof)
 * eventListeners:
 *  - listener: dk.tigerteam.mdsd.java.generator.extension.SerialVersionUIDGeneratorListener
 *    configuration:
 *      property1: value1
 *      property2: value2
 *  - listener: dk.tigerteam.mdsd.java.generator.extension.PropertySugarMethodsEventListener
 *    configuration:
 *      property1: value1
 *      property2: value2
 *  - listener: dk.tigerteam.mdsd.java.generator.extension.SerializablePojoListener
 *  - listener: dk.tigerteam.mdsd.java.generator.extension.ImportListener
 *
 * # Create Runtime Meta Model (is a requirement for createIntegrationTest)
 * createRuntimeMetaModel:
 *     packageName: dk.tigerteam.mddexample.meta
 *
 * # Create JPA Integration tests for all entities
 * createIntegrationTest:
 *     testClassName: CompleteModelIntegrationTest
 *     extendsClass: dk.tigerteam.mddexample.model.BaseJpaModelTest
 *     testPackageName: dk.tigerteam.mddexample.model
 *     autoHandleTransactionsAndResources: true
 * </pre>
 *
 * @author Jeppe Cramon
 */
public abstract class AbstractConfigurationBasedCodeGenerator<CONFIGURATION extends JavaConfiguration, GENERATOR_CONTEXT extends JavaGeneratorContext> implements ExtensibleCodeGenerator {
    public static Character dirSeperator = File.separatorChar;

    private Log logger = LogFactory.getLog(this.getClass());
    private final CONFIGURATION configuration;
    private final File baseDir;
    private final File baseOutputDir;
    private ClassLoader classLoader;
    private GroovyClassLoader groovyClassLoader;
    private List<MetaModelTransformer> metaModelTransformers = new ArrayList<MetaModelTransformer>();

    /**
     * Required constructor
     *
     * @param configuration The configuration
     * @param baseDir       the base dir that this configuration is run in (used for calculating absolute path'es)
     * @param baseOutputDir The directory to use for output files
     */
    protected AbstractConfigurationBasedCodeGenerator(CONFIGURATION configuration, File baseDir, File baseOutputDir) {
        if (configuration == null) throw new IllegalArgumentException("Configuration must be specified");
        if (baseDir == null) throw new IllegalArgumentException("BaseDir must be specified");
        if (baseOutputDir == null) throw new IllegalArgumentException("BaseOutputDir must be specified");

        this.configuration = configuration;
        this.baseDir = baseDir;
        this.baseOutputDir = baseOutputDir;
        fixGenerateToPaths();
    }

    public void addMetaModelTransformer(MetaModelTransformer metaModelTransformer) {
        metaModelTransformers.add(metaModelTransformer);
    }

    public void removeMetaModelTransformer(MetaModelTransformer metaModelTransformer) {
        metaModelTransformers.remove(metaModelTransformer);
    }

    public List<MetaModelTransformer> getMetaModelTransformers() {
        return Collections.unmodifiableList(metaModelTransformers);
    }

    /**
     * Required constructor
     *
     * @param configuration The configuration
     * @param baseDir       the base dir that this configuration is run in (used for calculating absolute path'es)
     * @param baseOutputDir The directory to use for output files
     * @param classLoader   The classloader to use
     */
    protected AbstractConfigurationBasedCodeGenerator(CONFIGURATION configuration, File baseDir, File baseOutputDir, ClassLoader classLoader) {
        if (configuration == null) throw new IllegalArgumentException("Configuration must be specified");
        if (baseDir == null) throw new IllegalArgumentException("BaseDir must be specified");
        if (baseOutputDir == null) throw new IllegalArgumentException("BaseOutputDir must be specified");

        this.configuration = configuration;
        this.baseDir = baseDir;
        this.baseOutputDir = baseOutputDir;
        this.classLoader = classLoader;
        fixGenerateToPaths();
    }

    protected void fixGenerateToPaths() {
        if (configuration.getGenerateBaseClassesToPath().equals("")) throw new RuntimeException("You need to provide a generateBaseClassesToPath: value");
        // Default all other folder values to the baseClassesPath
        if (configuration.getGenerateExtensionClassesToPath().equals("")) configuration.setGenerateExtensionClassesToPath(configuration.getGenerateBaseClassesToPath());
        if (configuration.getGenerateInterfacesToPath().equals("")) configuration.setGenerateInterfacesToPath(configuration.getGenerateBaseClassesToPath());
        if (configuration.getGenerateResourcesToPath().equals("")) configuration.setGenerateResourcesToPath(configuration.getGenerateBaseClassesToPath());
        if (configuration.getGenerateTestClassesToPath().equals("")) configuration.setGenerateTestClassesToPath(configuration.getGenerateBaseClassesToPath());
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    /**
     * Generate code - this method parses the metamodel and configures the generator context. Finally it calls
     * {@link #generateInternal(dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext)}
     *
     * @return The classes and interfaces generated
     */
    public final List<ClazzOrInterface> generate() {
        NameableMetaType.UseAliasIfAvailable = getConfiguration().isUseAliasIfAvailable();

        MetaModel metaModel = parseMetaModel();
        transformMetaModel(metaModel);

        GENERATOR_CONTEXT generatorContext = createGeneratorContext(metaModel);
        configureGeneratorContext(generatorContext);
        return generateInternal(generatorContext);
    }

    private void transformMetaModel(MetaModel metaModel) {
        for (MetaModelTransformer metaModelTransformer : metaModelTransformers) {
            metaModelTransformer.transform(metaModel);
        }
    }

    /**
     * This is the main code generator method where the platform/product specific code generator is instantiated, configured and executed
     *
     * @param generatorContext The Generator context to use (at this point it has been fully generically configured accroding to the specification
     *                         in {@link #configureGeneratorContext(dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext)} ). Additional
     *                         product/platform specific configuration might be required
     * @return The classes and interfaces generated
     */
    protected abstract List<ClazzOrInterface> generateInternal(GENERATOR_CONTEXT generatorContext);

    /**
     * Create an instance of the CodeGenerator sepcific {@link GENERATOR_CONTEXT}
     *
     * @param metaModel The meta model we wish to generate code according to
     * @return The generator context
     */
    protected abstract GENERATOR_CONTEXT createGeneratorContext(MetaModel metaModel);

    /**
     * Parse the meta model - this results in loading the Xmi file (absolute path is calculated) and {@link #resolveXmiReader()}
     * plus running the model preprocessing script file if its defined in {@link CONFIGURATION#getModelPreprocessingScript()}
     *
     * @return The meta model
     */
    protected MetaModel parseMetaModel() {
        XmiReader xmiReader = resolveXmiReader();
        logger.info("Resolved XmiReader '" + xmiReader.getClass().getName() + "'");
        File xmiModelPath = new File(configuration.getXmiModelPath());
        if (!xmiModelPath.isAbsolute() && getBaseDir() != null) {
            xmiModelPath = new File(toAbsolutePath(configuration.getXmiModelPath()));
        }
        MetaModel metaModel = xmiReader.read(xmiModelPath);
        if (configuration.getModelPreprocessingScript() != null) {
            runModelPreprocessingScript(metaModel);
        } else {
            // TODO: make configurable
            ModelPreprocessor modelPreprocessor = new DefaultModelPreprocessor();
            metaModel = modelPreprocessor.preprocessMetaModel(metaModel);
        }
        return metaModel;
    }

    /**
     * Runs the preprocessing script, specified in {@link CONFIGURATION#getModelPreprocessingScript()},
     * making the meta model available as an implicit variable called <code>metaModel</code> and
     * the configuration available as <code>configuration</code>         <br/>
     * The path is automatically corrected to become absolute, so in the configuration you must specify it as
     * relative.
     */
    protected void runModelPreprocessingScript(MetaModel metaModel) {
        Map<String, Object> bindingMap = new HashMap<String, Object>();
        bindingMap.put("metaModel", metaModel);
        bindingMap.put("configuration", configuration);
        Binding shellBinding = new Binding(bindingMap);
        GroovyShell groovyShell = new GroovyShell(shellBinding);
        FileReader fileReader = null;
        String scriptPath = toAbsolutePath(configuration.getModelPreprocessingScript());
        logger.debug("Running Model Preprocessing script '" + scriptPath + "'");
        try {
            fileReader = new FileReader(scriptPath);
            groovyShell.evaluate(fileReader);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Failed to read Model Processing Script '" + scriptPath + "'", e);
        } finally {
            IOUtils.closeQuietly(fileReader);
        }
    }

    protected <T extends AbstractCodeWriter> T createCodeWriter() {
        Class<T> codeWriterClass = null;
        if (configuration.getSourceCodeFormatting() != null && configuration.getSourceCodeFormatting().getImplementation() != null) {
            codeWriterClass = loadExtensionClass(configuration.getSourceCodeFormatting().getImplementation());
        } else {
            codeWriterClass = getDefaultCodeWriter();
        }

        // Were're trying to instantiate: e.g. = new JalopyFormattingCodeWriter(configuration, new DefaultPackageRewriter(configuration),
        // baseDir.getAbsolutePath(), new HashSet<String>(configuration.getSourceCodeFormatting().getExcludeFormattingForPackages()));
        Constructor<T> constructor = ReflectUtils.getConstructorIfAvailable(codeWriterClass, new Class<?>[]{
                JavaCodeGenerationPaths.class, PackageRewriter.class, String.class, Collection.class});
        if (constructor == null) {
            throw new RuntimeException("Can't find a constructor in the specified AbstractCodeWriter subclass '" + codeWriterClass.getName()
                    + "', which satisfies arguments of type (JavaCodeGenerationPaths.class, PackageRewriter.class, String.class, Collection.class");
        }
        T codeWriter = null;
        try {
            codeWriter = constructor.newInstance(configuration, createPackageRewriter(), baseOutputDir.getAbsolutePath(),
                    (configuration.getSourceCodeFormatting() != null ? new HashSet<String>(configuration.getSourceCodeFormatting()
                            .getExcludeFormattingForPackages()) : Collections.EMPTY_LIST));
        } catch (Exception e) {
            throw new RuntimeException("Failed to invoke constructor '" + constructor.toGenericString() + "'", e);
        }

        if (configuration.isForceOverwriteGeneratedExtensionClasses()) {
            logger.warn("WARNING: FORCE OVERWRITING GENERATED EXTENSION CLASSES!!!");
            codeWriter.setForceOverwriteGeneratedExtensionClasses(true);
        }
        codeWriter.setUseImports(getConfiguration().isUseImports());
        logger.info("Codewriter is using imports '" + codeWriter.isUseImports() + "'");
        return codeWriter;
    }

    /**
     * Create a Package rewriter configured according to the {@link CONFIGURATION}'s content - default implementation is {@link DefaultPackageRewriter}
     *
     * @return The package rewriter
     */
    protected PackageRewriter createPackageRewriter() {
        return new DefaultPackageRewriter(configuration.getBasePackageName(), configuration.getStripPackagePrefix(), configuration.getExcludePackageRewritingForPackages());
    }

    /**
     * The default {@link AbstractCodeWriter} to use - default {@link JalopyFormattingCodeWriter}
     *
     * @return The default {@link AbstractCodeWriter} to use
     */
    @SuppressWarnings("unchecked")
    protected <T extends AbstractCodeWriter> Class<T> getDefaultCodeWriter() {
        return (Class<T>) JalopyFormattingCodeWriter.class;
    }

    /**
     * Helper method to configure the {@link GENERATOR_CONTEXT} according to the {@link CONFIGURATION}.
     * The following parts are configured:<br/>
     * {@link GENERATOR_CONTEXT#setCollectionTypeResolver(dk.tigerteam.trimm.mdsd.java.generator.CollectionTypeResolver)}  by calling {@link #getDefaultCollectionTypeResolver()}<br/>
     * {@link GENERATOR_CONTEXT#setCollectionPropertyNameSuffix(String)} from {@link CONFIGURATION#getCollectionPropertyNameSuffix()}<br/>
     * {@link GENERATOR_CONTEXT#setAddDefaultConstructor(boolean)} from {@link CONFIGURATION#isAddDefaultConstructor()}<br/>
     * {@link GENERATOR_CONTEXT#setCreateExtensionClazzes(boolean)}  from {@link CONFIGURATION#isGenerateExtensionClasses()}<br/>
     * {@link GENERATOR_CONTEXT#setAddExtensionAndBaseClazzAnnotations(boolean)}  from {@link CONFIGURATION#isAddExtensionAndBaseClassAnnotations()}<br/>
     * {@link GENERATOR_CONTEXT#getEventListeners()} is configured with a {@link BuiltInTypesListener} which is configured using each {@link CONFIGURATION#getMapUmlPropertyTypes()}'s sub Map's <code>"name"</code> and <code>"javaType"</code> keys<br/>
     * {@link GENERATOR_CONTEXT#getEventListeners()} is configured from {@link CONFIGURATION#getEventListeners()}<br/>
     *
     * @param generator_context The generator context to configure
     */
    protected void configureGeneratorContext(GENERATOR_CONTEXT generator_context) {
        generator_context.setCollectionTypeResolver(getDefaultCollectionTypeResolver());
        generator_context.setCollectionPropertyNameSuffix(configuration.getCollectionPropertyNameSuffix());
        generator_context.setAddDefaultConstructor(configuration.isAddDefaultConstructor());
        generator_context.setCreateExtensionClazzes(configuration.isGenerateExtensionClasses());
        generator_context.setAddExtensionAndBaseClazzAnnotations(configuration.isAddExtensionAndBaseClassAnnotations());

        // Built in types
        generator_context.addEventListeners(new BuiltInTypesListener() {
            @Override
            protected void resolveBuiltInTypes(ClazzOrInterface owner, Type type) {
                if (configuration.getMapUmlPropertyTypes() != null) {
                    for (Map<String, String> mapping : configuration.getMapUmlPropertyTypes()) {
                        if (mapping.get("name").equals(type.getName())) {
                            type.setName(mapping.get("javaType")).external();
                            return;
                        }
                    }
                }
                super.resolveBuiltInTypes(owner, type);
            }
        });

        addEventListeners(generator_context);
    }

    /**
     * The default {@link JavaCollectionTypeResolver} to use - default {@link JavaCollectionTypeResolver}
     *
     * @return The default {@link JavaCollectionTypeResolver} to use
     */
    protected JavaCollectionTypeResolver getDefaultCollectionTypeResolver() {
        return new JavaCollectionTypeResolver();
    }

    private void addEventListeners(GENERATOR_CONTEXT generator_context) {
        // Listeners
        for (EventListenerConfiguration eventListener : configuration.getEventListeners()) {
            try {
                GeneratorEventListener generatorEventListener = loadExtension(new ExtensionConfiguration(eventListener.getListener(), eventListener.getConfiguration()));
                logger.info("Loaded and configuration EventListener '" + generatorEventListener.getClass().getName() + " from Yaml specification '"
                        + eventListener + "'");
                generator_context.addEventListeners(generatorEventListener);
            } catch (Exception e) {
                logger.error("Failed to load EventListener '" + eventListener + "'", e);
            }
        }
    }

    /**
     * Resolve the {@link XmiReader} to be used by {@link #parseMetaModel()} <p/>
     * Currently supports the following values for {@link CONFIGURATION#getUmlTool()}:
     * <ul>
     * <li>EA          -> {@link EAXmiReader}</li>
     * <li>MagicDraw16 -> {@link MagicDraw16XmiReader}</li>
     * <li>MagicDraw17 -> {@link MagicDraw17XmiReader}</li>
     * <li>All other cases are deferred to resolving using {@link #loadExtension(ExtensionConfiguration)}</li>
     * </ul>
     *
     * @return The {@link XmiReader} to use according to the {@link CONFIGURATION}
     */
    protected XmiReader resolveXmiReader() {
        String umlTool = configuration.getUmlTool();
        if (umlTool == null || "".equals(umlTool)) {
            throw new IllegalArgumentException(
                    "You need to provide a 'umlTool' value - Possible values are MagicDraw, EA, .groovy scriptfile (containing an XmiReader compatible class definition) or a Fully Qualified Class Name (FQCN)");
        }
        if ("EA".equalsIgnoreCase(umlTool)) {
            return new EAXmiReader();
        } else if ("MagicDraw16".equalsIgnoreCase(umlTool)) {
            return new MagicDraw16XmiReader();
        } else if ("MagicDraw17".equalsIgnoreCase(umlTool)) {
            return new MagicDraw17XmiReader();
        } else {
            return loadExtension(new ExtensionConfiguration(umlTool));
        }
    }

    @Override
    public <T> T loadExtension(ExtensionConfiguration extensionConfiguration) {
        T extension = null;
        if (extensionConfiguration.getExtension().endsWith(".groovy")) {
            extension = instantiateGroovyClassFromScriptFile(extensionConfiguration.getExtension());
        } else {
            extension = instantiateClass(extensionConfiguration.getExtension());
        }
        logger.debug("Loaded extension '" + extensionConfiguration.getExtension() + "'");
        // Set properties on the listener
        for (Map.Entry<String, Object> propertyEntry : extensionConfiguration.getProperties().entrySet()) {
            if (ReflectUtils.hasSetterProperty(extension.getClass(), propertyEntry.getKey())) {
                try {
                    ReflectUtils.setPropertyValue(extension, propertyEntry.getKey(), propertyEntry.getValue());
                    logger.trace("Property '" + propertyEntry.getKey() + "' configured to value '" + propertyEntry.getValue() + "' on '" + extensionConfiguration.getExtension() + "'");
                } catch (Exception e) {
                    logger.error("Failed to set property '" + propertyEntry.getKey() + "' on extension '" + extension.getClass() + "'", e);
                }
            } else {
                logger.error("Can't set property '" + propertyEntry.getKey() + "' on extension '" + extension.getClass() + "' as it doesn't exist");
            }
        }


        return extension;
    }

    @Override
    public <T> Class<T> loadExtensionClass(String extensionDefinition) {
        Class<T> extensionClass = null;
        if (extensionDefinition.endsWith(".groovy")) {
            extensionClass = loadGroovyClass(extensionDefinition);
        } else {
            extensionClass = loadClass(extensionDefinition);
        }
        return extensionClass;
    }

    /**
     * Resolve a relative path to an absolute path but preappending the {@link #getBaseDir()}'s absolute path
     *
     * @param relativePath The relative path (may start with an optional <code>/</code>)
     * @return the absolute path
     */
    protected String toAbsolutePath(String relativePath) {
        return AbstractConfigurationBasedCodeGenerator.toAbsolutePath(getBaseDir(), relativePath);
    }

    @Override
    public <T> Class<T> loadClass(String fqcn) {
        fqcn = fqcn.trim();
        try {
            @SuppressWarnings("unchecked")
            Class<T> classLoaded = (Class<T>) getClassLoader().loadClass(fqcn);
            logger.debug("Loaded class from classpath: '" + fqcn + "'");
            return classLoaded;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Couldn't find class '" + fqcn + "'", e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> Class<T> loadGroovyClass(String groovyScriptFilePath) {
        groovyScriptFilePath = groovyScriptFilePath.trim();
        if (groovyClassLoader == null) {
            groovyClassLoader = new GroovyClassLoader(getClassLoader());
        }
        try {
            Class<T> classLoaded = groovyClassLoader.parseClass(new File(groovyScriptFilePath));
            logger.debug("Loaded Groovy class '" + classLoaded.getName() + "' from Groovy script file '" + groovyScriptFilePath + "'");
            return classLoaded;
        } catch (CompilationFailedException e) {
            throw new RuntimeException("Failed to compile Groovy file '" + groovyScriptFilePath + "'", e);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read Groovy file '" + groovyScriptFilePath + "'", e);
        }
    }

    @Override
    public ClassLoader getClassLoader() {
        if (classLoader == null)
            return Thread.currentThread().getContextClassLoader();
        else
            return classLoader;
    }


    @Override
    public <T> T instantiateGroovyClassFromScriptFile(String relativeScriptPath) {
        String scriptPath = toAbsolutePath(relativeScriptPath.trim());
        Class<? extends T> classLoaded = null;
        try {
            classLoaded = loadGroovyClass(scriptPath);
            return (T) classLoaded.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Failed to load Groovy class from '" + scriptPath + "' " + e.getMessage(), e);
//					"Failed to load Groovy class from '" + scriptPath + "' ." + classLoaded != null ? " Class loaded from script file: '"
//							+ classLoaded.getName() + "'" : "", e); Nullpointer
        }
    }

    @Override
    public <T> T instantiateClass(String fqcn) {
        Class<? extends T> classLoaded = null;
        try {
            classLoaded = loadClass(fqcn);
            return (T) classLoaded.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Failed to instantiate class from FQCN '" + fqcn + "'."
                    + (classLoaded != null ? " Class loaded: '" + classLoaded.getName() + "'" : ""), e);
        }
    }

    /**
     * The configuration used for this configuration reader
     */
    public CONFIGURATION getConfiguration() {
        return configuration;
    }

    @Override
    public File getBaseDir() {
        return baseDir;
    }

    @Override
    public File getBaseOutputDir() {
        return baseOutputDir;
    }

    /**
     * Resolve a relative path to an absolute path but preappending the baseDir's absolute path<br/>
     * <b>Note: It is assumed that the path <b>is relative</b> - so if you specify an aboslute path the result will be wrong!
     *
     * @param baseDir      the Base dir to use for setting the absolute path
     * @param relativePath The relative path (may start with an optional <code>/</code>)
     * @return the absolute path
     */
    public static String toAbsolutePath(File baseDir, String relativePath) {
        String path = baseDir.getAbsolutePath();
        if (!relativePath.startsWith(dirSeperator.toString())) {
            path = path + dirSeperator;
        }
        path = path + relativePath;
        return path;
    }


}
