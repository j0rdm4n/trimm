/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.configuration;

/**
 * Command line configuration setting for Eclipse source code formatter
 *
 * @author Lasse Cramon
 */
public class EclipseFormatterConfig {

    private String eclipsePath;
    private String javaVmPath;
    private String configFilePath;

    // For my YAML hommies
    protected EclipseFormatterConfig() {
    }

    public EclipseFormatterConfig(String eclipsePath, String javaVmPath, String configFilePath) {
        super();
        this.eclipsePath = eclipsePath;
        this.javaVmPath = javaVmPath;
        this.configFilePath = configFilePath;
    }

    public String getEclipsePath() {
        return eclipsePath;
    }

    public void setEclipsePath(String eclipsePath) {
        this.eclipsePath = eclipsePath;
    }

    public String getJavaVmPath() {
        return javaVmPath;
    }

    public void setJavaVmPath(String javaVmPath) {
        this.javaVmPath = javaVmPath;
    }

    public String getConfigFilePath() {
        return configFilePath;
    }

    public void setConfigFilePath(String configFilePath) {
        this.configFilePath = configFilePath;
    }

}
