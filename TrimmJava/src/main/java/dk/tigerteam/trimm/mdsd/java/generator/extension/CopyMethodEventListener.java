/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.util.MdsdJavaUtil;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import dk.tigerteam.trimm.util.Utils;

import java.util.Collections;
import java.util.List;

/**
 * Adds copy method to class that copy all properties and calls super.copy if in hierarchy.
 * <p/>
 * <pre>
 * public class Brugerformular {
 *  private java.util.Date gyldigTil;
 *
 *
 *  public Brugerformular getCopy() {
 *      super.copy();
 *      Brugerformular brugerformular = new Brugerformular();
 *      brugerformular.setGyldigTil(getGyldigTil());
 *      return brugerformular;
 *  }
 *  ....
 * }
 * </pre>
 *
 * @author Lasse Cramon
 */
public class CopyMethodEventListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

    @Override
    public void generationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
        for (ClazzOrInterface clazzOrInterface : allGeneratedClazzes) {
            if (clazzOrInterface.isClazz() && !clazzOrInterface.isTestClazz()) {
                createCopyMethod((Clazz) clazzOrInterface);
            }
        }
    }

    private void createCopyMethod(Clazz clazz) {
    	if(!clazz.isBaseClazz()) {
    		List<Property> properties = clazz.getAllPropertiesIncludingSuperClazzProperties();
    		List<Property> superClazzProperties = clazz.getAllSuperClazzProperties();
    		String methodName = "getCopy";
    		boolean isGetCopyGenerated = false;
    		if (clazz.getMethod("getCopy", Collections.<Parameter> emptyList()) != null) {
    			methodName = "getCopy_Generated";
    			isGetCopyGenerated = true;
    		}
        Method method = new Method(methodName, true, AccessModifier.Public);
        if (clazz.isBaseClazz()) {
            method.setReturnType(new Type(JavaGeneratorContext.getContext().getExtensionClazzForBaseClazz(clazz)));
        } else {
            method.setReturnType(new Type(clazz));
        }

//        if (clazz.extendsAGeneratedClazz() && clazz.isExtensionClazz() && (clazz.getExtendsType().getClazz().hasMethod("getCopy", Collections.<Parameter>emptyList()) ||
//        		clazz.getExtendsType().getClazz().hasMethod("getCopy_Generated", Collections.<Parameter>emptyList()))) {
//        		if(isGetCopyGenerated) {
//        			method.addCode(new CodeSnippet("return super.getCopy_Generated();"));
//        		} else {
//        			method.addCode(new CodeSnippet("return super.getCopy();"));
//        		}
//            clazz.addChildren(method);
//            return;
//        }

        
        String clazzName = Utils.uncapitalize(clazz.getName());
        if (clazz.isBaseClazz()) {
            clazzName = Utils.uncapitalize(JavaGeneratorContext.getContext().getExtensionClazzForBaseClazz(clazz).getName());
        }
        FQCN fqcn = clazz.asFQCN();
//        if (clazz.isBaseClazz()) {
//            fqcn = JavaGeneratorContext.getContext().getExtensionClazzForBaseClazz(clazz).asFQCN();
//        }
        
        method.addCode(new CodeSnippet(fqcn, " ", clazzName, " = new ", fqcn, "();"));
        
        for (Property property : properties) {
            if (property.getField() != null) {
                if (property.getField().getAccessModifiers().contains(AccessModifier.Static)) {
                    continue;
                }
                boolean isSuperClazzProperty = isSuperClazzProperty(superClazzProperties, property);
                if (property.getMetaType().getCardinalityType() == CardinalityType.Many) {
                    if (MdsdJavaUtil.isPropertyPrimitive(property)) {
                    	if(isSuperClazzProperty) {
                    		method.addCode(new CodeSnippet(clazzName, ".", property.getGetterMethod().getName(), "().addAll(", property.getGetterMethod().getName(), "()", ");"));
                    	} else {
                        method.addCode(new CodeSnippet(clazzName, ".", property.getGetterMethod().getName(), "().addAll(", property.getName(), ");"));
                    	}
                    } else {
                    	if(isSuperClazzProperty) {
                        method.addCode(new CodeSnippet("for(", property.getField().getType().getGenerics().get(0), " ", "element", " : ", property.getGetterMethod().getName(), "()", ") ", "{",
                                clazzName, ".", property.getGetterMethod().getName(), "().add(",
                                property.getField().getType().getGenerics().get(0).isExternal() ? "element" : isGetCopyGenerated ? "element.getCopy_Generated()" : "element.getCopy()", ");", "}"));
                    	} else {
                    		method.addCode(new CodeSnippet("for(", property.getField().getType().getGenerics().get(0), " ", "element", " : ", property.getName(), ") ", "{",
                            clazzName, ".", property.getGetterMethod().getName(), "().add(",
                            property.getField().getType().getGenerics().get(0).isExternal() ? "element" : isGetCopyGenerated ? "element.getCopy_Generated()" : "element.getCopy()", ");", "}"));
                    	}
                    }
                } else {
                    if (property.getSetterMethod() == null) {
                        continue;
                    }
                    if (MdsdJavaUtil.isPropertyPrimitive(property) || isExternalType(property)) {
                    		if(isSuperClazzProperty) {
                    			method.addCode(new CodeSnippet(clazzName, ".", property.getSetterMethod().getName(), "(", property.getGetterMethod().getName(), "()", ");"));
                    		} else {
                    			method.addCode(new CodeSnippet(clazzName, ".", property.getSetterMethod().getName(), "(", property.getName(), ");"));
                    		}
                    } else {
                    	if(isSuperClazzProperty) {
                    		method.addCode(new CodeSnippet(clazzName, ".", property.getSetterMethod().getName(), "(", property.getGetterMethod().getName(), "()", ".getCopy()", ");"));
                    	} else {
                        method.addCode(new CodeSnippet(clazzName, ".", property.getSetterMethod().getName(), "(", property.getName(), isGetCopyGenerated ? ".getCopy_Generated()" : ".getCopy()", ");"));
                    	}
                    }
                }
            } else {
                if (property.getAccessModifiers().contains(AccessModifier.Static)) {
                    continue;
                }
                method.addCode(new CodeSnippet(clazzName, ".", property.getName(), "= ", property.getName()));
            }
        }
        method.addCode(new CodeSnippet("return ", clazzName, ";"));
        clazz.addChildren(method);
    	}
    }

    private boolean isExternalType(Property property) {
        if (property.getField().getType().isExternal() || property.getField().getType().getWrappedJavaClass() != null) {
            return true;
        }
        return false;
    }
    
    private boolean isSuperClazzProperty(List<Property> superClazzProperties, Property p) {
    	for (Property property : superClazzProperties) {
				if(MdsdJavaUtil.isPropertyNameEqual(property, p)) {
					return true;
				}
			}
    	return false;
    }

}
