/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

/**
 * A FQCN allows you to export a {@link Clazz}'s Fully Qualified Class Name (FQCN) in (for instance) a {@link CodeSnippet}.<br/>
 * The FQCN can be returned by calling {@link Clazz#asFQCN()} or {@link Clazz#getFQCN()}. The difference between these two calls is that
 * {@link Clazz#getFQCN()} is resolved at calltime, whereas {@link Clazz#asFQCN()}, codegeneration wise, is resolved <b>after</b> package
 * rewriting has occurred.
 * <p/>
 * Example: <code>new CodeSnippet(clazz.asFQCN(), " ", instanceVarName, " = new ", clazz.asFQCN(),  "();");</code>
 *
 * @author Jeppe Cramon
 * @see dk.tigerteam.trimm.mdsd.java.generator.codewriter.PackageRewriter
 * @see Clazz#asFQCN()
 * @see Clazz#getFQCN()
 */
public class FQCN extends CodeElement {

    private ClazzOrInterface clazzOrInterface;
    private boolean generateQuoted;
    private boolean overrideImports;

    public FQCN(ClazzOrInterface clazz) {
        this.clazzOrInterface = clazz;
    }

    public FQCN(ClazzOrInterface clazz, boolean generatedQuoted) {
        this.clazzOrInterface = clazz;
        this.generateQuoted = generatedQuoted;
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		if (generateQuoted) {
//			writer.print(Utils.quotedString(clazzOrInterface.getFQCN()));
//		} else {
//			writer.print(clazzOrInterface.getFQCN());
//		}
//	}

    public ClazzOrInterface getClazzOrInterface() {
        return clazzOrInterface;
    }

    public boolean isGenerateQuoted() {
        return generateQuoted;
    }

    public boolean isOverrideImports() {
        return overrideImports;
    }

    public FQCN withOverrideImports(boolean overrideImports) {
        this.overrideImports = overrideImports;
        return this;
    }

    @Override
    public String toString() {
        return clazzOrInterface.getName();
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
