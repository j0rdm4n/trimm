/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;

import java.util.List;

/**
 * Helper class, which splits {@link #handle(GeneratorEvent)} into {@link GeneratorEvent} specific handler methods.<p/>
 * The general rule is a {@link GeneratorEvent} is delegated to a method with the name handle{typeof GeneratorEvent}(typeof GeneratorEvent).<br/>
 * Example:<br/>
 * {@link ClazzEvent} is delegated to {@link #handleClazzEvent(ClazzEvent)}
 * and
 * {@link SimplePropertyEvent} is delegated to {@link #handleSimplePropertyEvent(SimplePropertyEvent)}
 *
 * @author Jeppe Cramon
 */
public class AbstractGeneratorEventListener implements GeneratorEventListener {

    public AbstractGeneratorEventListener() {
    }

    public void generationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
    }

    public void generationCommenced() {
    }

    public boolean handle(GeneratorEvent event) {
        if (event instanceof ClazzEvent) {
            return handleClazzEvent((ClazzEvent) event);
        } else if (event instanceof InterfaceEvent) {
            return handleInterfaceEvent((InterfaceEvent) event);
        } else if (event instanceof EnumeratorLiteralEvent) {
            return handleEnumeratorEvent((EnumeratorLiteralEvent) event);
        } else if (event instanceof SimplePropertyEvent) {
            return handleSimplePropertyEvent((SimplePropertyEvent) event);
        } else if (event instanceof OneToOneAssociationEvent) {
            return handleOneToOneAssociationEvent((OneToOneAssociationEvent) event);
        } else if (event instanceof OneToManyAssociationEvent) {
            return handleOneToManyAssociationEvent((OneToManyAssociationEvent) event);
        } else if (event instanceof ManyToOneAssociationEvent) {
            return handleManyToOneAssociationEvent((ManyToOneAssociationEvent) event);
        } else if (event instanceof ManyToManyAssociationEvent) {
            return handleManyToManyAssociationEvent((ManyToManyAssociationEvent) event);
        } else if (event instanceof MethodEvent) {
            return handleMethodEvent((MethodEvent) event);
        } else {
            throw new IllegalArgumentException("Unknown GeneratorEvent " + event.getClass());
        }
    }

    protected boolean handleMethodEvent(MethodEvent event) {
        return true;
    }

    protected boolean handleInterfaceEvent(InterfaceEvent event) {
        return true;
    }

    protected boolean handleManyToManyAssociationEvent(
            ManyToManyAssociationEvent event) {
        return true;
    }

    protected boolean handleManyToOneAssociationEvent(
            ManyToOneAssociationEvent event) {
        return true;
    }

    protected boolean handleOneToManyAssociationEvent(
            OneToManyAssociationEvent event) {
        return true;
    }

    protected boolean handleOneToOneAssociationEvent(
            OneToOneAssociationEvent event) {
        return true;
    }

    protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
        return true;
    }

    protected boolean handleEnumeratorEvent(EnumeratorLiteralEvent literalEvent) {
        return true;
    }

    protected boolean handleClazzEvent(ClazzEvent event) {
        return true;
    }

    /**
     * Sugar method for {@link JavaGeneratorContext#getContext()}
     */
    protected JavaGeneratorContext getContext() {
        return JavaGeneratorContext.getContext();
    }

    /**
     * Sugar method for {@link JavaGeneratorContext#getContext(Class)}
     */
    protected <T extends JavaGeneratorContext> T getContext(Class<T> contextType) {
        try {
            return JavaGeneratorContext.getContext(contextType);
        } catch (IllegalStateException e) {
            throw new IllegalStateException("You need to use a " + contextType.getName() + " class (or a subclass thereof) together with " + this.getClass().getName());
        }
    }
}
