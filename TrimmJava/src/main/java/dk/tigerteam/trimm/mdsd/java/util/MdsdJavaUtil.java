/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.util;

import dk.tigerteam.trimm.mdsd.java.codedom.Property;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.AssociationType;
import dk.tigerteam.trimm.util.ObjectUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Various small helper/utility methods
 *
 * @author Lasse Cramon
 */
public class MdsdJavaUtil {

    public static void sortPropertiesAlphabetical(List<Property> properties) {
        Collections.sort(properties, alphabetical);
    }

    private static Comparator<Property> alphabetical = new Comparator<Property>() {
        public int compare(Property o1, Property o2) {
            return o1.getName().compareTo(o2.getName());
        }
    };

    public static String metaPropertiesAsQuotedCommaSeparatedString(List<MetaProperty> properties) {
        StringBuilder result = new StringBuilder();
        for (MetaProperty property : properties) {
            if (result.length() > 0) {
                result.append(", ");
            }
            result.append("\"");
            result.append(property.getName());
            result.append("\"");
        }
        return result.toString();
    }

    public static String propertiesAsQuotedCommaSeparatedString(List<Property> properties) {
        StringBuilder result = new StringBuilder();
        for (Property property : properties) {
            if (result.length() > 0) {
                result.append(", ");
            }
            result.append("\"");
            result.append(property.getName());
            result.append("\"");
        }
        return result.toString();
    }


    /**
     * Write out the property names comma separated
     *
     * @param properties
     * @return
     */
    public static String propertiesAsCommaSeparatedString(List<Property> properties) {
        StringBuilder result = new StringBuilder();
        for (Property property : properties) {
            if (result.length() > 0) {
                result.append(", ");
            }
            result.append(property.getName());
        }
        return result.toString();
    }

    public static boolean isPropertyCollection(Property property) {
        if (property.getMetaType() != null
                && property.getMetaType().isPartInAnAssociation()
                && (property.getMetaType().getAssociationType() == AssociationType.ManyToMany || property.getMetaType().getAssociationType() == AssociationType.OneToMany)) {
            return true;
        }
        return false;
    }

    public static boolean isPropertyPrimitive(Property property) {
        if (property.getMetaType() != null && property.getField().getType().isBuiltInType()) {
            return true;
        }
        return false;
    }
    
    public static boolean isPropertyNameEqual(Property p1, Property p2) {
      return ObjectUtil.equals(p1.getName(), p2.getName());
    }

}
