/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.codewriter;

import de.hunsicker.jalopy.Jalopy;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaCodeGenerationPaths;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaConfiguration;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.GeneratorSummary.UpdateType;
import dk.tigerteam.trimm.util.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * {@link JavaCodeWriter} which uses <b>Jalopy</b> to format the source files.
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class JalopyFormattingCodeWriter extends JavaCodeWriter {

    private static final Log logger = LogFactory.getLog(JalopyFormattingCodeWriter.class);
    private Jalopy jalopy;

    public JalopyFormattingCodeWriter(JavaCodeGenerationPaths javaCodeGenerationPaths, PackageRewriter packageRewriter, String rootDirectory, String... excludeFormattingForPackages) {
        super(javaCodeGenerationPaths, packageRewriter, rootDirectory, Arrays.asList(excludeFormattingForPackages));
    }

    public JalopyFormattingCodeWriter(JavaCodeGenerationPaths javaCodeGenerationPaths, PackageRewriter packageRewriter, String rootDirectory,
                                      Collection<String> excludeFormattingForPackages) {
        super(javaCodeGenerationPaths, packageRewriter, rootDirectory, excludeFormattingForPackages);
    }

    @Override
    public List<File> writeCode(Collection<ClazzOrInterface> clazzesToWrite) throws IOException {
        jalopy = new Jalopy();
        logger.debug("Writing '" + clazzesToWrite.size() + "' artifacts with code writer '" +
                this.getClass().getSimpleName() + "' using file extension '" + getFileExtension() + "'");
        getSummary().setNumberOfGeneratedArtifacts(clazzesToWrite.size());
        // Ensure that all packages have been rewritten prior to writing out ANY code!
        rewritePackageNames(clazzesToWrite);

        List<File> generatedFiles = new LinkedList<File>();

        for (ClazzOrInterface clazz : clazzesToWrite) {
            if (!shouldGenerateSourceFileForClazz(clazz)) {
                // Don't generate
                logger.trace("DontGenerate stereotype found for clazz '" + clazz + "'");
                getSummary().addToNumberOfDontGenerateClasses();
                continue;
            }
            String fqcn = clazz.getFQCN();
            String fqcnPath = generateFqcnPath(fqcn, getFileExtension());

            String destinationFolder = resolveDestionationFolder(clazz);

            File destinationFolderFile = createDestinationFolder(destinationFolder);

            String fullPath = createPackageFolders(fqcnPath, destinationFolderFile);

            File clazzFile = new File(fullPath);
            if (clazzFile.exists()) {
                updateFile(clazzFile, clazz, generatedFiles);
            } else {
                logger.debug("Adding artifact '" + clazz.getName() + "' to new file '" + clazzFile.getAbsolutePath() + "'");
                writeFile(clazz, clazzFile);
                if (canFormat(fqcn)) {
                    jalopyformat(clazz, clazzFile);
                }
                updateSummary(clazz, UpdateType.New);
                generatedFiles.add(clazzFile);
            }
        }
        return generatedFiles;
    }

    private boolean canFormat(String fqcn) {
        if (!getExcludeFormattingForPackages().contains(stripClassName(fqcn))) {
            return true;
        }
        return false;
    }

    private void jalopyformat(ClazzOrInterface clazz, File clazzFile) throws FileNotFoundException {
        jalopy.setInput(clazzFile);
        jalopy.setOutput(clazzFile);
        jalopy.setEncoding(JavaConfiguration.DEFAULT_ENCODING);
        try {
            if (!jalopy.format()) {
                System.out.println("ERROR: Jalopy formatting failed for Clazz " + clazz + " in file '" + clazzFile.getAbsolutePath() + "'");
            }
        } catch (Exception e) {
            System.out.println("ERROR: Jalopy formatting failed for Clazz " + clazz + " in file '" + clazzFile.getAbsolutePath() + "'");
        }
        if (jalopy.getState() == Jalopy.State.ERROR) {
            System.out.println("ERROR: Error formatting '" + clazzFile.getAbsolutePath() + "'");
        } else if (jalopy.getState() == Jalopy.State.WARN) {
            System.out.println("WARNING: Warnings while formatting '" + clazzFile.getAbsolutePath() + "'");
        }
    }

    private String stripClassName(String fqcn) {
        int index = fqcn.lastIndexOf(".");
        if (index > 0) {
            return fqcn.substring(0, index);
        }
        return fqcn;
    }

    @Override
    protected void updateFile(File clazzFile, ClazzOrInterface clazz, List<File> generatedFiles) throws IOException {
        if (isUpdatable(clazz)) {
            File temp = File.createTempFile("Temp" + clazz.getName(), ".temp");
            writeFile(clazz, temp);
            if (canFormat(clazz.getFQCN())) {
                jalopyformat(clazz, temp);
            }
            if (isFileContentEqual(clazzFile, temp)) {
                // No update
                Utils.deleteFile(temp);
                generatedFiles.add(clazzFile);
                updateSummary(clazz, UpdateType.NoUpdate);
                logger.debug("No update found for artifact '" + clazz.getName() + "' keeping existing file '" + clazzFile.getAbsolutePath() + "'");
            } else {
                // Update
                writeFile(clazz, clazzFile);
                if (canFormat(clazz.getFQCN())) {
                    jalopyformat(clazz, clazzFile);
                }
                generatedFiles.add(clazzFile);
                Utils.deleteFile(temp);
                updateSummary(clazz, UpdateType.Update);
                logger.debug("Update found for artifact '" + clazz.getName() + "' updating existing file '" + clazzFile.getAbsolutePath() + "'");
            }
        } else {
            logger.debug("Artifact '" + clazz.getName() + "' is not updatable keeping existing file '" + clazzFile.getAbsolutePath() + "'");
            updateSummary(clazz, UpdateType.NoUpdate);
            generatedFiles.add(clazzFile);
        }
    }


}
