/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.configuration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import dk.tigerteam.trimm.mdsd.BaseClazz;
import dk.tigerteam.trimm.mdsd.ExtensionClazz;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.JalopyFormattingCodeWriter;
import dk.tigerteam.trimm.mdsd.java.generator.event.GeneratorEventListener;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.NameableMetaType;
import dk.tigerteam.trimm.mdsd.runtime.RuntimeMetaModel;
import dk.tigerteam.trimm.mdsd.uml2.xmi.XmiReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Configuration for Java specialized Code generation
 *
 * @author Jeppe Cramon
 */
public class JavaConfiguration implements JavaCodeGenerationPaths {
    protected String xmiModelPath;
    protected String umlTool;
    protected String basePackageName = "";
    protected String rootSuperClass;
    protected String collectionPropertyNameSuffix;
    protected List<String> excludePackageRewritingForPackages;
    protected String stripPackagePrefix;
    protected boolean generateIsPropertyMethodForBooleanWrapperTypeProperties;
    protected boolean generateExtensionClasses;
    protected boolean addExtensionAndBaseClassAnnotations;
    protected SourceCodeFormatting sourceCodeFormatting;
    protected EclipseFormatterConfig eclipseFormatterConfig;
    protected String generateBaseClassesToPath = "";
    protected String generateExtensionClassesToPath = "";
    protected String generateInterfacesToPath = "";
    protected String generateTestClassesToPath = "";
    protected String generateResourcesToPath = "";
    protected List<EventListenerConfiguration> eventListeners = new ArrayList<EventListenerConfiguration>();
    protected RuntimeMetaModelSetup createRuntimeMetaModel;
    protected List<Map<String, String>> mapUmlPropertyTypes;
    protected boolean addDefaultConstructor;
    protected boolean forceOverwriteGeneratedExtensionClasses = false;
    protected boolean useAliasIfAvailable = false;
    protected String modelPreprocessingScript;
    protected boolean useImports = true;
    public static final String DEFAULT_ENCODING = "UTF-8";
    private String encoding = DEFAULT_ENCODING;

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    /**
     * Setting this value to true will ensure that BOTH a getter and is property accessor method will be generated
     * for property of type {@link Boolean} (boolean wrapper type). Otherwise (and default)
     * ONLY a getter property accessor method will be generated
     * @return
     */
    public boolean isGenerateIsPropertyMethodForBooleanWrapperTypeProperties() {
        return generateIsPropertyMethodForBooleanWrapperTypeProperties;
    }

    public void setGenerateIsPropertyMethodForBooleanWrapperTypeProperties(boolean generateIsPropertyMethodForBooleanWrapperTypeProperties) {
        this.generateIsPropertyMethodForBooleanWrapperTypeProperties = generateIsPropertyMethodForBooleanWrapperTypeProperties;
    }

    /**
     * Allows you to specify the relative path to a .groovy script that can preprocess the {@link MetaModel} before target specific (e.g. Java/JPA) Codegeneration begin
     */
    public String getModelPreprocessingScript() {
        return modelPreprocessingScript;
    }

    public void setModelPreprocessingScript(String modelPreprocessingScript) {
        this.modelPreprocessingScript = modelPreprocessingScript;
    }

    /**
     * When code calls {@link NameableMetaType#getAliasOrName()} should alias be used per default (if available)?
     */
    public boolean isUseAliasIfAvailable() {
        return useAliasIfAvailable;
    }

    public void setUseAliasIfAvailable(boolean useAliasIfAvailable) {
        this.useAliasIfAvailable = useAliasIfAvailable;
    }

    /**
     * Force the code generator to override Extension classes - Use with caution!!!!
     */
    public boolean isForceOverwriteGeneratedExtensionClasses() {
        return forceOverwriteGeneratedExtensionClasses;
    }

    public void setForceOverwriteGeneratedExtensionClasses(boolean forceOverwriteGeneratedExtensionClasses) {
        this.forceOverwriteGeneratedExtensionClasses = forceOverwriteGeneratedExtensionClasses;
    }

    /**
     * What suffix should be used for Collection property/field names - default is "Collection"
     */
    public String getCollectionPropertyNameSuffix() {
        return collectionPropertyNameSuffix;
    }

    public void setCollectionPropertyNameSuffix(String collectionPropertyNameSuffix) {
        this.collectionPropertyNameSuffix = collectionPropertyNameSuffix;
    }

    /**
     * Should a default no-arg constructor be inserted in the generated code?
     */
    public boolean isAddDefaultConstructor() {
        return addDefaultConstructor;
    }

    public void setAddDefaultConstructor(boolean addDefaultConstructor) {
        this.addDefaultConstructor = addDefaultConstructor;
    }

    /**
     * What UML tool is used to generate the Xmi file the contains our {@link MetaModel}?<br/>
     * - Supported values are "MagicDraw" and "EA" (for Enterprise Architect)<br/>
     * - If the value ends with ".groovy" it's assumed to be a relative path to a Groovy script (that must contain an implementation of {@link XmiReader}) which is loaded<br/>
     * - If the value doesn't end with ".groovy" it's assumed that the value is a Fully Qualified Class Name (FQCN) pointing to an {@link XmiReader} implementation on the classpath
     */
    public String getUmlTool() {
        return umlTool;
    }

    public void setUmlTool(String umlTool) {
        this.umlTool = umlTool;
    }

    public List<String> getExcludePackageRewritingForPackages() {
        return excludePackageRewritingForPackages;
    }

    public void setExcludePackageRewritingForPackages(List<String> excludePackageRewritingForPackages) {
        this.excludePackageRewritingForPackages = excludePackageRewritingForPackages;
    }

    /**
     * Relative path to the XMI file (e.g. Enterprise Architect XMI 2.1 export or a Magic Draw .MDZIP file)
     */
    public String getXmiModelPath() {
        return xmiModelPath;
    }

    public void setXmiModelPath(String xmiModelPath) {
        this.xmiModelPath = xmiModelPath;
    }

    /**
     * What base package name should be used (for Package rewriting)
     */
    public String getBasePackageName() {
        return basePackageName;
    }

    public void setBasePackageName(String basePackageName) {
        if (basePackageName == null) basePackageName = "";
        this.basePackageName = basePackageName;
    }

    /**
     * Should we generate Extension classes as well as base classes (aka 3 level inheritance/the generator gap) ?
     */
    public boolean isGenerateExtensionClasses() {
        return generateExtensionClasses;
    }

    public void setGenerateExtensionClasses(boolean generateExtensionClasses) {
        this.generateExtensionClasses = generateExtensionClasses;
    }

    /**
     * What sourcecode formatting should be used (if implementation isn't specified {@link JalopyFormattingCodeWriter} is used).<br/>
     * Exclusion of formatting can also be specified for several packages (and thereby also apply to subpackages of these)
     */
    public SourceCodeFormatting getSourceCodeFormatting() {
        return sourceCodeFormatting;
    }

    public void setSourceCodeFormatting(SourceCodeFormatting sourceCodeFormatting) {
        this.sourceCodeFormatting = sourceCodeFormatting;
    }

    /**
     * Where should base classes (or incase Extension classes aren't generated - all classes) be placed?<br/>
     * Should be to a folder not placed under source control (e.g. <code>target/generate-sources/some-subfolder</code> for maven)<p/>
     * Note: relative path should be specified
     */
    public String getGenerateBaseClassesToPath() {
        return generateBaseClassesToPath;
    }

    public void setGenerateBaseClassesToPath(String generateBaseClassesToPath) {
        this.generateBaseClassesToPath = generateBaseClassesToPath;
    }

    /**
     * Where should Extension classes be placed?<br/>
     * Should be a sourcefolder that is placed under version control (e.g. <code>src/main/extensions</code> under maven)<p/>
     * Note: relative path should be specified
     */
    public String getGenerateExtensionClassesToPath() {
        return generateExtensionClassesToPath;
    }

    public void setGenerateExtensionClassesToPath(
            String generateExtensionClassesToPath) {
        this.generateExtensionClassesToPath = generateExtensionClassesToPath;
    }

    /**
     * Where should interfaces be placed?<br/>
     * Should be to a folder not placed under source control (e.g. <code>target/generate-sources/some-subfolder</code> for maven)<p/>
     * Note: relative path should be specified
     */
    public String getGenerateInterfacesToPath() {
        return generateInterfacesToPath;
    }

    public void setGenerateInterfacesToPath(String generateInterfacesToPath) {
        this.generateInterfacesToPath = generateInterfacesToPath;
    }

    /**
     * Where should test classes be placed?<br/>
     * Should be to a folder not placed under source control (e.g. <code>target/generate-sources/some-test-subfolder</code> for maven)<p/>
     * Note: relative path should be specified
     */
    public String getGenerateTestClassesToPath() {
        return generateTestClassesToPath;
    }

    public void setGenerateTestClassesToPath(String generateTestClassesToPath) {
        this.generateTestClassesToPath = generateTestClassesToPath;
    }

    /**
     * Should a {@link RuntimeMetaModel} be generated?
     *
     * @return
     */
    public RuntimeMetaModelSetup getCreateRuntimeMetaModel() {
        return createRuntimeMetaModel;
    }

    public void setCreateRuntimeMetaModel(
            RuntimeMetaModelSetup createRuntimeMetaModel) {
        this.createRuntimeMetaModel = createRuntimeMetaModel;
    }


    /**
     * What {@link GeneratorEventListener}'s should be used?<p/>
     * If the listener name is a relative path to a file that ends with ".groovy" then it's assumed that this file contains a Groovy implementation of the {@link GeneratorEventListener} interface<br/>
     * Otherwise it's assumed that name is a Fully Qualified Class Name (FQCN) for a class the implements the {@link GeneratorEventListener} interface. This class will be loaded from the classpath
     */
    public List<EventListenerConfiguration> getEventListeners() {
        return eventListeners;
    }

    public void setEventListeners(List<EventListenerConfiguration> eventListeners) {
        if (eventListeners == null) this.eventListeners.clear();
        if (eventListeners != null) {
            this.eventListeners.addAll(eventListeners);
        }
    }

    /**
     * Specify mappings from a simple type in UML to a certain Java type (e.g. in UML we write "DateTime", but we mean to used Joda's DateTime class)<p/>
     * <i>Example YAML configuration:</i>
     * <pre>
     * mapUmlPropertyTypes:
     *     - name: DateTime
     *       javaType: org.joda.time.DateTime
     *       jpaTypeConverterClass: dk.tigerteam.mddexample.hibernate.PersistentDateTime
     *     - name: LocalDate
     *       javaType: org.joda.time.LocalDate
     *       jpaTypeConverterClass: dk.tigerteam.mddexample.hibernate.PersistentLocalDate
     * </pre>
     * Note: The jpaTypeConverterClass is only valid in a JPA code generation setup
     */
    public List<Map<String, String>> getMapUmlPropertyTypes() {
        return mapUmlPropertyTypes;
    }

    public void setMapUmlPropertyTypes(MapUmlPropertyTypesBuilder builder) {
        mapUmlPropertyTypes = new ArrayList(builder.getMappingList());
    }


    public void setMapUmlPropertyTypes(
            List<Map<String, String>> mapUmlPropertyTypes) {
        this.mapUmlPropertyTypes = mapUmlPropertyTypes;
    }

    public String getGenerateResourcesToPath() {
        return generateResourcesToPath;
    }

    /**
     * Where should resources (i.e. plain files) be placed?<br/>
     * Should be to a folder not placed under source control (e.g. <code>target/generate-sources/some-resources-subfolder</code> for maven)<p/>
     * Note: relative path should be specified
     */
    public void setGenerateResourcesToPath(String generateResourcesToPath) {
        this.generateResourcesToPath = generateResourcesToPath;
    }

    /**
     * If a package starts with this prefix, then the prefix is stripped from the package before code generation is done
     */
    public String getStripPackagePrefix() {
        return stripPackagePrefix;
    }

    public void setStripPackagePrefix(String stripPackagePrefix) {
        this.stripPackagePrefix = stripPackagePrefix;
    }

    /**
     * In case {@link #isGenerateExtensionClasses()} is true, should we add {@link ExtensionClazz} and {@link BaseClazz} annotations to the generated classes?
     */
    public boolean isAddExtensionAndBaseClassAnnotations() {
        return addExtensionAndBaseClassAnnotations;
    }

    public void setAddExtensionAndBaseClassAnnotations(boolean addExtensionAndBaseClassAnnotations) {
        this.addExtensionAndBaseClassAnnotations = addExtensionAndBaseClassAnnotations;
    }

    /**
     * Fully Qualified Class Name (FQCN) for the topmost Class in all generated hierarchies
     */
    public String getRootSuperClass() {
        return rootSuperClass;
    }

    public void setRootSuperClass(String rootSuperClass) {
        this.rootSuperClass = rootSuperClass;
    }

    public EclipseFormatterConfig getEclipseFormatterConfig() {
        return eclipseFormatterConfig;
    }

    public void setEclipseFormatterConfig(EclipseFormatterConfig eclipseFormatterConfig) {
        this.eclipseFormatterConfig = eclipseFormatterConfig;
    }

    public boolean isUseImports() {
        return useImports;
    }

    public void setUseImports(boolean useImports) {
        this.useImports = useImports;
    }

    @Override
    public List<String> getGenerateToPaths() {
        List<String> paths = new ArrayList<String>();
        if (generateBaseClassesToPath != null) {
            paths.add(generateBaseClassesToPath);
        }
        if (generateExtensionClassesToPath != null && generateExtensionClasses) {
            paths.add(generateExtensionClassesToPath);
        }
        if (generateInterfacesToPath != null) {
            paths.add(generateInterfacesToPath);
        }
        if (generateTestClassesToPath != null) {
            paths.add(generateTestClassesToPath);
        }
        if (generateResourcesToPath != null) {
            paths.add(generateResourcesToPath);
        }
        return paths;
    }

    /**
     * Simple Java based build for constructing a mapping similar to this:
     * <pre>
     * mapUmlPropertyTypes:
     *      - name: DateTime
     *      javaType: org.joda.time.DateTime
     *      xxx: yyyyyy
     *      - name: LocalDate
     *      javaType: org.joda.time.LocalDate
     * </pre>
     * The result of running this builders different <code>map</code> methods will be a List of Map's that contains
     * keys understood by the JavaConfiguration and generator.
     * @see #UML_TYPE_NAME_KEY
     * @see #JAVA_TYPE_KEY
     */
    public static class MapUmlPropertyTypesBuilder {
        public static final String UML_TYPE_NAME_KEY = "name";
        public static final String JAVA_TYPE_KEY = "javaType";
        private List<Map<String, String>> mappingList = Lists.newArrayList();

        /**
         * Create a mapping between a TypeName used in the UML/Metamodel and the Java class that replaces it in the generated code
         * @param umlTypeName the type name used in the UML/metamodel. This is represented in the result Map under the key {@link #UML_TYPE_NAME_KEY}
         * @param javaClass The Java class that should be used in the generated code. This is represented in the result Map under the key {@link #JAVA_TYPE_KEY}
         * @return This builder instance
         */
        public MapUmlPropertyTypesBuilder map(String umlTypeName, Class<?> javaClass) {
           return map(umlTypeName, javaClass.getName());
        }

        /**
         * Create a mapping between a TypeName used in the UML/Metamodel and the Java class that replaces it in the generated code
         * @param umlTypeName the type name used in the UML/metamodel. This is represented in the result Map under the key {@link #UML_TYPE_NAME_KEY}
         * @param javaClassAsFQCN The Java class that should be used in the generated code as a Fully Qualified Class Name (FQCN).
         *                        This is represented in the result Map under the key {@link #JAVA_TYPE_KEY}
         * @return This builder instance
         */
        public MapUmlPropertyTypesBuilder map(String umlTypeName, String javaClassAsFQCN) {
            return map(umlTypeName, javaClassAsFQCN, null);
        }

        /**
         * Create a mapping between a TypeName used in the UML/Metamodel and the Java class that replaces it in the generated code
         * @param umlTypeName the type name used in the UML/metamodel. This is represented in the result Map under the key {@link #UML_TYPE_NAME_KEY}
         * @param javaClassAsFQCN The Java class that should be used in the generated code as a Fully Qualified Class Name (FQCN).
         *                        This is represented in the result Map under the key {@link #JAVA_TYPE_KEY}
         * @param keyValuePairs The additional key value pairs that should be added. The array is interpreted as {key,value, key, value}
         * @return This builder instance
         */
        public MapUmlPropertyTypesBuilder map(String umlTypeName, String javaClassAsFQCN, String...keyValuePairs) {
            Map<String, String> mapping = Maps.newHashMap();
            mapping.put(UML_TYPE_NAME_KEY, umlTypeName);
            mapping.put(JAVA_TYPE_KEY, javaClassAsFQCN);
            if (keyValuePairs != null) {
                if (keyValuePairs.length % 2 != 0) {
                    throw new IllegalArgumentException("Found " + keyValuePairs.length + " keyValuePairs, which is an uneven number");
                }
                for (int i = 0; i < keyValuePairs.length; i += 2) {
                    mapping.put(keyValuePairs[i], keyValuePairs[i + 1]);
                }
            }
            mappingList.add(mapping);
            return this;
        }

        public List<Map<String, String>> getMappingList() {
            return mappingList;
        }
    }
}
