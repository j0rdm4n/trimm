/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import dk.tigerteam.trimm.mdsd.meta.MetaType;

/**
 * Represents a OneToMany association between {@link #getMetaProperty()} and {@link #getOppositeMetaProperty()}.
 * <p/>
 * This means that {@link #getMetaProperty()} has {@link CardinalityType#Many} and {@link #getOppositeMetaProperty()} has
 * {@link CardinalityType#SingleOptional} or {@link CardinalityType#SingleRequired}.
 * <p/>
 * The way to interpret this (since it for many turns things up-side-down) is like this:
 * <p/>
 * <pre>
 * public class ClassThatOwnsMetaProperty/TypeOfOppositeProperty {
 * 	private Collection&lt;TypeOfMetaProperty> metaPropertyCollection;
 *
 * 	public Collection&lt;TypeOfMetaProperty> getMetaPropertyCollection() {
 * 		return metaPropertyCollection;
 *     }
 *
 * public void setMetaPropertyCollection(Collection&lt;TypeOfMetaProperty> metaPropertyCollection) {
 * 		this.metaPropertyCollection = metaPropertyCollection;
 *     }
 * }
 * -----------------------------------------------------------------------------------------------------
 * public class TypeOfMetaProperty {
 * 	private ClassThatOwnsMetaProperty/TypeOfOppositeProperty oppositeProperty;
 *
 * 	public ClassThatOwnsMetaProperty/TypeOfOppositeProperty getOppositeProperty() {
 * 		return metaPropertyCollection;
 *     }
 *
 * public void setOppositeProperty(ClassThatOwnsMetaProperty/TypeOfOppositeProperty oppositeProperty) {
 * 		this.oppositeProperty = oppositeProperty;
 *     }
 * }
 * </pre>
 *
 * @author Jeppe Cramon
 */
public class OneToManyAssociationEvent extends AssociationEvent {
    public OneToManyAssociationEvent() {
        super();
    }

    public OneToManyAssociationEvent(ClazzOrInterface clazz, MetaProperty metaProperty, MetaProperty oppositeProperty) {
        super(clazz, metaProperty, oppositeProperty);
    }

    @Override
    public Property execute() {
        final Field field = new Field(getResolvedJavaFieldName(), true);
        field.setMetaType(getMetaProperty());

        ClazzOrInterface clazz = JavaGeneratorContext.getContext().getClazzFromMetaClazz(getMetaProperty().getType());
        if (clazz.isClazz() && ((Clazz) clazz).isBaseClazz()) {
            clazz = JavaGeneratorContext.getContext().getExtensionClazzForBaseClazz((Clazz) clazz);
        } else if (clazz.isInterface()) {
            clazz = JavaGeneratorContext.getContext().getInterfaceFromMetaClazz(getMetaProperty().getType());
        }
        final ClazzOrInterface propertyGenericClazzOrInterface = clazz;
        final Type propertyGenericType = new Type(propertyGenericClazzOrInterface);

        Type propertyType = new Type(resolveCollectionDefinitionType()).addGenerics(propertyGenericType.clone());
        Type initializerType = new Type(resolveCollectionInstanceType()).addGenerics(propertyGenericType.clone());
        field.setType(propertyType);
        field.setInitializerStatement(new CodeSnippet("new ", initializerType, "()"));

        JavaGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {
            public void execute() {
                String javaDoc = getMetaProperty().getDocumentation() != null ? getMetaProperty().getDocumentation() : "No documentation";
                javaDoc += "<p/>" + MetaType.newline + "Defined in " + getClazzOrInterfaceAsExtensionClazzIfNecessary().getName() + "<p/>"
                        + MetaType.newline;
                javaDoc += "Property type hierarchy:" + MetaType.newline
                        + JavaGeneratorContext.getContext().getHierarchyAsHtml(propertyGenericType.getClazzOrInterface());
                field.setCodeDocumentation(javaDoc);
            }

            public String getDescription() {
                return "JavaDoc for property " + getMetaProperty();
            }
        });

        Property property;
        if (isCreateFieldForProperty()) {
            property = new Property(getClazzOrInterface(), field);
            property.setFabricated(true);
        } else {
            property = new Property(getClazzOrInterface());
            property.setFabricated(true);
            property.setName(field.getName());
            property.setType(field.getType());
            property.setInitializerStatement(field.getInitializerStatement());
            property.setCodeDocumentation(field.getCodeDocumentation());
        }
        property.setMetaType(getMetaProperty());
        setProperty(property);
        broadcastEvent();

        if (isCreateAccessorMethodsForProperty()) {
            if (isReadable()) {
                if (property.getGetterMethod() == null) {
                    property.setGetterMethod(new GetterMethod());
                    property.getGetterMethod().setCodeDocumentation(getMetaProperty().getDocumentation());
                }
            }
            if (isWriteable()) {
                if (property.getSetterMethod() == null && !getMetaProperty().hasStereoType(MetaProperty.IMMUTABLE_STEREOTYPE_NAME)) {
                    property.setSetterMethod(new SetterMethod());
                    property.getSetterMethod().setCodeDocumentation(getMetaProperty().getDocumentation());
                }
            }
        }
        return property;
    }
}
