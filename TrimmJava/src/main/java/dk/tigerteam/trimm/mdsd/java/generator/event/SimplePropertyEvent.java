/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaType;
import dk.tigerteam.trimm.util.Utils;

import java.util.Arrays;

/**
 * Simple property event, which handles properties for Java builtin types such as String, Integer, float, long, Date, BigDecimal.
 *
 * @author Jeppe Cramon
 */
public class SimplePropertyEvent extends PropertyEvent {
    /**
     * Alternative Tagged value which can be used to specify a default value in UML tools that doesn't
     * allow defaultValue or has poor support for it (e.g. MagicDraw, which doesn't allow array style initialization
     * as defaultValue content)
     */
    public static final String DEFAULT_VALUE_TAGGED_VALUE_NAME = "defaultValue";

    public SimplePropertyEvent() {
    }

    public SimplePropertyEvent(ClazzOrInterface clazz, MetaProperty metaProperty) {
        super(clazz, metaProperty);
    }

    @Override
    public Property execute() {
        Field field = new Field(getResolvedJavaFieldName(), true);
        field.setMetaType(getMetaProperty());

        String defaultValue = getMetaProperty().getDefaultValue();
        if (defaultValue == null && getMetaProperty().hasTaggedValue(DEFAULT_VALUE_TAGGED_VALUE_NAME)) {
            defaultValue = getMetaProperty().getTaggedValue(DEFAULT_VALUE_TAGGED_VALUE_NAME);
        }

        // TODO: can be it external
        Type type = new Type(getMetaProperty().getType());

        switch (getMetaProperty().getCardinalityType()) {
            case Many:
                Type propertyGenericType = resolveGenericsSafePropertyType(type);

                Type propertyType = new Type(resolveCollectionDefinitionType()).addGenerics(propertyGenericType.clone());
                field.setType(propertyType);
                if (defaultValue != null) {
                    field.setInitializerStatement(new CodeSnippet(Arrays.class, ".asList(", defaultValue, ")"));
                } else {
                    Type initializerType = new Type(resolveCollectionInstanceType()).addGenerics(propertyGenericType.clone());
                    field.setInitializerStatement(new CodeSnippet("new ", initializerType, "()"));
                }
                break;
            case SingleOptional:
                // TODO: Convert any Simple types to Wrapper types
                field.setType(type);
                if (defaultValue != null) {
                    if (type.isStringType()) {
                        field.setInitializerStatement(new CodeSnippet(Utils.quotedString(defaultValue)));
                    } else {
                        field.setInitializerStatement(new CodeSnippet(defaultValue));
                    }
                }
                break;
            case SingleRequired:
                // TODO: Convert any Wrapper types to Simple types
                field.setType(type);
                if (defaultValue != null) {
                    if (type.isStringType()) {
                        field.setInitializerStatement(new CodeSnippet(Utils.quotedString(defaultValue)));
                    } else {
                        field.setInitializerStatement(new CodeSnippet(defaultValue));
                    }
                }
                break;
        }

        String javaDoc = getMetaProperty().getDocumentation() != null ? getMetaProperty().getDocumentation() : "No documentation";
        javaDoc += "<p/>" + MetaType.newline + "Defined in " + this.getClazz().getName() + "<p/>" + MetaType.newline;
        field.setCodeDocumentation(javaDoc);

        Property property;
        if (isCreateFieldForProperty()) {
            property = new Property(getClazzOrInterface(), field);
        } else {
            property = new Property(getClazzOrInterface());
            property.setName(field.getName());
            property.setType(field.getType());
            property.setInitializerStatement(field.getInitializerStatement());
            property.setCodeDocumentation(field.getCodeDocumentation());
        }
        property.setMetaType(getMetaProperty());
        setProperty(property);

        // Allow listeners to change the type before we define accessor methods
        broadcastEvent();

        if (getMetaProperty().isStatic()) {
            field.removeAccessModifiers(AccessModifier.Private, AccessModifier.PackagePrivate, AccessModifier.Protected);
            field.addAccessModifiers(AccessModifier.Static, AccessModifier.Public, AccessModifier.Final);
            field.setName(getMetaProperty().getName());
        } else if (isCreateAccessorMethodsForProperty()) {
            if (isReadable()) {
                if (property.getGetterMethod() == null) {
                    property.setGetterMethod(new GetterMethod());
                    property.getGetterMethod().setCodeDocumentation(getMetaProperty().getDocumentation());
                }
            }
            if (isWriteable()) {
                if (property.getSetterMethod() == null && !getMetaProperty().hasStereoType(MetaProperty.IMMUTABLE_STEREOTYPE_NAME)) {
                    property.setSetterMethod(new SetterMethod());
                    property.getSetterMethod().setCodeDocumentation(getMetaProperty().getDocumentation());
                }
            }
        }
        return property;
    }

    protected Type resolveGenericsSafePropertyType(Type type) {
        if (type.getName().equals("int")) {
            return new Type("Integer");
        }
        if (type.getName().equals("long")) {
            return new Type("Long");
        }
        if (type.getName().equals("short")) {
            return new Type("Short");
        }
        if (type.getName().equals("char")) {
            return new Type("Character");
        }
        if (type.getName().equals("float")) {
            return new Type("Float");
        }
        if (type.getName().equals("double")) {
            return new Type("Double");
        }
        if (type.getName().equals("boolean")) {
            return new Type("Boolean");
        }
        if (type.getName().equals("byte")) {
            return new Type("Byte");
        } else {
            return type;
        }
    }

}
