/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredEventHandler;

/**
 * Generates a <code>private static final long serialVersionUID</code> field for all {@link Clazz}' based on their fields/properties.
 * <p/>
 * Example: <code>private static final long serialVersionUID = 590742170313829643L;</code>
 *
 * @author Jeppe Cramon
 */
public class SerialVersionUIDGeneratorListener extends AbstractGeneratorEventListener {

    @Override
    protected boolean handleClazzEvent(final ClazzEvent event) {
        if (!event.getClazz().getClass().equals(Clazz.class))
            return true;

        JavaGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {

            public String getDescription() {
                return "serialVersionUID for Clazz " + event.getMetaClazz();
            }

            public void execute() {
                // Generate: "private static final long serialVersionUID =
                // 590742170313829643L;"

                long serialVersionUID = event.getClazz().getFQCN().hashCode();
                for (Property property : event.getClazz().getProperties()) {
                    serialVersionUID += property.getField().getName().hashCode();
                }

                event.getClazz().addChildren(
                        new Field("serialVersionUID", true).setType(new Type("long"))
                                .setInitializerStatement(new CodeSnippet(Math.abs(serialVersionUID) + "L"))
                                .addAccessModifiers(AccessModifier.Private, AccessModifier.Static, AccessModifier.Final)

                );
            }

        });
        return true;
    }
}
