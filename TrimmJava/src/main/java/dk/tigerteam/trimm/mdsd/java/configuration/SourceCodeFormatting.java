/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.configuration;

import dk.tigerteam.trimm.mdsd.java.generator.codewriter.JalopyFormattingCodeWriter;

import java.util.ArrayList;
import java.util.List;

/**
 * YAML specific configuration code (for setting up Sourcecode formatting)
 *
 * @author Jeppe Cramon
 */
public class SourceCodeFormatting {
    private String implementation;
    private List<String> excludeFormattingForPackages;

    /**
     * What sourcecode formatting should be used (if implementation isn't specified {@link JalopyFormattingCodeWriter} is used).<br/>
     *
     * @return
     */
    public String getImplementation() {
        return implementation;
    }

    public void setImplementation(String implementation) {
        this.implementation = implementation;
    }

    /**
     * Exclusion of formatting can also be specified for several packages (and thereby also apply to subpackages of these)<p/>
     * <i>YAML example configuration:</i>
     * <pre>
     * sourceCodeFormatting:
     *     implementation: dk.tigerteam.mdsd.java.generator.codewriter.JalopyFormattingCodeWriter
     *     excludeFormattingForPackages:
     *         - dk.tigerteam.mddexample.meta
     *         - dk.tigerteam.mddexample.test
     * </pre>
     *
     * @return
     */
    public List<String> getExcludeFormattingForPackages() {
        if (excludeFormattingForPackages == null) {
            return new ArrayList<String>();
        }
        return excludeFormattingForPackages;
    }

    public void setExcludeFormattingForPackages(List<String> excludeFormattingForPackages) {
        this.excludeFormattingForPackages = excludeFormattingForPackages;
    }

}