/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.codedom.GenericType.Specialization;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.PackageRewriter;
import dk.tigerteam.trimm.util.Utils;

import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * Code visitor that can output Java code to a {@link CodeWriter} - supports Package rewriting
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class JavaCodeWritingVisitor extends AbstractCodeWritingVisitor {

    /**
     * For debug and test purposes (uses no PackageRewriting and no imports)
     * @param codeElement The code element to write code for
     * @return The codeelement (and all its subelements) as Java code (in string form)
     */
    public static String ToCode(CodeElement codeElement) {
        StringWriter writer = new StringWriter();
        JavaCodeWritingVisitor codeWritingVisitor = new JavaCodeWritingVisitor(new CodeWriter(writer), null, false);
        codeElement.visit(codeWritingVisitor);
        return writer.toString();
    }

    public JavaCodeWritingVisitor(CodeWriter writer, PackageRewriter packageRewriter, boolean useImports) {
        super(writer, packageRewriter, useImports);
    }

    @Override
    public void handle(Annotation annotation) {
        if (isUseImports()) {
            writer.print("@").print(annotation.getSimpleName());
        } else {
            writer.print("@").print(annotation.getName());
        }
        if (annotation.hasChildren()) {
            writer.print("(");
            printCodeElementCollection(annotation.getChildren());
            writer.print(")");
        }
        writer.println();
    }

    @Override
    public void handle(AnnotationAttribute annotationAttribute) {
        if (annotationAttribute.getName() != null && !annotationAttribute.getName().equals(AnnotationAttribute.DEFAULT_VALUE_ATTRIBUTE_NAME)) {
            writer.print(annotationAttribute.getName()).print("=");
        }
        if (annotationAttribute.hasMultipleValues()) {
            writer.print("{");
        }
        int count = 0;
        for (Object value : annotationAttribute.getValues()) {
            if (count > 0) {
                writer.print(", ");
            }
            if (value instanceof CodeElement) {
                ((CodeElement) value).visit(this);
            } else if (value instanceof Enum) {
                Enum<?> enumValue = (Enum<?>) value;
                if (isUseImports()) {
                    writer.print(enumValue.getDeclaringClass().getSimpleName() + "." + enumValue.name());
                } else {
                    writer.print(enumValue.getDeclaringClass().getName() + "." + enumValue.name());
                }
            } else if (value instanceof String) {
                writer.print(Utils.quotedString((String) value));
            } else if (value != null) {
                writer.print(value.toString());
            } else {
                throw new IllegalStateException("AnnotationAttribute " + annotationAttribute + " had a null value. Context: " + annotationAttribute.createOwnershipContextString());
            }
            count++;
        }
        if (annotationAttribute.hasMultipleValues()) {
            writer.print("}");
        }
    }

    @Override
    public void handle(AnnotationDefinition annotationDefinition) {
        // TODO: Complete
    }

    @Override
    public void handle(Clazz clazz) {
        CodePackage codePackage = clazz.getPackage();
        if (codePackage != null) {
            writer.print("package ").print(codePackage.getName()).println(";");
        }

        for (Import import_ : clazz.getImports()) {
            import_.visit(this);
        }

        printJavaDoc(clazz);

        printAnnotations(clazz);
        printAccessModifiers(clazz);
        writer.print("class ");
        writer.print(clazz.getName());

        if (clazz.getGenerics().size() > 0) {
            writer.print("<");
            printCodeElementCollection(clazz.getGenerics());
            writer.print(">");
        }

        writer.print(" ");
        if (clazz.getExtendsType() != null) {
            writer.print("extends ");
            clazz.getExtendsType().visit(this);
            writer.print(" ");
        }
        if (clazz.getImplementsTypes().size() > 0) {
            writer.print("implements ");
            printCodeElementCollection(clazz.getImplementsTypes());
        }

        writer.println("{");

        for (CodeElement child : clazz.getChildren()) {
            child.visit(this);
        }

        writer.println("}");
    }

    @Override
    public void handle(CodePackage codePackage) {
        // Do nothing so far
    }

    @Override
    public void handle(Constructor constructor) {
        Clazz definedInClazz = constructor.findOwnerOfType(Clazz.class);
        if (definedInClazz != null && !constructor.hasAccessModifier(AccessModifier.Static)) {
            // Use the Clazz' name as our constructor name
            constructor.setName(definedInClazz.getName());
        }
        handle((Method) constructor);
    }

    @Override
    public void handle(Enumeration enumeration) {
        CodePackage codePackage = enumeration.getPackage();
        if (codePackage != null) {
            writer.print("package ").print(codePackage.getName()).println(";");
        }

        for (Import import_ : enumeration.getImports()) {
            import_.visit(this);
        }

        printJavaDoc(enumeration);
        printAnnotations(enumeration);
        printAccessModifiers(enumeration);
        writer.print("enum ");
        writer.print(enumeration.getName());
        writer.print(" ");
        if (enumeration.getImplementsTypes().size() > 0) {
            writer.print("implements ");
            printCodeElementCollection(enumeration.getImplementsTypes());
        }
        writer.println(" {");
        List<EnumLiteralField> enumValues = enumeration.getEnumValues();
        printCodeElementCollection(enumValues);
        writer.println(";");

        // Write out the rest of the children if any (its late, perhaps there's a better way to sort this stuff out)
        List<CodeElement> remainingChildren = new LinkedList<CodeElement>();
        for (CodeElement child : enumeration.getChildren()) {
            if (!enumValues.contains(child)) {
                remainingChildren.add(child);
            }
        }

        for (CodeElement child : remainingChildren) {
            child.visit(this);
        }

        writer.println("}");
    }

    @Override
    public void handle(EnumLiteralField enumLiteralField) {
        if (enumLiteralField.getClazzOrInterfaceOwner().isInterface()) {
            return;
        }

        printJavaDoc(enumLiteralField);
        printAnnotations(enumLiteralField);

        writer.print(" ");
        writer.print(enumLiteralField.getName());
        if (enumLiteralField.getInitializerStatement() != null) {
            writer.print("(");
            enumLiteralField.getInitializerStatement().visit(this);
            writer.print(")");
        }
    }

    @Override
    public void handle(Field field) {
        if (field.getClazzOrInterfaceOwner().isInterface() && !field.hasAccessModifier(AccessModifier.Static)) {
            return;
        }

        printJavaDoc(field);
        printAnnotations(field);
        printAccessModifiers(field);
        field.getType().visit(this);
        writer.print(" ");
        writer.print(field.getName());
        if (field.getInitializerStatement() != null) {
            writer.print(" = ");
            field.getInitializerStatement().visit(this);
        }
        writer.println(";");
    }

    @Override
    public void handle(FQCN fqcn) {
        if (fqcn.isGenerateQuoted()) {
            if (isUseImports() && !fqcn.isOverrideImports()) {
                writer.print(Utils.quotedString(fqcn.getClazzOrInterface().getName()));
            } else if (isUseImports() && fqcn.isOverrideImports()) {
                writer.print(Utils.quotedString(fqcn.getClazzOrInterface().getFQCN()));
            } else {
                writer.print(Utils.quotedString(fqcn.getClazzOrInterface().getFQCN()));
            }
        } else {
            if (isUseImports() && !fqcn.isOverrideImports()) {
                writer.print(fqcn.getClazzOrInterface().getName());
            } else if (isUseImports() && fqcn.isOverrideImports()) {
                writer.print(fqcn.getClazzOrInterface().getFQCN());
            } else {
                writer.print(fqcn.getClazzOrInterface().getFQCN());
            }
        }
    }

    @Override
    public void handle(GenericType genericType) {
        if (genericType.hasSymbol()) {
            writer.print(genericType.getSymbol());
        }
        if (genericType.getSpecialization() != Specialization.None) {
            writer.print(genericType.getSpecialization().toString().toLowerCase());
        }
        if (genericType.getName() != null) {
            handle((Type) genericType);
        }
    }

    @Override
    public void handle(GetterMethod getterMethod) {
        handle((Method) getterMethod);
    }

    @Override
    public void handle(SetterMethod setterMethod) {
        handle((Method) setterMethod);
    }

    @Override
    public void handle(Import _import) {
        writer.print("import ").print(_import.getName()).println(";");
    }

    @Override
    public void handle(Interface _interface) {
        CodePackage codePackage = _interface.getPackage();
        if (codePackage != null) {
            writer.print("package ").print(codePackage.getName()).println(";");
        }
        for (Import import_ : _interface.getImports()) {
            import_.visit(this);
        }
        printJavaDoc(_interface);
        printAnnotations(_interface);
        printAccessModifiers(_interface);
        writer.print("interface ");
        writer.print(_interface.getName());

        if (_interface.getGenerics().size() > 0) {
            writer.print("<");
            printCodeElementCollection(_interface.getGenerics());
            writer.print(">");
        }

        writer.print(" ");
        if (_interface.getExtendsTypes().size() > 0) {
            writer.print("extends ");
            printCodeElementCollection(_interface.getExtendsTypes());
        }
        writer.println("{");

        for (CodeElement child : _interface.getChildren()) {
            child.visit(this);
        }

        writer.print("}");
    }

    @Override
    public void handle(Method method) {
        printJavaDoc(method);
        printAnnotations(method);
        printAccessModifiers(method);
        method.getReturnType().visit(this);
        // Support static constructors without name nor arguments
        if (method.getName() != null) {
            writer.print(" ").print(method.getName()).print("(");
            printCodeElementCollection(method.getParameters());
            writer.print(") ");
            if (method.getThrowsExceptions().size() > 0) {
                writer.print("throws ");
                printCodeElementCollection(method.getThrowsExceptions());
            }
        }
        if (method.getClazzOrInterfaceOwner().isInterface() || method.hasAccessModifier(AccessModifier.Abstract)) {
            writer.println(";");
        } else {
            writer.println(" {");
            writeMethodBodyContent(method);
            writer.println("}");
        }
    }

    @Override
    public void handle(PackageFQN packageFQN) {
        if (packageFQN.isGenerateQuoted()) {
            writer.print(Utils.quotedString(packageFQN.getCodePackage().getName()));
        } else {
            writer.print(packageFQN.getCodePackage().getName());
        }
    }

    @Override
    public void handle(Parameter parameter) {
        parameter.getType().visit(this);
        if (parameter.isVariableArgument()) {
            writer.print("...");
        } else {
            writer.print(" ");
        }
        writer.print(parameter.getName());
    }

    @Override
    public void handle(Property property) {
        for (CodeElement child : property.getChildren()) {
            child.visit(this);
        }
    }

    @Override
    public void handle(Type type) {
        List<? extends Type> generics = type.getGenerics();

        if (type.getClazz() != null) {
            if (isUseImports()) {
                writer.print(type.getClazz().getName());
            } else {
                if (hasPackageRewriter()) {
                    getPackageRewriter().rewrite(type.getClazz());
                    type.setName(type.getClazz().getFQCN());
                }
                writer.print(type.getClazz().getFQCN());
            }
            if (type.getClazz().getGenerics().size() > 0) {
                generics = type.getClazz().getGenerics();
            }
        } else if (type.getInterface() != null) {
            if (isUseImports()) {
                writer.print(type.getInterface().getName());
            } else {
                if (hasPackageRewriter()) {
                    getPackageRewriter().rewrite(type.getInterface());
                    type.setName(type.getInterface().getFQCN());
                }
                writer.print(type.getInterface().getFQCN());
            }
            if (type.getInterface().getGenerics().size() > 0) {
                generics = type.getInterface().getGenerics();
            }
        }  else if (type.getWrappedJavaClass() != null) {
            if (isUseImports())
                writer.print(type.getWrappedJavaClass().getSimpleName());
            else
                writer.print(type.getName());
        } else {
            if (hasPackageRewriter()) {
                type.setName(getPackageRewriter().rewrite(type.getName()));
            }
            writer.print(type.getName());
        }

        if (generics.size() > 0) {
            writer.print("<");
            printCodeElementCollection(generics);
            writer.print(">");
        }

        if (type.isReference()) {
            writer.print(".class");
        }

    }

    @Override
    public void handle(CodeModel codeModel) {
        // DO nothing so far
    }


    @Override
    public void handle(Closure closure) {
        if (closure.getParameters().size() > 1) {
            writer.print("(");
        }
        printCodeElementCollection(closure.getParameters());
        if (closure.getParameters().size() > 1) {
            writer.print(")");
        }
        writer.print(" => ");
        writer.println("{");
        writeMethodBodyContent(closure);
        writer.println("}");
    }

}
