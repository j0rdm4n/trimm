/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.Enumeration;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;

/**
 * Strategy interface so inheritance chains and super classes can be controlled in a project or technology by technology specific way
 *
 * @author Jeppe Cramon
 */
public interface JavaInheritanceAndClazzesStrategy {

    /**
     * This method returns what super class that should be used for a given {@link Clazz} in case is hasn't got one already.<br/>
     *
     * @param clazz The clazz
     * @return The type to use as Super class or <code>null</code> if the class shouldn't inherit from a super class
     */
    Type resolveSuperClazzType(Clazz clazz);

    /**
     * Test if a given {@link Clazz} is an {@link Enumeration} clazz
     *
     * @param clazz The clazz to test
     * @return true if the {@link Clazz} is an enumeration, otherwise false
     */
    boolean isEnumerationClazz(Clazz clazz);

    /**
     * Test if a given type refers to (through {@link Type#getClazz()}) is an {@link Enumeration} clazz
     *
     * @param clazz The clazz to test
     * @return true if the {@link Clazz} the type refers to is an enumeration, otherwise false
     */
    boolean isEnumerationClazz(Type type);
}
