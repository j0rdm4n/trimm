/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.Enumeration;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.CodeWritingStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.java.generator.extension.ConstructorForImmutablePropertiesListener;
import dk.tigerteam.trimm.mdsd.java.generator.extension.SerialVersionUIDGeneratorListener;
import dk.tigerteam.trimm.mdsd.meta.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.*;

import static dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext.getContext;
import static dk.tigerteam.trimm.mdsd.java.generator.JavaStereotypes.DONT_GENERATE_STEREOTYPE_NAME;

/**
 * The main code generator class. The {@link JavaGenerator} handles the
 * entire process of moving from a {@link MetaModel} world to a {@link CodeModel} world.<br/>
 * The configuration for the JavaGenerator is  the {@link JavaGeneratorContext} instance which is passed to the
 * the {@link #transform(JavaGeneratorContext)} or {@link #transformAndWriteCode(JavaGeneratorContext, CodeWritingStrategy)} method,
 * which kicks off the generation process.
 * These methods processes all {@link MetaClazz}' in the {@link MetaModel} during the following life cycle:
 * <ol>
 * <li><b>FirstPassGeneration</b> ({@link #firstPassGeneration(CodeModel, MetaModel)})
 * <ol>
 * <li>First a corresponding {@link CodePackage} is created for each {@link MetaPackage} in the {@link MetaModel}.</li>
 * <li>Then all {@link MetaClazz} are first pre-processed through the template method {@link JavaCodeGenerationStrategy#preProcessMetaClazz(MetaClazz)}.</li>
 * <li>After this the {@link MetaClazz} is run through the {@link JavaCodeGenerationStrategy#shouldGenerateForMetaClazz(MetaClazz)}.<br/>
 * If this method returns true then<br/>
 * - A {@link Clazz}, {@link Interface} or {@link Enumeration} instance is created (depending on whether we are processing a {@link MetaClazz},
 * a {@link MetaInterface} or a {@link MetaEnumeration}).<br/>
 * - The generated {@link ClazzOrInterface} (which is the common class for {@link Clazz}, {@link Interface} and {@link Enumeration}) is processed by
 * {@link JavaCodeGenerationStrategy#preProcessClazz(ClazzOrInterface, MetaClazz)}</li>
 * </ol>
 * </li>
 * <li><b>Reconstruction of the inheritance hierarchy</b> ({@link #reconstructClazzHierarchy(List)}) - After all classes have been created we
 * perform the reconstruction of inheritance hierarchies for {@link ClazzOrInterface} according to how it's defined in the {@link MetaModel}</li>
 * <li><b>SecondPassGeneration</b> ({@link #secondPassGeneration(List)})<br/>
 * For each {@link ClazzOrInterface}' generated in the {@link #firstPassGeneration(CodeModel, MetaModel)} (and returned by
 * {@link #reconstructClazzHierarchy(List)}) we now do the following:
 * <ul>
 * <li>Call either {@link #processClazz(Clazz)} (which create a {@link ClazzEvent} for both {@link MetaClazz} and {@link MetaEnumeration}) or
 * {@link #processInterface(Interface)} (which create a
 * {@link InterfaceEvent}) and execute the event</li>
 * <li>This in turn calls either {@link #generateEnumerators(Enumeration)} for {@link Enumeration} or
 * {@link #generateFieldsAndProperties(ClazzOrInterface)} for {@link Clazz}' or {@link Interface}</li>
 * <li>{@link #generateEnumerators(Enumeration)} will create and execute
 * {@link EnumeratorLiteralEvent} for each {@link MetaProperty} in the {@link MetaEnumeration} that
 * defined the {@link Enumeration}</li>
 * <li>{@link #generateFieldsAndProperties(ClazzOrInterface)} will create and execute {@link PropertyEvent}
 * subtypes ({@link SimplePropertyEvent} or a {@AssociationEvent} subclass such as {@link OneToOneAssociationEvent} or {@link OneToManyAssociationEvent})
 * for each {@link MetaProperty} defined in the {@link MetaClazz}/{@link MetaInterface} that defined the {@link Clazz} by delegating it to
 * {@link #createPropertyEvent(ClazzOrInterface, MetaProperty)})</li>
 * <li>{@link #generateMethods(dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface)}</li> is called. This will generate a {@link MethodEvent} for
 * each {@link MetaOperation} defined in the meta model
 * </ul>
 * </li>
 * <li><b>Finally it will run {@link DeferredEventHandler}'</b> ({@link #runDeferredEventHandlers()}) - At this point all {@link ClazzOrInterface}'s are done, but
 * certain {@link GeneratorEventListener} (like {@link SerialVersionUIDGeneratorListener} and JPANamedTablesAndColumnsListener (from
 * TRIMMJpa) might have needed all {@link Clazz}' and {@link Property}' to be created, before they can complete their event handling.</li>
 * <li><b>Postprocess Clazzes ({@link JavaCodeGenerationStrategy#postProcessClazzes(List)})</b><br/>
 * This where the processing ends, unless you've called the {@link #transformAndWriteCode(JavaGeneratorContext, CodeWritingStrategy)} method
 * in which case the code is written to the destination using the provided {@link CodeWritingStrategy}
 *
 * @author Jeppe Cramon
 */
public class JavaGenerator {
    private static Log log = LogFactory.getLog(JavaGenerator.class);

    private JavaCodeGenerationStrategy javaCodeGenerationStrategy;
    private boolean fixMagicDrawPackageNameInClassNameBug;

    /**
     * Configure the generator using the {@link DefaultJavaCodeGenerationStrategy}
     */
    public JavaGenerator() {
        this(new DefaultJavaCodeGenerationStrategy());
    }

    public boolean isFixMagicDrawPackageNameInClassNameBug() {
        return fixMagicDrawPackageNameInClassNameBug;
    }

    public void setFixMagicDrawPackageNameInClassNameBug(boolean fixMagicDrawPackageNameInClassNameBug) {
        this.fixMagicDrawPackageNameInClassNameBug = fixMagicDrawPackageNameInClassNameBug;
    }

    /**
     * Configure the generator using a custom {@link JavaCodeGenerationStrategy}
     *
     * @param javaCodeGenerationStrategy the strategy to use
     */
    public JavaGenerator(JavaCodeGenerationStrategy javaCodeGenerationStrategy) {
        this.javaCodeGenerationStrategy = javaCodeGenerationStrategy;
    }

    public JavaCodeGenerationStrategy getJavaCodeGenerationStrategy() {
        return javaCodeGenerationStrategy;
    }

    // ----------------------------------------- A P I ---------------------------------------------------------

    /**
     * Transform from the {@link MetaModel} to the {@link CodeModel} according to the configuration of the {@link JavaGeneratorContext}
     *
     * @param generatorContext The context according which the transformation will occur
     * @return All the generated {@link ClazzOrInterface}'s
     */
    public List<ClazzOrInterface> transform(JavaGeneratorContext generatorContext) {
        log.info("Starting java code transformation");
        MetaModel metaModel = generatorContext.getMetaModel();
        if (metaModel == null) {
            throw new NullPointerException("Generator Context didn't contain a Meta model");
        }
        CodeModel codeModel = generatorContext.getCodeModel();
        if (codeModel == null) {
            throw new NullPointerException("Generator Context didn't contain a Java model");
        }

        boolean addConstructorForImmutablePropertiesListener = true;
        for (GeneratorEventListener generatorEventListener : generatorContext.getEventListeners()) {
            if (generatorEventListener instanceof ConstructorForImmutablePropertiesListener) {
                addConstructorForImmutablePropertiesListener = false;
            }
        }
        if (addConstructorForImmutablePropertiesListener) {
            generatorContext.getEventListeners().add(
                    new DeferredEventHandlerAdapter(new ConstructorForImmutablePropertiesListener())
            );
        }

        JavaGeneratorContext.setContext(generatorContext);
        try {
            List<MetaClazz> allMetaClazzesToGenerateCodeFor = firstPassGeneration(codeModel, metaModel);
            // Run first pass on any referenced meta models
            if (metaModel.getReferencedMetaModels().size() > 0) {
                for (MetaModel referencedMetaModel : metaModel.getReferencedMetaModels().values()) {
                    firstPassGeneration(codeModel, referencedMetaModel);
                }
            }
            if (!validateMetaClazzes(allMetaClazzesToGenerateCodeFor)) {
                throw new RuntimeException("Failed to generate code, due to validation errors. See the error log.");
            }

            List<ClazzOrInterface> allClazzesToGeneratorCodeFor = reconstructClazzHierarchy(allMetaClazzesToGenerateCodeFor);
            List<ClazzOrInterface> allGeneratedClazzes = secondPassGeneration(allClazzesToGeneratorCodeFor);
            runDeferredEventHandlers();

            // Spot extra classes generated by listeners
            List<ClazzOrInterface> allClazzesAndInterfaces = generatorContext.getCodeModel().findChildrenOfType(ClazzOrInterface.class);


            allGeneratedClazzes = javaCodeGenerationStrategy.postProcessClazzes(allClazzesAndInterfaces);
            runGenerationComplete(allGeneratedClazzes);

            return allGeneratedClazzes;
        } finally {
            JavaGeneratorContext.removeContext();
        }
    }

    /**
     * All in one transformation from {@link MetaModel} to {@link CodeModel} and finally write out the {@link Clazz}' using the
     * {@link CodeWritingStrategy}
     *
     * @param generatorContext    The Java generator context
     * @param codeWritingStrategy The {@link CodeWritingStrategy} to use for writing the generated {@link Clazz}' to the destination
     * @return All the {@link Clazz}' generated
     */
    public List<ClazzOrInterface> transformAndWriteCode(JavaGeneratorContext generatorContext, CodeWritingStrategy codeWritingStrategy) {
        List<ClazzOrInterface> allGeneratedClazzes = transform(generatorContext);
        log.debug("Writing code using strategy " + codeWritingStrategy.getClass().getName());
        codeWritingStrategy.writeCode(generatorContext, allGeneratedClazzes);
        return allGeneratedClazzes;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------

    /**
     * This method is run when secondPhase is complete and all postProcessing is complete. This is the last chance for event listeners to do
     * some work (like validation) in the method {@link GeneratorEventListener#generationComplete(java.util.List)}
     *
     * @param allGeneratedClazzes All the {@link Clazz}' that were generated during phase 2
     */
    protected void runGenerationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
        for (GeneratorEventListener eventListener : getContext().getEventListeners()) {
            eventListener.generationComplete(allGeneratedClazzes);
        }
    }

    /**
     * At this point we have generated {@link Clazz} instances for all {@link MetaClazz}' that we want to generate code for, but they're not placed into their hierarchies.
     * Before we can continue to secondPass, we need reconstruct the {@link Clazz} hierarchy from the information in the {@link MetaClazz}'s
     *
     * @param allMetaClazzesToGenerateCodeFor
     *         All the {@link MetaClazz}' that we want to generate code for (see {@link JavaCodeGenerationStrategy#shouldGenerateForMetaClazz(MetaClazz)})
     * @return All the {@link Clazz}'s to generate code for (the number of clazzes might be much larger than the number of {@link MetaClazz})
     */
    protected List<ClazzOrInterface> reconstructClazzHierarchy(List<MetaClazz> allMetaClazzesToGenerateCodeFor) {
        List<ClazzOrInterface> allClazzesToGeneratorCodeFor = new ArrayList<ClazzOrInterface>();

        for (MetaClazz metaClazz : allMetaClazzesToGenerateCodeFor) {
            if (!metaClazz.isInterface()) {
                // Lookup the Clazz (which was generated prior to this Event being created)
                Clazz clazz = (Clazz) getContext().getClazzFromMetaClazz(metaClazz);
                allClazzesToGeneratorCodeFor.add(clazz);
                // Handle super clazz
                if (metaClazz.getSuperClazz() != null) {
                    clazz.setExtendsType(metaClazz.getSuperClazz());
                }
                handleRealizations(clazz);
            } else {
                Interface _interface = getContext().getInterfaceFromMetaClazz(metaClazz);
                allClazzesToGeneratorCodeFor.add(_interface);
                handleRealizations(_interface);
            }
        }
        // Finally insert extension clazzes in the hierarchy (if they're used)
        if (getContext().isCreateExtensionClazzes()) {
            Set<Clazz> baseClazzes = getContext().getAllBaseClazzes();
            for (Clazz baseClazz : baseClazzes) {
                Clazz extensionClazz = getContext().getExtensionClazzForBaseClazz(baseClazz);
                allClazzesToGeneratorCodeFor.add(extensionClazz);

                List<Clazz> subClazzesOfBaseClazz = getContext().getCodeModel().findAllDirectSubClazzesOfClazz(baseClazz);
                for (Clazz subClazzOfBaseClazz : subClazzesOfBaseClazz) {
                    subClazzOfBaseClazz.setExtendsType(extensionClazz);
                }
                // Must be done last, else we'll include our selves in the search for subclazzes of baseClazz
                extensionClazz.setExtendsType(baseClazz);
            }
        }

        return allClazzesToGeneratorCodeFor;
    }

    /**
     * Handle {@link Clazz} or {@link Interface} {@link MetaClazz#getRealizations()}. A {@link MetaInterface} realization (meaning a case
     * where {@link MetaClazz#isInterface()} returns true, is translated into {@link Interface#getExtendsTypes()} whereas, a true
     * {@link MetaClazz} realization (meaning {@link MetaClazz#isInterface()} returns false) is translated into
     * {@link Clazz#getImplementsTypes()}
     *
     * @param clazzOrInterface The {@link Clazz} or {@link Interface} that we're going to handle the {@link MetaClazz#getRealizations()} for
     */
    protected void handleRealizations(ClazzOrInterface clazzOrInterface) {
        for (MetaInterface implementsMetaInterface : clazzOrInterface.getMetaType().getRealizations()) {
            Interface implementsInterface = getContext().getInterfaceFromMetaClazz(implementsMetaInterface);
            if (clazzOrInterface.isClazz()) {
                ((Clazz) clazzOrInterface).getImplementsTypes().add(new Type(implementsInterface));
            } else {
                ((Interface) clazzOrInterface).getExtendsTypes().add(new Type(implementsInterface));
            }
        }
    }

    /**
     * Run all the {@link DeferredEventHandler}' - At this point all {@link Clazz} are done, but certain {@link GeneratorEventListener} , like
     * {@link SerialVersionUIDGeneratorListener} and JPANamedTablesAndColumnsListener (from TigerMDSDJpa) might have needed all {@link Clazz}'
     * and {@link Property}' to be done, before they can complete their event handling.
     */
    protected void runDeferredEventHandlers() {
        for (DeferredEventHandler deferredEventHandler : getContext().getQueuedDeferredEventHandlers()) {
            try {
                deferredEventHandler.execute();
            } catch (Exception e) {
                try {
                    log.error("Failed wile executing DeferredEventHandler: " + deferredEventHandler.getDescription(), e);
                } catch (Exception e1) {
                    log.error("Failed wile executing DeferredEventHandler (see next error log statement for why there's no description)", e);
                    log.error("Failed wile executing DeferredEventHandler: <unable to aquire description due to getDescription() failure>", e1);
                }
            }
        }
        getContext().getQueuedDeferredEventHandlers().clear();
    }

    /**
     * Validate meta classes and their properties. Use error logging to print out error messages to the developer.
     *
     * @param allMetaClazzesToGenerateCodeFor
     *         All the metaClazzes that we're going to generate code for in {@link #secondPassGeneration(List)}
     * @return true if we should continue with {@link #secondPassGeneration(List)} otherwise it returns false.
     */
    protected boolean validateMetaClazzes(List<MetaClazz> allMetaClazzesToGenerateCodeFor) {
        log.info("Performing MetaClazz validations");
        // Check for duplicate classes
        boolean shouldContinueWith2ndPassGeneration = true;
        Collection<MetaClazz> metaClazzesCheckForDuplicates = new HashSet<MetaClazz>();
        for (MetaClazz metaClazz : allMetaClazzesToGenerateCodeFor) {
            for (MetaClazz metaClazzesChecked : metaClazzesCheckForDuplicates) {
                if (metaClazzesChecked.getFQCN().equals(metaClazz.getFQCN())) {
                    log.error("Duplicate MetaClazz. MetaClazz '" + metaClazz.toString() + "' collides with '" + metaClazzesChecked.toString()
                            + "'. Both MetaClazz' have FQCN: '" + metaClazz.getFQCN() + "'");
                    shouldContinueWith2ndPassGeneration = false;
                    break;
                }
            }
            metaClazzesCheckForDuplicates.add(metaClazz);
        }
        return shouldContinueWith2ndPassGeneration;
    }

    /**
     * <li><b>SecondPassGeneration</b> ({@link #secondPassGeneration(List)})<br/>
     * For each {@link Clazz}' generated in the {@link #firstPassGeneration(CodeModel, MetaModel)} (and returned by
     * {@link #reconstructClazzHierarchy(List)}) we now do the following:
     * <ul>
     * <li>Call either {@link #processClazz(Clazz)} (which create a {@link ClazzEvent}) or
     * {@link #processInterface(Interface)} (which create a
     * {@link InterfaceEvent}) and execute the event</li>
     * <li>This in turn calls either {@link #generateEnumerators(Enumeration)} for {@link Enumeration} or
     * {@link #generateFieldsAndProperties(ClazzOrInterface)} for {@link Clazz}'</li>
     * <li>{@link #generateEnumerators(Enumeration)} will create and execute
     * {@link EnumeratorLiteralEvent} for each {@link MetaProperty} in the {@link MetaEnumeration} that
     * defined the {@link Enumeration}</li>
     * <li>{@link #generateFieldsAndProperties(ClazzOrInterface)} will create and execute {@link PropertyEvent}
     * subtypes for each {@link MetaProperty} in the {@link MetaClazz} that defined the {@link Clazz} by delagating to
     * {@link #createPropertyEvent(ClazzOrInterface, MetaProperty)})</li>
     * </ul>
     * </li>
     *
     * @param allClazzesToGenerateCodeFor All the {@link ClazzOrInterface}' to generated code for
     */
    protected List<ClazzOrInterface> secondPassGeneration(List<ClazzOrInterface> allClazzesToGenerateCodeFor) {
        log.info("Second pass generation - stage 1");
        for (ClazzOrInterface clazz : allClazzesToGenerateCodeFor) {
            if (clazz.isInterface()) {
                new InterfaceEvent((Interface) clazz).execute();
            } else {
                new ClazzEvent((Clazz) clazz).execute();
            }
        }

        log.info("Second pass generation - stage 2");
        List<ClazzOrInterface> allGeneratedClazzes = new LinkedList<ClazzOrInterface>();
        for (ClazzOrInterface clazz : allClazzesToGenerateCodeFor) {
            ClazzOrInterface generatedClazz = null;
            if (clazz.isInterface()) {
                log.debug("Processing Interface '" + clazz.getFQCN() + "'");
                generatedClazz = processInterface((Interface) clazz);
            } else {
                log.debug("Processing Clazz '" + clazz.getFQCN() + "'");
                generatedClazz = processClazz((Clazz) clazz);
            }
            allGeneratedClazzes.add(generatedClazz);
        }
        return allGeneratedClazzes;
    }

    /**
     * Pre creates all Packages and Clazz'es (which we should generate code for, see {@link JavaCodeGenerationStrategy#shouldGenerateForMetaClazz(MetaClazz)}) know from
     * the MetaModel so that our code can focus on finding references to packages and classes without having to worry about whether they'be
     * been created yet or not<p/>
     * Classes in the MetaModel may be annotated using the {@link JavaStereotypes#EXTENSION_CLASS_STEREOTYPE_NAME} and
     * {@link JavaStereotypes#NO_EXTENSION_CLASS_STEREOTYPE_NAME} to control Extension class generation explicitly
     *
     * @param codeModel The CodeDom model (which contains the result of the the MetaModel transformation)
     * @param metaModel The Metamodel
     * @return All MetaClazz's that we should generate code for in the secondParseGeneration() method
     */
    protected List<MetaClazz> firstPassGeneration(CodeModel codeModel, MetaModel metaModel) {
        log.info("First pass JavaModel generation");
        // Run through all Meta packages and generate
        for (MetaPackage metaPackage : metaModel.findAllChildrenOfType(MetaPackage.class)) {
            // Check if it has already been created (main vs. referenced meta models)
            boolean packageHasntBeenGenerated = getContext().getCodeModel().findChildWithName(CodePackage.class, metaPackage.getFullPackageName()) == null;
            boolean packageShouldParticipateInGeneration = !metaPackage.hasStereotypeUpwardsRecursively(DONT_GENERATE_STEREOTYPE_NAME);
            if (packageHasntBeenGenerated && packageShouldParticipateInGeneration) {
                CodePackage codePackage = new CodePackage(metaPackage.getFullPackageName());
                codePackage.setMetaType(metaPackage);
                codeModel.addChildren(codePackage);
            }
        }

        List<MetaClazz> metaClazzesToGenerateCodeFor = new LinkedList<MetaClazz>();
        List<MetaClazz> allMetaClazzes = metaModel.findAllChildrenOfType(MetaClazz.class);
        if (log.isDebugEnabled()) {
            log.debug("Found MetaClazz candidates for firstPassGeneration: " + allMetaClazzes);
        }
        for (MetaClazz metaClazz : allMetaClazzes) {
            javaCodeGenerationStrategy.preProcessMetaClazz(metaClazz);

            if (javaCodeGenerationStrategy.shouldGenerateForMetaClazz(metaClazz)) {
                log.trace("1. pass processing " + metaClazz.toString());
                metaClazzesToGenerateCodeFor.add(metaClazz);

                ClazzOrInterface clazz;
                if (metaClazz.isEnumerationClazz()) {
                    clazz = new Enumeration(metaClazz.getName(), true);
                } else if (metaClazz.isInterface()) {
                    clazz = new Interface(metaClazz.getName(), true);
                } else {
                    clazz = new Clazz(metaClazz.getName(), true);
                }
                clazz.setCodeModel(codeModel);
                if (metaClazz.getPackage() != null) {
                    clazz.setPackage(
                            codeModel.findChildWithName(CodePackage.class, metaClazz.getPackage().getFullPackageName())
                    );
                } else {
                    // Add to the model instead of to the package
                    codeModel.getChildren().add(clazz);
                }
                clazz.addAttribute(Clazz.METACLAZZ_ID, metaClazz.getId());
                clazz.setMetaType(metaClazz);

                if (getContext().isCreateExtensionClazzes() && !metaClazz.isEnumerationClazz() && !metaClazz.isInterface() && javaCodeGenerationStrategy.shouldGenerateExtensionClazzFor(clazz)
                        && !clazz.hasNoExtensionClassStereoType()) {
                    javaCodeGenerationStrategy.createExtensionClazz(codeModel, metaClazz, clazz);
                } else if (!getContext().isCreateExtensionClazzes() && !metaClazz.isEnumerationClazz() && !metaClazz.isInterface()
                        && javaCodeGenerationStrategy.shouldGenerateExtensionClazzFor(clazz) && clazz.hasExtensionClassStereoType()) {
                    javaCodeGenerationStrategy.createExtensionClazz(codeModel, metaClazz, clazz);
                }

                javaCodeGenerationStrategy.preProcessClazz(clazz, metaClazz);
            }
        }
        return metaClazzesToGenerateCodeFor;
    }


    /**
     * Generate the {@link ClazzEvent} for the {@link Clazz} (at this point we've passed {@link #firstPassGeneration(CodeModel, MetaModel)}
     * This method is responsible for handling all {@link MetaProperty}'s in the {@link MetaClazz} (in case we're working with a base clazz)
     * by either calling {@link #generateEnumerators(Enumeration)} in case or {@link MetaClazz} is a
     * {@link MetaEnumeration}, otherwise it calls {@link #generateFieldsAndProperties(ClazzOrInterface)}
     *
     * @param clazz The clazz to generate code for
     * @return The clazz that will be added to the list of generated clazzes (this allows you to swap instances if you need to)
     */
    protected Clazz processClazz(Clazz clazz) {
        // We wont generate properties for extension clazzes as this is only done in the base clazzes
        if (clazz.isExtensionClazz()) {
            return clazz;
        }

        if (clazz.isEnumeration()) {
            generateEnumerators((Enumeration) clazz);
        }
        generateFieldsAndProperties(clazz);
        generateMethods(clazz);
        return clazz;
    }

    /**
     * Generate the {@link InterfaceEvent} for the {@link Interface} (at this point we've passed
     * {@link #firstPassGeneration(CodeModel, MetaModel)} This method is responsible for handling all {@link MetaProperty}'s in the
     * {@link MetaInterface} (in case we're working with a base clazz) by either calling
     *
     * @param _interface The interface to generate code for
     * @return The interface to add to the list of generated classes/interfaces (allows you to swap instance if you need to)
     */
    protected Interface processInterface(Interface _interface) {
        generateFieldsAndProperties(_interface);
        generateMethods(_interface);
        return _interface;
    }

    /**
     * Generate {@link Method}'s for all {@link MetaOperation}'s in a {@link ClazzOrInterface}
     *
     * @param clazzOrInterface For this interface/class
     */
    protected void generateMethods(ClazzOrInterface clazzOrInterface) {
        List<MetaOperation> operations = clazzOrInterface.getMetaType().getOperations();
        if (log.isTraceEnabled()) {
            log.trace("ClazzOrInterface " + clazzOrInterface + " has operations: " + operations);
        }

        for (MetaOperation metaOperation : operations) {
            if (javaCodeGenerationStrategy.shouldGenerateForMetaOperation(metaOperation)) {
                log.trace("Generating operation " + metaOperation);
                new MethodEvent(clazzOrInterface, metaOperation).execute();
            }
        }
    }

    /**
     * create and execute {@link EnumeratorLiteralEvent} for each {@link MetaProperty} in the
     * {@link MetaEnumeration} that defined the {@link Enumeration}
     *
     * @param enumeration The Java {@link Enumeration}
     */
    protected void generateEnumerators(Enumeration enumeration) {
        for (MetaProperty metaProperty : enumeration.getMetaType().getProperties()) {
            if (javaCodeGenerationStrategy.shouldGenerateForEnumeratorProperty(metaProperty) && metaProperty.isEnumerationLiteral()) {
                EnumeratorLiteralEvent literalEvent = new EnumeratorLiteralEvent(enumeration, metaProperty);
                log.trace("2nd pass generation for Enum value '" + enumeration.getFQCN() + "#" + metaProperty.getName() + "'");
                literalEvent.execute();
            }
        }
    }


    /**
     * Create and execute {@link PropertyEvent} subtypes for each {@link MetaProperty} in the {@link MetaClazz} that defined the {@link Clazz}
     * by delegating to {@link #createPropertyEvent(ClazzOrInterface, MetaProperty)}
     *
     * @param clazz The {@link Clazz} to generate properties for
     */
    protected void generateFieldsAndProperties(ClazzOrInterface clazz) {
        for (MetaProperty metaProperty : clazz.getMetaType().getProperties()) {
            // Fix MagicDraw which sometimes screws up Clazz names (includes the package-name in the Class name)
            if (fixMagicDrawPackageNameInClassNameBug) {
                if (metaProperty.getType() != null && metaProperty.getType().getPackage() != null
                        && metaProperty.getType().getName().startsWith(metaProperty.getType().getPackage().getName())) {
                    if (metaProperty.getType().getName() != null && metaProperty.getType().getPackage().getName() != null
                            && metaProperty.getType().getPackage().getName().length() > 0
                            && metaProperty.getType().getName().length() > metaProperty.getType().getPackage().getName().length() + 1) {
                        metaProperty.getType().setName(
                                metaProperty.getType().getName().substring(metaProperty.getType().getPackage().getName().length() + 1));
                    }
                }
            }
            if (metaProperty.isPartInAnAssociation() && metaProperty.getAssociationType() == MetaProperty.AssociationType.ManyToMany) {
                javaCodeGenerationStrategy.resolveManyToManyBidirectionality(metaProperty);
            }


            if (!metaProperty.isEnumerationLiteral() && javaCodeGenerationStrategy.shouldGenerateForMetaProperty(metaProperty)) {
                PropertyEvent event = createPropertyEvent(clazz, metaProperty);
                log.trace("2nd pass generation for Property '" + clazz.getFQCN() + "#" + metaProperty.getName() + "#" + metaProperty.getId()
                        + "'. PropertyType: '" + event.getClass().getSimpleName() + "'");
                event.execute();
            }
        }
    }


    /**
     * Create a matching PropertyEvent for the given {@link MetaProperty}
     * <ul>
     * <li>{@link SimplePropertyEvent} for simple (Java builtin types)</li>
     * <li>{@link AssociationEvent} subtypes (such as {@link OneToOneAssociationEvent}, {@link OneToManyAssociationEvent},
     * {@link ManyToOneAssociationEvent} and {@link ManyToManyAssociationEvent}) for Associations</li>
     * </ul>
     *
     * @param clazz            The {@link Clazz} we're currently working on
     * @param thisMetaProperty The {@link MetaProperty} that belongs to the {@link Clazz} (through {@link Clazz}' {@link CodeElement#getMetaType()} relation
     *                         to {@link MetaClazz})
     * @return a {@link PropertyEvent} matching the given {@link MetaProperty}
     */
    protected PropertyEvent createPropertyEvent(ClazzOrInterface clazz, MetaProperty thisMetaProperty) {
        if (!thisMetaProperty.isPartInAnAssociation()) {
            return new SimplePropertyEvent(clazz, thisMetaProperty);
        }

        MetaProperty oppositeProperty = thisMetaProperty.getAssociation().getOppositeProperty(thisMetaProperty);
        switch (thisMetaProperty.getAssociationType()) {
            case ManyToOne:
                return new ManyToOneAssociationEvent(clazz, thisMetaProperty, oppositeProperty);
            case OneToOne:
                return new OneToOneAssociationEvent(clazz, thisMetaProperty, oppositeProperty);
            case ManyToMany:
                return new ManyToManyAssociationEvent(clazz, thisMetaProperty, oppositeProperty);
            case OneToMany:
                return new OneToManyAssociationEvent(clazz, thisMetaProperty, oppositeProperty);
            default:
                throw new IllegalStateException("Unknown AssociationType '" + thisMetaProperty.getAssociationType() + "'");
        }
    }

}
