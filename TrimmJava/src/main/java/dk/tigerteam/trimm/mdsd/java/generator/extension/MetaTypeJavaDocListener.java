/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.Field;
import dk.tigerteam.trimm.mdsd.java.codedom.Property;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.meta.MetaType;

/**
 * Writes out {@link MetaType} information to a {@link Clazz}, {@link Property} and {@link Field} as Javadoc, so the extension developer can see the relationship
 * between the MetaModel and the CodeModel (ie. the java code generated). It's very useful for extension debugging purposes.
 *
 * @author Jeppe Cramon
 */
public class MetaTypeJavaDocListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

    private boolean includeGenerationTimestamp = true;

    @Override
    protected boolean handleClazzEvent(ClazzEvent event) {
        StringBuilder requiredPropertiesDoc = getContext().getRequiredPropertiesJavaDoc(event.getClazz());
        requiredPropertiesDoc.append("<p/>").append(MetaType.newline);

        StringBuilder javadoc = new StringBuilder();
        javadoc.append((event.getClazz().getCodeDocumentation() != null ? event.getClazz().getCodeDocumentation() : "No Documentation") + "<p/>" + MetaType.newline
                + MetaType.newline + requiredPropertiesDoc.toString() + "Extra Meta info:<br/>");
        javadoc.append(MetaType.newline + event.getMetaClazz().getDescription() + "<br/>");
        if (includeGenerationTimestamp) {
            javadoc.append(MetaType.newline + "Generated: " + JavaGeneratorContext.getContext().getGenerationTimestamp().toString());
        }
        event.getClazz().setCodeDocumentation(javadoc.toString());
        return true;
    }

    @Override
    protected boolean handleEnumeratorEvent(EnumeratorLiteralEvent literalEvent) {
        String javaDoc = literalEvent.getField().getCodeDocumentation();
        javaDoc += "<p/>" + MetaType.newline + MetaType.newline + "Extra Meta info:<br/>" + MetaType.newline
                + literalEvent.getMetaProperty().getDescription();

        literalEvent.getField().setCodeDocumentation(javaDoc);
        return true;
    }

    protected boolean handlePropertyEvent(PropertyEvent event) {
        String javaDoc = event.getProperty().getField().getCodeDocumentation();
        javaDoc += "<p/>" + MetaType.newline + MetaType.newline + "Extra Meta info:<br/>" + MetaType.newline
                + event.getMetaProperty().getDescription();

        event.getProperty().getField().setCodeDocumentation(javaDoc);
        if (event.getProperty().getSetterMethod() != null)
            event.getProperty().getSetterMethod().setCodeDocumentation(javaDoc);
        if (event.getProperty().getGetterMethod() != null)
            event.getProperty().getGetterMethod().setCodeDocumentation(javaDoc);
        return true;
    }

    @Override
    protected boolean handleMethodEvent(MethodEvent event) {
        String javaDoc = event.getMethod().getCodeDocumentation();
        javaDoc += "<p/>" + MetaType.newline + MetaType.newline + "Extra Meta info:<br/>" + MetaType.newline
                + event.getOperation().getDescription();

        event.getMethod().setCodeDocumentation(javaDoc);
        return true;
    }

    @Override
    public boolean handle(GeneratorEvent event) {
        if (event instanceof PropertyEvent) {
            return handlePropertyEvent((PropertyEvent) event);
        } else {
            return super.handle(event);
        }
    }

    public boolean isIncludeGenerationTimestamp() {
        return includeGenerationTimestamp;
    }

    public MetaTypeJavaDocListener setIncludeGenerationTimestamp(boolean includeGenerationTimestamp) {
        this.includeGenerationTimestamp = includeGenerationTimestamp;
        return this;
    }

}
