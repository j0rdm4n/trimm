/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.codewriter;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaCodeGenerationPaths;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaConfiguration;
import dk.tigerteam.trimm.mdsd.java.generator.JavaStereotypes;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.GeneratorSummary.UpdateType;
import dk.tigerteam.trimm.util.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Common base class for all Code Generators
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 * @see JavaCodeWriter
 */
public abstract class AbstractCodeWriter {

    private static final Log logger = LogFactory.getLog(AbstractCodeWriter.class);

    private JavaCodeGenerationPaths javaCodeGenerationPaths;
    private PackageRewriter packageRewriter;
    private String rootDirectory;
    private GeneratorSummary summary;
    private String fileExtension;
    private boolean forceOverwriteGeneratedExtensionClasses = false;
    private Collection<String> excludeFormattingForPackages = new HashSet<String>();
    private boolean useImports;

    public AbstractCodeWriter(JavaCodeGenerationPaths javaCodeGenerationPaths, PackageRewriter packageRewriter, String rootDirectory, String... excludeFormattingForPackages) {
        this(javaCodeGenerationPaths, packageRewriter, rootDirectory, Arrays.asList(excludeFormattingForPackages));
    }

    public AbstractCodeWriter(JavaCodeGenerationPaths javaCodeGenerationPaths, PackageRewriter packageRewriter, String rootDirectory,
                              Collection<String> excludeFormattingForPackages) {
        this.javaCodeGenerationPaths = javaCodeGenerationPaths;
        this.packageRewriter = packageRewriter;
        this.rootDirectory = rootDirectory;
        this.excludeFormattingForPackages = excludeFormattingForPackages;
        this.summary = new GeneratorSummary();
    }

    public Collection<String> getExcludeFormattingForPackages() {
        return excludeFormattingForPackages;
    }

    // ==============================================================================================================

    public abstract List<File> writeCode(Collection<ClazzOrInterface> clazzesToWrite) throws IOException;

    protected abstract void writeFile(ClazzOrInterface clazz, File clazzFile) throws IOException;

    protected abstract String resolveDestionationFolder(ClazzOrInterface clazz);

    // ==============================================================================================================

    protected String resolveFullDirectoryPath(String path) {
        if (rootDirectory == null) {
            return path;
        } else if (path == null) {
            return null; // Safe guard so we never risk deleting all files in the root of some folder (like the root of the project)
        } else {
            if (path.startsWith("/") || path.startsWith("\\")) {
                return rootDirectory + path;
            } else {
                return rootDirectory + "/" + path;
            }
        }
    }

    protected String createPackageFolders(String fqcnPath, File destinationFolderFile) {
        String fullPath = destinationFolderFile.getAbsolutePath()
                + (destinationFolderFile.getAbsolutePath().endsWith(File.separator) ? "" : File.separator) + fqcnPath;
        int index = fullPath.lastIndexOf(File.separatorChar);
        if (index != -1) {
            String directoryPath = fullPath.substring(0, index);
            File packageFolder = new File(directoryPath);
            boolean mkdirs = packageFolder.mkdirs();
            if (!mkdirs) {
                logger.trace("Could not create package folder '" + packageFolder.getAbsolutePath() + "' probably because it already exists");
            } else {
                logger.trace("Created source package folder '" + packageFolder.getAbsolutePath() + "'");
            }
        }
        return fullPath;
    }

    protected File createDestinationFolder(String destinationFolder) {
        File destinationFolderFile = new File(destinationFolder);
        if (!destinationFolderFile.exists()) {
            boolean result = destinationFolderFile.mkdirs();
            if (!result) {
                logger.trace("Could not create destination folder '" + destinationFolderFile.getAbsolutePath()
                        + "' probably because it already exists");
            } else {
                logger.debug("Created source destination folder '" + destinationFolderFile.getAbsolutePath() + "'");
            }
        }
        return destinationFolderFile;
    }

    protected void rewritePackageNames(Collection<ClazzOrInterface> clazzesToWrite) {
        for (ClazzOrInterface clazz : clazzesToWrite) {
            if (packageRewriter != null && clazz.getPackage() != null) {
                packageRewriter.rewrite(clazz.getPackage());
            }
            String fqcn = clazz.getFQCN();
            if (fqcn == null) {
                throw new IllegalStateException("Couldn't resolve FQCN for Clazz based on MetaClazz with Id '"
                        + clazz.getAttribute(Clazz.METACLAZZ_ID) + "'");
            }
            logger.trace("Clazz '" + clazz.getName() + "' fqnc packagerewrite to '" + fqcn + "'");
        }
    }

    protected String generateFqcnPath(String fqcn, String fileExtension) {
        try {
            if (File.separator.equals("\\")) {
                return fqcn.replaceAll("\\.", File.separator + File.separator) + fileExtension;
            } else {
                return fqcn.replaceAll("\\.", File.separator) + fileExtension;
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed during replaceAll on '" + fqcn + "'", e);
        }
    }

    protected boolean shouldGenerateSourceFileForClazz(ClazzOrInterface clazz) {
        if ((clazz.getMetaType() != null && clazz.getMetaType().hasStereotypeUpwardsRecursively
                (JavaStereotypes.DONT_GENERATE_STEREOTYPE_NAME)) || (clazz.isClazz() && ((Clazz) clazz).getClazzStyle() == Clazz.ClazzStyle.SameFileClazz)) {
            return false;
        }
        return true;
    }

    protected boolean isUpdatable(ClazzOrInterface clazz) {
        if (clazz.isClazz()) {
            if (((Clazz) clazz).isExtensionClazz()) {
                return forceOverwriteGeneratedExtensionClasses;
            } else {
                return true;
            }
        }
        return true;
    }

    protected void updateSummary(ClazzOrInterface clazz, UpdateType updateType) {
        switch (updateType) {
            case New:
                if (clazz.isClazz()) {
                    Clazz c = (Clazz) clazz;
                    if (c.isBaseClazz()) {
                        getSummary().addToNumberOfNewBaseClasses();
                        getSummary().addNewBaseClassName(clazz.getFQCN());
                    } else if (c.isExtensionClazz()) {
                        getSummary().addToNumberOfNewExtensionClasses();
                        getSummary().addNewExtensionClassName(clazz.getFQCN());
                    } else if (clazz.isEnumeration()) {
                        getSummary().addToNumberOfNewEnumerationClasses();
                        getSummary().addNewEnumerationName(clazz.getFQCN());
                    } else if (clazz.isTestClazz()) {
                        getSummary().addToNumberOfNewTestClasses();
                        getSummary().addNewTestClassName(clazz.getFQCN());
                    } else {
                        getSummary().addToNumberOfNewBaseClasses();
                        getSummary().addNewBaseClassName(clazz.getFQCN());
                    }
                } else if (clazz.isInterface()) {
                    getSummary().addToNumberOfNewInterfaceClasses();
                    getSummary().addNewInterfaceName(clazz.getFQCN());
                }
                break;
            case NoUpdate:
                if (clazz.isClazz()) {
                    Clazz c = (Clazz) clazz;
                    if (c.isBaseClazz()) {
                        getSummary().addToNumberOfNotUpdatedBaseClasses();
                    } else if (c.isExtensionClazz()) {
                        getSummary().addToNumberOfNotUpdatedExtensionClasses();
                    } else if (clazz.isEnumeration()) {
                        getSummary().addToNumberOfNotUpdatedEnumerationClasses();
                    } else if (clazz.isTestClazz()) {
                        getSummary().addToNumberOfNotUpdatedTestClasses();
                    } else {
                        getSummary().addToNumberOfNotUpdatedBaseClasses();
                    }
                } else if (clazz.isInterface()) {
                    getSummary().addToNumberOfNotUpdatedInterfaceClasses();
                }
                break;
            case Update:
                if (clazz.isClazz()) {
                    Clazz c = (Clazz) clazz;
                    if (c.isBaseClazz()) {
                        getSummary().addToNumberOfUpdatedBaseClasses();
                        getSummary().addUpdatedBaseClassName(clazz.getFQCN());
                    } else if (c.isExtensionClazz()) {
                        getSummary().addToNumberOfUpdatedExtensionClasses();
                        getSummary().addUpdatedExtensionClassName(clazz.getFQCN());
                    } else if (clazz.isEnumeration()) {
                        getSummary().addToNumberOfUpdatedEnumerationClasses();
                        getSummary().addUpdatedEnumerationName(clazz.getFQCN());
                    } else if (clazz.isTestClazz()) {
                        getSummary().addToNumberOfUpdatedTestClasses();
                        getSummary().addUpdatedTestClassName(clazz.getFQCN());
                    } else {
                        getSummary().addToNumberOfUpdatedBaseClasses();
                        getSummary().addUpdatedBaseClassName(clazz.getFQCN());
                    }
                } else if (clazz.isInterface()) {
                    getSummary().addToNumberOfUpdatedInterfaceClasses();
                    getSummary().addUpdatedInterfaceName(clazz.getFQCN());
                }
                break;
            default:
                break;
        }
    }

    protected boolean isFileContentEqual(File file, File temp) throws IOException {
        logger.trace("Comparing exiting file '" + file.getName() + " : " + file.length() + "' content with temp file '"
                + temp.getName() + " : " + temp.length() + "' content");
        if (Utils.isFileStringContentEqual(file, temp, JavaConfiguration.DEFAULT_ENCODING)) {
            return true;
        }
        return false;
    }

    // ==============================================================================================================

    public String getGenerateInterfacesToDirectory() {
        return resolveFullDirectoryPath(javaCodeGenerationPaths.getGenerateInterfacesToPath());
    }

    public String getGenerateClazzesAndBaseClazzesToDirectory() {
        return resolveFullDirectoryPath(javaCodeGenerationPaths.getGenerateBaseClassesToPath());
    }

    public String getGenerateExtensionClazzesToDirectory() {
        return resolveFullDirectoryPath(javaCodeGenerationPaths.getGenerateExtensionClassesToPath());
    }

    public String getGenerateTestClazzesToDirectory() {
        return resolveFullDirectoryPath(javaCodeGenerationPaths.getGenerateTestClassesToPath());
    }

    public JavaCodeGenerationPaths getJavaCodeGenerationPaths() {
        return javaCodeGenerationPaths;
    }

    public void setJavaCodeGenerationPaths(JavaCodeGenerationPaths configuration) {
        this.javaCodeGenerationPaths = configuration;
    }

    public boolean isForceOverwriteGeneratedExtensionClasses() {
        return forceOverwriteGeneratedExtensionClasses;
    }

    public void setForceOverwriteGeneratedExtensionClasses(boolean forceOverwriteGeneratedExtensionClasses) {
        this.forceOverwriteGeneratedExtensionClasses = forceOverwriteGeneratedExtensionClasses;
    }

    public PackageRewriter getPackageRewriter() {
        return packageRewriter;
    }

    public void setPackageRewriter(PackageRewriter packageRewriter) {
        this.packageRewriter = packageRewriter;
    }

    public String getRootDirectory() {
        return rootDirectory;
    }

    public void setRootDirectory(String rootDirectory) {
        this.rootDirectory = rootDirectory;
    }

    public GeneratorSummary getSummary() {
        return summary;
    }

    public boolean isUseImports() {
        return useImports;
    }

    public void setUseImports(boolean useImports) {
        this.useImports = useImports;
    }

    /**
     * The extension (including the start dot) - e.g. <code>.java</code> or <code>.groovy</code
     */
    public String getFileExtension() {
        return fileExtension;
    }

    /**
     * The extension (including the start dot) - e.g. <code>.java</code> or <code>.groovy</code>
     */
    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

}
