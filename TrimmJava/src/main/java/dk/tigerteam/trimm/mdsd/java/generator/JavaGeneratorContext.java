/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.exception.FindException;
import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.DefaultPackageRewriter;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import dk.tigerteam.trimm.util.IndentPrintWriter;

import java.io.StringWriter;
import java.util.*;

/**
 * A {@link JavaGeneratorContext} contains are data and configuration (like registered {@link GeneratorEventListener}') for a given
 * generation process.
 * <p/>
 * The {@link JavaGeneratorContext} is bound to the current thread and can be fetched using {@link JavaGeneratorContext#getContext()}
 *
 * @author Jeppe Cramon
 */
public class JavaGeneratorContext {

    private static final ThreadLocal<JavaGeneratorContext> context = new ThreadLocal<JavaGeneratorContext>();

    private final MetaModel metaModel;
    private CodeModel codeModel = new CodeModel();
    private List<GeneratorEventListener> eventListeners = new LinkedList<GeneratorEventListener>();
    private CollectionTypeResolver collectionTypeResolver = new JavaCollectionTypeResolver();
    private JavaInheritanceAndClazzesStrategy inheritanceStrategy = new DefaultJavaInheritanceAndClazzesStrategy();
    private boolean createExtensionClazzes;
    private boolean addExtensionAndBaseClazzAnnotations;
    private String collectionPropertyNameSuffix = "Collection";
    private boolean addDefaultConstructor;
    private List<DeferredEventHandler> deferredEventHandlerList = new LinkedList<DeferredEventHandler>();
    private Map<Clazz, StringBuilder> clazzRequiredPropertiesJavaDocCache = new HashMap<Clazz, StringBuilder>();
    private Map<ClazzOrInterface, String> clazzHierarchyJavaDocCache = new HashMap<ClazzOrInterface, String>();
    private Map<Clazz, Clazz> baseClazzToExtensionClazzMap = new HashMap<Clazz, Clazz>();
    private Date generationTimestamp = new Date();
    private boolean generateIsPropertyMethodForBooleanWrapperTypeProperties;


    // ------------------------------------------------------------------------------------------------------------

    /**
     * Get the currently active {@link JavaGeneratorContext} execution (is multithread safe)
     */
    public static JavaGeneratorContext getContext() {
        return context.get();
    }

    /**
     * Get the currently active {@link JavaGeneratorContext} execution (is multithread safe) but cast to your preferred subclass
     *
     * @throws IllegalStateException In case there was a {@link ClassCastException} while cast casting to your preferred subclass
     */
    @SuppressWarnings("unchecked")
    public static <T extends JavaGeneratorContext> T getContext(Class<T> contextType) {
        try {
            return (T) getContext();
        } catch (ClassCastException e) {
            throw new IllegalStateException("You need to use a " + contextType.getName() + " class (or a subclass thereof) as Context class");
        }
    }

    public static void setContext(JavaGeneratorContext generatorContext) {
        context.set(generatorContext);
    }


    public static void removeContext() {
        context.remove();
    }

    // ------------------------------------------------------------------------------------------------------------

    /**
     * Define a new JavaGeneratorContext - defaults to using an empty {@link CodeModel}
     * and {@link DefaultPackageRewriter}, {@link JavaCollectionTypeResolver}, {@link DefaultJavaInheritanceAndClazzesStrategy},
     * while setting {@link #isCreateExtensionClazzes()}, {@link #isAddExtensionAndBaseClazzAnnotations()}  and
     * {@link #isAddDefaultConstructor()} to <code>false</code>.<br/>
     * Default {@link #getCollectionPropertyNameSuffix()} is <code>Collection</code>
     *
     * @param metaModel The MetaModel to use for the Code generation
     */
    public JavaGeneratorContext(MetaModel metaModel) {
        this.metaModel = metaModel;
    }

    public JavaGeneratorContext(MetaModel metaModel, CollectionTypeResolver collectionTypeResolver, JavaInheritanceAndClazzesStrategy inheritanceStrategy, GeneratorEventListener... eventListeners) {
        this.metaModel = metaModel;
        this.collectionTypeResolver = collectionTypeResolver;
        this.inheritanceStrategy = inheritanceStrategy;
        if (eventListeners != null)
            this.eventListeners.addAll(Arrays.asList(eventListeners));
    }

    /**
     * Define a new JavaGeneratorContext
     *
     * @param metaModel                    The MetaModel to use for the Code generation
     * @param codeModel                    The CodeModel to use for the result of the CodeGeneration
     * @param collectionTypeResolver       The Collection type result to use for the Code Generation
     * @param inheritanceStrategy          The inheritance strategy to use for the Code Generation
     * @param createExtensionClazzes       Should we create extension classes for every MetaClazz (aka. Generator Gap Pattern) -
     *                                     An extension clazz is a Clazz that extends from the {@link dk.tigerteam.trimm.mdsd.java.codedom.Clazz} that we create on behalf of a {@link dk.tigerteam.trimm.mdsd.meta.MetaClazz}, in this case known as
     *                                     a base clazz. The base clazz is then renamed (default to "Abstract"+baseClazz.getName()) and made abstract). The extension clazz will
     *                                     be named according to the original MetaClazz.<br/> <i>Note: The {@link dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext} contains the base Clazz to Extension Clazz mapping. </i>
     * @param addExtensionAndBaseClazzAnnotations
     *                                     Should Extensions Classes and Base Classes be annotated (e.g. required for some unit testing generation)
     * @param collectionPropertyNameSuffix What suffix should collections end with - default is "Collection"
     * @param addDefaultConstructor        Should a default NoArg constructor be inserted (in case there are no immutable properties)?
     * @param eventListeners               Event listeners
     */
    public JavaGeneratorContext(MetaModel metaModel, CodeModel codeModel,
                                CollectionTypeResolver collectionTypeResolver,
                                JavaInheritanceAndClazzesStrategy inheritanceStrategy, boolean createExtensionClazzes,
                                boolean addExtensionAndBaseClazzAnnotations, String collectionPropertyNameSuffix,
                                boolean addDefaultConstructor, GeneratorEventListener... eventListeners) {
        this.metaModel = metaModel;
        this.codeModel = codeModel;
        if (eventListeners != null)
            this.eventListeners.addAll(Arrays.asList(eventListeners));
        this.collectionTypeResolver = collectionTypeResolver;
        this.inheritanceStrategy = inheritanceStrategy;
        this.createExtensionClazzes = createExtensionClazzes;
        this.addExtensionAndBaseClazzAnnotations = addExtensionAndBaseClazzAnnotations;
        this.collectionPropertyNameSuffix = collectionPropertyNameSuffix;
        this.addDefaultConstructor = addDefaultConstructor;
    }

    public void setCodeModel(CodeModel codeModel) {
        this.codeModel = codeModel;
    }

    public boolean isAddExtensionAndBaseClazzAnnotations() {
        return addExtensionAndBaseClazzAnnotations;
    }

    public void setAddExtensionAndBaseClazzAnnotations(boolean addExtensionAndBaseClazzAnnotations) {
        this.addExtensionAndBaseClazzAnnotations = addExtensionAndBaseClazzAnnotations;
    }

    public List<GeneratorEventListener> getEventListeners() {
        return eventListeners;
    }

    public void addAsFirstEventListener(GeneratorEventListener eventListener) {
        ((LinkedList<GeneratorEventListener>) getEventListeners()).addFirst(eventListener);
    }

    public void addAsLastEventListener(GeneratorEventListener eventListener) {
        if (eventListener instanceof DeferredGeneratorEventListener) {
            ((LinkedList<GeneratorEventListener>) getEventListeners()).addLast(new DeferredEventHandlerAdapter(eventListener));
        } else {
            ((LinkedList<GeneratorEventListener>) getEventListeners()).addLast(eventListener);
        }
    }

    /**
     * Add one or more {@link GeneratorEventListener}' to this {@link JavaGenerator}
     *
     * @param generatorEventListeners the {@link GeneratorEventListener}' to register with this {@link JavaGenerator}
     * @return This {@link JavaGenerator} instance
     */
    public JavaGeneratorContext addEventListeners(GeneratorEventListener... generatorEventListeners) {
        for (GeneratorEventListener generatorEventListener : generatorEventListeners) {
            if (generatorEventListener instanceof DeferredGeneratorEventListener) {
                this.eventListeners.add(new DeferredEventHandlerAdapter(generatorEventListener));
            } else {
                this.eventListeners.add(generatorEventListener);
            }
        }
        return this;
    }

    public String getCollectionPropertyNameSuffix() {
        return collectionPropertyNameSuffix;
    }

    public void setCollectionPropertyNameSuffix(String collectionPropertyNameSuffix) {
        if (collectionPropertyNameSuffix == null) {
            this.collectionPropertyNameSuffix = "";
        } else {
            this.collectionPropertyNameSuffix = collectionPropertyNameSuffix;
        }
    }

    public void setCreateExtensionClazzes(boolean createExtensionClazzes) {
        this.createExtensionClazzes = createExtensionClazzes;
    }

    public void setAddDefaultConstructor(boolean addDefaultConstructor) {
        this.addDefaultConstructor = addDefaultConstructor;
    }

    public Date getGenerationTimestamp() {
        return generationTimestamp;
    }

    public boolean isAddDefaultConstructor() {
        return addDefaultConstructor;
    }

    /**
     * Should we create extension classes for every MetaClazz (aka. Generator Gap Pattern) -
     * An extension clazz is a Clazz that extends from the {@link Clazz} that we create on behalf of a {@link MetaClazz}, in this case known as
     * a base clazz. The base clazz is then renamed (default to "Abstract"+baseClazz.getName()) and made abstract). The extension clazz will
     * be named according to the original MetaClazz.<br/> <i>Note: The {@link JavaGeneratorContext} contains the base Clazz to Extension Clazz mapping. </i>
     */
    public boolean isCreateExtensionClazzes() {
        return createExtensionClazzes;
    }

    public JavaInheritanceAndClazzesStrategy getInheritanceStrategy() {
        return inheritanceStrategy;
    }

    public void setInheritanceStrategy(JavaInheritanceAndClazzesStrategy inheritanceStrategy) {
        this.inheritanceStrategy = inheritanceStrategy;
    }

    /**
     * Get the raw map of the base clazz to extension clazz mapping
     */
    protected Map<Clazz, Clazz> getBaseClazzToExtensionClazzMap() {
        return baseClazzToExtensionClazzMap;
    }

    public Set<Clazz> getAllBaseClazzes() {
        return Collections.unmodifiableSet(getBaseClazzToExtensionClazzMap().keySet());
    }

    /**
     * Register the mapping between baseClazz and extensionClazz so that we can use it later
     *
     * @param baseClazz      The base {@link Clazz}
     * @param extensionClazz The extension {@link Clazz} for the base clazz
     * @return this
     */
    public JavaGeneratorContext registerBaseClazzAndExtensionClazzPair(Clazz baseClazz, Clazz extensionClazz) {
        baseClazzToExtensionClazzMap.put(baseClazz, extensionClazz);
        return this;
    }

    /**
     * Get the extension {@link Clazz} for the given base {@link Clazz}
     *
     * @param baseClazz The base Clazz for which we want the extension clazz
     * @return The extension Clazz for the given base Clazz
     * @throws IllegalArgumentException In case the is no mappin registered between the base Clazz and an extension Clazz
     */
    public Clazz getExtensionClazzForBaseClazz(Clazz baseClazz) {

        for (Map.Entry<Clazz, Clazz> entry : baseClazzToExtensionClazzMap.entrySet()) {
            if (entry.getKey().equals(baseClazz)) {
                return entry.getValue();
            }
        }

        throw new IllegalArgumentException("Couldn't find an extension Clazz registration for base Clazz " + baseClazz);
    }

    /**
     * Create a HTML formatted Java doc string that documents which properties are required (i.e. properties with
     * {@link CardinalityType#SingleRequired}) sorted alphabetically<br/>
     * Note: Should be called when the class if fully created with all fields and properties (usually in a {@link DeferredEventHandler})
     *
     * @param clazz The clazz for which we want all required properties
     * @return The required parameters formatted in HTML
     */
    public StringBuilder getRequiredPropertiesJavaDoc(Clazz clazz) {
        if (clazzRequiredPropertiesJavaDocCache.containsKey(clazz)) {
            return clazzRequiredPropertiesJavaDocCache.get(clazz);
        }

        List<Property> requiredProperties = getRequiredProperties(clazz);

        StringBuilder requiredPropertiesDoc = new StringBuilder("<u>Required properties:</u> ");
        int index = 0;
        for (Property property : requiredProperties) {
            if (index > 0) {
                requiredPropertiesDoc.append(", ");
            }
            requiredPropertiesDoc.append("<b>");
            requiredPropertiesDoc.append(property.getName());
            requiredPropertiesDoc.append("</b>");
            index++;
        }
        if (index == 0) {
            requiredPropertiesDoc.append("<i>none</i>");
        }

        clazzRequiredPropertiesJavaDocCache.put(clazz, requiredPropertiesDoc);
        return requiredPropertiesDoc;
    }

    public String getHierarchyAsHtml(ClazzOrInterface clazzOrInterface) {
        if (clazzOrInterface.isInterface()) {
            return getHierarchyAsHtml((Interface) clazzOrInterface);
        } else {
            return getHierarchyAsHtml((Clazz) clazzOrInterface);
        }
    }

    public String getHierarchyAsHtml(Interface _interface) {
        if (clazzHierarchyJavaDocCache.containsKey(_interface)) {
            return clazzHierarchyJavaDocCache.get(_interface);
        }

        return "&lt;Not implemented yet>";
    }

    public String getHierarchyAsHtml(Clazz clazz) {
        if (clazzHierarchyJavaDocCache.containsKey(clazz)) {
            return clazzHierarchyJavaDocCache.get(clazz);
        }

        boolean includeSuperClazzes = true;
        boolean highlightClazz = true;

        StringWriter stringWriter = new StringWriter();
        IndentPrintWriter writer = new IndentPrintWriter(stringWriter);

        Clazz rootClazz = clazz;
        if (includeSuperClazzes) {
            rootClazz = codeModel.getRootSuperClazz(clazz);
        }
        writer.println("<ul>");
        writer.indent();
        recursivelyWriteClazzHierarchy(writer, rootClazz, highlightClazz ? clazz : null);
        writer.outdent();
        writer.println("</ul>");

        // clazzHierarchyJavaDocCache.put(clazz, stringWriter.toString());
        return stringWriter.toString();
    }

    private void recursivelyWriteClazzHierarchy(IndentPrintWriter writer, Clazz clazz, Clazz highlightClazz) {
        writer.print("<li>");
        String clazzName = clazz.getName();
        if (clazz.hasAccessModifier(AccessModifier.Abstract)) {
            clazzName = "<i>" + clazzName + " (Abstract)</i>";
        }

        if (clazz == highlightClazz) {
            writer.print("<b>" + clazzName + "</b>");
        } else {
            writer.print(clazzName);
        }
        writer.println("</li>");

        List<Clazz> subClazzesOfClazz = codeModel.findAllDirectSubClazzesOfClazz(clazz);
        if (subClazzesOfClazz.size() > 0) {
            writer.println("<ul>");
            writer.indent();
            for (Clazz subClazz : subClazzesOfClazz) {
                recursivelyWriteClazzHierarchy(writer, subClazz, highlightClazz);
            }
            writer.outdent();
            writer.println("</ul>");
        }
    }

    /**
     * Get all required properties (i.e. properties with {@link CardinalityType#SingleRequired}) formatted alphabetically (looks in this clazz
     * and all super clazzes).<br/>
     * Note: Should be called when the class if fully created with all fields and properties (usually in a {@link DeferredEventHandler})
     *
     * @param clazz The clazz for which we want all required properties
     * @return The required properties
     */
    public List<Property> getRequiredProperties(Clazz clazz) {
        SortedSet<Property> properties = new TreeSet<Property>(new Comparator<Property>() {
            public int compare(Property o1, Property o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        properties.addAll(clazz.getAllPropertiesIncludingSuperClazzProperties());

        List<Property> result = new LinkedList<Property>();
        for (Property property : properties) {
            if (property.getMetaType().getCardinalityType() == CardinalityType.SingleRequired) {
                result.add(property);
            }
        }
        return result;
    }

    /**
     * Get the MetaModel that we are processing
     */
    public MetaModel getMetaModel() {
        return metaModel;
    }

    /**
     * Call this method to queue a {@link DeferredEventHandler} for later execution. The {@link JavaGenerator} determines when
     * {@link DeferredEventHandler}' are executed, please its documentation for details.
     *
     * @param deferredEventHandler The {@link DeferredEventHandler} to queue
     * @return This context instance
     */
    public JavaGeneratorContext queueDeferredEventHandler(DeferredEventHandler deferredEventHandler) {
        deferredEventHandlerList.add(deferredEventHandler);
        return this;
    }

    /**
     * Get all {@link DeferredEventHandler}' that have been queued
     */
    public List<DeferredEventHandler> getQueuedDeferredEventHandlers() {
        return deferredEventHandlerList;
    }

    /**
     * Get the {@link CollectionTypeResolver}.
     */
    public CollectionTypeResolver getCollectionTypeResolver() {
        return collectionTypeResolver;
    }

    /**
     * Set the {@link CollectionTypeResolver}.
     *
     * @param collectionTypeResolver The {@link CollectionTypeResolver} to use
     */
    public void setCollectionTypeResolver(CollectionTypeResolver collectionTypeResolver) {
        this.collectionTypeResolver = collectionTypeResolver;
    }

    /**
     * Get the root {@link CodeModel} object
     */
    public CodeModel getCodeModel() {
        return codeModel;
    }

    /**
     * Search the {@link CodeModel} (see {@link #getCodeModel}) and its children recursively using the {@link Criteria}.
     *
     * @param <T>      The type of elements to find (or subtypes thereof)
     * @param criteria The criteria to use
     * @return The elements found
     * @see CodeModel#find(Criteria)
     */
    public <T extends CodeElement> List<T> find(Criteria<T> criteria) {
        return codeModel.find(criteria);
    }

    /**
     * Find a single child of the given type and with the given name. We search in {@link CodeModel} (see {@link #getCodeModel}) and all its
     * children recursively.
     *
     * @param <T>         The type of element to find (or a subtype thereof)
     * @param elementType The type of element that we want to find
     * @param name        The name of the {@link CodeElement} that we want to find
     * @return The element found or <code>null</code> if no matching element could be found
     * @throws FindException in case we found more than one matching elements
     */
    public <T extends CodeElement> T findChildWithName(Class<T> elementType, String name) {
        return codeModel.findChildWithName(elementType, name);
    }

    /**
     * Search the {@link CodeModel} (see {@link #getCodeModel}) and all its children recursively, using the {@link Criteria}, <b>for a single
     * matching element</b>
     *
     * @param <T>      The type of element to find (or a subtype thereof)
     * @param criteria The criteria to use
     * @return The single element found or <code>null</code> if no matching element could be found
     * @throws FindException in case we found more than one matching elements
     */
    public <T extends CodeElement> T findSingle(Criteria<T> criteria) {
        return codeModel.findSingle(criteria);
    }

    /**
     * Search the {@link CodeModel} (see {@link #getCodeModel}) for a {@link Clazz} that corresponds to the {@link MetaClazz} provided as
     * parameter (will never return extension clazzes)
     *
     * @param metaClazz The {@link MetaClazz} that we want to find the corresponding {@link Clazz} for
     * @return The {@link Clazz} found or <code>null</code> if no matching {@link Clazz} could be found
     */
    public ClazzOrInterface findClazzFromMetaClazz(final MetaClazz metaClazz) {
        ClazzOrInterface clazz = codeModel.findSingle(new Criteria<ClazzOrInterface>(ClazzOrInterface.class) {
            public boolean isOk(ClazzOrInterface clazz) {
                boolean isExtensionClazz = false;
                if (clazz.isClazz()) {
                    isExtensionClazz = ((Clazz) clazz).isExtensionClazz();
                }
                return (metaClazz.equals(clazz.getMetaType()) && !isExtensionClazz);
            }
        });
        return clazz;
    }

    /**
     * Search the {@link CodeModel} (see {@link #getCodeModel}) for a {@link Clazz} that corresponds to the {@link MetaClazz} provided as
     * parameter (will never return extension clazzes)
     *
     * @param metaClazz The {@link MetaClazz} that we want to find the corresponding {@link Clazz} for
     * @return The {@link Clazz} found
     * @throws FindException if we didn't find a matching {@link Clazz}
     */
    public ClazzOrInterface getClazzFromMetaClazz(final MetaClazz metaClazz) {
        ClazzOrInterface clazz = findClazzFromMetaClazz(metaClazz);
        if (clazz == null) {
            throw new FindException("Failed to find Clazz for metaClazz '" + metaClazz.getFQCN() + "' (id: " + metaClazz.getId() + ") - "
                    + "Contains the following Clazz's " + getCodeModel().findChildrenOfType(Clazz.class).toString());
        } else {
            return clazz;
        }
    }

    /**
     * Search the {@link CodeModel} (see {@link #getCodeModel}) for a {@link Clazz} that corresponds to the {@link MetaClazz} provided as
     * parameter (will never return extension clazzes)
     *
     * @param metaClazz The {@link MetaClazz} that we want to find the corresponding {@link Clazz} for
     * @return The {@link Clazz} found or <code>null</code> if no matching {@link Clazz} could be found
     */
    public Interface findInterfaceFromMetaClazz(final MetaClazz metaClazz) {
        Interface clazz = codeModel.findSingle(new Criteria<Interface>(Interface.class) {
            public boolean isOk(Interface clazz) {
                return (metaClazz.equals(clazz.getMetaType()));
            }
        });
        return clazz;
    }

    /**
     * Search the {@link CodeModel} (see {@link #getCodeModel}) for a {@link Interface} that corresponds to the {@link MetaClazz} provided as
     * parameter (will never return extension clazzes)
     *
     * @param metaClazz The {@link MetaClazz} that we want to find the corresponding {@link Interface} for
     * @return The {@link Interface} found
     * @throws FindException if we didn't find a matching {@link Interface}
     */
    public Interface getInterfaceFromMetaClazz(final MetaClazz metaClazz) {
        Interface clazz = findInterfaceFromMetaClazz(metaClazz);
        if (clazz == null) {
            throw new FindException("Failed to find Clazz for metaClazz '" + metaClazz.getFQCN() + "' (id: " + metaClazz.getId() + ") - "
                    + "Contains the following Clazz's " + getCodeModel().findChildrenOfType(Interface.class).toString());
        } else {
            return clazz;
        }
    }

    /**
     * Broadcast given event to all registered {@link GeneratorEventListener}'
     *
     * @param generatorEvent The event to broadcast
     */
    public void broadcastEvent(GeneratorEvent generatorEvent) {
        for (GeneratorEventListener eventListener : eventListeners) {
            if (!eventListener.handle(generatorEvent))
                break;
        }
    }

    public boolean generateIsPropertyMethodForBooleanWrapperTypeProperties() {
      return generateIsPropertyMethodForBooleanWrapperTypeProperties;
    }

    public void setGenerateIsPropertyMethodForBooleanWrapperTypeProperties(boolean generateIsPropertyMethodForBooleanWrapperTypeProperties) {
        this.generateIsPropertyMethodForBooleanWrapperTypeProperties = generateIsPropertyMethodForBooleanWrapperTypeProperties;
    }
}
