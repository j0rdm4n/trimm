/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import java.util.List;

/**
 * The root object in the {@link CodeModel}/Dom hierarchy (instance wise)
 *
 * @author Jeppe Cramon
 */
public class CodeModel extends CodeComposite {

    public enum Filter {
        ExcludeAbstractClazzes, ExcludeExtensionClazzes, ExcludeBaseClazzes
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		throw new IllegalArgumentException("Don't call us");
//	}

    public List<Clazz> findAllDirectSubClazzesOfClazz(final Clazz superClazz) {
        return find(new Criteria<Clazz>(Clazz.class) {
            @Override
            public boolean isOk(Clazz element) {
                return element.getExtendsType() != null && element.getExtendsType().getClazz() == (superClazz);
            }
        });
    }

    public List<Clazz> findAllDirectSubClazzesOfClazz(final Clazz superClazz, final Filter... filters) {
        return find(new Criteria<Clazz>(Clazz.class) {
            @Override
            public boolean isOk(Clazz element) {
                if (element.getExtendsType() != null && element.getExtendsType().getClazz() == (superClazz)) {
                    return applyFilters(element, filters);
                } else {
                    return false;
                }
            }
        });
    }

    public List<Clazz> findAllSubClazzesOfClazz(final Clazz superClazz, final boolean excludeAbstractButNotBaseClazzez) {
        return find(new Criteria<Clazz>(Clazz.class) {
            @Override
            public boolean isOk(Clazz element) {
                if (excludeAbstractButNotBaseClazzez && element.getAccessModifiers().contains(AccessModifier.Abstract)) {
                    return element.isSubClassOfClazz(superClazz) && element.isBaseClazz();
                } else {
                    return element.isSubClassOfClazz(superClazz);
                }
            }
        });
    }

    public List<Clazz> findAllSubClazzesOfClazz(final Clazz superClazz, final Filter... filters) {
        return find(new Criteria<Clazz>(Clazz.class) {
            @Override
            public boolean isOk(Clazz element) {
                if (element.isSubClassOfClazz(superClazz)) {
                    return applyFilters(element, filters);
                } else {
                    return false;
                }
            }
        });
    }

    public Clazz findClazzByMetaTypeId(final String metaTypeId, final Filter... filters) {
        List<Clazz> result = find(new Criteria<Clazz>(Clazz.class) {
            @Override
            public boolean isOk(Clazz element) {
                return element.getMetaType() != null && element.getMetaType().getId().equals(metaTypeId) && applyFilters(element, filters);
            }
        });
        if (result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public Clazz getClazzByMetaTypeId(final String metaTypeId, final Filter... filters) {
        Clazz result = findClazzByMetaTypeId(metaTypeId, filters);
        if (result != null) {
            return result;
        } else {
            throw new IllegalArgumentException("Couldn't find Clazz by its metaTypeId '" + metaTypeId + "'");
        }
    }

    protected boolean applyFilters(Clazz clazz, final Filter... filters) {
        boolean include = true;
        for (Filter filter : filters) {
            switch (filter) {
                case ExcludeAbstractClazzes:
                    include &= !clazz.hasAccessModifier(AccessModifier.Abstract);
                    break;
                case ExcludeBaseClazzes:
                    include &= !clazz.isBaseClazz();
                    break;
                case ExcludeExtensionClazzes:
                    include &= !clazz.isExtensionClazz();
                    break;
            }
        }
        return include;
    }

    /**
     * Get the top most super {@link Clazz} in given clazz parameters hierarchy (note doesn't consider other {@link Class}'s like a Mapped
     * Superclass, which isn't defined as a Clazz)
     *
     * @param clazz The clazz for which we want to the Root super clazz
     * @return the top most super {@link Clazz} in given clazz parameters hierarchy (or ourselves if we're the top level Clazz)
     */
    public Clazz getRootSuperClazz(Clazz clazz) {
        Clazz rootClazz = clazz;
        while (rootClazz.getExtendsType() != null && rootClazz.getExtendsType().getClazz() != null) {
            rootClazz = rootClazz.getExtendsType().getClazz();
        }
        return rootClazz;
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
