/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.bean.bidirectional.*;
import dk.tigerteam.trimm.collection.WrappedCollection;
import dk.tigerteam.trimm.collection.WrappedList;
import dk.tigerteam.trimm.collection.WrappedSet;
import dk.tigerteam.trimm.collection.WrappedSortedSet;
import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.util.Utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

/**
 * This listener is responsible for wrapping all bidirectional relationships between properties, by
 * using specialized wrapper classes (such as, but not limited to, {@link OneToOneWrapper}, {@link ManyToOneWrapper}, {@link OneToOneWrapper},
 * {@link OneToManyCollectionWrapper}, {@link ManyToManyCollectionWrapper}).
 * <p/>
 * This ensures that if you have defined a bi-directional association between two classes in your Metamodel then the generated java code
 * will ensure that the code behaves accordingly (ie. it acts bi-directionally in-memory), so you don't have to maintain bi-directionality yourself.
 * <p/>
 * <pre>
 * With the following UML definition
 * ---------                ---------
 * |       | RoleA    RoleB |       |
 * |   A   |________________|   B   |
 * |       |                |       |
 * ---------                ---------
 * </pre>
 * Then the following code assertion will hold true
 * <pre>
 *     A a = new A();
 *     B b = new B();
 *     a.setRoleB(b);
 *     assert b.getRoleA() == a
 * </pre>
 *
 * @author Jeppe Cramon
 */
public class BidirectionalGeneratorEventListener extends
        AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

    /**
     * This lister is responsible for wrapping all bidirectional relationships between properties, by
     * using specialized wrapper classes (such as, but not limited to, {@link OneToOneWrapper}, {@link ManyToOneWrapper}, {@link OneToOneWrapper},
     * {@link OneToManyCollectionWrapper}, {@link ManyToManyCollectionWrapper}).
     */
    public BidirectionalGeneratorEventListener() {
    }

    @Override
    protected boolean handleManyToManyAssociationEvent(
            ManyToManyAssociationEvent event) {
        if (event.getClazzOrInterface().isInterface()) {
            return true;
        }

        if (!event.getMetaProperty().getAssociation().isBidirectional()) {
            return true;
        }

//		public Collection<Company> getWorksForCompanies() {
//			return new ManyToManyCollectionWrapper<Person, Company>(this, worksForCompanies) {
//				@Override
//				protected Collection<Person> getSourceCollectionInTarget(Company o) {
//					return o.getEmployees();
//				}
//			};
//		}
//		
        Type propertyCollectionType = event.getProperty().getField().getType();
        Type propertyType = propertyCollectionType.getGenerics().get(0).clone();
        String fieldName = event.getProperty().getField().getName();
        Clazz thisClazz = event.getClazz();
        if (thisClazz.isBaseClazz()) {
            thisClazz = getContext().getExtensionClazzForBaseClazz(thisClazz);
        }
        Type thisClazzType = new Type(thisClazz);
        String oppositePropertyCapitalizedName = Utils.capitalize(event.getResolvedOppositeMetaPropertyName());

        CodeSnippet snippet = new CodeSnippet();

        Class<?> wrapperClass = null;
        if (propertyCollectionType.getName().equals(java.util.Collection.class.getName())) {
            wrapperClass = ManyToManyCollectionWrapper.class;
        } else if (propertyCollectionType.getName().equals(Set.class.getName())) {
            wrapperClass = ManyToManySetWrapper.class;
        } else if (propertyCollectionType.getName().equals(SortedSet.class.getName())) {
            wrapperClass = ManyToManySortedSetWrapper.class;
        } else if (propertyCollectionType.getName().equals(List.class.getName())) {
            wrapperClass = ManyToManyListWrapper.class;
        } else {
            throw new RuntimeException("Unsupported Many-to-Many PropertyCollectionType '" + propertyCollectionType.getName() + "' for property '" + event.getProperty().getField().getMetaType() + "'");
        }

        snippet.print("return new ").print(new Type(wrapperClass, false).addGenerics(thisClazzType.clone(), propertyType.clone())).print("((").print(thisClazzType).print(")this, ").print(fieldName).println(") {");
        snippet.println("@Override");
        snippet.print("protected ").print(new Type(propertyCollectionType.getName()).addGenerics(thisClazzType.clone())).print(" getSourceCollectionInTarget(").print(propertyType).println(" o) {");
        snippet.print("return o.get").print(oppositePropertyCapitalizedName).println("();");
        snippet.println("}");
        snippet.println("};");

        event.getProperty().getGetterMethod().getChildren().clear();
        event.getProperty().getGetterMethod().getChildren().add(snippet);
        addSuppressWarningsToJavaMethod(event.getProperty().getGetterMethod());

        event.getProperty().removeSetterMethod();
        return true;
    }

    @Override
    protected boolean handleManyToOneAssociationEvent(
            final ManyToOneAssociationEvent event) {
        if (event.getClazzOrInterface().isInterface()) {
            return true;
        }
        if (!event.getMetaProperty().getAssociation().isBidirectional()) {
            return true;
        }

//		public void setPerson(Person person) {
        //		new ManyToOneWrapper<Person, Address>(this) {
        //			@Override
        //			protected void addManySideObjectToOneSideCollection(Person oneSide,
        //					Address manySide) {
        //				((WrappedCollection<Address>)oneSide.getAddresses()).getWrappedCollection().add(manySide);
        //			}
        //
        //			@Override
        //			protected void removeManySideObjectFromOneSideCollection(
        //					Person oneSide, Address manySide) {
        //				((WrappedCollection<Address>)oneSide.getAddresses()).getWrappedCollection().remove(manySide);
        //			}
        //
        //			@Override
        //			protected Person getOneSideObjectInManySideObject(Address manySide) {
        //				return manySide.getPerson();
        //			}
        //
        //			@Override
        //			protected void setOneSideObjectInManySideObject(Address manySide,
        //					Person oneSide) {
        //				Address.this.person = oneSide;
        //			}
        //		}.updateOneSideObject(person);
        //	}

        Type propertyType = event.getProperty().getField().getType();
        String fieldName = event.getProperty().getField().getName();
        Clazz extensionClazz = event.getClazz();
        if (extensionClazz.isBaseClazz()) {
            extensionClazz = getContext().getExtensionClazzForBaseClazz(extensionClazz);
        }
        Clazz baseClazz = event.getClazz();
        Type thisClazzType = new Type(extensionClazz);

        String oppositePropertyCapitalizedName = Utils.capitalize(event.getResolvedOppositeMetaPropertyName());

        // Find out what the opposite side (the One-to-Many) uses as Collection type
        Property oppositeProperty = getContext().findSingle(new Criteria<Property>(Property.class) {
            @Override
            public boolean isOk(Property element) {
                return element.getMetaType().equals(event.getOppositeMetaProperty());
            }
        });
        if (oppositeProperty == null) {
            throw new RuntimeException("Crap - couldn't find Opposite Property based on Opposite MetaProperty " + event.getOppositeMetaProperty());
        }
        Type oppositePropertyCollectionType = oppositeProperty.getField().getType();
        Class<?> wrapperClass = null;
        if (oppositePropertyCollectionType.getName().equals(java.util.Collection.class.getName())) {
            wrapperClass = WrappedCollection.class;
        } else if (oppositePropertyCollectionType.getName().equals(Set.class.getName())) {
            wrapperClass = WrappedSet.class;
        } else if (oppositePropertyCollectionType.getName().equals(SortedSet.class.getName())) {
            wrapperClass = WrappedSortedSet.class;
        } else if (oppositePropertyCollectionType.getName().equals(List.class.getName())) {
            wrapperClass = WrappedList.class;
        } else {
            throw new RuntimeException("Unsupported One-to-Many PropertyCollectionType '" + oppositePropertyCollectionType.getName() + "' for property '" + event.getOppositeMetaProperty() + "'");
        }

        CodeSnippet snippet = new CodeSnippet();
        snippet.print(
                "new ").print(new Type(ManyToOneWrapper.class, false).addGenerics(propertyType, new Type(extensionClazz))).println("((").print(extensionClazz.asFQCN()).print(")this) {");

        snippet.println("@Override");
        snippet.print("protected void addManySideObjectToOneSideCollection(").print(propertyType).print(" oneSide, ").print(thisClazzType).println(" manySide) {");
        snippet.print("((").print(new Type(wrapperClass, false).addGenerics(thisClazzType.clone())).print(")oneSide.get").print(oppositePropertyCapitalizedName).println("()).getWrappedCollection().add(manySide);");
        snippet.println("}");

        snippet.println("@Override");
        snippet.print("protected void removeManySideObjectFromOneSideCollection(").print(propertyType).print(" oneSide, ").print(thisClazzType).println(" manySide) {");
        snippet.print("((").print(new Type(wrapperClass, false).addGenerics(thisClazzType.clone())).print(")oneSide.get").print(oppositePropertyCapitalizedName).println("()).getWrappedCollection().remove(manySide);");
        snippet.println("}");

        snippet.println("@Override");
        snippet.print("protected ").print(propertyType).print(" getOneSideObjectInManySideObject(").print(thisClazzType).println(" manySide) {");
        snippet.print("return ").print("((").print(baseClazz.asFQCN()).print(")manySide).").print(fieldName).println(";");
        snippet.println("}");

        snippet.println("@Override");
        snippet.print("protected void setOneSideObjectInManySideObject(").print(thisClazzType).print(" manySide, ").print(propertyType).println(" oneSide) {");
        snippet.print("((").print(baseClazz.asFQCN()).print(")manySide).").print(fieldName).println(" = oneSide;");
        snippet.println("}");

        snippet.println("}.updateOneSideObject(parameter);");

        event.getProperty().getSetterMethod().getChildren().clear();
        event.getProperty().getSetterMethod().getChildren().add(snippet);
        addSuppressWarningsToJavaMethod(event.getProperty().getSetterMethod());
        return true;
    }

    @Override
    protected boolean handleOneToManyAssociationEvent(
            OneToManyAssociationEvent event) {
        if (event.getClazzOrInterface().isInterface()) {
            return true;
        }
        if (!event.getMetaProperty().getAssociation().isBidirectional()) {
            return true;
        }

//		public Collection<Address> getAddresses() {
//			return new OneToManyCollectionWrapper<Person, Address>(this, addresses) {
//				@Override
//				protected Person getOneSideObjectInManySideObject(Address manySideObject) {
//					return manySideObject.getPerson();
//				}
//
//				@Override
//				protected void setOneSideObjectInManySideObject(Address manySideObject, Person oneSideObject) {
//					manySideObject.setPerson(oneSideObject);
//				}
//			};
//		}

        Type propertyCollectionType = event.getProperty().getField().getType();
        Type propertyType = propertyCollectionType.getGenerics().get(0).clone();
        String fieldName = event.getProperty().getField().getName();
        Clazz thisClazz = event.getClazz();
        if (thisClazz.isBaseClazz()) {
            thisClazz = getContext().getExtensionClazzForBaseClazz(thisClazz);
        }
        Type thisClazzType = new Type(thisClazz);

        String oppositePropertyCapitalizedName = Utils.capitalize(event.getResolvedOppositeMetaPropertyName());

        Class<?> wrapperClass = null;
        if (propertyCollectionType.getName().equals(java.util.Collection.class.getName())) {
            wrapperClass = OneToManyCollectionWrapper.class;
        } else if (propertyCollectionType.getName().equals(Set.class.getName())) {
            wrapperClass = OneToManySetWrapper.class;
        } else if (propertyCollectionType.getName().equals(SortedSet.class.getName())) {
            wrapperClass = OneToManySortedSetWrapper.class;
        } else if (propertyCollectionType.getName().equals(List.class.getName())) {
            wrapperClass = OneToManyListWrapper.class;
        } else {
            throw new RuntimeException("Unsupported One-to-Many PropertyCollectionType '" + propertyCollectionType.getName() + "' for property '" + event.getProperty().getField().getMetaType() + "'");
        }

        CodeSnippet snippet = new CodeSnippet();
        snippet.print("return new ").print(new Type(wrapperClass, false).addGenerics(thisClazzType, propertyType)).println("((").print(thisClazzType).print(")this, ").print(fieldName).println(") {");

        snippet.println("@Override");
        snippet.print("protected ").print(thisClazzType).print(" getOneSideObjectInManySideObject(").print(propertyType).println(" manySideObject) {");
        snippet.print("return manySideObject.get").print(oppositePropertyCapitalizedName).println("();");
        snippet.print("}");

        snippet.println("@Override");
        snippet.print("protected void setOneSideObjectInManySideObject(").print(propertyType).println(" manySideObject, ").print(thisClazzType).println(" oneSideObject) {");
        snippet.print("manySideObject.set").print(oppositePropertyCapitalizedName).println("(oneSideObject);");
        snippet.println("}");

        snippet.println("};");

        event.getProperty().getGetterMethod().getChildren().clear();
        event.getProperty().getGetterMethod().getChildren().add(snippet);
        addSuppressWarningsToJavaMethod(event.getProperty().getGetterMethod());
        event.getProperty().removeSetterMethod();

        return true;
    }

    @Override
    protected boolean handleOneToOneAssociationEvent(
            OneToOneAssociationEvent event) {
        if (event.getClazzOrInterface().isInterface()) {
            return true;
        }
        if (!event.getMetaProperty().getAssociation().isBidirectional()) {
            return true;
        }


//		public void setPersonName(PersonName personName) {
//			new OneToOneWrapper<PersonName>() {
//				@Override
//				protected PersonName getTargetObjectInSourceObject() {
//					return Person.this.personName;
//				}
//				
//				@Override
//				protected void setTargetObjectInSourceObject(PersonName newTarget) {
//					Person.this.personName = newTarget;
//				}
//
//				@Override
//				protected void createRelationToTargetObject(PersonName newTarget) {
//					newTarget.setPerson(Person.this);
//				}
//
//				@Override
//				protected void removeRelationToTargetObject(PersonName currentTarget) {
//					currentTarget.setPerson(null);
//				}
//			}.updateTargetProperty(personName);
//		}

        Type propertyType = event.getProperty().getField().getType();
        String fieldName = event.getProperty().getField().getName();
        Clazz extensionClazz = event.getClazz();
        if (extensionClazz.isBaseClazz()) {
            extensionClazz = getContext().getExtensionClazzForBaseClazz(extensionClazz);
        }
        Clazz baseClazz = event.getClazz();

        CodeSnippet snippet = new CodeSnippet();
        snippet.print(
                "new ").print(new Type(OneToOneWrapper.class, false).addGenerics(propertyType)).println("() {");
        snippet.println("@Override");
        snippet.print("protected ").print(propertyType).println(" getTargetObjectInSourceObject() {");
        snippet.print("return ").print(baseClazz.asFQCN()).print(".this.").print(fieldName).println(";");
        snippet.println("}");

        snippet.println("@Override");
        snippet.print("protected void setTargetObjectInSourceObject(").print(propertyType).println(" newTarget) {");
        snippet.print(baseClazz.asFQCN()).print(".this.").print(fieldName).println(" = newTarget;");
        snippet.println("}");

        snippet.println("@Override");
        snippet.print("protected void createRelationToTargetObject(").print(propertyType).println(" newTarget) {");
        snippet.print("newTarget.set").print(Utils.capitalize(event.getResolvedOppositeMetaPropertyName())).print("((").print(extensionClazz.asFQCN()).print(")").print(baseClazz.asFQCN()).println(".this);");
        snippet.println("}");

        snippet.println("@Override");
        snippet.print("protected void removeRelationToTargetObject(").print(propertyType).println(" currentTarget) {");
        snippet.print("currentTarget.set").print(Utils.capitalize(event.getResolvedOppositeMetaPropertyName())).print("(null);");
        snippet.println("}");
        snippet.print("}.updateTargetProperty(parameter);");

        event.getProperty().getSetterMethod().getChildren().clear();
        event.getProperty().getSetterMethod().getChildren().add(snippet);
        addSuppressWarningsToJavaMethod(event.getProperty().getSetterMethod());
        return true;
    }

    /**
     * Add @SuppressWarnings({ "serial", "unchecked", "rawtypes" })
     *
     * @param method the method that will have the SuppressWarning annotation applied
     */
    private void addSuppressWarningsToJavaMethod(Method method) {
        Annotation suppressWarnings = method.findOrAddAnnotation(SuppressWarnings.class);

        List<Object> values = null;
        if (suppressWarnings.hasAnnotationAttribute(AnnotationAttribute.DEFAULT_VALUE_ATTRIBUTE_NAME)) {
            values = suppressWarnings.getAnnotationAttributeValues(AnnotationAttribute.DEFAULT_VALUE_ATTRIBUTE_NAME);
        } else {
            values = new LinkedList<Object>();
        }

        if (!values.contains("serial")) {
            values.add("serial");
        }
        if (!values.contains("unchecked")) {
            values.add("unchecked");
        }
        if (!values.contains("rawtypes")) {
            values.add("rawtypes");
        }
        suppressWarnings.setAnnotationAttributeValues(AnnotationAttribute.DEFAULT_VALUE_ATTRIBUTE_NAME, values);
    }

}
