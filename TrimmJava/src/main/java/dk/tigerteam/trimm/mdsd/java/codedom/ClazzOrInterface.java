/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.JavaStereotypes;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Common base class for {@link Interface} and {@link Clazz}
 *
 * @author Jeppe Cramon
 */
public abstract class ClazzOrInterface extends CodeComposite implements Comparable<ClazzOrInterface> {

    private List<GenericType> generics = new LinkedList<GenericType>();
    private List<Import> imports = new LinkedList<Import>();
    private boolean isTestClazz = false;

    public abstract boolean isInterface();

    public ClazzOrInterface(AccessModifier... accessModifiers) {
        super(accessModifiers);
    }

    public ClazzOrInterface(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
    }

    public ClazzOrInterface(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

    public boolean isTestClazz() {
        return isTestClazz;
    }

    public ClazzOrInterface setTestClazz(boolean isTestClazz) {
        this.isTestClazz = isTestClazz;
        return this;
    }

    @Override
    public MetaClazz getMetaType() {
        return (MetaClazz) super.getMetaType();
    }

    public CodePackage getPackage() {
        return findOwnerOfType(CodePackage.class);
    }

    public ClazzOrInterface setPackage(CodePackage package_) {
        this.setOwner(package_);
        return this;
    }

    /**
     * Sugar method which lookup the package by its full name in the CodeModel (effective fetches the {@link CodeModel} from the
     * {@link JavaGeneratorContext} and looks up the package using {@link CodeModel#findChildWithName(Class, String)} and sets the
     * returned {@link CodePackage} as <code>owner</code> of this instance
     *
     * @param packageName The full package name
     * @return this object instance
     */
    public ClazzOrInterface setPackage(String packageName) {
        this.setOwner(JavaGeneratorContext.getContext().getCodeModel().findChildWithName(CodePackage.class, packageName));
        return this;
    }

    public List<GenericType> getGenerics() {
        return generics;
    }

    public ClazzOrInterface addGenerics(GenericType... genericTypes) {
        generics.addAll(Arrays.asList(genericTypes));
        return this;
    }

    public List<Import> getImports() {
        return imports;
    }

    public ClazzOrInterface addImport(Import... imports) {
        this.imports.addAll(Arrays.asList(imports));
        return this;
    }

    public ClazzOrInterface addImport(Import import_) {
        if (!imports.contains(import_)) {
            imports.add(import_);
        }
        return this;
    }

    public ClazzOrInterface addImport(Class<?> importReference) {
        Import import_ = new Import(importReference.getName());
        if (!imports.contains(import_)) {
            imports.add(import_);
        }
        return this;
    }

    /**
     * Get the Fully Qualified Class Name as a <b>String</b>.
     * <p/>
     * A FQCN allows you to export a {@link ClazzOrInterface}'s Fully Qualified Class Name (FQCN) in (for instance) a {@link CodeSnippet}.<br/>
     * The FQCN can be returned by calling {@link #asFQCN()} or {@link #getFQCN()}. The difference between these two calls is that
     * {@link #getFQCN()} is resolved at calltime, whereas {@link #asFQCN()}, codegeneration wise, is resolved <b>after</b> package
     * rewriting has occurred.
     * <p/>
     * Example: <code>new CodeSnippet(clazz.asFQCN(), " ", instanceVarName, " = new ", clazz.asFQCN(),  "();");</code>
     *
     * @return The class' Fully Qualified Class Name as a String (resolved at calltime <b>without</b> packagerewriting)
     */
    public String getFQCN() {
        // TODO: Handle inner classes (use dollar as separator instead)
        CodePackage codePackage = getPackage();
        if (codePackage != null) {
            return codePackage.getName() + "." + getName();
        } else {
            return getName();
        }
    }

    /**
     * Get the Clazz' Fully Qualified Class Name as an {@link CodeElement} object.
     * <p/>
     * A FQCN allows you to export a {@link ClazzOrInterface}'s Fully Qualified Class Name (FQCN) in (for instance) a {@link CodeSnippet}.<br/>
     * The FQCN can be returned by calling {@link #asFQCN()} or {@link #getFQCN()}. The difference between these two calls is that
     * {@link #getFQCN()} is resolved at calltime, whereas {@link #asFQCN()}, codegeneration wise, is resolved <b>after</b> package
     * rewriting has occurred.
     * <p/>
     * Example: <code>new CodeSnippet(clazz.asFQCN(), " ", instanceVarName, " = new ", clazz.asFQCN(),  "();");</code>
     */
    public FQCN asFQCN() {
        return new FQCN(this);
    }

    /**
     * Get the Clazz' Fully Qualified Class Name as an {@link CodeElement} object.
     * <p/>
     * A FQCN allows you to export a {@link ClazzOrInterface}'s Fully Qualified Class Name (FQCN) in (for instance) a {@link CodeSnippet}.<br/>
     * The FQCN can be returned by calling {@link #asFQCN()} or {@link #getFQCN()}. The difference between these two calls is that
     * {@link #getFQCN()} is resolved at calltime, whereas {@link #asFQCN()}, codegeneration wise, is resolved <b>after</b> package
     * rewriting has occurred.
     * <p/>
     * Example: <code>new CodeSnippet(clazz.asFQCN(), " ", instanceVarName, " = new ", clazz.asFQCN(),  "();");</code>
     */
    public FQCN asFQCN(boolean quoted) {
        return new FQCN(this, quoted);
    }

    /**
     * Does this Clazz represent an Enumeration (sugar for testing against the sub class being used)
     */
    public boolean isEnumeration() {
        return false;
    }

    /**
     * Does this Clazz represent a Clazz (sugar for testing against the sub class being used)
     */
    public boolean isClazz() {
        return false;
    }

    public boolean hasExtensionClassStereoType() {
        if (this.getMetaType() != null && this.getMetaType().hasStereotypeUpwardsRecursively(JavaStereotypes.EXTENSION_CLASS_STEREOTYPE_NAME)) {
            return true;
        }
        return false;
    }

    public boolean hasNoExtensionClassStereoType() {
        if (this.getMetaType() != null && this.getMetaType().hasStereotypeUpwardsRecursively(JavaStereotypes.NO_EXTENSION_CLASS_STEREOTYPE_NAME)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getFQCN() == null) ? 0 : getFQCN().hashCode());
        result = prime * result + ((getMetaType() == null) ? 0 : getMetaType().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final ClazzOrInterface other = (ClazzOrInterface) obj;
        if (getFQCN() == null) {
            if (other.getFQCN() != null)
                return false;
        } else if (!getFQCN().equals(other.getFQCN())) {
            return false;
        }

        if (getMetaType() == null) {
            if (other.getMetaType() != null)
                return false;
        } else if (!getMetaType().equals(other.getMetaType())) {
            return false;
        }
        return true;
    }

}
