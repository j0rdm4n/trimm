/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.InterfaceEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Creates a java {@link Import} statement for every TRIMM Code-Element {@link Type} in a {@link Clazz} or {@link Interface}
 * If you use any of the {@link dk.tigerteam.trimm.mdsd.java.configuration.AbstractConfigurationBasedCodeGenerator}
 * subclasses (e.g. if you use TRIMM from Ant or Maven) then the import listener is automatically configured as the last listener.
 *
 * @author Lasse Cramon
 */
public class ImportListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

    private static final Log logger = LogFactory.getLog(ImportListener.class);
    private static String basePackageName;

    public ImportListener(String basePackageName) {
        ImportListener.basePackageName = basePackageName;
    }

    @Override
    protected boolean handleClazzEvent(ClazzEvent event) {
        return true;
    }

    private void handleClazzImports(Clazz clazz) {
        logger.debug("Adding imports for clazz '" + clazz.getFQCN() + "'");
        for (GenericType genericType : clazz.getGenerics()) {
            addImport(clazz, genericType);
        }
        addImport(clazz, clazz.getExtendsType());
        for (Type implement : clazz.getImplementsTypes()) {
            addImport(clazz, implement);
        }
        for (Annotation annotation : clazz.getAnnotations()) {
            addAnnotationImports(clazz, annotation);
        }
        List<Property> properties = clazz.getProperties();
        for (Property property : properties) {
            handlePropertyImports(clazz, property);
        }
        List<Method> methods = clazz.getMethods();
        for (Method method : methods) {
            handleMethodImports(clazz, method);
        }
        for (CodeElement codeElement : clazz.getChildren()) {
            if (codeElement instanceof Clazz) {
                handleClazzImports((Clazz) codeElement);
            } else if (codeElement instanceof Field) {
                handleFieldImports(clazz, (Field) codeElement);
            }
        }
    }

    private void handleMethodImports(Clazz clazz, Method method) {
        addImport(clazz, method.getReturnType());
        for (Parameter parameter : method.getParameters()) {
            addImport(clazz, parameter.getType());
        }
        for (Annotation annotation : method.getAnnotations()) {
            addAnnotationImports(clazz, annotation);
        }
        for (Type throws_ : method.getThrowsExceptions()) {
            addImport(clazz, throws_);
        }
        for (CodeElement codeElement : method.getChildren()) {
            if (codeElement instanceof Property) {
                handlePropertyImports(clazz, (Property) codeElement);
            } else if (codeElement instanceof CodeSnippet) {
                for (Object snippet : ((CodeSnippet) codeElement).getSnippets()) {
                    if (snippet instanceof Type) {
                        addImport(clazz, (Type) snippet);
                    }
                }
            }
        }
    }

    private void handlePropertyImports(Clazz clazz, Property property) {
        addImport(clazz, property.getType());
        for (Annotation annotation : property.getAnnotations()) {
            addAnnotationImports(clazz, annotation);
        }
        if (property.getInitializerStatement() != null) {
            CodeSnippet initializerStatement = property.getInitializerStatement();
            for (Object snippet : initializerStatement.getSnippets()) {
                if (snippet instanceof Type) {
                    addImport(clazz, (Type) snippet);
                }
            }
        }
        if (property.getField() != null) {
            handleFieldImports(clazz, property.getField());
        }
        for (CodeElement codeElement : property.getChildren()) {
            if (codeElement instanceof Method) {
                handleMethodImports(clazz, (Method) codeElement);
            }
        }
    }

    private void handleFieldImports(Clazz clazz, Field field) {
        addImport(clazz, field.getType());
        for (Annotation annotation : field.getAnnotations()) {
            addAnnotationImports(clazz, annotation);
        }
        if (field.getInitializerStatement() != null) {
            CodeSnippet initializerStatement = field.getInitializerStatement();
            for (Object snippet : initializerStatement.getSnippets()) {
                if (snippet instanceof Type) {
                    addImport(clazz, (Type) snippet);
                }
            }
        }
    }

    public static void addImport(Clazz clazz, Type type) {
        if (type != null && basePackageName != null) {
            if (type.getClazz() != null) {
                if (containsDot(type.getClazz().getFQCN()) /*&& !type.getClazz().equals(clazz)*/) {
                    logger.trace("Adding '" + type.getClazz().getFQCN() + "' import");
                    clazz.addImport(new Import(internalRewrite(type.getClazz().getFQCN())));
                }
            } else if (type.getWrappedJavaClass() != null && !type.isBuiltInType() && !type.getWrappedJavaClass().getName().startsWith("java.lang.")) {
                clazz.addImport(new Import(type.getWrappedJavaClass().getName()));
            } else {
                if (containsDot(type.getName()) && !type.isBuiltInType() && type.getWrappedJavaClass() != null && !type.getWrappedJavaClass().getName().startsWith("java.lang.")) {
                    logger.trace("Adding '" + type.getName() + "' import");
                    clazz.addImport(new Import(type.getName()));

                }
            }
            for (Type genericType : type.getGenerics()) {
                addImport(clazz, genericType);
            }
        }
    }

    private void addAnnotationImports(Clazz clazz, Annotation annotation) {
        if (containsDot(annotation.getName()) && !annotation.getName().startsWith("java.lang.")) {
            logger.trace("Adding '" + annotation.getName() + "' import");
            clazz.addImport(new Import(annotation.getName()));
        }
        for (AnnotationAttribute annotationAttribute : annotation.getAnnotationAttributes()) {
            for (Object value : annotationAttribute.getValues()) {
                if (value instanceof Annotation) {
                    addAnnotationImports(clazz, (Annotation) value);
                } else if (value instanceof FQCN) {
                    logger.trace("Adding '" + ((FQCN) value).getClazzOrInterface().getFQCN() + "' import");
                    String fqcn = ((FQCN) value).getClazzOrInterface().getFQCN();
                    clazz.addImport(new Import(internalRewrite(fqcn)));
                } else if (value instanceof Enum) {
                    Enum<?> enumValue = (Enum<?>) value;
                    logger.trace("Adding '" + enumValue.getDeclaringClass().getName() + "' import");
                    clazz.addImport(new Import(enumValue.getDeclaringClass().getName()));
                }
            }
        }
    }

    // TODO: make with ClazzOrInterface
    public static void addImport(Interface interface_, Type type) {
        if (type != null) {
            if (type.getClazz() != null) {
                if (containsDot(type.getClazz().getFQCN())) {
                    logger.trace("Adding '" + type.getClazz().getFQCN() + "' import");
                    interface_.addImport(new Import(internalRewrite(type.getClazz().getFQCN())));
                }
            } else {
                if (containsDot(type.getName())) {
                    logger.trace("Adding '" + type.getName() + "' import");
                    interface_.addImport(new Import(type.getName()));
                }
            }
        }
    }

    private void addAnnotationImports(Interface interface_, Annotation annotation) {
        if (containsDot(annotation.getName())) {
            logger.trace("Adding '" + annotation.getName() + "' import");
            interface_.addImport(new Import(annotation.getName()));
        }
        for (AnnotationAttribute annotationAttribute : annotation.getAnnotationAttributes()) {
            for (Object value : annotationAttribute.getValues()) {
                if (value instanceof Annotation) {
                    logger.trace("Adding '" + ((Annotation) value).getName() + "' import");
                    interface_.addImport(new Import(((Annotation) value).getName()));
                } else if (value instanceof FQCN) {
                    logger.trace("Adding '" + ((FQCN) value).getClazzOrInterface().getFQCN() + "' import");
                    String fqcn = ((FQCN) value).getClazzOrInterface().getFQCN();
                    interface_.addImport(new Import(internalRewrite(fqcn)));
                } else if (value instanceof Enum) {
                    Enum<?> enumValue = (Enum<?>) value;
                    logger.trace("Adding '" + enumValue.getDeclaringClass().getName() + "' import");
                    interface_.addImport(new Import(enumValue.getDeclaringClass().getName()));
                }
            }
        }
    }

    /**
     * To filter out possible language included types like String
     *
     * @param fqcn The fully qualified class name
     * @return true if the FQCN contains a dot . otherwise false
     */
    private static boolean containsDot(String fqcn) {
        if (fqcn != null) {
            return fqcn.contains(".");
        }
        return false;
    }

    private static String internalRewrite(String fqcn) {
        if (basePackageName != null && !basePackageName.isEmpty() && !fqcn.startsWith(basePackageName)) {
            fqcn = basePackageName + "." + fqcn;
        }
        return fqcn;
    }

    @Override
    protected boolean handleInterfaceEvent(InterfaceEvent event) {
        return true;
    }

    private void handleInterfaceImports(Interface interface_) {
        logger.debug("Adding imports for interface '" + interface_.getFQCN() + "'");
        for (GenericType genericType : interface_.getGenerics()) {
            addImport(interface_, genericType);
        }
        for (Type extends_ : interface_.getExtendsTypes()) {
            addImport(interface_, extends_);
        }
        for (Annotation annotation : interface_.getAnnotations()) {
            addAnnotationImports(interface_, annotation);
        }

        for (CodeElement codeElement : interface_.getChildren()) {
            if (codeElement instanceof Property) {
                Property property = (Property) codeElement;
                handlePropertyImports(interface_, property);
            } else if (codeElement instanceof Method) {
                Method method = (Method) codeElement;
                addImport(interface_, method.getReturnType());
                for (Parameter parameter : method.getParameters()) {
                    addImport(interface_, parameter.getType());
                }
                for (Type throws_ : method.getThrowsExceptions()) {
                    addImport(interface_, throws_);
                }
                for (Annotation annotation : method.getAnnotations()) {
                    addAnnotationImports(interface_, annotation);
                }
                // TODO: field
            } else {
                logger.warn("CodeElement '" + codeElement + "' imports not handled.....");
            }
        }
    }

    private void handlePropertyImports(Interface interface_, Property property) {
        addImport(interface_, property.getType());
        for (Annotation annotation : property.getAnnotations()) {
            addAnnotationImports(interface_, annotation);
        }
        if (property.getField() != null) {
            addImport(interface_, property.getField().getType());
            for (Annotation annotation : property.getField().getAnnotations()) {
                addAnnotationImports(interface_, annotation);
            }
        }
        // TODO: handle snippets
    }

    @Override
    public void generationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
        for (ClazzOrInterface clazzOrInterface : allGeneratedClazzes) {
            if (clazzOrInterface.isClazz()) {
                Clazz clazz = (Clazz) clazzOrInterface;
                handleClazzImports(clazz);
                for (CodeElement codeElement : clazz.getChildren()) {
                    if (codeElement instanceof Clazz) {
                        handleClazzImports((Clazz) codeElement);
                    }
                }
            } else if (clazzOrInterface.isInterface()) {
                Interface interface_ = (Interface) clazzOrInterface;
                handleInterfaceImports(interface_);
                for (CodeElement codeElement : interface_.getChildren()) {
                    if (codeElement instanceof Clazz) {
                        handleClazzImports((Clazz) codeElement);
                    } else if (codeElement instanceof Interface) {
                        handleInterfaceImports((Interface) codeElement);
                    }
                }
            }
        }
    }

}
