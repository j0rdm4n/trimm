/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.configuration;

import dk.tigerteam.trimm.mdsd.java.generator.event.GeneratorEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Detailed EventListener configuration.
 * When used from e.g. YAML this will look like this:
 * <pre>
 * eventListeners:
 *  - listener: dk.tigerteam.mdsd.java.generator.extension.SerialVersionUIDGeneratorListener
 *    configuration:
 *      property1: value1
 *      property2: value2
 *  - listener: dk.tigerteam.mdsd.java.generator.extension.PropertySugarMethodsEventListener
 *    configuration:
 *      property1: value1
 *      property2: value2
 *  - listener: dk.tigerteam.mdsd.java.generator.extension.SerializablePojoListener
 *  - listener: dk.tigerteam.mdsd.java.generator.extension.ImportListener
 * </pre>
 */
public class EventListenerConfiguration {
    private String listener;
    private Map<String, Object> configuration = new HashMap<String, Object>();

    public EventListenerConfiguration() {
    }

    /**
     * Shorthand (if no configuration is needed) + Backwards compatible version (from when "- listener: FQCN" wasn't required and you could
     * just write "- FQCN")
     */
    public EventListenerConfiguration(String listener) {
        this.listener = listener;
    }

    public EventListenerConfiguration(Class<? extends GeneratorEventListener> eventListenerClass) {
        this.listener = eventListenerClass.getName();
    }

    public String getListener() {
        return listener;
    }

    public void setListener(String listener) {
        this.listener = listener;
    }

    public Map<String, Object> getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Map<String, Object> configuration) {
        this.configuration = configuration;
    }
}
