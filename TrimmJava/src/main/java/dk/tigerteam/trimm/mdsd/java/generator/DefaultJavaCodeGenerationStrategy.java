/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.BaseClazz;
import dk.tigerteam.trimm.mdsd.ExtensionClazz;
import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaOperation;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;

import java.util.List;

import static dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext.getContext;
import static dk.tigerteam.trimm.mdsd.java.generator.JavaStereotypes.DONT_GENERATE_STEREOTYPE_NAME;

/**
 * Default strategy for dealing with Java code generation
 *
 * @author Jeppe Cramon
 */
public class DefaultJavaCodeGenerationStrategy implements JavaCodeGenerationStrategy {
    /**
     * Should the {@link JavaGenerator} generate an Extension Clazz for the ClazzOrInterface (following the Base/Extension class pattern) -
     * Default false for interfaces and enumerations and true for everything else
     *
     * @param clazz The clazz or interface in question
     */
    @Override
    public boolean shouldGenerateExtensionClazzFor(ClazzOrInterface clazz) {
        if (clazz.isInterface() || clazz.isEnumeration()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Post process Clazz' after that have been fully resolved and any listeners are done doing their job (this is after secondPassGeneration
     * is done but before code is written to the file system)
     *
     * @param clazzes All clazzes that have been generated
     * @return The clazzes to be written to the file system
     */
    @Override
    public List<ClazzOrInterface> postProcessClazzes(List<ClazzOrInterface> clazzes) {
        return clazzes;
    }

    /**
     * Callback method that allows for specialization of the base Clazz name (default is to preappend it with "Abstract")
     *
     * @param baseClazz      The base Clazz
     * @param extensionClazz The extension Clazz
     * @return The name to use for the base Clazz
     */
    @Override
    public String getBaseClazzName(Clazz baseClazz, Clazz extensionClazz) {
        return "Abstract" + baseClazz.getName();
    }

    /**
     * Preprocess a Clazz before any properties and associations are resolved and processed.<br/>
     * Can be useful for processing Stereotypes, like JPA Embeddable cases, which changes a lot about how code later is generated.
     *
     * @param clazz     The clazz just created during firstPassGeneration
     * @param metaClazz The clazz' corresponding MetaClazz
     */
    @Override
    public void preProcessClazz(ClazzOrInterface clazz, MetaClazz metaClazz) {
    }

    /**
     * Change to fix and validate any MetaClazz's before generation starts. This is typically used to fix built in types, like the user writes
     * "Date", but means "java.util.Date". (however we recommend using {@link dk.tigerteam.trimm.mdsd.java.generator.extension.BuiltInTypesListener} for this purpose specifically)
     *
     * @param metaClazz The metaClazz to fix (if at all)
     */
    @Override
    public void preProcessMetaClazz(MetaClazz metaClazz) {
    }

    /**
     * Override this method to control which {@link MetaClazz}' we generate {@link Clazz}'<br/>
     * Standard rules for not allowing code generation:
     * <ul>
     * <li>If {@link dk.tigerteam.trimm.mdsd.meta.MetaClazz#isBuiltInType()} is <code>true</code></li>
     * <li>If the {@link MetaClazz} or any owner (upwards recursively) has stereotype {@link JavaStereotypes#DONT_GENERATE_STEREOTYPE_NAME} applied</li>
     * </ul>
     *
     * @param metaClazz The MetaClazz to test against
     * @return true if we should generate for the {@link MetaClazz} otherwise false
     */
    @Override
    public boolean shouldGenerateForMetaClazz(MetaClazz metaClazz) {
        if (metaClazz.isBuiltInType()) {
            return false;
        } else if (metaClazz.getName() == null) {
            throw new IllegalArgumentException("MetaClazz doesn't have a name. Id: " + metaClazz.getId());
        } else if (metaClazz.hasStereotypeUpwardsRecursively(DONT_GENERATE_STEREOTYPE_NAME)) {
            return false;
        }
        return true;
    }

    /**
     * Guard method to turn of all method generation or just individual methods<br/>
     * Default rule for disallowing code generation:
     * <ul>
     * <li>The meta operation has the stereotype {@link JavaStereotypes#DONT_GENERATE_STEREOTYPE_NAME} applied</li>
     * </ul>
     *
     * @param metaOperation The meta operation that we're processing
     * @return true if we should generate a {@link dk.tigerteam.trimm.mdsd.java.codedom.Method} for the {@link dk.tigerteam.trimm.mdsd.meta.MetaOperation}, otherwise false
     */
    @Override
    public boolean shouldGenerateForMetaOperation(MetaOperation metaOperation) {
        return !metaOperation.hasStereoType(DONT_GENERATE_STEREOTYPE_NAME);
    }

    /**
     * Guard method used by {@link JavaGenerator#generateEnumerators(dk.tigerteam.trimm.mdsd.java.codedom.Enumeration)}.<br/>
     * Determines if we should generate an {@link dk.tigerteam.trimm.mdsd.java.generator.event.EnumeratorLiteralEvent} for the given
     * {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty}.<br/>
     * Default rules for disallowing code generation:
     * <ul>
     * <li>The meta property has the stereotype {@link JavaStereotypes#DONT_GENERATE_STEREOTYPE_NAME} applied</li>
     * <li>We don't generate {@link dk.tigerteam.trimm.mdsd.java.generator.event.EnumeratorLiteralEvent} for {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty}' that are
     * part in an Association (we don't want to generate properties in the enumeration class for the opposite side of an association to our
     * enumeration)
     * </li>
     * </ul>
     *
     * @param metaProperty The {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty}
     * @return true if we should generate an {@link dk.tigerteam.trimm.mdsd.java.generator.event.EnumeratorLiteralEvent} for the given
     *         {@link dk.tigerteam.trimm.mdsd.meta.MetaProperty}, otherwise false.
     */
    @Override
    public boolean shouldGenerateForEnumeratorProperty(MetaProperty metaProperty) {
        // We don't want to generate properties in the enumeration class for the opposite side of an association to our enumeration
        if (metaProperty.isPartInAnAssociation()) {
            return false;
        }

        return !metaProperty.hasStereoType(DONT_GENERATE_STEREOTYPE_NAME);
    }

    /**
     * Guard method that determines if we should generate code for the given {@link MetaProperty}.<br/>
     * Defaults for disallowing generation are:
     * <ul>
     * <li>For unidirectional associations, we only generate for the {@link MetaProperty} that is the owner of the Association (
     * {@link MetaProperty#isOwnerOfAssociation()})</li>
     * <li>If the meta property has the stereotype {@link JavaStereotypes#DONT_GENERATE_STEREOTYPE_NAME}</li>
     * </ul>
     *
     * @param metaProperty The meta property that we are testing
     * @return true if we should generate for the given {@link MetaProperty}, otherwise false
     */
    @Override
    public boolean shouldGenerateForMetaProperty(MetaProperty metaProperty) {
        if (metaProperty.hasStereoType(DONT_GENERATE_STEREOTYPE_NAME)) return false;

        boolean isUnidirectionalAssociation = metaProperty.isPartInAnAssociation() && !metaProperty.getAssociation().isBidirectional();
        if (isUnidirectionalAssociation) {
            return metaProperty.isOwnerOfAssociation();
        }
        return true;
    }

    /**
     * This method is used to determined how Many-to-Many bidirectionality is handled. In the {@link dk.tigerteam.trimm.mdsd.meta.MetaModel} a Many-to-Many
     * {@link dk.tigerteam.trimm.mdsd.meta.MetaAssociation} might not be modeled/marked as bidirectional, but logically it does make sense to code wise handle them as
     * bidirectional, which is what this method does by default.
     *
     * @param metaProperty the meta property
     */
    @Override
    public void resolveManyToManyBidirectionality(MetaProperty metaProperty) {
    }

    /**
     * Create an extension class for the metaClazz and it's matches clazz instance - the workings in here involves shifting the
     * clazz hierarchy around making the original clazz superclass of the generated extension class, where by the original clazz by our
     * definition becomes the base class. Base-class name is resolved using
     * {@link #getBaseClazzName(dk.tigerteam.trimm.mdsd.java.codedom.Clazz, dk.tigerteam.trimm.mdsd.java.codedom.Clazz)}.
     * Finally the baseclass is made <code>abstract</code><p/>
     * After this {@link dk.tigerteam.trimm.mdsd.ExtensionClazz} and {@link dk.tigerteam.trimm.mdsd.BaseClazz}
     * annotations are placed if {@link dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext#isAddExtensionAndBaseClazzAnnotations()}
     * is set to <code>true</code>.<p/>
     * Finally the Base class and Extension class pair is registered in the {@link JavaGeneratorContext} using
     * {@link JavaGeneratorContext#registerBaseClazzAndExtensionClazzPair(dk.tigerteam.trimm.mdsd.java.codedom.Clazz, dk.tigerteam.trimm.mdsd.java.codedom.Clazz)}
     *
     * @param codeModel The code model
     * @param metaClazz The metaClazz
     * @param clazz     The clazz instance created based on the metaClazz
     */
    public void createExtensionClazz(CodeModel codeModel, MetaClazz metaClazz, ClazzOrInterface clazz) {
        Clazz baseClazz = (Clazz) clazz;
        Clazz extensionClazz = new Clazz(metaClazz.getName(), true);
        extensionClazz.setName(baseClazz.getName());
        extensionClazz.setCodeModel(codeModel);
        extensionClazz.setPackage(baseClazz.getPackage());
        extensionClazz.addAttribute(Clazz.METACLAZZ_ID, metaClazz.getId());
        extensionClazz.setExtensionClazz(true);
        extensionClazz.setMetaType(baseClazz.getMetaType());
        if (getContext().isAddExtensionAndBaseClazzAnnotations()) {
            extensionClazz.addAnnotations(new Annotation(ExtensionClazz.class).addAnnotationAttribute("baseClazz",
                    new CodeSnippet(baseClazz.asFQCN(), ".class")));
        }
        baseClazz.setName(getBaseClazzName(baseClazz, extensionClazz));
        baseClazz.addAccessModifiers(CodeElement.AccessModifier.Abstract);
        baseClazz.setBaseClazz(true);
        if (getContext().isAddExtensionAndBaseClazzAnnotations()) {
            baseClazz.addAnnotations(new Annotation(BaseClazz.class).addAnnotationAttribute("extensionClazz",
                    new CodeSnippet(extensionClazz.asFQCN(), ".class")));
        }
        getContext().registerBaseClazzAndExtensionClazzPair(baseClazz, extensionClazz);
    }
}
