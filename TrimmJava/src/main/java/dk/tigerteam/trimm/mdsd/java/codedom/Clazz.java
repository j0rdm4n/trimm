/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Models a Java {@link Class}.
 *
 * @author Jeppe Cramon
 */
public class Clazz extends ClazzOrInterface {

    public static final String METACLAZZ_ID = "metaClassId";

    public enum ClazzStyle {
        Normal, InnerClazz, AnonymousInnerClazz, SameFileClazz
    }

    private ClazzStyle clazzStyle = ClazzStyle.Normal;
    private Type extendsType;
    private List<Type> implementsTypes = new LinkedList<Type>();
    private boolean isExtensionClazz = false;
    private boolean isBaseClazz = false;

    public Clazz(AccessModifier... accessModifiers) {
        super(accessModifiers);
    }

    public Clazz(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
    }

    public Clazz(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

    @Override
    public boolean isInterface() {
        return false;
    }

    @Override
    public boolean isClazz() {
        return true;
    }

    @Override
    public MetaClazz getMetaType() {
        return (MetaClazz) super.getMetaType();
    }

    public ClazzStyle getClazzStyle() {
        return clazzStyle;
    }

    public Clazz setClazzStyle(ClazzStyle clazzStyle) {
        this.clazzStyle = clazzStyle;
        return this;
    }

    public Type getExtendsType() {
        return extendsType;
    }

    public boolean isSubClassOfClazz(Clazz clazz) {
        Clazz currentClazz = this;
        while (currentClazz != null) {
            Clazz superClazz = currentClazz.getExtendsType() != null ? currentClazz.getExtendsType().getClazz() : null;
            if (superClazz != null) {
                if (superClazz.equals(clazz)) {
                    return true;
                } else {
                    currentClazz = superClazz;
                }
            } else {
                currentClazz = null;
            }
        }
        return false;
    }

    public boolean isSubClassOfClazz(String javaClazzName) {
        Clazz currentClazz = this;
        while (currentClazz != null) {
            Type superClazz = currentClazz.getExtendsType() != null ? currentClazz.getExtendsType() : null;
            if (superClazz != null) {
                if (javaClazzName.equals(superClazz.getName())) {
                    return true;
                } else if (superClazz.getClazz() != null) {
                    currentClazz = superClazz.getClazz();
                } else {
                    currentClazz = null;
                }
            } else {
                currentClazz = null;
            }
        }
        return false;
    }

    public boolean isSubClassOfClazz(Class<?> clazz) {
        return isSubClassOfClazz(clazz.getName());
    }

    public boolean extendsAGeneratedClazz() {
        if (this.getExtendsType() == null) {
            return false;
        }
        return this.getExtendsType().getClazz() != null;
    }

    public boolean implementsAGeneratedInterface() {
        for (Type type : this.getImplementsTypes()) {
            if (type.getInterface() != null) {
                return true;
            }
        }
        return false;
    }

    public boolean implementsJavaInterface(Class<?> javaInterface) {
        for (Type type : this.getImplementsTypes()) {
            if (type.getWrappedJavaClass() != null
                    && (javaInterface.equals(type.getWrappedJavaClass()) || javaInterface.getName().equals(type.getName()))) {
                return true;
            } else if (type.getInterface() != null && type.getInterface().extendsJavaInterface(javaInterface)) {
                return true;
            }
        }
        if (this.getExtendsType() != null && this.getExtendsType().getClazz() != null) {
            return this.getExtendsType().getClazz().implementsJavaInterface(javaInterface);
        }
        return false;
    }

    public boolean isAbstract() {
        return hasAccessModifier(AccessModifier.Abstract);
    }

    public Clazz setExtendsType(Class<?> extendsType) {
        this.extendsType = new Type(extendsType);
        return this;
    }

    public Clazz setExtendsType(Type extendsType) {
        this.extendsType = extendsType;
        return this;
    }

    public Clazz setExtendsType(MetaClazz extendsType) {
        Clazz clazz = (Clazz) JavaGeneratorContext.getContext().getClazzFromMetaClazz(extendsType);
        this.extendsType = new Type(clazz);
        return this;
    }

    public List<Type> getImplementsTypes() {
        return implementsTypes;
    }

    public Clazz setImplementsTypes(List<Type> implementsTypes) {
        this.implementsTypes = implementsTypes;
        return this;
    }

    public Clazz addImplementsTypes(Type... types) {
        implementsTypes.addAll(Arrays.asList(types));
        return this;
    }

    public Clazz addImplementsTypes(Interface... types) {
        for (Interface _interface : types) {
            implementsTypes.add(new Type(_interface));
        }
        return this;
    }

    public Clazz addImplementsTypes(Class<?>... types) {
        for (Class<?> type : types) {
            implementsTypes.add(new Type(type));
        }
        return this;
    }

    /**
     * Get all immuteable properties (i.e. properties with {@link MetaProperty#IMMUTABLE_STEREOTYPE_NAME}) (looks in
     * this clazz and all super clazzes).<br/>
     * Note: Should be called when the class if fully created with all fields and properties (usually in a
     * {@link dk.tigerteam.trimm.mdsd.java.generator.event.DeferredEventHandler})
     *
     * @return The immuteable properties
     */
    public List<Property> getImmuteableProperties() {
        List<Property> properties = this.getAllPropertiesIncludingSuperClazzProperties();

        List<Property> result = new LinkedList<Property>();
        for (Property property : properties) {
            if (property.getMetaType() != null && (property.getMetaType().hasStereoType(MetaProperty.IMMUTABLE_STEREOTYPE_NAME) || !property.getMetaType().isWriteable())) {
                result.add(property);
            }
        }
        return result;
    }

    /**
     * Will only return clazzes included in the model and not possible
     * superclazzes that these clazzes extends like @{link AbstractEntity}
     *
     * @return all SuperClazzes of this clazz
     */
    public List<Clazz> getAllSuperClazzes() {
        List<Clazz> clazzes = new LinkedList<Clazz>();
        Clazz currentClazz = this.getExtendsType() != null ? this.getExtendsType().getClazz() : null;
        while (currentClazz != null) {
            clazzes.add(0, currentClazz);
            currentClazz = currentClazz.getExtendsType() != null ? currentClazz.getExtendsType().getClazz() : null;
        }
        return clazzes;
    }

    /**
     * Get all properties of this Clazz and then its super Clazz, and its super clazz' super clazz, etc. they are ordered from more specific
     * class (this class) to the least specific (the top most super class), to allow for fluent interface (where a method returns this
     * typically) safe method chaining
     *
     * @return list of all properties
     */
    public List<Property> getAllPropertiesIncludingSuperClazzProperties() {
        List<Property> properties = new LinkedList<Property>();
        Clazz currentClazz = this;
        while (currentClazz != null) {
            properties.addAll(0, currentClazz.getProperties());
            currentClazz = currentClazz.getExtendsType() != null ? currentClazz.getExtendsType().getClazz() : null;
        }
        return properties;
    }

    /**
     * Get all properties of this Clazz possible super clazzez, they are ordered from more specific
     * class (this class) to the least specific (the top most super class), to allow for fluent interface (where a method returns this
     * typically) safe method chaining
     *
     * @return list of all super clazz properties
     */
    public List<Property> getAllSuperClazzProperties() {
        List<Property> properties = new LinkedList<Property>();
        Clazz currentClazz = this.getExtendsType() != null ? this.getExtendsType().getClazz() : null;
        while (currentClazz != null) {
            properties.addAll(0, currentClazz.getProperties());
            currentClazz = currentClazz.getExtendsType() != null ? currentClazz.getExtendsType().getClazz() : null;
        }
        return properties;
    }

    public List<Property> getProperties() {
        return findChildrenOfType(Property.class);
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		CodePackage codePackage = getPackage();
//		if (codePackage != null) {
//			writer.print("package ").print(codePackage.getName()).println(";");
//		}
//
//		// TODO: Handle imports
//
//		writer.printJavaDoc(this);
//
//		writer.printAnnotations(this);
//		writer.printAccessModifiers(this);
//		writer.print("class ");
//		writer.print(getName());
//
//		if (getGenerics().size() > 0) {
//			writer.print("<");
//			writer.printCodeElementCollection(getGenerics());
//			writer.print(">");
//		}
//
//		writer.print(" ");
//		if (extendsType != null) {
//			writer.print("extends ");
//			extendsType.generateCode(writer);
//			writer.print(" ");
//		}
//		if (implementsTypes.size() > 0) {
//			writer.print("implements ");
//			writer.printCodeElementCollection(implementsTypes);
//		}
//
//		writer.println("{");
//
//		for (CodeElement child : getChildrenAsXmiElements()) {
//			child.generateCode(writer);
//		}
//
//		writer.println("}");
//	}

    /**
     * Get a method which matches the methodName and parameters list
     *
     * @param methodName The name of the method we're looking for
     * @param parameters The parameters that the method we're looking for must match
     * @return The matching method or <code>null</code> if no matching method can be found
     */
    public Method getMethod(String methodName, List<Parameter> parameters) {
        for (Method method : getMethods()) {
            if (methodName.equals(method.getName()) && method.getParameters().equals(parameters)) {
                return method;
            }
        }
        return null;
    }

    /**
     * Do we have a method which matches the methodName and parameters list<br/>
     * Functions as a guard method for {@link #getMethod(String, List)}
     *
     * @param methodName The name of the method we're looking for
     * @param parameters The parameters that the method we're looking for must match
     * @return The matching method or <code>null</code> if no matching method can be found
     */
    public boolean hasMethod(String methodName, List<Parameter> parameters) {
        return getMethod(methodName, parameters) != null;
    }

    /**
     * Get all methods from the collection of children
     *
     * @return The method's in this {@link Clazz}
     */
    public List<Method> getMethods() {
        return findImmediateChildrenOfType(Method.class);
    }

    public List<Constructor> getConstructors() {
        return findImmediateChildrenOfType(Constructor.class);
    }

    public Constructor getConstructor(List<Parameter> parameters) {
        for (Constructor constructor : getConstructors()) {
            if (constructor.getName().equals(this.getName()) && constructor.getParameters().equals(parameters)) {
                return constructor;
            }
        }
        return null;
    }

    public boolean hasConstructor(List<Parameter> parameters) {
        return getConstructor(parameters) != null;
    }

    public boolean hasDefaultConstructor() {
        for (Constructor constructor : getConstructors()) {
            if (this.getName().equals(constructor.getName()) && constructor.getParameters().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Is this clazz functioning as Extension Class for another class (aka. the base Class)
     */
    public boolean isExtensionClazz() {
        return isExtensionClazz;
    }

    /**
     * Is this clazz functioning as Extension Class for another class (aka. the base Class)
     */
    public ClazzOrInterface setExtensionClazz(boolean isExtensionClazz) {
        this.isExtensionClazz = isExtensionClazz;
        return this;
    }

    /**
     * Is this clazz functioning as Base Class for another class (aka. the Extension Class)
     */
    public boolean isBaseClazz() {
        return isBaseClazz;
    }

    /**
     * Is this clazz functioning as Base Class for another class (aka. the Extension Class)
     */
    public ClazzOrInterface setBaseClazz(boolean isBaseClazz) {
        this.isBaseClazz = isBaseClazz;
        return this;
    }

    public Clazz setExtendsType(Clazz superClazz) {
        setExtendsType(new Type(superClazz));
        return this;
    }

    public boolean implementsInterface(Interface _interface) {
        for (Type implementedType : getImplementsTypes()) {
            if (implementedType.getInterface() == null)
                continue;
            Interface implementedInterface = implementedType.getInterface();
            if (implementedInterface.equals(_interface)) {
                return true;
            }
            // Search up the interface hierarchy
            if (implementedInterface.extendsInterface(_interface)) {
                return true;
            }
        }

        // Check any superclazzes
        if (extendsType != null && extendsType.getClazz() != null) {
            return extendsType.getClazz().implementsInterface(_interface);
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(ClazzOrInterface o) {
        if (o instanceof Interface) {
            Interface other = (Interface) o;
            if (this.implementsInterface(other)) {
                return -1; // We're less since we implement the other interface
            } else {
                return 0;
            }
        } else {
            Clazz other = (Clazz) o;
            if (this.isSubClassOfClazz(other)) {
                return -1; // We're less since we extends the other clazz
            } else if (other.isSubClassOfClazz(this)) {
                return 1; // We're greater since since the other clazz extends us
            } else {
                return 0;
            }
        }
    }

    /**
     * Find a property by its name. If you need an exception if it's not found use {@link #getPropertyByNameRequired(String)}
     *
     * @param propertyByName The name of the property
     * @return The property instance, if found, or <code>null</code> if not found
     */
    public Property getPropertyByName(String propertyByName) {
        for (Property property : getProperties()) {
            if (propertyByName.equals(property.getName())) {
                return property;
            }
        }
        return null;
    }

    /**
     * Find a property by its name. If you don't want an exception if it's not found use {@link #getPropertyByName(String)}
     *
     * @param propertyByName The name of the property
     * @return The property instance, if found
     * @throws IllegalStateException if the property can't be found
     */
    public Property getPropertyByNameRequired(String propertyByName) {
        Property property = getPropertyByName(propertyByName);
        if (property != null) {
            return property;
        } else {
            throw new IllegalStateException("Couldn't find property by name '" + propertyByName + "' in " + this.toString());
        }
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

    @Override
    public String toString() {
        return "Clazz [name=" + getFQCN() + ", isExtensionClazz=" + isExtensionClazz + ", isBaseClazz=" + isBaseClazz + ", isInterface()=" + isInterface()
                + ", isClazz()=" + isClazz() + ", isTestClazz()=" + isTestClazz() + ", isEnumeration()="
                + isEnumeration() + ", isFabricated()=" + isFabricated() + "]";
    }

}
