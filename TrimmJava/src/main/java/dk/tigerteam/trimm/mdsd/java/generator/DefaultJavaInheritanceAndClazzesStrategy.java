/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator;


import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.Enumeration;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;

public class DefaultJavaInheritanceAndClazzesStrategy implements
        JavaInheritanceAndClazzesStrategy {

    public boolean isEnumerationClazz(Clazz clazz) {
        return clazz instanceof Enumeration;
    }

    public boolean isEnumerationClazz(Type type) {
        return type.getClazz() instanceof Enumeration;
    }

    /**
     * Default: <code>null</code>, meaning no specific Super class (i.e. will resolve to java.lang.Object by the Java compiler)
     */
    public Type resolveSuperClazzType(Clazz clazz) {
        return null;
    }
}
