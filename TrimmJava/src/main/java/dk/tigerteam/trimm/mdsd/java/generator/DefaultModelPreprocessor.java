/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.MetaType;
import dk.tigerteam.trimm.mdsd.meta.NameableMetaType;

public class DefaultModelPreprocessor implements ModelPreprocessor {

    @Override
    public MetaModel preprocessMetaModel(MetaModel metaModel) {
        metaModel = swapMetaModelAlias(metaModel);
        return metaModel;
    }

    private MetaModel swapMetaModelAlias(MetaModel metaModel) {
        for (MetaType metaType : metaModel.getChildren()) {
            if (metaType instanceof NameableMetaType && ((NameableMetaType) metaType).getAlias() != null) {
                NameableMetaType nameableMetaType = (NameableMetaType) metaType;
                String old = nameableMetaType.getName();
                nameableMetaType.setName(nameableMetaType.getAlias());
                nameableMetaType.setAlias(old);
            }
        }
        return metaModel;
    }

}
