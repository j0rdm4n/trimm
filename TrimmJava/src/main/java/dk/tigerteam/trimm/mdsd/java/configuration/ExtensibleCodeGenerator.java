package dk.tigerteam.trimm.mdsd.java.configuration;

import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaModelTransformer;

import java.io.File;
import java.util.List;

/**
 * Code generators that implement this interface will support loading extensions based on Java classes or Groovy classes/scripts
 * @since 1.0.1
 * @author Jeppe Cramon
 */
public interface ExtensibleCodeGenerator {
    /**
     * Load an extension <b>instance</b> - adheres to the following rules:
     * <ul>
     * <li>If it ends with .groovy is will be loaded as a Groovy Class by reading it in from the groovy script file using {@link #instantiateGroovyClassFromScriptFile(String)}</li>
     * <li>Otherwise it will be deferred to {@link #instantiateClass(String)}</li>
     * </ul>
     *
     * @param extensionConfiguration
     * @return The extension <b>instance</b>
     */
    <T> T loadExtension(ExtensionConfiguration extensionConfiguration);

    /**
     * Load an extension <b>class</b> - adheres to the following rules:
     * <ul>
     * <li>If it ends with .groovy is will be loaded as a Groovy Class by reading it in from the groovy script file using {@link #loadGroovyClass(String)}</li>
     * <li>Otherwise it will be deferred to {@link #loadClass(String)}</li>
     * </ul>
     *
     * @param extensionDefinition The extension definition (e.g. path to a groovy script file or a Fully Qualified Class Name)
     * @param <T>                 The type of object to return
     * @return The extension <b>class</b>
     */
    <T> Class<T> loadExtensionClass(String extensionDefinition);

    /**
     * Load a class from its Fully Qualified Class Name (FQCN)
     *
     * @param fqcn the class' Fully Qualified Class Name (FQCN)
     * @param <T>  The type of class
     * @return The class
     */
    <T> Class<T> loadClass(String fqcn);

    /**
     * Load a Groovy defined class from an <b>absolute</b> (or relative path which is accessible within the execution path of the generator)
     *
     * @param groovyScriptFilePath The groovy script file path
     * @param <T>                  The type of class to return
     * @return The groovy defined class
     */
    @SuppressWarnings("unchecked")
    <T> Class<T> loadGroovyClass(String groovyScriptFilePath);

    /**
     * The classloader to use for loading classes and groovy script defined classes.<br/>
     * If a customer classloader has been set then this will be used. Otherwise it will use the current threads Context Classloader.
     */
    ClassLoader getClassLoader();

    /**
     * Instantiate a Groovy defined class from a <b>relative</b> path
     *
     * @param relativeScriptPath The groovy script file path
     * @param <T>                The type of object to return
     * @return The groovy class <b>instance</b>
     */
    <T> T instantiateGroovyClassFromScriptFile(String relativeScriptPath);

    /**
     * Instantiate a class from its Fully Qualified Class Name (FQCN)
     *
     * @param fqcn The class' Fully Qualified Class Name (FQCN)
     * @param <T>  The type of object to return
     * @return The class <b>instance</b>
     */
    <T> T instantiateClass(String fqcn);

    /**
     * Get the base dir that this configuration is run in (used for calculating absolute path'es)
     */
    File getBaseDir();

    /**
     * The directory to use for output files
     */
    File getBaseOutputDir();
}
