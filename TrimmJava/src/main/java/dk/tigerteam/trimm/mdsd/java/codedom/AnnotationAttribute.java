/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * {@link AnnotationAttribute}' models the arguments/parameters/attribute (child of many names) that you supply an Annotation with. An
 * {@link AnnotationAttribute} can recursively contains {@link Annotation}' which again contains {@link AnnotationAttribute}'s. To create a
 * default value {@link AnnotationAttribute} either use {@link #defaultValue(Object...)} or
 * {@link Annotation#withDefaultValueAnnotationAttribute(Object...)}
 * <p/>
 * For AttributeValues that aren't other Annotations the following rules apply:
 * <ul>
 * <li>An instance of {@link Enum} - writes out the FQCN name for the Enum class + "." + the enum instances <code>.name()</code> value</li>
 * <li>An instance of a String - will be written out in Quotes " "</li>
 * <li>An instance of anything else - will be written out using the values <code>toString()</code> method</li>
 * </ul>
 *
 * @author Jeppe Cramon
 */
public class AnnotationAttribute extends CodeElement implements Cloneable {

    public static final String DEFAULT_VALUE_ATTRIBUTE_NAME = "";
    private List<Object> values = new LinkedList<Object>();

    public AnnotationAttribute() {
    }

    public AnnotationAttribute(String name) {
        super(name);
    }

    public static AnnotationAttribute defaultValue(String... defaultValues) {
        AnnotationAttribute attribute = new AnnotationAttribute(DEFAULT_VALUE_ATTRIBUTE_NAME);
        attribute.addValues(defaultValues);
        return attribute;
    }

    public static AnnotationAttribute defaultValue(Object... defaultValues) {
        AnnotationAttribute attribute = new AnnotationAttribute(DEFAULT_VALUE_ATTRIBUTE_NAME);
        attribute.addValues(defaultValues);
        return attribute;
    }

    public static AnnotationAttribute defaultValue(Annotation... defaultValues) {
        AnnotationAttribute attribute = new AnnotationAttribute(DEFAULT_VALUE_ATTRIBUTE_NAME);
        attribute.addValues(defaultValues);
        return attribute;
    }

    public static AnnotationAttribute defaultValue(List<Annotation> defaultValues) {
        AnnotationAttribute attribute = new AnnotationAttribute(DEFAULT_VALUE_ATTRIBUTE_NAME);
        attribute.values.addAll(defaultValues);
        return attribute;
    }

    public static AnnotationAttribute defaultValue(Enum<?>... defaultValues) {
        AnnotationAttribute attribute = new AnnotationAttribute(DEFAULT_VALUE_ATTRIBUTE_NAME);
        attribute.addValues(defaultValues);
        return attribute;
    }

    public AnnotationAttribute(String name, String... values) {
        super(name);
        addValues(values);
    }

    public AnnotationAttribute(String name, Enum<?>... values) {
        super(name);
        addValues(values);
    }

    public AnnotationAttribute(String name, Object... values) {
        super(name);
        addValues(values);
    }

    public AnnotationAttribute(String name, Annotation... values) {
        super(name);
        addValues(values);
    }

    /**
     * Values will typically be of either {@link String} or {@link Annotation} type
     */
    public List<Object> getValues() {
        return values;
    }

    public AnnotationAttribute setValues(List<Object> values) {
        this.values = values;
        return this;
    }

    public AnnotationAttribute setValues(String... values) {
        this.values.clear();
        return addValues(values);
    }

    public AnnotationAttribute setValues(Object... values) {
        this.values.clear();
        return addValues(values);
    }

    public AnnotationAttribute setValues(Enum<?>... values) {
        this.values.clear();
        return addValues(values);
    }

    public AnnotationAttribute addValues(String... values) {
        this.values.addAll(Arrays.asList(values));
        return this;
    }

    public AnnotationAttribute addValues(Object... values) {
        this.values.addAll(Arrays.asList(values));
        return this;
    }

    public AnnotationAttribute addValues(Enum<?>... values) {
        for (Enum<?> enumValue : values) {
            getValues().add(enumValue);
        }
        return this;
    }

    public AnnotationAttribute addValues(Annotation... annotations) {
        for (Annotation annotation : annotations) {
            getValues().add(annotation);
        }
        return this;
    }

    public boolean hasValues() {
        return values.size() > 0;
    }

    public boolean hasMultipleValues() {
        return values.size() > 1;
    }

    public AnnotationAttribute clone() {
        AnnotationAttribute result = new AnnotationAttribute();
        result.setName(getName());
        for (Object value : getValues()) {
            if (value instanceof Annotation) {
                result.addValues(((Annotation) value).clone());
            } else {
                result.addValues(value);
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((values == null) ? 0 : values.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final AnnotationAttribute other = (AnnotationAttribute) obj;
        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;
        if (values == null) {
            if (other.values != null)
                return false;
        } else if (!values.equals(other.values))
            return false;
        return true;
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		if (getName() != null && !getName().equals(DEFAULT_VALUE_ATTRIBUTE_NAME)) {
//			writer.print(getName()).print("=");
//		}
//		if (hasMultipleValues()) {
//			writer.print("{");
//		}
//		int count = 0;
//		for (Object value : values) {
//			if (count > 0) {
//				writer.print(", ");
//			}
//			if (value instanceof CodeElement) {
//				((CodeElement) value).generateCode(writer);
//			} else if (value instanceof Enum) {
//				Enum<?> enumValue = (Enum<?>) value;
//				writer.print(enumValue.getDeclaringClass().getName() + "." + enumValue.name());
//			} else if (value instanceof String) {
//				writer.print(Utils.quotedString((String) value));
//			} else {
//				writer.print(value.toString());
//			}
//			count++;
//		}
//		if (hasMultipleValues()) {
//			writer.print("}");
//		}
//	}

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
