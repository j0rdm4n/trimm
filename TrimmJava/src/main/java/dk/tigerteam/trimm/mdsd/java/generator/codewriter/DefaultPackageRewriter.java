/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.codewriter;

import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.CodePackage;
import dk.tigerteam.trimm.mdsd.java.generator.JavaStereotypes;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * Default {@link PackageRewriter} - Allows for setting a basePackageName, packagePrefix stripping, as well as packages that shouldn't be rewritten
 *
 * @author Jeppe Cramon
 */
public class DefaultPackageRewriter implements PackageRewriter {

    private String basePackageName;
    private String stripPackagePrefix;
    private Set<String> excludePackageRewritingForPackages = Collections.emptySet();

    public DefaultPackageRewriter() {
    }

    public DefaultPackageRewriter(String basePackageName) {
        this.basePackageName = basePackageName;
    }

    public DefaultPackageRewriter(String basePackageName, String stripPackagePrefix) {
        this.basePackageName = basePackageName;
        this.stripPackagePrefix = stripPackagePrefix;
    }

    public DefaultPackageRewriter(String basePackageName, Collection<String> excludePackageRewritingForPackages) {
        this.basePackageName = basePackageName;
        if (excludePackageRewritingForPackages != null) {
            this.excludePackageRewritingForPackages.addAll(excludePackageRewritingForPackages);
        }
    }

    public DefaultPackageRewriter(String basePackageName, String stripPackagePrefix, Collection<String> excludePackageRewritingForPackages) {
        this.basePackageName = basePackageName;
        this.stripPackagePrefix = stripPackagePrefix;
        if (excludePackageRewritingForPackages != null) {
            this.excludePackageRewritingForPackages.addAll(excludePackageRewritingForPackages);
        }
    }

    public void rewrite(CodePackage package_) {
        boolean renamePackage = true;
        if (package_.getMetaType() != null && package_.getMetaType().hasStereoType(JavaStereotypes.DONT_GENERATE_STEREOTYPE_NAME)) {
            renamePackage = false;
        }

        if (stripPackagePrefix != null) {
            stripPackagePrefix(stripPackagePrefix, package_);
        }
        if (renamePackage && !exclude(excludePackageRewritingForPackages, package_)) {
            package_.setName(internalRewrite(package_.getName()));
        }

    }

    public void rewrite(ClazzOrInterface clazz) {
        if (clazz.getPackage() != null && !clazz.getMetaType().hasStereoType(JavaStereotypes.DONT_GENERATE_STEREOTYPE_NAME)) {
            rewrite(clazz.getPackage());
        }
    }

    public String rewrite(String fqcn) {
        return fqcn;
    }

    private String internalRewrite(String fqcn) {
        if (!fqcn.startsWith(basePackageName)) {
            fqcn = basePackageName + "." + fqcn;
        }
        return fqcn;
    }

    public boolean exclude(Set<String> excludePackageRewritingForPackages, CodePackage package_) {
        if (excludePackageRewritingForPackages == null) {
            return false;
        }
        for (String excludePackage : excludePackageRewritingForPackages) {
            if (package_.getName().startsWith(excludePackage) || package_.getName().equals(excludePackage)) {
                return true;
            }
        }
        return false;
    }

    public void stripPackagePrefix(String prefix, CodePackage package_) {
        if (package_.getName().startsWith(prefix)) {
            String pack = package_.getName().replace(prefix, "");
            pack = pack.trim();
            if (pack.startsWith(".")) {
                pack = pack.substring(1, pack.length());
            }
            package_.setName(pack);
        }
    }

    public String getBasePackageName() {
        return basePackageName;
    }

    public void setBasePackageName(String basePackageName) {
        this.basePackageName = basePackageName;
    }

    public Set<String> getExcludePackageRewritingForPackages() {
        return excludePackageRewritingForPackages;
    }

    public String getStripPackagePrefix() {
        return stripPackagePrefix;
    }

    public void setStripPackagePrefix(String stripPackagePrefix) {
        this.stripPackagePrefix = stripPackagePrefix;
    }
}
