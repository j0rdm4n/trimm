/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

/**
 * Models a Java field. Can have {@link #initializerStatement} which determines what instance type to use (and how it's acquired).
 *
 * @author Jeppe Cramon
 */
public class Field extends CodeElement {
    private Type type;
    private CodeSnippet initializerStatement;

    public Field(AccessModifier... accessModifiers) {
        super(accessModifiers);
        if (accessModifiers == null || accessModifiers.length == 0) {
            addAccessModifiers(AccessModifier.Private);
        }
    }

    public Field(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
        if (accessModifiers == null || accessModifiers.length == 0) {
            addAccessModifiers(AccessModifier.Private);
        }
    }

    public Field(String name, Type type, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
        this.type = type;
        if (accessModifiers == null || accessModifiers.length == 0) {
            addAccessModifiers(AccessModifier.Private);
        }
    }

    public Field(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
        if (accessModifiers == null || accessModifiers.length == 0) {
            addAccessModifiers(AccessModifier.Private);
        }
    }

    public Field(String name, Clazz owner, boolean fabricated, AccessModifier... accessModifiers) {
        this(name, fabricated, accessModifiers);
        setOwner(owner);
    }

    /**
     * Used for Dynamically types languages, like Groovy, where fields, variables, properties, etc. doesn't need types
     *
     * @return true if no {@link #getType()} has been specified
     */
    public boolean isDynamicallyTyped() {
        return type == null;
    }

    public Type getType() {
        return type;
    }

    public Field setType(Type type) {
        this.type = type;
        return this;
    }

    public Field setType(Clazz clazz) {
        this.type = new Type(clazz);
        return this;
    }

    public Field setType(Class<?> clazz) {
        this.type = new Type(clazz);
        return this;
    }

    public CodeSnippet getInitializerStatement() {
        return initializerStatement;
    }

    public Field setInitializerStatement(CodeSnippet initializedStatement) {
        this.initializerStatement = initializedStatement;
        return this;
    }

    public ClazzOrInterface getClazzOrInterfaceOwner() {
        if (getOwner() == null) {
            return null;
        }
        if (getOwner() instanceof Property) {
            return ((Property) getOwner()).getOwner();
        } else if (getOwner() instanceof ClazzOrInterface) {
            return (ClazzOrInterface) getOwner();
        } else {
            throw new IllegalStateException("Didn't expect Field owner to be of type " + getOwner().getClass());
        }
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		if (getClazzOrInterfaceOwner().isInterface() && !this.hasAccessModifier(AccessModifier.Static)) {
//			return;
//		}
//
//		writer.printJavaDoc(this);
//		writer.printAnnotations(this);
//		writer.printAccessModifiers(this);
//		type.generateCode(writer);
//		writer.print(" ");
//		writer.print(getName());
//		if (initializerStatement != null) {
//			writer.print(" = ");
//			initializerStatement.generateCode(writer);
//		}
//		writer.println(";");
//
//	}

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
