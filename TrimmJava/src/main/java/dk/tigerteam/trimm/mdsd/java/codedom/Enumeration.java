/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.meta.MetaEnumeration;

import java.util.List;

/**
 * Models a Java {@link Enum}, which is our model is a specialization of {@link Clazz}. All the enumeration values are stored in the
 * {@link #getChildren()} bag as {@link Field}'s.
 *
 * @author Jeppe Cramon
 */
public class Enumeration extends Clazz {
    public Enumeration(AccessModifier... accessModifiers) {
        super(accessModifiers);
    }

    public Enumeration(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
    }

    public Enumeration(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

    @Override
    public MetaEnumeration getMetaType() {
        return (MetaEnumeration) super.getMetaType();
    }

    public List<EnumLiteralField> getEnumValues() {
        return this.findChildrenOfType(EnumLiteralField.class);
    }

    public List<Property> getProperties() {
        return this.findChildrenOfType(Property.class);
    }

    @Override
    public boolean isEnumeration() {
        return true;
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		CodePackage codePackage = getPackage();
//		if (codePackage != null) {
//			writer.print("package ").print(codePackage.getName()).println(";");
//		}
//		writer.printJavaDoc(this);
//		writer.printAnnotations(this);
//		writer.printAccessModifiers(this);
//		writer.print("enum ");
//		writer.print(getName());
//		writer.print(" ");
//		if (getImplementsTypes().size() > 0) {
//			writer.print("implements ");
//			writer.printCodeElementCollection(getImplementsTypes());
//		}
//		writer.println(" {");
//		List<EnumLiteralField> enumValues = this.getEnumValues();
//		writer.printCodeElementCollection(enumValues);
//		writer.println(";");
//
//		// Write out the rest of the children if any (its late, perhaps there's a better way to sort this stuff out)
//		List<CodeElement> remainingChildren = new LinkedList<CodeElement>();
//		for (CodeElement child : getChildrenAsXmiElements()) {
//			if (!enumValues.contains(child)) {
//				remainingChildren.add(child);
//			}
//		}
//
//		for (CodeElement child : remainingChildren) {
//			child.generateCode(writer);
//		}
//
//		writer.println("}");
//	}

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
