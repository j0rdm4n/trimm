/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.bean.bidirectional.OneToManyListWrapper;
import dk.tigerteam.trimm.exception.FindException;
import dk.tigerteam.trimm.mdsd.meta.MetaType;
import dk.tigerteam.trimm.util.ObjectUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * A {@link CodeComposite} is an abstract base class for all {@link CodeElement}' that can contain children (according to a variation of the
 * Composite pattern where there's a difference between the Leaf/Child and the Composite/Owner).
 * <p/>
 * All Owner/child relations are handled bidirectionally, so you don't have to maintain this yourself.
 *
 * @author Jeppe Cramon
 */
public abstract class CodeComposite extends CodeElement {

    private List<CodeElement> children = new ArrayList<CodeElement>();

    public CodeComposite(AccessModifier... accessModifiers) {
        super(accessModifiers);
    }

    public CodeComposite(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
    }

    public CodeComposite(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

    /**
     * Do we have any children?
     */
    public boolean hasChildren() {
        return children.size() > 0;
    }

    /**
     * Get all the children of this element
     */
    public List<CodeElement> getChildren() {
        return new OneToManyListWrapper<CodeComposite, CodeElement>(this, children) {
            private static final long serialVersionUID = -1791557405295752264L;

            @Override
            protected CodeComposite getOneSideObjectInManySideObject(CodeElement manySideObject) {
                return manySideObject.getOwner();
            }

            @Override
            protected void setOneSideObjectInManySideObject(CodeElement manySideObject, CodeComposite oneSideObject) {
                manySideObject.setOwner(oneSideObject);
            }
        };
    }

    /**
     * Add all the children to this element (use #add instead)
     *
     * @param children The children to add
     * @return This element instance
     */
    public CodeComposite addChildren(CodeElement... children) {
        getChildren().addAll(Arrays.asList(children));
        return this;
    }

    /**
     * Add all the children to this element
     *
     * @param children The children to add
     * @return This element instance
     */
    public CodeComposite add(CodeElement... children) {
        getChildren().addAll(Arrays.asList(children));
        return this;
    }

    /**
     * Find a single child of the given type and with the given name. We search in <b>this</b> {@link CodeComposite} element and all its
     * children recursively.
     *
     * @param <T>         The type of element to find (or a subtype thereof)
     * @param elementType The type of element that we want to find
     * @param name        The name of the {@link CodeElement} that we want to find
     * @return The element found or <code>null</code> if no matching element could be found
     * @throws FindException in case we found more than one matching elements
     */
    public <T extends CodeElement> T findChildWithName(final Class<T> elementType, final String name) {
        return findSingle(new Criteria<T>(elementType) {
            public boolean isOk(T element) {
                return (ObjectUtil.equals(element.getName(), name));
            }

            @Override
            public String toString() {
                return "findChildWithName '" + name + "' and type '" + elementType + "'";
            }
        });
    }

    /**
     * Find a single child of the given type and with the given name. We only search in <b>this</b> {@link CodeComposite} elements
     * immediate/direct children.
     *
     * @param <T>         The type of element to find (or a subtype thereof)
     * @param elementType The type of element that we want to find
     * @param name        The name of the {@link CodeElement} that we want to find
     * @return The element found or <code>null</code> if no matching element could be found
     * @throws FindException in case we found more than one matching elements
     */
    public <T extends CodeElement> T findImmediateChildWithName(final Class<T> elementType, final String name) {
        return findSingleInImmediateChildren(new Criteria<T>(elementType) {
            public boolean isOk(T element) {
                return (ObjectUtil.equals(element.getName(), name));
            }

            @Override
            public String toString() {
                return "findImmediateChildWithName '" + name + "' and type '" + elementType + "'";
            }
        });
    }

    /**
     * Find a single child of the given type. We search <b>this</b> {@link CodeComposite} element and all its children recursively.
     *
     * @param <T>         The type of elements to find (or subtypes thereof)
     * @param elementType The type of element that we want to find
     * @return The element found or <code>null</code> if no matching element could be found
     * @throws FindException in case we found more than one matching elements
     */
    public <T extends CodeElement> T findChildOfType(final Class<T> elementType) {
        return findSingle(new Criteria<T>(elementType) {
            public boolean isOk(T element) {
                return true;
            }

            @Override
            public String toString() {
                return "findChildOfType type '" + elementType + "'";
            }
        });
    }

    /**
     * Find children of the given type. We search <b>this</b> {@link CodeComposite} element and all its children recursively.
     *
     * @param <T>         The type of elements to find (or subtypes thereof)
     * @param elementType The type of elements that we want to find
     * @return The elements found
     */
    public <T extends CodeElement> List<T> findChildrenOfType(final Class<T> elementType) {
        return find(new Criteria<T>(elementType) {
            public boolean isOk(T element) {
                return true;
            }

            @Override
            public String toString() {
                return "findChildrenOfType type '" + elementType + "'";
            }
        });
    }

    /**
     * Find children of the given type. We only search in the immediate/direct children of this {@link CodeComposite} element<br/>
     * Note: This {@link CodeComposite} element will not be checked.
     *
     * @param <T>         The type of elements to find (or subtypes thereof)
     * @param elementType The type of elements that we want to find
     * @return The elements found
     */
    public <T extends CodeElement> List<T> findImmediateChildrenOfType(final Class<T> elementType) {
        return findInImmediateChildren(new Criteria<T>(elementType) {
            public boolean isOk(T element) {
                return true;
            }

            @Override
            public String toString() {
                return "findImmediateChildrenOfType type '" + elementType + "'";
            }
        });
    }

    /**
     * Search <b>this</b> {@link CodeComposite} element and all its children recursively, using the {@link Criteria}, <b>for a single matching
     * element</b>
     *
     * @param <T>      The type of element to find (or a subtype thereof)
     * @param criteria The criteria to use
     * @return The single element found or <code>null</code> if no matching element could be found
     * @throws FindException in case we found more than one matching elements
     */
    public <T extends CodeElement> T findSingle(Criteria<T> criteria) {
        List<T> result = find(criteria);
        if (result.isEmpty()) {
            return null;
        } else if (result.size() == 1) {
            return result.get(0);
        } else {
            throw new FindException("Found '" + result.size() + "' results from Criteria: " + criteria.toString());
        }
    }

    /**
     * Find according to the {@link Criteria}, but only in the immediate/direct children of this {@link CodeComposite} element<br/>
     * Note: This {@link CodeComposite} element will not be checked against the criteria
     *
     * @param <T>      The type of element to find (or a subtype thereof)
     * @param criteria The criteria to use
     * @return The single element found or <code>null</code> if no matching element could be found
     * @throws FindException in case we found more than one matching elements
     */
    public <T extends CodeElement> T findSingleInImmediateChildren(Criteria<T> criteria) {
        List<T> result = findInImmediateChildren(criteria);
        if (result.isEmpty()) {
            return null;
        } else if (result.size() == 1) {
            return result.get(0);
        } else {
            throw new FindException("Found '" + result.size() + "' results from Criteria: " + criteria.toString());
        }
    }

    /**
     * Search <b>this</b> {@link CodeComposite} element and all its children recursively using the {@link Criteria}.
     *
     * @param <T>      The type of elements to find (or subtypes thereof)
     * @param criteria The criteria to use
     * @return The elements found
     */
    @SuppressWarnings("unchecked")
    public <T extends CodeElement> List<T> find(Criteria<T> criteria) {
        Class<?> tClass = (Class<?>) criteria.getType();
        List<T> result = new LinkedList<T>();
        if (tClass.isAssignableFrom(this.getClass()) && criteria.isOk((T) this)) {
            result.add((T) this);
        }
        recursivelyFind(this, criteria, result, tClass);
        return result;
    }

    /**
     * Internal helper method for {@link #find(Criteria)}
     */
    @SuppressWarnings("unchecked")
    private <T extends CodeElement> void recursivelyFind(CodeComposite composite, Criteria<T> criteria, List<T> elementsFound, Class<?> tClass) {
        for (CodeElement element : composite.children) {
            if (tClass.isAssignableFrom(element.getClass()) && criteria.isOk((T) element)) {
                elementsFound.add((T) element);
            }
            if (element instanceof CodeComposite) {
                recursivelyFind((CodeComposite) element, criteria, elementsFound, tClass);
            }
        }
    }

    /**
     * Find according to the {@link Criteria}, but only in the immediate/direct children of this {@link CodeComposite} element<br/>
     * Note: This {@link CodeComposite} element will not be checked against the criteria
     *
     * @param <T>      The type of elements to find (or subtypes thereof)
     * @param criteria The criteria to use
     * @return The elements found
     */
    @SuppressWarnings("unchecked")
    public <T extends CodeElement> List<T> findInImmediateChildren(Criteria<T> criteria) {
        List<T> result = new LinkedList<T>();
        Class<?> tClass = criteria.getType();
        for (CodeElement element : this.children) {
            if (tClass.isAssignableFrom(element.getClass()) && criteria.isOk((T) element)) {
                result.add((T) element);
            }
        }
        return result;
    }

    /**
     * Search <b>this</b> {@link CodeComposite} element and all its children recursively, for a single matching
     * code element who {@link dk.tigerteam.trimm.mdsd.java.codedom.CodeElement#getMetaType()} has a matching {@link MetaType#getId()}
     *
     * @param <T>               The actual CodeElement type
     * @param typeOfJavaElement the actual CodeElement type as a class
     * @param metaTypeId        The id of the MetaType that we want to match
     * @return the matching element
     * @throws IllegalStateException in case it can't find the JavaElement from the {@link MetaType#getId()}
     */
    public <T extends CodeElement> T getByMetaTypeId(final Class<T> typeOfJavaElement, final String metaTypeId) {
        T findSingle = findByMetaTypeId(typeOfJavaElement, metaTypeId);
        if (findSingle == null) {
            throw new IllegalStateException("Couldn't find a '" + typeOfJavaElement.getSimpleName() + "' by its metaTypeId '" + metaTypeId + "'");
        } else {
            return findSingle;
        }
    }

    /**
     * Search <b>this</b> {@link CodeComposite} element and all its children recursively, for a single matching
     * code element who {@link dk.tigerteam.trimm.mdsd.java.codedom.CodeElement#getMetaType()} has a matching {@link MetaType#getId()}
     *
     * @param <T>               The actual CodeElement type
     * @param typeOfJavaElement The actual CodeElement type as a class
     * @param metaTypeId        The id of the MetaType that we want to match
     * @return The matching element or <code>null</code>
     */
    public <T extends CodeElement> T findByMetaTypeId(final Class<T> typeOfJavaElement, final String metaTypeId) {
        return findSingle(new Criteria<T>(typeOfJavaElement) {
            @Override
            public boolean isOk(T element) {
                return element.getMetaType() != null && element.getMetaType().getId().equals(metaTypeId);
            }
        });
    }
}
