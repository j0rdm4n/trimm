/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import java.util.List;

/**
 * Models a Java style {@link java.lang.annotation.Annotation}.<br/>
 * An {@link Annotation} can contain {@link AnnotationAttribute}' which model the arguments/parameters/attribute (child of many names) that
 * you supply an Annotation with.
 *
 * @author Jeppe Cramon
 * @see AnnotationAttribute
 */
public class Annotation extends CodeComposite implements Cloneable {

    public Annotation() {
        super();
    }

    public Annotation(String name) {
        super(name);
    }

    public Annotation(Class<? extends java.lang.annotation.Annotation> annotationClazz) {
        super(annotationClazz.getName());
    }

    public Annotation(Class<? extends java.lang.annotation.Annotation> annotationClazz, boolean external) {
        super(annotationClazz.getName());
        setExternal(external);
    }

    public List<AnnotationAttribute> getAnnotationAttributes() {
        return findImmediateChildrenOfType(AnnotationAttribute.class);
    }

    /**
     * Strips package from name
     *
     * @return simple annotation class name
     */
    public String getSimpleName() {
        int index = getName().lastIndexOf(".");
        if (index != -1) {
            return getName().substring(index + 1, getName().length());
        }
        return getName();
    }

    public Annotation addAnnotationAttributes(AnnotationAttribute... annotationAttributes) {
        if (annotationAttributes != null) {
            for (AnnotationAttribute annotationAttribute : annotationAttributes) {
                AnnotationAttribute existingAnnotationAttribute = getAnnotationAttribute(annotationAttribute.getName());
                if (existingAnnotationAttribute != null) {
                    existingAnnotationAttribute.addValues(annotationAttribute.getValues().toArray());
                } else {
                    getChildren().add(annotationAttribute);
                }
            }
        }
        return this;
    }

    /**
     * Supply with an array of name/value pairs (doesn't support multiple values for any key).<br/>
     *
     * @param annotationAttributes the annotation attribute name/value pairs (e.g. "attributeKey1", "value1", "attributeKey2", "value2")
     * @return This annotation instance
     */
    public Annotation addAnnotationAttributes(String... annotationAttributes) {
        if (annotationAttributes != null) {
            if (annotationAttributes.length % 2 != 0) {
                throw new IllegalArgumentException("Found " + annotationAttributes.length + " annotation attributes, which is an uneven number");
            }
            for (int i = 0; i < annotationAttributes.length; i += 2) {
                addAnnotationAttributes(new AnnotationAttribute(annotationAttributes[i], annotationAttributes[i + 1]));
            }
        }
        return this;
    }

    /**
     * Supply with name/value pair
     */
    public Annotation addAnnotationAttribute(String name, Annotation... values) {
        addAnnotationAttributes(new AnnotationAttribute(name, values));
        return this;
    }

    /**
     * Supply with name/value pair
     */
    public Annotation addAnnotationAttribute(String name, String... values) {
        addAnnotationAttributes(new AnnotationAttribute(name, values));
        return this;
    }

    /**
     * Supply with name/value pair
     */
    public Annotation addAnnotationAttribute(String name, Object... values) {
        addAnnotationAttributes(new AnnotationAttribute(name, values));
        return this;
    }

    /**
     * Get an {@link AnnotationAttribute} by its name
     *
     * @param name The name of the {@link AnnotationAttribute}
     * @return The {@link AnnotationAttribute} instance or <code>null</code> if this {@link Annotation} didn't contain an
     *         {@link AnnotationAttribute} with the given name
     */
    public AnnotationAttribute getAnnotationAttribute(String name) {
        for (AnnotationAttribute annotationAttribute : getAnnotationAttributes()) {
            if (annotationAttribute.getName().equals(name)) {
                return annotationAttribute;
            }
        }
        return null;
    }

    /**
     * Supply with name/value pair
     */
    public Annotation addAnnotationAttribute(String name, Enum<?>... values) {
        addAnnotationAttributes(new AnnotationAttribute(name, values));
        return this;
    }

    // ---------------------- Default value -----------------------
    public Annotation withDefaultValueAnnotationAttribute(Annotation... values) {
        addAnnotationAttributes(AnnotationAttribute.defaultValue(values));
        return this;
    }

    public Annotation withDefaultValueAnnotationAttribute(String... values) {
        addAnnotationAttributes(AnnotationAttribute.defaultValue(values));
        return this;
    }

    public Annotation withDefaultValueAnnotationAttribute(Object... values) {
        addAnnotationAttributes(AnnotationAttribute.defaultValue(values));
        return this;
    }

    public Annotation withDefaultValueAnnotationAttribute(Enum<?>... values) {
        addAnnotationAttributes(AnnotationAttribute.defaultValue(values));
        return this;
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		writer.print("@").print(getName());
//		if (hasChildren()) {
//			writer.print("(");
//			writer.printCodeElementCollection(getChildrenAsXmiElements());
//			writer.print(")");
//		}
//		writer.println();
//	}

    /**
     * Returns the values of the default value annotation attribute. The value will typically be either {@link String}, Enum or
     * {@link Annotation}
     *
     * @return The value
     * @throws IllegalArgumentException in case no AnnotationAttribute with the given name exists
     * @see AnnotationAttribute#DEFAULT_VALUE_ATTRIBUTE_NAME
     */
    public List<?> getDefaultValueAnnotationAttributeValues() {
        return getAnnotationAttributeValues(AnnotationAttribute.DEFAULT_VALUE_ATTRIBUTE_NAME);
    }

    /**
     * Returns the single value of the default value annotation attribute. The value will typically be either {@link String}, Enum or
     * {@link Annotation}. If more than one value exists, it throws an IllegalArgumentException
     *
     * @return The value if found, null if there are no values
     * @throws IllegalArgumentException in case no AnnotationAttribute with the given name exists or if there is more than 1 value.
     * @see AnnotationAttribute#DEFAULT_VALUE_ATTRIBUTE_NAME
     */
    public Object getDefaultValueAnnotationAttributeValue() {
        return getAnnotationAttributeValue(AnnotationAttribute.DEFAULT_VALUE_ATTRIBUTE_NAME);
    }

    /**
     * Does this {@link Annotation} have an {@link AnnotationAttribute} with the given name
     *
     * @param name The name of the {@link AnnotationAttribute}
     * @return true this {@link Annotation} have an {@link AnnotationAttribute} with the given name, otherwise false
     */
    public boolean hasAnnotationAttribute(String name) {
        return getAnnotationAttribute(name) != null;
    }

    /**
     * Sets the values of the annotation attribute with the given name. Each value will typically be either {@link String} or
     * {@link Annotation}.<br/>
     * If an AnnotationAttribute with the given name doesn't exist it will be added
     *
     * @param name   The name of the AnnotationAttribute
     * @param values The values
     */
    public void setAnnotationAttributeValues(String name, List<Object> values) {
        AnnotationAttribute annotationAttribute = getAnnotationAttribute(name);
        if (annotationAttribute == null) {
            annotationAttribute = new AnnotationAttribute(name);
            addAnnotationAttributes(annotationAttribute);
        }
        annotationAttribute.setValues(values);
    }

    /**
     * Returns the values of the annotation attribute with the given name. The value will typically be either {@link String} or
     * {@link Annotation}
     *
     * @param name The annotation-attribute name
     * @return The value
     * @throws IllegalArgumentException in case no AnnotationAttribute with the given name exists
     */
    public List<Object> getAnnotationAttributeValues(String name) {
        AnnotationAttribute annotationAttribute = getAnnotationAttribute(name);
        if (annotationAttribute != null) {
            return annotationAttribute.getValues();
        }
        throw new IllegalArgumentException("Couldn't find an AnnotationAttribute with name '" + name + "' in Annotation @'" + getName() + "'");
    }

    /**
     * Returns the single value of the annotation attribute with the given name. The value will typically be either {@link String} or
     * {@link Annotation}. If more than one value exists, it throws an IllegalArgumentException
     *
     * @param name The annotation-attribute name
     * @return The value if found, null if there are no values
     * @throws IllegalArgumentException in case no AnnotationAttribute with the given name exists or if there is more than 1 value.
     */
    public Object getAnnotationAttributeValue(String name) {
        List<?> values = getAnnotationAttributeValues(name);
        if (values.isEmpty()) {
            return null;
        } else if (values.size() == 1) {
            return values.get(0);
        } else {
            throw new IllegalArgumentException("Found '" + values.size() + "' values for AnnotationAttribute with name '" + name
                    + "' in Annotation @'" + getName() + "'. Expected to find 0 or 1 value!");
        }
    }

    @Override
    public Annotation clone() {
        Annotation result = new Annotation();
        result.setName(getName());
        result.setExternal(isExternal());
        for (AnnotationAttribute attribute : getAnnotationAttributes()) {
            result.addAnnotationAttributes(attribute.clone());
        }
        return result;
    }

    public void removeAnnotationAttribute(String name) {
        AnnotationAttribute annotationAttribute = getAnnotationAttribute(name);
        if (annotationAttribute != null) {
            getChildren().remove(annotationAttribute);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getChildren() == null) ? 0 : getChildren().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Annotation other = (Annotation) obj;
        if (getChildren() == null) {
            if (other.getChildren() != null)
                return false;
        } else if (!getChildren().equals(other.getChildren()))
            return false;
        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;
        return true;
    }

    /**
     * Merge the annotation attribute with the definitions in the annotation parameter.<br/>
     * The attributes in the the annotation parameter will win in case of a collision (meaning the existing {@link AnnotationAttribute}'s will
     * get its values overridden with the values in the {@link AnnotationAttribute} that comes from the annotation parameter
     *
     * @param annotation The annotation who's attributes should be merge with this annotation instance
     * @return This annotation instance
     */
    public Annotation mergeAttributesWith(Annotation annotation) {
        for (AnnotationAttribute annotationAttribute : annotation.getAnnotationAttributes()) {
            AnnotationAttribute existingAttribute = this.getAnnotationAttribute(annotationAttribute.getName());
            if (existingAttribute != null) {
                existingAttribute.setValues(annotationAttribute.getValues());
            } else {
                this.addAnnotationAttributes(annotationAttribute);
            }
        }
        return this;
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
