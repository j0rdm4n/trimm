/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import dk.tigerteam.trimm.mdsd.meta.MetaType;

import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class TestDataCreatorGenerator {

    public static List<ClazzOrInterface> postProcessClazzes(String destinationPackageName, List<ClazzOrInterface> allClazzes) {
        CodePackage destinationPackage = new CodePackage(destinationPackageName);
        JavaGeneratorContext.getContext().getCodeModel().addChildren(destinationPackage);
        Clazz testDataCreator = new Clazz("TestDataCreator", true);
        testDataCreator.addAccessModifiers(AccessModifier.Public, AccessModifier.Abstract);
        destinationPackage.addChildren(testDataCreator);


        for (ClazzOrInterface clazzOrInterface : allClazzes) {
            if (!clazzOrInterface.isInterface()) {
                Clazz clazz = (Clazz) clazzOrInterface;
                if (!(clazz instanceof Enumeration) && !clazz.getAccessModifiers().contains(AccessModifier.Abstract) && !clazz.isFabricated()) {
                    generateCreateMethodForClazz(testDataCreator, clazz, false);
                    generateCreateMethodForClazz(testDataCreator, clazz, true);
                }
            }
        }

        allClazzes.add(testDataCreator);
        return allClazzes;
    }

    private static void generateCreateMethodForClazz(Clazz testDataCreator, Clazz clazz, boolean onlyRequired) {
        // We're going to sort the properties so any XMI export order difference doesn't affect the generated
        // create methods argument order.
        SortedSet<Property> properties = new TreeSet<Property>(new Comparator<Property>() {
            public int compare(Property o1, Property o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        properties.addAll(clazz.getAllPropertiesIncludingSuperClazzProperties());

        Method createMethod = new Method("create" + clazz.getName() + (onlyRequired ? "Required" : ""), true);
        testDataCreator.addChildren(createMethod);
        createMethod.addAccessModifiers(AccessModifier.Public, AccessModifier.Static);
        createMethod.setReturnType(new Type(clazz));

        String instanceVarName = "result";
        createMethod.addChildren(new CodeSnippet(clazz.asFQCN(), " ", instanceVarName, " = new ", clazz.asFQCN(), "();"));

        StringBuilder doc = new StringBuilder();
        doc.append("Create a new " + clazz.getName());
        if (clazz.getMetaType().getDocumentation() != null) {
            doc.append(" - <i>" + clazz.getMetaType().getDocumentation() + "</i>");
        }

        for (Property property : properties) {
            if (property.getField().getAccessModifiers().contains(AccessModifier.Static)) {
                continue;
            }
            boolean isAllowedNull = property.getMetaType().getMinCardinality() == 0;
            if (onlyRequired && isAllowedNull) {
                continue;
            }
            Parameter parameter = new Parameter(property.getName());
            parameter.setType(property.getField().getType());
            createMethod.addParameters(parameter);

            doc.append(MetaType.newline).append("@param ").append(parameter.getName()).append(" Set" + (isAllowedNull ? " <b>optional</b> " : " "))
                    .append(parameter.getName());
            if (property.getMetaType().getDocumentation() != null) {
                doc.append(" - <i>" + property.getMetaType().getDocumentation() + "</i>");
            }

            if (property.getMetaType().getCardinalityType() == CardinalityType.Many) {

                if (isAllowedNull) {
                    createMethod.addChildren(new CodeSnippet("if (", parameter.getName(), " != null) {"));
                }
                createMethod.addChildren(new CodeSnippet(instanceVarName, ".", property.getGetterMethod().getName(), "().addAll(", parameter.getName(), ");"));
                if (isAllowedNull) {
                    createMethod.addChildren(new CodeSnippet("}"));
                }
            } else {
                if (property.getSetterMethod() == null) {
                    //throw new IllegalStateException("Null setter method for MetaProperty " + property.getMetaType() + " in class " + clazz);
                    continue;
                }
                createMethod.addChildren(new CodeSnippet(instanceVarName, ".", property.getSetterMethod().getName(), "(", parameter.getName(), ");"));
            }

        }

        createMethod.addChildren(new CodeSnippet("return ", instanceVarName, ";"));
        doc.append(MetaType.newline).append("@returns The newly created " + clazz.getName() + " with all properties set as specified in the parameterlist");
        createMethod.setCodeDocumentation(doc.toString());
    }
}
