/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;

/**
 * All interesting things in the {@link dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator} happens as events.<br/>
 * The {@link GeneratorEvent} is the base class for all Event specializations.
 *
 * @author Jeppe Cramon
 */
public abstract class GeneratorEvent {

    /**
     * Does the actual processing related to the event. This method is responsible for broadcasting it self to registered
     * {@link GeneratorEventListener}'s afterwards
     *
     * @return The {@link CodeElement} that was a result of the event processing (if any)
     */
    public abstract CodeElement execute();

    /**
     * Call this method to broadcast this {@link GeneratorEvent} instance to all registered {@link GeneratorEventListener}'.<br/>
     * This is a shorthand for calling {@link JavaGeneratorContext#broadcastEvent(GeneratorEvent)}
     */
    protected void broadcastEvent() {
        JavaGeneratorContext.getContext().broadcastEvent(this);
    }
}
