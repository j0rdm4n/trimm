/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.java.generator.codewriter.PackageRewriter;

/**
 * A Package FQC allows you to export a {@link CodePackage}'s Fully Qualified Name (FQN) in (for instance) a {@link CodeSnippet}.<br/>
 * The FQN can be returned by calling {@link CodePackage#asFQN()} or {@link CodePackage#getName()}. The difference between these two calls
 * is that {@link CodePackage#getFQN()} is resolved at calltime, whereas {@link CodePackage#asFQN()}, codegeneration wise, is resolved
 * <b>after</b> package rewriting has occurred.
 * <p/>
 * Example: <code>new CodeSnippet("import", javaPackage.asFQN(), ";");</code>
 *
 * @author Jeppe Cramon
 * @see JavaCodeWriter_old
 * @see PackageRewriter
 * @see CodePackage#asFQN()
 * @see CodePackage#getName()
 */
public class PackageFQN extends CodeElement {

    private CodePackage codePackage;
    private boolean generateQuoted;

    public PackageFQN(CodePackage codePackage) {
        super();
        this.codePackage = codePackage;
    }

    public PackageFQN(CodePackage codePackage, boolean generateQuoted) {
        super();
        this.codePackage = codePackage;
        this.generateQuoted = generateQuoted;
    }

    public CodePackage getCodePackage() {
        return codePackage;
    }

    public boolean isGenerateQuoted() {
        return generateQuoted;
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		if (generateQuoted) {
//			writer.print(Utils.quotedString(codePackage.getName()));
//		} else {
//			writer.print(codePackage.getName());
//		}
//	}

    @Override
    public String toString() {
        return codePackage.getName();
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }


}
