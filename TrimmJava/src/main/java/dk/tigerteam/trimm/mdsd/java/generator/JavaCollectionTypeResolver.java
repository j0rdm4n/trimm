/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.java.generator.event.ManyToManyAssociationEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.OneToManyAssociationEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.PropertyEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.SimplePropertyEvent;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;

import java.util.*;

/**
 * Default implementation.<br/>
 * For {@link SimplePropertyEvent} and {@link OneToManyAssociationEvent} the following rules apply:
 * <ul>
 * <li>If we find an association where {@link #isOrdered(PropertyEvent)} returns true, in which case we will use {@link List} as collection
 * definition type and {@link ArrayList} as Collection instantiation type.</li>
 * <li>If we find an association where {@link #isBag(PropertyEvent)} returns true, in which case we will use {@link Collection} as
 * collection definition type and {@link ArrayList} as Collection instantiation type.</li>
 * <li>If we find an association where {@link #isNoDuplicates(PropertyEvent)} returns true, in which case we will use {@link Set} as
 * collection definition type and {@link HashSet} as Collection instantiation type.</li>
 * <li>If we find an association where {@link #isSorted(PropertyEvent)} returns true, in which case we will use {@link SortedSet} as
 * collection definition type and {@link TreeSet} as Collection instantiation type.</li>
 * </ul>
 * For {@link ManyToManyAssociationEvent} we always return {@link Set} and {@link HashSet} for defintiontype and instancetype respectively
 * <p/>
 * <p/>
 * {@link #isOrdered(PropertyEvent)} only returns true if it find a stereotype called "ordered" on the association.<br/>
 * {@link #isSorted(PropertyEvent)} only returns true if it finds a stereotype called "sorted" on the association
 * {@link #isBag(PropertyEvent)} only returns true if it finds a stereotype called "bag" on the association
 * {@link #isNoDuplicates(PropertyEvent)} returns true if it finds a stereotype called "noduplicates" on the association or if the
 * association is neither ordered, sorted nor bag styled.<br/>
 *
 * @author Jeppe Cramon
 */
public class JavaCollectionTypeResolver implements CollectionTypeResolver {
    /**
     * Stereotype name for Ordered collection style
     */
    public static final String ORDERED_STEREOTYPE_NAME = "ordered";
    /**
     * Stereotype name for NoDuplicates collection style
     */
    public static final String NO_DUPLICATES_STEREOTYPE_NAME = "noduplicates";
    /**
     * Stereotype name for Sorted collection style
     */
    public static final String SORTED_STEREOTYPE_NAME = "sorted";
    /**
     * Stereotype name for Bag collection style
     */
    public static final String BAG_STEREOTYPE_NAME = "bag";

    @SuppressWarnings("rawtypes")
    public Class<? extends Collection> resolveCollectionDefinitionType(PropertyEvent propertyEvent) {
        if (propertyEvent instanceof SimplePropertyEvent || propertyEvent instanceof OneToManyAssociationEvent
                || propertyEvent instanceof ManyToManyAssociationEvent) {
            if (isOrdered(propertyEvent)) {
                return resolveOrderedCollectionDefinitionType(propertyEvent);
            } else if (isSorted(propertyEvent)) {
                return resolveSortedCollectionDefinitionType(propertyEvent);
            } else if (isNoDuplicates(propertyEvent)) {
                return resolveNoDuplicatesCollectionDefinitionType(propertyEvent);
            } else if (isBag(propertyEvent)) {
                return resolveBagCollectionDefinitionType(propertyEvent);
            }
        }
        return resolveDefaultCollectionDefinitionType(propertyEvent);
    }

    /**
     * Override this method to provide a different Collection definition type for Bags than {@link Collection}
     *
     * @param propertyEvent The {@link PropertyEvent} we're processing
     * @return the {@link Collection} type to use
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveBagCollectionDefinitionType(PropertyEvent propertyEvent) {
        return Collection.class;
    }

    /**
     * Override this method to provide a different Collection definition type for the default collection style than {@link Set}
     *
     * @param propertyEvent The {@link PropertyEvent} we're processing
     * @return the {@link Collection} type to use
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveDefaultCollectionDefinitionType(PropertyEvent propertyEvent) {
        return Set.class;
    }

    /**
     * Override this method to provide a different Collection definition type for NoDuplicates than {@link Set}
     *
     * @param propertyEvent The {@link PropertyEvent} we're processing
     * @return the {@link Collection} type to use
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveNoDuplicatesCollectionDefinitionType(PropertyEvent propertyEvent) {
        return Set.class;
    }

    /**
     * Override this method to provide a different Collection definition type for Sorted than {@link SortedSet}
     *
     * @param propertyEvent The {@link PropertyEvent} we're processing
     * @return the {@link Collection} type to use
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveSortedCollectionDefinitionType(PropertyEvent propertyEvent) {
        return SortedSet.class;
    }

    /**
     * Override this method to provide a different Collection definition type for Ordered than {@link List}
     *
     * @param propertyEvent The {@link PropertyEvent} we're processing
     * @return the {@link Collection} type to use
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveOrderedCollectionDefinitionType(PropertyEvent propertyEvent) {
        return List.class;
    }

    protected Class<?> resolveProvidedCollectionDefinitionType(PropertyEvent propertyEvent) {
        return findProvidedDefinitionType(propertyEvent.getMetaProperty().getType().getName());
    }

    @SuppressWarnings("rawtypes")
    public Class<? extends Collection> resolveCollectionInstanceType(PropertyEvent propertyEvent) {
        if (propertyEvent instanceof SimplePropertyEvent || propertyEvent instanceof OneToManyAssociationEvent
                || propertyEvent instanceof ManyToManyAssociationEvent) {
            if (isSorted(propertyEvent)) {
                return resolveSortedCollectionInstanceType(propertyEvent);
            } else if (isNoDuplicates(propertyEvent)) {
                return resolveNoDuplicatesCollectionInstanceType(propertyEvent);
            } else if (isBag(propertyEvent)) {
                return resolveBagCollectionInstanceType(propertyEvent);
            } else if (isOrdered(propertyEvent)) {
                return resolveOrderedCollectionInstanceType(propertyEvent);
            }
        }
        return resolveDefaultCollectionInstanceType(propertyEvent);
    }

    /**
     * Override this method to provide a different Collection instance type for the default collection style than {@link HashSet}
     *
     * @param propertyEvent The {@link PropertyEvent} we're processing
     * @return the {@link Collection} type to use
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveDefaultCollectionInstanceType(PropertyEvent propertyEvent) {
        return HashSet.class;
    }

    /**
     * Override this method to provide a different Collection definition type for Ordered than {@link ArrayList}
     *
     * @param propertyEvent The {@link PropertyEvent} we're processing
     * @return the {@link Collection} type to use
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveOrderedCollectionInstanceType(PropertyEvent propertyEvent) {
        return ArrayList.class;
    }

    /**
     * Override this method to provide a different Collection definition type for Bag than {@link ArrayList}
     *
     * @param propertyEvent The {@link PropertyEvent} we're processing
     * @return the {@link Collection} type to use
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveBagCollectionInstanceType(PropertyEvent propertyEvent) {
        return ArrayList.class;
    }

    /**
     * Override this method to provide a different Collection definition type for NoDuplicates than {@link HashSet}
     *
     * @param propertyEvent The {@link PropertyEvent} we're processing
     * @return the {@link Collection} type to use
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveNoDuplicatesCollectionInstanceType(PropertyEvent propertyEvent) {
        return HashSet.class;
    }

    /**
     * Override this method to provide a different Collection definition type for Sorted than {@link TreeSet}
     *
     * @param propertyEvent The {@link PropertyEvent} we're processing
     * @return the {@link Collection} type to use
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveSortedCollectionInstanceType(PropertyEvent propertyEvent) {
        return TreeSet.class;
    }

    protected Class<?> resolveProvidedCollectionInstanceType(PropertyEvent propertyEvent) {
        return findProvidedInstanceType(propertyEvent.getMetaProperty().getType().getName());
    }

    // -------------------------------------------------------------------------------------------

    /**
     * Is the {@link MetaProperty} in the {@link PropertyEvent} defined as NoDuplicates?
     */
    public boolean isNoDuplicates(PropertyEvent propertyEvent) {
        return isNoDuplicates(propertyEvent.getMetaProperty());
    }

    /**
     * Is the {@link MetaProperty} in the {@link PropertyEvent} defined as Ordered?
     */
    public boolean isOrdered(PropertyEvent propertyEvent) {
        return isOrdered(propertyEvent.getMetaProperty());
    }

    /**
     * Is the {@link MetaProperty} in the {@link PropertyEvent} defined as Sorted?
     */
    public boolean isSorted(PropertyEvent propertyEvent) {
        return isSorted(propertyEvent.getMetaProperty());
    }

    /**
     * Is the {@link MetaProperty} in the {@link PropertyEvent} defined as Bag?
     */
    public boolean isBag(PropertyEvent propertyEvent) {
        return isBag(propertyEvent.getMetaProperty());
    }

    // -------------------------------------------------------------------------------------------

    /**
     * Is the {@link MetaProperty} in the {@link PropertyEvent} defined as NoDuplicates?
     */
    public boolean isNoDuplicates(MetaProperty metaProperty) {
        return metaProperty.isPartInAnAssociation() && (metaProperty.getAssociation().hasStereoType(NO_DUPLICATES_STEREOTYPE_NAME))
                || metaProperty.hasStereoType(NO_DUPLICATES_STEREOTYPE_NAME);
    }

    /**
     * Is the {@link MetaProperty} in the {@link PropertyEvent} defined as Ordered?
     */
    public boolean isOrdered(MetaProperty metaProperty) {
        return metaProperty.isPartInAnAssociation() && (metaProperty.getAssociation().hasStereoType(ORDERED_STEREOTYPE_NAME))
                || metaProperty.isOrdered() || metaProperty.hasStereoType(ORDERED_STEREOTYPE_NAME);
    }

    /**
     * Is the {@link MetaProperty} in the {@link PropertyEvent} defined as Sorted?
     */
    public boolean isSorted(MetaProperty metaProperty) {
        return metaProperty.isPartInAnAssociation() && (metaProperty.getAssociation().hasStereoType(SORTED_STEREOTYPE_NAME))
                || metaProperty.hasStereoType(SORTED_STEREOTYPE_NAME);
    }

    /**
     * Is the {@link MetaProperty} in the {@link PropertyEvent} defined as Bag?
     */
    public boolean isBag(MetaProperty metaProperty) {
        return metaProperty.isPartInAnAssociation() && (metaProperty.getAssociation().hasStereoType(BAG_STEREOTYPE_NAME))
                || metaProperty.hasStereoType(BAG_STEREOTYPE_NAME);
    }

    protected Class<?> findProvidedDefinitionType(String typeName) {
        if (typeName.equals(Map.class.getSimpleName())) {
            return Map.class;
        } else if (typeName.equals(Set.class.getSimpleName())) {
            return Set.class;
        } else if (typeName.equals(List.class.getSimpleName())) {
            return List.class;
        } else if (typeName.equals(EnumMap.class.getSimpleName())) {
            return EnumMap.class;
        } else if (typeName.equals(EnumSet.class.getSimpleName())) {
            return EnumSet.class;
        } else if (typeName.equals(Collection.class.getSimpleName())) {
            return Collection.class;
        }
        return Collection.class;
    }

    protected Class<?> findProvidedInstanceType(String typeName) {
        if (typeName.equals(Map.class.getSimpleName())) {
            return HashMap.class;
        } else if (typeName.equals(Set.class.getSimpleName())) {
            return HashSet.class;
        } else if (typeName.equals(List.class.getSimpleName())) {
            return ArrayList.class;
        } else if (typeName.equals(EnumMap.class.getSimpleName())) {
            return EnumMap.class;
        } else if (typeName.equals(EnumSet.class.getSimpleName())) {
            return EnumSet.class;
        } else if (typeName.equals(Collection.class.getSimpleName())) {
            return ArrayList.class;
        }
        return ArrayList.class;
    }

}