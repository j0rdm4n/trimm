/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeSnippet;
import dk.tigerteam.trimm.mdsd.java.codedom.Method;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.util.Utils;

/**
 * Creates a nice sugar method for 1-1 and Many-1 associations, which allows fetching the association, and in case it doesn't exist creates a new association and assigns it.
 * <p/>
 * For a class Person with a (1-1) association to an Address class through the property "homeAddress" it will create a <code>person.getOrNewHomeAddress()</code> property sugar method.
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class GetOrNewListener extends AbstractGeneratorEventListener implements
        DeferredGeneratorEventListener {

    @Override
    protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
        return generateGetOrNewPropertyMethod(event);
    }

    @Override
    protected boolean handleManyToOneAssociationEvent(
            ManyToOneAssociationEvent event) {
        return generateGetOrNewPropertyMethod(event);
    }

    private boolean generateGetOrNewPropertyMethod(AssociationEvent event) {
        if (event.getClazzOrInterface().isInterface()
                || (event.getProperty().getField().getType().getClazz() != null && event.getProperty().getField().getType().getClazz().getImmuteableProperties().size() > 0)
                || event.getProperty().getField().getType().getClazzOrInterface().isEnumeration()) {
            return true;
        }

        Type type = event.getProperty().getField().getType();
        String fieldName = event.getProperty().getField().getName();
        String methodName = "getOrNew" + Utils.capitalize(fieldName);
        Clazz clazz = event.getClazz();
        Method method = new Method(methodName, clazz, true, AccessModifier.Public);
        method.setReturnType(type);
        CodeSnippet snippet = new CodeSnippet();
        snippet.println("if(" + fieldName + " == null) {");
        snippet.printlnAll("  " + fieldName + " = new ", type.getClazzOrInterface().asFQCN(), "();");
        snippet.println("}");
        snippet.println("return " + fieldName + ";");
        method.addCode(snippet);

        clazz.addChildren(method);
        return true;
    }

}
