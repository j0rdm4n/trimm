/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * A ClazzEvent corresponds to a {@link Clazz}
 *
 * @author Jeppe Cramon
 */
public class ClazzEvent extends GeneratorEvent {

    protected static final Log logger = LogFactory.getLog(ClazzEvent.class);

    private Clazz clazz;
    private MetaClazz metaClazz;

    public ClazzEvent(Clazz clazz) {
        super();
        this.clazz = clazz;
    }

    public Clazz getClazz() {
        return clazz;
    }

    public MetaClazz getMetaClazz() {
        return metaClazz;
    }

    public Clazz execute() {
        logger.debug("Generating for Clazz '" + clazz + "'");
        metaClazz = clazz.getMetaType();
        clazz.addAccessModifiers(AccessModifier.Public);
        if (metaClazz.isAbstract()) {
            clazz.addAccessModifiers(AccessModifier.Abstract);
        }
        if (metaClazz.isTemplateParameterized()) {
            for (MetaClazz templateParameterClazz : metaClazz.getTemplateParameters()) {
                clazz.addGenerics(new GenericType(templateParameterClazz));
            }
            // Remove <...>
            int index = clazz.getName().indexOf('<');
            if (index != -1) {
                clazz.setName(clazz.getName().substring(0, index));
            }
        }
        if (JavaGeneratorContext.getContext().isAddDefaultConstructor() && !getClazz().hasDefaultConstructor()) {
            Constructor constructor = null;
            if (metaClazz.isEnumerationClazz()) {
                constructor = new Constructor(getClazz().getName(), true);
            } else {
                constructor = new Constructor(getClazz().getName(), true, AccessModifier.Protected);
            }
            getClazz().add(constructor);
        }

        // Handle supper clazz
        if (getClazz().getExtendsType() == null) {
            JavaGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {
                /**
                 * Execute the deferred event handling code
                 */
                public void execute() {
                    // If the clazz still doesn't inherit from another clazz (i.e. another Listener hasn't changed the hierarchy) then we can apply a
                    // super clazz
                    if (getClazz().getExtendsType() == null) {
                        Type mappedSuperClazzType = JavaGeneratorContext.getContext().getInheritanceStrategy().resolveSuperClazzType(clazz);
                        if (mappedSuperClazzType != null) {
                            clazz.setExtendsType(mappedSuperClazzType);
                        }
                    }
                }

                public String getDescription() {
                    return "Resolving possible Super clazz for " + getMetaClazz();
                }
            });
        }

        if (metaClazz.isEnumerationClazz()) {
            JavaGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {

                /**
                 * Execute the deferred event handling code
                 */
                public void execute() {
                    // Write a constructor if there are any properties defined (a little too late, could be a deferred listener)
                    List<Property> properties = getClazz().getProperties();
                    if (properties.size() > 0) {
                        Constructor constructor = new Constructor(getClazz().getName(), true, AccessModifier.Private);
                        for (Property property : properties) {
                            if (property.getField() != null) {
                                constructor.addParameters(new Parameter(property.getName(), property.getField().getType()));
                            } else {
                                constructor.addParameters(new Parameter(property.getName(), property.getType()));
                            }
                            constructor.addCode(new CodeSnippet("this.", property.getName(), " = ", property.getName(), ";"));
                        }
                        getClazz().add(constructor);
                    }
                }

                /**
                 * Description of what the deferred event handler instance is doing (for better error reporting)
                 */
                public String getDescription() {
                    return "Possible EnumLiteral constructor for " + getMetaClazz();
                }
            });
        }

        // TODO: Handle inner classes
        broadcastEvent();
        return clazz;
    }
}
