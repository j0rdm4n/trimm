/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaAssociation;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;

/**
 * Common base class for all {@link PropertyEvent}' that relate to a {@link MetaProperty} which is part in an association
 * ({@link MetaProperty#isPartInAnAssociation()}).<p/>
 * Carries information about the {@link MetaProperty} that we're currently processing
 * ({@link #getMetaProperty()}) and the {@link MetaProperty} on the opposite side of the {@link MetaAssociation} (as sugar info, can also
 * be read using {@link #getMetaProperty()#getOppositeMetaProperty()})
 *
 * @author Jeppe Cramon
 */
public abstract class AssociationEvent extends PropertyEvent {

    private MetaProperty oppositeMetaProperty;

    public AssociationEvent() {
    }

    public AssociationEvent(ClazzOrInterface clazz, MetaProperty metaProperty, MetaProperty oppositeProperty) {
        super(clazz, metaProperty);
        this.oppositeMetaProperty = oppositeProperty;
    }

    /**
     * The {@link MetaProperty} on the opposite side of the {@link MetaAssociation} (can also be fetched by calling
     * {@link #getMetaProperty()#getOppositeMetaProperty()}
     */
    public MetaProperty getOppositeMetaProperty() {
        return oppositeMetaProperty;
    }

    /**
     * Resolve the opposite {@link MetaProperty}' name.If it's null, then we'll
     * use the type as name
     */
    public String getResolvedOppositeMetaPropertyName() {
        return resolveJavaFieldNameFromMetaProperty(oppositeMetaProperty);
    }

    /**
     * Returns the Clazz or Interface that owns this Association. If the owner is a Clazz and is a Base class
     * then we will lookup the Extension class for it using the Generator context.
     */
    protected ClazzOrInterface getClazzOrInterfaceAsExtensionClazzIfNecessary() {
        ClazzOrInterface result = getClazzOrInterface();
        if (result.isClazz() && ((Clazz) result).isBaseClazz()) {
            result = JavaGeneratorContext.getContext().getExtensionClazzForBaseClazz((Clazz) result);
        }
        return result;
    }

}
