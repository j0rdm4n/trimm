/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JavaDomainLogicCreator {

    protected static final Log log = LogFactory.getLog(JavaDomainLogicCreator.class);

    private String destinationPackageName;
    private String domainLogicFactoryClassName = "DomainLogicFactory";
    private Class<?> superLogicInterface;
    private Map<Clazz, Interface> clazzToLogicInterfaceMap = new HashMap<Clazz, Interface>();

    public JavaDomainLogicCreator() {
        super();
    }

    public JavaDomainLogicCreator(String destinationPackageName) {
        super();
        this.destinationPackageName = destinationPackageName;
    }

    public String getDestinationPackageName() {
        return destinationPackageName;
    }

    public void setDestinationPackageName(String destinationPackageName) {
        this.destinationPackageName = destinationPackageName;
    }

    public String getDomainLogicFactoryClassName() {
        return domainLogicFactoryClassName;
    }

    public void setDomainLogicFactoryClassName(String domainLogicFactoryClassName) {
        this.domainLogicFactoryClassName = domainLogicFactoryClassName;
    }

    public Class<?> getSuperLogicInterface() {
        return superLogicInterface;
    }

    public void setSuperLogicInterface(Class<?> superLogicInterface) {
        this.superLogicInterface = superLogicInterface;
    }

    public List<ClazzOrInterface> createLogicInterfaces(List<ClazzOrInterface> createdClazzez) {

        CodeModel codeModel = JavaGeneratorContext.getContext().getCodeModel();
        CodePackage codePackage = new CodePackage(destinationPackageName, true);
        codeModel.addChildren(codePackage);
        List<ClazzOrInterface> interfaces = new ArrayList<ClazzOrInterface>();
        for (ClazzOrInterface clazzOrInterface : createdClazzez) {
            if (!clazzOrInterface.isInterface()) {
                Clazz clazz = (Clazz) clazzOrInterface;
                if (!(clazz instanceof Enumeration) && !clazz.isFabricated()) {
                    boolean areWeRootClazz = codeModel.getRootSuperClazz(clazz) == clazz;

                    Interface domaininterface = clazzToLogicInterfaceMap.get(clazz);
                    if (domaininterface == null) {
                        domaininterface = new Interface(clazz.getName() + "Logic", true, AccessModifier.Public);
                        codePackage.addChildren(domaininterface);
                        clazzToLogicInterfaceMap.put(clazz, domaininterface);
                    }
                    log.debug("Generating Logic interface '" + domaininterface.asFQCN() + "' for class '" + clazz.getFQCN() + "'");
                    if (areWeRootClazz) {
                        if (superLogicInterface != null) {
                            domaininterface.addExtendsTypes(new Type(superLogicInterface));
                        }
                    } else {
                        Clazz superClazz = clazz.getExtendsType().getClazz();
                        if (superClazz == null) {
                            throw new IllegalStateException("Expected a super CLAZZ for Clazz " + clazz + ". Instead found super type " + clazz.getExtendsType());
                        }
                        Interface superClazzLogicInterface = clazzToLogicInterfaceMap.get(superClazz);
                        if (superClazzLogicInterface == null) {
                            superClazzLogicInterface = new Interface(superClazz.getName() + "Logic", true, AccessModifier.Public);
                            codePackage.addChildren(superClazzLogicInterface);
                            clazzToLogicInterfaceMap.put(superClazz, superClazzLogicInterface);
                        }
                        domaininterface.addExtendsTypes(new Type(superClazzLogicInterface));
                    }

                    Method getter = new Method("get", domaininterface, true, AccessModifier.Public);
                    getter.setReturnType(new Type(clazz));
                    Method setter = new Method("set", domaininterface, true, AccessModifier.Public);
                    setter.addParameters(new Parameter("entity").setType(new Type(clazz)));

                    // Add getLogic method to clazz
                    Method getLogic = new Method("getLogic", clazz, true, AccessModifier.Public);
                    getLogic.setReturnType(new Type(domaininterface));
                    getLogic.addChildren(new CodeSnippet("return null;"));

                    if (!areWeRootClazz) {
                        getLogic.addAnnotations(new Annotation(Override.class));
                    }
                    interfaces.add(domaininterface);
                }
            }
        }
        createdClazzez.addAll(interfaces);
        return createdClazzez;
    }

}
