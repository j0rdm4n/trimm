/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.util.Utils;

/**
 * Since JavaBean properties are merely accessor methods (getter and setter) for a field, the {@link Property} abstraction contains the
 * field that it's property for and the two accessor methods.<br/>
 * . If fact both {@link Field} and the {@link #getGetterMethod()} and {@link #getSetterMethod()} methods (and their corresponsing setter
 * methods) are in fact just sugar methods, which search the {@link Property}' {@link #getChildren()} for {@link Field},
 * {@link GetterMethod} and {@link SetterMethod}.<br/>
 * This means that you can create more than two methods for each {@link Property}, however you cannot have more than one {@link Field},
 * {@link GetterMethod} and {@link SetterMethod} (in coherence with the JavaBeans specification).
 *
 * @author Jeppe Cramon
 */
public class Property extends CodeComposite {
    // Type and initializerStatement are used for languages that define Property as a built in concept, like Groovy
    private Type type;
    // Type and initializerStatement are used for languages that define Property as a built in concept, like Groovy
    private CodeSnippet initializerStatement;

    /**
     * New Property for Languages without native support for Properties (i.e. requires an underlying field)
     *
     * @param owner owner
     * @param field            The underlying Field (used for languages, like Java, that doesn't support properties natively)
     * @param accessModifiers  Its access modifiers
     */
    public Property(ClazzOrInterface owner, Field field, AccessModifier... accessModifiers) {
        super();
        owner.addChildren(this);
        setField(field);
        addAccessModifiers(accessModifiers);
    }

    /**
     * New Property for Languages with native support for Properties (i.e. doesn't require an underlying field)
     *
     * @param owner owner
     * @param type             The type for the property
     * @param accessModifiers  Its access modifiers
     */
    public Property(ClazzOrInterface owner, Type type, AccessModifier... accessModifiers) {
        super();
        owner.addChildren(this);
        setType(type);
        addAccessModifiers(accessModifiers);
    }

    /**
     * Used to define a property with dynamic (or no type) in languages like Groovy
     *
     * @param owner owner
     * @param accessModifiers  Its access modifiers
     */
    public Property(ClazzOrInterface owner, AccessModifier... accessModifiers) {
        super();
        owner.addChildren(this);
        addAccessModifiers(accessModifiers);
    }

    /**
     * Used for Dynamically types languages, like Groovy, where fields, variables, properties, parameters, etc. doesn't need types
     *
     * @return true if no {@link #getType()} has been specified
     */
    public boolean isDynamicallyTyped() {
        return type == null;
    }


    /**
     * The type for this property - Type and initializerStatement are used for languages that define Property as a built in concept, like Groovy
     * <p/>Note: Other languages, like Java, must access the underlying Field to know the Type and initializer statement
     *
     * @return
     */
    public Type getType() {
        return type;
    }

    /**
     * The type for this property - Type and initializerStatement are used for languages that define Property as a built in concept, like Groovy
     * <p/>Note: Other languages, like Java, must access the underlying Field to know the Type and initializer statement
     *
     * @param type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * The initializer statement for this property - Type and initializerStatement are used for languages that define Property as a built in concept, like Groovy
     * <p/>Note: Other languages, like Java, must access the underlying Field to know the Type and initializer statement
     *
     * @return
     */
    public CodeSnippet getInitializerStatement() {
        return initializerStatement;
    }

    /**
     * The initializer statement for this property - Type and initializerStatement are used for languages that define Property as a built in concept, like Groovy
     * <p/>Note: Other languages, like Java, must access the underlying Field to know the Type and initializer statement
     *
     * @param initializerStatement
     */
    public void setInitializerStatement(CodeSnippet initializerStatement) {
        this.initializerStatement = initializerStatement;
    }

    @Override
    public MetaProperty getMetaType() {
        return (MetaProperty) super.getMetaType();
    }


    @Override
    public ClazzOrInterface getOwner() {
        return (ClazzOrInterface) super.getOwner();
    }

    /**
     * Set the {@link Field} that defines this {@link Property}
     *
     * @param field The field that defines this {@link Property}
     * @return This property instance
     */
    public Property setField(Field field) {
        Field existingField = getField();
        if (existingField != field) {
            getChildren().remove(existingField);
        }
        if (field != null) {
            addChildren(field);
            setName(field.getName());
        } else {
            setName(null);
        }
        return this;
    }

    /**
     * Get the {@link Field} that defines this {@link Property}
     */
    public Field getField() {
        return findChildOfType(Field.class);
    }

    /**
     * If the Getter property' name is left out, it will automatically be set, when it's added to a Property object (you <u>can</u> set the
     * name, but it's not necessary).
     * <p/>
     * If not specified, then the Return type will be set to the type of the Property it's added to.
     *
     * @param getter The getter method to set
     * @return This property instance
     */
    public Property setGetterMethod(GetterMethod getter) {
        GetterMethod existingGetterMethod = getGetterMethod();
        if (existingGetterMethod != null) {
            getChildren().remove(existingGetterMethod);
        }
        if (getter != null) {
            if (getter.getAccessModifiers().isEmpty()) {
                getter.addAccessModifiers(AccessModifier.Public);
            }
            if (getter.getReturnType() == null || getter.getReturnType() == Type.VOID) {
                getter.setReturnType(getField().getType());
            }

            if (getter.getName() == null) {
                if (getter.getReturnType().isBooleanType() && !getter.getReturnType().isBooleanWrapperType()) {
                    getter.setName("is" + Utils.capitalize(getName()));
                } else {
                    getter.setName("get" + Utils.capitalize(getName()));
                }
            }

            addChildren(getter);

            // Handle Boolean, so it also has a Get method (temporary hack)
            if (getter.getReturnType().isBooleanWrapperType() && JavaGeneratorContext.getContext().generateIsPropertyMethodForBooleanWrapperTypeProperties()) {
                Method booleanWrapperTypeGetterMethod = new Method();
                booleanWrapperTypeGetterMethod.addAccessModifiers(AccessModifier.Public);
                booleanWrapperTypeGetterMethod.setReturnType(getField().getType());
                booleanWrapperTypeGetterMethod.setName("is" + Utils.capitalize(getName()));
                booleanWrapperTypeGetterMethod.setCodeDocumentation(getField().getCodeDocumentation());
                booleanWrapperTypeGetterMethod.addChildren(new CodeSnippet("return " + getField().getName() + ";"));
                addChildren(booleanWrapperTypeGetterMethod);
            }
        }
        return this;
    }

    /**
     * Get the getter method
     */
    public GetterMethod getGetterMethod() {
        return findChildOfType(GetterMethod.class);
    }

    /**
     * The SetterMethod' name is automatically set, when it's added to a {@link Property} object (you <u>can</u> set the name, but it's not
     * necessary)
     * <p/>
     * If no parameter is provided in the Setter method, then the standard {@link Property} implementation will provide a default argument of
     * the same type as the property field and with the value of {@link SetterMethod#DEFAULT_PARAMETER_NAME}<br/>
     *
     * @param setter The setter method to set
     * @return This property instance
     */
    public Property setSetterMethod(SetterMethod setter) {
        SetterMethod existingSetterMethod = getSetterMethod();
        if (existingSetterMethod != null) {
            getChildren().remove(existingSetterMethod);
        }
        if (setter != null) {
            if (setter.getAccessModifiers().isEmpty()) {
                setter.addAccessModifiers(AccessModifier.Public);
            }
            if (setter.getName() == null) {
                setter.setName("set" + Utils.capitalize(getName()));
            }
            if (setter.getParameters().isEmpty()) {
                // Supply default parameter
                setter.addParameters(new Parameter(SetterMethod.DEFAULT_PARAMETER_NAME).setType(getField().getType()));
            }
            addChildren(setter);
        }
        return this;
    }

    public Property removeSetterMethod() {
        return setSetterMethod(null);
    }

    public Property removeGetterMethod() {
        return setGetterMethod(null);
    }

    /**
     * Get the setter method
     */
    public SetterMethod getSetterMethod() {
        return findChildOfType(SetterMethod.class);
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		for (CodeElement child : getChildrenAsXmiElements()) {
//			child.generateCode(writer);
//		}
//	}

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
