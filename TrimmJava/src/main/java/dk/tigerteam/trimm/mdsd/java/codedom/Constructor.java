/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

/**
 * Models a Java constructor (ie. a method with the same name as the {@link Clazz} it's added to) and {@link Type#NONE} as return type
 *
 * @author Jeppe Cramon
 */
public class Constructor extends Method {

    public Constructor(AccessModifier... accessModifiers) {
        super(accessModifiers);
        setFabricated(true);
    }

    public Constructor(String name, AccessModifier... accessModifiers) {
        super(name, true, accessModifiers);
    }

    public Constructor(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

    @Override
    public Method setReturnType(Type returnType) {
        throw new IllegalArgumentException("You can't set a return type for a Constructor");
    }

    @Override
    public Type getReturnType() {
        return Type.NONE;
    }

    /**
     * When the owner is set, we will look for the {@link Clazz} that we're defined in. If we find such as {@link Clazz} we will use its name.
     * This process is repeated again in {@link #generateCode(CodeWriter)}, just to be certain that we don't have a constructor with a wrong
     * or no name)
     */
    @Override
    public CodeElement setOwner(CodeComposite owner) {
        CodeElement result = super.setOwner(owner);
        Clazz definedInClazz = this.findOwnerOfType(Clazz.class);
        if (definedInClazz != null && !hasAccessModifier(AccessModifier.Static)) {
            // Use the Clazz' name as our constructor name
            setName(definedInClazz.getName());
        }
        return result;
    }
//
//	@Override
//	public void generateCode(CodeWriter writer) {
//		Clazz definedInClazz = this.findOwnerOfType(Clazz.class);
//		if (definedInClazz != null && !hasAccessModifier(AccessModifier.Static)) {
//			// Use the Clazz' name as our constructor name
//			setName(definedInClazz.getName());
//		}
//		super.generateCode(writer);
//	}

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }
}
