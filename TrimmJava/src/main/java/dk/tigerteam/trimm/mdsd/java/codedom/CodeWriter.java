/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.meta.MetaType;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Locale;

/**
 * Used when generating code in {@link CodeElement#generateCode(CodeWriter)}.<br/>
 * Has a part from various append, print and println method also specialized methods
 * that can printout specific {@link CodeElement} properties, like {@link AccessModifier}'.
 *
 * @author Jeppe Cramon
 */
public class CodeWriter {

    private PrintWriter printWriter;

    public CodeWriter(PrintWriter printWriter) {
        super();
        this.printWriter = printWriter;
    }

    /**
     * Initialize using a Writer (which internally will be wrapped in a PrintWriter)
     * @param writer
     */
    public CodeWriter(Writer writer) {
        this(new PrintWriter(writer));
    }

    public CodeWriter append(char c) {
        printWriter.append(c);
        return this;
    }

    public CodeWriter append(CharSequence csq, int start, int end) {
        printWriter.append(csq, start, end);
        return this;
    }

    public CodeWriter append(CharSequence csq) {
        printWriter.append(csq);
        return this;
    }

    public CodeWriter format(Locale l, String format, Object... args) {
        printWriter.format(l, format, args);
        return this;
    }

    public CodeWriter format(String format, Object... args) {
        printWriter.format(format, args);
        return this;
    }

    public CodeWriter print(boolean b) {
        printWriter.print(b);
        return this;
    }

    public CodeWriter print(char c) {
        printWriter.print(c);
        return this;
    }

    public CodeWriter print(char[] s) {
        printWriter.print(s);
        return this;
    }

    public CodeWriter print(double d) {
        printWriter.print(d);
        return this;
    }

    public CodeWriter print(float f) {
        printWriter.print(f);
        return this;
    }

    public CodeWriter print(int i) {
        printWriter.print(i);
        return this;
    }

    public CodeWriter print(long l) {
        printWriter.print(l);
        return this;
    }

    public CodeWriter print(Object obj) {
        printWriter.print(obj);
        return this;
    }

    public CodeWriter print(Class<?> clazz) {
        printWriter.print(clazz.getName());
        return this;
    }

    public CodeWriter print(String s) {
        printWriter.print(s);
        return this;
    }

    public CodeWriter printf(Locale l, String format, Object... args) {
        printWriter.printf(l, format, args);
        return this;
    }

    public CodeWriter printf(String format, Object... args) {
        printWriter.printf(format, args);
        return this;
    }

    public CodeWriter println() {
        printWriter.println();
        return this;
    }

    public CodeWriter println(boolean x) {
        printWriter.println(x);
        return this;
    }

    public CodeWriter println(char x) {
        printWriter.println(x);
        return this;
    }

    public CodeWriter println(char[] x) {
        printWriter.println(x);
        return this;
    }

    public CodeWriter println(double x) {
        printWriter.println(x);
        return this;
    }

    public CodeWriter println(float x) {
        printWriter.println(x);
        return this;
    }

    public CodeWriter println(int x) {
        printWriter.println(x);
        return this;
    }

    public CodeWriter println(long x) {
        printWriter.println(x);
        return this;
    }

    public CodeWriter println(Object x) {
        printWriter.println(x);
        return this;
    }

    public CodeWriter println(Class<?> clazz) {
        printWriter.println(clazz.getName());
        return this;
    }

    public CodeWriter println(String x) {
        printWriter.println(x);
        return this;
    }

    /**
     * Print out all the elements {@link AccessModifier}' in order they were specified
     *
     * @param element The element to write out the {@link CodeElement#getAccessModifiers()} for
     * @return This {@link CodeWriter} instance
     */
    @Deprecated
    public CodeWriter printAccessModifiers(CodeElement element) {
        for (AccessModifier modifier : element.getAccessModifiers()) {
            print(modifier.name().toLowerCase()).print(" ");
        }
        return this;
    }

    /**
     * Write out the elements comma separated
     * @param elements
     * @return
     */
//	@Deprecated
//	public CodeWriter printCodeElementCollection(Collection<? extends CodeElement> elements) {
//		int count = 0;
//		for (CodeElement element : elements) {
//			if (count > 0) {
//				print(", ");
//			}
//			element.generateCode(this);
//			count++;
//		}
//		return this;
//	}


    /**
     * Print out all the elements {@link Annotation}'
     * @param element The element to write out the {@link CodeElement#getAnnotations()} for
     * @return This {@link CodeWriter} instance
     */
//	@Deprecated
//	public CodeWriter printAnnotations(CodeElement element) {
//		for (Annotation annotation : element.getAnnotations()) {
//			annotation.generateCode(this);
//			println();
//		}
//		return this;
//	}

    /**
     * Print out the elements java doc using standard JavaDoc style comments
     *
     * @param element The element to write java doc for
     * @return This {@link CodeWriter} instance
     */
    @Deprecated
    public CodeWriter printJavaDoc(CodeElement element) {
        if (element.getCodeDocumentation() == null) {
            return this;
        }
        println("/**");
        for (String docLine : element.getCodeDocumentation().split(MetaType.newline)) {
            print(" * ");
            println(docLine);
        }
        println(" */");
        return this;
    }
}
