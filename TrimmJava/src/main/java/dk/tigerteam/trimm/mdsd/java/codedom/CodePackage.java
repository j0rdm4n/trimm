/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.meta.MetaPackage;

/**
 * Models a Java package
 * <p/>
 * <b>Note: Unlike for the {@link MetaPackage}'s, {@link CodePackage}'s are flat and not nested inside {@link CodeModel#getChildren()}.</b><br/>
 * So two packages dk.tigerteam.model.legalparty and dk.tigerteam.model.legalparty.address can be found side by side in the
 * {@link CodeModel#getChildren()} (if we use {@link MetaPackage}'s then the package "address", would be found as a child of "legalparty",
 * which is a child of "model", etc.
 *
 * @author Jeppe Cramon
 */
public class CodePackage extends CodeComposite {
    public CodePackage(AccessModifier... accessModifiers) {
        super(accessModifiers);
    }

    public CodePackage(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
    }

    public CodePackage(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		// TODO: Consider allow to write out Package java doc to package.html...?
//		throw new IllegalArgumentException("Don't call us");
//	}

    /**
     * Get the Fully Qualified Name for the Package, so when he code is generated, we allow for writing the package rewritten name
     */
    public PackageFQN asFQN() {
        return new PackageFQN(this);
    }

    /**
     * Get the Fully Qualified Name for the Package, so when he code is generated, we allow for writing the package rewritten name
     */
    public PackageFQN asFQN(boolean quoted) {
        return new PackageFQN(this, quoted);
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
