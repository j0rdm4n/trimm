/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.java.generator.event.GeneratorEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.PropertyEvent;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;

import java.util.*;

/**
 * Customization interface which allows the user to specify which Collection definition and instantiation types to use for a given
 * Collection (Simple Property of type Collection (Many), One-To-Many and Many-to-Many typically) association in the model.
 * <p/>
 * <b>It's a requirement that either of the {@link #isBag(PropertyEvent)}, {@link #isNoDuplicates(PropertyEvent)},
 * {@link #isOrdered(PropertyEvent)} or {@link #isSorted(PropertyEvent)} return true for any given {@link GeneratorEvent} combination.</b><br/>
 * The recommended default is to let the {@link #isNoDuplicates(PropertyEvent)} method return true for all unspecified cases, as this
 * typically will result in {@link Set} and {@link HashSet} for definition-type and instance-types respectively, which is safe to use in all
 * cases with JPA/Hibernate.
 *
 * @author Jeppe Cramon
 */
public interface CollectionTypeResolver {
    /**
     * Resolve what Collection type to use for Collection field definitions.<br/>
     * Example if {@link Collection} is returned we will get this: private <b>Collection</b>&lt;Person> persons;</code>
     *
     * @param propertyEvent the property event
     * @return The Collection type to use for Collection field definitions
     */
    @SuppressWarnings("rawtypes")
    public Class<? extends Collection> resolveCollectionDefinitionType(
            PropertyEvent propertyEvent);

    /**
     * Resolve what Collection type to use for Collection field instances.<br/>
     * Example if {@link ArrayList} is returned we will get this: private Collection&lt;Person> persons = new <b>ArrayList</b>&lt;Person>();
     *
     * @param propertyEvent the property event
     * @return The Collection type to use for Collection field instances.
     */
    @SuppressWarnings("rawtypes")
    public Class<? extends Collection> resolveCollectionInstanceType(
            PropertyEvent propertyEvent);

    /**
     * Returns true if the association (typically One-To-Many) is ordered. <br/>
     * This will typically mean that {@link List} and {@link ArrayList} will be used for for definition-type and instance-type respectively
     *
     * @param propertyEvent the property event
     */
    public boolean isOrdered(PropertyEvent propertyEvent);

    /**
     * Returns true if the association (typically One-To-Many) is sorted.<br/>
     * This will typically mean that {@link SortedSet} and {@link TreeSet} will be used for for definition-type and instance-type respectively
     *
     * @param propertyEvent the property event
     */
    public boolean isSorted(PropertyEvent propertyEvent);

    /**
     * Returns true if the Collection musn't contain duplicates.<br/>
     * This will typically mean that {@link Set} and {@link HashSet} will be used for for definition-type and instance-type respectively
     *
     * @param propertyEvent the property event
     */
    public boolean isNoDuplicates(PropertyEvent propertyEvent);

    /**
     * Returns true if the Collection allows duplicates and is unordered<br/>
     * This will typically mean that {@link Collection} and {@link ArrayList} will be used for for definition-type and instance-type respectively
     *
     * @param propertyEvent the property event
     */
    public boolean isBag(PropertyEvent propertyEvent);

    /**
     * Returns true if the association (typically One-To-Many) is ordered. <br/>
     * This will typically mean that {@link List} and {@link ArrayList} will be used for for definition-type and instance-type respectively
     *
     * @param metaProperty the meta property
     */
    public boolean isOrdered(MetaProperty metaProperty);

    /**
     * Returns true if the association (typically One-To-Many) is sorted.<br/>
     * This will typically mean that {@link SortedSet} and {@link TreeSet} will be used for for definition-type and instance-type respectively
     *
     * @param metaProperty the meta property
     */
    public boolean isSorted(MetaProperty metaProperty);

    /**
     * Returns true if the Collection musn't contain duplicates.<br/>
     * This will typically mean that {@link Set} and {@link HashSet} will be used for for definition-type and instance-type respectively
     *
     * @param metaProperty the meta property
     */
    public boolean isNoDuplicates(MetaProperty metaProperty);

    /**
     * Returns true if the Collection allows duplicates and is unordered<br/>
     * This will typically mean that {@link Collection} and {@link ArrayList} will be used for for definition-type and instance-type respectively
     *
     * @param metaProperty the meta property
     */
    public boolean isBag(MetaProperty metaProperty);

}
