/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.codewriter;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaCodeGenerationPaths;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaConfiguration;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.GeneratorSummary.UpdateType;
import dk.tigerteam.trimm.util.Utils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * The {@link JavaCodeWriter} is responsible for the processes of generation source code files ({@link File}) from {@link Clazz}' - typically in combination with {@link JavaCodeWritingVisitor}.<p/>
 * The {@link JavaCodeWriter} is also responsible for transforming packages to folders and placing Clazz' source code files in the right
 * package folder (according to the Java rules).
 * <p/>
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 * @see JavaCodeWritingVisitor
 */
public class JavaCodeWriter extends AbstractCodeWriter {

    private static final Log logger = LogFactory.getLog(JavaCodeWriter.class);
    public static final String JAVA_FILE_EXTENSION = ".java";

    public JavaCodeWriter(JavaCodeGenerationPaths javaCodeGenerationPaths, PackageRewriter packageRewriter, String rootDirectory, String... excludeFormattingForPackages) {
        this(javaCodeGenerationPaths, packageRewriter, rootDirectory, Arrays.asList(excludeFormattingForPackages));
    }

    public JavaCodeWriter(JavaCodeGenerationPaths javaCodeGenerationPaths, PackageRewriter packageRewriter, String rootDirectory,
                          Collection<String> excludeFormattingForPackages) {
        super(javaCodeGenerationPaths, packageRewriter, rootDirectory, excludeFormattingForPackages);
        setFileExtension(JAVA_FILE_EXTENSION);
    }

    /**
     * Generate code for the {@link Clazz}' listed in the clazzesToWrite paramter.
     *
     * @param clazzesToWrite All the {@link Clazz}' to generate code for
     * @return All the generated sourcecode files
     * @throws IOException In case something I/O goes wrong during the process
     */
    @Override
    public List<File> writeCode(Collection<ClazzOrInterface> clazzesToWrite) throws IOException {
        logger.debug("Writing '" + clazzesToWrite.size() + "' artifacts with code writer '" +
                this.getClass().getSimpleName() + "' using file extension '" + getFileExtension() + "'");
        getSummary().setNumberOfGeneratedArtifacts(clazzesToWrite.size());
        // Ensure that all packages have been rewritten prior to writing out ANY code!
        rewritePackageNames(clazzesToWrite);

        List<File> generatedFiles = new LinkedList<File>();
        for (ClazzOrInterface clazz : clazzesToWrite) {
            if (!shouldGenerateSourceFileForClazz(clazz)) {
                // Don't generate
                logger.trace("DontGenerate stereotype found for clazz '" + clazz + "'");
                getSummary().addToNumberOfDontGenerateClasses();
                continue;
            }
            String fqcn = clazz.getFQCN();
            String fqcnPath = generateFqcnPath(fqcn, getFileExtension());

            String destinationFolder = resolveDestionationFolder(clazz);

            File destinationFolderFile = createDestinationFolder(destinationFolder);

            String fullPath = createPackageFolders(fqcnPath, destinationFolderFile);

            File clazzFile = new File(fullPath);
            if (clazzFile.exists()) {
                updateFile(clazzFile, clazz, generatedFiles);
            } else {
                logger.debug("Adding artifact '" + clazz.getName() + "' to new file '" + clazzFile.getAbsolutePath() + "'");
                writeFile(clazz, clazzFile);
                updateSummary(clazz, UpdateType.New);
                generatedFiles.add(clazzFile);
            }
        }
        return generatedFiles;
    }

    @Override
    protected void writeFile(ClazzOrInterface clazz, File clazzFile) throws IOException {
        logger.trace("Writing '" + clazz + "' content to file '" + clazzFile.getAbsolutePath() + "'");
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        // Generate Source code
        AbstractCodeWritingVisitor codeVisitor = createCodeWritingVisitor(new CodeWriter(printWriter));
        clazz.visit(codeVisitor);
        FileUtils.writeStringToFile(clazzFile, stringWriter.getBuffer().toString(), JavaConfiguration.DEFAULT_ENCODING);
        IOUtils.closeQuietly(stringWriter);
        IOUtils.closeQuietly(printWriter);
    }

    /**
     * Create the Code writing Codevisitor to use
     *
     * @param codeWriter The code writer to use for writing out the code
     */
    protected AbstractCodeWritingVisitor createCodeWritingVisitor(CodeWriter codeWriter) {
        return new JavaCodeWritingVisitor(codeWriter, getPackageRewriter(), isUseImports());
    }

    @Override
    protected String resolveDestionationFolder(ClazzOrInterface clazz) {
        if (clazz.isClazz()) {
            if (((Clazz) clazz).isExtensionClazz()) {
                return getGenerateExtensionClazzesToDirectory();
            } else if (clazz.isTestClazz()) {
                return getGenerateTestClazzesToDirectory();
            } else {
                return getGenerateClazzesAndBaseClazzesToDirectory();
            }
        } else if (clazz.isInterface()) {
            return getGenerateInterfacesToDirectory();
        }
        throw new RuntimeException("Could not resolve destination folder for clazz '" + clazz.getName() + "' of type '" + clazz.getMetaType()
                + "'");
    }

    protected void updateFile(File clazzFile, ClazzOrInterface clazz, List<File> generatedFiles) throws IOException {
        if (isUpdatable(clazz)) {
            File temp = File.createTempFile("Temp" + clazz.getName(), ".temp");
            writeFile(clazz, temp);
            if (isFileContentEqual(clazzFile, temp)) {
                // No update
                Utils.deleteFile(temp);
                generatedFiles.add(clazzFile);
                updateSummary(clazz, UpdateType.NoUpdate);
                logger.debug("No update found for artifact '" + clazz.getName() + "' keeping exisitng file '" + clazzFile.getAbsolutePath() + "'");
            } else {
                // Update
                writeFile(clazz, clazzFile);
                generatedFiles.add(clazzFile);
                Utils.deleteFile(temp);
                updateSummary(clazz, UpdateType.Update);
                logger.debug("Update found for artifact '" + clazz.getName() + "' updating exisitng file '" + clazzFile.getAbsolutePath() + "'");
            }
        } else {
            logger.debug("Artifact '" + clazz.getName() + "' is not updatable keeping exisitng file '" + clazzFile.getAbsolutePath() + "'");
            updateSummary(clazz, UpdateType.NoUpdate);
            generatedFiles.add(clazzFile);
        }
    }

}
