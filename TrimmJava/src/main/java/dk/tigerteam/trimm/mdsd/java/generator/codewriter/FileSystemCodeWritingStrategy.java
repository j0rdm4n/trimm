/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.codewriter;

import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.util.FileSystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileSystemCodeWritingStrategy implements CodeWritingStrategy {

    private static final Log logger = LogFactory.getLog(FileSystemCodeWritingStrategy.class);
    private JavaCodeWriter codeWriter;

    public FileSystemCodeWritingStrategy(JavaCodeWriter codeWriter) {
        super();
        this.codeWriter = codeWriter;
    }

    public GeneratorSummary writeCode(JavaGeneratorContext generatorContext, List<ClazzOrInterface> allGeneratedClazzes) {
        try {
            List<File> filesGenerated = codeWriter.writeCode(allGeneratedClazzes);
            // Delete obsolete non extension classes
            List<File> deletedBaseClasses = new ArrayList<File>();
            if (codeWriter.getJavaCodeGenerationPaths().getGenerateBaseClassesToPath() != null) {
                deletedBaseClasses = FileSystemUtils.deleteFilesFromFileSystemNotPresentInArgument(
                        codeWriter.getGenerateClazzesAndBaseClazzesToDirectory(), filesGenerated);
            }
            List<File> deletedInterfaces = new ArrayList<File>();
            if (codeWriter.getJavaCodeGenerationPaths().getGenerateInterfacesToPath() != null) {
                deletedInterfaces = FileSystemUtils.deleteFilesFromFileSystemNotPresentInArgument(
                        codeWriter.getGenerateInterfacesToDirectory(), filesGenerated);
            }
            if (codeWriter.getJavaCodeGenerationPaths().getGenerateTestClassesToPath() != null) {
                FileSystemUtils.deleteFilesFromFileSystemNotPresentInArgument(codeWriter.getGenerateTestClazzesToDirectory(), filesGenerated);
            }
            codeWriter.getSummary().setNumberOfDeletedBaseClasses(deletedBaseClasses.size());
            codeWriter.getSummary().setNumberOfDeletedInterfaceClasses(deletedInterfaces.size());
            logger.info(codeWriter.getSummary().printSummary());
            logger.info("Done generating code");
            return codeWriter.getSummary();
        } catch (IOException e) {
            throw new RuntimeException("Failed during CodeWriting phase: " + e.getMessage(), e);
        }
    }

}
