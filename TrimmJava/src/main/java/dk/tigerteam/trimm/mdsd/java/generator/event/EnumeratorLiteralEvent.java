/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.meta.MetaEnumeration;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;

/**
 * An {@link EnumeratorLiteralEvent} handles a single Enumerator value (aka. an Enumerator Literal) inside an {@link Enumeration} (which
 * corresponds to an {@link MetaEnumeration})
 * <p/>
 * Example: <br/>
 * Given an {@link MetaEnumeration} with name "Colors" and 3 {@link MetaProperty}' with names "RED", "GREEN", "BLUE", the processing of this
 * {@link MetaEnumeration} would result in 4 events being fired.<br/>
 * <ul>
 * <li>1 {@link ClazzEvent} for the {@link MetaEnumeration} "Colors"</li>
 * <li>3 {@link EnumeratorLiteralEvent}'. One for each of the {@link MetaProperty}' instances.</li>
 * </ul>
 *
 * @author Jeppe Cramon
 */
public class EnumeratorLiteralEvent extends GeneratorEvent {
    private EnumLiteralField field;
    private MetaProperty metaProperty;
    private Clazz clazz;

    public EnumeratorLiteralEvent() {
        super();
    }

    public EnumeratorLiteralEvent(Clazz clazz, MetaProperty metaProperty) {
        super();
        this.clazz = clazz;
        this.metaProperty = metaProperty;
    }

    public Clazz getClazz() {
        return clazz;
    }

    public MetaProperty getMetaProperty() {
        return metaProperty;
    }

    public void setMetaProperty(MetaProperty metaProperty) {
        this.metaProperty = metaProperty;
    }

    public EnumLiteralField getField() {
        return field;
    }

    @Override
    public Field execute() {
        if (metaProperty.getName() == null) {
            throw new IllegalStateException("Enumerator MetaProperty doesn't have a name: " + metaProperty);
        }
        field = new EnumLiteralField(getMetaProperty().getName(), true);
        field.setMetaType(getMetaProperty());
        field.setType(Type.NONE);
        field.getAccessModifiers().clear();
        field.setCodeDocumentation(getMetaProperty().getDocumentation());

        if (metaProperty.hasTaggedValue(MetaProperty.ENUM_LITERAL_VALUE_TAGGEDVALUE_NAME)) {
            field.setInitializerStatement(new CodeSnippet(metaProperty.getTaggedValue(MetaProperty.ENUM_LITERAL_VALUE_TAGGEDVALUE_NAME)));
        }

        clazz.addChildren(field);
        return field;
    }

}
