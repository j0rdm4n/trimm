/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.util.MdsdJavaUtil;
import dk.tigerteam.trimm.mdsd.meta.MetaType;

import java.util.LinkedList;
import java.util.List;

/**
 * Supports the creation of a single Constructor for a Clazz which has the stereotype <code>Constructor</code>.
 * The arguments/parameters to the constructor can be specified by given the class Attributes/Association that should
 * be part of the constructor's arguments with the <code>ConstructorArgument</code> stereotype.<br/>
 * You can also specify an optional Constructor code body by adding a <code>constructorBody</code> Tagged value to the Clazz.
 * <p/>
 * This listeners can also handle super class constructor arguments in case a Clazz inherits from another Clazz which also is annotated
 * with the <code>Constructor</code> stereotype.
 *
 * @author Lasse Cramon
 * @author Jeppe Cramon
 */
public class ConstructorListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

    public static final String CONSTRUCTOR_STEREOTYPE_NAME = "Constructor";

    public static final String CONSTRUCTOR_ARGUMENT_STEREOTYPE_NAME = "ConstructorArgument";

    public static final String CONSTRUCTOR_BODY_TAGVALUE_NAME = "constructorBody";

    @Override
    protected boolean handleClazzEvent(ClazzEvent event) {
        Clazz clazz = event.getClazz();
        if (hasConstructorStereoType(clazz.getMetaType())) {
            List<Property> properties = clazz.getAllPropertiesIncludingSuperClazzProperties();
            List<Property> superClazzProperties = new LinkedList<Property>();
            if (clazz.extendsAGeneratedClazz()) {
                superClazzProperties = clazz.getExtendsType().getClazz().getAllPropertiesIncludingSuperClazzProperties();
            }

            Constructor constructor = new Constructor(clazz.getName(), CodeElement.AccessModifier.Public);
            if (!superClazzProperties.isEmpty()) {
                //MdsdJavaUtil.sortPropertiesAlphabetical(superClazzProperties);
                constructor.addCode(new CodeSnippet("super(", MdsdJavaUtil.propertiesAsCommaSeparatedString(superClazzProperties), ");"));
            }
            if (!properties.isEmpty()) {
                //MdsdJavaUtil.sortPropertiesAlphabetical(properties);
                for (Property property : properties) {
                    if (hasConstructorArgumentStereoType(property.getMetaType())) {
                        constructor.addParameters(new Parameter(property.getName(), property.getField().getType()));
                    }
                    if (!superClazzProperties.contains(property) && property.getMetaType() != null && hasConstructorArgumentStereoType(property.getMetaType())) {
                        constructor.addCode(new CodeSnippet("this.", property.getField().getName(), " = ", property.getName(), ";"));
                    }
                }
            }
            String constructorBody = clazz.getMetaType().getTaggedValue(CONSTRUCTOR_BODY_TAGVALUE_NAME);
            if (constructorBody != null) {
                constructor.addCode(new CodeSnippet(constructorBody));
            }
            if (!constructor.getChildren().isEmpty()) {
                clazz.add(constructor);
            }
        }
        return true;
    }

    private boolean hasConstructorStereoType(MetaType metaType) {
        if (metaType != null && metaType.hasStereoType(CONSTRUCTOR_STEREOTYPE_NAME)) {
            return true;
        }
        return false;
    }

    private boolean hasConstructorArgumentStereoType(MetaType metaType) {
        if (metaType != null && metaType.hasStereoType(CONSTRUCTOR_ARGUMENT_STEREOTYPE_NAME)) {
            return true;
        }
        return false;
    }

}
