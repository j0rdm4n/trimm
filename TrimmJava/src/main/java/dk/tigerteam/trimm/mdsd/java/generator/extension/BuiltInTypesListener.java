/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.Parameter;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.MethodEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.SimplePropertyEvent;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * Listener which corrects the FQCN for a series of built in types ("date" -> {@link Date}, "bigdecimal" -> {@link BigDecimal},
 * "biginteger" -> {@link BigInteger}, "calendar" -> {@link Calendar} and "timestamp" -> {@link Timestamp}).<p/>
 * <b>Note: This listener should be registered as the very first listener to work properly!!</b>
 *
 * @author Jeppe Cramon
 */
public class BuiltInTypesListener extends AbstractGeneratorEventListener {

    private static final Log logger = LogFactory.getLog(BuiltInTypesListener.class);

    @Override
    protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
        Type type = null;
        if (event.getMetaProperty().getCardinalityType() == CardinalityType.Many) {
            type = event.getProperty().getField() != null ? event.getProperty().getField().getType().getGenerics().get(0) : event.getProperty().getType().getGenerics().get(0);
        } else {
            type = event.getProperty().getField() != null ? event.getProperty().getField().getType() : event.getProperty().getType();
        }
        MetaClazz metaClazz = event.getMetaProperty().getType();
        resolveBuiltInTypes(event.getClazzOrInterface(), type);
        if (type.getClazzOrInterface() == null) {
            // Fix the meta model as well (serves later processes that use the built in types directly, without having the possibility of package rewriting, well)
            metaClazz.setName(type.getName());
        }
        return true;
    }

    @Override
    protected boolean handleMethodEvent(MethodEvent event) {
        for (Parameter parameter : event.getMethod().getParameters()) {
            if (parameter.getType() == null || parameter.getType().getName() == null || parameter.getType().getName().equals("null")) throw new IllegalStateException("Parameter " + parameter +  " in Operation " + event.getOperation() + " is <null>");
            if (parameter.getMetaType().getType().isBuiltInType()) {
                resolveBuiltInTypes(event.getClazzOrInterface(), parameter.getType());
                // Fix the meta model as well (serves later processes that use the built in types directly, without having the possibility of package rewriting, well)
                parameter.getMetaType().getType().setName(parameter.getType().getName());
            }
        }
        if (event.getMethod().getMetaType().getReturnParameter().getType().isBuiltInType()) {
            resolveBuiltInTypes(event.getClazzOrInterface(), event.getMethod().getReturnType());
            event.getMethod().getMetaType().getReturnParameter().getType().setName(event.getMethod().getReturnType().getName());
        }
        return true;
    }

    /**
     * Override this method to provide custom resolving of built in types (resolving means fixing the {@link Type#getName()} property)
     *
     * @param type The type to resolve
     */
    protected void resolveBuiltInTypes(ClazzOrInterface owner, Type type) {
        if (type.getClazz() == null && type.getWrappedJavaClass() == null) {
            logger.debug("Trying to resolve built in type '" + type.getName() + "'");
            if (type.getName().endsWith("[]")) {
                type.external();
            } else if (type.getName().equalsIgnoreCase("void")) {
                type.setName("void").external();
            } else if (type.getName().equalsIgnoreCase("date")) {
                type.setName(Date.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("bigdecimal")) {
                type.setName(BigDecimal.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("biginteger")) {
                type.setName(BigInteger.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("calendar")) {
                type.setName(Calendar.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("timestamp")) {
                type.setName(Timestamp.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("String")) {
                type.setName("String").external();
            } else if (type.getName().equals("Char")) {
                type.setName(char.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("boolean")) {
                type.setName(boolean.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("double")) {
               type.setName(double.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("integer")) {
               type.setName(int.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("int")) {
              type.setName(int.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("float")) {
                type.setName(float.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("long")) {
                type.setName(long.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("byte")) {
                type.setName(byte.class.getName()).external();
            } else if (type.getName().equalsIgnoreCase("short")) {
                type.setName(short.class.getName()).external();
            } else {
                throw new RuntimeException("Type '" + type + "' (defined in "+ owner + ") is unknown, please use" +
                        " configuration 'mapUmlPropertyTypes' property to map custom types");
            }
        }
    }


}
