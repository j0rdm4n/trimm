/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator;

/**
 * Collection of various Java code generation related Stereotypes
 *
 * @author Jeppe Cramon
 * @see JavaCollectionTypeResolver
 */
public abstract class JavaStereotypes {
    /**
     * Extension class generation is FORCED even if it isn't turned on generally
     */
    public static final String EXTENSION_CLASS_STEREOTYPE_NAME = "ExtensionClass";
    /**
     * Extension class generation is prohibited even if it's turned on generally
     */
    public static final String NO_EXTENSION_CLASS_STEREOTYPE_NAME = "NoExtensionClass";

    /**
     * Name of the stereotype that can be applied to Classes, Interfaces, Enumerations, Operations and Properties to indicate that they
     * shouldn't be included in the generation process
     */
    public static final String DONT_GENERATE_STEREOTYPE_NAME = "DontGenerate";

}
