/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.codewriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Call the Eclipse source code formatter tool from the command line
 *
 * @author Lasse Cramon
 */
public class EclipseCodeFormatter {

    private static final Log logger = LogFactory.getLog(EclipseCodeFormatter.class);

    private static final String FORMATTER_CLASS_NAME = "org.eclipse.jdt.core.JavaCodeFormatter";
    private String eclipsePath;
    private String javaVmPath;
    private String configFilePath;
    private String[] sourcePaths;

    public EclipseCodeFormatter(String eclipsePath, String javaVmPath,
                                String configFilePath, String... sourcePaths) {
        super();
        this.eclipsePath = eclipsePath;
        this.javaVmPath = javaVmPath;
        this.configFilePath = configFilePath;
        this.sourcePaths = sourcePaths;
    }

    public void formatCode() {
        for (int i = 0; i < sourcePaths.length; i++) {
            try {
                String command = setupFormatterArguments(sourcePaths[i]);
                if (command == null) {
                    return;
                }
                logger.debug("Running Eclipse code formatter with '" + command + "'");
                Runtime runtime = Runtime.getRuntime();
                @SuppressWarnings("unused")
                Process process = runtime.exec(command);
            } catch (Exception e) {
                logger.error("Running the Eclipse code formatter produced error " + e.toString());
            }
        }
        logger.debug("Finised formatting source code '" + sourcePaths + "' with Eclipse code formatter");
    }

    private String setupFormatterArguments(String sourceFilesPath) {
        if (eclipsePath == null || eclipsePath.isEmpty()) {
            System.err
                    .println("Path to installed eclipse has to be configured, will not be able to format generated code");
            return null;
        }
        if (javaVmPath == null || javaVmPath.isEmpty()) {
            System.err
                    .println("Path to installed jvm is not correctly configured, will not be able to format generated code");
            return null;
        }
        if (configFilePath == null || configFilePath.isEmpty()) {
            System.err
                    .println("Path to formatter config file has to be configured, will not be able to format generated code");
            return null;
        }
        if (sourceFilesPath == null || sourceFilesPath.isEmpty()) {
            System.err
                    .println("Path to source files has to be configured, will not be able to format generated code");
            return null;
        }
        StringBuilder builder = new StringBuilder();
        builder.append(eclipsePath).append(" ");
        builder.append("-vm").append(" ").append(javaVmPath).append(" ");
        builder.append("-application").append(" ").append(FORMATTER_CLASS_NAME).append(" ");
        builder.append("-verbose").append(" ");
        builder.append("-config").append(" ");
        builder.append(configFilePath).append(" ");
        builder.append(sourceFilesPath);
        return builder.toString();
    }

}
