/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.meta.MetaOperation;
import dk.tigerteam.trimm.mdsd.meta.MetaParameter;

/**
 * Event for {@link Method}'s which are based on {@link MetaOperation}'s
 *
 * @author Jeppe Cramon
 */
public class MethodEvent extends GeneratorEvent {
    private Method method;
    private MetaOperation operation;
    private ClazzOrInterface clazzOrInterface;
    /**
     * Stereotype that can be applied to Operations to indicate that their implementation is specified in UML using the
     * {@link #METHOD_BODY_TAGGED_VALUE} tagged value<br/>
     * This is an alternative to using the code segment in UML (which is supported by EA but not by MagicDraw)
     */
    public static final String IMPLEMENTED_STEREOTYPE = "Implemented";
    /**
     * Tagged value that accompanies the {@link #IMPLEMENTED_STEREOTYPE} stereotype to provide the implementation code for the Operation
     */
    public static final String METHOD_BODY_TAGGED_VALUE = "methodBody";

    /**
     * Create a new MethodEvent
     *
     * @param clazz     The {@link ClazzOrInterface} that this method belongs to
     * @param operation The {@link MetaOperation} that this method is based upon
     */
    public MethodEvent(ClazzOrInterface clazz, MetaOperation operation) {
        super();
        this.clazzOrInterface = clazz;
        this.operation = operation;
    }

    /**
     * The {@link Method} created by this event
     */
    public Method getMethod() {
        return method;
    }

    /**
     * The {@link MetaOperation} that this method is based upon
     */
    public MetaOperation getOperation() {
        return operation;
    }

    /**
     * The {@link ClazzOrInterface} that this method belongs to
     */
    public ClazzOrInterface getClazzOrInterface() {
        return clazzOrInterface;
    }

    @Override
    public CodeElement execute() {
        method = new Method(operation.getName(), true, AccessModifier.fromVisibility(operation.getVisibility()));
        method.setMetaType(operation);
        if (operation.isStatic()) {
            method.addAccessModifiers(AccessModifier.Static);
        }
        if (operation.hasStereoType(IMPLEMENTED_STEREOTYPE) && operation.getTaggedValue(METHOD_BODY_TAGGED_VALUE) != null) {
            method.getChildren().add(new CodeSnippet(operation.getTaggedValue(METHOD_BODY_TAGGED_VALUE)));
        } else if (operation.getCode() == null || operation.isAbstract()) {
            method.addAccessModifiers(AccessModifier.Abstract);
        } else if (operation.getCode() != null && !"".equals(operation.getCode())) {
            method.getChildren().add(new CodeSnippet(operation.getCode()));
        }

        method.setCodeDocumentation(operation.getDocumentation());
        if (operation.getReturnParameter() == null || operation.getReturnParameter().getType() == null) {
            throw new RuntimeException("MetaOperation '" + operation.getName() + operation.getId() + "' does not have return type specified");
        }
        // Return type
        Type returnType = new Type(operation.getReturnParameter().getType());
        method.setReturnType(returnType);

        // Parameters
        for (MetaParameter metaParameter : operation.getParameters()) {
            Parameter parameter = new Parameter(metaParameter.getName());
            parameter.setMetaType(metaParameter);
            parameter.setType(new Type(metaParameter.getType()));
            parameter.setCodeDocumentation(metaParameter.getDocumentation());
            method.addParameters(parameter);
        }

        // Add to the Clazz or Interface
        clazzOrInterface.getChildren().add(method);

        // Allow listeners to change the type before we define accessor methods
        broadcastEvent();
        return method;
    }

}
