/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;

import java.util.List;

/**
 * Common interface for all {@link GeneratorEvent} listeners.<br/>
 * All interesting things that the {@link dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator} does is issued as {@link GeneratorEvent}'. This allows custom extensions without
 * having to specialize {@link dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator} or other classes that it uses.
 *
 * @author Jeppe Cramon
 * @see AbstractGeneratorEventListener Which provides nice typified handler methods according to the Template method design pattern
 */
public interface GeneratorEventListener {
    /**
     * Handle the event
     *
     * @param event The event to handle
     * @return true if the event loop handling should continue for this event, otherwise false
     */
    boolean handle(GeneratorEvent event);

    /**
     * This method will be called, when generation is commenced (begun). Use this method to perform eventual initialization or resetting.
     */
    void generationCommenced();

    /**
     * This method is called when generation is complete
     *
     * @param allGeneratedClazzes A (modifiable) list of all the Clazzes which have been generated
     */
    void generationComplete(List<ClazzOrInterface> allGeneratedClazzes);
}
