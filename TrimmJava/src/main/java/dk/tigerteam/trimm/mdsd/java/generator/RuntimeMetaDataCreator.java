/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.meta.*;
import dk.tigerteam.trimm.mdsd.runtime.*;
import dk.tigerteam.trimm.util.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


/**
 * Create a MetaModel construction, by which you can query MetaData about a given {@link Clazz}, {@link Field}, {@link Method} or
 * {@link Property} at runtime (i.e. when your application is running)
 *
 * @see dk.tigerteam.trimm.persistence.mdsd.test.JpaUnitTest
 * @see dk.tigerteam.trimm.mdsd.jpa.JpaUnittestCreator
 * @see dk.tigerteam.trimm.persistence.mdsd.test.RuntimeMetaTestDataCreator
 * @author Jeppe Cramon
 */
public class RuntimeMetaDataCreator {

    private String destinationPackageName;
    private String metaModelFactoryClassName = "RuntimeMetaModelFactory";
    private boolean useLazyInitialization = true;

    public RuntimeMetaDataCreator() {
        super();
    }

    public RuntimeMetaDataCreator(String destinationPackageName) {
        super();
        this.destinationPackageName = destinationPackageName;
    }

    public String getMetaModelFactoryClassName() {
        return metaModelFactoryClassName;
    }

    public void setMetaModelFactoryClassName(String metaModelFactoryClassName) {
        this.metaModelFactoryClassName = metaModelFactoryClassName;
    }

    public String getDestinationPackageName() {
        return destinationPackageName;
    }

    public void setDestinationPackageName(String destinationPackageName) {
        this.destinationPackageName = destinationPackageName;
    }

    public boolean isUseLazyInitialization() {
        return useLazyInitialization;
    }

    public void setUseLazyInitialization(boolean useLazyInitialization) {
        this.useLazyInitialization = useLazyInitialization;
    }

    public void create(List<ClazzOrInterface> clazzesCreated) {
        CodeModel codeModel = JavaGeneratorContext.getContext().getCodeModel();
        MetaModel metaModel = JavaGeneratorContext.getContext().getMetaModel();

        CodePackage codePackage = new CodePackage(destinationPackageName);
        codeModel.addChildren(codePackage);
        Clazz factory = new Clazz(metaModelFactoryClassName, true, AccessModifier.Public, AccessModifier.Abstract);
        codePackage.addChildren(factory);
        factory.addAccessModifiers();

        // Create static field
        factory.addChildren(new Field("metaModel", true, AccessModifier.Private, AccessModifier.Static)
                .setType(RuntimeMetaModel.class));

        RuntimeMetaDataCreationContext context = new RuntimeMetaDataCreationContext(codeModel, metaModel);
        for (MetaType metaType : metaModel.getChildren()) {
            recursivelyGenerate(metaType, context);
        }
        // Create the necessary initialization methods
        Method initializeMethod = new Method("initialize", factory, true, AccessModifier.Private, AccessModifier.Static);
        initializeMethod.addChildren(new CodeSnippet().printlnAll("instantiatePackages();", "instantiateClazzesAndInterfaces();",
                "establishClazzHierarchyAndRealizations();", "initializeClazzProperties();", "initializeClazzOperations();",
                "initializeAssociations();"));

        handleInstantiatePackagesCreation(factory, context);
        handleInstantiateClazzesCreation(factory, context);
        handleEstablishClazzHierarchy(factory, context);
        handleInitializeClazzPropertiesCreation(factory, context);
        handleInitializeClazzOperationsCreation(factory, context);
        handleInitializeAssociationsCreation(factory, context);

        // Create static constructor
        Constructor constructor = new Constructor(AccessModifier.Static);
        factory.addChildren(constructor);

        // if (!useLazyInitialization) {
        constructor.addChildren(new CodeSnippet("metaModel = new ", RuntimeMetaModel.class, "(", Utils.quotedString(metaModel.getId()), ");")
                .println());
        constructor.addChildren(new CodeSnippet("initialize();"));
        // }

        // Add getMetaModel static method
        new Method("getMetaModel", factory, true, AccessModifier.Public, AccessModifier.Static).setReturnType(RuntimeMetaModel.class)
                .addChildren(new CodeSnippet("return metaModel;"));

        clazzesCreated.add(factory);
    }

    private void handleEstablishClazzHierarchy(Clazz factory, RuntimeMetaDataCreationContext context) {
        Method method = new Method("establishClazzHierarchyAndRealizations", factory, true, AccessModifier.Private, AccessModifier.Static);
        method.addChildren(context.establishClazzHierarchy);
    }

    private void handleInitializeAssociationsCreation(Clazz factory, RuntimeMetaDataCreationContext context) {
        Method method = new Method("initializeAssociations", factory, true, AccessModifier.Private, AccessModifier.Static);
        method.addChildren(context.createAssociationCode);
    }

    private void handleInitializeClazzPropertiesCreation(Clazz factory, RuntimeMetaDataCreationContext context) {
        Method initializeClazzProperties = new Method("initializeClazzProperties", factory, true, AccessModifier.Private, AccessModifier.Static);
        CodeSnippet initializeClassProperiesCode = new CodeSnippet();
        initializeClazzProperties.addChildren(initializeClassProperiesCode);

        for (MetaClazz metaClazz : context.clazzInitContexts.keySet()) {
            Method initClazzPropertiesMethod = new Method("init" + metaClazz.getFQCN().replaceAll("\\.", "_") + "Properties", factory, true,
                    AccessModifier.Private, AccessModifier.Static);
            // Call the method from "initializeClazzProperties"
            initializeClassProperiesCode.println(initClazzPropertiesMethod.getName() + "();");
            // Create body
            initClazzPropertiesMethod.addChildren(context.clazzInitContexts.get(metaClazz).classPropertiesInitCode);
        }
    }

    private void handleInitializeClazzOperationsCreation(Clazz factory, RuntimeMetaDataCreationContext context) {
        Method initializeClazzProperties = new Method("initializeClazzOperations", factory, true, AccessModifier.Private, AccessModifier.Static);
        CodeSnippet initializeClassOperationsCode = new CodeSnippet();
        initializeClazzProperties.addChildren(initializeClassOperationsCode);

        for (MetaClazz metaClazz : context.clazzInitContexts.keySet()) {
            Method initClazzOperationsMethod = new Method("init" + metaClazz.getFQCN().replaceAll("\\.", "_") + "Operations", factory, true,
                    AccessModifier.Private, AccessModifier.Static);
            // Call the method from "initializeClazzOperations"
            initializeClassOperationsCode.println(initClazzOperationsMethod.getName() + "();");
            // Create body
            initClazzOperationsMethod.addChildren(context.clazzInitContexts.get(metaClazz).classOperationsInitCode);
        }
    }

    private void handleInstantiateClazzesCreation(Clazz factory, RuntimeMetaDataCreationContext context) {
        Method method = new Method("instantiateClazzesAndInterfaces", factory, true, AccessModifier.Private, AccessModifier.Static);
        method.addChildren(context.createClazzCode);
    }

    private void handleInstantiatePackagesCreation(Clazz factory, RuntimeMetaDataCreationContext context) {
        Method method = new Method("instantiatePackages", factory, true, AccessModifier.Private, AccessModifier.Static);
        method.addChildren(new CodeSnippet("metaModel"), context.createPackageCode, new CodeSnippet(";"));
    }

    protected void recursivelyGenerate(MetaType metaType, RuntimeMetaDataCreationContext context) {
        if (metaType instanceof MetaPackage) {
            recursivelyGenerate((MetaPackage) metaType, context);
        } else if (metaType instanceof MetaClazz) {
            recursivelyGenerate((MetaClazz) metaType, context);
        } else if (metaType instanceof MetaAssociation) {
            if (metaType.getOwner() != context.metaModel) {
                throw new IllegalStateException("Unsupported metaType '" + metaType.getClass().getName() + "'. Details: " + metaType
                        + ", with owner: " + (metaType.getOwner() != null ? metaType.getOwner() : "<null>"));
            }
        } else {
            throw new IllegalStateException("Unsupported metaType '" + metaType.getClass().getName() + "'. Details: " + metaType
                    + ", with owner: " + (metaType.getOwner() != null ? metaType.getOwner() : "<null>"));
        }
    }

    protected void recursivelyGenerate(MetaProperty metaProperty, RuntimeMetaClazzCreationContext metaTypeContext) {
        String propertyName = metaProperty.getName();
        boolean hasCorrespondingJavaProperty = false;
        try {
            propertyName = metaTypeContext.rootContext.codeModel.getByMetaTypeId(Property.class, metaProperty.getId()).getName();
            hasCorrespondingJavaProperty = true;
        } catch (IllegalStateException e) {
            // Ignore
        }

        CodeSnippet classPropertiesInitCode = metaTypeContext.classPropertiesInitCode;
        classPropertiesInitCode.printAll("new ", RuntimeMetaProperty.class, "(", Utils.quotedString(metaProperty.getId()), ", ",
                metaProperty.isFabricated(), ", ", propertyName != null ? Utils.quotedString(propertyName) : "(String)null", ", ",
                hasCorrespondingJavaProperty, ")");
        if (!metaProperty.getOwnerClazz().isEnumerationClazz()) {
            if (metaProperty.getType().isBuiltInType()) {
                classPropertiesInitCode.printAll(".setType(new ", RuntimeMetaClazz.class, "(", new Type(UUID.class, true), ".randomUUID().toString(), true, ",
                        metaProperty.getType().getName() + ".class", ").setBuiltInType(true))");
            } else {
                classPropertiesInitCode.printAll(".setType((", RuntimeMetaClazz.class, ")metaModel.getById(", Utils.quotedString(metaProperty.getType()
                        .getId()), "))");
            }
        }
        classPropertiesInitCode.printAll(".setAggregation(", RuntimeMetaProperty.Aggregation.class, ".", metaProperty.getAggregation()
                .toString(), ")");
        if (metaProperty.getDefaultValue() != null) {
            classPropertiesInitCode.printAll(".setDefaultValue(", Utils.quotedString(metaProperty.getDefaultValue()), ")");
        }
        classPropertiesInitCode.printAll(".setStatic(", metaProperty.isStatic(), ")");
        classPropertiesInitCode.printAll(".setDerived(", metaProperty.isDerived(), ")");
        classPropertiesInitCode.printAll(".setLowerBound(", metaProperty.getLowerBound(), ")");
        classPropertiesInitCode.printAll(".setUpperBound(", metaProperty.getUpperBound(), ")");
        classPropertiesInitCode.printAll(".setMaxCardinality(", metaProperty.getMaxCardinality(), ")");
        classPropertiesInitCode.printAll(".setMinCardinality(", metaProperty.getMinCardinality(), ")");
        classPropertiesInitCode.printAll(".setOrdered(", metaProperty.isOrdered(), ")");
        classPropertiesInitCode.printAll(".setReadable(", metaProperty.isReadable(), ")");
        classPropertiesInitCode.printAll(".setWriteable(", metaProperty.isWriteable(), ")");
        classPropertiesInitCode.printAll(".setOwner((", RuntimeMetaClazz.class, ")metaModel.getById(",
                Utils.quotedString(metaTypeContext.metaClazz.getId()), "))");
        generateCommonRuntimeMetaTypeCode(metaProperty, classPropertiesInitCode);
        classPropertiesInitCode.println(";");
        // Handle association
        if (metaProperty.isPartInAnAssociation()) {
            MetaAssociation association = metaProperty.getAssociation();
            MetaProperty ownerProperty = null;
            MetaProperty ownedProperty = null;
            if (metaProperty.isOwnerOfAssociation()) {
                ownerProperty = metaProperty;
                ownedProperty = association.getOppositeProperty(metaProperty);
            } else {
                ownedProperty = metaProperty;
                ownerProperty = association.getOppositeProperty(metaProperty);
            }
            CodeSnippet associationCode = metaTypeContext.rootContext.createAssociationCode;
            associationCode.printlnAll("new ", RuntimeMetaAssociation.class, "(", Utils.quotedString(association.getId()), ", ",
                    association.isFabricated(), ").setOwnerProperty((", RuntimeMetaProperty.class, ")metaModel.getById(",
                    Utils.quotedString(ownerProperty.getId()), ")).setOwnedProperty((", RuntimeMetaProperty.class, ")metaModel.getById(",
                    Utils.quotedString(ownedProperty.getId()), "))");
            associationCode.printAll(".setBidirectional(", association.isBidirectional(), ")");
            generateCommonRuntimeMetaTypeCode(association, associationCode);
            associationCode.println(";");
        }
    }

    protected void recursivelyGenerate(MetaOperation metaOperation, RuntimeMetaClazzCreationContext metaTypeContext) {
        boolean hasCorrespondingJavaMethod = false;
        // Just try and look for Java methods, if some Listener has generated a java method for then we will add info to
        // the runtime metamodel about it
        try {
            if (metaTypeContext.rootContext.codeModel.getByMetaTypeId(Method.class, metaOperation.getId()) != null) {
                hasCorrespondingJavaMethod = true;
            }
        } catch (IllegalStateException e) {
            // Ignore
        }

        CodeSnippet classOperationsInitCode = metaTypeContext.classOperationsInitCode;
        classOperationsInitCode.printAll("new ", RuntimeMetaOperation.class, "(", Utils.quotedString(metaOperation.getId()), ", ",
                metaOperation.isFabricated(), ", ", Utils.quotedString(metaOperation.getName()), ", ", hasCorrespondingJavaMethod, ")");

        classOperationsInitCode.printAll(".setStatic(", metaOperation.isStatic(), ")");
        classOperationsInitCode.printAll(".setAbstract(", metaOperation.isAbstract(), ")");
        classOperationsInitCode.printAll(".setBehaviour(", metaOperation.getBehaviour(), ")");
        classOperationsInitCode.printAll(".setCode(", metaOperation.getCode(), ")");
        // Generate for the operations parameters
        for (MetaParameter parameter : metaOperation.getParameters()) {
            classOperationsInitCode.printAll(".addParameters((", RuntimeMetaParameter.class, ")");
            generateForParameter(classOperationsInitCode, parameter);
            classOperationsInitCode.printAllLn(")");
        }
        // Generate for the return parameter
        classOperationsInitCode.printAll(".setReturnParameter((", RuntimeMetaParameter.class, ")");
        generateForParameter(classOperationsInitCode, metaOperation.getReturnParameter());
        classOperationsInitCode.printAllLn(")");

        classOperationsInitCode.printAll(".setOwner((", RuntimeMetaClazz.class, ")metaModel.getById(",
                Utils.quotedString(metaTypeContext.metaClazz.getId()), "))");
        generateCommonRuntimeMetaTypeCode(metaOperation, classOperationsInitCode);

        classOperationsInitCode.println(";");
    }

    private void generateForParameter(CodeSnippet classOperationsInitCode, MetaParameter parameter) {
        classOperationsInitCode.printAll("new ", RuntimeMetaParameter.class, "(", Utils.quotedString(parameter.getId()), ", ",
                parameter.isFabricated(), ", ", Utils.quotedString(parameter.getName()), ")");
        if (parameter.getType().isBuiltInType()) {
            classOperationsInitCode.printAll(".setType(new ", RuntimeMetaClazz.class, "(", new Type(UUID.class, true), ".randomUUID().toString(), true, ",
                    parameter.getType().getName() + ".class", ").setBuiltInType(true))");
        } else {
            classOperationsInitCode.printAll(".setType((", RuntimeMetaClazz.class, ")metaModel.getById(", Utils.quotedString(parameter.getType()
                    .getId()), "))");
        }
        generateCommonRuntimeMetaTypeCode(parameter, classOperationsInitCode);
    }

    protected void recursivelyGenerate(MetaClazz metaClazz, RuntimeMetaDataCreationContext context) {
        if (context.shouldGenerateFor(metaClazz)) {
            RuntimeMetaClazzCreationContext runtimeMetaTypeCreationContext = null;
            if (metaClazz.isInterface()) {
                Interface _interface = JavaGeneratorContext.getContext().getInterfaceFromMetaClazz(metaClazz);
                Class<? extends RuntimeMetaClazz> runtimeMetaClazzType = chooseMetaRuntimeMetaClazzType(metaClazz);
                runtimeMetaTypeCreationContext = new RuntimeMetaClazzCreationContext(_interface, metaClazz, context);
                context.createClazzCode.printAll("new ", runtimeMetaClazzType, "(", Utils.quotedString(metaClazz.getId()), ", ",
                        metaClazz.isFabricated(), ", ", _interface.asFQCN(true), ")");
                context.createClazzCode.printAll(".setOwner(metaModel.getById(", Utils.quotedString(metaClazz.getOwner().getId()), "))");
                context.createClazzCode.println("");
                generateCommonRuntimeMetaTypeCode(metaClazz, context.createClazzCode);
                context.createClazzCode.println(";");

                // Realizations
                for (MetaClazz realizer : ((MetaInterface) metaClazz).getRealizedBy()) {
                    context.establishClazzHierarchy.println("// " + metaClazz.getName() + " is realized by " + realizer.getName());
                    context.establishClazzHierarchy.printAllLn("((", RuntimeMetaInterface.class, ")metaModel.getById(",
                            Utils.quotedString(metaClazz.getId()), ")).getRealizedBy().add((", RuntimeMetaClazz.class, ")metaModel.getById(",
                            Utils.quotedString(realizer.getId()), "));");
                }
            } else {
                Clazz clazz = (Clazz) JavaGeneratorContext.getContext().getClazzFromMetaClazz(metaClazz);

                Class<? extends RuntimeMetaClazz> runtimeMetaClazzType = chooseMetaRuntimeMetaClazzType(metaClazz);
                context.createClazzCode.printAll("new ", runtimeMetaClazzType, "(", Utils.quotedString(metaClazz.getId()), ", ",
                        metaClazz.isFabricated(), ", ", clazz.asFQCN(true).withOverrideImports(true), ")");
                context.createClazzCode.printAll(".setOwner(metaModel.getById(", Utils.quotedString(metaClazz.getOwner().getId()), "))");
                context.createClazzCode.println("");
                generateCommonRuntimeMetaTypeCode(metaClazz, context.createClazzCode);
                context.createClazzCode.println(";");

                generateMetaTypeFieldAndMethod(metaClazz, clazz, runtimeMetaClazzType);

                // Clazz hierarchy
                if (metaClazz.getSuperClazz() != null) {
                    context.establishClazzHierarchy.println("// " + metaClazz.getName() + " has super-clazz " + metaClazz.getSuperClazz().getName());
                    context.establishClazzHierarchy.printAll("((", RuntimeMetaClazz.class, ")metaModel.getById(", Utils.quotedString(metaClazz.getId()),
                            ")).setSuperClazz((", RuntimeMetaClazz.class, ")metaModel.getById(", Utils.quotedString(metaClazz.getSuperClazz().getId()), "));");
                    context.establishClazzHierarchy.println();
                }

                runtimeMetaTypeCreationContext = new RuntimeMetaClazzCreationContext(clazz, metaClazz, context);
                context.clazzInitContexts.put(metaClazz, runtimeMetaTypeCreationContext);

                for (MetaProperty property : metaClazz.findImmediateChildrenOfType(MetaProperty.class)) {
                    recursivelyGenerate(property, runtimeMetaTypeCreationContext);
                }
            }

            for (MetaOperation operation : metaClazz.findImmediateChildrenOfType(MetaOperation.class)) {
                recursivelyGenerate(operation, runtimeMetaTypeCreationContext);
            }
        }
    }

    private void generateMetaTypeFieldAndMethod(MetaClazz metaClazz, Clazz clazz, Class<? extends RuntimeMetaClazz> runtimeMetaClazzType) {
        dk.tigerteam.trimm.mdsd.java.codedom.Field metaTypeField = new dk.tigerteam.trimm.mdsd.java.codedom.Field("metaType", clazz, true, AccessModifier.Public,
                AccessModifier.Static);
        metaTypeField.setType(new Type(runtimeMetaClazzType));
        metaTypeField.setInitializerStatement(new CodeSnippet("(", new Type(runtimeMetaClazzType), ") ", new Type(destinationPackageName + "."
                + metaModelFactoryClassName), ".getMetaModel().getById(", Utils.quotedString(metaClazz.getId()), ")"));
        clazz.addChildren(metaTypeField);

        // Add instance getMetaType() method
        Method getMetaTypeMethod = new Method("getMetaType", clazz, true, AccessModifier.Public).setReturnType(new Type(runtimeMetaClazzType));
        getMetaTypeMethod.addChildren(new CodeSnippet("return metaType;"));
        if (clazz.getExtendsType() != null && clazz.getExtendsType().getClazz() != null) {
            getMetaTypeMethod.addAnnotations(new Annotation(Override.class));
        }

        // Generate for the Extension Clazz too if we're processing a Base Clazz (note: There's only one MetaClazz for both the
        // Base clazz and the Extension Clazz, therefore this extra step)
        if (clazz.isBaseClazz()) {
            Clazz extensionClazzForBaseClazz = JavaGeneratorContext.getContext().getExtensionClazzForBaseClazz(clazz);
            generateMetaTypeFieldAndMethod(metaClazz, extensionClazzForBaseClazz, runtimeMetaClazzType);
        }
    }

    protected Class<? extends RuntimeMetaClazz> chooseMetaRuntimeMetaClazzType(MetaClazz metaClazz) {
        if (metaClazz instanceof MetaEnumeration)
            return RuntimeMetaEnumeration.class;
        if (metaClazz instanceof MetaAssociationClazz)
            return RuntimeMetaAssociationClazz.class;
        if (metaClazz instanceof MetaInterface)
            return RuntimeMetaInterface.class;
        return RuntimeMetaClazz.class;
    }

    protected void recursivelyGenerate(MetaPackage metaPackage, RuntimeMetaDataCreationContext context) {
        if (context.shouldGenerateFor(metaPackage)) {
            CodePackage codePackage = context.codeModel.getByMetaTypeId(CodePackage.class, metaPackage.getId());
            context.createPackageCode.printAll(".add(new ", RuntimeMetaPackage.class, "(", Utils.quotedString(metaPackage.getId()), ", ",
                    metaPackage.isFabricated(), ", ", codePackage.asFQN(true), "))");
            context.createPackageCode.println("");
            generateCommonRuntimeMetaTypeCode(metaPackage, context.createPackageCode);
            for (MetaType clazz : metaPackage.findImmediateChildrenOfType(MetaClazz.class)) {
                recursivelyGenerate(clazz, context);
                // Include enums
                for (MetaType nestedClazzesAndEnums : clazz.findImmediateChildrenOfType(MetaClazz.class)) {
                    recursivelyGenerate(nestedClazzesAndEnums, context);
                }
            }
        }
        for (MetaType _package : metaPackage.findImmediateChildrenOfType(MetaPackage.class)) {
            recursivelyGenerate(_package, context);
        }

    }

    protected void generateCommonRuntimeMetaTypeCode(MetaType metaType, CodeSnippet codeSnippet) {
        for (String taggedName : metaType.getTaggedValues().keySet()) {
            String taggedValue = metaType.getTaggedValue(taggedName);
            codeSnippet.printAll(".addTaggedValue(", Utils.quotedString(taggedName.replaceAll("\"", "'").replaceAll("\r", "").replaceAll("\n", "")),
                    ", ", taggedValue == null ? "null" : Utils.quotedString(taggedValue.replaceAll("\"", "'").replaceAll("\r", "").replaceAll("\n", "")),
                    ")");
            codeSnippet.println();
        }
        for (MetaStereoType stereoType : metaType.getStereoTypes()) {
            codeSnippet.printAll(".add(new ", RuntimeMetaStereoType.class, "(", Utils.quotedString(stereoType.getId()), ", ",
                    stereoType.isFabricated(), ", ", Utils.quotedString(stereoType.getName()), "))");
            codeSnippet.println();
        }
        if (metaType.getDocumentation() != null) {
            codeSnippet.printAll(".setDocumentation(", Utils.quotedString(metaType.getDocumentation().replaceAll("\"", "'").replaceAll("\r", "")
                    .replaceAll("\n", "")), ")");
            codeSnippet.println();
        }
    }

    public static class RuntimeMetaDataCreationContext {
        public CodeModel codeModel;
        public MetaModel metaModel;
        public CodeSnippet createPackageCode = new CodeSnippet();
        public CodeSnippet createClazzCode = new CodeSnippet();
        public CodeSnippet createAssociationCode = new CodeSnippet();
        public CodeSnippet establishClazzHierarchy = new CodeSnippet();
        public Map<MetaClazz, RuntimeMetaClazzCreationContext> clazzInitContexts = new HashMap<MetaClazz, RuntimeMetaClazzCreationContext>();

        public RuntimeMetaDataCreationContext(CodeModel codeModel, MetaModel metaModel) {
            super();
            this.codeModel = codeModel;
            this.metaModel = metaModel;
        }

        public boolean shouldGenerateFor(MetaPackage metaPackage) {
            try {
                codeModel.getByMetaTypeId(CodePackage.class, metaPackage.getId());
                return true;
            } catch (IllegalStateException e) {
                return false;
            }
        }

        public boolean shouldGenerateFor(MetaClazz metaClazz) {
            return JavaGeneratorContext.getContext().findClazzFromMetaClazz(metaClazz) != null
                    || JavaGeneratorContext.getContext().findInterfaceFromMetaClazz(metaClazz) != null;
        }

    }

    public static class RuntimeMetaClazzCreationContext {
        public RuntimeMetaType runtimeMetaType;
        public MetaClazz metaClazz;
        public CodeElement javaElement;
        public RuntimeMetaDataCreationContext rootContext;

        private CodeSnippet classPropertiesInitCode = new CodeSnippet();
        private CodeSnippet classOperationsInitCode = new CodeSnippet();

        public RuntimeMetaClazzCreationContext(CodeElement javaElement, MetaClazz metaType, RuntimeMetaDataCreationContext context) {
            super();
            this.javaElement = javaElement;
            this.metaClazz = metaType;
            this.rootContext = context;
        }
    }
}
