/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.codedom.GenericType;
import dk.tigerteam.trimm.mdsd.java.codedom.Interface;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaInterface;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * An {@link InterfaceEvent} corresponds to a {@link Interface}
 *
 * @author Jeppe Cramon
 */
public class InterfaceEvent extends GeneratorEvent {
    protected static final Log log = LogFactory.getLog(InterfaceEvent.class);

    private Interface _interface;
    private MetaInterface metaInterface;

    public InterfaceEvent(Interface _interface) {
        super();
        this._interface = _interface;
    }

    public MetaInterface getMetaInterface() {
        return metaInterface;
    }

    public Interface getInterface() {
        return _interface;
    }

    public Interface execute() {
        log.debug("Generating for Interface '" + _interface.getFQCN() + "'");

        metaInterface = _interface.getMetaType();
        _interface.addAccessModifiers(AccessModifier.Public);

        if (metaInterface.isTemplateParameterized()) {
            for (MetaClazz templateParameterClazz : metaInterface.getTemplateParameters()) {
                _interface.addGenerics(new GenericType(templateParameterClazz));
            }
            // Remove <...>
            int index = _interface.getName().indexOf('<');
            if (index != -1) {
                _interface.setName(_interface.getName().substring(0, index));
            }

        }

        // TODO: Handle inner classes
        broadcastEvent();
        return _interface;
    }
}
