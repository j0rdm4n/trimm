/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.codewriter;

import dk.tigerteam.trimm.mdsd.java.codedom.CodeWriter;

import java.util.ArrayList;
import java.util.List;

/**
 * Summary of the generation activity, used my {@link CodeWriter}'s to capture the overall result of a code generation process
 *
 * @author Lasse Cramon
 */
public class GeneratorSummary {

    public enum UpdateType {
        New, Update, NoUpdate
    }

    private static final String newLine = System.getProperty("line.separator");
    private int numberOfGeneratedArtifacts = 0;
    private int newBaseClasses = 0;
    private int updatedBaseClasses = 0;
    private int notUpdatedBaseClasses = 0;
    private int newEnumerationClasses = 0;
    private int updatedEnumerationClasses = 0;
    private int notUpdatedEnumerationClasses = 0;
    private int newInterfaceClasses = 0;
    private int updatedInterfaceClasses = 0;
    private int notUpdatedInterfaceClasses = 0;
    private int newExtensionClasses = 0;
    private int updatedExtensionClasses = 0;
    private int notUpdatedExtensionClasses = 0;
    private int newTestClasses = 0;
    private int updatedTestClasses = 0;
    private int notUpdatedTestClasses = 0;
    @SuppressWarnings("unused")
    private int numberOfDeletedBaseClasses = 0;
    @SuppressWarnings("unused")
    private int numberOfDeletedInterfaceClasses = 0;
    private int numberOfDontGenerateClasses = 0;
    private int total = 0;
    private List<String> newBaseClassesNames;
    private List<String> newExtensionClassesNames;
    private List<String> newEnumerationNames;
    private List<String> newInterfaceNames;
    private List<String> newTestClassNames;
    private List<String> updatedBaseClassesNames;
    private List<String> updatedExtensionClassesNames;
    private List<String> updatedEnumerationNames;
    private List<String> updatedInterfaceNames;
    private List<String> updatedTestClassNames;

    public void addToNumberOfUpdatedBaseClasses() {
        this.updatedBaseClasses++;
        this.total++;
    }

    public void addToNumberOfNotUpdatedBaseClasses() {
        this.notUpdatedBaseClasses++;
        this.total++;
    }

    public void addToNumberOfNewEnumerationClasses() {
        this.newEnumerationClasses++;
        this.total++;
    }

    public void addToNumberOfUpdatedEnumerationClasses() {
        this.updatedEnumerationClasses++;
        this.total++;
    }

    public void addToNumberOfNotUpdatedEnumerationClasses() {
        this.notUpdatedEnumerationClasses++;
        this.total++;
    }

    public void addToNumberOfUpdatedInterfaceClasses() {
        this.updatedInterfaceClasses++;
        this.total++;
    }

    public void addToNumberOfNotUpdatedInterfaceClasses() {
        this.notUpdatedInterfaceClasses++;
        this.total++;
    }

    public void addToNumberOfNewBaseClasses() {
        this.newBaseClasses++;
        this.total++;
    }

    public void addToNumberOfNewExtensionClasses() {
        this.newExtensionClasses++;
        this.total++;
    }

    public void addToNumberOfUpdatedExtensionClasses() {
        this.updatedExtensionClasses++;
        this.total++;
    }

    public void addToNumberOfNotUpdatedExtensionClasses() {
        this.notUpdatedExtensionClasses++;
        this.total++;
    }

    public void addToNumberOfNewTestClasses() {
        this.newTestClasses++;
        this.total++;
    }

    public void addToNumberOfUpdatedTestClasses() {
        this.updatedTestClasses++;
        this.total++;
    }

    public void addToNumberOfNotUpdatedTestClasses() {
        this.notUpdatedTestClasses++;
        this.total++;
    }

    public void addToNumberOfNewInterfaceClasses() {
        this.newInterfaceClasses++;
        this.total++;
    }

    public void addToNumberDeletedBaseClasses() {
        this.numberOfDeletedBaseClasses++;
    }

    public void addToNumberOfDeletedInterfaceClasses() {
        this.numberOfDeletedInterfaceClasses++;
    }

    public void addToNumberOfDontGenerateClasses() {
        this.numberOfDontGenerateClasses++;
        this.total++;
    }

    public void setNumberOfGeneratedArtifacts(int numberOfGeneratedArtifacts) {
        this.numberOfGeneratedArtifacts = numberOfGeneratedArtifacts;
    }

    public int getNumberOfGeneratedArtifacts() {
        return numberOfGeneratedArtifacts;
    }

    public int getNumberOfNotUpdatedInterfaceClasses() {
        return notUpdatedInterfaceClasses;
    }

    public void setNumberOfDeletedBaseClasses(int numberOfDeletedBaseClasses) {
        this.numberOfDeletedBaseClasses = numberOfDeletedBaseClasses;
    }

    public void setNumberOfDeletedInterfaceClasses(int numberOfDeletedInterfaceClasses) {
        this.numberOfDeletedInterfaceClasses = numberOfDeletedInterfaceClasses;
    }

    public List<String> getNewBaseClassesNames() {
        return newBaseClassesNames;
    }

    public List<String> getNewExtensionClassesNames() {
        return newExtensionClassesNames;
    }

    public List<String> getNewEnumerationNames() {
        return newEnumerationNames;
    }

    public List<String> getNewInterfaceNames() {
        return newInterfaceNames;
    }

    public List<String> getNewTestClassNames() {
        return newTestClassNames;
    }

    public List<String> getUpdatedBaseClassesNames() {
        return updatedBaseClassesNames;
    }

    public List<String> getUpdatedExtensionClassesNames() {
        return updatedExtensionClassesNames;
    }

    public List<String> getUpdatedEnumerationNames() {
        return updatedEnumerationNames;
    }

    public List<String> getUpdatedInterfaceNames() {
        return updatedInterfaceNames;
    }

    public void addNewBaseClassName(String className) {
        if (newBaseClassesNames == null) {
            newBaseClassesNames = new ArrayList<String>();
        }
        newBaseClassesNames.add(className);
    }

    public void addNewExtensionClassName(String className) {
        if (newExtensionClassesNames == null) {
            newExtensionClassesNames = new ArrayList<String>();
        }
        newExtensionClassesNames.add(className);
    }

    public void addNewEnumerationName(String className) {
        if (newEnumerationNames == null) {
            newEnumerationNames = new ArrayList<String>();
        }
        newEnumerationNames.add(className);
    }

    public void addNewInterfaceName(String className) {
        if (newInterfaceNames == null) {
            newInterfaceNames = new ArrayList<String>();
        }
        newInterfaceNames.add(className);
    }

    public void addNewTestClassName(String className) {
        if (newTestClassNames == null) {
            newTestClassNames = new ArrayList<String>();
        }
        newTestClassNames.add(className);
    }

    public void addUpdatedBaseClassName(String className) {
        if (updatedBaseClassesNames == null) {
            updatedBaseClassesNames = new ArrayList<String>();
        }
        updatedBaseClassesNames.add(className);
    }

    public void addUpdatedExtensionClassName(String className) {
        if (updatedExtensionClassesNames == null) {
            updatedExtensionClassesNames = new ArrayList<String>();
        }
        updatedExtensionClassesNames.add(className);
    }

    public void addUpdatedEnumerationName(String name) {
        if (updatedEnumerationNames == null) {
            updatedEnumerationNames = new ArrayList<String>();
        }
        updatedEnumerationNames.add(name);
    }

    public void addUpdatedInterfaceName(String name) {
        if (updatedInterfaceNames == null) {
            updatedInterfaceNames = new ArrayList<String>();
        }
        updatedInterfaceNames.add(name);
    }

    public void addUpdatedTestClassName(String name) {
        if (updatedTestClassNames == null) {
            updatedTestClassNames = new ArrayList<String>();
        }
        updatedTestClassNames.add(name);
    }

    public String printSummary() {
        StringBuilder builder = new StringBuilder();
        builder.append("######################################################").append(newLine);
        builder.append("Trimm Generator Summary                          #").append(newLine);
        builder.append("------------------------------------------------------").append(newLine);
        builder.append("Total number of artifacts from xmi:  " + numberOfGeneratedArtifacts).append(newLine);
        builder.append("------------------------------------------------------").append(newLine);
        builder.append("New Base classes:                    " + newBaseClasses).append(newLine);
        builder.append("New Extension classes:               " + newExtensionClasses).append(newLine);
        builder.append("New Enumeration classes:             " + newEnumerationClasses).append(newLine);
        builder.append("New Test classes:                    " + newTestClasses).append(newLine);
        builder.append("New Interfaces:                      " + newInterfaceClasses).append(newLine);
        builder.append("Updated Base classes:                " + updatedBaseClasses).append(newLine);
        builder.append("Updated Extension classes:           " + updatedExtensionClasses).append(newLine);
        builder.append("Updated Enumeration classes:         " + updatedEnumerationClasses).append(newLine);
        builder.append("Updated Test classes:                " + updatedTestClasses).append(newLine);
        builder.append("Updated Interfaces:                  " + updatedInterfaceClasses).append(newLine);
        builder.append("Not updated Base classes:            " + notUpdatedBaseClasses).append(newLine);
        builder.append("Not updated Extension classes:       " + notUpdatedExtensionClasses).append(newLine);
        builder.append("Not updated Enumeration classes:     " + notUpdatedEnumerationClasses).append(newLine);
        builder.append("Not updated Test classes:            " + notUpdatedTestClasses).append(newLine);
        builder.append("Not updated Interfaces:              " + notUpdatedInterfaceClasses).append(newLine);
        builder.append("Number of not generated artifacts:   " + numberOfDontGenerateClasses).append(newLine);
        // builder.append("Deleted Base classes:             " + ).append(newLine);
        // builder.append("Deleted Interfaces:               " + ).append(newLine);
        builder.append("------------------------------------------------------").append(newLine);
        builder.append("Number of artifacts processed:       " + total).append(newLine);
        builder.append("------------------------------------------------------").append(newLine);
        builder.append("######################################################").append(newLine);
        printClassNames(builder, newBaseClassesNames, "# New Base classes: ");
        printClassNames(builder, newExtensionClassesNames, "# New Extension classes: ");
        printClassNames(builder, newEnumerationNames, "# New Enumeration classes: ");
        printClassNames(builder, newTestClassNames, "# New Test classes: ");
        printClassNames(builder, newInterfaceNames, "# New Interface classes: ");
        printClassNames(builder, updatedBaseClassesNames, "# Updated Base classes: ");
        printClassNames(builder, updatedExtensionClassesNames, "# Updated Extension classes: ");
        printClassNames(builder, updatedEnumerationNames, "# Updated Enumeration classes: ");
        printClassNames(builder, updatedTestClassNames, "# Updated Test classes: ");
        printClassNames(builder, updatedInterfaceNames, "# Updated Interface classes: ");
        return builder.toString();
    }

    private void printClassNames(StringBuilder builder, List<String> classNames, String text) {
        if (classNames != null && !classNames.isEmpty()) {
            builder.append(text).append(newLine);
            int count = 0;
            int lastLineChange = 0;
            for (String className : classNames) {
                if (count++ > 0) builder.append(", ");
                builder.append(className);
                if (count - lastLineChange == 5) {
                    lastLineChange = count;
                    builder.append(newLine);
                }
            }
            builder.append(newLine);
        }
    }

}
