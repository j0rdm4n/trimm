/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.Property;
import dk.tigerteam.trimm.mdsd.java.generator.CollectionTypeResolver;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import dk.tigerteam.trimm.util.Utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Base class for all {@link Property} related events.<br/>
 * An important thing to remember is that properties have cardinality, which can be read through {@link MetaProperty#getCardinalityType()},
 * {@link MetaProperty#getMinCardinality()} and {@link MetaProperty#getMaxCardinality()}.
 * <p/>
 * Therefore we work with non pluralized and pluralized field names (and thereby also property names since they typically reflect the field
 * name), to make this fact clearer to the developer using the generated Java code (apart from the fact that they in case of
 * {@link CardinalityType#Many} deal with a Collection type).<br/>
 * To control field names override {@link #resolveJavaFieldNameFromMetaProperty(MetaProperty)} and
 * {@link #resolveNonPluralisedJavaFieldNameForMetaProperty(MetaProperty)}
 * <p/>
 *
 * @author Jeppe Cramon
 * @see {@link Property} for a description of the design reasons for this construction.
 */
public abstract class PropertyEvent extends GeneratorEvent {
    private Property property;
    private MetaProperty metaProperty;
    private ClazzOrInterface clazz;
    private boolean createFieldForProperty = true;
    private boolean createAccessorMethodsForProperty = true;

    public PropertyEvent() {
        super();
    }

    /**
     * @param clazz        The {@link Clazz} that this property belongs to
     * @param metaProperty The {@link MetaProperty} that is the basis for the {@link #getProperty()} instance and that we are processing
     */
    public PropertyEvent(ClazzOrInterface clazz, MetaProperty metaProperty) {
        super();
        this.clazz = clazz;
        this.metaProperty = metaProperty;
    }

    /**
     * Should we create a {@link dk.tigerteam.trimm.mdsd.java.codedom.Field} for the property - default is <code>true</code>
     *
     * @return value
     */
    public boolean isCreateFieldForProperty() {
        return createFieldForProperty;
    }

    /**
     * Should we per default create a {@link dk.tigerteam.trimm.mdsd.java.codedom.Field} for the property - default is <code>true</code>
     *
     * @param createFieldForProperty value
     */
    public void setCreateFieldForProperty(boolean createFieldForProperty) {
        this.createFieldForProperty = createFieldForProperty;
    }

    /**
     * Should we per default create a {@link dk.tigerteam.trimm.mdsd.java.codedom.GetterMethod} and {@link dk.tigerteam.trimm.mdsd.java.codedom.SetterMethod} for the property - default is <code>true</code><p/>
     * <i>Note: the standard rules by disallowing setter method for readonly properties still apply, even if this is property is set to true</i>
     *
     * @return value
     */
    public boolean isCreateAccessorMethodsForProperty() {
        return createAccessorMethodsForProperty;
    }

    /**
     * Should we per default create a {@link dk.tigerteam.trimm.mdsd.java.codedom.GetterMethod} and {@link dk.tigerteam.trimm.mdsd.java.codedom.SetterMethod} for the property - default is <code>true</code><p/>
     * <i>Note: the standard rules by disallowing setter method for readonly properties still apply, even if this is property is set to true</i>
     *
     * @param createAccessorMethodsForProperty
     *         value
     */
    public void setCreateAccessorMethodsForProperty(boolean createAccessorMethodsForProperty) {
        this.createAccessorMethodsForProperty = createAccessorMethodsForProperty;
    }

    /**
     * The {@link Clazz} that this property belongs to
     *
     * @return
     */
    public ClazzOrInterface getClazzOrInterface() {
        return clazz;
    }

    /**
     * The {@link Clazz} that this property belongs to
     *
     * @return
     */
    public Clazz getClazz() {
        if (clazz.isClazz()) {
            return (Clazz) clazz;
        } else {
            throw new IllegalStateException("Can't cast internal ClazzOrInterface to Clazz, because it's of type '" + clazz.getClass().getName()
                    + "' - Details: " + clazz);
        }
    }

    /**
     * The {@link Property} instance that this property event is responsible for handling/creating
     */
    public Property getProperty() {
        return property;
    }

    /**
     * The {@link Property} instance that this property event is responsible for handling/creating
     */
    public void setProperty(Property property) {
        this.property = property;
    }

    /**
     * The {@link MetaProperty} that is the basis for the {@link #getProperty()} instance
     */
    public MetaProperty getMetaProperty() {
        return metaProperty;
    }

    /**
     * The {@link MetaProperty} that is the basis for the {@link #getProperty()} instance
     */
    public void setMetaProperty(MetaProperty metaProperty) {
        this.metaProperty = metaProperty;
    }

    /**
     * Is this property readable (sugar for {@link MetaProperty#isReadable()})
     */
    protected boolean isReadable() {
        return metaProperty.isReadable();
    }

    /**
     * Is this property writable (sugar for {@link MetaProperty#isWriteable()})
     *
     * @return
     */
    protected boolean isWriteable() {
        return metaProperty.isWriteable();
    }

    /**
     * Resolves the name of the Java field from the name of the MetaProperty parameter while taking cardinality into consideration.<br/>
     * The naming rule follows that of {@link #resolveNonPluralisedJavaFieldNameForMetaProperty(MetaProperty)}, but also appends a
     * "Collection" string to the end of the name if the {@link MetaProperty#getCardinalityType()} is {@link CardinalityType#Many}
     *
     * @param metaProperty The {@link MetaProperty} to resolve a Java field name for
     * @return The Java field name for the {@link MetaProperty} parameter
     */
    public String resolveJavaFieldNameFromMetaProperty(MetaProperty metaProperty) {
        String propertyName = resolveNonPluralisedJavaFieldNameForMetaProperty(metaProperty);
        if (metaProperty.getCardinalityType() == CardinalityType.Many) {
            propertyName += JavaGeneratorContext.getContext().getCollectionPropertyNameSuffix();
        }
        return propertyName;
    }

    /**
     * Resolves the name of the Java field from the name of the MetaProperty parameter.<br/>
     * This method doesn't concern it self with the {@link MetaProperty#getCardinalityType()} value (in case you need this use
     * {@link #resolveJavaFieldNameFromMetaProperty(MetaProperty)})
     * <p/>
     * If the {@link MetaProperty} has a <b>name</b>, then this is used.<br/>
     * If the name is missing, the metaProperties <b>type</b>'s name is used instead.<br/>
     * Note: To ensure proper java code, this method ensures that the first letter of the name is uncapitalized.
     *
     * @param metaProperty The {@link MetaProperty} to resolve a Java field name for
     * @return The Java field name for the {@link MetaProperty} parameter
     */
    public String resolveNonPluralisedJavaFieldNameForMetaProperty(MetaProperty metaProperty) {
        String propertyName = metaProperty.getName();
        if (propertyName != null && propertyName.trim().length() > 0) {
            propertyName = Utils.uncapitalize(propertyName);
        } else {
            if (metaProperty.getType() == null)
                throw new IllegalStateException("Property " + metaProperty + " doesn't have a type or a name");
            propertyName = Utils.uncapitalize(metaProperty.getType().getName());
        }
        return propertyName;
    }

    /**
     * Resolve the {@link MetaProperty} <b>name</b>.If it's null, then we'll use the {@link MetaProperty}'s <b>type</b> as name
     *
     * @return
     */
    public String getResolvedJavaFieldName() {
        return resolveJavaFieldNameFromMetaProperty(metaProperty);
    }

    /**
     * Resolve what type of {@link Collection} to use for the Properties field definition (for fields with {@link CardinalityType#Many}).<br/>
     * This is a shorthand for calling {@link JavaGeneratorContext#getCollectionTypeResolver()} and
     * {@link CollectionTypeResolver#resolveCollectionDefinitionType(PropertyEvent)}
     * <p/>
     * Example: a {@link MetaProperty} has {@link CardinalityType#Many} and is of type {@link String}. If
     * {@link CollectionTypeResolver#resolveCollectionDefinitionType(PropertyEvent)} returns {@link Set} and
     * {@link CollectionTypeResolver#resolveCollectionInstanceType(PropertyEvent)} returns {@link HashSet}, then the complete field definition
     * will look like this: <code>private Set&lt;String> fieldName = new HashSet&lt;String>();</code>.
     *
     * @return The type of {@link Collection} to use for the definition of the field.
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveCollectionDefinitionType() {
        return JavaGeneratorContext.getContext().getCollectionTypeResolver().resolveCollectionDefinitionType(this);
    }

    /**
     * Resolve what type of {@link Collection} to use for the Properties field initialization (for fields with {@link CardinalityType#Many}).<br/>
     * This is a shorthand for calling {@link JavaGeneratorContext#getCollectionTypeResolver()} and
     * {@link CollectionTypeResolver#resolveCollectionInstanceType(PropertyEvent)}
     * <p/>
     * Example: a {@link MetaProperty} has {@link CardinalityType#Many} and is of type {@link String}. If
     * {@link CollectionTypeResolver#resolveCollectionDefinitionType(PropertyEvent)} returns {@link Set} and
     * {@link CollectionTypeResolver#resolveCollectionInstanceType(PropertyEvent)} returns {@link HashSet}, then the complete field definition
     * will look like this: <code>private Set&lt;String> fieldName = new HashSet&lt;String>();</code>.
     *
     * @return The type of {@link Collection} to use for the initialization of the field.
     */
    @SuppressWarnings("rawtypes")
    protected Class<? extends Collection> resolveCollectionInstanceType() {
        return JavaGeneratorContext.getContext().getCollectionTypeResolver().resolveCollectionInstanceType(this);
    }
}
