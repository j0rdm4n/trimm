/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.event;

import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.extension.SerialVersionUIDGeneratorListener;

/**
 * A deferred event handler refers to a part of a typical EventListener code, which cannot run at the time it was called, typically because
 * not all information required to handle the event is available<br/>
 * The {@link DeferredEventHandler}'s are queued in the {@link JavaGeneratorContext} for later execution by calling
 * {@link JavaGeneratorContext#queueDeferredEventHandler(DeferredEventHandler)}.<br/> {@link DeferredEventHandler}'s are executed <b>after</b>
 * all Clazz's have completed 2. stage of the generation (meaning they should have all their properties, fields, methods, etc. defined).
 * <p/>
 * Examples of when {@link DeferredEventHandler} makes senses:
 * <ul>
 * <li>In {@link SerialVersionUIDGeneratorListener}. This EventListener listens for {@link ClazzEvent}' because they denote when a new Clazz
 * is being processed (and SerialVersionUID relates to the Clazz). But because none of the Clazz' Fields (and thereby Properties) have been
 * proessed yet (and we need to have all Fields defined as the UID is calculated based on the Fields in the Clazz), we need to defer the
 * SerialUID calculation and generation until the Clazz is done being processed (and thereby also all other Clazz' are done).</li>
 * <li>{@link JPANamedTablesAndColumnsListener} uses {@link DeferredEventHandler} to handle {@link AttributeOverride} and
 * {@link AssociationOverride} since to properly deduce these, all Clazz' and their properties basically needs to be processed.
 * </ul>
 *
 * @author Jeppe Cramon
 */
public interface DeferredEventHandler {
    /**
     * Execute the deferred event handling code
     */
    void execute();

    /**
     * Description of what the deferred event handler instance is doing (for better error reporting)
     */
    String getDescription();
}
