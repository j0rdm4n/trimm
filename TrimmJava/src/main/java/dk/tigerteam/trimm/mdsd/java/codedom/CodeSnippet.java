/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.meta.MetaType;

import java.util.LinkedList;
import java.util.List;

/**
 * Models a CodeSnippet, which can contain arbitrary {@link CodeElement}' or Text/Strings.<p/>
 * Example:
 * <pre>
 * 		CodeSnippet snippet = new CodeSnippet();
 * 		snippet.print("return new ").print(new Type(ManyToManySetWrapper.class).addGenerics(thisClazzType.clone(), propertyType.clone())).print("(this, ").print(fieldName).println(") {");
 * 		snippet.println("@Override");
 * 		snippet.print("protected ").print(new Type(propertyCollectionType.getName()).addGenerics(thisClazzType.clone())).print(" getSourceCollectionInTarget(").print(propertyType).println(" o) {");
 * 		snippet.print("return o.get").print(oppositePropertyCapitalizedName).println("();");
 * 		snippet.println("}");
 * 		snippet.println("};");
 *
 * 		event.getProperty().getGetterMethod().getChildrenAsXmiElements().clear();
 * 		event.getProperty().getGetterMethod().getChildrenAsXmiElements().add(snippet);
 * </pre>
 *
 * @author Jeppe Cramon
 */
public class CodeSnippet extends CodeElement {
    private List<Object> snippets = new LinkedList<Object>();

    public CodeSnippet() {
        super();
    }

    /**
     * Prints every object (supports String, {@link Class}, {@link CodeElement} and for the rest it performs {@link Object#toString()})
     */
    public CodeSnippet(Object... snippets) {
        super();
        printAll(snippets);
    }

    /**
     * Println's every string
     */
    public CodeSnippet(String... snippets) {
        super();
        printlnAll((Object[]) snippets);
    }

    /**
     * Print's all arguments (supports String, {@link Class}, {@link CodeElement} and for the rest it performs {@link Object#toString()})
     */
    public CodeSnippet printAll(Object... snippets) {
        for (Object object : snippets) {
            print(object);
        }
        return this;
    }

    /**
     * (supports String, {@link Class}, {@link CodeElement} and for the rest it performs {@link Object#toString()})
     */
    public CodeSnippet print(Object object) {
        if (object != null) {
            if (object instanceof Long || object.getClass().equals(long.class)) {
                print(object.toString() + "L");
            } else if (object instanceof Double || object.getClass().equals(double.class)) {
                print(object.toString() + "D");
            } else if (object instanceof Float || object.getClass().equals(float.class)) {
                print(object.toString() + "F");
            } else if (object instanceof CodeElement) {
                print((CodeElement) object);
            } else if (object instanceof Class) {
                print((Class<?>) object);
            } else {
                print(object.toString());
            }
        } else {
            print("null");
        }
        return this;
    }

    /**
     * (supports String, {@link Class}, {@link CodeElement} and for the rest it performs {@link Object#toString()})
     */
    public CodeSnippet println(Object object) {
        if (object != null) {
            if (object instanceof Long || object.getClass().equals(long.class)) {
                println(object.toString() + "L");
            } else if (object instanceof Double || object.getClass().equals(double.class)) {
                println(object.toString() + "D");
            } else if (object instanceof Float || object.getClass().equals(float.class)) {
                println(object.toString() + "F");
            } else if (object instanceof CodeElement) {
                println((CodeElement) object);
            } else if (object instanceof Class) {
                println((Class<?>) object);
            } else {
                print(object.toString());
            }
        } else {
            print("null");
        }
        return this;
    }

    /**
     * Println's all arguments (supports String, {@link Class}, {@link CodeElement} and for the rest it performs {@link Object#toString()})
     */
    public CodeSnippet printlnAll(Object... snippets) {
        for (Object object : snippets) {
            println(object);
        }
        return this;
    }

    /**
     * Print all arguments (supports String, {@link Class}, {@link CodeElement} and for the rest it performs {@link Object#toString()}) with a
     * final println()
     */
    public CodeSnippet printAllLn(Object... snippets) {
        for (Object object : snippets) {
            print(object);
        }
        println();
        return this;
    }

    public CodeSnippet print(String text) {
        snippets.add(text);
        return this;
    }

    public CodeSnippet println() {
        snippets.add(MetaType.newline);
        return this;
    }

    public CodeSnippet println(String text) {
        snippets.add(text);
        snippets.add(MetaType.newline);
        return this;
    }

    public CodeSnippet print(CodeElement element) {
        snippets.add(element);
        return this;
    }

    public CodeSnippet print(Class<?> clazz) {
        print(clazz.getName().replace('$', '.'));
        return this;
    }

    public CodeSnippet println(Class<?> clazz) {
        println(clazz.getName().replace('$', '.'));
        return this;
    }

    public CodeSnippet println(CodeElement element) {
        snippets.add(element);
        snippets.add(MetaType.newline);
        return this;
    }

    /**
     * Helper method to insert quotes (") around the text
     *
     * @param stringtoQuote The text to insert quotes around
     * @return The quoted string
     */
    public static String quote(String stringtoQuote) {
        StringBuilder builder = new StringBuilder("\"");
        builder.append(stringtoQuote);
        builder.append("\"");
        return builder.toString();
    }

    /**
     * Get the internal snippets of this snippet
     */
    public List<Object> getSnippets() {
        return snippets;
    }

    @Override
    public String toString() {
        return snippets.toString();
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
