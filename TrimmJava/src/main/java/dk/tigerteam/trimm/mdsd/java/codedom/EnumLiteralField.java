/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

/**
 * Specialized Field for rendering Enumerator Literals correctly when they have initializer statement
 *
 * @author Jeppe Cramon
 */
public class EnumLiteralField extends Field {
    public EnumLiteralField(AccessModifier... accessModifiers) {
        super(accessModifiers);
    }

    public EnumLiteralField(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
    }

    public EnumLiteralField(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

    public EnumLiteralField(String name, Clazz owner, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, owner, fabricated, accessModifiers);
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		if (getClazzOrInterfaceOwner().isInterface()) {
//			return;
//		}
//
//		writer.printJavaDoc(this);
//		writer.printAnnotations(this);
//
//		writer.print(" ");
//		writer.print(getName());
//		if (getInitializerStatement() != null) {
//			writer.print("(");
//			getInitializerStatement().generateCode(writer);
//			writer.print(")");
//		}
//	}

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
