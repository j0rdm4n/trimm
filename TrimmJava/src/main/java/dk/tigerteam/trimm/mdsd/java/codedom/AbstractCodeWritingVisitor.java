/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.java.generator.codewriter.PackageRewriter;
import dk.tigerteam.trimm.mdsd.meta.MetaType;

import java.util.Collection;

/**
 * A {@link CodeVisitor} that support writing code, plus optionally supports package name rewriting
 *
 * @author Lasse Cramon
 * @author Jeppe Cramon
 */
public abstract class AbstractCodeWritingVisitor implements CodeVisitor {

    protected CodeWriter writer;
    private PackageRewriter packageRewriter;
    private boolean useImports;

    protected AbstractCodeWritingVisitor(CodeWriter writer, PackageRewriter packageRewriter, boolean useImports) {
        this.writer = writer;
        this.packageRewriter = packageRewriter;
        this.useImports = useImports;
    }

    public CodeWriter getCodeWriter() {
        return writer;
    }

    public PackageRewriter getPackageRewriter() {
        return packageRewriter;
    }

    public boolean hasPackageRewriter() {
        return packageRewriter != null;
    }

    public boolean isUseImports() {
        return useImports;
    }

    // ----------------------------------- Helper methods ----------------------------

    /**
     * Visit and write out the elements comma separated
     *
     * @param elements the elements to visit and write out
     */
    protected CodeWriter printCodeElementCollection(Collection<? extends CodeElement> elements) {
        int count = 0;
        for (CodeElement element : elements) {
            if (count > 0) {
                writer.print(", ");
            }
            element.visit(this);
            count++;
        }
        return writer;
    }

    /**
     * Print out all the elements {@link dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier}' in order they were specified
     *
     * @param element The element to write out the {@link CodeElement#getAccessModifiers()} for
     * @return This {@link CodeWriter} instance
     */
    protected CodeWriter printAccessModifiers(CodeElement element) {
        if (element.getOwner() instanceof Interface) return writer;

        for (CodeElement.AccessModifier modifier : element.getAccessModifiers()) {
            if (modifier != CodeElement.AccessModifier.PackagePrivate)
                writer.print(modifier.name().toLowerCase()).print(" ");
        }
        return writer;
    }

    /**
     * Print out all the elements {@link Annotation}'
     *
     * @param element The element to write out the {@link CodeElement#getAnnotations()} for
     * @return This {@link CodeWriter} instance
     */
    protected CodeWriter printAnnotations(CodeElement element) {
        for (Annotation annotation : element.getAnnotations()) {
            annotation.visit(this);
            writer.println();
        }
        return writer;
    }

    /**
     * Print out the elements java doc using standard JavaDoc style comments
     *
     * @param element The element to write java doc for
     * @return This {@link CodeWriter} instance
     */
    protected CodeWriter printJavaDoc(CodeElement element) {
        if (element.getCodeDocumentation() == null) {
            return writer;
        }
        writer.println("/**");
        for (String docLine : element.getCodeDocumentation().split(MetaType.newline)) {
            writer.print(" * ");
            writer.println(docLine);
        }
        writer.println(" */");
        return writer;
    }

    protected void writeMethodBodyContent(Method method) {
        if (method instanceof GetterMethod && method.getChildren().isEmpty()) {
            // Write standard Getter method body content
            writer.print("return ").print(method.getOwner().getName()).println(";");
        } else if (method instanceof SetterMethod && method.getChildren().isEmpty()) {
            // Write standard Setter method body content
            writer.print("this.").print(method.getOwner().getName()).print(" = ").print(SetterMethod.DEFAULT_PARAMETER_NAME).println(";");
        } else {
            for (CodeElement child : method.getChildren()) {
                child.visit(this);
            }
        }
    }

    // ---------------------------------------------------------------------------------------------------------
    @Override
    public void handle(CodeSnippet codeSnippet) {
        for (Object snippet : codeSnippet.getSnippets()) {
            if (snippet instanceof CodeElement) {
                ((CodeElement) snippet).visit(this);
            } else {
                writer.print(snippet.toString());
            }
        }
    }

}
