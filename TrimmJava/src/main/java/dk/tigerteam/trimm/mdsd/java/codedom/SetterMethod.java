/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.codedom;

/**
 * Represents a Setter property method.
 * <p/>
 * It's name is automatically set, when it's added to a {@link Property} object (you <u>can</u> set the name, but it's not necessary and
 * therefore the default name constructor is left out)
 * <p/>
 * If no parameter is provided in the Setter method, then the standard {@link Property} implementation will provide a default argument of
 * the same type as the property field and with the value of {@link #DEFAULT_PARAMETER_NAME}
 * <p/>
 * Unless you change it, then the default return type is set to Type.VOID.
 *
 * @author Jeppe Cramon
 */
public class SetterMethod extends Method {

    public static final String DEFAULT_PARAMETER_NAME = "parameter";

    public SetterMethod(AccessModifier... accessModifiers) {
        super(accessModifiers);
        setReturnType(Type.VOID);
    }

    public SetterMethod(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
        setReturnType(Type.VOID);
    }

    public SetterMethod(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
        setReturnType(Type.VOID);
    }

    @Override
    public Property getOwner() {
        return (Property) super.getOwner();
    }

//	@Override
//	protected void writeMethodBodyContent(CodeWriter writer) {
//		if (getChildrenAsXmiElements().isEmpty()) {
//			// Write standard Setter method body content
//			writer.print("this.").print(getOwner().getName()).print(" = ").print(SetterMethod.DEFAULT_PARAMETER_NAME).println(";");
//		} else {
//			super.writeMethodBodyContent(writer);
//		}
//	}

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
