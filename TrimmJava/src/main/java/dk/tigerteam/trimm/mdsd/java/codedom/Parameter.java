/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.meta.MetaParameter;

/**
 * Model a method definition parameter (supports {@link #variableArgument} parameters).<br/>
 * Example:
 * <p/>
 * <pre>
 * SetterMethod setter = new SetterMethod();
 * ...
 * setter.addParameters(new Parameter(SetterMethod.DEFAULT_PARAMETER_NAME)
 * 					.setType(field.getType()));
 * </pre>
 *
 * @author Jeppe Cramon
 */
public class Parameter extends CodeElement {
    private Type type;
    private boolean variableArgument = false;
    /**
     * Only applies to languages that support default values for parameters<p/>
     * Rules for when writing out default values:
     * <ul>
     * <li>An instance of {@link Enum} - writes out the FQCN name for the Enum class + "." + the enum instances <code>.name()</code> value</li>
     * <li>An instance of a String - will be written out in Quotes " "</li>
     * <li>An instance of anything else - will be written out using the values <code>toString()</code> method</li>
     * </ul>
     */
    private Object defaultValue;

    public Parameter() {
        super();
    }

    public Parameter(String name) {
        super(name);
    }

    public Parameter(String name, boolean variableArgument) {
        super(name);
        setVariableArgument(variableArgument);
    }

    public Parameter(String name, Type type) {
        super(name);
        setType(type);
    }

    public Parameter(String name, Type type, boolean variableArgument) {
        super(name);
        setType(type);
        setVariableArgument(variableArgument);
    }

    public Parameter(String name, ClazzOrInterface type) {
        super(name);
        setType(new Type(type));
    }

    public Parameter(String name, ClazzOrInterface type, boolean variableArgument) {
        super(name);
        setType(new Type(type));
        setVariableArgument(variableArgument);
    }

    public Parameter(String name, Class<?> type) {
        super(name);
        setType(new Type(type));
    }

    public Parameter(String name, Class<?> type, boolean variableArgument) {
        super(name);
        setType(new Type(type));
        setVariableArgument(variableArgument);
    }

    @Override
    public MetaParameter getMetaType() {
        return (MetaParameter) super.getMetaType();
    }

    public Type getType() {
        return type;
    }

    public Parameter setType(Type type) {
        this.type = type;
        return this;
    }

    /**
     * Used for Dynamically types languages, like Groovy, where fields, variables, properties, parameters, etc. doesn't need types
     *
     * @return true if no {@link #getType()} has been specified
     */
    public boolean isDynamicallyTyped() {
        return type == null;
    }


    public boolean isVariableArgument() {
        return variableArgument;
    }

    public Parameter setVariableArgument(boolean variableArgument) {
        this.variableArgument = variableArgument;
        return this;
    }

    /**
     * Only applies to languages that support default values for parameters<p/>
     * Rules for when writing out default values:
     * <ul>
     * <li>An instance of {@link Enum} - writes out the FQCN name for the Enum class + "." + the enum instances <code>.name()</code> value</li>
     * <li>An instance of a String - will be written out in Quotes " "</li>
     * <li>An instance of anything else - will be written out using the values <code>toString()</code> method</li>
     * </ul>
     *
     * @return
     */
    public Object getDefaultValue() {
        return defaultValue;
    }

    /**
     * Only applies to languages that support default values for parameters<p/>
     * Rules for when writing out default values:
     * <ul>
     * <li>An instance of {@link Enum} - writes out the FQCN name for the Enum class + "." + the enum instances <code>.name()</code> value</li>
     * <li>An instance of a String - will be written out in Quotes " "</li>
     * <li>An instance of anything else - will be written out using the values <code>toString()</code> method</li>
     * </ul>
     *
     * @param defaultValue
     */
    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }

    //	@Override
//	public void generateCode(CodeWriter writer) {
//		type.generateCode(writer);
//		if (variableArgument) {
//			writer.print("...");
//		} else {
//			writer.print(" ");
//		}
//		writer.print(getName());
//	}

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + (variableArgument ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Parameter other = (Parameter) obj;
        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (variableArgument != other.variableArgument)
            return false;
        return true;
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }


    @Override
    public String toString() {
        return "Parameter{" +
                "name=" + getName() +
                ", type=" + type +
                ", variableArgument=" + variableArgument +
                ", defaultValue=" + defaultValue +
                '}';
    }
}
