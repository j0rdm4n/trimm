/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import dk.tigerteam.trimm.mdsd.meta.MetaType;
import dk.tigerteam.trimm.util.Utils;

import java.util.Arrays;
import java.util.List;

/**
 * Generates setter property sugar accessors called "with" or "addTo" which support fluent interface style programming (where several methods can be
 * chained together without having to insert semicolons).
 * These with/addTo method takes the value to set as an argument and returns the current object (the object instance that the property is defined within).
 * <p/>
 * Example:
 * <p/>
 * <pre>
 * public class Consultation {
 *  private org.joda.time.DateTime consultationDate;
 *
 *  public org.joda.time.DateTime getConsultationDate() {
 *      return consultationDate;
 *  }
 *
 *  public void setConsultationDate(org.joda.time.DateTime parameter) {
 *      this.consultationDate = parameter;
 *  }
 *
 *  public Consultation withConsultationDate(org.joda.time.DateTime consultationDate) {
 *      setConsultationDate(consultationDate);
 *      return (Consultation) this;
 *  }
 *  --------------------------------------------------------------------------------------------------
 *  private java.util.Set<Prescription> prescriptionCollection = new java.util.HashSet<Prescription>();
 *  public java.util.Set<Prescription> getPrescriptionCollection() {
 *      return prescriptionCollection;
 *  }
 *
 *  public void setPrescriptionCollection(java.util.Set<Prescription> parameter) {
 *      this.prescriptionCollection = parameter;
 *  }
 *  public Consultation addToPrescriptionCollection(Prescription... values) {
 *    getPrescriptionCollection().addAll(java.util.Arrays.asList(values));
 *    return (Consultation) this;
 *  }
 * }
 * </pre>
 *
 * @author Jeppe Cramon
 */
public class PropertySugarMethodsEventListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

    /**
     * Generated setter property sugar accessors called "with" which support fluent interface style programming (where several methods can be
     * chained). These with method takes the value to set as an argument and returns as value the current object within which the property
     * lives.
     * <p/>
     * Example:
     * <p/>
     * <pre>
     * public class Brugerformular {
     *  private java.util.Date gyldigTil;
     *  public java.util.Date getGyldigTil() {
     *    return gyldigTil;
     *  }
     *
     *  public void setGyldigTil(java.util.Date parameter) {
     *      this.gyldigTil = parameter;
     *  }
     *
     *  public Brugerformular withGyldigTil(java.util.Date value) {
     *      setGyldigTil(value);
     *      return this;
     *  }
     *  ....
     * }
     * </pre>
     */
    public PropertySugarMethodsEventListener() {
    }

    @Override
    protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
        if (event.getMetaProperty().getCardinalityType() == CardinalityType.Many) {
            return internalHandleToManyAssociationEvent(event);
        } else {
            return internalHandleSimpleOrToOneAssociationEvent(event);
        }
    }

    @Override
    protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
        return internalHandleSimpleOrToOneAssociationEvent(event);
    }

    @Override
    protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
        return internalHandleSimpleOrToOneAssociationEvent(event);
    }

    @Override
    protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
        return internalHandleToManyAssociationEvent(event);
    }

    @Override
    protected boolean handleManyToManyAssociationEvent(ManyToManyAssociationEvent event) {
        return internalHandleToManyAssociationEvent(event);
    }

    protected boolean internalHandleToManyAssociationEvent(PropertyEvent event) {
        if (event.getClazzOrInterface().isInterface()) {
            return true;
        }
        if (event.getProperty().getGetterMethod() != null) {
            StringBuilder requiredPropertiesDoc = getContext().getRequiredPropertiesJavaDoc(event.getClazz());

            String javaDoc = "Fluent interface sugar<p/>"
                    + MetaType.newline
                    + (event.getProperty().getGetterMethod().getCodeDocumentation() != null ? (" - " + event.getProperty().getGetterMethod().getCodeDocumentation()) : "");
            javaDoc += "<p/>" + MetaType.newline + "Defined in " + event.getClazz().getName() + "<p/>" + MetaType.newline;
            if (event.getProperty().getField().getType().getGenerics().get(0).getClazz() != null) {
                javaDoc += "Property type hierarchy:"
                        + MetaType.newline
                        + JavaGeneratorContext.getContext()
                        .getHierarchyAsHtml(event.getProperty().getField().getType().getGenerics().get(0).getClazz()) + "<p/>" + MetaType.newline;
                ;
            }
            Type returnType = new Type(event.getClazz().getMetaType());
            Method addToMethod = (Method) new Method("addTo" + Utils.capitalize(event.getProperty().getField().getName()), true)
                    .addParameters(new Parameter("values", true).setType(event.getProperty().getField().getType().getGenerics().get(0).clone()))
                    .setReturnType(returnType)
                            // Handles Base/Extension class' for us
                    .addCode(
                            new CodeSnippet(event.getProperty().getGetterMethod().getName() + "().addAll(", new Type(Arrays.class, true), ".asList(values));", "return (",
                                    returnType, ") this;")).addAccessModifiers(AccessModifier.Public).setCodeDocumentation(javaDoc);

            event.getClazz().addChildren(addToMethod);

            createOverridingMethodsWithCovariantReturnTypesForAllSubclazzes(event, addToMethod);

            // Finally add required properties info (must be added afterwards to avoid adding it to overriding methods in subclasses)
            addToMethod.setCodeDocumentation(addToMethod.getCodeDocumentation() + "<p/>" + MetaType.newline + requiredPropertiesDoc.toString());

        }
        return true;
    }

    protected boolean internalHandleSimpleOrToOneAssociationEvent(PropertyEvent event) {
        if (event.getClazzOrInterface().isInterface()) {
            return true;
        }
        if (event.getProperty().getSetterMethod() != null) {
            // Use the Setter method to ensure we don't run into problems with Bidirectional wrappers
            StringBuilder requiredPropertiesDoc = getContext().getRequiredPropertiesJavaDoc(event.getClazz());
            String javaDoc = "Fluent interface sugar<p/>"
                    + MetaType.newline
                    + (event.getProperty().getSetterMethod().getCodeDocumentation() != null ? (" - " + event.getProperty().getSetterMethod().getCodeDocumentation()) : "");
            javaDoc += "<p/>" + MetaType.newline + "Defined in " + event.getClazz().getName() + "<p/>" + MetaType.newline;
            if (event.getProperty().getField().getType().getClazz() != null) {
                javaDoc += "Property type hierarchy:" + MetaType.newline
                        + JavaGeneratorContext.getContext().getHierarchyAsHtml(event.getProperty().getField().getType().getClazz()) + "<p/>"
                        + MetaType.newline;
                ;
            }

            Type returnType = new Type(event.getClazz().getMetaType());
            Method withMethod = (Method) new Method("with" + Utils.capitalize(event.getProperty().getField().getName()), true)
                    .addParameters(new Parameter(event.getProperty().getName()).setType(event.getProperty().getField().getType().clone()))
                    .setReturnType(returnType)
                            // Handles Base/Extension class' for us
                    .addCode(
                            new CodeSnippet(event.getProperty().getSetterMethod().getName() + "(" + event.getProperty().getName() + ");", "return (",
                                    returnType, ") this;")).addAccessModifiers(AccessModifier.Public).setCodeDocumentation(javaDoc);

            event.getClazz().addChildren(withMethod);

            createOverridingMethodsWithCovariantReturnTypesForAllSubclazzes(event, withMethod);

            // Finally add required properties info (must be added afterwards to avoid adding it to overriding methods in subclasses)
            withMethod.setCodeDocumentation(withMethod.getCodeDocumentation() + "<p/>" + MetaType.newline + requiredPropertiesDoc.toString());
        }
        return true;
    }

    /**
     * Create an overriding method with the same name and parameter list in all sub clazzes of this clazz. The overriding method will use a
     * covariant return type, so we don't have to consider the calling order when chaining methods from different subclazzes (doesn't write
     * methods to extension clazzes).
     *
     * @param event               The property event
     * @param selfReturningMethod The (self referencing) method which we should create override methods for in subclazzes
     */
    protected void createOverridingMethodsWithCovariantReturnTypesForAllSubclazzes(PropertyEvent event, Method selfReturningMethod) {
        List<Clazz> allSubClazzesOfClazz = event.getClazz().getCodeModel().findAllSubClazzesOfClazz(event.getClazz(), true);
        for (Clazz subClazz : allSubClazzesOfClazz) {
            if (subClazz.isExtensionClazz())
                continue;
            StringBuilder requiredPropertiesDoc = getContext().getRequiredPropertiesJavaDoc(subClazz);
            if (!subClazz.hasMethod(selfReturningMethod.getName(), selfReturningMethod.getParameters())) {
                StringBuilder parameterNames = new StringBuilder();
                int index = 0;
                for (Parameter parameter : selfReturningMethod.getParameters()) {
                    if (index > 0)
                        parameterNames.append(", ");
                    parameterNames.append(parameter.getName());
                }
                Clazz returnType = subClazz.isBaseClazz() ? getContext().getExtensionClazzForBaseClazz(subClazz) : subClazz;
                subClazz.addChildren(new Method(selfReturningMethod.getName(), AccessModifier.Public)
                        .addParameters(selfReturningMethod.getParameters().toArray(new Parameter[0]))
                        .setReturnType(new Type(returnType))
                        .addCode(
                                new CodeSnippet("return (", returnType.asFQCN(), ") super.", selfReturningMethod.getName(), "(", parameterNames.toString(),
                                        ");")).addAnnotations(new Annotation(Override.class))
                        .setCodeDocumentation(selfReturningMethod.getCodeDocumentation() + "<p/>" + MetaType.newline + requiredPropertiesDoc.toString()));
            }
        }
    }
}
