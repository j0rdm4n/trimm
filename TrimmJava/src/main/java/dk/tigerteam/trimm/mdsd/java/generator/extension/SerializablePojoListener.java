/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.Interface;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredEventHandler;
import dk.tigerteam.trimm.mdsd.java.generator.event.InterfaceEvent;

import java.io.Serializable;

/**
 * Makes sure that any given Clazz or Interface always implement {@link Serializable}.<br/>
 * The processing will occur deferred and will make sure that Serializable only will be applied if it isn't already a part of the
 * inheritance hierarchy
 *
 * @author Jeppe Cramon
 */
public class SerializablePojoListener extends AbstractGeneratorEventListener {

    @Override
    protected boolean handleClazzEvent(final ClazzEvent event) {

        JavaGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {

            public String getDescription() {
                return "Serializable POJO for Clazz " + event.getMetaClazz();
            }

            public void execute() {
                if (!implementsSerializable(event.getClazz())) {
                    // Only add Serializable if we don't inherit from a Clazz or Interface
                    if (!event.getClazz().extendsAGeneratedClazz() && !event.getClazz().implementsAGeneratedInterface()) {
                        event.getClazz().addImplementsTypes(Serializable.class);
                    }
                }
            }
        });
        return true;
    }

    @Override
    protected boolean handleInterfaceEvent(final InterfaceEvent event) {
        JavaGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {

            public String getDescription() {
                return "Serializable POJO for Interface " + event.getMetaInterface();
            }

            public void execute() {
                if (!extendsSerializable(event.getInterface())) {
                    // Only add Serializable if we don't inherit from a Clazz or Interface
                    if (!event.getInterface().extendsAGeneratedInterface()) {
                        event.getInterface().addExtendsTypes(Serializable.class);
                    }
                }
            }
        });
        return true;
    }

    private boolean implementsSerializable(Clazz clazz) {
        return clazz.implementsJavaInterface(Serializable.class);
    }

    private boolean extendsSerializable(Interface anInterface) {
        return anInterface.extendsJavaInterface(Serializable.class);
    }
}