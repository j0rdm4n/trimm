/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

/**
 * Represents a Getter property method.
 * <p/>
 * It's name is automatically set, when it's added to a Property object (you <u>can</u> set the name yourself, but it's not necessary and
 * therefore the default name constructor is left out).
 * <p/>
 * If not specified, then the Return type will be set to the type of the Property it's added to.
 *
 * @author Jeppe Cramon
 */
public class GetterMethod extends Method {

    public GetterMethod(AccessModifier... accessModifiers) {
        super(accessModifiers);
    }

    public GetterMethod(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
    }

    public GetterMethod(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

    @Override
    public Property getOwner() {
        return (Property) super.getOwner();
    }

//	@Override
//	@Deprecated
//	protected void writeMethodBodyContent(CodeWriter writer) {
//		if (getChildrenAsXmiElements().isEmpty()) {
//			// Write standard Getter method body content
//			writer.print("return ").print(getOwner().getName()).println(";");
//		} else {
//			super.writeMethodBodyContent(writer);
//		}
//	}

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }

}
