/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.java.codedom;

import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaInterface;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Models a Java Interface
 *
 * @author Jeppe Cramon
 */
public class Interface extends ClazzOrInterface {
    private List<Type> extendsTypes = new LinkedList<Type>();

    public Interface(AccessModifier... accessModifiers) {
        super(accessModifiers);
    }

    public Interface(String name, AccessModifier... accessModifiers) {
        super(name, accessModifiers);
    }

    public Interface(String name, boolean fabricated, AccessModifier... accessModifiers) {
        super(name, fabricated, accessModifiers);
    }

    @Override
    public MetaInterface getMetaType() {
        return (MetaInterface) super.getMetaType();
    }

    @Override
    public boolean isInterface() {
        return true;
    }

    public List<Type> getExtendsTypes() {
        return extendsTypes;
    }

    public Interface setExtendsTypes(List<Type> implementsTypes) {
        this.extendsTypes = implementsTypes;
        return this;
    }

    public Interface addExtendsTypes(Type... types) {
        extendsTypes.addAll(Arrays.asList(types));
        return this;
    }

    public Interface addExtendsTypes(Class<?>... types) {
        for (Class<?> type : types) {
            extendsTypes.add(new Type(type));
        }
        return this;
    }

    public CodePackage getPackage() {
        return findOwnerOfType(CodePackage.class);
    }

    public Interface setPackage(CodePackage package_) {
        this.setOwner(package_);
        return this;
    }

    public Interface setPackage(String packageName) {
        this.setOwner(JavaGeneratorContext.getContext().getCodeModel().findChildWithName(CodePackage.class, packageName));
        return this;
    }

    public String getFQCN() {
        CodePackage codePackage = getPackage();
        if (codePackage != null) {
            return codePackage.getName() + "." + getName();
        } else {
            return getName();
        }
    }

    public boolean extendsAGeneratedInterface() {
        for (Type type : this.getExtendsTypes()) {
            if (type.getInterface() != null) {
                return true;
            }
        }
        return false;
    }

    public boolean extendsJavaInterface(Class<?> javaInterface) {
        for (Type type : this.getExtendsTypes()) {
            if (type.getWrappedJavaClass() != null
                    && (javaInterface.equals(type.getWrappedJavaClass()) || javaInterface.getName().equals(type.getName()))) {
                return true;
            } else if (type.getInterface() != null && type.getInterface().extendsJavaInterface(javaInterface)) {
                return true;
            }
        }
        return false;
    }

//	@Override
//	public void generateCode(CodeWriter writer) {
//		CodePackage codePackage = getPackage();
//		if (codePackage != null) {
//			writer.print("package ").print(codePackage.getName()).println(";");
//		}
//
//		// TODO: Handle imports
//		writer.printJavaDoc(this);
//		writer.printAnnotations(this);
//		writer.printAccessModifiers(this);
//		writer.print("interface ");
//		writer.print(getName());
//
//		if (getGenerics().size() > 0) {
//			writer.print("<");
//			writer.printCodeElementCollection(getGenerics());
//			writer.print(">");
//		}
//
//		writer.print(" ");
//		if (extendsTypes.size() > 0) {
//			writer.print("extends ");
//			writer.printCodeElementCollection(extendsTypes);
//		}
//		writer.println("{");
//
//		for (CodeElement child : getChildrenAsXmiElements()) {
//			child.generateCode(writer);
//		}
//
//		writer.print("}");
//	}

    public int compareTo(ClazzOrInterface o) {
        if (o instanceof Interface) {
            Interface other = (Interface) o;
            if (other.extendsInterface(this)) {
                return 1; // We're greate since the other interface extends us
            } else if (this.extendsInterface(other)) {
                return -1;
            } else {
                return 0;
            }
        } else {
            Clazz other = (Clazz) o;
            if (other.implementsInterface(this)) {
                return 1; // We're greate since the other Clazz implements us
            } else {
                return 0;
            }
        }
    }

    /**
     * Returns true if this Interface extends the other interface
     *
     * @param other The other interface to test against
     * @return true if this Interface extends the other interface, otherwise false
     */
    public boolean extendsInterface(Interface other) {
        for (Type superType : getExtendsTypes()) {
            if (superType.getInterface() == null)
                continue;
            Interface superInterface = superType.getInterface();
            if (superInterface.equals(other)) {
                return true;
            }
            // Search up
            if (superInterface.extendsInterface(other)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void visit(CodeVisitor codeVisitor) {
        codeVisitor.handle(this);
    }


}
