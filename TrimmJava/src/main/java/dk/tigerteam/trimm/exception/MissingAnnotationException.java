/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.exception;

/**
 * Exception thrown if the code called an Annotation method which required the Annotation to be present (they typically come in pairs, one
 * that requires the Annotation to be present and one that doesn't).
 *
 * @author Jeppe Cramon
 * @see dk.tigerteam.trimm.mdsd.java.codedom.CodeElement#getRequiredAnnotation(Class)
 * @see dk.tigerteam.trimm.mdsd.java.codedom.CodeElement#getRequiredAnnotation(String)
 * @see dk.tigerteam.trimm.mdsd.java.codedom.CodeElement#getAnnotation(Class)
 * @see dk.tigerteam.trimm.mdsd.java.codedom.CodeElement#getAnnotation(String)
 */
public class MissingAnnotationException extends RuntimeException {
    private static final long serialVersionUID = 2243631600418436968L;

    public MissingAnnotationException() {
    }

    public MissingAnnotationException(String message) {
        super(message);
    }

    public MissingAnnotationException(Throwable cause) {
        super(cause);
    }

    public MissingAnnotationException(String message, Throwable cause) {
        super(message, cause);
    }

}
