/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.Criteria;
import dk.tigerteam.trimm.mdsd.java.codedom.Property;
import dk.tigerteam.trimm.mdsd.meta.*;
import dk.tigerteam.trimm.mdsd.uml2.xmi.ea.EAXmiReader;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class AssociationClassTest {
	
	@Test
	public void testAssociationClazzGeneration() {
		
		MetaModel metaModel = new EAXmiReader().read(this.getClass().getResource("AssociationClassTestCase.xml"));
		assertNotNull(metaModel);
		
		// Check that the MetaModel contains our B MetaAssociationClazz
		try {
			MetaAssociationClazz bMetaClazz = metaModel.findSingleMatchRequired(new MetaCriteria<MetaAssociationClazz>(MetaAssociationClazz.class) {
				@Override
				public boolean isOk(MetaAssociationClazz metaAssocitationClazz) {
					return  (metaAssocitationClazz.getName().equals("B"));
				}
			});
			assertEquals(4, bMetaClazz.getProperties().size());
			MetaProperty bInternalProperty = bMetaClazz.getPropertyByNameRequired("bInternalProperty");
			assertEquals("int", bInternalProperty.getType().getName());
		} catch (RuntimeException e) {
			metaModel.forEach(new ForEach() {
				public void each(MetaType metaType) {
					System.out.println(metaType.toString());
				}
			});
			throw e;
		}
		
		JavaGenerator javaGenerator = new JavaGenerator();
		JavaGeneratorContext generatorContext = new JavaGeneratorContext(metaModel);
		// Do your stuff :)
		List<ClazzOrInterface> transformResult = javaGenerator.transform(generatorContext);
		assertEquals(4, transformResult.size());
		
		// Can we find our B Clazz
		Clazz bClazz = generatorContext.getCodeModel().findSingle(new Criteria<Clazz>(Clazz.class) {
			@Override
			public boolean isOk(Clazz element) {
				return element.getName().equals("B");
			}
		});
		assertNotNull(bClazz);
		
		// Test for the right Java associations (to A, C and D) and bInternalProperty
		assertEquals(new ArrayList<Property>(bClazz.getProperties()).size(), 4);
		Property aProperty = bClazz.getPropertyByNameRequired("a");
		Property cProperty = bClazz.getPropertyByNameRequired("c");
		Property dProperty = bClazz.getPropertyByNameRequired("d");
		Property bInternalProperty = bClazz.getPropertyByNameRequired("bInternalProperty");
		
		
		
	}
}
