/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.Criteria;
import dk.tigerteam.trimm.mdsd.java.codedom.JavaCodeWritingVisitor;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.uml2.xmi.ea.EAXmiReader;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class BuiltInTypesListenerTest {

    @Test
    public void testTypeConversion() {

        MetaModel metaModel = new EAXmiReader().read(this.getClass().getResource("BuiltInTypesListener.xml"));
        assertNotNull(metaModel);

        JavaGenerator javaGenerator = new JavaGenerator();
        JavaGeneratorContext generatorContext = new JavaGeneratorContext(metaModel);
        generatorContext.addEventListeners(new BuiltInTypesListener());
        // Do your stuff :)
        List<ClazzOrInterface> transformResult = javaGenerator.transform(generatorContext);
        assertEquals(1, transformResult.size());

        // Can we find our Test Clazz
        Clazz testClazz
                = generatorContext.getCodeModel().findSingle(new Criteria<Clazz>(Clazz.class) {
            @Override
            public boolean isOk(Clazz element) {
                return element.getName().equals("TestClass");
            }
        });
        assertNotNull(testClazz);
        System.out.println(JavaCodeWritingVisitor.ToCode(testClazz));

        assertEquals(Timestamp.class.getName(), testClazz.getPropertyByName("timestampProperty").getField().getType().getName());
        assertEquals(BigDecimal.class.getName(), testClazz.getPropertyByName("bigDecimalProperty").getField().getType().getName());
        assertEquals(BigInteger.class.getName(), testClazz.getPropertyByName("bigIntegerProperty").getField().getType().getName());
        assertEquals(Calendar.class.getName(), testClazz.getPropertyByName("calendarProperty").getField().getType().getName());
        assertEquals(Date.class.getName(), testClazz.getPropertyByName("dateProperty").getField().getType().getName());
        assertEquals("String", testClazz.getPropertyByName("stringProperty").getField().getType().getName());
    }

    @Test
    public void verifyThatHandGeneratedMetaPropertiesHaveClazzAndWrappedJavaClass() {

        MetaModel metaModel = new MetaModel("metaModel");

        MetaClazz newClazz = new MetaClazz("idX");
        newClazz.setName("ClassX");

        MetaProperty content = new MetaProperty("idY");
        content.setName("content");
        content.setType(new MetaClazz(byte[].class));
        newClazz.add(content);

        MetaProperty name = new MetaProperty("idZ");
        name.setName("name");
        name.setType(new MetaClazz(String.class));
        newClazz.add(name);

        metaModel.add(newClazz);

        JavaGenerator javaGenerator = new JavaGenerator();
        JavaGeneratorContext generatorContext = new JavaGeneratorContext(metaModel);
        generatorContext.addEventListeners(new BuiltInTypesListener());
        // Do your stuff :)
        List<ClazzOrInterface> transformResult = javaGenerator.transform(generatorContext);
        assertEquals(1, transformResult.size());

        // Can we find our Test Clazz
        Clazz classX
                = generatorContext.getCodeModel().findSingle(new Criteria<Clazz>(Clazz.class) {
            @Override
            public boolean isOk(Clazz element) {
                return element.getName().equals("ClassX");
            }
        });
        assertNotNull(classX);
        System.out.println(JavaCodeWritingVisitor.ToCode(classX));

        //assertEquals("String", classX.getPropertyByName("stringProperty").getField().getType().getName());
    }
}
