/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.generator.extension;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.uml2.xmi.ea.EAXmiReader;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class ConstructorListenerTest {
    @Test
    public void testContructorListener() {

        MetaModel metaModel = new EAXmiReader().read(this.getClass().getResource("ConstructorListener.xml"));
        assertNotNull(metaModel);

        JavaGenerator javaGenerator = new JavaGenerator();
        JavaGeneratorContext generatorContext = new JavaGeneratorContext(metaModel);
        generatorContext.addEventListeners(new ConstructorListener());
        // Do your stuff :)
        List<ClazzOrInterface> transformResult = javaGenerator.transform(generatorContext);
        assertEquals(2, transformResult.size());

        // Can we find our Entity Clazz
        Clazz entityClazz
                = generatorContext.getCodeModel().findSingle(new Criteria<Clazz>(Clazz.class) {
            @Override
            public boolean isOk(Clazz element) {
                return element.getName().equals("Entity");
            }
        });
        assertNotNull(entityClazz);
        System.out.println(JavaCodeWritingVisitor.ToCode(entityClazz));

        assertEquals(1, entityClazz.getConstructors().size());
        Constructor constructor = entityClazz.getConstructors().get(0);
        assertEquals(1, constructor.getParameters().size());
        assertEquals("id", constructor.getParameters().get(0).getName());
        assertEquals("long", constructor.getParameters().get(0).getType().toString());
        List<CodeSnippet> snippets = constructor.findChildrenOfType(CodeSnippet.class);
        assertEquals(1, snippets.size());
        assertEquals("this.id = id;", JavaCodeWritingVisitor.ToCode(snippets.get(0)));


        // ----------------------------
        // Find our Customer class and test it
        Clazz customerClazz
                = generatorContext.getCodeModel().findSingle(new Criteria<Clazz>(Clazz.class) {
            @Override
            public boolean isOk(Clazz element) {
                return element.getName().equals("Customer");
            }
        });
        assertNotNull(customerClazz);
        assertEquals(entityClazz, customerClazz.getExtendsType().getClazz());
        System.out.println(JavaCodeWritingVisitor.ToCode(customerClazz));
        assertEquals(1, customerClazz.getConstructors().size());
        Constructor customerConstructor = customerClazz.getConstructors().get(0);
        assertEquals(3, customerConstructor.getParameters().size());
        assertEquals("id", customerConstructor.getParameters().get(0).getName());
        assertEquals("long", customerConstructor.getParameters().get(0).getType().toString());
        assertEquals("firstName", customerConstructor.getParameters().get(1).getName());
        assertEquals("String", customerConstructor.getParameters().get(1).getType().toString());
        assertEquals("lastName", customerConstructor.getParameters().get(2).getName());
        assertEquals("String", customerConstructor.getParameters().get(2).getType().toString());
        List<CodeSnippet> customerContructorSnippets = customerConstructor.findChildrenOfType(CodeSnippet.class);
        assertEquals(4, customerContructorSnippets.size());
        assertEquals("super(id);", JavaCodeWritingVisitor.ToCode(customerContructorSnippets.get(0)));
        assertEquals("this.firstName = firstName;", JavaCodeWritingVisitor.ToCode(customerContructorSnippets.get(1)));
        assertEquals("this.lastName = lastName;", JavaCodeWritingVisitor.ToCode(customerContructorSnippets.get(2)));
        assertEquals("throw new RuntimeException(\"constructorBody\");", JavaCodeWritingVisitor.ToCode(customerContructorSnippets.get(3)));

    }
}
