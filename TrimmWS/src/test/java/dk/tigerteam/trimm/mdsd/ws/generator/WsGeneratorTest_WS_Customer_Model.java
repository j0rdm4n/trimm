/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.generator;

import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.uml2.xmi.XmiReader;
import dk.tigerteam.trimm.mdsd.ws.*;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static junit.framework.Assert.assertNotNull;

/**
 * @author Jeppe Cramon
 */
public class WsGeneratorTest_WS_Customer_Model {

    @Test
    public void simpleTest() throws IOException {
        URL modelFile = this.getClass().getResource("Customer_WS_Model.xml");
        assertNotNull(modelFile);
        WsGenerationResult result = DefaultWsGenerator.generate(modelFile.getFile(), false, true, true, "", new NoHeaderSoapHeaderHandler());
        assertNotNull(result);

        WsGeneratorHelper.writeFiles("target/generated-sources/wsdl", result);
    }

    @Test
    public void testWithHeaderSpecificationAndClassicNamespaceandFileNameResolvers() throws IOException {
        URL modelFile = this.getClass().getResource("Customer_WS_Model.xml");
        assertNotNull(modelFile);

        SoapHeaderHandler soapHeaderHandler = new OneHeaderSoapHeaderHandler(
                new OneHeaderSoapHeaderHandler.SoapHeaderConfiguration(
                        "iheader", "http://internal/header", "header/header.xsd", "InternalHeader", "headerPart", "IHeaderElem"
                )
        );

        XmiReader xmiReader = WsGeneratorHelper.selectXmiReader(modelFile.getFile());
        final MetaModel metaModel = xmiReader.read(new File(modelFile.getFile()));

        WsGenerator wsGenerator = new WsGenerator(true, true, true, "",
                new DefaultSimpleTypeResolver(),  new ClassicNamespaceFileNameAndLocationResolver(),
                new DefaultSoapFaultHandler(), soapHeaderHandler);
        WsGenerationResult result = wsGenerator.generate(metaModel);
        WsGeneratorHelper.writeFiles("target/generated-sources/wsdl-classic", result);
    }

    @Test
        public void testWithHeaderSpecificationAndDefaultNamespaceandFileNameResolvers() throws IOException {
            URL modelFile = this.getClass().getResource("Customer_WS_Model.xml");
            assertNotNull(modelFile);


            SoapHeaderHandler soapHeaderHandler = new OneHeaderSoapHeaderHandler(
                    new OneHeaderSoapHeaderHandler.SoapHeaderConfiguration(
                            "iheader", "http://internal/header", "header/header.xsd", "InternalHeader", "headerPart", "IHeaderElem"
                    )
            );

            XmiReader xmiReader = WsGeneratorHelper.selectXmiReader(modelFile.getFile());
            final MetaModel metaModel = xmiReader.read(new File(modelFile.getFile()));
            WsGenerator wsGenerator = new WsGenerator(true, true, true, "",
                    new DefaultSimpleTypeResolver(), new DefaultNamespaceFileNameAndLocationResolver(),
                    new DefaultSoapFaultHandler(), soapHeaderHandler);
            WsGenerationResult result = wsGenerator.generate(metaModel);
            WsGeneratorHelper.writeFiles("target/generated-sources/wsdl-default", result);
        }

}