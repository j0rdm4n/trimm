/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.wsdlimport;

import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.uml2.xmi.ea.EAXmiWriter;
import dk.tigerteam.trimm.util.JDomWriter;
import org.apache.ws.commons.schema.ValidationEventHandler;
import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaCollection;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.Test;
import org.xml.sax.InputSource;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * 
 * @author Jeppe Cramon
 */
public class Xsd2MetaModelTest {
    @Test
    public void test() throws IOException, JDOMException {

    	MetaModel metaModel = new MetaModel("root");
        Xsd2MetaModel xsd2MetaModel = new Xsd2MetaModel();
        XmlSchemaCollection xmlSchemaCollection = new XmlSchemaCollection();
        XmlSchema xmlSchema = xmlSchemaCollection.read(new InputSource(new FileInputStream("src/test/resources/matmas05.xsd")), new ValidationEventHandler());
        xsd2MetaModel.transform(metaModel, xmlSchemaCollection);

        EAXmiWriter xmiWriter = new EAXmiWriter();
        final Document document = xmiWriter.write(metaModel, "I1006");
        JDomWriter.write(document, "target/generated-sources/xsd2metamodel",  "model.xml");
    }
}
