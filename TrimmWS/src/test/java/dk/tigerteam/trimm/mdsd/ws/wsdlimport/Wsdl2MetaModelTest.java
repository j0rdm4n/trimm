/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.wsdlimport;

import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaConfiguration;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.DefaultPackageRewriter;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.FileSystemCodeWritingStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.JalopyFormattingCodeWriter;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.JavaCodeWriter;
import dk.tigerteam.trimm.mdsd.java.generator.extension.BuiltInTypesListener;
import dk.tigerteam.trimm.mdsd.java.generator.extension.MetaTypeJavaDocListener;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.MetaPackage;
import dk.tigerteam.trimm.mdsd.uml2.xmi.ea.EAXmiWriter;
import dk.tigerteam.trimm.util.JDomWriter;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * TODO: doc...
 * 
 * @author Jeppe Cramon
 */
public class Wsdl2MetaModelTest {
		
	private String outputPath = "target/generated-sources/wsdl2metamodel";
	
	private JavaConfiguration createConfiguration() {
		JavaConfiguration configuration = new JavaConfiguration();
		configuration.setGenerateInterfacesToPath(outputPath);
		configuration.setGenerateBaseClassesToPath(outputPath);
		configuration.setGenerateExtensionClassesToPath(outputPath);
		configuration.setGenerateTestClassesToPath(outputPath);
		return configuration;
	}
	
  @Test
  @Ignore
  public void test() throws IOException, JDOMException {
    SAXBuilder builder = new SAXBuilder();
//Document doc = builder.build(this.getClass().getResourceAsStream("/wsdlimport/MaintenanceOrderSimpleByBasicDataQueryResponse_in.wsdl"));
    Document doc = builder.build(this.getClass().getResourceAsStream("/wsdlimport/Read Maintenance Order.wsdl"));

    MetaModel metaModel = new MetaModel("root");
    Wsdl2MetaModel wsdl2MetaModel = new Wsdl2MetaModel();
    wsdl2MetaModel.transform(metaModel, doc.getRootElement());

    EAXmiWriter xmiWriter = new EAXmiWriter();
    final Document document = xmiWriter.write(metaModel, "I1020");
    JDomWriter.write(document, "target/generated-sources/wsdl2metamodel",  "model.xml");

    // Try writing out the Java model to see what we've comeup with so far
    //debugGenerateJava(metaModel);
  }

  private void debugGenerateJava(MetaModel metaModel) {
    for (MetaPackage metaPackage : metaModel.findAllChildrenOfType(MetaPackage.class)) {
        metaPackage.setName(metaPackage.getName().replace('.', '_').replace('-', '.'));
    }

    JavaGeneratorContext generatorContext = new JavaGeneratorContext(metaModel);
    generatorContext.addEventListeners(new BuiltInTypesListener() {
        /**
         * Override this method to provide custom resolving of built in types (resolving means fixing the {@link Type#getName()} property)
         *
         * @param type The type to resolveNamespace
         */
        @Override
        protected void resolveBuiltInTypes(ClazzOrInterface owner, Type type) {
            if (type.getName().equals("token") || type.getName().equals("language") || type.getName().equals("string") || type.getName().equals("anyURI")) {
                type.setName("String");
            } else {
                super.resolveBuiltInTypes(owner, type);    //To change body of overridden methods use File | Settings | File Templates.
            }
        }
    });
    generatorContext.addEventListeners(new MetaTypeJavaDocListener());
    JavaCodeWriter codeWriter = new JalopyFormattingCodeWriter(
            createConfiguration(),
            new DefaultPackageRewriter(""), "");
    JavaGenerator generator = new JavaGenerator();
    List<ClazzOrInterface> allGeneratedClazzes = generator.transformAndWriteCode(generatorContext, new FileSystemCodeWritingStrategy(codeWriter));
  }
}
