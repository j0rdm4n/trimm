/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.generator;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaPackage;

import java.util.HashSet;
import java.util.Set;


/**
 * Classic handling is basically the same as DefaultNamespaceFileNameAndLocationResolver
 * except for these differences:
 * <p/>
 * <h1>Classic File location</h1>
 * The default is to use the root output folder for WSDL documents and an "xsd" folder for ALL schemas
 * <p/>
 * <h1>Classic filename resolving</h1>
 * A given Class becomes a part of a Schema. The schemas file name will be name of the package the Class is inside.
 * If that name has already been taken then we recursively (upwards) try to take the next super package and prefix the filename with that (with an underscore '_' between)
 * until we reach a unique filename or run out of super packages, in which case an exception is thrown.
 *
 * <p/>
 * WSDL documents files get's the same name as the Interface that represents the service inside the WSDL document (no replacement of spaces).
 * If that name has already been taken then we recursively (upwards) try to take the next super package and prefix the filename with that (with an underscore '_' between)
 * until we reach a unique filename or run out of super packages, in which case an exception is thrown.
 * <p/>
 * Wrapper XMl Schemas per default get the suffix "Wrapper"
 *
 * @author Jeppe Cramon
 */
public class ClassicNamespaceFileNameAndLocationResolver extends DefaultNamespaceFileNameAndLocationResolver {
    private Set<String> usedXmlSchemaFileNames = new HashSet<String>();
    private Set<String> usedWsdlDocumentFileNames = new HashSet<String>();

    @Override
    public String resolveXmlSchemaUnversionedFileName(MetaClazz xmlType) {
        String fileName = null;
        MetaPackage metaPackage = xmlType.getPackage();

        while (metaPackage != null) {
            if (fileName == null)
                fileName = metaPackage.getAliasOrName();
            else
                fileName = fileName + "_" + metaPackage.getAliasOrName();

            if (!usedXmlSchemaFileNames.contains(fileName)) break;
            metaPackage = metaPackage.getOwner();
        }

        if (fileName == null)
            throw new IllegalStateException("Can't resolveNamespace a file name for Xml Type " + xmlType.getFQCN());
        if (usedXmlSchemaFileNames.contains(fileName)) fileName = fileName + "_" + xmlType.getAliasOrName();
        if (usedXmlSchemaFileNames.contains(fileName))
            throw new IllegalStateException("Can't find a unique filename for Xml Type " + xmlType.getFQCN());
        usedXmlSchemaFileNames.add(fileName);
        return fileName;
    }

    @Override
    public String resolveWsdlDocumentUnversionedFileName(MetaClazz serviceMetaInterface) {
        String fileName = serviceMetaInterface.getAliasOrName();

        if (usedWsdlDocumentFileNames.contains(fileName)) {
            MetaPackage metaPackage = serviceMetaInterface.getPackage();
            while (metaPackage != null) {
                fileName = metaPackage.getAliasOrName() + "_" + fileName;

                if (!usedWsdlDocumentFileNames.contains(fileName)) break;
                metaPackage = metaPackage.getOwner();
            }
        }

        if (usedWsdlDocumentFileNames.contains(fileName))
            throw new IllegalStateException("Can't find a unique filename for Wsdl Document " + serviceMetaInterface.getFQCN());
        usedWsdlDocumentFileNames.add(fileName);
        return fileName;

    }

    @Override
    public String resolveWsdlDocumentWrapperSchemaSuffix(MetaClazz serviceMetaInterface) {
        return "Wrapper";
    }

    @Override
    public String resolveRootRelativePath(String namespace, MetaClazz xmlType) {
        if (xmlType.isInterface()) return "";
        else return "xsd";
    }

}
