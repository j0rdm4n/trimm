/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import java.util.HashSet;
import java.util.Set;

/**
 * TODO: doc...
 * 
 * @author Jeppe Cramon
 */
public class WsdlDocument extends AbstractWsArtifact {
	
	public static final Namespace WSDL_NAMESPACE = Namespace.getNamespace("wsdl", "http://schemas.xmlsoap.org/wsdl/");
	public static final Namespace SOAP_NAMESPACE = Namespace.getNamespace("soap", "http://schemas.xmlsoap.org/wsdl/soap/");
  private Set<String> faultMessagesDefined = new HashSet<String>();
  private MetaClazz metaInterface;
	private XmlSchema wrapperSchema;
	
	public WsdlDocument(MetaClazz metaInterface) {
		super();
        this.metaInterface = metaInterface;
	}

	public WsdlDocument(String namespace, MetaClazz metaInterface) {
		super(namespace);
        this.metaInterface = metaInterface;
	}

	public WsdlDocument(String namespace, String namespacePrefix, MetaClazz metaInterface) {
		super(namespace, namespacePrefix);
        this.metaInterface = metaInterface;
		
		Document xmlDefinition = new Document(new Element("definitions", WSDL_NAMESPACE));
		xmlDefinition.getRootElement().addNamespaceDeclaration(Namespace.getNamespace(namespacePrefix, namespace));
		xmlDefinition.getRootElement().addNamespaceDeclaration(XmlSchema.XSD_NAMESPACE);
		xmlDefinition.getRootElement().addNamespaceDeclaration(SOAP_NAMESPACE);
		xmlDefinition.getRootElement().setAttribute("targetNamespace", namespace);
		
		setXmlDefinition(xmlDefinition);
	}

	public XmlSchema getWrapperSchema() {
		return wrapperSchema;
	}

	public void setWrapperSchema(XmlSchema wrapperSchema) {
		this.wrapperSchema = wrapperSchema;
	}

    public MetaClazz getMetaInterface() {
        return metaInterface;
    }

    /**
     * Collects the names of all the Faults messages that have added to the WSDL document (since a WSDL document can
     * contain more than 1 operation and each may define the use the same Fault message we only want one Fault message to be defined)
     */
    public Set<String> getFaultMessagesDefined() {
        return faultMessagesDefined;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WsdlDocument that = (WsdlDocument) o;

        if (!metaInterface.equals(that.metaInterface)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return metaInterface.hashCode();
    }

    @Override
    public String toString() {
        return "WsdlDocument{" +
                "metaInterface=" + metaInterface + ", " +
                "namespace=" + getNamespace() +
                '}';
    }
}
