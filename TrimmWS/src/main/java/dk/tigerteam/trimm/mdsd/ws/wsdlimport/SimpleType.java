/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.wsdlimport;

import org.apache.ws.commons.schema.XmlSchemaFacet;
import org.apache.ws.commons.schema.XmlSchemaObjectCollection;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jeppe
 * Date: Sep 11, 2009
 * Time: 1:39:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleType {
    private String baseType;
    private List<XmlSchemaFacet> facets = new LinkedList<XmlSchemaFacet>();

    public String getBaseType() {
        return baseType;
    }

    public void setBaseType(String baseType) {
        this.baseType = baseType;
    }

    public List<XmlSchemaFacet> getFacets() {
        return facets;
    }

    public void addFacets(XmlSchemaObjectCollection facets) {
        Iterator<XmlSchemaFacet> iterator = facets.getIterator();
        while (iterator.hasNext()) {
            XmlSchemaFacet facet =  iterator.next();
            this.facets.add(facet);
        }
    }
}
