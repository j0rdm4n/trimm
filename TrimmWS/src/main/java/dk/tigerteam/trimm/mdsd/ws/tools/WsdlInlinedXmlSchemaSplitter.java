/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.tools;

import dk.tigerteam.trimm.util.JDomWriter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPath;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: jeppe
 * Date: Oct 1, 2009
 * Time: 3:46:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class WsdlInlinedXmlSchemaSplitter {
    public static List<String> split(File wsdlFile, String outputFolder) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(wsdlFile);

        Element wsdlDefinitions = doc.getRootElement();
        if (!"definitions".equals(wsdlDefinitions.getName())) {
            throw new IllegalArgumentException("Couldn't find WSDL definitions element in document");
        }

        Namespace xsdNs = getNamespace(wsdlDefinitions, "http://www.w3.org/2001/XMLSchema");
        Namespace wsdlNs = getNamespace(wsdlDefinitions, "http://schemas.xmlsoap.org/wsdl");

        XPath xpath = XPath.newInstance(p("types", wsdlNs));
        xpath.addNamespace(wsdlNs);

        Element wsdlTypes = (Element) xpath.selectSingleNode(doc.getRootElement());
        if (wsdlTypes == null) {
            throw new IllegalArgumentException("Couldn't fnd WSDL types element in the document");
        }

        List<Element> wsdlTypeElementsToRemove = new LinkedList<Element>();
        Element xmlSchemaForImport = new Element("schema", xsdNs);
        Map<String, Document> xmlSchemasExtracted = new HashMap<String, Document>();

        for (Element typesChild : (List<Element>)wsdlTypes.getChildren()) {
            if (typesChild.getName().equals("schema") && typesChild.getAttribute("targetNamespace") != null) {
                Element xmlSchema = typesChild;
                wsdlTypeElementsToRemove.add(xmlSchema);

                Element xmlSchemaCloned = (Element) xmlSchema.clone();
                for (Namespace additionalNamespace : (List<Namespace>)wsdlDefinitions.getAdditionalNamespaces()) {
                    xmlSchemaCloned.addNamespaceDeclaration(additionalNamespace);    
                }

                Document xmlSchemaDoc = new Document(xmlSchemaCloned);
                String fileName = typesChild.getAttributeValue("targetNamespace");
                if (fileName.startsWith("http://")) {
                    fileName = fileName.substring("http://".length());
                }
                fileName = fileName.replace('/', '_').replace('.', '_') + ".xsd";
                //JDomWriter.write(xmlSchemaDoc, outputFolder, fileName);
                xmlSchemasExtracted.put(fileName, xmlSchemaDoc);

                xmlSchemaForImport.addContent(
                    new Element("import", xsdNs)
                        .setAttribute("namespace", xmlSchema.getAttributeValue("targetNamespace"))
                        .setAttribute("schemaLocation", fileName)
                );
            }
        }
        for (Element elementToRemove : wsdlTypeElementsToRemove) {    
            wsdlTypes.getChildren().remove(elementToRemove);
        }

        if (xmlSchemaForImport.getChildren().size() > 0) {
            wsdlTypes.addContent(xmlSchemaForImport);
        }


        createSoapBindingsIfMissing(wsdlDefinitions, wsdlNs, xsdNs);



        List<String> filesCreated = new LinkedList<String>();

        String wsdlFileName = "Split-" + wsdlFile.getName();
        filesCreated.add(wsdlFileName);
        JDomWriter.write(doc, outputFolder, wsdlFileName);

        XPath importXpath = XPath.newInstance(p("import", xsdNs));
        xpath.addNamespace(xsdNs);
        // Write out the XML Schemas (just brute force import everything potentially circularly)
        for (Map.Entry<String, Document> documentEntry : xmlSchemasExtracted.entrySet()) {
            List<Element> imports = (List<Element>) importXpath.selectNodes(documentEntry.getValue().getRootElement());
            for (Element anImport : imports) {
                String namespace = anImport.getAttributeValue("namespace");
                for (Map.Entry<String, Document> xmlSchemaEntry : xmlSchemasExtracted.entrySet()) {
                    if (xmlSchemaEntry.getValue().getRootElement().getAttributeValue("targetNamespace").equals(namespace)) {
                        anImport.setAttribute("schemaLocation", xmlSchemaEntry.getKey());
                        break;
                    }
                }
            }
            filesCreated.add(documentEntry.getKey());
            JDomWriter.write(documentEntry.getValue(), outputFolder, documentEntry.getKey());
        }

        return filesCreated;
    }

    private static String p(String type, Namespace ns) {
        if (ns.getPrefix() == null || "".equals(ns.getPrefix())) {
            return type;
        } else {
            return ns.getPrefix() + ":" + type;
        }
    }

    private static Namespace getNamespace(Element element, String namespaceStr) {
        Namespace namespace = findNamespaceRecursively(element, namespaceStr);
        if (namespace == null) throw new IllegalArgumentException("Couldn't find a namespace instance for namespace '" + namespaceStr + "'");
        return namespace;
    }

    private static Namespace findNamespaceRecursively(Element element, String namespaceStr) {
        List<Namespace> namespaces = new LinkedList<Namespace>();
        namespaces.add(element.getNamespace());
        namespaces.addAll(element.getAdditionalNamespaces());

        for (Namespace namespace : namespaces) {
            if (namespaceStr.equals(namespace.getURI()) || (namespaceStr+"/").equals(namespace.getURI()) || namespaceStr.equals(namespace.getURI() + "/")) {
                return namespace;
            }
        }

        for (Element childElement : (List<Element>)element.getChildren()) {
            final Namespace namespace = findNamespaceRecursively(childElement, namespaceStr);
            if (namespace != null) {
                return namespace;
            }
        }
        return null;
    }

    private static void createSoapBindingsIfMissing(Element wsdlDefinition, Namespace wsdlNs, Namespace xsdNs) throws JDOMException {
        XPath bindingXpath = XPath.newInstance(p("binding", wsdlNs));
        bindingXpath.addNamespace(wsdlNs);

        if (bindingXpath.selectSingleNode(wsdlDefinition) != null) {
            return;
        }

        Namespace thisNs = getNamespace(wsdlDefinition, wsdlDefinition.getAttributeValue("targetNamespace"));
        Namespace soapNs = Namespace.getNamespace("soap", "http://schemas.xmlsoap.org/wsdl/soap/");
        wsdlDefinition.addNamespaceDeclaration(soapNs);

        XPath portTypeXPath = XPath.newInstance(p("portType", wsdlNs));
        portTypeXPath.addNamespace(wsdlNs);

        Element portType = (Element) portTypeXPath.selectSingleNode(wsdlDefinition);
        if (portType == null) {
            throw new IllegalStateException("Coulnd't find a PortType definition");
        }

        // Process all operations
        final String portTypeName = portType.getAttributeValue("name");
        final String portTypeBindingName = portTypeName + "Binding";
        Element binding = new Element("binding", wsdlNs)
                .setAttribute("name", portTypeBindingName)
                .setAttribute("type", p(portTypeName, thisNs)).
                addContent(
                    new Element("binding", soapNs)
                        .setAttribute("style", "document")
                        .setAttribute("transport", "http://schemas.xmlsoap.org/soap/http")
                );
        wsdlDefinition.addContent(binding);
        List<Element> operations = portType.getChildren("operation", wsdlNs);
        for (Element operation : operations) {
            String operationName = operation.getAttributeValue("name");
            Element operationBinding = new Element("operation", wsdlNs)
                                            .setAttribute("name", operationName)
                                            .addContent(
                                                new Element("operation", soapNs)
                                                        .setAttribute("soapAction", operationName)
                                                        .setAttribute("style", "document")
                                            );

            binding.addContent(operationBinding);
            // Handle Input messages
            Element wsdlInputBinding = new Element("input", wsdlNs);
            operationBinding.addContent(
                wsdlInputBinding
            );
            List<Element> inputs = operation.getChildren("input", wsdlNs);
            for (Element input : inputs) {

                final Element body = new Element("body", soapNs)
                        .setAttribute("use", "literal");

                if (input.getAttributeValue("name") != null) {
                    body.setAttribute("parts", input.getAttributeValue("name"));
                }

                wsdlInputBinding.addContent(
                     body
                );
            }

            // Handle Output messages
            Element wsdlOutputBinding = new Element("output", wsdlNs);
            operationBinding.addContent(
                wsdlOutputBinding
            );
            List<Element> outputs = operation.getChildren("output", wsdlNs);
            for (Element output : outputs) {
                final Element body = new Element("body", soapNs)
                        .setAttribute("use", "literal");

                if (output.getAttributeValue("name") != null) {
                    body.setAttribute("parts", output.getAttributeValue("name"));
                }

                wsdlOutputBinding.addContent(
                     body
                );
            }


            // Handle Fault messages
            List<Element> faults = operation.getChildren("fault", wsdlNs);
            for (Element fault : faults) {
                Element wsdlFaultBinding = new Element("fault", wsdlNs)
                                                    .setAttribute("name", fault.getAttributeValue("name"));
                operationBinding.addContent(
                    wsdlFaultBinding
                );

                wsdlFaultBinding.addContent(
                    new Element("fault", soapNs)
                        .setAttribute("name", fault.getAttributeValue("name"))
                        .setAttribute("use", "literal")
                );
            }
        }


        // Add WSDL Service
        final String serviceName = portTypeName + "Service";
        wsdlDefinition.addContent(
            new Element("service", wsdlNs)
                 .setAttribute("name", serviceName)
                 .addContent(
                new Element("port", wsdlNs)
                    .setAttribute("name", portTypeName + "Port")
                    .setAttribute("binding", p(portTypeBindingName, thisNs))
                    .addContent(
                        new Element("address", soapNs)
                            .setAttribute("location", thisNs.getURI() + serviceName)
                )
            )
        );

    }
}
