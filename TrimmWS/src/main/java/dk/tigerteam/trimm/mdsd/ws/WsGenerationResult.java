/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * TODO: doc...
 * 
 * @author Jeppe Cramon
 */
public class WsGenerationResult {
	
	private Map<MetaClazz, XmlSchema> metaClazzToXmlSchemaMapping = new HashMap<MetaClazz, XmlSchema>();
	private Map<String, XmlSchema> namespaceToXmlSchemaMapping = new HashMap<String, XmlSchema>();
	private Set<XmlSchema> xmlSchemas = new HashSet<XmlSchema>();
	
	private Map<MetaClazz, WsdlDocument> metaClazzToWsdlDocumentMapping = new HashMap<MetaClazz, WsdlDocument>();
	private Set<WsdlDocument> wsdlDocuments = new HashSet<WsdlDocument>();
	
	public Map<MetaClazz, XmlSchema> getMetaClazzToXmlSchemaMapping() {
		return metaClazzToXmlSchemaMapping;
	}
	
	public Map<String, XmlSchema> getNamespaceToXmlSchemaMapping() {
		return namespaceToXmlSchemaMapping;
	}

	public XmlSchema getMetaClazzToXmlSchemaMapping(MetaClazz type) {
		XmlSchema xmlSchema = getMetaClazzToXmlSchemaMapping().get(type);
		if (xmlSchema == null) {
			throw new IllegalStateException("Couldn't find a XmlSchema mapping for MetaClazz " + type);
		}
		return xmlSchema;
	}
	
	public Set<XmlSchema> getXmlSchemas() {
		return xmlSchemas;
	}

	public Map<MetaClazz, WsdlDocument> getMetaClazzToWsdlDocumentMapping() {
		return metaClazzToWsdlDocumentMapping;
	}

	public Set<WsdlDocument> getWsdlDocuments() {
		return wsdlDocuments;
	}
}
