/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import dk.tigerteam.trimm.mdsd.meta.MetaOperation;
import org.jdom2.Element;

import java.util.List;

/**
 * Strategy for custom SOAP Fault message handling
 * 
 * @author Jeppe Cramon
 */
public interface SoapFaultHandler {
	/**
	 * Generate WSDL messages for the Faults that this metaOperation has applied (if any), e.g.
	 * <pre>
	 * &lt;wsdl:message name="{Fault name}">
	 *   	&lt;wsdl:part name="{Fault name}"
	 *		    element="{Fault element}" />
	 * &lt;/wsdl:message>
	 * </pre>
	 * 
	 * @param wsdlDocument
	 *          The WSDL Document that this metaOperation belongs to
	 * @param messages
	 *          The WSDL Messages (JDOM objects representing the WSDL Messages) - Note check and update
	 *          {@link WsdlDocument#getFaultMessagesDefined()} to make sure that we don't define duplicate WSDL Fault Messages
	 * @param metaOperation
	 *          The {@link dk.tigerteam.mdsd.meta.MetaOperation} that we're want to handle WSDL Fault messages for
	 * @param result
	 *          The complete {@link WsGenerationResult} (in case you need to look up additional data)
	 */
	void createFaultMessages(WsdlDocument wsdlDocument, List<Element> messages, MetaOperation metaOperation, WsGenerationResult result);

	/**
	 * Apply the faults to the WSDL PortType operation definition, e.g.
	 * <pre>
	 * &lt;wsdl:operation name="{operation name}">
	 *		&lt;wsdl:input name="{request element name}"
	 *		message="tns:{request message}" />
	 *		&lt;wsdl:output name="{response element name}"
	 *			message="tns:{response message}" />
	 *		&lt;wsdl:fault name="{Fault name}" message="{Fault namespace}:{Fault message}" />
	 *		&lt;wsdl:fault name="{Other Fault name}" message="{Other Fault namespace}:{Other Fault message}" />
	 *	&lt;/wsdl:operation>
	 * </pre>
	 * 
	 * @param wsdlDocument
	 *          The WSDL Document that this metaOperation belongs to
	 * @param metaOperation
	 *          The {@link dk.tigerteam.mdsd.meta.MetaOperation} that we're want to handle WSDL Fault messages for
	 * @param portTypeOperation
	 *          The PortType operation definition (XML)
	 */
	void applyPortTypeFaults(WsdlDocument wsdlDocument, MetaOperation metaOperation, Element portTypeOperation);

	/**
	 * Apply the faults to the operations WSDL binding definition, e.g.
	 * 
	 * <pre>
	 * 		&lt;wsdl:operation name="{operation name}">
	 * 			&lt;soap:operation soapAction="" style="document" />
	 * 			&lt;wsdl:input>
	 * 				&lt;soap:body parts="parameters" use="literal" />
	 * 				&lt;soap:header message="{header prefix}:{SOAP header message name}" part="{SOAP header part}"
	 * 					use="literal" />
	 * 			&lt;/wsdl:input>
	 * 			&lt;wsdl:output>
	 * 				&lt;soap:body parts="parameters" use="literal" />
	 * 			&lt;/wsdl:output>
	 * 			&lt;wsdl:fault name="{Some Fault}">
	 * 				&lt;soap:fault name="{Fault name}" use="literal" />
	 * 			&lt;/wsdl:fault>
	 * 			&lt;wsdl:fault name="{Some other fault}">
	 * 				&lt;soap:fault name="{Some other fault name}" use="literal" />
	 * 			&lt;/wsdl:fault>
	 * 		&lt;/wsdl:operation>
	 * </pre>
	 * 
	 * @param metaOperation
	 *          The {@link dk.tigerteam.mdsd.meta.MetaOperation} that we're want to handle WSDL Fault messages for
	 * @param wsdlDocument
	 *          The WSDL Document that this metaOperation belongs to
	 * @param bindingOperation
	 *          The operations Binding definition (XML)
	 */
	void applyWsdlBindingFaults(MetaOperation metaOperation, WsdlDocument wsdlDocument, Element bindingOperation);
}
