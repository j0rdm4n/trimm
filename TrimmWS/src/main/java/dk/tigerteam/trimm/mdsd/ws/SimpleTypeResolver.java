/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import dk.tigerteam.trimm.mdsd.meta.MetaParameter;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import org.jdom2.Element;
import org.jdom2.Namespace;

import java.util.LinkedList;
import java.util.List;

/**
 * Strategy interface which can resolveNamespace restrictions on simple types according to how they're defined
 * in the model (e.g. using Stereotypes or Tagged value)
 * 
 * @author Jeppe Cramon
 */
public interface SimpleTypeResolver {
    /**
     * Resolve the Simple type restriction for this metaProperty.
     *
     * @param metaProperty The metaProperty to resolveNamespace the restriction for
     * @return The SimpleTypeRestriction which contains information about the restriction - MUST never be null
     */
    SimpleTypeRestriction resolveRestriction(MetaProperty metaProperty);

    /**
     * Resolve which builtin XML Schema type for use for a MetaProperty
     *
     * @param metaProperty The MetaProperty to resolveNamespace the XML SChema type for
     * @return The schema type (e.g. token) - MUST never be null
     */
    String resolveXsdType(MetaProperty metaProperty);

    /**
     * Resolve which builtin XML Schema type for use for a metaParameter
     *
     * @param metaParameter The metaParameter to resolveNamespace the XML SChema type for
     * @return The schema type (e.g. token) - MUST never be null
     */
    String resolveXsdType(MetaParameter metaParameter);

    /**
     * Value object which contains the simple type restrictions for a given MetaProperty (which is based on a built in MetaClazz)
     */
    public class SimpleTypeRestriction {

        public enum WhiteSpaceRestriction {
            collapse, replace, preserve
        }

        /**
         * Specifies the lower bounds for numeric values (the value must be greater than or equal to this value)
         */
        public String minInclusive;
        /**
         * Specifies the upper bounds for numeric values (the value must be less than or equal to this value)
         */
        public String maxInclusive;
        /**
         * Specifies the lower bounds for numeric values (the value must be greater than this value)
         */
        public String minExclusive;
        /**
         * Specifies the upper bounds for numeric values (the value must be less than this value)
         */
        public String maxExclusive;
        /**
         * Defines a list of acceptable values
         */
        public final List<String> enumerationValues = new LinkedList<String>();
        /**
         * Defines the exact sequence of characters that are acceptable (regular expression)
         */
        public String pattern;
        /**
         * Specifies how white space (line feeds, tabs, spaces, and carriage returns) is handled
         */
        public WhiteSpaceRestriction whiteSpace;
        /**
         * Specifies the exact number of characters or list items allowed. Must be equal to or greater than zero
         */
        public Long length;
        /**
         * Specifies the minimum number of characters or list items allowed. Must be equal to or greater than zero
         */
        public Long minLength;
        /**
         * Specifies the maximum number of characters or list items allowed. Must be equal to or greater than zero
         */
        public Long maxLength;
        /**
         * Specifies the maximum number of decimal places allowed. Must be equal to or greater than zero
         */
        public Long fractionDigits;
        /**
         * Specifies the exact number of digits allowed. Must be greater than zero
         */
        public Long totalDigits;

        public Element createSimpleType(String name, Namespace xsdNamespace, String xsdType) {
            Element restriction = new Element("restriction", xsdNamespace)
                    .setAttribute("base", xsdNamespace.getPrefix() + ":" + xsdType);
            Element simpleType = new Element("simpleType", xsdNamespace)
                    .addContent(restriction);

            if (name != null)
                simpleType.setAttribute("name", name);

            if (minInclusive != null) {
                restriction.addContent(
                        new Element("minInclusive", xsdNamespace)
                                .setAttribute("value", minInclusive.toString())
                );
            }
            if (maxInclusive != null) {
                restriction.addContent(
                        new Element("maxInclusive", xsdNamespace)
                                .setAttribute("value", maxInclusive.toString())
                );
            }
            if (minExclusive != null) {
                restriction.addContent(
                        new Element("minExclusive", xsdNamespace)
                                .setAttribute("value", minExclusive.toString())
                );
            }
            if (maxExclusive != null) {
                restriction.addContent(
                        new Element("maxExclusive", xsdNamespace)
                                .setAttribute("value", maxExclusive.toString())
                );
            }
            if (pattern != null) {
                restriction.addContent(
                        new Element("pattern", xsdNamespace)
                                .setAttribute("value", pattern.toString())
                );
            }
            if (whiteSpace != null) {
                restriction.addContent(
                        new Element("whiteSpace", xsdNamespace)
                                .setAttribute("value", whiteSpace.name().toLowerCase())
                );
            }
            if (length != null) {
                restriction.addContent(
                        new Element("length", xsdNamespace)
                                .setAttribute("value", length.toString())
                );
            }
            if (minLength != null) {
                restriction.addContent(
                        new Element("minLength", xsdNamespace)
                                .setAttribute("value", minLength.toString())
                );
            }
            if (maxLength != null) {
                restriction.addContent(
                        new Element("maxLength", xsdNamespace)
                                .setAttribute("value", maxLength.toString())
                );
            }
            if (fractionDigits != null) {
                restriction.addContent(
                        new Element("fractionDigits", xsdNamespace)
                                .setAttribute("value", fractionDigits.toString())
                );
            }
            if (totalDigits != null) {
                restriction.addContent(
                        new Element("totalDigits", xsdNamespace)
                                .setAttribute("value", totalDigits.toString())
                );
            }
            for (String enumerationValue : enumerationValues) {
                restriction.addContent(
                        new Element("enumeration", xsdNamespace)
                                .setAttribute("value", enumerationValue)
                );
            }

            return simpleType;
        }


        public void apply(Element propertyElement, Namespace xsdNamespace, String xsdType) {
            if (!hasRestrictions()) {
                propertyElement.setAttribute("type", xsdNamespace.getPrefix() + ":" + xsdType);
            } else {
                propertyElement.addContent(
                        createSimpleType(null, xsdNamespace, xsdType)
                );
            }
        }

        public boolean hasRestrictions() {
            return minInclusive != null || maxInclusive != null || minExclusive != null ||
                    maxExclusive != null || enumerationValues.size() > 0 || pattern != null ||
                    whiteSpace != null || length != null || minLength != null || maxLength != null ||
                    fractionDigits != null || totalDigits != null;
        }
    }
}
