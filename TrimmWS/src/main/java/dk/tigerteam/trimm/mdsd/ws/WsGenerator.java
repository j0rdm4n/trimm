/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import dk.tigerteam.trimm.mdsd.meta.*;
import dk.tigerteam.trimm.mdsd.ws.SimpleTypeResolver.SimpleTypeRestriction;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.IllegalAddException;
import org.jdom2.Namespace;

import java.util.*;

import static dk.tigerteam.trimm.mdsd.ws.WsdlDocument.SOAP_NAMESPACE;
import static dk.tigerteam.trimm.mdsd.ws.WsdlDocument.WSDL_NAMESPACE;
import static dk.tigerteam.trimm.mdsd.ws.XmlSchema.XSD_NAMESPACE;

public class WsGenerator {
  private static final String ENUM_BASE_TYPE_TAGGEDVALUE = "EnumBaseType";
	private static final String WS_ENUMERATION_STEREOTYPE = "WSEnumeration";
	private static final String NILLABLE_TAGGEDVALUE = "nillable";
	private static final String NAMESPACE = "Namespace";
  private boolean inlineReferencedSchemasInWsdlDocument = true;
  private SimpleTypeResolver simpleTypeResolver;
  private NamespaceFileNameAndLocationResolver namespaceFileNameAndLocationResolver;
  private SoapFaultHandler soapFaultHandler;
  private SoapHeaderHandler soapHeaderHandler;
  private Map<String, String> namespace2Prefix = new HashMap<String, String>();
  private boolean createWrapperSchema;
  private boolean inlineWrapperSchema;
  private String wrapperElementNameSuffix = "";
  private boolean ignoreAttributeStereoTypes;

  public WsGenerator(
                     boolean inlineReferencedSchemasInWsdlDocument,
                     boolean createWrapperSchema,
                     boolean inlineWrapperSchema,
                     String wrapperElementNameSuffix,
                     SimpleTypeResolver simpleTypeResolver,
                     NamespaceFileNameAndLocationResolver namespaceFileNameAndLocationResolver,
                     SoapFaultHandler soapFaultHandler,
                     SoapHeaderHandler soapHeaderHandler) {
    super();
    this.inlineReferencedSchemasInWsdlDocument = inlineReferencedSchemasInWsdlDocument;
    this.simpleTypeResolver = simpleTypeResolver;
    this.namespaceFileNameAndLocationResolver = namespaceFileNameAndLocationResolver;
    this.createWrapperSchema = createWrapperSchema;
    this.soapFaultHandler = soapFaultHandler;
    this.inlineWrapperSchema = inlineWrapperSchema;
    this.wrapperElementNameSuffix = wrapperElementNameSuffix == null ? "" : wrapperElementNameSuffix;
    this.soapHeaderHandler = soapHeaderHandler;
  }
  
  public SoapHeaderHandler getSoapHeaderHandler() {
		return soapHeaderHandler;
	}
  
  public String getWrapperElementNameSuffix() {
		return wrapperElementNameSuffix;
	}

	public boolean isIgnoreAttributeStereoTypes() {
    return ignoreAttributeStereoTypes;
  }

  public void setIgnoreAttributeStereoTypes(boolean ignoreAttributeStereoTypes) {
    this.ignoreAttributeStereoTypes = ignoreAttributeStereoTypes;
  }

  public WsGenerationResult generate(MetaModel metaModel) {
    WsGenerationResult result = new WsGenerationResult();
    List<MetaClazz> allEntitiesAndInterfaces = metaModel.find(new MetaCriteria<MetaClazz>(MetaClazz.class) {

      @Override
      public boolean isOk(MetaClazz element) {
        if (element.getAliasOrName() != null && !element.hasStereoType("Boundary") && shouldGenerateForMetaClazz(element)) {
          return true;
        } else {
          System.out.println("Ignoring " + element + " since it doesn't have a name or an alias");
          return false;
        }
      }
    });
    for (MetaClazz metaClazz : allEntitiesAndInterfaces) {
      if (metaClazz.isInterface()) {
        stage1CreateWsdlDocument(metaClazz, result);
      } else {
        stage1CreateXmlSchema(metaClazz, result);
      }
    }

    for (MetaClazz metaClazz : allEntitiesAndInterfaces) {
      if (metaClazz.isInterface()) {
        stage2CreateWsdlDocument(metaClazz, result);
      } else {
        stage2CreateXmlSchema(metaClazz, result);
      }
    }

    // Run through all XmlSchemas generated and handle imports of referenced XmlSchemas
    for (XmlSchema xmlSchema : result.getXmlSchemas()) {
      for (XmlSchema referencedXmlSchema : xmlSchema.getReferencedXmlSchemas()) {
        if (referencedXmlSchema.equals(xmlSchema)) {
          continue;
        }
        if (!inlineReferencedSchemasInWsdlDocument) {
          Element importElement = new Element("import", XSD_NAMESPACE);
          importElement.setAttribute("namespace", referencedXmlSchema.getNamespace());
          String relativeSchemaPath = resolveRelativeSchemaImportPath(xmlSchema, referencedXmlSchema);
          importElement.setAttribute("schemaLocation", relativeSchemaPath);
          xmlSchema.getXmlDefinition().getRootElement().addContent(0, importElement);
        }
        try {
          xmlSchema.getXmlDefinition().getRootElement().addNamespaceDeclaration(Namespace.getNamespace(referencedXmlSchema.getNamespacePrefix(), referencedXmlSchema.getNamespace()));
        } catch (IllegalAddException e) {
          throw new RuntimeException("Namespace prefix '" + referencedXmlSchema.getNamespacePrefix() + "' collided with another namespace prefix. List of all namespaces defined:"
            + xmlSchema.getXmlDefinition().getRootElement().getAdditionalNamespaces(), e);
        }
      }
    }


    // Run through all WsdlDocuments that have Wrapper Schemas
    for (WsdlDocument wsdlDocument : result.getWsdlDocuments()) {
      Element wsdlTypes = new Element("types", WSDL_NAMESPACE);
      Element xmlSchemaForImports = new Element("schema", XSD_NAMESPACE);
      
      Set<XmlSchema> alreadyInlinedXmlSchemas = new HashSet<XmlSchema>();
      if (createWrapperSchema) {
        if (inlineWrapperSchema) {
        	if (!inlineReferencedSchemasInWsdlDocument) {
        		// Make sure that schemas referenced from the wrapper schema isn't inlined
        		alreadyInlinedXmlSchemas.addAll(result.getXmlSchemas());
        		alreadyInlinedXmlSchemas.remove(wsdlDocument.getWrapperSchema());
        	}
          //wsdlTypes.addContent(wsdlDocument.getWrapperSchema().getXmlDefinition().cloneContent());
        	// Only import referenced schemas (without location) IF all schemas are inlined
        	recursivelyInlineXmlSchemas(result, wsdlTypes, wsdlDocument, wsdlDocument.getWrapperSchema(), alreadyInlinedXmlSchemas, inlineReferencedSchemasInWsdlDocument);
          // Don't write out the XML Schema
          result.getXmlSchemas().remove(wsdlDocument.getWrapperSchema());
        } else {
          Element schemaImport = new Element("import", XSD_NAMESPACE)
            .setAttribute("namespace", wsdlDocument.getWrapperSchema().getNamespace())
            .setAttribute("schemaLocation", resolveRelativeSchemaImportPath(wsdlDocument, wsdlDocument.getWrapperSchema()));
          xmlSchemaForImports.addContent(schemaImport);
        }
      }

      for (XmlSchema referencedXmlSchema : wsdlDocument.getReferencedXmlSchemas()) {
        if (inlineReferencedSchemasInWsdlDocument && referencedXmlSchema.getXmlDefinition() != null) {
          recursivelyInlineXmlSchemas(result, wsdlTypes, wsdlDocument, referencedXmlSchema, alreadyInlinedXmlSchemas, true);

        } else {
          Element importElement = new Element("import", XSD_NAMESPACE);
          importElement.setAttribute("namespace", referencedXmlSchema.getNamespace());
          String relativeSchemaPath = resolveRelativeSchemaImportPath(wsdlDocument, referencedXmlSchema);
          importElement.setAttribute("schemaLocation", relativeSchemaPath);
          xmlSchemaForImports.addContent(importElement);
          try {
            wsdlDocument.getXmlDefinition().getRootElement().addNamespaceDeclaration(Namespace.getNamespace(referencedXmlSchema.getNamespacePrefix(), referencedXmlSchema.getNamespace()));
          } catch (IllegalAddException e) {
            throw new RuntimeException("Namespace prefix '" + referencedXmlSchema.getNamespacePrefix() + "' collided with another namespace prefix. List of all namespaces defined:"
              + wsdlDocument.getXmlDefinition().getRootElement().getAdditionalNamespaces(), e);
          }
        }
      }
      if (xmlSchemaForImports.getChildren().size() > 0) {
        wsdlTypes.addContent(0, xmlSchemaForImports);
      }

      if (wsdlTypes.getChildren().size() > 0) {
        wsdlDocument.getXmlDefinition().getRootElement().addContent(0, wsdlTypes);
      }
    }

    return result;
  }

  private void recursivelyInlineXmlSchemas(WsGenerationResult result, Element wsdlTypes, WsdlDocument wsdlDocument, XmlSchema referencedXmlSchema, Set<XmlSchema> alreadyInlinedXmlSchemas, boolean importReferenceSchemas) {
    if (!alreadyInlinedXmlSchemas.contains(referencedXmlSchema) && referencedXmlSchema.getXmlDefinition() != null) {
      @SuppressWarnings("rawtypes")
			List clonedReferencedSchema = referencedXmlSchema.getXmlDefinition().cloneContent();
      try {
        ((Element) clonedReferencedSchema.get(0)).addNamespaceDeclaration(Namespace.getNamespace(referencedXmlSchema.getNamespacePrefix(), referencedXmlSchema.getNamespace()));
      } catch (IllegalAddException e) {
        throw new RuntimeException("Namespace prefix '" + referencedXmlSchema.getNamespacePrefix() + "' collided with another namespace prefix. List of all namespaces defined:"
          + ((Element) clonedReferencedSchema.get(0)).getAdditionalNamespaces(), e);
      }
      
      // Import referenced namespaces
      if (importReferenceSchemas) {
	      for (XmlSchema subReferencedXmlSchema : referencedXmlSchema.getReferencedXmlSchemas()) {
	      	if (!subReferencedXmlSchema.getNamespace().equals(referencedXmlSchema.getNamespace())) {
		      	Element importElement = new Element("import", XSD_NAMESPACE);
		        importElement.setAttribute("namespace", subReferencedXmlSchema.getNamespace());
		        ((Element) clonedReferencedSchema.get(0)).addContent(0, importElement);
	      	}
				}
      }
      

      wsdlTypes.addContent(clonedReferencedSchema);
      // Don't write out the XML Schema
      //result.getXmlSchemas().remove(referencedXmlSchema);

      try {
        wsdlDocument.getXmlDefinition().getRootElement().addNamespaceDeclaration(Namespace.getNamespace(referencedXmlSchema.getNamespacePrefix(), referencedXmlSchema.getNamespace()));
      } catch (IllegalAddException e) {
        throw new RuntimeException("Namespace prefix '" + referencedXmlSchema.getNamespacePrefix() + "' collided with another namespace prefix. List of all namespaces defined:"
          + wsdlDocument.getXmlDefinition().getRootElement().getAdditionalNamespaces(), e);
      }

      referencedXmlSchema.setHasBeenInlined(true);
      alreadyInlinedXmlSchemas.add(referencedXmlSchema);
      for (XmlSchema subReferencedSchema : referencedXmlSchema.getReferencedXmlSchemas()) {
        recursivelyInlineXmlSchemas(result, wsdlTypes, wsdlDocument, subReferencedSchema, alreadyInlinedXmlSchemas, importReferenceSchemas);
      }
    }
  }

  /**
   * Simple tempalte method that allows users to skip generation for classes after own choice
   *
   * @param metaClazz The Clazz that's being considered for generation
   * @return true fi DefaultWsGenerator should generate for the clazz
   */
  protected boolean shouldGenerateForMetaClazz(MetaClazz metaClazz) {
    return true;
  }

  protected String resolveRelativeSchemaImportPath(WsArtifact xmlSchema,
                                                   WsArtifact referencedXmlSchema) {
  	int commonPathPartLength = 0;
  	String[] referencedPathParts = referencedXmlSchema.getRootRelativePath().split("/");
  	StringBuilder path = new StringBuilder();
  	
  	if (!xmlSchema.getRootRelativePath().equals("")) {
	    String relativePath = referencedXmlSchema.getRootRelativePath();
	    String[] pathParts = xmlSchema.getRootRelativePath().split("/");
	    String commonPath = null;
	    for (String pathPart : pathParts) {
	      if (commonPath == null) {
	        commonPath = pathPart;
	      } else {
	        commonPath = commonPath + "/" + pathPart;
	      }
	      int indexOfCommonPath = relativePath.indexOf(commonPath);
	      if (indexOfCommonPath != -1) {
	        commonPathPartLength++;
	      } else {
	        break;
	      }
	    }
	    
	    for (int i = 0; i < pathParts.length - commonPathPartLength; i++) {
	      path.append("../");
	    }
  	}
  	
    for (int i = commonPathPartLength; i < referencedPathParts.length; i++) {
      if (!path.toString().endsWith("/") && path.length() > 0)
        path.append("/");

      path.append(referencedPathParts[i]);
    }

    if (!path.toString().endsWith("/") && path.length() > 0)
      path.append("/");
    path.append(referencedXmlSchema.getFileName());

    return path.toString();
  }

  protected String resolveNamespace(MetaClazz xmlType) {
    String namespace = namespaceFileNameAndLocationResolver.resolveNamespace(xmlType);
    return namespace.replace(' ', '_');
  }

  protected String resolveNamespacePrefix(MetaClazz xmlType) {
    String namespace = resolveNamespace(xmlType);

    if (namespace2Prefix.containsKey(namespace)) {
      return namespace2Prefix.get(namespace);
    }

    String[] namespaceParts = namespace.split("/");

    String prefix = null;
    for (int i = namespaceParts.length - 1; i >= 0; i--) {
      if (prefix != null) {
        prefix += "_" + namespaceParts[i];
      } else {
        prefix = namespaceParts[i];
      }

      if (!namespace2Prefix.values().contains(prefix)) {
        break;
      }
    }

    if (namespace2Prefix.values().contains(prefix)) {
      throw new RuntimeException("Failed to createId a unique namespace prefix for " + xmlType);
    }

    namespace2Prefix.put(namespace, prefix);
    return prefix;
  }

  protected void stage1CreateXmlSchema(MetaClazz xmlType,
                                       WsGenerationResult result) {
    String namespace = resolveNamespace(xmlType);
    XmlSchema xmlSchema = result.getNamespaceToXmlSchemaMapping().get(namespace);
    if (xmlSchema == null) {
      xmlSchema = new XmlSchema(namespace, resolveNamespacePrefix(xmlType), true);

      //String fqpn = xmlType.getPackage().getFullPackageNameUsingAliasIfAvailable();
      //String path = fqpn.substring(0, fqpn.indexOf("." + xmlType.getPackage().getAliasOrName()));
      xmlSchema.setRootRelativePath(resolveRootRelativePath(namespace, xmlType));
      String unversionedFileName =  namespaceFileNameAndLocationResolver.resolveXmlSchemaUnversionedFileName(xmlType);
			String resolveVersion = namespaceFileNameAndLocationResolver.resolveVersion(xmlType);
			xmlSchema.setFileName((unversionedFileName + resolveVersion + ".xsd").replace(' ', '_'));
      //System.out.println("Resolve Root Relative Path '" + xmlSchema.getRootRelativePath() +"', unVersionedFileName: '" + unversionedFileName + "', version: '" + resolveVersion + "', & Filename: '" + xmlSchema.getFileName() + "' for MetaClazz '" + xmlType +"' in Meta Package '" + xmlType.getPackage() + "' with Alias: '" + xmlType.getPackage().getAlias() + "' & AliasOrName: '" + xmlType.getPackage().getAliasOrName() + "'");
      result.getNamespaceToXmlSchemaMapping().put(namespace, xmlSchema);
      result.getXmlSchemas().add(xmlSchema);

    }
    result.getMetaClazzToXmlSchemaMapping().put(xmlType, xmlSchema);
    xmlType.setAttribute(NAMESPACE, namespace);
  }

  private String resolveRootRelativePath(String resolvedNamespace, MetaClazz xmlType) {
    String rootRelativePath = namespaceFileNameAndLocationResolver.resolveRootRelativePath(resolvedNamespace, xmlType);
    return rootRelativePath.replace(' ', '_');
  }

  protected void stage2CreateXmlSchema(MetaClazz xmlType,
                                       WsGenerationResult result) {
    String namespace = (String) xmlType.getAttribute(NAMESPACE);
    XmlSchema xmlSchema = result.getNamespaceToXmlSchemaMapping().get(namespace);
    Document xmlDefinition = xmlSchema.getXmlDefinition();

    boolean isSimpleType = xmlType.getProperties().size() == 1 && xmlType.getProperties().get(0).getType().isBuiltInType() && xmlType.getSuperClazz() == null;
		if (isSimpleType) {
      SimpleTypeRestriction typeRestriction = simpleTypeResolver.resolveRestriction(xmlType.getProperties().get(0));
      String xsdType = simpleTypeResolver.resolveXsdType(xmlType.getProperties().get(0));
      // Define the simple type
      xmlDefinition.getRootElement().addContent(typeRestriction.createSimpleType(xmlType.getAliasOrName().replace(' ', '_'), XSD_NAMESPACE, xsdType));

      // Define the element and simple content type
      Element xmlTypeElement = new Element("element", XSD_NAMESPACE);
      xmlTypeElement.setAttribute("name", xmlType.getAliasOrName());
      xmlTypeElement.setAttribute("type", xmlSchema.getNamespacePrefix() + ":" + xmlType.getAliasOrName());
      if (xmlType.getDocumentation() != null && !"".equals(xmlType.getDocumentation())) {
      	xmlTypeElement.addContent(createXmlSchemaDocumentationAnnotation(xmlType));
      }
      xmlDefinition.getRootElement().addContent(xmlTypeElement);
    } else if (xmlType.isEnumerationClazz()) {
    	SimpleTypeRestriction typeRestriction = new SimpleTypeRestriction();
    	String xsdType = "string"; // default if no WS Enumeration stereotype is present
    	if (xmlType.hasStereoType(WS_ENUMERATION_STEREOTYPE) && xmlType.hasNotNullTaggedValue(ENUM_BASE_TYPE_TAGGEDVALUE)) {
    		xsdType = xmlType.getTaggedValue(ENUM_BASE_TYPE_TAGGEDVALUE);
    	}
    	// Add enum values
    	for (MetaProperty property : xmlType.getProperties()) {
    		typeRestriction.enumerationValues.add(property.getAliasOrName());
			}
      // Define the simple type
      xmlDefinition.getRootElement().addContent(typeRestriction.createSimpleType(xmlType.getAliasOrName().replace(' ', '_'), XSD_NAMESPACE, xsdType));

      // Define the element and simple content type
      Element xmlTypeElement = new Element("element", XSD_NAMESPACE);
      if (xmlType.getDocumentation() != null && !"".equals(xmlType.getDocumentation())) {
      	xmlTypeElement.addContent(createXmlSchemaDocumentationAnnotation(xmlType));
      }
      xmlTypeElement.setAttribute("name", xmlType.getAliasOrName());
      xmlTypeElement.setAttribute("type", xmlSchema.getNamespacePrefix() + ":" + xmlType.getAliasOrName());
      xmlDefinition.getRootElement().addContent(xmlTypeElement);
    } else {
      // Complex type
      Element xmlComplexType = createComplexElement(xmlType, result, xmlSchema.getReferencedXmlSchemas());
      xmlDefinition.getRootElement().addContent(xmlComplexType);

      // Define the element and complex type
      Element xmlTypeElement = new Element("element", XSD_NAMESPACE);
      if (xmlType.getDocumentation() != null && !"".equals(xmlType.getDocumentation())) {
      	xmlTypeElement.addContent(createXmlSchemaDocumentationAnnotation(xmlType));
      }
      xmlTypeElement.setAttribute("name", xmlType.getAliasOrName());
      xmlTypeElement.setAttribute("type", xmlSchema.getNamespacePrefix() + ":" + xmlType.getAliasOrName());
      xmlDefinition.getRootElement().addContent(xmlTypeElement);
    }
  }
  
  protected Element createXmlSchemaDocumentationAnnotation(MetaType metaType) {
  	Element annotation = new Element("annotation", XSD_NAMESPACE)
  													.addContent(
  															new Element("documentation", XSD_NAMESPACE)
  																.setText(metaType.getDocumentation() != null ? metaType.getDocumentation() : "")
  													);
  	return annotation;
  }

  protected Element createComplexElement(MetaClazz xmlType,
                                         WsGenerationResult result, Set<XmlSchema> referencedXmlSchemas) {
    if (xmlType.getAliasOrName() == null) {
      throw new IllegalArgumentException(xmlType + " didn't have a name or an alias");
    }
    Element xmlComplexType = new Element("complexType", XSD_NAMESPACE);
    xmlComplexType.setAttribute("name", xmlType.getAliasOrName());
    if (xmlType.getDocumentation() != null && !"".equals(xmlType.getDocumentation())) {
    	xmlComplexType.addContent(createXmlSchemaDocumentationAnnotation(xmlType));
    }
    if (xmlType.isAbstract()) {
    	xmlComplexType.setAttribute("abstract", "true");
    }
    
    // Handle inheritance...
    Element addDefinitionsToElement = xmlComplexType;
    if (xmlType.getSuperClazz() != null) {
    	Element complexContext = new Element("complexContent", XSD_NAMESPACE);
    	Element extension = new Element("extension", XSD_NAMESPACE);
    	XmlSchema propertyTypeXmlSchema = result.getMetaClazzToXmlSchemaMapping(xmlType.getSuperClazz());
    	extension.setAttribute("base", propertyTypeXmlSchema.getNamespacePrefix() + ":" + xmlType.getSuperClazz().getAliasOrName());
    	complexContext.addContent(extension);
    	xmlComplexType.addContent(complexContext);
    	referencedXmlSchemas.add(propertyTypeXmlSchema);
    	addDefinitionsToElement = extension;
    } 

    Element sequence = new Element("sequence", XSD_NAMESPACE);
    addDefinitionsToElement.addContent(sequence);

    for (MetaProperty metaProperty : xmlType.getProperties()) {
    	if (metaProperty.getType() == null) {
    		throw new IllegalStateException("MetaProperty doesn't have a type. MetaProperty: " + metaProperty);
    	}
    	
    	
      if (!metaProperty.isPartInAnAssociation() || (metaProperty.isPartInAnAssociation() && metaProperty.isOwnerOfAssociation())) {
        if (metaProperty.hasStereoType("WSAttribute") && !ignoreAttributeStereoTypes) {
          Element attributeElement = new Element("attribute", XSD_NAMESPACE);
          attributeElement.setAttribute("name", resolveMetaPropertyName(metaProperty));

          if (metaProperty.getType().isBuiltInType()) {
            //propertyElement.setAttribute("type", XSD_NAMESPACE.getPrefix() + ":" + resolveXsdType(metaProperty.getType()));
            SimpleTypeResolver.SimpleTypeRestriction typeRestriction = simpleTypeResolver.resolveRestriction(metaProperty);
            String xsdType = simpleTypeResolver.resolveXsdType(metaProperty);
            //if (typeRestriction.hasRestrictions()) {
            //  throw new IllegalStateException("Attribute types can't have Restrictions! - " + metaProperty + " in " + metaProperty.getOwner());
            //}
            typeRestriction.apply(attributeElement, XSD_NAMESPACE, xsdType);
          } else {
            XmlSchema propertyTypeXmlSchema = result.getMetaClazzToXmlSchemaMapping(metaProperty.getType());
            attributeElement.setAttribute("type", propertyTypeXmlSchema.getNamespacePrefix() + ":" + metaProperty.getType().getAliasOrName());

            referencedXmlSchemas.add(propertyTypeXmlSchema);
          }
          if (metaProperty.getMinCardinality() == 0) {
            attributeElement.setAttribute("use", "required");
          }
          addDefinitionsToElement.addContent(attributeElement);
        } else {
          Element propertyElement = new Element("element", XSD_NAMESPACE);
          propertyElement.setAttribute("name", resolveMetaPropertyName(metaProperty));

          if (metaProperty.getType().isBuiltInType()) {
            //propertyElement.setAttribute("type", XSD_NAMESPACE.getPrefix() + ":" + resolveXsdType(metaProperty.getType()));
            SimpleTypeResolver.SimpleTypeRestriction typeRestriction = simpleTypeResolver.resolveRestriction(metaProperty);
            String xsdType = simpleTypeResolver.resolveXsdType(metaProperty);
            typeRestriction.apply(propertyElement, XSD_NAMESPACE, xsdType);
          } else {
            XmlSchema propertyTypeXmlSchema = result.getMetaClazzToXmlSchemaMapping(metaProperty.getType());
            propertyElement.setAttribute("type", propertyTypeXmlSchema.getNamespacePrefix() + ":" + metaProperty.getType().getAliasOrName());

            referencedXmlSchemas.add(propertyTypeXmlSchema);
          }
          propertyElement.setAttribute("minOccurs", ((Integer) metaProperty.getMinCardinality()).toString());
          if (metaProperty.getMaxCardinality() == -1) {
            propertyElement.setAttribute("maxOccurs", "unbounded");
          } else {
            propertyElement.setAttribute("maxOccurs", ((Integer) metaProperty.getMaxCardinality()).toString());
          }
          
          propertyElement.setAttribute("nillable", resolveNillability(metaProperty));
          sequence.addContent(propertyElement);
        }
      }
    }
    return xmlComplexType;
  }

  private String resolveNillability(MetaProperty metaProperty) {
		if (metaProperty.hasTaggedValue(NILLABLE_TAGGEDVALUE)) {
			String nillableTaggedValue = metaProperty.getTaggedValue(NILLABLE_TAGGEDVALUE);
			if (nillableTaggedValue != null && !"".equals(nillableTaggedValue)) {
				return nillableTaggedValue;
			} 
		} 
		return "false";
	}

	protected String resolveMetaPropertyName(MetaProperty metaProperty) {
    if (metaProperty.getAliasOrName() != null) {
      return metaProperty.getAliasOrName();
    } else {
      return metaProperty.getType().getAliasOrName();
    }
  }

  protected void stage1CreateWsdlDocument(MetaClazz serviceMetaInterface,
                                          WsGenerationResult result) {
    String namespace = resolveNamespace(serviceMetaInterface);
    WsdlDocument wsdlDocument = result.getMetaClazzToWsdlDocumentMapping().get(serviceMetaInterface);
    if (wsdlDocument == null) {
      wsdlDocument = new WsdlDocument(namespace, resolveNamespacePrefix(serviceMetaInterface), serviceMetaInterface);
      wsdlDocument.setRootRelativePath(resolveRootRelativePath(namespace, serviceMetaInterface));
      String unversionedFileName =  namespaceFileNameAndLocationResolver.resolveWsdlDocumentUnversionedFileName(serviceMetaInterface);
      wsdlDocument.setFileName((unversionedFileName + namespaceFileNameAndLocationResolver.resolveVersion(serviceMetaInterface) + ".wsdl").replace(' ', '_'));
      result.getMetaClazzToWsdlDocumentMapping().put(serviceMetaInterface, wsdlDocument);
      result.getWsdlDocuments().add(wsdlDocument);
      serviceMetaInterface.setAttribute(NAMESPACE, namespace);

      // Create Wrapper xmlSchema
      if (createWrapperSchema) {
        XmlSchema wrapperSchema = new XmlSchema(namespace, wsdlDocument.getNamespacePrefix(), true);
        wrapperSchema.setRootRelativePath(resolveRootRelativePath(namespace, serviceMetaInterface));
        String suffix =  namespaceFileNameAndLocationResolver.resolveWsdlDocumentWrapperSchemaSuffix(serviceMetaInterface);
        wrapperSchema.setFileName((serviceMetaInterface.getAliasOrName() + namespaceFileNameAndLocationResolver.resolveVersion(serviceMetaInterface) + suffix + ".xsd").replace(' ', '_'));
        wsdlDocument.setWrapperSchema(wrapperSchema);
        result.getNamespaceToXmlSchemaMapping().put(namespace, wrapperSchema);
        result.getXmlSchemas().add(wrapperSchema);
        result.getMetaClazzToXmlSchemaMapping().put(serviceMetaInterface, wrapperSchema);
      }
    }


  }

  protected void stage2CreateWsdlDocument(MetaClazz serviceMetaInterface,
                                          WsGenerationResult result) {
    WsdlDocument wsdlDocument = result.getMetaClazzToWsdlDocumentMapping().get(serviceMetaInterface);
    String namespace = resolveNamespace(serviceMetaInterface);
    Document xmlDefinition = wsdlDocument.getXmlDefinition();
    List<Element> messages = new LinkedList<Element>();
    List<Element> portTypeOperations = new LinkedList<Element>();
    List<Element> bindingOperations = new LinkedList<Element>();
    for (MetaOperation metaOperation : serviceMetaInterface.getOperations()) {
      if (createWrapperSchema) {
        createWrapperSchemaElements(wsdlDocument, metaOperation, result);
      }
      createWsdlMessages(wsdlDocument, messages, metaOperation, result);

      createWsdlPortTypeOperation(wsdlDocument, portTypeOperations,
        metaOperation);

      createWsdlBindingOperation(wsdlDocument, bindingOperations, metaOperation);
    }

    // Messages
    xmlDefinition.getRootElement().addContent(messages);

    // Port Type
    Element portTypeElement = new Element("portType", WSDL_NAMESPACE);
    portTypeElement.setAttribute("name", serviceMetaInterface.getAliasOrName() + "PortType");
    portTypeElement.addContent(portTypeOperations);
    xmlDefinition.getRootElement().addContent(portTypeElement);

    // Binding
    Element bindingElement = new Element("binding", WSDL_NAMESPACE)
      .setAttribute("name", serviceMetaInterface.getAliasOrName() + "Binding")
      .setAttribute("type", wsdlDocument.getNamespacePrefix() + ":" + serviceMetaInterface.getAliasOrName() + "PortType")
      .addContent(
        new Element("binding", SOAP_NAMESPACE)
          .setAttribute("style", "document")
          .setAttribute("transport", "http://schemas.xmlsoap.org/soap/http")
      )
      .addContent(bindingOperations);
    xmlDefinition.getRootElement().addContent(bindingElement);

    // Service
    // Documentation
    Element docElem = new Element("documentation", WSDL_NAMESPACE);
    if (serviceMetaInterface.getDocumentation() != null && !"".equals(serviceMetaInterface.getDocumentation())) {
      docElem.setText(serviceMetaInterface.getDocumentation());
    } else {
    	docElem.setText("");
    }

    
    String url = namespace + "/" + serviceMetaInterface.getAliasOrName().replace(' ', '_');
    Element serviceElement = new Element("service", WSDL_NAMESPACE)
      .setAttribute("name", serviceMetaInterface.getAliasOrName())
      .addContent(docElem)
      .addContent(
        new Element("port", WSDL_NAMESPACE)
          .setAttribute("name", serviceMetaInterface.getAliasOrName() + "Port")
          .setAttribute("binding", wsdlDocument.getNamespacePrefix() + ":" + serviceMetaInterface.getAliasOrName() + "Binding")
          .addContent(
          new Element("address", SOAP_NAMESPACE)
            .setAttribute("location", url.startsWith("http") ? url : "http://localhost/" + url)
        )
      );
    xmlDefinition.getRootElement().addContent(serviceElement);
    
    soapHeaderHandler.applySoapHeaderImports(serviceMetaInterface, wsdlDocument, namespace);
  }

  protected Element createWsdlDocumentation(MetaClazz serviceMetaInterface) {
    return new Element("documentation", WSDL_NAMESPACE)
      .setText("Based on Service Interface: " + serviceMetaInterface.getFQCNUsingAliasIfAvailable());
  }

  protected void createWrapperSchemaElements(WsdlDocument wsdlDocument,
                                             MetaOperation metaOperation, WsGenerationResult result) {

    // Define the element and complex type for the parameters
    Element requestElement = new Element("element", XSD_NAMESPACE);
    requestElement.setAttribute("name", metaOperation.getAliasOrName() + wrapperElementNameSuffix);

    Element requestComplexType = new Element("complexType", XSD_NAMESPACE);
    Element sequence = new Element("sequence", XSD_NAMESPACE);
    requestComplexType.addContent(sequence);

    for (MetaParameter metaParameter : metaOperation.getParameters()) {
      if (metaParameter.getDirection() == MetaParameter.Direction.In || metaParameter.getDirection() == MetaParameter.Direction.InOut) {
        Element parameterElement = new Element("element", XSD_NAMESPACE);
        parameterElement.setAttribute("name", metaParameter.getAliasOrName());

        if (metaParameter.getType().isBuiltInType()) {
          parameterElement.setAttribute("type", XSD_NAMESPACE.getPrefix() + ":" + simpleTypeResolver.resolveXsdType(metaParameter));
        } else {
          XmlSchema propertyTypeXmlSchema = result.getMetaClazzToXmlSchemaMapping(metaParameter.getType());
          parameterElement.setAttribute("type", propertyTypeXmlSchema.getNamespacePrefix() + ":" + metaParameter.getType().getAliasOrName());

          wsdlDocument.getWrapperSchema().getReferencedXmlSchemas().add(propertyTypeXmlSchema);
        }
        parameterElement.setAttribute("minOccurs", "1");
        parameterElement.setAttribute("maxOccurs", "1");
        parameterElement.setAttribute("nillable", "false");
        sequence.addContent(parameterElement);
      }
    }

    requestElement.addContent(requestComplexType);
    wsdlDocument.getWrapperSchema().getXmlDefinition().getRootElement().addContent(requestElement);


    // Return param
    Element responseElement = new Element("element", XSD_NAMESPACE);
    responseElement.setAttribute("name", metaOperation.getAliasOrName() + wrapperElementNameSuffix + "Response");

    Element reponseComplexType = new Element("complexType", XSD_NAMESPACE);
    Element responseSequence = new Element("sequence", XSD_NAMESPACE);
    reponseComplexType.addContent(responseSequence);

    if (metaOperation.getReturnParameter().getType() != null && !"void".equalsIgnoreCase(metaOperation.getReturnParameter().getType().getAliasOrName())) {
      Element returnParameterElement = new Element("element", XSD_NAMESPACE);
      returnParameterElement.setAttribute("name", metaOperation.getReturnParameter().getType().getAliasOrName());

      if (metaOperation.getReturnParameter().getType().isBuiltInType()) {
        returnParameterElement.setAttribute("type", XSD_NAMESPACE.getPrefix() + ":" + simpleTypeResolver.resolveXsdType(metaOperation.getReturnParameter()));
      } else {
        XmlSchema propertyTypeXmlSchema = result.getMetaClazzToXmlSchemaMapping(metaOperation.getReturnParameter().getType());
        returnParameterElement.setAttribute("type", propertyTypeXmlSchema.getNamespacePrefix() + ":" + metaOperation.getReturnParameter().getType().getAliasOrName());

        wsdlDocument.getWrapperSchema().getReferencedXmlSchemas().add(propertyTypeXmlSchema);
      }
      returnParameterElement.setAttribute("minOccurs", "1");
      returnParameterElement.setAttribute("maxOccurs", "1");
      returnParameterElement.setAttribute("nillable", "false");
      responseSequence.addContent(returnParameterElement);
    }
    for (MetaParameter metaParameter : metaOperation.getParameters()) {
      if (metaParameter.getDirection() == MetaParameter.Direction.Out || metaParameter.getDirection() == MetaParameter.Direction.InOut) {
        Element parameterElement = new Element("element", XSD_NAMESPACE);
        parameterElement.setAttribute("name", metaParameter.getAliasOrName());

        if (metaParameter.getType().isBuiltInType()) {
          parameterElement.setAttribute("type", XSD_NAMESPACE.getPrefix() + ":" + simpleTypeResolver.resolveXsdType(metaParameter));
        } else {
          XmlSchema propertyTypeXmlSchema = result.getMetaClazzToXmlSchemaMapping(metaParameter.getType());
          parameterElement.setAttribute("type", propertyTypeXmlSchema.getNamespacePrefix() + ":" + metaParameter.getType().getAliasOrName());

          wsdlDocument.getWrapperSchema().getReferencedXmlSchemas().add(propertyTypeXmlSchema);
        }
        parameterElement.setAttribute("minOccurs", "1");
        parameterElement.setAttribute("maxOccurs", "1");
        parameterElement.setAttribute("nillable", "false");
        responseSequence.addContent(parameterElement);
      }
    }


    responseElement.addContent(reponseComplexType);
    wsdlDocument.getWrapperSchema().getXmlDefinition().getRootElement().addContent(responseElement);

  }

  private void createWsdlBindingOperation(WsdlDocument wsdlDocument,
                                          List<Element> bindingOperations, MetaOperation metaOperation) {
    Element bindingOperation = new Element("operation", WSDL_NAMESPACE)
      .setAttribute("name", metaOperation.getAliasOrName());
    
    // Documentation
    if (metaOperation.getDocumentation() != null && !"".equals(metaOperation.getDocumentation())) {
    	Element docElem = new Element("documentation", WSDL_NAMESPACE);
      docElem.setText(metaOperation.getDocumentation());
			bindingOperation.addContent(docElem);
    }

    bindingOperation.addContent(
        new Element("operation", SOAP_NAMESPACE)
          .setAttribute("soapAction", metaOperation.getAliasOrName())
          .setAttribute("style", "document")
      );

    Element operationInput = new Element("input", WSDL_NAMESPACE);
    bindingOperation.addContent(operationInput);
    Element soapBodyElement = new Element("body", SOAP_NAMESPACE);
    soapBodyElement.setAttribute("parts", metaOperation.getAliasOrName() + "Parameters");
    soapBodyElement.setAttribute("use", "literal");
    operationInput.addContent(soapBodyElement);

    Element operationOutput = null;
    if (createWrapperSchema || (metaOperation.getReturnParameter().getType() != null && !"void".equalsIgnoreCase(metaOperation.getReturnParameter().getType().getAliasOrName()))) {
	    operationOutput = new Element("output", WSDL_NAMESPACE);
	    bindingOperation.addContent(operationOutput);
	    soapBodyElement = new Element("body", SOAP_NAMESPACE);
	    soapBodyElement.setAttribute("parts", metaOperation.getAliasOrName() + "ResponseParameters");
	    soapBodyElement.setAttribute("use", "literal");
	    operationOutput.addContent(soapBodyElement);
    }

    soapFaultHandler.applyWsdlBindingFaults(metaOperation, wsdlDocument, bindingOperation);
    soapHeaderHandler.applyWsdlBindingHeaders(metaOperation, wsdlDocument, bindingOperation, operationInput, operationOutput);
    bindingOperations.add(bindingOperation);
  }

  private void createWsdlPortTypeOperation(WsdlDocument wsdlDocument,
                                           List<Element> portTypeOperations, MetaOperation metaOperation) {
    Element portTypeOperation = new Element("operation", WSDL_NAMESPACE);
    portTypeOperation.setAttribute("name", metaOperation.getAliasOrName());
    
    // Documentation
    if (metaOperation.getDocumentation() != null && !"".equals(metaOperation.getDocumentation())) {
    	Element docElem = new Element("documentation", WSDL_NAMESPACE);
      docElem.setText(metaOperation.getDocumentation());
			portTypeOperation.addContent(docElem);
    }
    
    Element operationInput = new Element("input", WSDL_NAMESPACE);
    operationInput.setAttribute("name", metaOperation.getAliasOrName() + "Parameters");
    operationInput.setAttribute("message", wsdlDocument.getNamespacePrefix() + ":" + metaOperation.getAliasOrName());
    portTypeOperation.addContent(operationInput);
    if (createWrapperSchema || (metaOperation.getReturnParameter().getType() != null && !"void".equalsIgnoreCase(metaOperation.getReturnParameter().getType().getAliasOrName()))) {
	    Element operationOutput = new Element("output", WSDL_NAMESPACE);
	    operationOutput.setAttribute("name", metaOperation.getAliasOrName() + "ResponseParameters");
	    operationOutput.setAttribute("message", wsdlDocument.getNamespacePrefix() + ":" + metaOperation.getAliasOrName() + "Response");
	    portTypeOperation.addContent(operationOutput);
    }

    soapFaultHandler.applyPortTypeFaults(wsdlDocument, metaOperation, portTypeOperation);

    portTypeOperations.add(portTypeOperation);
  }

  protected void createWsdlMessages(WsdlDocument wsdlDocument,
                                    List<Element> messages, MetaOperation metaOperation, WsGenerationResult result) {
    if (createWrapperSchema) {
      Element requestMessage = new Element("message", WSDL_NAMESPACE);
      requestMessage.setAttribute("name", metaOperation.getAliasOrName());
      Element requestMessagePart = new Element("part", WSDL_NAMESPACE);
      requestMessagePart.setAttribute("name", metaOperation.getAliasOrName() + "Parameters");
      requestMessagePart.setAttribute("element", wsdlDocument.getNamespacePrefix() + ":" + metaOperation.getAliasOrName() + wrapperElementNameSuffix);
      requestMessage.addContent(requestMessagePart);
      messages.add(requestMessage);

      Element responseMessage = new Element("message", WSDL_NAMESPACE);
      responseMessage.setAttribute("name", metaOperation.getAliasOrName() + "Response");
      Element responseMessagePart = new Element("part", WSDL_NAMESPACE);
      responseMessagePart.setAttribute("name", metaOperation.getAliasOrName() + "ResponseParameters");
      responseMessagePart.setAttribute("element", wsdlDocument.getNamespacePrefix() + ":" + metaOperation.getAliasOrName() + wrapperElementNameSuffix + "Response");
      responseMessage.addContent(responseMessagePart);
      messages.add(responseMessage);
    } else {
      if (metaOperation.getParameters().size() > 1) {
        throw new IllegalStateException("Can't create without Wrapper Schema for operation with more than 1 parameter: " + metaOperation);
      }
      Element requestMessage = new Element("message", WSDL_NAMESPACE);
      requestMessage.setAttribute("name", metaOperation.getAliasOrName());
      Element requestMessagePart = new Element("part", WSDL_NAMESPACE);
      requestMessagePart.setAttribute("name", metaOperation.getAliasOrName() + "Parameters");

      final MetaClazz requestParamType = metaOperation.getParameters().get(0).getType();
      if (requestParamType == null) {
        throw new IllegalStateException("No Type for request parameter in operation " + metaOperation + " in " + metaOperation.getOwner());
      }
      XmlSchema requestSchema = result.getMetaClazzToXmlSchemaMapping().get(requestParamType);
      if (requestSchema == null) {
        throw new IllegalStateException("No Request Schema for request param " + requestParamType + " in operation " + metaOperation + " in " + metaOperation.getOwner());
      }
      requestMessagePart.setAttribute("element", requestSchema.getNamespacePrefix() + ":" + metaOperation.getParameters().get(0).getType().getAliasOrName());
      requestMessage.addContent(requestMessagePart);
      messages.add(requestMessage);
      wsdlDocument.getReferencedXmlSchemas().add(requestSchema);

      if (metaOperation.getReturnParameter() != null && metaOperation.getReturnParameter().getType() != null && !"void".equals(metaOperation.getReturnParameter().getType().getAliasOrName())) {
	        Element responseMessage = new Element("message", WSDL_NAMESPACE);
		      responseMessage.setAttribute("name", metaOperation.getAliasOrName() + "Response");
		      Element responseMessagePart = new Element("part", WSDL_NAMESPACE);
		      responseMessagePart.setAttribute("name", metaOperation.getAliasOrName() + "ResponseParameters");
		      
		      XmlSchema responseSchema = result.getMetaClazzToXmlSchemaMapping().get(metaOperation.getReturnParameter().getType());
		      responseMessagePart.setAttribute("element", responseSchema.getNamespacePrefix() + ":" + metaOperation.getReturnParameter().getType().getAliasOrName());
		      responseMessage.addContent(responseMessagePart);
		      messages.add(responseMessage);
		      wsdlDocument.getReferencedXmlSchemas().add(responseSchema);
      }

    }

    soapFaultHandler.createFaultMessages(wsdlDocument, messages, metaOperation, result);
  }

  public boolean isInlineReferencedSchemasInWsdlDocument() {
    return inlineReferencedSchemasInWsdlDocument;
  }

  public SimpleTypeResolver getSimpleTypeResolver() {
    return simpleTypeResolver;
  }

  public NamespaceFileNameAndLocationResolver getNamespaceFileNameAndLocationResolver() {
    return namespaceFileNameAndLocationResolver;
  }

  public SoapFaultHandler getSoapFaultHandler() {
    return soapFaultHandler;
  }
}
