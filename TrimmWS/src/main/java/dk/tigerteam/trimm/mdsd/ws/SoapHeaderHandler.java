/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaOperation;
import org.jdom2.Element;

/**
 * TODO: doc...
 * 
 * @author Jeppe Cramon
 */
public interface SoapHeaderHandler {

	/**
	 * Add any needed SoapHeader imports and namespace definitions, e.g. 
	 * <pre>
	 *&lt;wsdl:definitions
	 * 	xmlns:tns="{namespace}"
	 * 	xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	 * 	xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
	 * 	<b>xmlns:header="{header namespace}"</b>
	 * 	targetNamespace={namespace}">
	 * 	<b>&lt;wsdl:import
	 * 		namespace="{header namespace}"
	 * 		location="{wsdl location fo the header definition if needed}" /></b>
	 * 	...
	 * </pre>
	 * @param serviceMetaInterface The WS Interface that we're generating WSDL document for
	 * @param wsdlDocument The WSDL document for the serviceMetaInterface, which contains the XmlDefinition that we should add the import statements to
	 * @param namespace The service namespace
	 * @see WsdlDocument#WSDL_NAMESPACE
	 * @see WsdlDocument#SOAP_NAMESPACE
	 * @see XmlSchema#XSD_NAMESPACE
	 */
	void applySoapHeaderImports(MetaClazz serviceMetaInterface, WsdlDocument wsdlDocument, String namespace);

	/**
	 * Add any SOAP headers to the WSDL Binding definition for the current MetaOperation, e.g.
	 * <pre>
	 * 		&lt;wsdl:operation name="{operation name}">
	 * 			&lt;soap:operation soapAction="" style="document" />
	 * 			&lt;wsdl:input>
	 * 				&lt;soap:body parts="parameters" use="literal" />
	 * 		<b>		&lt;soap:header message="{header prefix}:{SOAP header message name}" part="{SOAP header part}"
	 * 					use="literal" /></b>
	 * 			&lt;/wsdl:input>
	 * 			&lt;wsdl:output>
	 * 				&lt;soap:body parts="parameters" use="literal" />
	 * 			&lt;/wsdl:output>
	 * 			&lt;wsdl:fault name="{Some Fault}">
	 * 				&lt;soap:fault name="{Fault name}" use="literal" />
	 * 			&lt;/wsdl:fault>
	 * 			&lt;wsdl:fault name="{Some other fault}">
	 * 				&lt;soap:fault name="{Some other fault name}" use="literal" />
	 * 			&lt;/wsdl:fault>
	 * 		&lt;/wsdl:operation>
	 * </pre>
	 * @param metaOperation The operation that we're currently processing WSDL binding for
	 * @param wsdlDocument The WSDL document containing the wsdl definition of the Meta interface that the MetaOperation belongs to
	 * @param bindingOperation The XML structure containing the WSDL Binding for the operation
	 * @param operationOutput The WSDL input section
	 * @param operationInput The WSDL output section
	 */
	void applyWsdlBindingHeaders(MetaOperation metaOperation, WsdlDocument wsdlDocument, Element bindingOperation, Element operationInput, Element operationOutput);

}
