/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.generator;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaOperation;
import dk.tigerteam.trimm.mdsd.ws.SoapHeaderHandler;
import dk.tigerteam.trimm.mdsd.ws.WsdlDocument;
import dk.tigerteam.trimm.mdsd.ws.XmlSchema;
import dk.tigerteam.trimm.util.FileSystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom2.Element;
import org.jdom2.Namespace;

import static dk.tigerteam.trimm.mdsd.ws.WsdlDocument.WSDL_NAMESPACE;

/**
 * Currently only handles WSDL (which defines the complete Message and part) and file system based XSD import (where it creates the Header message) 
 * 
 * @author Jeppe Cramon
 */
public class OneHeaderSoapHeaderHandler implements SoapHeaderHandler {
	private Log log = LogFactory.getLog(OneHeaderSoapHeaderHandler.class);
	private SoapHeaderConfiguration soapHeaderConfiguration;
	private Namespace headerNamespace;

	public OneHeaderSoapHeaderHandler(SoapHeaderConfiguration soapHeaderConfiguration) {
		super();
		this.soapHeaderConfiguration = soapHeaderConfiguration;
		headerNamespace = Namespace.getNamespace(soapHeaderConfiguration.getNamespacePrefix(), soapHeaderConfiguration.getNamespace());
	}

	@Override
	public void applySoapHeaderImports(MetaClazz serviceMetaInterface, WsdlDocument wsdlDocument, String namespace) {
		log.debug("Adding Namespace " + soapHeaderConfiguration.getNamespacePrefix() + ":" + soapHeaderConfiguration.getNamespace()
				+ " in  WsdlDocument " + wsdlDocument.getFileName() + "  in " + wsdlDocument.getRootRelativePath());
		wsdlDocument.getXmlDefinition().getRootElement().addNamespaceDeclaration(headerNamespace);

		if (soapHeaderConfiguration.getLocation().endsWith(".wsdl")) {
			addWsdlImport(wsdlDocument);
		} else if (soapHeaderConfiguration.getLocation().endsWith(".xsd")) {
			addXmlSchemaImport(wsdlDocument);
			addHeaderMessage(wsdlDocument);
		} else
			throw new IllegalStateException("Can't add header for location '" + soapHeaderConfiguration.getLocation()
					+ "'. Must end with .wsdl or .xsd");
	}

	private void addHeaderMessage(WsdlDocument wsdlDocument) {
		Element headerMessage = new Element("message", WSDL_NAMESPACE);
		headerMessage.setAttribute("name", soapHeaderConfiguration.getWsdlMessageName());
		Element headerPart = new Element("part", WSDL_NAMESPACE);
		headerPart.setAttribute("name", soapHeaderConfiguration.getWsdlMessagePart());
		headerPart.setAttribute("element", soapHeaderConfiguration.getNamespacePrefix() + ":" + soapHeaderConfiguration.getWsdlMessageElement());
		headerMessage.addContent(headerPart);
		wsdlDocument.getXmlDefinition().getRootElement().addContent(0, headerMessage);
	}

	private void addXmlSchemaImport(WsdlDocument wsdlDocument) {
		XmlSchema xmlSchema = new XmlSchema(soapHeaderConfiguration.getNamespace(), soapHeaderConfiguration.getNamespacePrefix(), false);
		int lastIndexOf = soapHeaderConfiguration.getLocation().lastIndexOf('/');
		if (lastIndexOf == -1) {
			xmlSchema.setFileName(soapHeaderConfiguration.getLocation());
			xmlSchema.setRootRelativePath("");
		} else {
			xmlSchema.setFileName(soapHeaderConfiguration.getLocation().substring(lastIndexOf + 1));
		//	String correctRelativePath = FileSystemUtils.correctRelativePath(wsdlDocument.getRootRelativePath(),
		//			soapHeaderConfiguration.getLocation());
			xmlSchema.setRootRelativePath(soapHeaderConfiguration.getLocation().substring(0, soapHeaderConfiguration.getLocation().lastIndexOf('/')));
		}
		wsdlDocument.getReferencedXmlSchemas().add(xmlSchema);
	}

	private void addWsdlImport(WsdlDocument wsdlDocument) {
		Element importElement = new Element("import", WsdlDocument.WSDL_NAMESPACE).setAttribute("namespace",
				soapHeaderConfiguration.getNamespace());
		if (soapHeaderConfiguration.getLocation() != null) {
			if (soapHeaderConfiguration.getLocation().startsWith("http")) {
				importElement.setAttribute("location", soapHeaderConfiguration.getLocation());
				log.debug("Adding wsdl import for namespace " + soapHeaderConfiguration.getNamespace() + " and location "
						+ soapHeaderConfiguration.getLocation());
			} else {
				String correctRelativePath = FileSystemUtils.correctRelativePath(wsdlDocument.getRootRelativePath(),
						soapHeaderConfiguration.getLocation());
				importElement.setAttribute("location", correctRelativePath);
				log.debug("Adding wsdl import for namespace " + soapHeaderConfiguration.getNamespace() + " and relative location "
						+ correctRelativePath);
			}
		} else {
			log.debug("No Header location provided");
		}

		wsdlDocument.getXmlDefinition().getRootElement().addContent(0, importElement);
	}

	@Override
	public void applyWsdlBindingHeaders(MetaOperation metaOperation, WsdlDocument wsdlDocument, Element bindingOperation,
			Element operationInput, Element operationOutput) {
		log.debug("Adding Soap Header to Operation " + metaOperation + " in  WsdlDocument " + wsdlDocument.getFileName() + "  in "
				+ wsdlDocument.getRootRelativePath());
		
		String headerMessage = null;
		boolean isXsdBasedHeader = soapHeaderConfiguration.getLocation().endsWith(".xsd");
		if (isXsdBasedHeader) {
			headerMessage = wsdlDocument.getNamespacePrefix() + ":" + soapHeaderConfiguration.getWsdlMessageName();
		} else {
			headerMessage = headerNamespace.getPrefix() + ":" + soapHeaderConfiguration.getWsdlMessageName();
		}
		
		operationInput.addContent(new Element("header", WsdlDocument.SOAP_NAMESPACE)
				.setAttribute("message", headerMessage)
				.setAttribute("part", soapHeaderConfiguration.getWsdlMessagePart()).setAttribute("use", "literal"));
		operationOutput.addContent(new Element("header", WsdlDocument.SOAP_NAMESPACE)
				.setAttribute("message", headerMessage)
				.setAttribute("part", soapHeaderConfiguration.getWsdlMessagePart()).setAttribute("use", "literal"));
	}

	public SoapHeaderConfiguration getSoapHeaderConfiguration() {
		return soapHeaderConfiguration;
	}

	public static class SoapHeaderConfiguration {
		private String namespacePrefix = null;
		private String namespace = null;
		private String location = null;
		private String wsdlMessageName = null;
		private String wsdlMessagePart = null;
		private String wsdlMessageElement = null;
		public SoapHeaderConfiguration(String namespacePrefix, String namespace, String location, String wsdlMessageName,
				String wsdlMessagePart, String wsdlMessageElement) {
			super();
			this.namespacePrefix = namespacePrefix;
			this.namespace = namespace;
			this.location = location;
			this.wsdlMessageName = wsdlMessageName;
			this.wsdlMessagePart = wsdlMessagePart;
			this.wsdlMessageElement = wsdlMessageElement;
		}
		public String getNamespacePrefix() {
			return namespacePrefix;
		}
		public String getNamespace() {
			return namespace;
		}
		public String getLocation() {
			return location;
		}
		public String getWsdlMessageName() {
			return wsdlMessageName;
		}
		public String getWsdlMessagePart() {
			return wsdlMessagePart;
		}
		public String getWsdlMessageElement() {
			return wsdlMessageElement;
		}
		@Override
		public String toString() {
			return "SoapHeaderConfiguration [" + (namespacePrefix != null ? "namespacePrefix=" + namespacePrefix + ", " : "")
					+ (namespace != null ? "namespace=" + namespace + ", " : "") + (location != null ? "location=" + location + ", " : "")
					+ (wsdlMessageName != null ? "wsdlMessageName=" + wsdlMessageName + ", " : "")
					+ (wsdlMessagePart != null ? "wsdlMessagePart=" + wsdlMessagePart + ", " : "")
					+ (wsdlMessageElement != null ? "wsdlMessageElement=" + wsdlMessageElement : "") + "]";
		}
	}

}
