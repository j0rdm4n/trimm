/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.wsdlimport;

import dk.tigerteam.trimm.mdsd.meta.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ws.commons.schema.*;

import javax.xml.namespace.QName;
import java.util.*;

/**
 * Class which specializes in Xml schema (XSD) to MetaModel transformation
 */
public class Xsd2MetaModel extends AbstractXml2MetaModel {
    private static final Log log = LogFactory.getLog(Xsd2MetaModel.class);

    public void transform(MetaModel metaModel, XmlSchemaCollection xmlSchemaCollection) {
        metaModel.setAttribute(XmlAttributeKeys.ElementQName2ComplexTypeQName.name(), new HashMap<QName, QName>());
           

        List<MetaClazz> allClazzesCreated = new LinkedList<MetaClazz>();
        for (XmlSchema xmlSchema : xmlSchemaCollection.getXmlSchemas()) {
            allClazzesCreated.addAll(firstPassTransformation(xmlSchema, xmlSchemaCollection, metaModel));
        }

        for (MetaClazz metaClazz : allClazzesCreated) {
            secondPassTransform(metaClazz, xmlSchemaCollection, metaModel);
        }
    }

    @SuppressWarnings("unchecked")
	private List<MetaClazz> firstPassTransformation(XmlSchema xmlSchema, XmlSchemaCollection xmlSchemaCollection, MetaModel metaModel) {
        List<MetaClazz> clazzesCreated = new LinkedList<MetaClazz>();

        Iterator iterator = xmlSchema.getItems().getIterator();
        while (iterator.hasNext()) {
            XmlSchemaObject schemaObject = (XmlSchemaObject) iterator.next();

            if (schemaObject instanceof XmlSchemaElement) {
                // Handle Element alias'
                XmlSchemaElement element = (XmlSchemaElement) schemaObject;
                if (element.getSchemaType() == null) {
                    throw new IllegalStateException("Didn't expect an element without SchemaType as this point - " + schemaObject);
                }
                QName elementQName = element.getQName();
                QName realTypeQName = element.getSchemaType().getQName();
                Map<QName, QName> map = (Map<QName, QName>) metaModel.getAttribute(XmlAttributeKeys.ElementQName2ComplexTypeQName.name());
                map.put(elementQName, realTypeQName);
            }

            if (!(schemaObject instanceof XmlSchemaComplexType || schemaObject instanceof XmlSchemaElement)) {
                continue;
            }

            QName qName = null;
            String name = null;
            if (schemaObject instanceof XmlSchemaComplexType) {
            	qName = ((XmlSchemaComplexType) schemaObject).getQName();
                name = ((XmlSchemaComplexType) schemaObject).getName();
            } else if (schemaObject instanceof XmlSchemaElement) {
            	qName = ((XmlSchemaElement) schemaObject).getQName();
                name = ((XmlSchemaElement) schemaObject).getName();
            } else {
            	// Go no further ;)
            	continue;
            }
            
            
            MetaPackage metaPackage = resolveMetaPackageForNamespace(xmlSchema.getTargetNamespace(), metaModel);
            MetaClazz metaClazz = createWithId(MetaClazz.class);
			metaClazz.setName(name);
            metaClazz.setAttribute(XmlAttributeKeys.XmlSchemaObject.name(), schemaObject);
            metaClazz.setAttribute(XmlAttributeKeys.XmlSchema.name(), xmlSchema);
            
			metaClazz.setAttribute(XmlAttributeKeys.QName.name(), qName);
            metaPackage.add(metaClazz);
            clazzesCreated.add(metaClazz);
            if (log.isDebugEnabled()) {
                log.debug("Defined " + metaClazz + " in package " + metaPackage + " based on " + schemaObject);
            }
        }
        return clazzesCreated;
    }

    private void secondPassTransform(MetaClazz metaClazz, XmlSchemaCollection xmlSchemaCollection, MetaModel metaModel) {
        XmlSchemaObject schemaObject = (XmlSchemaObject) metaClazz.getAttribute(XmlAttributeKeys.XmlSchemaObject.name());
        XmlSchema xmlSchema = (XmlSchema) metaClazz.getAttribute(XmlAttributeKeys.XmlSchema.name());

        if (schemaObject instanceof XmlSchemaComplexType) {
            handleComplexType2PassTransformation(metaClazz, (XmlSchemaComplexType)schemaObject, xmlSchema, xmlSchemaCollection, metaModel);
        } else if (schemaObject instanceof XmlSchemaElement && ((XmlSchemaElement)schemaObject).getSchemaType() instanceof XmlSchemaComplexType) { 
            handleComplexType2PassTransformation(metaClazz, (XmlSchemaComplexType)((XmlSchemaElement)schemaObject).getSchemaType(), xmlSchema, xmlSchemaCollection, metaModel);
        } else {
            throw new IllegalStateException("Unexpected SchemaObject " + schemaObject);
        }
        
    }

    private void handleComplexType2PassTransformation(MetaClazz metaClazz, XmlSchemaComplexType xmlSchemaComplexType, XmlSchema xmlSchema, XmlSchemaCollection xmlSchemaCollection, MetaModel metaModel) {
        if (xmlSchemaComplexType.getContentModel() instanceof XmlSchemaSimpleContent) {
            if (xmlSchemaComplexType.getContentModel().getContent() instanceof XmlSchemaSimpleContentExtension) {
                XmlSchemaSimpleContentExtension extension = (XmlSchemaSimpleContentExtension) xmlSchemaComplexType.getContentModel().getContent();
                SimpleType contentType = resolveSimpleType(extension.getBaseTypeName(), xmlSchemaCollection);
                // TODO: Add a simple property for the content
                MetaProperty contentProperty = (MetaProperty) createWithId(MetaProperty.class)
                    .setType((MetaClazz) createWithId(MetaClazz.class).setBuiltInType(true).setName(contentType.getBaseType()).setFabricated(true))
                    .setName("content")
                    .setAttribute(XmlAttributeKeys.SimpleType.name(), contentType);
                metaClazz.add(contentProperty);
                if (log.isDebugEnabled()) {
                    log.debug("Defined property " + contentProperty + " for SimpleContent extension in " + xmlSchemaComplexType);
                }

                // Handle attributes
                Iterator<XmlSchemaAttribute> attributesIterator = extension.getAttributes().getIterator();
                while (attributesIterator.hasNext()) {
                    XmlSchemaAttribute attribute = attributesIterator.next();
                    String name = attribute.getName();
                    SimpleType attributeType = resolveSimpleType(attribute, xmlSchemaCollection);
                    MetaProperty attributeProperty = (MetaProperty) createWithId(MetaProperty.class)
                        .setType((MetaClazz) createWithId(MetaClazz.class).setBuiltInType(true).setName(attributeType.getBaseType()).setFabricated(true))
                        .setName(name)
                        .setAttribute(XmlAttributeKeys.SimpleType.name(), attributeType);
                    // TODO: Default value, etc.
                    // TODO: Add simple property for the content
                    metaClazz.add(attributeProperty);
                    if (log.isDebugEnabled()) {
                        log.debug("Defined property " + attributeProperty + " for attribute " + attribute +" defined on SimpleContent extension in " + xmlSchemaComplexType);
                    }
                }
            } else if (xmlSchemaComplexType.getContentModel().getContent() instanceof XmlSchemaSimpleContentRestriction) {
                // Will this ever happen at this point? - Let's try and see
                throw new IllegalArgumentException("Can't handle ContentModel content of type " + xmlSchemaComplexType.getContentModel().getContent().getClass());
            } else {
                throw new IllegalArgumentException("Can't handle ContentModel content of type " + xmlSchemaComplexType.getContentModel().getContent().getClass());
            }

        } else {
            if (xmlSchemaComplexType.getParticle() instanceof XmlSchemaSequence) {
                Iterator<XmlSchemaObject> particleIterator = ((XmlSchemaSequence)xmlSchemaComplexType.getParticle()).getItems().getIterator();
                while (particleIterator.hasNext()) {
                    XmlSchemaObject particleItem = particleIterator.next();
                    if (particleItem instanceof XmlSchemaElement) {
                        XmlSchemaElement element = (XmlSchemaElement) particleItem;
                        String name = element.getName();
                        MetaProperty elementProperty = (MetaProperty) createWithId(MetaProperty.class)
                            .setName(name)
                            .setAttribute(XmlAttributeKeys.QName.name(), element.getQName())
                            .setAttribute(XmlAttributeKeys.Element.name(), element);
                        
                        elementProperty.setMinCardinality((int) element.getMinOccurs());
                        elementProperty.setMaxCardinality((int) element.getMaxOccurs());

                        MetaClazz elementType = resolveElementType(xmlSchemaComplexType.getQName(), element, xmlSchema, xmlSchemaCollection, metaModel);
                        if (elementType == null) {
                            SimpleType simpleType = resolveSimpleType(element, xmlSchemaCollection);
                            elementType = (MetaClazz) createWithId(MetaClazz.class).setBuiltInType(true).setName(simpleType.getBaseType()).setFabricated(true);
                            elementProperty.setAttribute(XmlAttributeKeys.SimpleType.name(), simpleType);
                        } else {
                            // It was an association to a Clazz that was part of the XmlSchema, let's create an association to it
                            elementProperty.setAggregation(MetaProperty.Aggregation.Composite);

                            // First - lets create a property at the opposite end (the owned end)
                            MetaProperty ownedProperty = (MetaProperty) createWithId(MetaProperty.class).setFabricated(true);
                            ownedProperty.setType(metaClazz);
                            elementType.add(ownedProperty);

                            MetaAssociation metaAssociation = createWithId(MetaAssociation.class);
                            metaAssociation.setOwnerProperty(elementProperty).setOwnedProperty(ownedProperty);
                            metaAssociation.setFabricated(true);


                        }

                        elementProperty.setType(elementType);
                        metaClazz.add(elementProperty);
                    } else {
                        throw new IllegalArgumentException("Can't handle Particle children of type " + particleItem.getClass() + " for SchemaObject " + xmlSchemaComplexType);
                    }
                }
                // Handle attributes
                Iterator<XmlSchemaAttribute> attributesIterator = xmlSchemaComplexType.getAttributes().getIterator();
                while (attributesIterator.hasNext()) {
                    XmlSchemaAttribute attribute = attributesIterator.next();
                    String name = attribute.getName();
                    SimpleType attributeType = resolveSimpleType(attribute, xmlSchemaCollection);
                    MetaProperty attributeProperty = (MetaProperty) createWithId(MetaProperty.class)
                        .setType((MetaClazz) createWithId(MetaClazz.class).setBuiltInType(true).setName(attributeType.getBaseType()).setFabricated(true))
                        .setName(name)
                        .setAttribute(XmlAttributeKeys.SimpleType.name(), attributeType);
                    // TODO: Default value, etc.
                    // TODO: Add simple property for the content
                    metaClazz.add(attributeProperty);
                    if (log.isDebugEnabled()) {
                        log.debug("Defined property " + attributeProperty + " for attribute " + attribute +" defined in " + xmlSchemaComplexType);
                    }
                }
            } else {
                throw new IllegalArgumentException("Can't handle Particle of type " + xmlSchemaComplexType.getParticle().getClass() + " for " + xmlSchemaComplexType);
            }
        }
    }

    private MetaClazz resolveElementType(final QName parentSchemaObjectQName, final XmlSchemaElement element, XmlSchema xmlSchema, XmlSchemaCollection xmlSchemaCollection, MetaModel metaModel) {
        if (element.getSchemaTypeName() != null) {
            return metaModel.findSingleMatch(new MetaCriteria<MetaClazz>(MetaClazz.class) {
                @Override
                public boolean isOk(MetaClazz metaClazz) {
                    return element.getSchemaTypeName().equals(metaClazz.getAttribute(XmlAttributeKeys.QName.name()));
                }
            });
        } else if (element.getSchemaType() instanceof XmlSchemaComplexType) {
            MetaPackage metaPackage = resolveMetaPackageForNamespace(xmlSchema.getTargetNamespace(), metaModel);
            MetaClazz metaClazz = createWithId(MetaClazz.class);
            metaClazz.setName(element.getName());
            metaClazz.setAttribute(XmlAttributeKeys.XmlSchemaObject.name(), element);
            metaClazz.setAttribute(XmlAttributeKeys.XmlSchema.name(), xmlSchema);
            metaClazz.setAttribute(XmlAttributeKeys.QName.name(), element.getQName());
            metaPackage.add(metaClazz);
            if (log.isDebugEnabled()) {
                log.debug("Defined " + metaClazz + " in package " + metaPackage + " based on " + element);
            }
            // Find a complexType inside this anonymous element
            handleComplexType2PassTransformation(metaClazz, (XmlSchemaComplexType) element.getSchemaType(), xmlSchema, xmlSchemaCollection, metaModel);
            
            return metaClazz;
        } else if (element.getSchemaType() instanceof XmlSchemaSimpleType) {
        	return null; // SimpleType resolving is handled in handleComplexType2PassTransformation (which called us)
        } else {
            throw new IllegalArgumentException("Can't handle anonymous element schematype " + element.getSchemaType());
        }
    }

    private SimpleType resolveSimpleType(XmlSchemaAttribute attribute, XmlSchemaCollection xmlSchemaCollection) {
        if (attribute.getSchemaType() != null) {
            return resolveSimpleType(attribute.getSchemaType(), xmlSchemaCollection);
        } else {
            return resolveSimpleType(attribute.getSchemaTypeName(), xmlSchemaCollection);
        }
    }

    private SimpleType resolveSimpleType(XmlSchemaElement element, XmlSchemaCollection xmlSchemaCollection) {
        if (element.getSchemaType() != null) {
            return resolveSimpleType((XmlSchemaSimpleType) element.getSchemaType(), xmlSchemaCollection);
        } else {
            return resolveSimpleType(element.getSchemaTypeName(), xmlSchemaCollection);
        }                                      
    }


    private SimpleType resolveSimpleType(XmlSchemaSimpleType schemaType, XmlSchemaCollection xmlSchemaCollection) {
        SimpleType simpleType = new SimpleType();
        resolveSimpleType(simpleType, schemaType, xmlSchemaCollection);
        return simpleType;
    }

    private SimpleType resolveSimpleType(QName baseTypeName, XmlSchemaCollection xmlSchemaCollection) {
        SimpleType simpleType = new SimpleType();
        resolveSimpleType(simpleType, baseTypeName, xmlSchemaCollection);
        return simpleType;
    }

    private void resolveSimpleType(SimpleType simpleType, QName baseTypeName, XmlSchemaCollection xmlSchemaCollection) {
        XmlSchemaSimpleType baseType = (XmlSchemaSimpleType) xmlSchemaCollection.getTypeByQName(baseTypeName);
        resolveSimpleType(simpleType, baseType, xmlSchemaCollection);

    }

    private void resolveSimpleType(SimpleType simpleType, XmlSchemaSimpleType baseType, XmlSchemaCollection xmlSchemaCollection) {
        // TODO: There more options than SimpleTypeRestriction, but it will suffice for now
        // TODO: Decide what to do about SimpleType which are restrictions with enum values (should we instead create a MetaEnumation?)
        if (baseType.getContent() != null) {
            simpleType.addFacets(((XmlSchemaSimpleTypeRestriction)baseType.getContent()).getFacets());
        }

        //if ("http://www.w3.org/2001/XMLSchema".equals(baseType.getQName().getNamespaceURI())) {
        if (baseType.getContent() == null) {
            // We've read the end of the road
            simpleType.setBaseType(baseType.getQName().getLocalPart());
        } else {
            resolveSimpleType(simpleType, ((XmlSchemaSimpleTypeRestriction)baseType.getContent()).getBaseTypeName(), xmlSchemaCollection);
        }
    }



}
