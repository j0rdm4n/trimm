/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.generator;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.NameableMetaType;
import dk.tigerteam.trimm.mdsd.uml2.xmi.XmiReader;
import dk.tigerteam.trimm.mdsd.ws.SoapHeaderHandler;
import dk.tigerteam.trimm.mdsd.ws.WsGenerationResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Easy to use WebServices generator - basically this is just a driver for DefaultWsGenerator with preselected
 * DefaultSimpleTypeResolver, DefaultNamespaceResolver and DefaultSoapHandler
 * 
 * @author Jeppe Cramon
 */
public class DefaultWsGenerator {
  public static void main(String[] args) throws IOException {
    String generateToFolder;// = "src/main/ws-resources";
    String modelFilePath;// = "src/main/model/servicemodel.xml";
    System.out.println("************************************");
    System.out.println("* TigerMDSD - WebService generator *");
    System.out.println("************************************");
    if (args.length > 0) {
      if (args.length == 2) {
        modelFilePath = args[0];
        generateToFolder = args[1];
      } else if (args.length == 3) {
        if ("--alias".equalsIgnoreCase(args[0])) {
          NameableMetaType.UseAliasIfAvailable = true;
        }
        modelFilePath = args[1];
        generateToFolder = args[2];
      } else {
        printUsage();
        return;
      }
    } else {
      printUsage();
      return;
    }

    generateAndWriteFiles(generateToFolder, modelFilePath, false, true, true, "", new NoHeaderSoapHeaderHandler());
  }
  
  public static WsGenerationResult generate(String modelFilePath,  boolean inlineReferencedSchemas, boolean createWrapperSchema, boolean inlineWrapperSchema, String wrapperSuffix, SoapHeaderHandler soapHeaderHandler) {
  	dk.tigerteam.trimm.mdsd.ws.WsGenerator wsGenerator = new dk.tigerteam.trimm.mdsd.ws.WsGenerator(
	      inlineReferencedSchemas, createWrapperSchema, inlineWrapperSchema, wrapperSuffix,
	      new DefaultSimpleTypeResolver(), new DefaultNamespaceFileNameAndLocationResolver(),
	      new DefaultSoapFaultHandler(), soapHeaderHandler) {
	      @Override
	      protected boolean shouldGenerateForMetaClazz(MetaClazz metaClazz) {
	        return !(metaClazz.getAliasOrName().equalsIgnoreCase("Request") || metaClazz.getAliasOrName().equalsIgnoreCase("Response"));
	      }
	    };
	    XmiReader xmiReader = WsGeneratorHelper.selectXmiReader(modelFilePath);
	    final MetaModel metaModel = xmiReader.read(new File(modelFilePath));
	
	    WsGenerationResult result = wsGenerator.generate(metaModel);
	    return result;
  }

	public static void generateAndWriteFiles(String generateToFolder, String modelFilePath, boolean inlineReferencedSchemas, boolean createWrapperSchema, boolean inlineWrapperSchema, String wrapperSuffix, SoapHeaderHandler soapHeaderHandler) throws FileNotFoundException, IOException {
		try {
			System.out.println("Model XMI file: " + modelFilePath);
	    System.out.println("Output folder : " + generateToFolder);
	    System.out.println("------------------------------------------------------");
	
	    dk.tigerteam.trimm.mdsd.ws.WsGenerator wsGenerator = new dk.tigerteam.trimm.mdsd.ws.WsGenerator(
	      inlineReferencedSchemas, createWrapperSchema, inlineWrapperSchema, wrapperSuffix,
	      new DefaultSimpleTypeResolver(), new DefaultNamespaceFileNameAndLocationResolver(),
	      new DefaultSoapFaultHandler(), soapHeaderHandler) {
	      @Override
	      protected boolean shouldGenerateForMetaClazz(MetaClazz metaClazz) {
	        return !(metaClazz.getAliasOrName().equalsIgnoreCase("Request") || metaClazz.getAliasOrName().equalsIgnoreCase("Response"));
	      }
	    };
	    XmiReader xmiReader = WsGeneratorHelper.selectXmiReader(modelFilePath);
	    final MetaModel metaModel = xmiReader.read(new File(modelFilePath));
	
	    WsGenerationResult result = wsGenerator.generate(metaModel);
	    WsGeneratorHelper.writeFiles(generateToFolder, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

  private static void printUsage() {
    System.out.println("Invalid parameters!");
    System.out.println("Usage:");
    System.out.println("createWsdl {--alias} [path-to-model.xml] [output-folder]");
    System.out.println("   --alias is optional. If specified then alias' (if available) on classes, properties, methods, parameters, etc. will be used in place if the artifacts name");
  }
}