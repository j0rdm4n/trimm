/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.generator;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.MetaPackage;
import dk.tigerteam.trimm.mdsd.meta.MetaType;
import dk.tigerteam.trimm.mdsd.ws.NamespaceFileNameAndLocationResolver;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * <h1>Default filename resolving</h1>
 * A given Class becomes a part of a Schema. The schemas file name IS the name of the package within which the class lives.
 * If a class is placed inside a folder which is tagged using the Tagged value <code>Namespace</code> then that
 * namespace value is used as Xml schema filename (stripping the http:// part).
 * <p/>
 * WSDL documents files get's the same name as the Interface that represents the service inside the WSDL document (no replacement of spaces)
 * <p/>
 * Wrapper XMl Schemas per default get the suffix "Wrapper"
 * <h1>Default NamespaceResolver implementation.</h1>
 * Namespace resolving for a given MetaClazz or MetaInterface:
 * <ol>
 *     <li>Look for a Package/Folder with a Tagged value called <code>Namespace</code> - the value of this Tagged value will become the root of the namespace that HAS to start with <code>>http://</code (e.g. <code>http://ws.tigerteam.dk/</code>)</li>
 *     <li>For every package/folder underneath the Tagged Package/Folder will have its Name/Alias added to the namespaces path (spaces will be replaced with an underscore '_') plus a separating slash '/'</li>
 * </ol>
 * If the xmlType is an <code>Interface</code> (which is what all Services as modeled as) then we will also add the interface name/alias as the final namespace part (spaces will be replaced with an underscore '_')
 *
 * Example hierarchy:
 * <ul>
 *     <li>Model (root)</li>
 *     <ul>
 *         <li>WebServices (with tagged value <code>Namespace</code> with value <code>http://ws.tigerteam.dk/"</code></li>
 *         <ul>
 *             <li>Folder/Package with name <code>"Customers"</code></li>
 *             <ul>
 *                 <li>Folder/Package with name <code>"Search"</code></li>
 *                 <ul>
 *                     <li>Interface with name <code>"Customer Search Service"</code></li>
 *                     <li>Class with name <code>"CustomerSearchCriteria"</code></li>
 *                     <li>Class with name <code>"CustomerSearchResult"</code></li>
 *                 </ul>
 *
 *             </ul>
 *         </ul>
 *     </ul>
 * </ul>
 * This will result in the following namespaces:  <br/>
 * Interface: <code>"Customer Search Service"</code> will get namespace <code>"http://ws.tigerteam.dk/Customers/Search/Customer_Search_Service"</code>    <br/>
 * Class: <code>"CustomerSearchCriteria"</code> will get namespace <code>"http://ws.tigerteam.dk/Customers/Search/"</code>                                   <br/>
 * Class: <code>"CustomerSearchResult"</code> will get namespace <code>"http://ws.tigerteam.dk/Customers/Search/"</code>
 * <p/>
 * This resolver is also responsible for calculating where the should the files within this namespace be placed on the file system<br/>
 * Example: Given "<code>http://tigerteam.dk/person/service/</code>" you would return "<code>person/service/</code>" <em>(note the missing / at the start of the path)</em>
 * <h1>Default File location</h1>
 * The default is to use the same file system nesting (using folders) as is in the original model (package becomes a folder)
 * @author Jeppe Cramon
 */
public class DefaultNamespaceFileNameAndLocationResolver implements NamespaceFileNameAndLocationResolver {
  private Map<MetaClazz, String> metaClazzToNamespaceCache = new HashMap<MetaClazz, String>();

  public String resolveNamespace(MetaClazz xmlType) {
    if (metaClazzToNamespaceCache.containsKey(xmlType)) {
      return metaClazzToNamespaceCache.get(xmlType);
    }

    List<String> namespaceParts = new LinkedList<String>();

    if (xmlType.isInterface()) {
      namespaceParts.add(xmlType.getAliasOrName().replace(' ', '_'));
    }

    MetaType currentMetaType = xmlType;
    MetaModel metaModel = xmlType.getMetaModel();
    boolean hasMetANamespaceTaggedValue = false;
    while (currentMetaType != metaModel) {
      if (currentMetaType.hasTaggedValue("Namespace") && currentMetaType.getTaggedValue("Namespace") != null && !currentMetaType.getTaggedValue("Namespace").isEmpty()) {
        final String namespace = currentMetaType.getTaggedValue("Namespace");
        hasMetANamespaceTaggedValue = true;
        namespaceParts.add(0, namespace);
        break;
      } else if (currentMetaType instanceof MetaPackage) {
      	namespaceParts.add(0, ((MetaPackage) currentMetaType).getAliasOrName().replace(' ', '_'));
      }
      currentMetaType = currentMetaType.getOwner();
    }

    if (!hasMetANamespaceTaggedValue) {
        throw new IllegalStateException(currentMetaType + " doesn't have a 'Namespace' tagged value on it or in its package chain. Location '" + currentMetaType.toString() + "'");
    }

    boolean appendSlash = false;
    StringBuilder namespace = new StringBuilder();
    for (String namespacePart : namespaceParts) {
      if (appendSlash) {
        namespace.append("/");
      }
      namespace.append(namespacePart);
      appendSlash = !namespacePart.endsWith("/");
    }

    if (namespace.toString().isEmpty()) {
      throw new IllegalArgumentException("Empty namespace for " + xmlType);
    }

    namespace.append(resolveVersion(xmlType));

    System.out.println("Resolved namespace " + namespace.toString() + " for " + xmlType);

    metaClazzToNamespaceCache.put(xmlType, namespace.toString());


    return namespace.toString();
  }

  public String resolveVersion(MetaClazz xmlType) {
    /*String version = null;
    if (xmlType.getTaggedValue("Version") != null && !xmlType.getTaggedValue("Version").isEmpty()) {
        version = xmlType.getTaggedValue("Version");
    } else {
        MetaType currentMetaType = xmlType;
        MetaModel metaModel = xmlType.getMetaModel();
        while (currentMetaType != metaModel) {
            if (currentMetaType.hasTaggedValue("Version") && currentMetaType.getTaggedValue("Version") != null && !currentMetaType.getTaggedValue("Version").isEmpty()) {
                version = currentMetaType.getTaggedValue("Version");
                break;
            }
            currentMetaType = currentMetaType.getOwner();
        }
    }

    if (version == null) {
        return "";
    } else {
        return "-v" + version;
    }*/
    return "";
  }

  public String resolveRootRelativePath(String namespace, MetaClazz xmlType) {
    int index = namespace.indexOf("http://");
    if (index == -1) {
      throw new IllegalStateException("Expected namespaces to start with http://domainname/ - if your requirements are different please consider writing a custom NamespaceResolver");
    }
    index = namespace.indexOf("/", index+"http://".length()+1);
    if (index == -1) {
      throw new IllegalStateException("Expected namespaces to start with http://domainname/ - if your requirements are different please consider writing a custom NamespaceResolver");
    }
    return namespace.substring(index+1);
  }

@Override
   public String resolveXmlSchemaUnversionedFileName(MetaClazz xmlType) {
       final String namespace = xmlType.getOwner().getTaggedValue("Namespace");
       if (namespace != null && !"".equals(namespace) && !namespace.startsWith("http://")) {
           return namespace;
       } else {
           return xmlType.getPackage().getAliasOrName();
       }
   }

   @Override
   public String resolveWsdlDocumentUnversionedFileName(MetaClazz serviceMetaInterface) {
       return serviceMetaInterface.getAliasOrName();
   }

   @Override
   public String resolveWsdlDocumentWrapperSchemaSuffix(MetaClazz serviceMetaInterface) {
       return "Wrapper";
   }
}
