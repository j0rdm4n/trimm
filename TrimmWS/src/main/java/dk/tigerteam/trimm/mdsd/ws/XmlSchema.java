/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

/**
 * 
 * @author Jeppe Cramon
 */
public class XmlSchema extends AbstractWsArtifact {
	
	public static final Namespace XSD_NAMESPACE = Namespace.getNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
	private boolean hasBeenInlined;
	
	public XmlSchema() {
		super();
	}

	public XmlSchema(String namespace, String namespacePrefix, boolean generateSchemaDefinition) {
		super(namespace, namespacePrefix);
		
		if (generateSchemaDefinition) {
			Document xmlDefinition = new Document(new Element("schema", XSD_NAMESPACE));
			xmlDefinition.getRootElement().addNamespaceDeclaration(Namespace.getNamespace(namespacePrefix, namespace));
			xmlDefinition.getRootElement().setAttribute("targetNamespace", namespace);
			xmlDefinition.getRootElement().setAttribute("elementFormDefault", "qualified");
			xmlDefinition.getRootElement().setAttribute("attributeFormDefault", "unqualified");
			
			setXmlDefinition(xmlDefinition);
		}
	}
	
  public boolean isHasBeenInlined() {
		return hasBeenInlined;
	}

	public void setHasBeenInlined(boolean hasBeenInlined) {
		this.hasBeenInlined = hasBeenInlined;
	}

		@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((getNamespace() == null) ? 0 : getNamespace().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XmlSchema other = (XmlSchema) obj;
		if (getNamespace() == null) {
			if (other.getNamespace() != null)
				return false;
		} else if (!getNamespace().equals(other.getNamespace()))
			return false;
		return true;
	}

	@Override
	public String toString() {
        return "XmlSchema{" +
		        "namespace=" + getNamespace() +
             "}";
	}
}
