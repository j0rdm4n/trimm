/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.generator;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaOperation;
import dk.tigerteam.trimm.mdsd.ws.SoapHeaderHandler;
import dk.tigerteam.trimm.mdsd.ws.WsdlDocument;
import org.jdom2.Element;

/**
 * TODO: doc...
 * 
 * @author Jeppe Cramon
 */
public class NoHeaderSoapHeaderHandler implements SoapHeaderHandler {

	@Override
	public void applySoapHeaderImports(MetaClazz serviceMetaInterface, WsdlDocument wsdlDocument, String namespace) {

	}

	@Override
	public void applyWsdlBindingHeaders(MetaOperation metaOperation, WsdlDocument wsdlDocument, Element bindingOperation, Element operationInput, Element operationOutput) {

	}

}
