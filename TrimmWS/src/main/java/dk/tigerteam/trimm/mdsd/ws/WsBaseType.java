/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static dk.tigerteam.trimm.mdsd.ws.WsRestriction.*;


/**
 * See http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/datatypes.html#built-in-primitive-datatypes
 * and http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/datatypes.html#built-in-derived
 * <p/>
 * General rules for Dates:  "YYYY-MM-DD" where: <br/>
 * YYYY indicates the year<br/>
 * MM indicates the month<br/>
 * DD indicates the day<p/>
 * 
 * The time is specified in the following form "hh:mm:ss" where:<br/>
 * hh indicates the hour<br/>
 * mm indicates the minute<br/>
 * ss indicates the second<p/>
 * 
 * @author Jeppe Cramon
 */
public enum WsBaseType {
	/**
	 * The data must conform to the syntax of a Uniform Resource Identifier (URI), as defined in RFC 2396 as amended by RFC 2732. Example: 'http://www.tigerteam.dk/mdd/'
	 */
	anyUri("anyUri", "The data must conform to the syntax of a Uniform Resource Identifier (URI), as defined in RFC 2396 as amended by RFC 2732. Example: 'http://www.tigerteam.dk/mdd/'", ALL_STRING_RESTRICTIONS),
	/**
	 * Base64 encoded binary data. Represents a sequence of binary octets (bytes) encoded according to RFC 2045, the standard defining the MIME types
	 */
	base64Binary("base64Binary", "Base64 encoded binary data. Represents a sequence of binary octets (bytes) encoded according to RFC 2045, the standard defining the MIME types", ALL_STRING_RESTRICTIONS),
	/**
	 * A Boolean true or false value. Representations of true are 'true' and '1'; false is denoted as 'false' or '0'
	 */
	_boolean("boolean", "A Boolean true or false value. Representations of true are 'true' and '1'; false is denoted as 'false' or '0'", pattern, whiteSpace),
	/**
	 * A signed 8-bit integer in the range [-128, 127]
	 */
	_byte("byte", "A signed 8-bit integer in the range [-128, 127]", ALL_NUMERIC_RESTRICTIONS),
	/**
	 * Represents a specific date. The syntax is the same as that for the date part of dateTime, with an optional time zone indicator. Example: '1889-09-24'
	 */
	date("date", "Represents a specific date. The syntax is the same as that for the date part of dateTime, with an optional time zone indicator. Example: '1889-09-24'", ALL_DATE_RESTRICTIONS),
	dateTime("dateTime", "Represents a specific instant of time. It has the form YYYY-MM-DDThh:mm:ss folowed by an optional time-zone suffix.", ALL_DATE_RESTRICTIONS),
	decimal("decimal", "Any base-10 fixed-point number. There must be at least one digit to the left of the decimal point, and a leading '+' or '-' sign is allowed.", ALL_NUMERIC_RESTRICTIONS),
	_double("double", "A 64-bit floating-point decimal number as specified in the IEEE 754-1985 standard. The external form is the same as the float datatype.", ALL_FLOAT_DOUBLE_RESTRICTIONS),
	duration("duration", "Represents a duration of time, as a composite of years, months, days, hours, minutes, and seconds. Starts with '-' if negative. 'P' is always included - Example: 'P1Y2MT2H5.6S' is a duration of one year, two months, two hours, and 5.6 seconds.", ALL_DATE_RESTRICTIONS),
	ENTITY("ENTITY", "ENTITY represents the ENTITY attribute type from the XML specification. Derived from NCName", ALL_STRING_RESTRICTIONS),
	ENTITIES("ENTITIES", "ENTITIES is a space separated list of ENTITY's.ENTITY represents the ENTITY attribute type from the XML specification. Derived from NCName.", ALL_STRING_EXCEPT_PATTERN_RESTRICTIONS),
	_float("float", "A 32-bit floating-point decimal number as specified in the IEEE 754-1985 standard. Allowable values are the same as in the decimal type, optionally followed by an exponent, or one of the special values 'INF' (positive infinity), '-INF' (negative infinity), or 'NaN' (not a number). The exponent starts with either 'e' or 'E', optionally followed by a sign, and one or more digits. Example: '6.0235e-23'.", ALL_FLOAT_DOUBLE_RESTRICTIONS),
	gDay("gDay", "A day of the month in the Gregorian calendar. The syntax is '---DD' where DD is the day of the month. Example: the 27th of each month would be represented as '---27'.", ALL_DATE_RESTRICTIONS),
	gMonth("gMonth", "A month number in the Gregorian calendar. The syntax is '--MM--', where MM is the month number. For example, '--06--' represents the month of June.", ALL_DATE_RESTRICTIONS),
	gMonthDay("gMonthDay", "A Gregorian month and day as '--MM-DD'. Example: '--07-04' is the Fourth of July.", ALL_DATE_RESTRICTIONS),
	gYear("gYear", "A Gregorian year, specified as YYYY. Example: '1889'", ALL_DATE_RESTRICTIONS),
	gYearMonth("gYearMonth", "A Gregorian year and month. The syntax is YYYY-MM. Example: '1995-08' represents August 1995.", ALL_DATE_RESTRICTIONS),
	hexBinary("hexBinary", "Represents a sequence of octets (bytes), each given as two hexadecimal digits. Example: '0047dedbef' is five octets.", ALL_STRING_RESTRICTIONS),
	ID("ID", "A unique identifier as in the ID attribute type from the XML standard. Derived from the NCName datatype.", ALL_STRING_RESTRICTIONS),
	IDREF("IDREF", "An IDREF value is a reference to a unique identifier as defined under attribute types in the XML standard.Derived from the NCName datatype.", ALL_STRING_RESTRICTIONS),
	IDREFS("IDREFS", "An IDREF value is a reference to a unique identifier as defined under attribute types in the XML standard. An IDREFS value is a space-separated sequence of such references. Derived from the NCName datatype.", ALL_STRING_EXCEPT_PATTERN_RESTRICTIONS),
	_int("int", "A signed 32-bit integer in the range [-2,147,483,648, 2,147,483,647]. Derived from long", ALL_NUMERIC_RESTRICTIONS),
	integer("integer", "Represents a signed integer. Values may begin with an optional '+' or '-' sign. Derived from the decimal datatype.", ALL_NUMERIC_RESTRICTIONS),
	language("language", "One of the standardized language codes defined in RFC 1766. Example: 'fj' for Fijian. Derived from the token type.", ALL_STRING_RESTRICTIONS),
	_long("long", "A signed 64-bit integer, at least 18 digits are guaranteed. Derived from the integer datatype.", ALL_NUMERIC_RESTRICTIONS),
	Name("Name", "A name as defined in the XML standard. The first character can be a letter or underbar “_”, and the remaining characters may be letters, underbars, hyphen “-”, period “.”, or colon “:”. Derived from the token datatype.", ALL_STRING_RESTRICTIONS),
	NCName("NCName", "The local part of a qualified name. See the NCName definition in the document Namespaces in XML. Derived from the Name datatype.", ALL_STRING_RESTRICTIONS),
	negativeInteger("negativeInteger", "An integer containing only negative values (..,-2,-1). Derived from the nonPositiveInteger datatype.", ALL_NUMERIC_RESTRICTIONS),
	NMTOKEN("NMTOKEN", "Any sequence of name characters, defined in the XML standard: letters, underbars “_”, hyphen “-”, period “.”, or colon “:”.", ALL_STRING_RESTRICTIONS),
	NMTOKENS("NMTOKENS", "Any sequence of name characters, defined in the XML standard: letters, underbars “_”, hyphen “-”, period “.”, or colon “:”. A NMTOKENS data value is a space-separated sequence of NMTOKEN values.", ALL_STRING_EXCEPT_PATTERN_RESTRICTIONS),
	normalizedString("normalizedString", "This datatype describes a “normalized” string, meaning that it cannot include newline (LF), return (CR), or tab (HT) characters. Derived from the string type.", ALL_STRING_RESTRICTIONS),
	nonNegativeInteger("nonNegativeInteger", "An integer containing only non-negative values (0,1,2,..)", ALL_NUMERIC_RESTRICTIONS),
	nonPositiveInteger("nonPositiveInteger", "An integer containing only non-positive values (..,-2,-1,0)", ALL_NUMERIC_RESTRICTIONS),
	NOTATION("NOTATION", "NOTATION represents the NOTATION attribute type from the XML 1.0 specification. The ·value space· of NOTATION is the set of QNames of notations declared in the current schema. The ·lexical space· of NOTATION is the set of all names of notations declared in the current schema (in the form of QNames).", ALL_STRING_RESTRICTIONS),
	positiveInteger("positiveInteger", "An integer containing only positive values (1,2,..). Derived from the nonNegativeInteger datatype.", ALL_NUMERIC_RESTRICTIONS),
	QName("QName", "An XML qualified name, such as 'xsl:stylesheet'.", ALL_STRING_RESTRICTIONS),
	_short("short", "A signed 16-bit integer in the range [-32,768, 32,767]. Derived from the int datatype.", ALL_NUMERIC_RESTRICTIONS),
	string("string", "Any sequence of zero or more characters.", ALL_STRING_RESTRICTIONS),
	time("time", "A moment of time that repeats every day. The syntax is the same as that for dateTime, omitting everything up to and including the separator 'T'. Examples: '00:00:00' is midnight, and '13:04:00' is an hour and four minutes after noon.", ALL_DATE_RESTRICTIONS),
	token("token", "The values of this type represent tokenized strings. They may not contain newline (LF) or tab (HT) characters. They may not start or end with whitespace. The only occurrences of whitespace allowed inside the string are single spaces, never multiple spaces together. Derived from normalizedString.", ALL_STRING_RESTRICTIONS),
	unsignedByte("unsignedByte", "An unsigned 8-bit integer in the range [0, 255]. Derived from the unsignedShort datatype.", ALL_NUMERIC_RESTRICTIONS),
	unsignedInt("unsignedInt", "An unsigned 32-bit integer in the range [0, 4,294,967,295]. Derived from the unsignedLong datatype.", ALL_NUMERIC_RESTRICTIONS),
	unsignedLong("unsignedLong", "An unsigned 64-bit integer. Derived from the nonNegativeInteger datatype.", ALL_NUMERIC_RESTRICTIONS),
	unsignedShort("unsignedShort", "An unsigned 16-bit integer in the range [0, 65,535]. Derived from the unsignedInt datatype.", ALL_NUMERIC_RESTRICTIONS);
	
	
	
	private String xsdName;
	private List<WsRestriction> restrictions;
	private String description;
	
	private WsBaseType(String xsdName, String description, WsRestriction... restrictions) {
		this.xsdName = xsdName;
		this.description = description;
		if (restrictions != null) {
			this.restrictions = Arrays.asList(restrictions);
		} else {
			this.restrictions = Collections.emptyList();
		}
	}

	public String getXsdName() {
		return xsdName;
	}

	public List<WsRestriction> getRestrictions() {
		return restrictions;
	}
	
	public String getDescription() {
		return description;
	}
}
