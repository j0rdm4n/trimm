/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.generator;

import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.ws.AbstractSimpleTypeResolver;
import dk.tigerteam.trimm.mdsd.ws.SimpleTypeResolver.SimpleTypeRestriction.WhiteSpaceRestriction;
import dk.tigerteam.trimm.mdsd.ws.WsRestriction;

/**
 * Default SimpleTypeResolver implementation, reads restrictions from TaggedValues on the {@link MetaProperty}'s<br/>
 * @author Jeppe Cramon
 * @see WsRestriction
 * @see WhiteSpaceRestriction
 */
public class DefaultSimpleTypeResolver extends AbstractSimpleTypeResolver {
	
  public SimpleTypeRestriction resolveRestriction(MetaProperty metaProperty) {
    SimpleTypeRestriction restriction = new SimpleTypeRestriction();
    fillMatchingRestrictions(restriction, metaProperty);
    return restriction;
  }

	private void fillMatchingRestrictions(SimpleTypeRestriction restriction, MetaProperty metaProperty) {
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.pattern.name())) {
			restriction.pattern = metaProperty.getTaggedValue(WsRestriction.pattern.name());
		}
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.fractionDigits.name())) {
			restriction.fractionDigits = Long.parseLong(metaProperty.getTaggedValue(WsRestriction.fractionDigits.name()));
		}
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.totalDigits.name())) {
			restriction.totalDigits = Long.parseLong(metaProperty.getTaggedValue(WsRestriction.totalDigits.name()));
		}
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.length.name())) {
			restriction.length = Long.parseLong(metaProperty.getTaggedValue(WsRestriction.length.name()));
		}
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.minLength.name())) {
			restriction.minLength = Long.parseLong(metaProperty.getTaggedValue(WsRestriction.minLength.name()));
		}
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.maxLength.name())) {
			restriction.maxLength = Long.parseLong(metaProperty.getTaggedValue(WsRestriction.maxLength.name()));
		}
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.maxExclusive.name())) {
			restriction.maxExclusive = metaProperty.getTaggedValue(WsRestriction.maxExclusive.name());
		}
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.maxInclusive.name())) {
			restriction.maxInclusive = metaProperty.getTaggedValue(WsRestriction.maxInclusive.name());
		}
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.minExclusive.name())) {
			restriction.minExclusive = metaProperty.getTaggedValue(WsRestriction.minExclusive.name());
		}
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.minInclusive.name())) {
			restriction.minInclusive = metaProperty.getTaggedValue(WsRestriction.minInclusive.name());
		}
		if (metaProperty.hasNotNullTaggedValue(WsRestriction.whiteSpace.name())) {
			String value = metaProperty.getTaggedValue(WsRestriction.whiteSpace.name()).trim();
			try {
				restriction.whiteSpace = WhiteSpaceRestriction.valueOf(value);
			} catch (IllegalArgumentException e) {
				throw new RuntimeException("Failed to resolveNamespace Whitespace value '" + value + "' for MetaProperty: " + metaProperty.toString());
			}
		}
		// Enumeration is handled elsewhere by handling MetaEnumeration during generation
	}
}
