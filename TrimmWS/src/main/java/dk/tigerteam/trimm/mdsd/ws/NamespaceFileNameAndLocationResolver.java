/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;

/**
 * Namespace, filename and location strategy
 *
 * @author Jeppe Cramon
 */
public interface NamespaceFileNameAndLocationResolver {
    /**
     * Resolve the Namespace for this type
     *
     * @param xmlType The type to resolveNamespace namespace for (can either be a data type or a service interface)
     * @return the namespace (e.g. http://tigerteam.dk/person/personservice)
     */
    String resolveNamespace(MetaClazz xmlType);

    /**
     * Resolve the version for this type
     *
     * @param xmlType The type to resolveNamespace version for (can either be a data type or a service interface)
     * @return the version (e.g. "v1" or "")
     */
    String resolveVersion(MetaClazz xmlType);

    /**
     * Resolve the relative path for this namespace (ie. where should the files within this namespace be placed?)
     *
     * @param namespace The resolved namespace
     * @param xmlType   The Xml type (can either be a data type or a service interface)
     * @return the relative path ie. given "http://tigerteam.dk/person/service" you would return "person/service" (note the missing / at the start of the path)
     */
    String resolveRootRelativePath(String namespace, MetaClazz xmlType);

    /**
     * Resolve the File name (without version indication) for the file that should contain the XMLSchema<br/>
     * <b>Note: The name is the bare name, ie. meaning without .xsd suffix</b>
     *
     * @param xmlType The type
     * @return the file name, e.g. "Person"
     */
    String resolveXmlSchemaUnversionedFileName(MetaClazz xmlType);

    /**
     * Resolve the File name (without version indication) for the file that should contain the Service interface<br/>
     * <b>Note: The name is the bare name, ie. meaning without .wsdl suffix</b>
     *
     * @param serviceMetaInterface The service interface
     * @return the file name, e.g. "PersonService"
     */
    String resolveWsdlDocumentUnversionedFileName(MetaClazz serviceMetaInterface);

    /**
     * Resolve the suffix that applies to a Wsdl Documents externally located (versus inlined) XmlSchema
     *
     * @param serviceMetaInterface The service interface
     * @return The suffix (e.g. "Wrapper")
     */
    String resolveWsdlDocumentWrapperSchemaSuffix(MetaClazz serviceMetaInterface);
}
