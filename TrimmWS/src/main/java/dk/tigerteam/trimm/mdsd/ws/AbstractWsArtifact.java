/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import org.jdom2.Document;

import java.util.HashSet;
import java.util.Set;

/**
 * TODO: doc....
 * 
 * @author Jeppe Cramon
 */
public abstract class AbstractWsArtifact implements WsArtifact {
	private Document xmlDefinition;
	private String rootRelativePath;
	private String namespace;
	private Set<XmlSchema> referencedXmlSchemas = new HashSet<XmlSchema>();
	private String namespacePrefix;
	private String fileName;
	
	public AbstractWsArtifact() {
	}
	
	public AbstractWsArtifact(String namespace) {
		super();
		this.namespace = namespace;
	}

	public AbstractWsArtifact(String namespace, String namespacePrefix) {
		super();
		this.namespace = namespace;
		this.namespacePrefix = namespacePrefix;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Document getXmlDefinition() {
		return xmlDefinition;
	}

	public void setXmlDefinition(Document xmlDefinition) {
		this.xmlDefinition = xmlDefinition;
	}

	public String getRootRelativePath() {
		return rootRelativePath;
	}

	public void setRootRelativePath(String rootRelativePath) {
		this.rootRelativePath = rootRelativePath;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	public Set<XmlSchema> getReferencedXmlSchemas() {
		return referencedXmlSchemas;
	}
	
	public String getNamespacePrefix() {
		return namespacePrefix;
	}


}
