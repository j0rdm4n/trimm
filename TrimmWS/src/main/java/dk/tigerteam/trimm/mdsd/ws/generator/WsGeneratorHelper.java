/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.generator;

import dk.tigerteam.trimm.mdsd.uml2.xmi.XmiReader;
import dk.tigerteam.trimm.mdsd.uml2.xmi.ea.EAXmiReader;
import dk.tigerteam.trimm.mdsd.uml2.xmi.magicdraw.MagicDraw16XmiReader;
import dk.tigerteam.trimm.mdsd.ws.WsGenerationResult;
import dk.tigerteam.trimm.mdsd.ws.WsdlDocument;
import dk.tigerteam.trimm.mdsd.ws.XmlSchema;
import dk.tigerteam.trimm.util.JDomWriter;
import org.apache.commons.io.IOUtils;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.*;

public class WsGeneratorHelper {
	
	public static void writeFiles(String generateToFolder, WsGenerationResult result) throws FileNotFoundException, IOException {
		System.out.println("Writing to Output folder: '" + generateToFolder + "'");
		XMLOutputter serializer = new XMLOutputter();
		serializer.setFormat(Format.getPrettyFormat());
		for (XmlSchema xmlSchema : result.getXmlSchemas()) {
			if (!xmlSchema.isHasBeenInlined()) {
		  	System.out.println("XmlSchema Root Relative Path: '" + xmlSchema.getRootRelativePath() + "' & Filename: '" + xmlSchema.getFileName() + "'");
		  	String path = generateToFolder;
		  	if (xmlSchema.getRootRelativePath() != null && !xmlSchema.getRootRelativePath().equals("")) {
		  		if (!path.endsWith("/") && !xmlSchema.getRootRelativePath().startsWith("/")) path += "/";
		  		path += xmlSchema.getRootRelativePath();
		  	}
		    System.out.println("Generating: Xml Schema '" + xmlSchema.getFileName() + "' to path '" + path + "'");
		    JDomWriter.write(xmlSchema.getXmlDefinition(), path, xmlSchema.getFileName());
			}
		}

		StringBuilder readmeBuilder = new StringBuilder("Interfaces generated:\r\n");
		for (WsdlDocument wsdlDocument : result.getWsdlDocuments()) {
			String path = generateToFolder;
			if (wsdlDocument.getRootRelativePath() != null && !wsdlDocument.getRootRelativePath().equals("")) {
				if (!path.endsWith("/") && !wsdlDocument.getRootRelativePath().startsWith("/")) path += "/";
				path += wsdlDocument.getRootRelativePath();
			}

		  System.out.println("Generating: Wsdl Document '" + wsdlDocument.getFileName() + "' to path '" + path + "'");
		  JDomWriter.write(wsdlDocument.getXmlDefinition(), path, wsdlDocument.getFileName());
		  readmeBuilder.append(wsdlDocument.getMetaInterface().getFQCNUsingAliasIfAvailable() + " => " + path + " and file name "+  wsdlDocument.getFileName() + " (Namespace: " + wsdlDocument.getNamespace() + " )\r\n");
		}
		File readmeFile = new File(generateToFolder + "/README.TXT");
		PrintWriter readmeWriter = new PrintWriter(new FileOutputStream(readmeFile));
		readmeWriter.write(readmeBuilder.toString());
		IOUtils.closeQuietly(readmeWriter);
		System.out.println("Created " + readmeFile.getAbsolutePath());
		System.out.println("Done :)");
	}

  public static XmiReader selectXmiReader(String modelFilePath) {
    if (modelFilePath.endsWith("mdzip")) {
      return new MagicDraw16XmiReader();
    } else {
      return new EAXmiReader();
    }
  }
}
