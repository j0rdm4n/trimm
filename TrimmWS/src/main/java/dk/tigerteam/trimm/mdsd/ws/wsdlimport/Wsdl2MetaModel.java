/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.wsdlimport;

import dk.tigerteam.trimm.mdsd.meta.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ws.commons.schema.ValidationEventHandler;
import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaCollection;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPath;

import javax.xml.namespace.QName;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Class that can import a WSDL document (including inlined and referenced XML Schemas) and transform it to
 * a MetaModel which represents the code information in the WSDL
 */
public class Wsdl2MetaModel extends AbstractXml2MetaModel {
    private static final Log log = LogFactory.getLog(Wsdl2MetaModel.class);
    protected Xsd2MetaModel xsd2MetaModel;
    private XmlSchemaCollection xmlSchemaCollection;

    public Wsdl2MetaModel() {
        // Create a new Xml Schema collection and make sure that the Xsd2MetaModel uses the same XmlSchemaCollection
        xmlSchemaCollection = new XmlSchemaCollection();
        xsd2MetaModel = new Xsd2MetaModel();
    }

    public void transform(MetaModel metaModel, Element wsdlDocumentRootElement) throws JDOMException {
        Element wsdlDefinitions = wsdlDocumentRootElement;
        if (!"definitions".equals(wsdlDefinitions.getName())) {
            throw new IllegalArgumentException("Couldn't find WSDL definitions element in document");
        }

        Namespace wsdlNs = Namespace.getNamespace(wsdlDefinitions.getNamespacePrefix(),
                                                  wsdlDefinitions.getNamespaceURI());
        // Handle MetaPackage for Namespace
        MetaPackage metaPackage = resolveMetaPackageForWsdlTargetNamespace(metaModel, wsdlDefinitions);

        // Handle Interface for WSDL
        MetaInterface wsdlInterface = resolveMetaInterfaceForWSDLDocument(wsdlDefinitions, metaPackage);

        // Handle schemas defined or imported in <wsdl:types/>
        handleWsdlTypes(wsdlDefinitions, wsdlNs, metaModel);

        // Handle XmlSchema to MetaModel transformation
        handleXsd2MetaModelTransformation(metaModel);

        handleWsdlPortTypeOperations(wsdlDefinitions, wsdlNs, wsdlInterface, metaModel);
    }

    private void handleWsdlPortTypeOperations(Element wsdlDefinitions, Namespace wsdlNs, MetaInterface wsdlInterface, MetaModel metaModel) throws JDOMException {
        XPath xpath = XPath.newInstance("//" + wsdlNs.getPrefix() + ":portType/" + wsdlNs.getPrefix() + ":operation");
        xpath.addNamespace(wsdlNs);

        List<Element> operations = (List<Element>) xpath.selectNodes(wsdlDefinitions);
        for (Element operation : operations) {
            String operationName = operation.getAttributeValue("name");
            List<MessagePart> inputMessageParts = resolveMessageParts( operation.getChild("input", wsdlNs).getAttributeValue("message"), wsdlDefinitions, wsdlNs);
            // Output (may be optional)
            List<MessagePart> outputMessageParts = new LinkedList<MessagePart>();
            if (operation.getChild("output", wsdlNs) != null) {
                outputMessageParts = resolveMessageParts( operation.getChild("output", wsdlNs).getAttributeValue("message"), wsdlDefinitions, wsdlNs);
            }

            MetaOperation metaOperation = createWithId(MetaOperation.class);
            metaOperation.setName(operationName);
            for (MessagePart inputMessagePart : inputMessageParts) {
                metaOperation.addParameters((MetaParameter)
                        createWithId(MetaParameter.class)
                             .setType(findMetaClazzFromElementQName(inputMessagePart.getElementQName(), metaModel))
                             .setDirection(MetaParameter.Direction.In)
                             .setName(inputMessagePart.getName())
                );
            }
            for (MessagePart outputMessagePart : outputMessageParts) {
                metaOperation.addParameters((MetaParameter)
                        createWithId(MetaParameter.class)
                             .setType(findMetaClazzFromElementQName(outputMessagePart.getElementQName(), metaModel))
                             .setDirection(MetaParameter.Direction.Out)
                             .setName(outputMessagePart.getName())
                );
            }
            if (metaOperation.getReturnParameter() == null) {
                metaOperation.setReturnParameter(createWithId(MetaParameter.class).setDirection(MetaParameter.Direction.Return).setType((MetaClazz) createWithId(MetaClazz.class).setBuiltInType(true).setName("void")));
            }

            wsdlInterface.add(metaOperation);
            if (log.isDebugEnabled()) {
                log.debug("Defined " + metaOperation + " based on element " + operation);
            }
        }

    }

    private MetaClazz findMetaClazzFromElementQName(QName qName, MetaModel metaModel) {
        // Look up if the element just is an alias for a complex type that we've already handled (we don't want to
        // produce MetaClazz for simple alias')
        Map<QName, QName> elementQName2ComplexTypeQName = (Map<QName, QName>) metaModel.getAttribute(XmlAttributeKeys.ElementQName2ComplexTypeQName.name());
        if (elementQName2ComplexTypeQName.containsKey(qName)) {
            qName = elementQName2ComplexTypeQName.get(qName);
        }
        // Isn't Java just wonderful - closures where art thy?
        final QName qNameToFind = qName;
        return metaModel.findSingleMatchRequired(new MetaCriteria<MetaClazz>(MetaClazz.class) {
            @Override
            public boolean isOk(MetaClazz metaClazz) {
                return qNameToFind.equals(metaClazz.getAttribute(XmlAttributeKeys.QName.name()));
            }
        });
    }


    private List<MessagePart> resolveMessageParts(String messageName, Element wsdlDefinitions, Namespace wsdlNs) throws JDOMException {
        String[] messageNameParts = messageName.split(":");
        // TODO: Support multiple WSDLs in the future, for now we assume the message is defined in this WSDL document
        XPath xpath = XPath.newInstance(wsdlNs.getPrefix() + ":message[@name='" + messageNameParts[1] + "']");
        xpath.addNamespace(wsdlNs);

        Element messageElement = (Element) xpath.selectSingleNode(wsdlDefinitions);
        if (messageElement == null) {
            throw new IllegalStateException("Couldn't find message " + messageName);
        }

        List<MessagePart> parts = new LinkedList<MessagePart>();
        for (Element child : (List<Element>)messageElement.getChildren()) {
            if ("part".equals(child.getName())) {
                String[] elementParts = child.getAttributeValue("element").split(":");
                String elementNamespace = resolveNamespaceFromPrefix(elementParts[0], wsdlDefinitions);
                parts.add(new MessagePart(child.getAttributeValue("name"), new QName(elementNamespace, elementParts[1])));
            }
        }
        return parts;
    }

    private String resolveNamespaceFromPrefix(String prefix, Element wsdlDefinitions) {
        for (Namespace namespace : (List<Namespace>)wsdlDefinitions.getAdditionalNamespaces()) {
            if (namespace.getPrefix().equals(prefix)) {
                return namespace.getURI();
            }
        }
        throw new IllegalArgumentException("Failed to resolveRestriction Namespace URI for prefix " + prefix);
    }

    private void handleXsd2MetaModelTransformation(MetaModel metaModel) {
        xsd2MetaModel.transform(metaModel, xmlSchemaCollection);
    }

    private void handleWsdlTypes(Element wsdlDefinitions, Namespace wsdlNs, MetaModel metaModel) {
        Element wsdlTypes = wsdlDefinitions.getChild("types", wsdlNs);
        if (wsdlTypes == null) {
            throw new IllegalArgumentException("Couldn't find a WSDL types element in the document");
        }

        for (Element wsdlTypesSubElement : (List<Element>)wsdlTypes.getChildren()) {
            if ("schema".equals(wsdlTypesSubElement.getName())) {
                for (Namespace namespace : (List<Namespace>)wsdlDefinitions.getAdditionalNamespaces()) {
                    wsdlTypesSubElement.addNamespaceDeclaration(namespace);    
                }

                readSchema(wsdlTypesSubElement);
            } else {
                throw new IllegalArgumentException("Can't process <wsdl:types> subelement of type " + wsdlTypesSubElement);
            }
        }
    }

    private MetaInterface resolveMetaInterfaceForWSDLDocument(Element wsdlDefinitions, MetaPackage metaPackage) {
        MetaInterface wsdlInterface =  idGenerator.createWithId(MetaInterface.class);
        wsdlInterface.setName(wsdlDefinitions.getAttributeValue("name"));
        metaPackage.add(wsdlInterface);
        // TODO: Interface doc read from <wsdl:documentation/>
        if (log.isDebugEnabled()) {
            log.debug("Defined " + wsdlInterface);
        }
        return wsdlInterface;
    }

    private MetaPackage resolveMetaPackageForWsdlTargetNamespace(MetaModel metaModel, Element wsdlDefinitions) {
        String targetNamespace = wsdlDefinitions.getAttributeValue("targetNamespace");
        MetaPackage metaPackage = resolveMetaPackageForNamespace(targetNamespace, metaModel);
        return metaPackage;
    }

    protected XmlSchema readSchema(Element schemaRootElement) {
        StringWriter stringWriter = new StringWriter();
        try {
            new XMLOutputter().output(schemaRootElement, stringWriter);
        } catch (IOException e) {
            throw new IllegalArgumentException("Failed to read XML Schema based on element " + schemaRootElement, e);
        }

        XmlSchema xmlSchema = xmlSchemaCollection.read(new StringReader(stringWriter.getBuffer().toString()), new ValidationEventHandler() {

        });
        return xmlSchema;
    }


}
