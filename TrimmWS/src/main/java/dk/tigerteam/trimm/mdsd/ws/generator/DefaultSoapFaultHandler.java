/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.generator;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaOperation;
import dk.tigerteam.trimm.mdsd.ws.SoapFaultHandler;
import dk.tigerteam.trimm.mdsd.ws.WsGenerationResult;
import dk.tigerteam.trimm.mdsd.ws.WsdlDocument;
import dk.tigerteam.trimm.mdsd.ws.XmlSchema;
import org.jdom2.Element;

import java.util.LinkedList;
import java.util.List;

import static dk.tigerteam.trimm.mdsd.ws.WsdlDocument.SOAP_NAMESPACE;
import static dk.tigerteam.trimm.mdsd.ws.WsdlDocument.WSDL_NAMESPACE;

/**
 * Default SoapFaultHandler interface implementation
 * @author Jeppe Cramon
 */
public class DefaultSoapFaultHandler implements SoapFaultHandler {
  private static final String FAULT_TYPES_LIST = "FAULT_TYPES_LIST";
  private static final String FAULT_MESSAGES = "Fault Messages";

  public void createFaultMessages(WsdlDocument wsdlDocument, List<Element> messages, MetaOperation metaOperation, WsGenerationResult result) {
    boolean hasFaultMessages = metaOperation.hasTaggedValue(FAULT_MESSAGES) && metaOperation.getTaggedValue(FAULT_MESSAGES) != null;

    String[] faultTypes = null;
    if (hasFaultMessages) {
      faultTypes = metaOperation.getTaggedValue(FAULT_MESSAGES).split(",");
    }

    if (faultTypes == null || faultTypes.length == 0) {
      return;
    }


    List<MetaClazz> faultTypesList = new LinkedList<MetaClazz>();
    metaOperation.setAttribute(FAULT_TYPES_LIST, faultTypesList);
    for (String faultTypeId : faultTypes) {
      MetaClazz faultType = (MetaClazz) metaOperation.getMetaModel().findById(faultTypeId);
      if (faultType == null) {
      	// In EA references for strange reasons follow a pure UUID form and not the normal EAID_ reference form
        faultTypeId = "EAID_" + faultTypeId.substring(1, faultTypeId.length() - 1).replace('-', '_');
        faultType = (MetaClazz) metaOperation.getMetaModel().findById(faultTypeId);
      }
      if (faultType == null) {
      	throw new RuntimeException("Failed to find Fault type with id '" + faultTypeId + "' for Operation '" + metaOperation.toString());
      }
      faultTypesList.add(faultType);

      if (wsdlDocument.getFaultMessagesDefined().contains(faultType.getAliasOrName())) {
        continue;
      }

      Element faultMessage = new Element("message", WSDL_NAMESPACE);
      faultMessage.setAttribute("name", faultType.getAliasOrName());
      Element requestMessagePart = new Element("part", WSDL_NAMESPACE);
      requestMessagePart.setAttribute("name", faultType.getAliasOrName());

      XmlSchema faultTypeSchema = result.getMetaClazzToXmlSchemaMapping().get(faultType);
      requestMessagePart.setAttribute("element", faultTypeSchema.getNamespacePrefix() + ":" + faultType.getAliasOrName());
      faultMessage.addContent(requestMessagePart);
      messages.add(faultMessage);
      wsdlDocument.getReferencedXmlSchemas().add(faultTypeSchema);

      wsdlDocument.getFaultMessagesDefined().add(faultType.getAliasOrName());
    }


  }

  public void applyPortTypeFaults(WsdlDocument wsdlDocument, MetaOperation metaOperation, Element portTypeOperation) {
    List<MetaClazz> faultTypesList = (List<MetaClazz>) metaOperation.getAttribute(FAULT_TYPES_LIST);
    if (faultTypesList == null) {
      return;
    }

    for (MetaClazz faultType : faultTypesList) {
      Element fault = new Element("fault", WSDL_NAMESPACE)
        .setAttribute("message", wsdlDocument.getNamespacePrefix() + ":" + faultType.getAliasOrName())
        .setAttribute("name", faultType.getAliasOrName());
      portTypeOperation.addContent(fault);
    }
  }

  public void applyWsdlBindingFaults(MetaOperation metaOperation, WsdlDocument wsdlDocument, Element bindingOperation) {
    List<MetaClazz> faultTypesList = (List<MetaClazz>) metaOperation.getAttribute(FAULT_TYPES_LIST);
    if (faultTypesList == null) {
      return;
    }

    for (MetaClazz faultType : faultTypesList) {
      Element fault = new Element("fault", WSDL_NAMESPACE)
        .setAttribute("name", faultType.getAliasOrName())
        .addContent(
          new Element("fault", SOAP_NAMESPACE)
            .setAttribute("name", faultType.getAliasOrName())
            .setAttribute("use", "literal")

        );
      bindingOperation.addContent(fault);
    }
  }
}
