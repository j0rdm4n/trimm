/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.wsdlimport;

import dk.tigerteam.trimm.mdsd.meta.*;
import dk.tigerteam.trimm.mdsd.uml2.xmi.MetaIdGenerator;
import dk.tigerteam.trimm.mdsd.uml2.xmi.ea.EAMetaIdGenerator;

import java.util.HashMap;
import java.util.Map;

/**
 * Common base class for all Xml 2 meta model transformers
 * 
 * @author Jeppe Cramon
 */
public abstract class AbstractXml2MetaModel implements Xml2MetaModel {
	
    protected MetaIdGenerator idGenerator = new EAMetaIdGenerator();
    private Map<String, MetaPackage> namespace2MetaPackageCache = new HashMap<String, MetaPackage>();
    
    protected MetaPackage resolveMetaPackageForNamespace(final String targetNamespace, MetaModel metaModel) {
        if (namespace2MetaPackageCache.containsKey(targetNamespace)) {
            return namespace2MetaPackageCache.get(targetNamespace);
        }

        String strippedNamespace = targetNamespace;
        if (strippedNamespace != null) {
	        if (targetNamespace.startsWith("http://")) {
	            int indexOfNextSlash = targetNamespace.indexOf("/", "http://".length());
	            if (indexOfNextSlash != -1) {
	                strippedNamespace = targetNamespace.substring(indexOfNextSlash+1);
	            }
	        }
	        strippedNamespace = strippedNamespace.replace('.', '_');
        } else {
        	strippedNamespace = resolveNullTargetNamespace();
        }
        String[] namespaceParts = strippedNamespace.split("/");

        NameableMetaType currentRoot = metaModel;
        for (final String namespacePart : namespaceParts) {
            MetaPackage matchingPackage = currentRoot.findSingleMatchInImmediateChildren(new MetaCriteria<MetaPackage>(MetaPackage.class) {
                @Override
                public boolean isOk(MetaPackage element) {
                    return element.getName().equals(namespacePart);
                }
            });
            if (matchingPackage == null) {
                MetaPackage newPackage = idGenerator.createWithId(MetaPackage.class);
                newPackage.setName(namespacePart);
                currentRoot.add(newPackage);
                currentRoot = newPackage;
            }
        }

        MetaPackage matchingPackage = (MetaPackage) currentRoot;
        matchingPackage.setAttribute(XmlAttributeKeys.QName.name(), targetNamespace);
        namespace2MetaPackageCache.put(targetNamespace, matchingPackage);
        return matchingPackage;
    }


	protected String resolveNullTargetNamespace() {
		return "anonymous";
	}

    
    protected <T extends MetaType> T createWithId(Class<T> metaTypeClass) {
        return idGenerator.createWithId(metaTypeClass);        
    }

}
