/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaParameter;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;

/**
 * Default implementation to inherit from when creating a project default SimpleTypeResolver.<br/>
 * This implementation handles all built in XML Schema types, by being flexible in how they're spelled in the UML
 * model (i.e. it compares with known names case insensitively).<br/>
 * Also support BigDecimal -> Decimal and timestamp  -> dateTime mapping.
 * <p/>
 * Links to type examples and documentation in a readable form:
 * <ul>
 *  <li><a href="http://www.w3schools.com/schema/schema_dtypes_string.asp">String types</a></li>
 *  <li><a href="http://www.w3schools.com/schema/schema_dtypes_date.asp">Date types</a></li>
 *  <li><a href="http://www.w3schools.com/schema/schema_dtypes_numeric.asp">Numeric types</a></li>
 *  <li><a href="http://www.w3schools.com/schema/schema_dtypes_misc.asp">Misc types</a></li>
 * </ul>
 * @author Jeppe Cramon
 */
public abstract class AbstractSimpleTypeResolver implements SimpleTypeResolver {
    public String resolveXsdType(MetaProperty metaProperty) {
        return resolveXsdType(metaProperty.getType());
    }

    public String resolveXsdType(MetaParameter metaParameter) {
        return resolveXsdType(metaParameter.getType());
    }

    /**
     * Reusable method which takes a MetaClazz (either a MetaProperty and MetaParameters type) and
     * returns the resolved XML Schema (built in) type
     * @param metaClazz the meta clazz to get the native xsd type for
     * @return e.g. decimal, nonNegativeInteger, date, token, string, etc.
     */
    protected String resolveXsdType(MetaClazz metaClazz) {
        String typeName = metaClazz.getAliasOrName();
        if ("bigdecimal".equalsIgnoreCase(typeName)) {
            return "decimal";
        } else if ("negativeInteger".equalsIgnoreCase(typeName)) {
            return "negativeInteger";
        } else if ("nonNegativeInteger".equalsIgnoreCase(typeName)) {
            return "nonNegativeInteger";
        } else if ("nonPositiveInteger".equalsIgnoreCase(typeName)) {
            return "nonPositiveInteger";
        } else if ("positiveInteger".equalsIgnoreCase(typeName)) {
            return "positiveInteger";
        } else if ("unsignedLong".equalsIgnoreCase(typeName)) {
            return "unsignedLong";
        } else if ("unsignedInt".equalsIgnoreCase(typeName)) {
            return "unsignedInt";
        } else if ("unsignedShort".equalsIgnoreCase(typeName)) {
            return "unsignedShort";
        } else if ("unsignedByte".equalsIgnoreCase(typeName)) {
            return "unsignedByte";
        } else if ("timestamp".equalsIgnoreCase(typeName) || "datetime".equalsIgnoreCase(typeName)) {
            return "dateTime";
        } else if ("gDay".equalsIgnoreCase(typeName)) {
            return "gDay";
        } else if ("gMonth".equalsIgnoreCase(typeName)) {
            return "gMonth";
        } else if ("gMonthDay".equalsIgnoreCase(typeName)) {
            return "gMonthDay";
        } else if ("gYear".equalsIgnoreCase(typeName)) {
            return "gYear";
        } else if ("gYearMonth".equalsIgnoreCase(typeName)) {
            return "gYearMonth";
        } else if ("anyuri".equalsIgnoreCase(typeName)) {
            return "anyURI";
        } else if ("base64Binary".equalsIgnoreCase(typeName)) {
            return "base64Binary";
        } else if ("hexBinary".equalsIgnoreCase(typeName)) {
            return "hexBinary";
        } else if ("NOTATION".equalsIgnoreCase(typeName)) {
            return "NOTATION";
        } else if ("normalizedString".equalsIgnoreCase(typeName)) {
            return "normalizedString";
        } else if ("ID".equalsIgnoreCase(typeName)) {
            return "ID";
        } else if ("IDREF".equalsIgnoreCase(typeName)) {
            return "IDREF";
        } else if ("IDREFS".equalsIgnoreCase(typeName)) {
            return "IDREFS";
        } else if ("Name".equalsIgnoreCase(typeName)) {
            return "Name";
        } else if ("NCName".equalsIgnoreCase(typeName)) {
            return "NCName";
        } else if ("NMTOKEN".equalsIgnoreCase(typeName)) {
            return "NMTOKEN";
        } else if ("NOTOKENS".equalsIgnoreCase(typeName)) {
            return "NMTOKENS";
        } else if ("QName".equalsIgnoreCase(typeName)) {
            return "QName";
        } else if ("ENTITY".equalsIgnoreCase(typeName)) {
            return "ENTITY";
        } else if ("ENTITIES".equalsIgnoreCase(typeName)) {
            return "ENTITIES";
        }

        // Handle additional types (string, token, language, date, duration, time, byte, decimal, int, integer
        // long, short, boolean, double, float)
        return typeName.toLowerCase();
    }
}
