/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws;

/**
 *
<table border=1 cellpadding=0 cellspacing=0 width=765 style='border-collapse:
 collapse;table-layout:fixed;width:765pt'>
 <col width=114 style='mso-width-source:userset;mso-width-alt:4864;width:114pt'>
 <col width=65 style='width:65pt'>
 <col width=167 style='mso-width-source:userset;mso-width-alt:7125;width:167pt'>
 <col width=279 style='mso-width-source:userset;mso-width-alt:11904;width:279pt'>
 <col width=140 style='mso-width-source:userset;mso-width-alt:5973;width:140pt'>
 <tr height=15 style='height:15.0pt'>
  <td height=15 colspan=4 width=625 style='height:15.0pt;mso-ignore:colspan;
  width:625pt'>Here is a table showing the attribute groups that are allowed
  for each of the basic types.</td>
  <td width=140 style='width:140pt'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 colspan=5 style='height:15.0pt;mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>Type</td>
  <td>pattern</td>
  <td>length, minLength, maxLengt<span style='display:none'>h</span></td>
  <td>minInclusive, minExclusive, maxExclusive maxIn<span style='display:none'>clusive</span></td>
  <td>fractionDigits, totalDigit<span style='display:none'>s</span></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>anyURI</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>base64Binary</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>boolean</td>
  <td>x</td>
  <td colspan=3 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>byte</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>date</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>dateTime</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>decimal</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>double</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>duration</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>float</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>gDay</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>gMonth</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>gMonthDay</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>gYear</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>gYearMonth</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>hexBinary</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>ID</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>IDREF</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>IDREFS</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>int</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>integer</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>language</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>long</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>Name</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>NCName</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>negativeInteger</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>NMTOKEN</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>NMTOKENS</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>nonNegativeInteger</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>nonPositiveInteger</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>normalizedString</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>positiveInteger</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>QName</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>short</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>string</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>time</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>token</td>
  <td>x</td>
  <td>x</td>
  <td colspan=2 style='mso-ignore:colspan'></td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>unsignedByte</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>unsignedInt</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>unsignedLong</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
 <tr height=15 style='height:15.0pt'>
  <td height=15 style='height:15.0pt'>unsignedShort</td>
  <td>x</td>
  <td></td>
  <td>x</td>
  <td>x</td>
 </tr>
</table>

 * 
 * @author Jeppe Cramon
 *
 */
public enum WsRestriction {
	
	pattern, length, minLength, maxLength, enumeration, minInclusive, minExclusive, maxInclusive, maxExclusive, fractionDigits, totalDigits, whiteSpace;

	public static WsRestriction[] ALL_NUMERIC_RESTRICTIONS = new WsRestriction[] { pattern, enumeration, minInclusive, minExclusive, maxInclusive, maxExclusive, fractionDigits, totalDigits, whiteSpace };
	public static WsRestriction[] ALL_FLOAT_DOUBLE_RESTRICTIONS = new WsRestriction[] { pattern, enumeration, minInclusive, minExclusive, maxInclusive, maxExclusive, whiteSpace };
	public static WsRestriction[] ALL_STRING_RESTRICTIONS = new WsRestriction[] { pattern, enumeration, length, minLength, maxLength };
	public static WsRestriction[] ALL_STRING_EXCEPT_PATTERN_RESTRICTIONS = new WsRestriction[] { enumeration, length, minLength, maxLength };
	public static WsRestriction[] ALL_DATE_RESTRICTIONS = new WsRestriction[] { pattern, enumeration, minInclusive, minExclusive, maxInclusive, maxExclusive, whiteSpace};
	
}
