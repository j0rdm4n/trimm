/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.ws.generator;

import dk.tigerteam.trimm.mdsd.ws.tools.WsdlInlinedXmlSchemaSplitter;
import org.jdom2.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 */
public class WsdlSplitter {

  public static void main(String[] args) throws JDOMException, IOException {
    String wsdlFileName = null;
    String outputFolder = null;
    System.out.println("*****************************");
    System.out.println("* TigerMDSD - WSDL splitter *");
    System.out.println("*****************************");
    if (args.length >= 1) {
      wsdlFileName = args[0];
      if (args.length == 2) {
        outputFolder = args[1];
      } else {
        outputFolder = "";
      }

      System.out.println("WSDL File: " + wsdlFileName);
      System.out.println("Output folder: " + outputFolder);

      File wsdlFile = new File(wsdlFileName);
      List<String> filesCreated = WsdlInlinedXmlSchemaSplitter.split(wsdlFile, outputFolder);

      System.out.println("Created new files:");
      for (String file : filesCreated) {
        System.out.println("  " + file);
      }
    } else {
      printUsage();
      return;
    }

  }

  private static void printUsage() {
    System.out.println("Invalid parameters!");
    System.out.println("Usage:");
    System.out.println("   WsdlSplitter path-to-wsdl-file [output-folder]");
  }
}
