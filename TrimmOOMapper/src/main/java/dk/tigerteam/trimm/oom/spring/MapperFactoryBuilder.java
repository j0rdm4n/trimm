/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom.spring;

import dk.tigerteam.trimm.oom.Mapper;
import dk.tigerteam.trimm.oom.MapperFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;
import java.util.Map.Entry;

/**
 * Spring {@link FactoryBean} that allows for easy creation of a {@link MapperFactory} and
 * registration of all {@link Mapper}' which are marked with the {@link dk.tigerteam.trimm.oom.spring.Mapper} component annotation (and thereby discovered
 * by Spring Component scanning, which is e.g. configured like this:
 * <pre>
 * &lt;context:component-scan base-package="dk.tigerteam.project">
 * </pre>
 * 
 * @author Jeppe Cramon
 */
public class MapperFactoryBuilder implements FactoryBean<MapperFactory>, ApplicationContextAware {
	
	private static final Log log = LogFactory.getLog(MapperFactoryBuilder.class);
	private MapperFactory mapperFactory;
	private ApplicationContext applicationContext;
	
	@SuppressWarnings("unchecked")
	public MapperFactory getObject() throws Exception {
		if (mapperFactory == null) {
			mapperFactory = new MapperFactory();
			log.info("Looking for Mappers in the ApplicationContext");
			Map<String, Object> beansMap = applicationContext.getBeansWithAnnotation(dk.tigerteam.trimm.oom.spring.Mapper.class);
			log.info("Found " + beansMap.size() + " Mapper beans with names: " + beansMap.keySet());
			
			for (Entry<String, Object> entry : beansMap.entrySet()) {
				mapperFactory.registerMapper((Mapper<? extends Object, ? extends Object>) entry.getValue());
			}
		}
		return mapperFactory;
	}

	public Class<MapperFactory> getObjectType() {
		return MapperFactory.class;
	}

	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
