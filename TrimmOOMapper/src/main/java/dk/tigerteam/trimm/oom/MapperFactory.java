/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom;

import dk.tigerteam.trimm.oom.mappers.SameTypeMapper;
import dk.tigerteam.trimm.util.ReflectUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * The MapperFactory is responsible for storing and retrieving {@link Mapper} instances.
 * 
 * @author Jeppe Cramon
 * @see Mapper
 */
public class MapperFactory {
	
	private static final Log logger = LogFactory.getLog(MapperFactory.class);
	
	private Map<MappingPair, Mapper<? extends Object, ? extends Object>> mappers 
			= new HashMap<MappingPair, Mapper<? extends Object, ? extends Object>>();
	/**
	 * This map is useful for tracking collisions between Mappers which are anonymous innnerclasses (and thereby have no name which can aid in finding them)
	 */
	private Map<Mapper<? extends Object, ? extends Object>, String> mapperRegistrationStackTrace = new HashMap<Mapper<? extends Object,? extends Object>, String>();
	private boolean logStackTraceDuringMapperCollision = true;
	
	public MapperFactory() {
		super();
	}

	/**
	 * Should we track mapping registration stacktrace and report is together with mapper registration collisions,
	 * which is useful for tracking collisions between Mappers which are anonymous innnerclasses (and thereby have no name which can aid in finding them)<br/>
	 * Default value: true
	 * @param logStackTraceDuringMapperCollision
	 */
	public MapperFactory(boolean logStackTraceDuringMapperCollision) {
		super();
		this.logStackTraceDuringMapperCollision = logStackTraceDuringMapperCollision;
	}
	
	/**
	 * Should we track mapping registration stacktrace and report is together with mapper registration collisions,
	 * which is useful for tracking collisions between Mappers which are anonymous innnerclasses (and thereby have no name which can aid in finding them)<br/>
	 * Default value: true
	 */
	public boolean isLogStackTraceDuringMapperCollision() {
		return logStackTraceDuringMapperCollision;
	}

	/**
	 * Register a {@link Mapper} instance. This code automatically reads the
	 * supported internal and external types from the Parameterization of the
	 * Mapper
	 * 
	 * @param mapper
	 *            The mapper to register
	 * @return This factory instance
	 */
	public MapperFactory registerMapper(
			Mapper<? extends Object, ? extends Object> mapper) {
		Class<? extends Object> internalModelClass = getInternalModelClass(mapper);
		Class<? extends Object> externalModelClass = getExternalModelClass(mapper);

		if (internalModelClass==null || externalModelClass==null) {
		  throw new MappingException("Could not figure out which parameterized types were used. Hint: The mapper must be a concrete or inner class. internalModelClass="+internalModelClass + ", externalModelClass="+externalModelClass);
		}
		
		MappingPair mappingPair = new MappingPair(internalModelClass,
				externalModelClass);
		
		String thisMapperRegistrationStackTrace = null;
		if (logStackTraceDuringMapperCollision) {
			MappingException ex = new MappingException();
			ex.fillInStackTrace();
			thisMapperRegistrationStackTrace = ExceptionUtils.getFullStackTrace(ex);
			mapperRegistrationStackTrace.put(mapper, thisMapperRegistrationStackTrace);
		}

		// Check for collision with existing mappers
		Mapper<? extends Object, ? extends Object> existingModelMapper = internalGetMapperForTypes(mappingPair);
		if (existingModelMapper != null) {
			String message = "Mapper '" + mapper.getClass().getName()
					+ "' collides with existing Mapper '" + existingModelMapper.getClass().getName()
					+ "' for types " + mappingPair;
			if (logStackTraceDuringMapperCollision) {
				message += "\nThis Mapper registration stacktrace:\n" + thisMapperRegistrationStackTrace;
				message += "\n\nExisting Mapper stacktrace:\n" + mapperRegistrationStackTrace.get(existingModelMapper);
			}
			logger.warn(message);
		}

		// Register the new mapper for both the internal and external type
		logger.info("Registering Mapper '" + mapper.getClass().getName()
				+ "' for types " + mappingPair);
		mappers.put(mappingPair, mapper);
		mapper.setMapperFactory(this);

		return this;
	}

	/**
	 * Sugar method to read the Parameterization of the Mapper regarding the Internal Model class.
	 * @param mapper The mapper to read the parameterization from
	 * @return The Internal Model class
	 */
	public static Class<? extends Object> getInternalModelClass(
			Mapper<? extends Object, ? extends Object> mapper) {
		return ReflectUtils.getGenericClass(mapper.getClass(), Mapper.class, 0);
	}

	/**
	 * Sugar method to read the Parameterization of the Mapper regarding the External Model class.
	 * @param mapper The mapper to read the parameterization from
	 * @return The External Model class
	 */
	public static Class<? extends Object> getExternalModelClass(
			Mapper<? extends Object, ? extends Object> mapper) {
		return ReflectUtils.getGenericClass(mapper.getClass(), Mapper.class, 1);
	}

	/**
	 * Find a mapper which can convert an instance of the forType
	 * 
	 * @param fromType The fromType
	 * @param toType The toType
	 * @return The mapper for the forType
	 * @throws MappingException
	 *             In case there's no mapper for the given type
	 */
	@SuppressWarnings("unchecked")
	public Mapper<? extends Object, ? extends Object> getMapperForTypes(
			Class<? extends Object> fromType,
			Class<? extends Object> toType) throws MappingException {
		fromType = resolveSimpleTypes(fromType);
        toType = resolveSimpleTypes(toType);
		MappingPair mappingPair = new MappingPair(fromType,
				toType);
		Mapper<? extends Object, ? extends Object> mapper = internalGetMapperForTypes(mappingPair);
		if (mapper != null) {
			return mapper;
		} else {
			throw new MappingException(
					"Can't find a Mapper between types: '" + mappingPair + "'");
		}
	}

	/**
	 * Unsafe version of {@link #getMapperForTypes(Class, Class)} (doesn't throw an
	 * exception)
	 * 
	 * @param mappingPair
	 *            The types we want a mapper for
	 * @return The mapper for the forType or <code>null</code> if no mapper
	 *         was found
	 */
	protected Mapper<? extends Object, ? extends Object> internalGetMapperForTypes( 
			MappingPair mappingPair) {
	  Mapper<? extends Object, ? extends Object> result = mappers.get(mappingPair);
	  if (result!=null) return result;
    if (mappingPair.internalModelClass==mappingPair.externalModelClass) return new SameTypeMapper<Object>(this);
		return null;
	}

	/**
	 * Guard method for checking if there exists a {@link Mapper} for a given set of types
	 * 
	 * @param fromType The fromType
	 * @param toType The toType

	 * @return true if we have a mapper for the forType, otherwise returns false
	 */
	@SuppressWarnings("unchecked")
	public boolean hasMapperForTypes(
			Class<? extends Object> fromType,
			Class<? extends Object> toType) {
		fromType = resolveSimpleTypes(fromType);
        toType = resolveSimpleTypes(toType);
		return internalGetMapperForTypes(new MappingPair(fromType,
				toType)) != null;
	}
	
	public Mapper<? extends Object, ? extends Object> getMapperForTypeUsingExternalTypesHierarchy(Class<? extends Object> fromInternalType, Class<? extends Object> toExternalTypeSuperClass) {
		Set<Mapper<? extends Object, ? extends Object>> result = new HashSet<Mapper<? extends Object, ? extends Object>>();
		for (Entry<MappingPair, Mapper<? extends Object, ? extends Object>> entry : mappers.entrySet()) {
			if (entry.getKey().internalModelClass == fromInternalType
					&& toExternalTypeSuperClass.isAssignableFrom(entry.getKey().externalModelClass)) {
				result.add(entry.getValue());
			}
		}
		if (result.size() > 1) {
			throw new MappingException(
					"Found more than one Mapper that can map from internal type '" + fromInternalType + "' to external type's superclass '" + toExternalTypeSuperClass + "'");
		} else if (result.size() == 0) {
			throw new MappingException(
					"Couldn't find a Mapper that can map from internal type '" + fromInternalType + "' to external type's superclass '" + toExternalTypeSuperClass + "'");
		} else {
			return result.iterator().next();
		}
	}
	
	public Mapper<? extends Object, ? extends Object> getMapperForTypeUsingInternalTypesHierarchy(Class<? extends Object> fromInternalTypeSuperClass, Class<? extends Object> toExternalType) {
		Set<Mapper<? extends Object, ? extends Object>> result = new HashSet<Mapper<? extends Object, ? extends Object>>();
		for (Entry<MappingPair, Mapper<? extends Object, ? extends Object>> entry : mappers.entrySet()) {
			if (entry.getKey().externalModelClass == toExternalType
					&& fromInternalTypeSuperClass.isAssignableFrom(entry.getKey().internalModelClass)) {
				result.add(entry.getValue());
			}
		}
		if (result.size() > 1) {
			throw new MappingException(
					"Found more than one Mapper that can map from internal type superclass '" + fromInternalTypeSuperClass + "' to external type '" + toExternalType + "'");
		} else if (result.size() == 0) {
			throw new MappingException(
					"Couldn't find a Mapper that can map from internal type superclass  '" + fromInternalTypeSuperClass + "' to external type '" + toExternalType + "'");
		} else {
			return result.iterator().next();
		}
	}


	/**
	 * Generics sugar method which makes it very fluent to use wrappers while
	 * converting 
	 * @param <INTERNAL_MODEL> The type of the internal model
	 * @param <EXTERNAL_MODEL> The type of the external model
	 * @param internalModelClass The type of the internal model
	 * @param externalModelClass The type of the external model
	 * @return The Mapper that it registered to map between the two types
	 * @throws MappingException in case we can't find the Mapper requested
	 */
	@SuppressWarnings("unchecked")
	public <INTERNAL_MODEL, EXTERNAL_MODEL> Mapper<INTERNAL_MODEL, EXTERNAL_MODEL> getMapper(
            Class<INTERNAL_MODEL> internalModelClass,
            Class<EXTERNAL_MODEL> externalModelClass) throws MappingException {
        internalModelClass = resolveSimpleTypes(internalModelClass);
        externalModelClass = resolveSimpleTypes(externalModelClass);
        return (Mapper<INTERNAL_MODEL, EXTERNAL_MODEL>) getMapperForTypes(
                internalModelClass, externalModelClass);
    }

    @SuppressWarnings("rawtypes")
		private Class resolveSimpleTypes(Class modelClass) {
        if (long.class.equals(modelClass)) {
            return Long.class;
        } else if (short.class.equals(modelClass)) {
            return Short.class;
        } else if (int.class.equals(modelClass)) {
            return Integer.class;
        } else if (byte.class.equals(modelClass)) {
            return Byte.class;
        } else if (char.class.equals(modelClass)) {
            return Character.class;
        } else if (double.class.equals(modelClass)) {
            return Double.class;
        } else if (float.class.equals(modelClass)) {
            return Float.class;
        } else if (boolean.class.equals(modelClass)) {
            return Boolean.class;
        } else {
            return modelClass;
        }
    }

	// ---------------------------------------------------------------------------------------------------------------

	/**
	 * Internal storage class
	 */
	public static class MappingPair {
		
		public Class<? extends Object> internalModelClass;
		public Class<? extends Object> externalModelClass;
    private final int hashCode;
    
		public MappingPair(Class<? extends Object> internalModelClass,
				Class<? extends Object> externalModelClass) {
			super();
			this.internalModelClass = internalModelClass;
			this.externalModelClass = externalModelClass;
			this.hashCode = calcHashCode();
		}

		protected int calcHashCode() {
      final int prime = 31;
      int result = 1;
      result = prime
          + result
          + ((externalModelClass == null) ? 0 : externalModelClass
              .hashCode());
      result = prime
          + result
          + ((internalModelClass == null) ? 0 : internalModelClass
              .hashCode());
      return result;
    }

    @Override
    public int hashCode() {
      return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      if (hashCode() != obj.hashCode())
        return false;
      final MappingPair other = (MappingPair) obj;
      return ((this.internalModelClass.equals(other.internalModelClass) 
                && this.externalModelClass.equals(other.externalModelClass))
          ||  (this.internalModelClass.equals(other.externalModelClass) 
                && this.externalModelClass.equals(other.internalModelClass)));
    }

		@Override
		public String toString() {
			String iname = internalModelClass==null ? "<null>" : internalModelClass.getName();
      String ename = externalModelClass==null ? "<null>" : externalModelClass.getName();
      return iname + " and "
					+ ename;
		}
	}
}
