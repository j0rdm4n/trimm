/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom;


/**
 * <h1>Note: Mappers are <u>STATELESS</u>, so you musn't save state in them, except if it's of general use to all mappings and is done in the
 * constructor!!!</h1>
 * <p/>
 * General interface for all Mappers. A mapper is responsible for mapping between two different models.<br/>
 * We use the general term InternalModel and ExternalModel to easily differentiate between them while coding our Mappers.<p/>
 * 
 * The general design principle for Mappers is that your Mapper is only responsible for Mapping the types (i.e. the INTERNAL_MODEL
 * and EXTERNAL_MODEL) it knows about. The idea behind composite mapping where mappers are as small as possible, have
 * responsibility over a single set of entities (i.e. the INTERNAL_MODEL
 * and EXTERNAL_MODEL) and delegate all non-trivial property mappings (typically 1-1 mappings for simple types such as String, Long, BigDecimal, etc.)
 * to other Mappers.<br/>
 * 
 * When it needs to map non trivial properties, it must delegate the mapping responsibility to another {@link Mapper} which is
 * retrieves using {@link MapperFactory#getMapper(Class, Class)} or {@link MapperFactory#getMapperForTypes(Class, Class)}.
 * <b>Note: Mappers must be registrered with a {@link MapperFactory} and 
 * from there you can request a {@link Mapper} between types. 
 * This way you also create a looser coupling between mappers, because it's the {@link MapperFactory} that knows about
 * which mappers can map between types.</b><p/>
 * 
 * Example (using {@link AbstractMapper} to keep the code cleaner):
 * <pre>
 * public class InternalPerson2ExternalPersonMapper extends
 * 		AbstractMapper&lt;InternalPerson, ExternalPerson> {
 * 
 * 	public ExternalPerson <b>mapToExternalModel</b>(InternalPerson internalModel,
 * 			MappingContext context) {
 * 		ExternalPerson externalPerson = new ExternalPerson();
 * 
 * 		externalPerson.setAdresse(<b>getMapper</b>(InternalAddress.class,
 * 				ExternalAdresse.class).<b>mapToExternalModel</b>(
 * 				internalModel.getAddress(), context));
 * 
 * 		externalPerson.setPersonNavn(<b>getMapper</b>(InternalPersonName.class,
 * 				ExternalPersonNavn.class).<b>mapToExternalModel</b>(
 * 				internalModel.getPersonName(), context));
 * 
 * 		return externalPerson;
 * 	}
 * 
 * 	public InternalPerson <b>mapToInternalModel</b>(ExternalPerson externalModel,
 * 			MappingContext context) {
 * 		InternalPerson internalPerson = new InternalPerson();
 * 
 * 		internalPerson.setAddress(<b>getMapper</b>(InternalAddress.class,
 * 				ExternalAdresse.class).<b>mapToInternalModel</b>(
 * 				externalModel.getAdresse(), context));
 * 
 * 		internalPerson.setPersonName(<b>getMapper</b>(InternalPersonName.class,
 * 				ExternalPersonNavn.class).<b>mapToInternalModel</b>(
 * 				externalModel.getPersonNavn(), context));
 * 
 * 		return internalPerson;
 * 	}
 * }
 * </pre>
 * 
 * @author Jeppe Cramon
 *
 * @param <INTERNAL_MODEL> The type of the internal model
 * @param <EXTERNAL_MODEL> The type of the external model
 * @see AbstractMapper
 * @see SmartMapper
 */
public interface Mapper<INTERNAL_MODEL, EXTERNAL_MODEL> {
	/**
	 * Map from the Internal Model to the External Model. 
	 * @param internalModel The Internal Model instance
	 * @param context The context to use during mapping
	 * @return The External Model instance
	 * @throws MappingException In case something goes wrong during mapping
	 */
	EXTERNAL_MODEL mapToExternalModel(INTERNAL_MODEL internalModel, MappingContext context) throws MappingException;
	/**
	 * Map from the External Model to the Internal Model
	 * @param externalModel The External Model instance
	 * @param context The context to use during mapping
	 * @return The Internal Model instance
	 * @throws MappingException In case something goes wrong during mapping
	 */
	INTERNAL_MODEL mapToInternalModel(EXTERNAL_MODEL externalModel, MappingContext context) throws MappingException;
	
	/**
	 * Map from the Internal Model to the External Model
	 * @param internalModel The Internal Model instance
	 * @param existingExternalModel An existing external model
	 * @param context The context to use during mapping
	 * @return The External Model instance
	 * @throws MappingException In case something goes wrong during mapping
	 */
	EXTERNAL_MODEL mapToExternalModel(INTERNAL_MODEL internalModel, EXTERNAL_MODEL existingExternalModel, MappingContext context) throws MappingException;
	/**
	 * Map from the External Model to the Internal Model
	 * @param externalModel The External Model instance
	 * @param existingInternalModel An existing internal model
	 * @param context The context to use during mapping
	 * @return The Internal Model instance
	 * @throws MappingException In case something goes wrong during mapping
	 */
	INTERNAL_MODEL mapToInternalModel(EXTERNAL_MODEL externalModel, INTERNAL_MODEL existingInternalModel, MappingContext context) throws MappingException;
	
	/**
	 * Get access the the {@link MapperFactory} reference
	 * @return The {@link MapperFactory} reference
	 */
	MapperFactory getMapperFactory();
	/**
	 * Set the {@link MapperFactory} reference
	 * @param mapperFactory The reference to the {@link MapperFactory}
	 */
	void setMapperFactory(MapperFactory mapperFactory);
}


