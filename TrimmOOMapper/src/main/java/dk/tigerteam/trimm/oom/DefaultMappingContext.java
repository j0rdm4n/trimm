/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom;

import java.io.Serializable;
import java.util.Locale;

/**
 * Common base class for MappingContexts.
 * 
 * @author Jeppe Cramon
 */
public class DefaultMappingContext implements MappingContext, Serializable {
	
	private static final long serialVersionUID = 3383345891698905489L;
	private Locale locale;

	public DefaultMappingContext() {
		super();
	}
	
	public DefaultMappingContext(Locale locale) {
		super();
		this.locale = locale;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
}
