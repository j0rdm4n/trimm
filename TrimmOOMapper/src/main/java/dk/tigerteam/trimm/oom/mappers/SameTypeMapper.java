/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom.mappers;

import dk.tigerteam.trimm.oom.Mapper;
import dk.tigerteam.trimm.oom.MapperFactory;
import dk.tigerteam.trimm.oom.MappingContext;
import dk.tigerteam.trimm.oom.MappingException;

/**
 * TODO: doc...
 * 
 * @author Jeppe Cramon
 *
 * @param <SAME_MODEL>
 */
public class SameTypeMapper<SAME_MODEL> implements Mapper<SAME_MODEL, SAME_MODEL> {
	
  private MapperFactory factory;

  public SameTypeMapper(MapperFactory factory) {
    this.factory = factory;
  }
  
  public MapperFactory getMapperFactory() {
    return factory;
  }

  public SAME_MODEL mapToExternalModel(SAME_MODEL internalModel, MappingContext context)
    throws MappingException {
    return internalModel;
  }

  public SAME_MODEL mapToExternalModel(SAME_MODEL internalModel, SAME_MODEL existingExternalModel,
    MappingContext context) throws MappingException {
    return internalModel;
  }

  public SAME_MODEL mapToInternalModel(SAME_MODEL externalModel, MappingContext context)
    throws MappingException {
    return externalModel;
  }

  public SAME_MODEL mapToInternalModel(SAME_MODEL externalModel, SAME_MODEL existingInternalModel,
    MappingContext context) throws MappingException {
    return externalModel;
  }

  public void setMapperFactory(MapperFactory mapperFactory) {
    this.factory = mapperFactory;
  }
}
