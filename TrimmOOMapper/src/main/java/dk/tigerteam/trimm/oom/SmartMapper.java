/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom;

import dk.tigerteam.trimm.util.ReflectUtils;
import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Intelligent {@link Mapper} which supports recording of bidirectional Property-to-Property mapping<p/>
 * Smart mapping recording are typically performed in the constructor:
 * <pre>
 * public class InternalPersonName2ExternalPersonNavnMapper extends SmartMapper&lt;InternalPersonName, ExternalPersonNavn> {
 * 
 * 	public InternalPersonName2ExternalPersonNavnMapper() {
 *    super();
 * 		smartMap(internalProperty().getForeName(), externalProperty().getForNavn());
 * 		smartMap(internalProperty().getSurName(), externalProperty().getEfterNavn());
 * 	}
 * }
 * </pre>
 *
 * <b>Note 1:</b> Method chaining, such as internalProperty().getPersonName().getFirstName(), is <b>not</b> supported as
 * it goes against the idea behind composite mapping where mappers are as small as possible, have
 * responsibility over a single set of entities (i.e. the INTERNAL_MODEL
 * and EXTERNAL_MODEL) and delegate all non-trivial property mappings to other Mappers.<p/>
 *
 * <b>Note 2:</b> If you use the default constructor (the call to <code>super();</code>) you will be using {@link #DEFAULT_POLICY}.<br/>
 * Otherwise you can supply your own policy or used the alternative built-in policy {@link #UPDATE_IF_NECESSARY_POLICY}:
 * <pre>
 * public class KundeOplysning_CKundeOplysningMapper extends SmartMapper<KundeOplysning, CKundeOplysning> {
 *    public KundeOplysning_CKundeOplysningMapper() {
 *        <b>super(SmartMapper.UPDATE_IF_NECESSARY_POLICY);</b>
 *        smartMap(internalProperty().getAegtefaelleIndkomst(), externalProperty().getAegtefaelleIndkomst());
 *        smartMap(internalProperty().getAndenIndkomst(), externalProperty().getAndenIndkomst());
 *        smartMap(internalProperty().getCivilstand(), externalProperty().getCivilstand());
 *        smartMap(internalProperty().getPensionsgivendeLoen(), externalProperty().getPensionsgivendeLoen());
 *        smartMap(internalProperty().isEfterloen(), externalProperty().isEfterloen());
 *        smartMap(internalProperty().isFolkepension(), externalProperty().isFolkepension());
 *    }
 *    ...
 *  }
 * </pre>
 *
 * If you want to apply additional "manual" mapping you can do so by overriding
 * {@link #mapToExternalModel(Object, MappingContext)} and {@link #mapToInternalModel(Object, MappingContext)}
 * and <b>remember</b> to call the superclass method:
 * <pre>
 * <pre>
 * public class KundeOplysning_CKundeOplysningMapper extends SmartMapper<KundeOplysning, CKundeOplysning> {
 *    public KundeOplysning_CKundeOplysningMapper() {
 *        <b>super(SmartMapper.UPDATE_IF_NECESSARY_POLICY);</b>
 *        smartMap(internalProperty().getAegtefaelleIndkomst(), externalProperty().getAegtefaelleIndkomst());
 *        smartMap(internalProperty().getAndenIndkomst(), externalProperty().getAndenIndkomst());
 *        smartMap(internalProperty().getCivilstand(), externalProperty().getCivilstand());
 *        smartMap(internalProperty().getPensionsgivendeLoen(), externalProperty().getPensionsgivendeLoen());
 *        smartMap(internalProperty().isEfterloen(), externalProperty().isEfterloen());
 *        smartMap(internalProperty().isFolkepension(), externalProperty().isFolkepension());
 *    }
 *    <b>{@literal @Override}
 *    public CKundeOplysning mapToExternalModel(KundeOplysning internalModel, CKundeOplysning externalModel, MappingContext context) {
 *        CKundeOplysning oplysning = super.mapToExternalModel(internalModel, externalModel, context);</b>
 *        <i>oplysning.setBeregningsDato(internalModel.getBeregningsDatoInterval().getStartDato());
 *        oplysning.setSkattekommuneNu(internalModel.getSkattekommuneNu() != null ? internalModel.getSkattekommuneNu().getKode() : null);
 *        oplysning.setSkattekommuneVedPensionering(internalModel.getSkattekommuneVedPensionering() != null ? internalModel.getSkattekommuneVedPensionering().getKode() : null);</i>
 *        <b>return oplysning;
 *    }</b>
 *
 *    <b>{@literal @Override}
 *    public KundeOplysning mapToInternalModel(CKundeOplysning externalModel, KundeOplysning internalModel, MappingContext context) {
 *        KundeOplysning oplysning = super.mapToInternalModel(externalModel, internalModel, context);</b>
 *        <i>oplysning.setBeregningsDato(externalModel.getBeregningsDato());
 *        oplysning.setSkattekommuneNu(externalModel.getSkattekommuneNu());
 *        oplysning.setSkattekommuneVedPensionering(externalModel.getSkattekommuneVedPensionering());</i>
 *        <b>return oplysning;
 *    }</b>
 *  }
 * </pre>
 * 
 * @author Jeppe Cramon
 *
 * @param <INTERNAL_MODEL> The type of the internal model
 * @param <EXTERNAL_MODEL> The type of the external model
 */
public abstract class SmartMapper<INTERNAL_MODEL, EXTERNAL_MODEL> extends
		AbstractMapper<INTERNAL_MODEL, EXTERNAL_MODEL> {
			
	public enum MappingDirection {
		InternalToExternal, ExternalToInternal
	}

	/**
	 * The type of Mapping
	 */
	public enum MappingType {
		/**
		 * Both internal and external model value can be read and written
		 */
		ReadWrite,
		/**
		 * The Internal model value can only be read, not written
		 */
		InternalReadOnly,
		/**
		 * The External model value can only be read, not written
		 */
		ExternalReadOnly
	}

	private SmartPropertyInterceptor internalModelSmartPropertyInterceptor;
	private SmartPropertyInterceptor externalModelSmartPropertyInterceptor;
	private final Set<PropertyHolder> smartMappings = new LinkedHashSet<PropertyHolder>();
	private PropertyMapperPolicy propertyMapperPolicy = DEFAULT_POLICY;
	
	/**
	 * For composite mapping
	 */
	Set<PropertyDescriptor> internalComposites = new HashSet<PropertyDescriptor>();
	/**
	 * For composite mapping
	 */
	Set<PropertyDescriptor> externalComposites = new HashSet<PropertyDescriptor>();

	/**
	 * Default constructor - Default {@link #getPropertyMapperPolicy()} is
	 * {@link #DEFAULT_POLICY} (unless overridden by
	 * {@link #smartMap(Object, Object, PropertyMapperPolicy, MappingType)}
	 * or {@link #smartMap(Object, Object, PropertyMapperPolicy)})
	 */
	protected SmartMapper() {
	}

	/**
	 * Initialize with another Property Mapper Policy than
	 * {@link #DEFAULT_POLICY}, such as {@link #UPDATE_IF_NECESSARY_POLICY}
	 * 
	 * @param propertyMapperPolicy
	 *            The property mapper policy to use as the default mapping
	 *            (unless overridden by
	 *            {@link #smartMap(Object, Object, PropertyMapperPolicy, MappingType)}
	 *            or {@link #smartMap(Object, Object, PropertyMapperPolicy)})
	 */
	protected SmartMapper(PropertyMapperPolicy propertyMapperPolicy) {
		this.propertyMapperPolicy = propertyMapperPolicy;
	}

	/**
	 * Get the Property Mapper Policy to use as default (unless overridden by
	 * {@link #smartMap(Object, Object, PropertyMapperPolicy, MappingType)}
	 * or {@link #smartMap(Object, Object, PropertyMapperPolicy)})
	 */
	public PropertyMapperPolicy getPropertyMapperPolicy() {
		return propertyMapperPolicy;
	}

	/**
	 * Set the Property Mapper Policy to use as default (unless overridden by
	 * {@link #smartMap(Object, Object, PropertyMapperPolicy, MappingType)}
	 * or {@link #smartMap(Object, Object, PropertyMapperPolicy)})
	 */
	public void setPropertyMapperPolicy(
			PropertyMapperPolicy propertyMapperPolicy) {
		this.propertyMapperPolicy = propertyMapperPolicy;
	}

	/**
	 * Executes any smart mappings.<br/>
	 * <b>Note: When overriding this method remember to call
	 * super.mapToExternalModel(..) to have smartMappings executed</b>
	 * <p/>
	 * Example:
	 * 
	 * <pre>
	 *    &lt;b&gt;{@literal @Override}
	 *    public CKundeOplysning mapToExternalModel(KundeOplysning internalModel, CKundeOplysning externalModel, MappingContext context) {
	 *        CKundeOplysning oplysning = super.mapToExternalModel(internalModel, externalModel, context);&lt;/b&gt;
	 *        &lt;i&gt;oplysning.setBeregningsDato(internalModel.getBeregningsDatoInterval().getStartDato());
	 *        oplysning.setSkattekommuneNu(internalModel.getSkattekommuneNu() != null ? internalModel.getSkattekommuneNu().getKode() : null);
	 *        oplysning.setSkattekommuneVedPensionering(internalModel.getSkattekommuneVedPensionering() != null ? internalModel.getSkattekommuneVedPensionering().getKode() : null);&lt;/i&gt;
	 *        &lt;b&gt;return oplysning;
	 *    }&lt;/b&gt;
	 * </pre>
	 */
	public EXTERNAL_MODEL mapToExternalModel(INTERNAL_MODEL internalModel,
			EXTERNAL_MODEL externalModel, MappingContext context) {
		externalModel = executeSmartMappingsInternalToExternal(internalModel,
				externalModel, context);
		externalModel = mapCompositesToExternalModel(internalModel,
				externalModel, context);
		return externalModel;
	}

	/**
	 * Executes any smart mappings.<br/>
	 * <b>Note: When overriding this method remember to call
	 * super.mapToInternalModel(..) to have smartMappings executed</b>
	 * <p/>
	 * Example:
	 * 
	 * <pre>
	 *    &lt;b&gt;{@literal @Override}
	 *    public KundeOplysning mapToInternalModel(CKundeOplysning externalModel, KundeOplysning internalModel, MappingContext context) {
	 *        KundeOplysning oplysning = super.mapToInternalModel(externalModel, internalModel, context);&lt;/b&gt;
	 *        &lt;i&gt;oplysning.setBeregningsDato(externalModel.getBeregningsDato());
	 *        oplysning.setSkattekommuneNu(externalModel.getSkattekommuneNu());
	 *        oplysning.setSkattekommuneVedPensionering(externalModel.getSkattekommuneVedPensionering());&lt;/i&gt;
	 *        &lt;b&gt;return oplysning;
	 *    }&lt;/b&gt;
	 * </pre>
	 */
	public INTERNAL_MODEL mapToInternalModel(EXTERNAL_MODEL externalModel,
			INTERNAL_MODEL internalModel, MappingContext context) {
		internalModel = executeSmartMappingsExternalToInternal(externalModel,
				internalModel, context);
		internalModel = mapCompositesToInternalModel(externalModel,
				internalModel, context);
		return internalModel;
	}

	/**
	 * Executes any smart mappings (mapping recorded using
	 * {@link #smartMap(Object, Object)} and its sibling
	 * {@link #internalProperty()} and {@link #externalProperty()}) in
	 * {@link MappingDirection#ExternalToInternal}.
	 * <p/>
	 * This method is normally executed by the SmartMapper class when the
	 * {@link #mapToInternalModel(Object, MappingContext)} method is called
	 * 
	 * @param externalModel
	 *            The external model to map to the internal model
	 * @param internalModel
	 *            The internal model to map the external model to
	 * @param context
	 *            The mapping context to use
	 * @return The internal model
	 */
	protected INTERNAL_MODEL executeSmartMappingsExternalToInternal(
			EXTERNAL_MODEL externalModel, INTERNAL_MODEL internalModel,
			MappingContext context) {
		try {
			executeSmartMappings(MappingDirection.ExternalToInternal,
					internalModel, externalModel, context);
			return internalModel;
		} catch (Exception e) {
			throw new MappingException("Failed to map from External ('"
					+ getExternalModelClass() + "') to Internal ('"
					+ getInternalModelClass() + "') model", e);
		}
	}

	/**
	 * Executes any smart mappings (mapping recorded using
	 * {@link #smartMap(Object, Object)} and its sibling
	 * {@link #internalProperty()} and {@link #externalProperty()}) in
	 * {@link MappingDirection#InternalToExternal}.
	 * <p/>
	 * This method is normally executed by the SmartMapper class when the
	 * {@link #mapToExternalModel(Object, MappingContext)} method is called
	 * 
	 * @param internalModel
	 *            The internal model to map to the internal model
	 * @param externalModel
	 *            The external model to map the internal model to
	 * @param context
	 *            The mapping context to use
	 * @return The external model
	 */
	protected EXTERNAL_MODEL executeSmartMappingsInternalToExternal(
			INTERNAL_MODEL internalModel, EXTERNAL_MODEL externalModel,
			MappingContext context) {
		try {
			executeSmartMappings(MappingDirection.InternalToExternal,
					externalModel, internalModel, context);
			return externalModel;
		} catch (Exception e) {
			throw new MappingException("Failed to map from Internal ('"
					+ getInternalModelClass() + "') to External ('"
					+ getExternalModelClass() + "') model", e);
		}
	}

	/**
	 * Helper method to automate smart mapping execution
	 * 
	 * @param mappingDirection
	 * @param toModel
	 * @param fromModel
	 * @param context
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	@SuppressWarnings("unchecked")
	private void executeSmartMappings(MappingDirection mappingDirection,
			Object toModel, Object fromModel, MappingContext context)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {
		for (PropertyHolder holder : smartMappings) {
			// Get value
			PropertyDescriptor toProperty = holder
					.getToDescriptor(mappingDirection);
			if (toProperty == null) {
				// Readonly mapping, so we can't update it
				continue;
			}
			PropertyDescriptor fromProperty = holder
					.getFromDescriptor(mappingDirection);

			Method readMethod = fromProperty.getReadMethod();
			if (readMethod == null) {
				throw new IllegalStateException(
						"Error while executing SmartMappings. Missing ReadMethod for Property '"
								+ fromProperty.getDisplayName() + "'"
								+ " object: " + fromModel);
			}
			Object fromValue = null;
			try {
				fromValue = readMethod.invoke(fromModel, new Object[] {});
			} catch (IllegalArgumentException e) {
				throw new MappingException(
						"Error while executing SmartMappings. Error calling "
								+ readMethod + " on " + fromModel, e);
			}
			Class<? extends Object> fromType = (fromValue != null) ? fromValue
					.getClass() : fromProperty.getPropertyType();
			Class<? extends Object> toType = toProperty.getPropertyType();

			if (fromValue == null) {
				holder.getPolicy().smartMapNullValue(mappingDirection,
						fromModel, toModel, fromProperty, toProperty);
				continue;
			}

			Object toValue = null;

			Mapper<Object, Object> mapper = (Mapper<Object, Object>) getMapperFactory()
					.getMapperForTypes(fromType, toType);

			// Map
			try {
				switch (mappingDirection) {
				case ExternalToInternal:
					toValue = mapper.mapToInternalModel(fromValue, context);
					break;
				case InternalToExternal:
					toValue = mapper.mapToExternalModel(fromValue, context);
					break;
				}
			} catch (Exception e) {
				throw new MappingException("Failed to convert value '"
						+ fromValue
						+ "' (of type: '"
						+ (fromValue != null ? fromValue.getClass()
								: "<unknown>") + "') using Mapper '"
						+ mapper.getClass().getName() + "'", e);
			}

			// Set value
			holder.getPolicy().smartMapSetValue(mappingDirection, fromModel,
					toModel, fromProperty, toProperty, fromValue, toValue);
		}
	}

	/**
	 * Use this method to record a smart mapping. Example of usage:
	 * 
	 * <code>smartMap(internalProperty().getForeName(), externalProperty().getForNavn());</code>
	 * 
	 * @param ignoredInternalProperty
	 *            See example
	 * @param ignoredExternalProperty
	 *            See example
	 */
	protected void smartMap(Object ignoredInternalProperty,
			Object ignoredExternalProperty) {
		smartMap(ignoredInternalProperty, ignoredExternalProperty,
				propertyMapperPolicy, MappingType.ReadWrite);
	}

	/**
	 * Use this method to record a smart mapping. Example of usage:
	 * 
	 * <code>smartMap(internalProperty().getForeName(), externalProperty().getForNavn());</code>
	 * 
	 * @param ignoredInternalProperty
	 *            See example
	 * @param ignoredExternalProperty
	 *            See example
	 * @param mappingType
	 *            The type of Mapping
	 */
	protected void smartMap(Object ignoredInternalProperty,
			Object ignoredExternalProperty, MappingType mappingType) {
		smartMap(ignoredInternalProperty, ignoredExternalProperty,
				propertyMapperPolicy, mappingType);
	}

	/**
	 * Use this method to record a smart mapping. Example of usage:
	 * 
	 * <code>smartMap(internalProperty().getForeName(), externalProperty().getForNavn());</code>
	 * 
	 * @param ignoredInternalProperty
	 *            See example
	 * @param ignoredExternalProperty
	 *            See example
	 * @param mapperPolicy
	 *            The type of Mapping policy to use
	 */
	protected void smartMap(Object ignoredInternalProperty,
			Object ignoredExternalProperty, PropertyMapperPolicy mapperPolicy) {
		smartMap(ignoredInternalProperty, ignoredExternalProperty,
				mapperPolicy, MappingType.ReadWrite);
	}

	/**
	 * Use this method to record a smart mapping. Example of usage:
	 * 
	 * <code>smartMap(internalProperty().getForeName(), externalProperty().getForNavn(), new PropertyMapperPolicy(){});</code>
	 * 
	 * @param ignoredInternalProperty
	 *            See example
	 * @param ignoredExternalProperty
	 *            See example
	 * @param mapperPolicy
	 *            The mapping policy to use
	 * @param mappingType
	 *            The mapping type to use
	 */
	protected void smartMap(Object ignoredInternalProperty,
			Object ignoredExternalProperty, PropertyMapperPolicy mapperPolicy,
			MappingType mappingType) {
		try {
			if (internalModelSmartPropertyInterceptor == null
					|| externalModelSmartPropertyInterceptor == null) {
				throw new IllegalArgumentException(
						"Be sure to use both internalProperty() and externalProperty(). Example: smartMap(internalProperty().getForeName(), externalProperty().getForNavn());");
			}
			PropertyDescriptor internalModelProperty = ReflectUtils
					.getPropertyDescriptor(getInternalModelClass(),
							internalModelSmartPropertyInterceptor
									.getPropertyName());
			if (internalModelProperty == null) {
				throw new IllegalArgumentException("Can't find property name for internalModel Property");
			}
			PropertyDescriptor externalModelProperty = ReflectUtils
					.getPropertyDescriptor(getExternalModelClass(),
							externalModelSmartPropertyInterceptor
									.getPropertyName());
			smartMappings.add(new PropertyHolder(internalModelProperty,
					externalModelProperty, mapperPolicy, mappingType));
		} finally {
			internalModelSmartPropertyInterceptor = null;
			externalModelSmartPropertyInterceptor = null;
		}
	}

	/**
	 * Use this method to record a composite mapping
	 * 
	 * A composite mapping allows you to specify a mapping between hierarchies
	 * that doesn't map one-to-one. Instead a composite mapping allows you to
	 * "hold" one node of the hierarchies and traverse into the other branch.
	 * 
	 * <code>compositeMap(externalProperty().getComplexCompositeType());</code>
	 * 
	 * <pre>
	 *                        B
	 *     A     maps to      |
	 *                        C
	 *    
	 *   psedudocode:
	 *     Mapper&lt;A, B&gt; {
	 *       smartMap(internalProperty().getX(), externalProperty().getY());
	 *       compositeMap(externalProperty().getC());
	 *    }                      
	 *  Internal-&gt;External:
	 *   A.getX() -&gt; B.getY()
	 *   A -&gt; B.getC()
	 *  
	 *  External-&gt;Internal:
	 *   B.getY() -&gt; A.getX()
	 *   B.getC() -&gt; A
	 * 
	 * </pre>
	 * 
	 * Composite mapping can be used in both directions, and the direction is
	 * specified by called with either
	 * <code>compositeMap(<b>externalProperty()</b>.get?())</code> or
	 * <code>compositeMap(<b>internalProperty()</b>.get?())</code>
	 * 
	 * @param ignoredInternalOrExternalProperty
	 *            See example
	 * @see #internalProperty()
	 * @see #externalProperty()
	 */
	public void compositeMap(Object ignoredInternalOrExternalProperty) {
		if (externalModelSmartPropertyInterceptor == null
				&& internalModelSmartPropertyInterceptor == null) {
			throw new IllegalArgumentException(
					"Warning: Be sure to use internal/externalCompositeProperty() Example: compositeMap(internal/externalProperty().getForeName());");
		}
		if (externalModelSmartPropertyInterceptor != null
				&& internalModelSmartPropertyInterceptor != null) {
			throw new IllegalArgumentException(
					"Warning: Only use one of internal/externalCompositeProperty() Example: compositeMap(internal/externalProperty().getForeName());");
		}
		if (externalModelSmartPropertyInterceptor != null) {
			PropertyDescriptor property = ReflectUtils.getPropertyDescriptor(
					getExternalModelClass(),
					externalModelSmartPropertyInterceptor.getPropertyName());
			externalComposites.add(property);
			externalModelSmartPropertyInterceptor = null;
		}
		if (internalModelSmartPropertyInterceptor != null) {
			PropertyDescriptor property = ReflectUtils.getPropertyDescriptor(
					getInternalModelClass(),
					internalModelSmartPropertyInterceptor.getPropertyName());
			internalComposites.add(property);
			internalModelSmartPropertyInterceptor = null;
		}
	}

	@SuppressWarnings("unchecked")
	private INTERNAL_MODEL mapCompositesToInternalModel(
			EXTERNAL_MODEL externalModel, INTERNAL_MODEL internalModel,
			MappingContext context) {
		if (externalModel == null)
			return internalModel;
		for (PropertyDescriptor composite : internalComposites) {
			Method setter = composite.getWriteMethod();
			Mapper<?, EXTERNAL_MODEL> compositeMapper = getMapperFactory()
					.getMapper(setter.getParameterTypes()[0],
							getExternalModelClass());
			Object mappedComposite = compositeMapper.mapToInternalModel(
					externalModel, context);
			try {
				setter.invoke(internalModel, mappedComposite);
			} catch (Exception e) {
				throw new MappingException("setter exception in "
						+ getInternalModelClass().getCanonicalName() + "."
						+ setter.getName() + "", e);
			}
		}
		for (PropertyDescriptor composite : externalComposites) {
			Method getter = composite.getReadMethod();
			Object compositeToBeMapped;
			try {
				compositeToBeMapped = getter.invoke(externalModel);
				if (compositeToBeMapped == null)
					continue;
			} catch (Exception e) {
				throw new MappingException("getter exception in "
						+ getExternalModelClass().getCanonicalName() + "."
						+ getter.getName() + "", e);
			}
			Mapper compositeMapper = getMapperFactory().getMapper(
					getInternalModelClass(), getter.getReturnType());
			internalModel = (INTERNAL_MODEL) compositeMapper
					.mapToInternalModel(compositeToBeMapped, internalModel,
							context);
		}
		return internalModel;
	}

	@SuppressWarnings("unchecked")
	private EXTERNAL_MODEL mapCompositesToExternalModel(
			INTERNAL_MODEL internalModel, EXTERNAL_MODEL externalModel,
			MappingContext context) {
		if (internalModel == null)
			return externalModel;
		for (PropertyDescriptor composite : internalComposites) {
			Method getter = composite.getReadMethod();
			Object compositeToBeMapped;
			try {
				compositeToBeMapped = getter.invoke(internalModel);
				if (compositeToBeMapped == null)
					continue;
			} catch (Exception e) {
				throw new MappingException("Getter exception in "
						+ getInternalModelClass().getCanonicalName() + "."
						+ getter.getName() + "", e);
			}
			Mapper compositeMapper = getMapperFactory().getMapper(
					getter.getReturnType(), getExternalModelClass());
			externalModel = (EXTERNAL_MODEL) compositeMapper
					.mapToExternalModel(compositeToBeMapped, externalModel,
							context);
		}
		for (PropertyDescriptor composite : externalComposites) {
			Method setter = composite.getWriteMethod();
			Mapper<INTERNAL_MODEL, ?> compositeMapper = getMapperFactory()
					.getMapper(getInternalModelClass(),
							setter.getParameterTypes()[0]);
			Object mappedComposite = compositeMapper.mapToExternalModel(
					internalModel, context);
			try {
				setter.invoke(externalModel, mappedComposite);
			} catch (Exception e) {
				throw new MappingException("Setter exception in "
						+ getExternalModelClass().getCanonicalName() + "."
						+ setter.getName() + "", e);
			}
		}
		return externalModel;
	}

	/**
	 * Use this method in combination with {@link #smartMap(Object, Object)} and
	 * {@link #externalProperty()}
	 * 
	 * @return A proxy of the INTERNAL_MODEL class which is used to record which
	 *         properties that map to each other. Note this proxy can only be
	 *         used once.
	 */
	@SuppressWarnings("unchecked")
	protected INTERNAL_MODEL internalProperty() {
		Class<? extends Object> internalModelClass = getInternalModelClass();
		try {
			Enhancer enhancer = new Enhancer();
			enhancer.setSuperclass(internalModelClass);
			internalModelSmartPropertyInterceptor = new SmartPropertyInterceptor();
			enhancer
					.setCallbacks(new Callback[] { internalModelSmartPropertyInterceptor });
			return (INTERNAL_MODEL) enhancer.create();
		} catch (RuntimeException e) {
			throw new RuntimeException("Failed to smartout class '"
					+ internalModelClass + "'", e);
		}
	}

	/**
	 * Use this method in combination with {@link #smartMap(Object, Object)} and
	 * {@link #internalProperty()}
	 * 
	 * @return A proxy of the EXTERNAL_MODEL class which is used to record which
	 *         properties that map to each other. Note this proxy can only be
	 *         used once.
	 */
	@SuppressWarnings("unchecked")
	protected EXTERNAL_MODEL externalProperty() {
		Class<? extends Object> externalModelClass = getExternalModelClass();
		try {
			Enhancer enhancer = new Enhancer();
			enhancer.setSuperclass(externalModelClass);
			externalModelSmartPropertyInterceptor = new SmartPropertyInterceptor();
			enhancer
					.setCallbacks(new Callback[] { externalModelSmartPropertyInterceptor });
			return (EXTERNAL_MODEL) enhancer.create();
		} catch (RuntimeException e) {
			throw new RuntimeException("Failed to smartout class '"
					+ externalModelClass + "'", e);
		}
	}

	/**
	 * Our smart property interceptor. Used by
	 * {@link SmartMapper#internalProperty()} and
	 * {@link SmartMapper#externalProperty()}
	 * 
	 * @author Jeppe Cramon
	 */
	protected static class SmartPropertyInterceptor implements
			MethodInterceptor {
		private String propertyName;

		public SmartPropertyInterceptor() {
			super();
		}

		public Object intercept(Object object, Method method, Object[] args,
				MethodProxy mp) throws Throwable {
			if ((method.getName().startsWith("get") || method.getName()
					.startsWith("is"))
					&& propertyName == null) {
				propertyName = ReflectUtils.getPropertyName(method);
				// The result of this operation is not interesting, as we only
				// want to support a single method call
				return null;
			} else {
				throw new RuntimeException(
						"Sorry, but you can only call a single getter property. Method chaining, "
								+ "such as internalProperty().getPersonName().getFirstName(), isn't supported, since this goes against "
								+ "the idea behind composite mapping, where mappers are as small as possible and have responsibility over "
								+ "a single set of entities (i.e. the INTERNAL_MODEL and EXTERNAL_MODEL). "
								+ "Last method called: '" + method + "'");
			}
		}

		/**
		 * Get the name of the property that was recorded
		 */
		public String getPropertyName() {
			return propertyName;
		}
	}

	private class PropertyHolder {
		public PropertyHolder(PropertyDescriptor internalDescriptor,
				PropertyDescriptor externalDescriptor,
				PropertyMapperPolicy policy, MappingType mappingType) {
			this.internalDescriptor = internalDescriptor;
			this.externalDescriptor = externalDescriptor;
			this.policy = policy;
			this.mappingType = mappingType;
			hashCode = internalDescriptor.hashCode()
					* externalDescriptor.hashCode();
		}

		PropertyDescriptor internalDescriptor;
		PropertyDescriptor externalDescriptor;
		PropertyMapperPolicy policy;
		MappingType mappingType;
		private final int hashCode;

		@Override
		public boolean equals(Object obj) {
			PropertyHolder other = (PropertyHolder) obj;
			return internalDescriptor == other.internalDescriptor
					&& externalDescriptor == other.externalDescriptor;
		}

		@Override
		public int hashCode() {
			return hashCode;
		}

		public MappingType getMappingType() {
			return mappingType;
		}

		public PropertyDescriptor getFromDescriptor(
				MappingDirection mappingDirection) {
			if (mappingDirection == MappingDirection.InternalToExternal)
				return internalDescriptor;
			else
				return externalDescriptor;
		}

		public PropertyDescriptor getToDescriptor(
				MappingDirection mappingDirection) {
			if (mappingDirection == MappingDirection.InternalToExternal) {
				if (mappingType == MappingType.ExternalReadOnly) {
					return null;
				} else {
					return externalDescriptor;
				}
			} else {
				if (mappingType == MappingType.InternalReadOnly) {
					return null;
				} else {
					return internalDescriptor;
				}
			}
		}

		public PropertyMapperPolicy getPolicy() {
			return policy;
		}

	}

	/**
	 * The default Property Mapper Policy, which will perform nicely is most
	 * cases.<br/>
	 * If you would prefer not to perform unnecessary updates to properties you
	 * can consider using {@link #UPDATE_IF_NECESSARY_POLICY}
	 */
	public static final PropertyMapperPolicy DEFAULT_POLICY = new PropertyMapperPolicy() {
		public void smartMapSetValue(MappingDirection mappingDirection,
				Object fromModel, Object toModel,
				PropertyDescriptor fromProperty, PropertyDescriptor toProperty,
				Object fromValue, Object toValue)
				throws IllegalAccessException, InvocationTargetException {

			if (fromModel == null)
				throw new NullPointerException("fromModel is null"); // burde
			// ikke
			// ske

			Method writeMethod = toProperty.getWriteMethod();
			if (writeMethod == null) {
				throw new IllegalStateException(
						"Error while executing SmartMappings. Missing WriteMethod/Setter for Property '"
								+ toProperty.getDisplayName()
								+ "' in class '"
								+ toModel.getClass().getName() + "'");
			}
			try {
				writeMethod.invoke(toModel, toValue);
			} catch (IllegalArgumentException e) {
				throw new MappingException("Failed to set Property '"
						+ toProperty.getDisplayName()
						+ "' in class '"
						+ toModel.getClass().getName()
						+ "' with value '"
						+ fromValue
						+ "' (of type: '"
						+ (fromValue != null ? fromValue.getClass()
								: "<unknown>") + "')", e);
			}

		}

		public void smartMapNullValue(MappingDirection mappingDirection,
				Object fromModel, Object toModel,
				PropertyDescriptor fromProperty, PropertyDescriptor toProperty) {
			if (fromModel == null)
				throw new NullPointerException("fromModel is null"); // sker
			// ganske
			// ofte
		}
	};

	/**
	 * This policy will only call your setter method if the toValue is different
	 * from the properties existing value.<br/>
	 * This policy will call a (optional) Read/Getter method for the property
	 * before updating. If there is a read method, then the toValue is compared
	 * to the existing property value. <br/>
	 * <b>Only if the toValue and existing property value</b> are different,
	 * will the write/setter method be called. If there isn't a read method,
	 * then the write method will be called automatically.
	 */
	public static final PropertyMapperPolicy UPDATE_IF_NECESSARY_POLICY = new PropertyMapperPolicy() {
		public void smartMapSetValue(MappingDirection mappingDirection,
				Object fromModel, Object toModel,
				PropertyDescriptor fromProperty, PropertyDescriptor toProperty,
				Object fromValue, Object toValue)
				throws IllegalAccessException, InvocationTargetException {

			if (fromModel == null)
				throw new NullPointerException("fromModel is null"); // burde
			// ikke
			// ske

			Method readMethod = toProperty.getReadMethod();
			boolean callWriteMethod = false;
			if (readMethod != null) {
				Object existingPropertyValue = readMethod.invoke(toModel,
						new Object[0]);
				if (toValue == null && existingPropertyValue != null
						|| toValue != null && existingPropertyValue == null) {
					callWriteMethod = true;
				} else if (!existingPropertyValue.equals(toValue)) {
					callWriteMethod = true;
				}
			} else {
				// No read method, call set everytime
				callWriteMethod = true;
			}

			if (callWriteMethod) {
				Method writeMethod = toProperty.getWriteMethod();
				if (writeMethod == null) {
					throw new IllegalStateException(
							"Error while executing SmartMappings. Missing WriteMethod/Setter for Property '"
									+ toProperty.getDisplayName()
									+ "' in class '"
									+ toModel.getClass().getName() + "'");
				}
				try {
					writeMethod.invoke(toModel, toValue);
				} catch (IllegalArgumentException e) {
					throw new MappingException("Failed to set Property '"
							+ toProperty.getDisplayName()
							+ "' in class '"
							+ toModel.getClass().getName()
							+ "' with value '"
							+ fromValue
							+ "' (of type: '"
							+ (fromValue != null ? fromValue.getClass()
									: "<unknown>") + "')", e);
				}
			}
		}

		public void smartMapNullValue(MappingDirection mappingDirection,
				Object fromModel, Object toModel,
				PropertyDescriptor fromProperty, PropertyDescriptor toProperty) {
			if (fromModel == null)
				throw new NullPointerException("fromModel is null"); // sker ganske ofte
		}

	};
}
