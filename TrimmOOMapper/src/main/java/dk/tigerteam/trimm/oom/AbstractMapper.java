/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom;


/**
 * Helper superclass for writing your own {@link Mapper}. Provides {@link MapperFactory} reference storing and sugar method(s) for 
 * retrieving other mappers through the {@link MapperFactory} reference.<p/>
 * 
 * Example:
 * <pre>
 * public class InternalPerson2ExternalPersonMapper extends
 * 		AbstractMapper&lt;InternalPerson, ExternalPerson> {
 * 
 * 	public ExternalPerson <b>mapToExternalModel</b>(InternalPerson internalModel,
 * 			MappingContext context) {
 * 		ExternalPerson externalPerson = new ExternalPerson();
 * 
 * 		externalPerson.setAdresse(<b>getMapper</b>(InternalAddress.class,
 * 				ExternalAdresse.class).<b>mapToExternalModel</b>(
 * 				internalModel.getAddress(), context));
 * 
 * 		externalPerson.setPersonNavn(<b>getMapper</b>(InternalPersonName.class,
 * 				ExternalPersonNavn.class).<b>mapToExternalModel</b>(
 * 				internalModel.getPersonName(), context));
 * 
 * 		return externalPerson;
 * 	}
 * 
 * 	public InternalPerson <b>mapToInternalModel</b>(ExternalPerson externalModel,
 * 			MappingContext context) {
 * 		InternalPerson internalPerson = new InternalPerson();
 * 
 * 		internalPerson.setAddress(<b>getMapper</b>(InternalAddress.class,
 * 				ExternalAdresse.class).<b>mapToInternalModel</b>(
 * 				externalModel.getAdresse(), context));
 * 
 * 		internalPerson.setPersonName(<b>getMapper</b>(InternalPersonName.class,
 * 				ExternalPersonNavn.class).<b>mapToInternalModel</b>(
 * 				externalModel.getPersonNavn(), context));
 * 
 * 		return internalPerson;
 * 	}
 * }
 * </pre>
 * 
 * @author Jeppe Cramon
 *
 * @param <INTERNAL_MODEL> The type of the internal model
 * @param <EXTERNAL_MODEL> The type of the external model
 * @see SmartMapper
 */
public abstract class AbstractMapper<INTERNAL_MODEL, EXTERNAL_MODEL> implements Mapper<INTERNAL_MODEL, EXTERNAL_MODEL> {
	
	private MapperFactory mapperFactory;

	public AbstractMapper() {
		super();
	}

	public MapperFactory getMapperFactory() {
		return mapperFactory;
	}

	public void setMapperFactory(MapperFactory mapperFactory) {
		this.mapperFactory = mapperFactory;
	}
	
	/**
	 * Sugar method to read which {@link Class} that has been specified for the INTERNAL_MODEL
	 */
	@SuppressWarnings("unchecked")
	protected Class<INTERNAL_MODEL> getInternalModelClass() {
		return (Class<INTERNAL_MODEL>) MapperFactory.getInternalModelClass(this);
	}
	

	/**
	 * Sugar method to read which {@link Class} that has been specified for the EXTERNAL_MODEL
	 */
	@SuppressWarnings("unchecked")
	protected Class<EXTERNAL_MODEL> getExternalModelClass() {
		return (Class<EXTERNAL_MODEL>) MapperFactory.getExternalModelClass(this);
	}
	
	/**
	 * Default implementation, which calls {@link #createNewExternalModelInstance()} and calls
	 * {@link #mapToExternalModel(Object, Object, MappingContext)} using the default created instance.
	 */
	public EXTERNAL_MODEL mapToExternalModel(INTERNAL_MODEL internalModel,
			MappingContext context) throws MappingException {
		return mapToExternalModel(internalModel, createNewExternalModelInstance(), context);
	}

	/**
	 * Create a new instance of the External Model class
	 * @return The new instance, created using the default constructor
	 * @throws MappingException in case something goes wrong during instance creation
	 */
	protected EXTERNAL_MODEL createNewExternalModelInstance() throws MappingException {
		Class<EXTERNAL_MODEL> externalModelClass = getExternalModelClass();
		try {
			return externalModelClass.newInstance();
		} catch (Exception e) {
			throw new MappingException("Failed to create a new instance of ExternalModel class '" + externalModelClass);
		}
	}

	/**
	 * Default implementation, which calls {@link #createNewInternalModelInstance()} and calls
	 * {@link #mapToInternalModel(Object, Object, MappingContext)} using the default created instance.
	 */
	public INTERNAL_MODEL mapToInternalModel(EXTERNAL_MODEL externalModel,
			MappingContext context) throws MappingException {
		return mapToInternalModel(externalModel, createNewInternalModelInstance(), context);
	}

	/**
	 * Create a new instance of the Internal Model class
	 * @return The new instance, created using the default constructor
	 * @throws MappingException in case something goes wrong during instance creation
	 */
	protected INTERNAL_MODEL createNewInternalModelInstance() {
		Class<INTERNAL_MODEL> internalModelClass = getInternalModelClass();
		try {
			return internalModelClass.newInstance();
		} catch (Exception e) {
			throw new MappingException("Failed to create a new instance of InternalModel class '" + internalModelClass);
		}
	}
	
	/**
	 * Override this method if you want to allow users to provide their own instance of the ExternalModel
	 */
	public EXTERNAL_MODEL mapToExternalModel(INTERNAL_MODEL internalModel,
			EXTERNAL_MODEL existingExternalModel, MappingContext context)
			throws MappingException {
		throw new MappingException("Method EXTERNAL_MODEL mapToExternalModel(INTERNAL_MODEL internalModel, " + 
				"EXTERNAL_MODEL existingExternalModel, Locale locale) hasn't been overridden or is unsupported by this mapper.");
	}

	/**
	 * Override this method if you want to allow users to provide their own instance of the InternalModel
	 */
	public INTERNAL_MODEL mapToInternalModel(EXTERNAL_MODEL externalModel,
			INTERNAL_MODEL existingInternalModel, MappingContext context)
			throws MappingException {
		throw new MappingException("Method INTERNAL_MODEL mapToInternalModel(EXTERNAL_MODEL externalModel, " + 
				"INTERNAL_MODEL existingInternalModel, Locale locale) hasn't been overridden or is unsupported by this mapper.");
	}

	/**
	 * Sugar method for {@link MapperFactory#getMapper(Class, Class)}
	 */
	protected <ANOTHER_INTERNAL_MODEL, ANOTHER_EXTERNAL_MODEL> Mapper<ANOTHER_INTERNAL_MODEL, ANOTHER_EXTERNAL_MODEL> getMapper(Class<ANOTHER_INTERNAL_MODEL> internalModelClass, Class<ANOTHER_EXTERNAL_MODEL> externalModelClass) {
		return getMapperFactory().getMapper(internalModelClass, externalModelClass);
	}
	
	  @Override
  public String toString() {
      // for the benefit of the debugger
      Class<INTERNAL_MODEL> internalModelClass = getInternalModelClass();
      Class<EXTERNAL_MODEL> externalModelClass = getExternalModelClass();
      if (internalModelClass!=null && externalModelClass!=null) {
        return internalModelClass.getName() + " <=> " + externalModelClass.getName();
      }
      return super.toString();
  }
}