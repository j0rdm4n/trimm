/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom.spring;

import dk.tigerteam.trimm.oom.Mapper;
import dk.tigerteam.trimm.oom.MapperFactory;
import dk.tigerteam.trimm.oom.MappingException;
import dk.tigerteam.trimm.spring.ClasspathScanner;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.ClassFilter;
import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.Modifier;
import java.util.Collection;

/**
 * Spring {@link FactoryBean} that allows for easy creation of a {@link MapperFactory} and
 * registration of all {@link Mapper}' that are in the classpath according to the {@link #setIncludePackageNames(Collection)}
 * and {@link #setExcludePackageNames(Collection)} settings.<p/>
 * Example of how to use it in a Spring configuration file:
 * <pre>
 *   &lt;bean id="mapperFactory" class="dk.tigerteam.oom.MapperFactoryBuilder" >
 *    &lt;property name="includePackageNames">
 *      &lt;list>
 *        &lt;value>com.project.mappers&lt;/value>
 *      &lt;/list>
 *    &lt;/property>
 *    &lt;property name="excludePackageNames">
 *      &lt;list>
 *        &lt;value>com.project.mappers.old&lt;/value>
 *      &lt;/list>
 *    &lt;/property> 
 *  &lt;/bean>
 * </pre>
 * @author Jeppe Cramon
 */
public class ClasspathScanningMapperFactoryBuilder implements FactoryBean<MapperFactory> {
	
	private static final Log log = LogFactory.getLog(ClasspathScanningMapperFactoryBuilder.class);
	private MapperFactory mapperFactory;
	
	private ClasspathScanner classpathScanner = new ClasspathScanner(
		new ClassFilter() {
			public boolean matches(Class<?> clazz) {
				int modifiers = clazz.getModifiers();
				return Mapper.class.isAssignableFrom(clazz) && !Modifier.isAbstract(modifiers) && !Modifier.isInterface(modifiers);
			}
		}
	);
	
	@SuppressWarnings("unchecked")
	public MapperFactory getObject() throws Exception {
		if (mapperFactory == null) {
			log.info("Looking for Mappers. IncludePackageNames '" + classpathScanner.getIncludePackageNames() + "', ExcludePackageNames '" + classpathScanner.getExcludePackageNames() + "'");
			mapperFactory = new MapperFactory();
			for (Class<?> mapperClass : classpathScanner.scan()) {
				try {
					log.debug("Registrering Mapper '" + mapperClass.getName() + "'");
					mapperFactory.registerMapper((Mapper<? extends Object, ? extends Object>) mapperClass.newInstance());
				} catch (InstantiationException e) {
					throw new MappingException("Failed to instantiate Mapper '" + mapperClass + "'", e);
				}
			}
		}
		return mapperFactory;
	}

	public Class<MapperFactory> getObjectType() {
		return MapperFactory.class;
	}

	public boolean isSingleton() {
		return true;
	}

	public void setExcludePackageNames(Collection<String> excludePackageNames) {
		classpathScanner.setExcludePackageNames(excludePackageNames);
	}

	public void setIncludePackageNames(Collection<String> includePackageNames) {
		classpathScanner.setIncludePackageNames(includePackageNames);
	}
}
