/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom.mappers;


import dk.tigerteam.trimm.oom.AbstractMapper;
import dk.tigerteam.trimm.oom.MappingContext;

/**
 * Boolean/String mapper
 * 
 * @author Jeppe Cramon
 */
public class BooleanStringMapper extends AbstractMapper<Boolean, String> {
	
	@Override
	public String mapToExternalModel(Boolean internalModel, MappingContext context) {
		return internalModel.toString();
	}

	@Override
	public Boolean mapToInternalModel(String externalModel, MappingContext context) {
		return Boolean.valueOf(externalModel);
	}
}
