/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom;

/**
 * 
 * @author Jeppe Cramon
 *
 */
public class ExternalPerson {
	
	private ExternalAdresse adresse;
	private ExternalPersonNavn personNavn;
	
	public ExternalAdresse getAdresse() {
		return adresse;
	}
	public void setAdresse(ExternalAdresse adresse) {
		this.adresse = adresse;
	}
	public ExternalPersonNavn getPersonNavn() {
		return personNavn;
	}
	public void setPersonNavn(ExternalPersonNavn personNavn) {
		this.personNavn = personNavn;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adresse == null) ? 0 : adresse.hashCode());
		result = prime * result
				+ ((personNavn == null) ? 0 : personNavn.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ExternalPerson other = (ExternalPerson) obj;
		if (adresse == null) {
			if (other.adresse != null)
				return false;
		} else if (!adresse.equals(other.adresse))
			return false;
		if (personNavn == null) {
			if (other.personNavn != null)
				return false;
		} else if (!personNavn.equals(other.personNavn))
			return false;
		return true;
	}
}
