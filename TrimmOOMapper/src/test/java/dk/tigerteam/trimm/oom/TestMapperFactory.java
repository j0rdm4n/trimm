/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom;

import dk.tigerteam.trimm.oom.mappers.IntegerStringMapper;
import org.junit.Test;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.TypeVariable;
import java.util.*;

import static junit.framework.Assert.*;

/**
 * 
 * @author Jeppe Cramon
 *
 */
public class TestMapperFactory {
	
	@Test
	public void testRegister() throws Exception {
		MapperFactory factory = new MapperFactory();
		factory.registerMapper(new IntegerStringMapper());
		factory.registerMapper(new InternalAddress2ExternalAdresse());
		factory.registerMapper(new InternalPerson2ExternalPersonMapper());
		factory.registerMapper(new InternalPersonName2ExternalPersonNavnMapper());
		
		assertTrue(factory.hasMapperForTypes(String.class, Integer.class));
		assertTrue(factory.hasMapperForTypes(Integer.class, String.class));
		assertTrue(factory.hasMapperForTypes(InternalPersonName.class, ExternalPersonNavn.class));
		assertTrue(factory.hasMapperForTypes(ExternalPersonNavn.class, InternalPersonName.class));
		assertTrue(factory.hasMapperForTypes(InternalPerson.class, ExternalPerson.class));
		assertTrue(factory.hasMapperForTypes(ExternalPerson.class, InternalPerson.class));
		assertTrue(factory.hasMapperForTypes(InternalAddress.class, ExternalAdresse.class));
		assertTrue(factory.hasMapperForTypes(ExternalAdresse.class, InternalAddress.class));

		InternalPerson internalPerson = new InternalPerson();
		InternalAddress internalAddress = new InternalAddress();
		internalAddress.setStreetName("Kildetoften");
		internalAddress.setStreetNo(3);
		internalAddress.setZipCode("3550");
		internalPerson.setAddress(internalAddress);
		InternalPersonName internalPersonName = new InternalPersonName();
		internalPersonName.setForeName("Jeppe");
		internalPersonName.setSurName("Cramon");
		internalPerson.setPersonName(internalPersonName);
		
		ExternalPerson externalPerson = factory.getMapper(InternalPerson.class, ExternalPerson.class).mapToExternalModel(internalPerson, new DefaultMappingContext(Locale.getDefault()));
		assertNotNull(externalPerson);
		assertEquals(internalPersonName.getForeName(), externalPerson.getPersonNavn().getForNavn());
		assertEquals(internalPersonName.getSurName(), externalPerson.getPersonNavn().getEfterNavn());
		
		assertEquals(internalAddress.getStreetName(), externalPerson.getAdresse().getVejNavn());
		assertEquals(internalAddress.getStreetNo().toString(), externalPerson.getAdresse().getVejNr());
		assertEquals(internalAddress.getZipCode(), externalPerson.getAdresse().getPostNr());
		
		// Convert back and test
		InternalPerson internalPerson2 = factory.getMapper(InternalPerson.class, ExternalPerson.class).mapToInternalModel(externalPerson, new DefaultMappingContext(Locale.getDefault()));
		assertEquals(internalPerson, internalPerson2);
		
		// Convert back one final time and test
		ExternalPerson externalPerson2 = factory.getMapper(InternalPerson.class, ExternalPerson.class).mapToExternalModel(internalPerson2, new DefaultMappingContext(Locale.getDefault()));
		assertEquals(externalPerson, externalPerson2);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testCollectionMapper() {
		CollectionOfAddressesMapper mapper = new CollectionOfAddressesMapper();
		ParameterizedType collectionType = mapper.getCollectionType();
		assertNotNull(collectionType);
		Class<?> collectionTypeClass = mapper.getCollectionTypeClass(collectionType);
		assertNotNull(collectionTypeClass);
		
		Class<? extends Collection> class1 = new TestClass().getAddresses().getClass();
		TypeVariable[] types = class1.getTypeParameters();
		System.out.println(Arrays.toString(types));
	}
	
	public static class TestClass {
		public Collection<InternalAddress> getAddresses() {
			return new LinkedList<InternalAddress>();
		}
	}
	
	@Test
	public void testMappingPair() {
	  Mapper<String, Date> m1 = new AbstractMapper<String, Date>() {
	    
	  };
	  MapperFactory fac = new MapperFactory();
	  fac.registerMapper(m1);
	  
    assertNotNull(fac.getMapper(String.class, Date.class));
    assertNotNull(fac.getMapper(Date.class, String.class));
    
	}
}
