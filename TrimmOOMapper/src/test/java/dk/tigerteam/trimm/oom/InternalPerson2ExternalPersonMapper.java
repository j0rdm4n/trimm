/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom;

/**
 * 
 * @author Jeppe Cramon
 *
 */
public class InternalPerson2ExternalPersonMapper extends
		AbstractMapper<InternalPerson, ExternalPerson> {

	public ExternalPerson mapToExternalModel(InternalPerson internalModel,
			MappingContext context) {
		ExternalPerson externalPerson = new ExternalPerson();
		
		externalPerson.setAdresse(getMapper(InternalAddress.class,
				ExternalAdresse.class).mapToExternalModel(
				internalModel.getAddress(), context));
		
		externalPerson.setPersonNavn(getMapper(InternalPersonName.class,
				ExternalPersonNavn.class).mapToExternalModel(
				internalModel.getPersonName(), context));
		
		return externalPerson;
	}

	public InternalPerson mapToInternalModel(ExternalPerson externalModel,
			MappingContext context) {
		InternalPerson internalPerson = new InternalPerson();
		internalPerson.setAddress(getMapper(InternalAddress.class,
				ExternalAdresse.class).mapToInternalModel(
				externalModel.getAdresse(), context));
		internalPerson.setPersonName(getMapper(InternalPersonName.class,
				ExternalPersonNavn.class).mapToInternalModel(
				externalModel.getPersonNavn(), context));
		return internalPerson;
	}
}
