/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom;

/**
 * 
 * @author Jeppe Cramon
 *
 */
public class InternalPersonName2ExternalPersonNavnMapper extends SmartMapper<InternalPersonName, ExternalPersonNavn> {

	public InternalPersonName2ExternalPersonNavnMapper() {
		smartMap(internalProperty().getForeName(), externalProperty().getForNavn());
		smartMap(internalProperty().getSurName(), externalProperty().getEfterNavn());
	}

	@Override
	public ExternalPersonNavn mapToExternalModel(
			InternalPersonName internalModel, MappingContext context) {
		ExternalPersonNavn externalModel =  super.mapToExternalModel(internalModel, context);
		// Do custom mappings like when using AbstractMapper
		return externalModel;
	}

	@Override
	public InternalPersonName mapToInternalModel(
			ExternalPersonNavn externalModel, MappingContext context) {
		InternalPersonName internalModel = super.mapToInternalModel(externalModel, context);
		// Do custom mappings like when using AbstractMapper
		return internalModel;
	}
}
