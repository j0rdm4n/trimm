/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.oom;

/**
 * 
 * @author Jeppe Cramon
 *
 */
public class ExternalPersonNavn {
	
	private String forNavn;
	private String efterNavn;

	public String getForNavn() {
		return forNavn;
	}
	public void setForNavn(String forNavn) {
		this.forNavn = forNavn;
	}
	public String getEfterNavn() {
		return efterNavn;
	}
	public void setEfterNavn(String efterNavn) {
		this.efterNavn = efterNavn;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((efterNavn == null) ? 0 : efterNavn.hashCode());
		result = prime * result + ((forNavn == null) ? 0 : forNavn.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ExternalPersonNavn other = (ExternalPersonNavn) obj;
		if (efterNavn == null) {
			if (other.efterNavn != null)
				return false;
		} else if (!efterNavn.equals(other.efterNavn))
			return false;
		if (forNavn == null) {
			if (other.forNavn != null)
				return false;
		} else if (!forNavn.equals(other.forNavn))
			return false;
		return true;
	}
	
}
