/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.cqrs;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import dk.tigerteam.trimm.mdsd.java.generator.JavaStereotypes;
import dk.tigerteam.trimm.mdsd.java.generator.extension.HashCodeAndEqualsListener;
import dk.tigerteam.trimm.mdsd.meta.*;
import dk.tigerteam.trimm.util.Utils;

import java.util.*;

/**
 * TODO: Queries?
 */
public class GenericCQRSModelTransformer implements MetaModelTransformer {
    public static final String COMMAND_HANDLER_STEREOTYPE = "CommandHandler";
    public static final String AGGREGATE_ROOT_STEREOTYPE = "AggregateRoot";
    public static final String EVENT_STEREOTYPE = "Event";
    public static final String DELETE_EVENT_STEREOTYPE = "DeleteEvent";
    public static final String IDENTIFIER_STEREOTYPE = "Identifier";
    public static final String EVENT_LISTENER_STEREOTYPE = "EventListener";
    public static final String EVENT_SOURCED_ENTITY_STEREOTYPE = "EventSourcedEntity";

    public static final String UNHANDLED_STEREOTYPE = "Unhandled";
    public static final String HANDLES_EVENT_OPERATION_STEREOTYPE = "HandlesEvent";
    public static final String HANDLES_COMMAND_OPERATION_STEREOTYPE = "HandlesCommand";
    public static final String COMMAND_STEREOTYPE = "Command";
    public static final String CREATE_COMMAND_STEREOTYPE = "CreateCommand";

    public static final String VALUE_OBJECT_STEREOTYPE = "ValueObject";

    /**
     * The attribute key in the AggregateRoot's MetaClazz attributes where the "id" MetaProperty is stored (for use by
     * Event listeners)
     */
    public static final String AGGREGATE_ROOT_ID_ATTRIBUTE_KEY = "AggregateRootId";

    private boolean createExtensionClassesForEvents = false;
    private boolean createExtensionClassesForCommands = false;
    private boolean createExtensionClassesForValueObjects = false;

    public boolean isCreateExtensionClassesForCommands() {
        return createExtensionClassesForCommands;
    }

    public void setCreateExtensionClassesForCommands(boolean createExtensionClassesForCommands) {
        this.createExtensionClassesForCommands = createExtensionClassesForCommands;
    }

    public boolean isCreateExtensionClassesForEvents() {
        return createExtensionClassesForEvents;
    }

    public void setCreateExtensionClassesForEvents(boolean createExtensionClassesForEvents) {
        this.createExtensionClassesForEvents = createExtensionClassesForEvents;
    }

    public boolean isCreateExtensionClassesForValueObjects() {
        return createExtensionClassesForValueObjects;
    }

    public void setCreateExtensionClassesForValueObjects(boolean createExtensionClassesForValueObjects) {
        this.createExtensionClassesForValueObjects = createExtensionClassesForValueObjects;
    }

    @Override
    public void transform(MetaModel metaModel) {
        CQRSTransformationContext context = new CQRSTransformationContext(metaModel);
        transformAggregateRoots(context);
        transformEventSourcedEntities(context);
        transformEventListeners(context);
        transformCommandHandlers(context);
        transformValueObjects(context);
        transformIdentifiers(context);
    }

    private void transformIdentifiers(CQRSTransformationContext context) {
        List<MetaClazz> identifiers = context.getMetaModel().find(new MetaCriteria<MetaClazz>(MetaClazz.class) {
            @Override
            public boolean isOk(MetaClazz element) {
                return element.hasStereoType(IDENTIFIER_STEREOTYPE);
            }
        });

        for (MetaClazz identifier : identifiers) {
            identifier.add(new MetaStereoType(JavaStereotypes.NO_EXTENSION_CLASS_STEREOTYPE_NAME));
            identifier.add(new MetaStereoType(HashCodeAndEqualsListener.HASHCODE_AND_EQUALS_STEREOTYPE_NAME));
            identifier.getChildren().removeAll(identifier.getPropertiesPartInAnAssociation());
        }
    }

    private void transformEventSourcedEntities(CQRSTransformationContext context) {
        List<MetaClazz> eventSourcedEntities = context.getMetaModel().find(new MetaCriteria<MetaClazz>(MetaClazz.class) {
            @Override
            public boolean isOk(MetaClazz element) {
                return element.hasStereoType(EVENT_SOURCED_ENTITY_STEREOTYPE);
            }
        });

        for (MetaClazz eventSourcedEntity : eventSourcedEntities) {
            transformEventSourcedEntity(eventSourcedEntity, context);
        }
    }

    private void transformEventSourcedEntity(MetaClazz eventSourcedEntity, CQRSTransformationContext context) {
        Collection<MetaProperty> eventAssociations = Utils.filter(eventSourcedEntity.getPropertiesPartInAnAssociation(), new Utils.Filter<MetaProperty>() {
            @Override
            public boolean include(MetaProperty metaProperty) {
                return metaProperty.getType().hasStereoType(EVENT_STEREOTYPE) || metaProperty.getType().hasStereoType(DELETE_EVENT_STEREOTYPE);
            }
        });

        for (MetaProperty eventAssociation :  eventAssociations) {
            MetaClazz eventClazz = eventAssociation.getType();
            if (!eventAssociation.hasStereoType(UNHANDLED_STEREOTYPE)) {
                // Add event replay method to the aggregate root
                MetaOperation eventHandlerMethod = new MetaOperation("handle", false, MetaParameter.VoidReturnParameter(), new MetaParameter("event", eventClazz));
                eventHandlerMethod.add(new MetaStereoType(HANDLES_EVENT_OPERATION_STEREOTYPE));
                eventHandlerMethod.setVisibility(MetaType.Visibility.Protected);
                eventSourcedEntity.add(eventHandlerMethod);

                // Remove the association to the event as it's no longer of use
                eventSourcedEntity.getChildren().remove(eventAssociation);
            }
        }
    }

    private void transformEventListeners(CQRSTransformationContext context) {
        List<MetaClazz> eventListeners = context.getMetaModel().find(new MetaCriteria<MetaClazz>(MetaClazz.class) {
            @Override
            public boolean isOk(MetaClazz element) {
                return element.hasStereoType(EVENT_LISTENER_STEREOTYPE);
            }
        });

        for (MetaClazz eventListener : eventListeners) {
            transformEventListener(eventListener, context);
        }
    }

    private void transformEventListener(MetaClazz eventListener, CQRSTransformationContext context) {
        Collection<MetaProperty> eventAssociations = Utils.filter(eventListener.getPropertiesPartInAnAssociation(), new Utils.Filter<MetaProperty>() {
            @Override
            public boolean include(MetaProperty metaProperty) {
                return metaProperty.getType().hasStereoType(EVENT_STEREOTYPE) || metaProperty.getType().hasStereoType(DELETE_EVENT_STEREOTYPE);
            }
        });

        for (MetaProperty eventAssociation :  eventAssociations) {
            MetaClazz eventClazz = eventAssociation.getType();
            // Add event replay method to the aggregate root
            MetaOperation eventHandlerMethod = new MetaOperation("handle", false, MetaParameter.VoidReturnParameter(), new MetaParameter("event", eventClazz));
            eventHandlerMethod.add(new MetaStereoType(HANDLES_EVENT_OPERATION_STEREOTYPE));
            if (!eventListener.isInterface()) {
                eventHandlerMethod.setVisibility(MetaType.Visibility.Protected);
            }
            eventListener.add(eventHandlerMethod);
            // Remove the association to the event as it's no longer of use
            eventListener.getChildren().remove(eventAssociation);
        }

    }

    private void transformValueObjects(CQRSTransformationContext context) {
        List<MetaClazz> valueObjects = context.getMetaModel().find(new MetaCriteria<MetaClazz>(MetaClazz.class) {
            @Override
            public boolean isOk(MetaClazz element) {
                return element.hasStereoType(VALUE_OBJECT_STEREOTYPE) || element.doesInheritStereoType(VALUE_OBJECT_STEREOTYPE);
            }
        });

        for (MetaClazz valueObject : valueObjects) {
            transformValueObject(valueObject, context);
        }

    }

    private void transformValueObject(MetaClazz valueObject, CQRSTransformationContext context) {
        if (!createExtensionClassesForValueObjects)
            valueObject.add(new MetaStereoType(JavaStereotypes.NO_EXTENSION_CLASS_STEREOTYPE_NAME));

         // Make all properties in the Event immutable
        for (MetaProperty property : valueObject.getAllPropertiesIncludingSuperClazzProperties()) {
            property.setWriteable(false);
        }

        valueObject.add(new MetaStereoType(HashCodeAndEqualsListener.HASHCODE_AND_EQUALS_STEREOTYPE_NAME));
    }

    private void transformAggregateRoots(CQRSTransformationContext context) {
        List<MetaClazz> aggregateRoots = context.getMetaModel().find(new MetaCriteria<MetaClazz>(MetaClazz.class) {
            @Override
            public boolean isOk(MetaClazz element) {
                return element.hasStereoType(AGGREGATE_ROOT_STEREOTYPE);
            }
        });

        for (MetaClazz aggregateRoot : aggregateRoots) {
            transformAggregateRoot(aggregateRoot, context);
        }
    }

    private void transformAggregateRoot(MetaClazz aggregateRoot, CQRSTransformationContext context) {
        MetaProperty idProperty = Utils.single(aggregateRoot.getAllPropertiesIncludingSuperClazzProperties(), new Utils.Filter<MetaProperty>() {
            @Override
            public boolean include(MetaProperty metaProperty) {
                return metaProperty.hasStereoType(IDENTIFIER_STEREOTYPE) || metaProperty.getType().hasStereoType(IDENTIFIER_STEREOTYPE) ;
            }
        }, "Aggregate " + aggregateRoot);
        if (idProperty == null) throw new IllegalStateException("Aggregate root " + aggregateRoot.getFQCN() + " doesn't have a property with stereotype Identifier");
        idProperty.setWriteable(false);
        idProperty.setReadable(false);

        aggregateRoot.addAttribute(AGGREGATE_ROOT_ID_ATTRIBUTE_KEY, idProperty);
        context.registerAggregateRootIdProperty(aggregateRoot, idProperty);

        Collection<MetaProperty> eventAssociations = Utils.filter(aggregateRoot.getPropertiesPartInAnAssociation(), new Utils.Filter<MetaProperty>() {
            @Override
            public boolean include(MetaProperty metaProperty) {
                return metaProperty.getType().hasStereoType(EVENT_STEREOTYPE) || metaProperty.getType().hasStereoType(DELETE_EVENT_STEREOTYPE) ||
                        extendsMetaClazzWithStereotype(metaProperty.getType(), EVENT_STEREOTYPE) || extendsMetaClazzWithStereotype(metaProperty.getType(), DELETE_EVENT_STEREOTYPE);
            }
        });

        for (MetaProperty eventAssociation :  eventAssociations) {
            MetaClazz eventClazz = eventAssociation.getType();
            processEventClazz(eventClazz, aggregateRoot, idProperty);

            // Remember aggregate root and event association for later
            context.registerAggregateRootEvent(aggregateRoot, eventClazz);

            if (!eventAssociation.hasStereoType(UNHANDLED_STEREOTYPE)) {
                // Add event replay method to the aggregate root
                MetaOperation eventHandlerMethod = new MetaOperation("handle", false, MetaParameter.VoidReturnParameter(), new MetaParameter("event", eventClazz));
                eventHandlerMethod.add(new MetaStereoType(HANDLES_EVENT_OPERATION_STEREOTYPE));
                eventHandlerMethod.setVisibility(MetaType.Visibility.Protected);
                aggregateRoot.add(eventHandlerMethod);

                // Remove the association to the event as it's no longer of use
                aggregateRoot.getChildren().remove(eventAssociation);

                // Resolve event inheritance
                for (MetaClazz eventTypeSubClazz : eventClazz.getSubClazzes()) {
                    eventTypeSubClazz.add(new MetaStereoType(EVENT_STEREOTYPE));
                    // Remember aggregate root and event association for later
                    context.registerAggregateRootEvent(aggregateRoot, eventClazz);
                    for (MetaProperty property : eventTypeSubClazz.getAllPropertiesIncludingSuperClazzProperties()) {
                        property.setWriteable(false);
                    }
                    eventTypeSubClazz.add(new MetaStereoType(HashCodeAndEqualsListener.HASHCODE_AND_EQUALS_STEREOTYPE_NAME));
                    if (!createExtensionClassesForEvents)
                        eventTypeSubClazz.add(new MetaStereoType(JavaStereotypes.NO_EXTENSION_CLASS_STEREOTYPE_NAME));
                }
            }

        }

        // Make the remaining properties inaccessible from the outside
        for(MetaProperty metaProperty : aggregateRoot.getAllPropertiesIncludingSuperClazzProperties()) {
            metaProperty.setVisibility(MetaType.Visibility.Protected);
            metaProperty.setReadable(false);
            metaProperty.setWriteable(false);
        }
        handleCommandsAssociatedDirectlyWithTheAggregateRoot(aggregateRoot);
    }

    private void handleCommandsAssociatedDirectlyWithTheAggregateRoot(MetaClazz aggregateRoot) {
        // Handle AggregateRoots that have commands directly attached to them by creating an fabricated CommandHandler
        // interface that's named <AggregateRoot#Name>CommandHandler.
        Collection<MetaProperty> propertiesThatPointToACommand = Utils.filter(aggregateRoot.getAllPropertiesIncludingSuperClazzProperties(), new Utils.Filter<MetaProperty>() {
            @Override
            public boolean include(MetaProperty metaProperty) {
                return metaProperty.getType().hasStereoType(COMMAND_STEREOTYPE) || extendsMetaClazzWithStereotype(metaProperty.getType(), COMMAND_STEREOTYPE)
                        || metaProperty.getType().hasStereoType(CREATE_COMMAND_STEREOTYPE) || extendsMetaClazzWithStereotype(metaProperty.getType(), CREATE_COMMAND_STEREOTYPE);
            }
        });
        if (propertiesThatPointToACommand.size() > 0) {
            MetaInterface commandHandler = new MetaInterface(UUID.randomUUID().toString(), true);
            commandHandler.setName(aggregateRoot.getName() + "CommandHandler");
            commandHandler.add(new MetaStereoType(COMMAND_HANDLER_STEREOTYPE));
            // Transfer the metaProperty associations from the aggregate root to the new command handler interface
            for (MetaProperty metaProperty : propertiesThatPointToACommand) {
                commandHandler.add(metaProperty);
            }
            // Associate the command handler with aggregate root
            MetaProperty associationToTheAggregateRoot = new MetaProperty();
            associationToTheAggregateRoot.setType(aggregateRoot);
            commandHandler.add(associationToTheAggregateRoot);

            // Add command handler to the same package as the aggregate root
            aggregateRoot.getPackage().add(commandHandler);
        }
    }

    protected void processEventClazz(MetaClazz eventClazz, MetaClazz aggregateRoot, MetaProperty idProperty) {
        // Make all properties in the Event immutable
        for (MetaProperty property : eventClazz.getAllPropertiesIncludingSuperClazzProperties()) {
            property.setWriteable(false);
        }

        // Copy the aggregate id property
        MetaProperty idPropertyClone = idProperty.cloneSimplePropertyValues();
        idPropertyClone.setReadable(true);
        idPropertyClone.setVisibility(MetaType.Visibility.Private);
        idPropertyClone.setName(resolveCommandAndEventAggregatePropertyIdName(aggregateRoot, idProperty));
        eventClazz.add(idPropertyClone);
        eventClazz.add(new MetaStereoType(HashCodeAndEqualsListener.HASHCODE_AND_EQUALS_STEREOTYPE_NAME));

        if (!createExtensionClassesForEvents)
            eventClazz.add(new MetaStereoType(JavaStereotypes.NO_EXTENSION_CLASS_STEREOTYPE_NAME));
    }

    protected String resolveCommandAndEventAggregatePropertyIdName(MetaClazz aggregateRoot, MetaProperty idProperty) {
        if (idProperty.getName() == null && idProperty.getType().isBuiltInType()) {
            return Utils.uncapitalize(aggregateRoot.getName()) + "Id";
        }
        if (!idProperty.getType().isBuiltInType()) {
            return Utils.uncapitalize(idProperty.getType().getName());
        }
        return Utils.uncapitalize(aggregateRoot.getName()) + Utils.capitalize(idProperty.getName());
    }


    private void transformCommandHandlers(CQRSTransformationContext context) {
        List<MetaClazz> commandHandlers = context.getMetaModel().find(new MetaCriteria<MetaClazz>(MetaClazz.class) {
            @Override
            public boolean isOk(MetaClazz element) {
                return element.hasStereoType(COMMAND_HANDLER_STEREOTYPE);
            }
        });

        for (MetaClazz commandHandler : commandHandlers) {
            transformCommandHandler(commandHandler, context);
        }
    }

    private void transformCommandHandler(MetaClazz commandHandler, CQRSTransformationContext context) {
        MetaProperty aggregateRootProperty = Utils.single(commandHandler.getAllPropertiesIncludingSuperClazzProperties(), new Utils.Filter<MetaProperty>() {
            @Override
            public boolean include(MetaProperty metaProperty) {
                return metaProperty.getType().hasStereoType(AGGREGATE_ROOT_STEREOTYPE);
            }
        }, "CommandHandler " + commandHandler);
        if (aggregateRootProperty == null) throw new IllegalStateException("CommandHandler " + commandHandler.getFQCN() + " doesn't have an association to the AggregateRoot it's working on behalf");
        MetaClazz aggregateRootType = aggregateRootProperty.getType();
        context.registerCommandHandlerToAggregateRootAssociation(commandHandler, aggregateRootType);
        // Remove the association to the aggregate root as it's no longer needed
        commandHandler.getChildren().remove(aggregateRootProperty);

        Collection<MetaProperty> commandAssociations = Utils.filter(commandHandler.getPropertiesPartInAnAssociation(), new Utils.Filter<MetaProperty>() {
            @Override
            public boolean include(MetaProperty metaProperty) {
                MetaClazz type = metaProperty.getType();
                return type.hasStereoType(COMMAND_STEREOTYPE) || type.hasStereoType(CREATE_COMMAND_STEREOTYPE)
                        || extendsMetaClazzWithStereotype(type, COMMAND_STEREOTYPE) || extendsMetaClazzWithStereotype(type, CREATE_COMMAND_STEREOTYPE);
            }
        });

        MetaProperty aggregateRootIdProperty = context.getAggregateRootIdProperty(aggregateRootType);
        for (MetaProperty commandAssociation :  commandAssociations) {

            MetaClazz commandClazz = commandAssociation.getType();
            processCommandClazz(commandClazz, aggregateRootType, aggregateRootIdProperty);


            // Remember command handler and command association for later
            context.registerCommandHandlerCommand(commandHandler, commandClazz);

            // Add command handle method to the command handler
            MetaOperation cmdHandlerMethod = new MetaOperation("handle", false, MetaParameter.VoidReturnParameter(), new MetaParameter("cmd", commandClazz));
            if (commandHandler.isInterface())
                cmdHandlerMethod.setVisibility(MetaType.Visibility.Package);
            else
                cmdHandlerMethod.setVisibility(MetaType.Visibility.Public);

            cmdHandlerMethod.add(new MetaStereoType(HANDLES_COMMAND_OPERATION_STEREOTYPE));
            commandHandler.add(cmdHandlerMethod);

            // Remove the association to the command as it's no longer of use
            commandHandler.getChildren().remove(commandAssociation);
        }
    }

    protected void processCommandClazz(MetaClazz commandClazz, MetaClazz aggregateRoot, MetaProperty aggregateRootIdProperty) {
        // Make all properties in the Command immutable
        for (MetaProperty property : commandClazz.getAllPropertiesIncludingSuperClazzProperties()) {
            property.setWriteable(false);
        }

        // Copy the aggregate id property, unless it's a CreateCommand (which should not the the aggregateRootIdProperty as they can't carry the aggregate id (only ClientCreateCommand can do this))
        if (commandClazz.hasStereotypeUpwardsRecursively(COMMAND_STEREOTYPE)) {
            MetaProperty idPropertyClone = aggregateRootIdProperty.cloneSimplePropertyValues();
            idPropertyClone.setReadable(true);
            idPropertyClone.setVisibility(MetaType.Visibility.Private);
            idPropertyClone.setName(resolveCommandAndEventAggregatePropertyIdName(aggregateRoot, aggregateRootIdProperty));
            commandClazz.add(idPropertyClone);
        }
        commandClazz.add(new MetaStereoType(HashCodeAndEqualsListener.HASHCODE_AND_EQUALS_STEREOTYPE_NAME));

        if (!createExtensionClassesForCommands)
            commandClazz.add(new MetaStereoType(JavaStereotypes.NO_EXTENSION_CLASS_STEREOTYPE_NAME));
    }

    private boolean extendsMetaClazzWithStereotype(MetaClazz metaClazz, String stereotype) {
        if (metaClazz.getSuperClazz() != null) {
            if (metaClazz.getSuperClazz().hasStereoType(stereotype)) return true;
            return extendsMetaClazzWithStereotype(metaClazz.getSuperClazz(), stereotype);
        }
        return false;
    }

    public static class CQRSTransformationContext {
        private Map<MetaClazz, MetaProperty> aggregateRootToIdPropertyMap = Maps.newHashMap();
        private ListMultimap<MetaClazz, MetaClazz> aggregateRootToEventMap = ArrayListMultimap.create();
        private ListMultimap<MetaClazz, MetaClazz> aggregateRootToCommandHandlerMap = ArrayListMultimap.create();
        private Map<MetaClazz, MetaClazz> commandHandlerToAggregateRootMap = Maps.newHashMap();
        private ListMultimap<MetaClazz, MetaClazz> commandHandlerToCommandMap = ArrayListMultimap.create();
        private MetaModel metaModel;

        public CQRSTransformationContext(MetaModel metaModel) {
            this.metaModel = metaModel;
        }

        public MetaModel getMetaModel() {
            return metaModel;
        }

        public void registerAggregateRootIdProperty(MetaClazz aggregateRoot, MetaProperty idProperty) {
            aggregateRootToIdPropertyMap.put(aggregateRoot, idProperty);
        }

        public void registerAggregateRootEvent(MetaClazz aggregateRoot, MetaClazz eventClazz) {
            aggregateRootToEventMap.put(aggregateRoot, eventClazz);
        }

        public void registerCommandHandlerToAggregateRootAssociation(MetaClazz commandHandler, MetaClazz aggregateRootType) {
            aggregateRootToCommandHandlerMap.put(aggregateRootType, commandHandler);
            commandHandlerToAggregateRootMap.put(commandHandler, aggregateRootType);
        }

        public MetaProperty getAggregateRootIdProperty(MetaClazz aggregateRootType) {
            if (aggregateRootToIdPropertyMap.containsKey(aggregateRootType))
                return aggregateRootToIdPropertyMap.get(aggregateRootType);
            throw new IllegalStateException("Didn't find a mapping between " + aggregateRootType + " and its Id property");
        }

        public void registerCommandHandlerCommand(MetaClazz commandHandler, MetaClazz commandType) {
            commandHandlerToCommandMap.put(commandHandler, commandType);
        }
    }
}
