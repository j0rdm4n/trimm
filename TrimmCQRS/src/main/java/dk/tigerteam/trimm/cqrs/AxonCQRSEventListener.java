/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.cqrs;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import org.axonframework.commandhandling.annotation.CommandHandler;
import org.axonframework.domain.IdentifierFactory;
import org.axonframework.eventhandling.annotation.EventHandler;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedAggregateRoot;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedEntity;
import org.axonframework.eventsourcing.annotation.AggregateIdentifier;
import org.axonframework.eventsourcing.annotation.EventSourcedMember;
import org.axonframework.unitofwork.UnitOfWork;

import java.io.Serializable;
import java.util.Set;

public class AxonCQRSEventListener extends AbstractGeneratorEventListener {
    @Override
    protected boolean handleClazzEvent(final ClazzEvent event) {
        final Clazz clazz = event.getClazz();
        if (event.getMetaClazz().hasStereotypeUpwardsRecursively(GenericCQRSModelTransformer.COMMAND_STEREOTYPE) || event.getMetaClazz().hasStereotypeUpwardsRecursively(GenericCQRSModelTransformer.EVENT_STEREOTYPE) || event.getMetaClazz().hasStereotypeUpwardsRecursively(GenericCQRSModelTransformer.VALUE_OBJECT_STEREOTYPE)) {
            clazz.addImplementsTypes(Serializable.class);
        } else if (event.getMetaClazz().hasStereotypeUpwardsRecursively(GenericCQRSModelTransformer.AGGREGATE_ROOT_STEREOTYPE) && clazz.isBaseClazz()) {
            // Add default constructor for framework support
            clazz.add(new Constructor(CodeElement.AccessModifier.Protected).addAnnotations(new Annotation(SuppressWarnings.class).addAnnotationAttributes(AnnotationAttribute.defaultValue("UnusedDeclaration"))));

            // AggregateRoot should extend AbstractAnnotatedAggregateRoot<IDENTIFIER>
            // Find the identifier from the Clazz deferred (since properties are handled after the Clazz has been created)
            getContext().queueDeferredEventHandler(new DeferredEventHandler() {
                @Override
                public void execute() {
                    final MetaProperty idMetaProperty = (MetaProperty) event.getMetaClazz().getAttribute(GenericCQRSModelTransformer.AGGREGATE_ROOT_ID_ATTRIBUTE_KEY);
                    if (idMetaProperty == null) throw new IllegalStateException("Couldn't find a MetaProperty with stereotype " + GenericCQRSModelTransformer.IDENTIFIER_STEREOTYPE + " in AggregateRoot Meta Class: " + event.getMetaClazz());

                    ClazzOrInterface idClazz = getContext().findClazzFromMetaClazz(idMetaProperty.getType());
                    Type idType = null;
                    if (idClazz == null) {
                        idType = new Type(idMetaProperty.getType().getName());
                    } else {
                        idType = new Type(idClazz);
                    }

                    // Handle extends
                    Type extendsType = new Type(AbstractAnnotatedAggregateRoot.class);
                    extendsType.addGenerics(idType);
                    clazz.setExtendsType(extendsType);
                }

                @Override
                public String getDescription() {
                    return "Resolving AggregateRoot inheritance from Axon base classes for AggregateRoot " + event.getMetaClazz();
                }
            });
        } else if (event.getMetaClazz().hasStereotypeUpwardsRecursively(GenericCQRSModelTransformer.AGGREGATE_ROOT_STEREOTYPE) && clazz.isBaseClazz()) {
            // Add default constructor for framework support
            clazz.add(new Constructor(CodeElement.AccessModifier.Protected).addAnnotations(new Annotation(SuppressWarnings.class).addAnnotationAttributes(AnnotationAttribute.defaultValue("UnusedDeclaration"))));
            clazz.setExtendsType(AbstractAnnotatedEntity.class);
        } else if (event.getMetaClazz().hasStereoType(GenericCQRSModelTransformer.IDENTIFIER_STEREOTYPE)) {
            clazz.addImplementsTypes(Serializable.class);
            // Add the identifier Property
            Field identifierField = new Field("identifier", new Type(String.class), CodeElement.AccessModifier.Private, CodeElement.AccessModifier.Final);
            Property idProperty = new Property(clazz, identifierField, CodeElement.AccessModifier.Public);
            idProperty.setGetterMethod(new GetterMethod(CodeElement.AccessModifier.Public));

            // Add default constructor which uses Axon IdentifierFactory
            clazz.add(new Constructor(CodeElement.AccessModifier.Public)
                            .addCode(new CodeSnippet("this.identifier = IdentifierFactory.getInstance().generateIdentifier();"))
            );
            clazz.addImport(IdentifierFactory.class);

            // Add a toString that returns the identifier (this will by-pass the normal ToStringListener's toString generator)
            Method toString = (Method) new Method("toString", CodeElement.AccessModifier.Public).setReturnType(String.class).addAnnotations(new Annotation(Override.class));
            toString.addCode(new CodeSnippet("return identifier;"));
            clazz.add(toString);
        }


        return super.handleClazzEvent(event);
    }

    @Override
    protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
        boolean hasIdentifierStereotype = event.getMetaProperty().hasStereoType(GenericCQRSModelTransformer.IDENTIFIER_STEREOTYPE) || event.getMetaProperty().getType().hasStereoType(GenericCQRSModelTransformer.IDENTIFIER_STEREOTYPE);
        if (hasIdentifierStereotype && event.getMetaProperty().getOwnerClazz().hasStereoType(GenericCQRSModelTransformer.AGGREGATE_ROOT_STEREOTYPE)) {
            Field field = event.getProperty().getField();
            field.addAnnotations(new Annotation(AggregateIdentifier.class));
            Set<CodeElement.AccessModifier> accessModifiers = field.getAccessModifiers();
            accessModifiers.clear();
            accessModifiers.add(CodeElement.AccessModifier.Protected);
        }
        return super.handleSimplePropertyEvent(event);
    }

    @Override
    protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
        boolean hasIdentifierStereotype = event.getMetaProperty().hasStereoType(GenericCQRSModelTransformer.IDENTIFIER_STEREOTYPE) || event.getMetaProperty().getType().hasStereoType(GenericCQRSModelTransformer.IDENTIFIER_STEREOTYPE);
        if (hasIdentifierStereotype && event.getMetaProperty().getOwnerClazz().hasStereoType(GenericCQRSModelTransformer.AGGREGATE_ROOT_STEREOTYPE)) {
            Field field = event.getProperty().getField();
            field.addAnnotations(new Annotation(AggregateIdentifier.class));
            Set<CodeElement.AccessModifier> accessModifiers = field.getAccessModifiers();
            accessModifiers.clear();
            accessModifiers.add(CodeElement.AccessModifier.Protected);
        }  else if (event.getMetaProperty().getType().hasStereoType(GenericCQRSModelTransformer.EVENT_SOURCED_ENTITY_STEREOTYPE)) {
            event.getProperty().getField().addAnnotations(new Annotation(EventSourcedMember.class));
        }
        return super.handleOneToOneAssociationEvent(event);
    }

    @Override
    protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
        if (event.getMetaProperty().getType().hasStereoType(GenericCQRSModelTransformer.EVENT_SOURCED_ENTITY_STEREOTYPE)) {
            event.getProperty().getField().addAnnotations(new Annotation(EventSourcedMember.class));
        }
        return super.handleOneToManyAssociationEvent(event);
    }

    @Override
    protected boolean handleMethodEvent(MethodEvent event) {
        if (event.getOperation().hasStereoType(GenericCQRSModelTransformer.HANDLES_EVENT_OPERATION_STEREOTYPE)) {
            event.getMethod().addAnnotations(new Annotation(EventHandler.class));
        } else if (event.getOperation().hasStereoType(GenericCQRSModelTransformer.HANDLES_COMMAND_OPERATION_STEREOTYPE)) {
            event.getMethod().addAnnotations(new Annotation(CommandHandler.class));
            event.getMethod().addParameters(new Parameter("unitOfWork", UnitOfWork.class));
        }
        return true;
    }
}
