package dk.tigerteam.trimm.cqrs;

import dk.tigerteam.trimm.mdsd.java.configuration.EventListenerConfiguration;
import dk.tigerteam.trimm.mdsd.java.configuration.JavaConfiguration;
import dk.tigerteam.trimm.mdsd.java.configuration.PojoConfigurationBasedCodeGenerator;
import dk.tigerteam.trimm.mdsd.java.generator.extension.HashCodeAndEqualsListener;
import dk.tigerteam.trimm.mdsd.java.generator.extension.ToStringListener;
import org.junit.Test;

import java.io.File;

public class AxonTraderCQRSTest {

    public void manual_test() {
        JavaConfiguration javaConfiguration = new JavaConfiguration();
        javaConfiguration.setUmlTool("EA");
        javaConfiguration.setXmiModelPath("src/test/resources/AxonTrader.xml");
        javaConfiguration.setUseImports(true);
        javaConfiguration.setBasePackageName("dk.tigerteam.axon");
        javaConfiguration.setGenerateExtensionClasses(true);
        javaConfiguration.setAddExtensionAndBaseClassAnnotations(true);
        javaConfiguration.setGenerateBaseClassesToPath("target/generated-sources/axontrader-base");
        javaConfiguration.setGenerateExtensionClassesToPath("target/generated-sources/axontrader-extension");

        javaConfiguration.getEventListeners().add(new EventListenerConfiguration(AxonCQRSEventListener.class));
        javaConfiguration.getEventListeners().add(new EventListenerConfiguration(HashCodeAndEqualsListener.class));
        javaConfiguration.getEventListeners().add(new EventListenerConfiguration(ToStringListener.class));

        PojoConfigurationBasedCodeGenerator codeGenerator = new PojoConfigurationBasedCodeGenerator(javaConfiguration, new File(""), new File(""), null);
        codeGenerator.addMetaModelTransformer(new GenericCQRSModelTransformer());
        codeGenerator.generate();

    }
}
