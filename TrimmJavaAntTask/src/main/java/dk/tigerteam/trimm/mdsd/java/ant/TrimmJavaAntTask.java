/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.java.ant;

import dk.tigerteam.trimm.mdsd.java.configuration.JavaConfiguration;
import dk.tigerteam.trimm.mdsd.java.configuration.PojoConfigurationBasedCodeGenerator;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import java.io.File;
import java.io.FileNotFoundException;

public class TrimmJavaAntTask extends Task {

    private File yamlFile;
    private File outputDir;

    public void execute() {

        if (yamlFile == null) {
            throw new BuildException("There has to be a configured yaml file.");
        }
        this.log("Generation Java sourcecode files from configuration file '" + yamlFile.getAbsolutePath() + "'");
        File baseDir = getProject().getBaseDir();
        getProject().log("BaseDir: '" + baseDir + "'");

        JavaConfiguration jpaConfiguration;
        try {
            jpaConfiguration = PojoConfigurationBasedCodeGenerator.parseJavaConfiguration(yamlFile, false);
        } catch (FileNotFoundException e) {
            throw new BuildException("Could not find configured yaml file.", e);
        }

        if (outputDir == null) {
            outputDir = new File(baseDir.getAbsolutePath() + File.separatorChar + "generated");
        }
        getProject().log("Output dir: '" + outputDir.getAbsolutePath() + "'");
        outputDir.mkdirs();

        PojoConfigurationBasedCodeGenerator.generate(jpaConfiguration, yamlFile, baseDir, outputDir, getClass().getClassLoader());
    }

    public void setYamlFile(File yamlFile) {
        this.yamlFile = yamlFile;
    }

    /**
     * Optional - defaults to BaseDir + "/generated" for the project
     *
     * @param outputDir
     */
    public void setOutputDir(File outputDir) {
        this.outputDir = outputDir;
    }

}
