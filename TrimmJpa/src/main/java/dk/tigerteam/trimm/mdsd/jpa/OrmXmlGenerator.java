/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.util.List;

/**
 * Generate an XML {@link Document} which adheres to the JPA Orm.xml standard.<br/>
 * In this ORM XML are all {@link Entity} and {@link Embeddable} (see #isWriteOutEmbeddables() (default value is false)) {@link Clazz}' that
 * we generated code for in the {@link JavaGenerator}.
 *
 * @author Jeppe Cramon
 * @see #isWriteOutEmbeddables() (default value is false)
 * @see #setWriteOutEmbeddables(boolean)
 */
public class OrmXmlGenerator {
    private static final Log log = LogFactory.getLog(OrmXmlGenerator.class);
    public static final Namespace defaultNs = Namespace.getNamespace("http://java.sun.com/xml/ns/persistence/orm");
    public static final Namespace xsiNs = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
    private boolean writeOutEmbeddables = false;


    private JpaVersion jpaVersion = JpaVersion.Version2;

    /**
     * Which Jpa Version should be generated against. Default is {@link JpaVersion#Version2}
     *
     * @return
     */
    public JpaVersion getJpaVersion() {
        return jpaVersion;
    }

    /**
     * Which Jpa Version should be generated against
     *
     * @param jpaVersion
     */
    public void setJpaVersion(JpaVersion jpaVersion) {
        this.jpaVersion = jpaVersion;
    }

    /**
     * Should the generator write out Embeddables in the orm.xml file (this isn't necessary as long as embeddable objects are annotated with
     * {@link Embeddable}, which is exactly what this generator is looking for to decide if an entity is embeddable).
     */
    public boolean isWriteOutEmbeddables() {
        return writeOutEmbeddables;
    }

    /**
     * Should the generator write out Embeddables in the orm.xml file (this isn't necessary as long as embeddable objects are annotated with
     * {@link Embeddable}, which is exactly what this generator is looking for to decide if an entity is embeddable).
     */
    public void setWriteOutEmbeddables(boolean writeOutEmbeddables) {
        this.writeOutEmbeddables = writeOutEmbeddables;
    }

    /**
     * Generate ORM.xml contents for the {@link Entity} and {@link Embeddable} {@link Clazz}' in the clazzes argument.
     *
     * @param clazzes The clazzes to generate for.
     * @return The JDom document containing the orm xml contents.
     */
    public Document generate(List<ClazzOrInterface> clazzes) {
        Document doc = new Document();
        Element entityMappingsElement = new Element("entity-mappings", defaultNs);
        switch (jpaVersion) {
            case Version1:
                entityMappingsElement.setAttribute("schemaLocation",
                        "http://java.sun.com/xml/ns/persistence/orm http://java.sun.com/xml/ns/persistence/orm_1_0.xsd", xsiNs);
                entityMappingsElement.setAttribute("version", "1.0");
            case Version2:
                entityMappingsElement.setAttribute("schemaLocation",
                        "http://java.sun.com/xml/ns/persistence/orm http://java.sun.com/xml/ns/persistence/orm_2_0.xsd", xsiNs);
                entityMappingsElement.setAttribute("version", "2.0");
            default:
                break;
        }
        doc.addContent(entityMappingsElement);

        // Write out all entitiy classes
        for (ClazzOrInterface clazzOrInterface : clazzes) {
            if (!clazzOrInterface.isInterface()) {
                Clazz clazz = (Clazz) clazzOrInterface;
                if (clazz.hasAnnotation(Entity.class) /*&& !clazz.hasAccessModifier(AccessModifier.Abstract)*/) {
                    Element entityElement = new Element("entity", defaultNs);
                    entityElement.setAttribute("class", clazz.getFQCN());
                    entityMappingsElement.addContent(entityElement);
                } else if (clazz.hasAnnotation(Embeddable.class) && writeOutEmbeddables) {
                    Element entityElement = new Element("embeddable", defaultNs);
                    entityElement.setAttribute("class", clazz.getFQCN());
                    entityMappingsElement.addContent(entityElement);
                }
            }
        }
        log.info("Done generating ORM xml document");
        return doc;
    }
}
