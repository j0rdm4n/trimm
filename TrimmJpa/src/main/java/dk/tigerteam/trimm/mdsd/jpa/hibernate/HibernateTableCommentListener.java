/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.hibernate;

import dk.tigerteam.trimm.mdsd.java.generator.event.AbstractGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.DeferredEventHandler;
import org.hibernate.annotations.Table;

/**
 * Adds a (or reuses an existing) Hibernate {@link Table} annotation, and supplies the comment="" attribute with a compount value, which is
 * a mix of origial entity class name, entity comment, all properties and associations with their belonging original names and eventual
 * comment
 * <p/>
 * <b>Note: Must be run after a {@link JPANamedTablesAndColumnsListener} to ensure that we have a Table name to use for the {@link Table}
 * "appliesTo" attribute.
 * INCOMPLETE
 * @author Jeppe Cramon
 */
public class HibernateTableCommentListener extends AbstractGeneratorEventListener {
	@Override
	protected boolean handleClazzEvent(final ClazzEvent event) {
		getContext().queueDeferredEventHandler(new DeferredEventHandler() {

			public void execute() {
				// Run through all properties and gather mappings and docs
			}

			public String getDescription() {
				return "Table comment for Clazz " + event.getClazz() + "'";
			}
		});
		return true;
	}
}
