/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;

import javax.persistence.*;
import java.util.List;

/**
 * JPA specialized {@link GeneratorEventListener} class, which splits {@link GeneratorEvent}' into more specialized method, depending on the
 * nature and configuration of each {@link GeneratorEvent}, see the documentation for the top level methods (those inherited from
 * {@link AbstractGeneratorEventListener}) and move downwards into the specialized methods to understand the flow.
 * 
 * @author Jeppe Cramon
 */
public abstract class AbstractJpaGeneratorEventListener extends AbstractGeneratorEventListener {
	/**
	 * Stereotype that can be applied to properties to indicate that they are Lob candidates (meaning
	 * {@link #isLobCandidate(SimplePropertyEvent)} will return true for them no matter what)
	 */
	public static String LOB_STEREOTYPE = "LOB";
  /**
   * Can be applied to a property to indicate that the property is @Transient (ie. shouldn't be persisted)
   */
	public static String TRANSIENT_STEREOTYPE = "TRANSIENT";

	private int stringIsLobCandidateUpperBound = 4000;

	public AbstractJpaGeneratorEventListener() {
		super();
	}

	@Override
	protected JpaGeneratorContext getContext() {
		try {
			return super.getContext(JpaGeneratorContext.class);
		} catch (IllegalStateException e) {
			throw new IllegalStateException("You need to use a " + JpaJavaGenerator.class.getName()
					+ " class (or a subclass thereof) together with " + this.getClass().getName());
		}
	}

	/**
	 * The upperbound value above which a String will be marked as a {@link Lob} (default value is 4000)
	 */
	public int getStringIsLobCandidateUpperBound() {
		return stringIsLobCandidateUpperBound;
	}

	/**
	 * The upperbound value above which a String will be marked as a {@link Lob} (default value is 4000)
	 */
	public void setStringIsLobCandidateUpperBound(int stringIsLobCandidateUpperBound) {
		this.stringIsLobCandidateUpperBound = stringIsLobCandidateUpperBound;
	}

	/**
	 * Should we generate a RequiredConstraint (like nullable on Columns or Optional on Associations)
	 * 
	 * @param property
	 *          The property
	 * @return true if we should generate the constraint, otherwise false
	 */
	boolean shouldGenerateRequiredConstraintForProperty(Property property) {
		return getContext().getInheritanceStrategy().shouldGenerateRequiredConstraintForProperty(property);
	}

	/**
	 * Is the given {@link Clazz} an {@link Entity} class (ie. not an embeddable clazz or an enumeration clazz) - Sugar for
	 * {@link JpaInheritanceAndClazzesStrategy#isEntityClazz(Clazz)}
	 * 
	 * @param clazz
	 *          The clazz to test
	 * @return true if {@link Clazz} is an entity, otherwise false
	 */
	boolean isEntityClazz(Clazz clazz) {
		return getContext().getInheritanceStrategy().isEntityClazz(clazz);
	}

	/**
	 * Is the given type is a JPA {@link Embeddable} class - Sugar for {@link JpaInheritanceAndClazzesStrategy#isEmbeddableClazz(Type)}
	 * 
	 * @param type
	 *          The type to test
	 * @return true if the type is an embeddable entity, otherwise false
	 */
	boolean isEmbeddableClazz(Type type) {
		return getContext().getInheritanceStrategy().isEmbeddableClazz(type);
	}

	/**
	 * Is the given {@link Clazz} is a JPA {@link Embeddable} class - Sugar for
	 * {@link JpaInheritanceAndClazzesStrategy#isEmbeddableClazz(Clazz)}
	 * 
	 * @param clazz
	 *          The {@link Clazz} to test
	 * @return true if the {@link Clazz} is an embeddable entity, otherwise false
	 */
	boolean isEmbeddableClazz(Clazz clazz) {
		return getContext().getInheritanceStrategy().isEmbeddableClazz(clazz);
	}

	/**
	 * Is the given {@link MetaClazz} marked as {@link Embeddable} - Sugar for
	 * {@link JpaInheritanceAndClazzesStrategy#isEmbeddableClazz(MetaClazz)}
	 * 
	 * @param metaClazz
	 *          The {@link MetaClazz} to test
	 * @return true if the {@link MetaClazz} is an embeddable entity, otherwise false
	 */
	boolean isEmbeddableClazz(MetaClazz metaClazz) {
		return getContext().getInheritanceStrategy().isEmbeddableClazz(metaClazz);
	}

	/**
	 * Test if a given {@link Clazz} is an {@link Enumeration} clazz - Sugar for
	 * {@link JpaInheritanceAndClazzesStrategy#isEnumerationClazz(Clazz)}
	 * 
	 * @param clazz
	 *          The clazz to test
	 * @return true if the {@link Clazz} is an enumeration, otherwise false
	 */
	boolean isEnumerationClazz(Clazz clazz) {
		return getContext().getInheritanceStrategy().isEnumerationClazz(clazz);
	}

	/**
	 * Test if a given type refers to (through {@link Type#getClazz()}) is an {@link Enumeration} clazz - Sugar for
	 * {@link JpaInheritanceAndClazzesStrategy#isEnumerationClazz(Type)}
	 * 
	 * @param type
	 *          The clazz to test
	 * @return true if the {@link Clazz} the type refers to is an enumeration, otherwise false
	 */
	boolean isEnumerationClazz(Type type) {
		return getContext().getInheritanceStrategy().isEnumerationClazz(type);
	}

	/**
	 * Is this clazz at the root level Entity clazz wize (this will typically be the top level Entity clazz which carries the
	 * {@link Inheritance} annotation).
	 * 
	 * @param clazz
	 *          The clazz to test
	 * @return true if the clazz is a root level entity clazz
	 */
	boolean isRootEntityClazz(Clazz clazz) {
		return getContext().getInheritanceStrategy().isRootEntityClazz(clazz);
	}

	/**
	 * Handles {@link Clazz} event, by splitting the event up into {@link #handleEntityClazzEvent(ClazzEvent)} for {@link Entity} clazzes and
	 * {@link #handleEmbeddableClazzEvent(ClazzEvent)} for {@link Embeddable} clazzes.
	 * <p/>
	 * <i>Note: There should rarely be a reason to override this method</i>
	 * 
	 * @see #isEntityClazz(Clazz)
	 * @see #isEmbeddableClazz(Clazz)
	 * @see #handleEmbeddableClazzEvent(ClazzEvent)
	 * @see #handleEntityClazzEvent(ClazzEvent)
	 */
	@Override
	protected boolean handleClazzEvent(ClazzEvent event) {
		if (isEntityClazz(event.getClazz())) {
			return handleEntityClazzEvent(event);
		} else if (isEmbeddableClazz(event.getClazz())) {
			// Sanity check
			CodeModel codeModel = event.getClazz().getCodeModel();
			List<Clazz> subClasses = codeModel.findAllDirectSubClazzesOfClazz(event.getClazz());
			if (subClasses.size() > 0 && !isAllowSubClassingOfEmbeddables()) {
				throw new RuntimeException("Clazz '" + event.getClazz().getFQCN() + "' is marked as embeddable, but it has " + subClasses.size()
						+ " subclasses. We don't support inheritance of embeddables at this moment!");
			}

			return handleEmbeddableClazzEvent(event);
		}
		return true;
	}

	/**
	 * Do we allow an embeddable to have subclasses (the subclasses should typically be embeddables them selves, although some products may
	 * support mixing embeddable and entity hierarchies)
	 */
	public boolean isAllowSubClassingOfEmbeddables() {
		return true;
	}

	/**
	 * Handle simple properties.<br/>
	 * Splits into the following methods (their names so be self explanatory to what they're responsible for).
	 * <ul>
	 * <li>{@link #handleCollectionOfEmbeddableElementsProperty(SimplePropertyEvent)}</li>
	 * <li>{@link #handleCollectionOfBasicTypesProperty(SimplePropertyEvent)}</li>
	 * <li>{@link #handleBasicEmbeddedProperty(SimplePropertyEvent)}</li>
	 * <li>{@link #handleBasicEnumerationProperty(SimplePropertyEvent)}</li>
	 * <li>{@link #handleBasicLobProperty(SimplePropertyEvent)} (see {@link #isLobCandidate(SimplePropertyEvent)})</li>
	 * <li>{@link #handleBasicProperty(SimplePropertyEvent)}</li>
	 * </ul>
	 * Note: Ignores static fields.
	 */
	@Override
	protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
		if (event.getClazzOrInterface().isInterface()) {
			return true;
		}
		if (event.getProperty().getField().hasAccessModifier(AccessModifier.Static)) {
			return true;
		}
		if(event.getMetaProperty().hasStereoType(TRANSIENT_STEREOTYPE)) {
            if (!event.getProperty().getField().hasAnnotation(Transient.class)) {
			    event.getProperty().getField().addAnnotations(new Annotation(Transient.class));
            }
			return true;
		}

		// Try and spot SimpleProperties that really shouldn't be SimpleProperties
		if (event.getProperty().getField().getType().getClazz() != null && isEntityClazz(event.getProperty().getField().getType().getClazz())) {
			throw new IllegalStateException("MetaProperty " + event.getMetaProperty() + " in " + event.getClazz().getMetaType()
					+ " shouldn't be marked as a SimpleProperty, because its type " + event.getProperty().getField().getType().getClazz()
					+ "  is an Entity Clazz");
		}
		if (event.getMetaProperty().getCardinalityType() == CardinalityType.Many || event.getMetaProperty().getType().isArray()) {
			if (!event.getMetaProperty().getType().isArray() && isEmbeddableClazz(event.getProperty().getField().getType().getGenerics().get(0))) {
				return handleCollectionOfEmbeddableElementsProperty(event);
			} else {
				// Handle Lobs
				if (isLobCandidate(event)) {
					return handleBasicLobProperty(event);
				} else {
					return handleCollectionOfBasicTypesProperty(event);
				}
			}
		} else {
			if (isEmbeddableClazz(event.getProperty().getField().getType())) {
				return handleBasicEmbeddedProperty(event);
			} else if (isEnumerationClazz(event.getProperty().getField().getType())) {
				return handleBasicEnumerationProperty(event);
			} else {
				// Handle Lobs
				if (isLobCandidate(event)) {
					return handleBasicLobProperty(event);
				} else {
					return handleBasicProperty(event);
				}
			}
		}
	}

	/**
	 * Handles {@link OneToOneAssociationEvent}'.<br/>
	 * Splits into the following methods (their names so be self explanatory to what they're responsible for).
	 * <ul>
	 * <li>{@link #handleOneToOneOfEnumerationElement(OneToOneAssociationEvent)}</li>
	 * <li>{@link #handleOneToOneOfEmbeddableElement(OneToOneAssociationEvent)}</li>
	 * <li>{@link #handleOneToOneOwnerOfAssociation(OneToOneAssociationEvent)}</li>
	 * <li>{@link #handleOneToOneOwnedByAssociation(OneToOneAssociationEvent)}</li>
	 * </ul>
	 */
	@Override
	protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
		if (event.getClazzOrInterface().isInterface()) {
			return true;
		}
		MetaProperty metaProperty = event.getMetaProperty();
		if (isEnumerationClazz(event.getProperty().getField().getType().getClazz())) {
			return handleOneToOneOfEnumerationElement(event);
		} else if (isEmbeddableClazz(event.getProperty().getField().getType())) {
			return handleOneToOneOfEmbeddableElement(event);
		}

		boolean isBidirectional = metaProperty.isPartInAnAssociation() && metaProperty.getAssociation().isBidirectional();
		if (metaProperty.isOwnerOfAssociation()) {
			return handleOneToOneOwnerOfAssociation(event);
		} else if (isBidirectional) {
			return handleOneToOneOwnedByAssociation(event);
		}
		return true;
	}

	/**
	 * Handles {@link OneToManyAssociationEvent}'.<br/>
	 * Splits into the following methods (their names so be self explanatory to what they're responsible for).
	 * <ul>
	 * <li>{@link #handleOneToManyOfEnumeratedElements(OneToManyAssociationEvent)}</li>
	 * <li>{@link #handleOneToManyOfEmbeddableElements(OneToManyAssociationEvent)}</li>
	 * <li>{@link #handleOneToManyOwnedByAssociation(OneToManyAssociationEvent)}</li>
	 * <li>{@link #handleOneToManyOwnerOfAssociation(OneToManyAssociationEvent)}</li>
	 * </ul>
	 */
	@Override
	protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
		if (event.getClazzOrInterface().isInterface()) {
			return true;
		}
		if (isEnumerationClazz(((Type) event.getProperty().getField().getType().getChildren().get(0)).getClazz())) {
			return handleOneToManyOfEnumeratedElements(event);
		} else if (isEmbeddableClazz(((Type) event.getProperty().getField().getType().getChildren().get(0)).getClazz())) {
			return handleOneToManyOfEmbeddableElements(event);
		}

		MetaProperty metaProperty = event.getMetaProperty();
		boolean isBidirectional = metaProperty.isPartInAnAssociation() && metaProperty.getAssociation().isBidirectional();
		if (isBidirectional) {
			return handleOneToManyOwnedByAssociation(event);
		} else if (metaProperty.isOwnerOfAssociation()) {
			return handleOneToManyOwnerOfAssociation(event);
		} else {
			throw new IllegalStateException("Unknown state for metaProperty " + metaProperty);
		}
	}

	/**
	 * Handles {@link ManyToOneAssociationEvent}'.<br/>
	 * Splits into the following methods (their names so be self explanatory to what they're responsible for).
	 * <ul>
	 * <li>{@link #handleManyToOneOfEmbeddableElement(ManyToOneAssociationEvent)}</li>
	 * <li>{@link #handleManyToOneOwnerOfAssociation(ManyToOneAssociationEvent)}</li>
	 * </ul>
	 */
	@Override
	protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
		if (event.getClazzOrInterface().isInterface()) {
			return true;
		}
		MetaProperty metaProperty = event.getMetaProperty();
		if (isEnumerationClazz(event.getProperty().getField().getType().getClazz()) && metaProperty.isOwnerOfAssociation()) {
			throw new RuntimeException("Can't create a ManyToOne to an Enum for MetaProperty " + metaProperty + " in clazz "
					+ event.getProperty().getOwner());
		}

		if (isEmbeddableClazz(event.getProperty().getField().getType().getClazz())) {
			return handleManyToOneOfEmbeddableElement(event);
		}

		boolean isBidirectional = metaProperty.isPartInAnAssociation() && metaProperty.getAssociation().isBidirectional();
		if (isBidirectional || metaProperty.isOwnerOfAssociation()) {
			return handleManyToOneOwnerOfAssociation(event);
		} else {
			throw new IllegalStateException("Unknown state for metaProperty " + metaProperty);
		}
	}

	/**
	 * Called ManyToOne association to an {@link Embeddable} entity
	 */
	protected abstract boolean handleManyToOneOfEmbeddableElement(ManyToOneAssociationEvent event);

	/**
	 * Handles {@link ManyToOneAssociationEvent}'.<br/>
	 * Splits into the following methods (their names so be self explanatory to what they're responsible for).
	 * <ul>
	 * <li>{@link #handleManyToManyOwnerOfAssociation(ManyToManyAssociationEvent)}</li>
	 * <li>{@link #handleManyToManyOwnedByAssociation(ManyToManyAssociationEvent)}</li>
	 * </ul>
	 */
	@Override
	protected boolean handleManyToManyAssociationEvent(ManyToManyAssociationEvent event) {
		if (event.getClazzOrInterface().isInterface()) {
			return true;
		}
		MetaProperty metaProperty = event.getMetaProperty();

		if (isEnumerationClazz(event.getProperty().getField().getType().getClazz())) {
			throw new RuntimeException("Can't create a ManyToMany between an Enum (MetaProperty: " + metaProperty.getId()
					+ ") and something else");
		}

		if (metaProperty.isOwnerOfAssociation()) {
			return handleManyToManyOwnerOfAssociation(event);
		} else {
			return handleManyToManyOwnedByAssociation(event);
		}

	}

	/**
	 * Does the {@link Clazz}, that the {@link ClazzEvent} represents, extend from our MappedSuperClazz (as returned by
	 * the {@link JpaInheritanceAndClazzesStrategy#resolveMappedSuperClazzType(Clazz)} registered in the {@link JpaGeneratorContext)}
	 * 
	 * @param clazz
	 *          The {@link Clazz} to check
	 * @return true if the {@link Clazz} extends from our MappedSuperClazz, otherwise false
	 * @see {@link JpaInheritanceAndClazzesStrategy#resolveMappedSuperClazzType(Clazz)}
	 */
	protected boolean extendsMappedSuperClass(Clazz clazz) {
		if (clazz == null)
			return false;
		Type extendsType = clazz.getExtendsType();
		return (extendsType != null && extendsType.equals(getContext().getInheritanceStrategy().resolveMappedSuperClazzType(clazz)));
	}

	// ----------------------- JPA sugar handlers -----------------------------------------------------------------------
	/**
	 * Called for {@link Clazz}' which are marked as {@link JpaInheritanceAndClazzesStrategy#EMBEDDED_CLAZZ}.
	 * 
	 * @param event
	 *          The clazz event
	 * @return true if processing should continue, otherwise false
	 */
	protected abstract boolean handleEmbeddableClazzEvent(ClazzEvent event);

	/**
	 * Called for {@link Clazz}' which are not {@link Enumeration}' and NOT marked as {@link JpaInheritanceAndClazzesStrategy#EMBEDDED_CLAZZ}.
	 * 
	 * @param event
	 *          The clazz event
	 * @return true if processing should continue, otherwise false
	 */
	protected abstract boolean handleEntityClazzEvent(ClazzEvent event);

	/**
	 * Handle basic simple property (such as Java builtin types such as String, Float, int, char, Date, BigDecimal, etc.)
	 */
	protected abstract boolean handleBasicProperty(SimplePropertyEvent event);

	/**
	 * Handle a {@link Lob} type basic property
	 * 
	 * @see #isLobCandidate(SimplePropertyEvent) for the guard method which is used to decide if a property is a {@link Lob}
	 */
	protected abstract boolean handleBasicLobProperty(SimplePropertyEvent event);

	/**
	 * Basic property of an {@link Embeddable} type
	 */
	protected abstract boolean handleBasicEmbeddedProperty(SimplePropertyEvent event);

	/**
	 * Handle a collection of {@link Embeddable} types
	 */
	protected abstract boolean handleCollectionOfEmbeddableElementsProperty(SimplePropertyEvent event);

	/**
	 * Handle a collection of basic types (Integer, String, Long, etc.)
	 */
	protected abstract boolean handleCollectionOfBasicTypesProperty(SimplePropertyEvent event);

	/**
	 * Should the basic property be marked as a {@link Lob} ?
	 */
	protected boolean isLobCandidate(SimplePropertyEvent event) {
		if (event.getMetaProperty().hasStereoType(LOB_STEREOTYPE)) {
			return true;
		}

		boolean isLargeString = (event.getProperty().getField().getType().getName().equals("String") || event.getProperty().getField()
				.getType().getName().equals("java.lang.String"))
				&& event.getProperty().getMetaType().getUpperBound() != null
				&& event.getProperty().getMetaType().getUpperBound().longValue() > stringIsLobCandidateUpperBound;
		return event.getProperty().getField().getType().getName().equals("byte[]")
				|| event.getProperty().getField().getType().getName().equals("char[]") || isLargeString
				|| event.getProperty().getField().getType().getName().equals("java.sql.Blob")
				|| event.getProperty().getField().getType().getName().equals("java.sql.Clob");
	}

	/**
	 * Called for {@link OneToOneAssociationEvent} where our property <b>isn't the owner</b> of the association (i.e. it's the owned
	 * property).<br/>
	 * Note: This method will only be called for bidirectional OneToOne associations
	 */
	protected abstract boolean handleOneToOneOwnedByAssociation(OneToOneAssociationEvent event);

	/**
	 * Called for {@link OneToOneAssociationEvent} where our property <b>is the owner</b> of the association
	 */
	protected abstract boolean handleOneToOneOwnerOfAssociation(OneToOneAssociationEvent event);

	/**
	 * Called OneToOne association to an Embeddable entity (its twin method is
	 * {@link #handleBasicEmbeddedProperty(SimplePropertyEvent)} which is called if the association is modeled as a property instead of an
	 * association)
	 * @see #handleBasicEmbeddedProperty(SimplePropertyEvent) for its twin method
	 */
	protected abstract boolean handleOneToOneOfEmbeddableElement(OneToOneAssociationEvent event);

	/**
	 * Called for a OneToOne association to a {@link Enumeration} style {@link Clazz}.
	 */
	protected abstract boolean handleOneToOneOfEnumerationElement(OneToOneAssociationEvent event);

	/**
	 * Called for unidirectional OneToMany associations, where the OneToMany is the owner of the association
	 */
	protected abstract boolean handleOneToManyOwnerOfAssociation(OneToManyAssociationEvent event);

	/**
	 * Called for bidirectional OneToMany associations, where (naturally) the ManyToOne side is the owner (i.e. the OneToMany is the owned
	 * side).
	 */
	protected abstract boolean handleOneToManyOwnedByAssociation(OneToManyAssociationEvent event);

	/**
	 * Called for a OneToMany association to {@link JpaInheritanceAndClazzesStrategy#EMBEDDED_CLAZZ} {@link Clazz}'.<br/>
	 * Its twin sister is {@link #handleCollectionOfBasicTypesProperty(SimplePropertyEvent)}
	 */
	protected abstract boolean handleOneToManyOfEmbeddableElements(OneToManyAssociationEvent event);

	/**
	 * Called for OneToMany associations to a {@link Enumeration} style {@link Clazz}, which should be turned into CollectionOfElements
	 */
	protected abstract boolean handleOneToManyOfEnumeratedElements(OneToManyAssociationEvent event);

	/**
	 * Called for bidirectional and/or Owning ManyToOne Associations
	 */
	protected abstract boolean handleManyToOneOwnerOfAssociation(ManyToOneAssociationEvent event);

	/**
	 * Called for {@link ManyToMany} associations where the opposite side of the association is the owner of the association (i.e. this
	 * property is owned by the association)
	 */
	protected abstract boolean handleManyToManyOwnedByAssociation(ManyToManyAssociationEvent event);

	/**
	 * Called for {@link ManyToMany} associations where the property is the owner of the association
	 */
	protected abstract boolean handleManyToManyOwnerOfAssociation(ManyToManyAssociationEvent event);

	/**
	 * Called for SimpleProperties which are of type {@link Enumeration}
	 */
	protected abstract boolean handleBasicEnumerationProperty(SimplePropertyEvent event);
}