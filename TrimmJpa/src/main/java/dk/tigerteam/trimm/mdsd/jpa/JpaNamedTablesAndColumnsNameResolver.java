/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.codedom.Property;
import dk.tigerteam.trimm.mdsd.java.generator.event.AssociationEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.PropertyEvent;

/**
 * Resolver interface which is used by {@link JpaNamedTablesAndColumnsListener} to resolve what name
 * to use for {@link javax.persistence.Column}, {@link javax.persistence.JoinColumn},
 * {@link javax.persistence.JoinTable}, {@link javax.persistence.Table}, etc. naming.
 *
 * This can be useful if the default names (which follows the JPA default naming standard) collide with
 * database reserved words or if you use a "modern" database like Oracle which still limits names to 30 character ;)
 *
 * @author Jeppe Cramon
 * @see JpaNamedTablesAndColumnsListener
 */
public interface JpaNamedTablesAndColumnsNameResolver {
	
	String resolveJoinColumnName(PropertyEvent event, String defaultJoinColumnName);
	String resolveColumnName(PropertyEvent event, String defaultColumnName);
	String resolveTableName(ClazzEvent event, String defaultTableName);
	String resolveJoinTableName(PropertyEvent event, String defaultJoinTableName);
	String resolveInverseJoinColumnName(AssociationEvent event, String defaultInverseJoinColumnName);
	String resolveAttributeOverrideColumnName(PropertyEvent event, Property overrideAttributeProperty,
	String overrideAttributeName, String defaultAttributeOverrideColumnName);
	String resolveIndexColumnName(PropertyEvent event, String defaultIndexColumnName);
	
}
