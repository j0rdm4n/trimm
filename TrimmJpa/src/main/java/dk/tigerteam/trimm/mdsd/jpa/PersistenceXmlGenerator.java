/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import java.util.Map;
import java.util.Set;

/**
 * Generates a <code>persistence.xml</code> file based on the generated classes
 * @author Lasse Cramon
 */
public class PersistenceXmlGenerator {

	private static final Log log = LogFactory.getLog(PersistenceXmlGenerator.class);
	public static final Namespace defaultNs = Namespace.getNamespace("http://java.sun.com/xml/ns/persistence");
	public static final Namespace xsiNs = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
	public static final String DEFAULT_SCHEMA_LOCATION_1 = "http://java.sun.com/xml/ns/persistence http://java.sun.com/xml/ns/persistence/persistence_1_0.xsd";
	public static final String DEFAULT_SCHEMA_LOCATION_2 = "http://java.sun.com/xml/ns/persistence http://java.sun.com/xml/ns/persistence/persistence_2_0.xsd";
	public static final String DEFAULT_PERSISTENCE_UNIT_NAME = "persistenceUnitName";
	public static final String DEFAULT_TRANSACTION_TYPE = "RESOURCE_LOCAL";
	private JpaVersion jpaVersion = JpaVersion.Version2;

	/**
	 * Generate persistence.xml
	 * 
	 * @param generatedClasses
	 *          The clazzes to generate for.
	 * @return The JDom document containing the persistence xml contents.
	 */
	public Document generate(String schemaLocation, String persistenceVersion, Set<Class<?>> generatedClasses, String jtaDataSource,
			String nonJtaDataSource, String[] ormXmlFiles, Class<?> persistenceProvider, String persistenceUnitName, String transactionType,
			Map<String, String> properties) {
		log.debug("Generating persistence.xml document");
		Document doc = new Document();
		Element persistenceElement = new Element("persistence", defaultNs);
		persistenceElement.setAttribute("schemaLocation", schemaLocation, xsiNs);
		persistenceElement.setAttribute("version", persistenceVersion);
		doc.addContent(persistenceElement);

		Element persistenceUnitElement = new Element("persistence-unit", defaultNs);
		persistenceUnitElement.setAttribute("name", persistenceUnitName);
		persistenceUnitElement.setAttribute("transaction-type", transactionType);
		Element providerElement = new Element("provider", defaultNs);
		providerElement.addContent(persistenceProvider.getName());
		persistenceUnitElement.addContent(providerElement);
		if (jtaDataSource != null) {
			Element jtaDataSourceElement = new Element("jta-data-source", defaultNs);
			jtaDataSourceElement.addContent(jtaDataSource);
			persistenceElement.addContent(jtaDataSourceElement);
		}
		if (nonJtaDataSource != null) {
			Element nonJtaDataSourceElement = new Element("non-jta-data-source", defaultNs);
			nonJtaDataSourceElement.addContent(nonJtaDataSource);
			persistenceUnitElement.addContent(nonJtaDataSourceElement);
		}
		if (generatedClasses != null) {
			for (Class<?> clazz : generatedClasses) {
				Element clazzElement = new Element("class", defaultNs);
				clazzElement.addContent(clazz.getName());
				persistenceUnitElement.addContent(clazzElement);
			}
		}
		if (ormXmlFiles != null) {
			for (int i = 0; i < ormXmlFiles.length; i++) {
				Element mappingFileElement = new Element("mapping-file", defaultNs);
				mappingFileElement.addContent(ormXmlFiles[i]);
				persistenceUnitElement.addContent(mappingFileElement);
			}
		}
		if (properties != null) {
			Element propertiesElement = new Element("properties", defaultNs);
			Set<String> keys = properties.keySet();
			for (String key : keys) {
				String value = properties.get(key);
				if (value != null) {
					Element property = new Element("property", defaultNs);
					property.setAttribute("name", key);
					property.setAttribute("value", value);
					propertiesElement.addContent(property);
				}
			}
			persistenceUnitElement.addContent(propertiesElement);
		}

		persistenceElement.addContent(persistenceUnitElement);
		log.debug("Done generating persistence.xml document");
		return doc;
	}

	public Document generate(String[] ormXmlFiles, Class<?> persistenceProvider, String persistenceUnitName, String transactionType,
			Map<String, String> properties) {
		return generate(DEFAULT_SCHEMA_LOCATION_2, jpaVersion.getName(), null, null, null, ormXmlFiles, persistenceProvider, persistenceUnitName,
				transactionType, properties);
	}

	public Document generate(String[] ormXmlFiles, Class<?> persistenceProvider, Map<String, String> properties) {
		return generate(DEFAULT_SCHEMA_LOCATION_2, jpaVersion.getName(), null, null, null, ormXmlFiles, persistenceProvider,
				DEFAULT_PERSISTENCE_UNIT_NAME, DEFAULT_TRANSACTION_TYPE, properties);
	}

	public Document generate(String[] ormXmlFiles, Class<?> persistenceProvider) {
		return generate(DEFAULT_SCHEMA_LOCATION_2, jpaVersion.getName(), null, null, null, ormXmlFiles, persistenceProvider,
				DEFAULT_PERSISTENCE_UNIT_NAME, DEFAULT_TRANSACTION_TYPE, null);
	}
}
