/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeModel.Filter;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import dk.tigerteam.trimm.util.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * General purpose JPA field based annotation handler.
 * 
 * @see DefaultJpaInheritanceAndClazzesStrategy#EMBEDDED_CLAZZ class constant for information on how to make classes embeddable
 * @see #CASCADE_OPTION for information about overriding default cascading values
 * @see #getCascadeAnnotationAttributes(PropertyEvent) for information about overriding default cascading values
 * @see #parseCascadeOptionValue(String, PropertyEvent) for information about overriding default cascading values
 * @author Jeppe Cramon
 */
public class JpaGeneratorEventListener extends AbstractJpaGeneratorEventListener {
	
	private static final Log log = LogFactory.getLog(JpaGeneratorEventListener.class);

  /**
   * Stereotype value that can be applied to a Simple property to specify the property is the JPA {@link Id} property.
   */
  public static String ID_STEREOTYPE = "ID";
  /**
   * Stereotype value that can be applied to associations and/or Association Roles/Ends to specify the this association should be loaded
   * Eagerly.
   */
  public static String EAGER_LOADING_STEREOTYPE = "EAGER";
  /**
   * Stereotype value that can be applied to Associations and/or Association Roles/Ends to specify the this association should be loaded
   * Lazily.
   */
  public static String LAZY_LOADING_STEREOTYPE = "LAZY";

  /**
   * Cascade option tagged value name, which can be used on association to specify different cascading values than the default (which is
   * CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH). Note: The Value must contain fully qualified class names (FQCN's)
   *
   * @see #getCascadeAnnotationAttributes(PropertyEvent)
   * @see #parseCascadeOptionValue(String, PropertyEvent)
   */
  public static String CASCADE_OPTION = "cascade";

  /**
   * Tagged value that can be applied to One-To-Many Associations that already have {@link JpaInheritanceAndClazzesStrategy#EMBEDDED_CLAZZ} and
   * {@link dk.tigerteam.trimm.mdsd.java.generator.JavaCollectionTypeResolver#ORDERED_STEREOTYPE_NAME} stereotypes applied. Will transform
   * the association to have an {@link OrderBy} annotation (with the value of the Orderby tagged value as parameter)
   */
	public static final String ORDERBY_TAGNAME = "orderby";
	
	/**
	 * Should {@link ElementCollection} be set to {@link FetchType#EAGER} - default is true
	 */
	private boolean elementCollectionFetchTypeEager = true;

	/**
	 * Should all associations (OneToOne, OneToMany, ManyToOne and ManyToMany) default to LAZY fetching (can be overridden using StereoTypes
	 * {@link #LAZY_LOADING_STEREOTYPE} and {@link #EAGER_LOADING_STEREOTYPE}) -> Default is false
	 */
	private boolean defaultToLazyFetchingForAllAssociations = false;
	/**
	 * Should we generate a boolean "present" field (and getter method) in Embeddable classes?<br/>
	 * If we do this, then we can save an entity with an embeddable property and get the property back, even though the embeddable instance
	 * didn't have any properties filled (if we don't put in present, then Hibernate et.all. will set the embeddable property to null when
	 * fetching the object from db again)
	 */
	private boolean shouldGeneratePresentFieldInEmbeddables;

	/**
	 * Should the generator try to make base classes (according to the Base/Extension Clazz pattern) {@link MappedSuperclass} in case their
	 * only subclass is the Extension Clazz (default is true)<br/>
	 * This has the advantage of not polluting the Entity table with a discriminator column.
	 */
	private boolean shouldMakeBaseClazzesMappedSuperClassesIfPossible = true;


	private boolean removeSerialUnusedWarnings = false;

	/**
	 * General purpose JPA field based annotation handler.
	 */
	public JpaGeneratorEventListener() {
		super();
	}

	/**
	 * General purpose JPA field based annotation handler.
	 * 
	 * @param defaultToLazyFetchingForAllAssociations
	 *          Should all associations (OneToOne, OneToMany, ManyToOne and ManyToMany) default to LAZY fetching (can be overridden using
	 *          StereoTypes {@link #LAZY_LOADING_STEREOTYPE} and {@link #EAGER_LOADING_STEREOTYPE}) -> Default is false
	 */
	public JpaGeneratorEventListener(boolean defaultToLazyFetchingForAllAssociations) {
		super();
		this.defaultToLazyFetchingForAllAssociations = defaultToLazyFetchingForAllAssociations;
	}
	
	/**
	 * Should {@link ElementCollection} be set to {@link FetchType#EAGER} - default is true
	 */
	public boolean isElementCollectionFetchTypeEager() {
		return elementCollectionFetchTypeEager;
	}

	/**
	 * Should {@link ElementCollection} be set to {@link FetchType#EAGER} - default is true
	 */
	public JpaGeneratorEventListener setElementCollectionFetchTypeEager(boolean elementCollectionFetchTypeEager) {
		this.elementCollectionFetchTypeEager = elementCollectionFetchTypeEager;
		return this;
	}

	/**
	 * Should the generator try to make base classes (according to the Base/Extension Clazz pattern) {@link MappedSuperclass} in case their
	 * only subclass is the Extension Clazz (default is true)<br/>
	 * This has the advantage of not polluting the Entity table with a discriminator column.
	 */
	public boolean isShouldMakeBaseClazzesMappedSuperClassesIfPossible() {
		return shouldMakeBaseClazzesMappedSuperClassesIfPossible;
	}

	/**
	 * Should the generator try to make base classes (according to the Base/Extension Clazz pattern) {@link MappedSuperclass} in case their
	 * only subclass is the Extension Clazz (default is true)<br/>
	 * This has the advantage of not polluting the Entity table with a discriminator column.
	 */
	public JpaGeneratorEventListener setShouldMakeBaseClazzesMappedSuperClassesIfPossible(
			boolean shouldMakeBaseClazzesMappedSuperClassesIfPossible) {
		this.shouldMakeBaseClazzesMappedSuperClassesIfPossible = shouldMakeBaseClazzesMappedSuperClassesIfPossible;
		return this;
	}

	/**
	 * Should we generate a boolean "present" field (and getter method) in Embeddable classes?<br/>
	 * If we do this, then we can save an entity with an embeddable property and get the property back, even though the embeddable instance
	 * didn't have any properties filled (if we don't put in present, then Hibernate et.all. will set the embeddable property to null when
	 * fetching the object from db again)
	 */
	public boolean isShouldGeneratePresentFieldInEmbeddables() {
		return shouldGeneratePresentFieldInEmbeddables;
	}

	/**
	 * Should we generate a boolean "present" field (and getter method) in Embeddable classes?<br/>
	 * If we do this, then we can save an entity with an embeddable property and get the property back, even though the embeddable instance
	 * didn't have any properties filled (if we don't put in present, then Hibernate et.all. will set the embeddable property to null when
	 * fetching the object from db again)
	 */
	public JpaGeneratorEventListener setShouldGeneratePresentFieldInEmbeddables(boolean shouldGeneratePresentFieldInEmbeddables) {
		this.shouldGeneratePresentFieldInEmbeddables = shouldGeneratePresentFieldInEmbeddables;
		return this;
	}

	/**
	 * Should all associations (OneToOne, OneToMany, ManyToOne and ManyToMany) default to LAZY fetching (can be overridden using StereoTypes
	 * {@link #LAZY_LOADING_STEREOTYPE} and {@link #EAGER_LOADING_STEREOTYPE}) -> Default is false
	 */
	public boolean isDefaultToLazyFetchingForAllAssociations() {
		return defaultToLazyFetchingForAllAssociations;
	}

	/**
	 * Should all associations (OneToOne, OneToMany, ManyToOne and ManyToMany) default to LAZY fetching (can be overridden using StereoTypes
	 * {@link #LAZY_LOADING_STEREOTYPE} and {@link #EAGER_LOADING_STEREOTYPE}) -> Default is false
	 */
	public JpaGeneratorEventListener setDefaultToLazyFetchingForAllAssociations(boolean defaultToLazyFetchingForAllAssociations) {
		this.defaultToLazyFetchingForAllAssociations = defaultToLazyFetchingForAllAssociations;
		return this;
	}

	public boolean isRemoveSerialUnusedWarnings() {
		return removeSerialUnusedWarnings;
	}

	public JpaGeneratorEventListener setRemoveSerialUnusedWarnings(boolean removeSerialUnusedWarnings) {
		this.removeSerialUnusedWarnings = removeSerialUnusedWarnings;
		return this;
	}

	/**
	 * Called for {@link Clazz}' which are marked as {@link DefaultJpaInheritanceAndClazzesStrategy#EMBEDDED_CLAZZ}.
	 * 
	 * @param event
	 *          The clazz event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleEmbeddableClazzEvent(ClazzEvent event) {
		if (shouldGeneratePresentFieldInEmbeddables) {
			MetaProperty isPresentProperty = new MetaProperty(UUID.randomUUID().toString(), true);
			isPresentProperty.setType(new MetaClazz(boolean.class)).setDefaultValue("true").setName(event.getClazz().getName() + "_present");
			isPresentProperty.setWriteable(false).setDocumentation(
					"Generator created property which allows better Embeddable property handling in JPA");
			event.getMetaClazz().getChildren().add(isPresentProperty);
		}

		event.getClazz().addAnnotations(new Annotation(Embeddable.class, true));
		event.getClazz().addImplementsTypes(new Type(Serializable.class, true));
		if (event.getClazz().isBaseClazz() && shouldMakeBaseClazzesMappedSuperClassesIfPossible) {
			event.getClazz().addAnnotations(new Annotation(MappedSuperclass.class, true));
		}
		return true;
	}

	/**
	 * Called for {@link Clazz}' which are not {@link Enumeration}' and NOT marked as
	 * {@link DefaultJpaInheritanceAndClazzesStrategy#EMBEDDED_CLAZZ}.
	 * 
	 * @param event
	 *          The clazz event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleEntityClazzEvent(ClazzEvent event) {
        Clazz clazz = event.getClazz();
        clazz.addAnnotations(new Annotation(Entity.class, true));
        CodeModel codeModel = clazz.getCodeModel();
        List<Clazz> subClasses = codeModel.findAllSubClazzesOfClazz(clazz, Filter.ExcludeExtensionClazzes);

		// Handle inheritance
//        boolean hasSubClasses = subClasses.size() > 0;
        JpaInheritanceAndClazzesStrategy inheritanceStrategy = getContext().getInheritanceStrategy();
        if (clazz.getExtendsType() == null) {
        		handleMappedSuperClass(event);

            // Apply inheritance schema if we have subclasses or if we are a base class (adhering to the base/extension class pattern)
            inheritanceStrategy.applyJpaInheritanceStrategy(clazz, subClasses, shouldMakeBaseClazzesMappedSuperClassesIfPossible);

        }

        inheritanceStrategy.applyDiscriminatorValue(clazz, subClasses);
		return true;
	}

	/**
	 * Is called when an Entity doesn't inherit from another entity in the model.<br/>
	 * This allows us to specify a general (or specific) Mapped super class for such entities.
	 * 
	 * @param event
	 *          The ClazzEvent
	 */
	protected void handleMappedSuperClass(ClazzEvent event) {
		Type mappedSuperClazzType = getContext().getInheritanceStrategy().resolveMappedSuperClazzType(event.getClazz());
		if (mappedSuperClazzType != null) {
			event.getClazz().setExtendsType(mappedSuperClazzType);
		}
	}

	/**
	 * Handle basic property.
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleBasicProperty(SimplePropertyEvent event) {
		if (event.getMetaProperty().hasStereoType(ID_STEREOTYPE)) {
			JpaIdStrategy jpaIdStrategy = getContext().getJpaIdStrategy();
			if (jpaIdStrategy == null) {
				throw new IllegalStateException("No JpaIdStrategy found in the JpaGeneratorContext");
			}
			jpaIdStrategy.handleIdProperty(event);
			return true;
		}
		
		Annotation basicAnnotation = new Annotation(Basic.class);
		if (event.getMetaProperty().getMinCardinality() == 0) {
			basicAnnotation.addAnnotationAttribute("optional", true);
		}
		event.getProperty().getField().addAnnotations(basicAnnotation);
		if (event.getProperty().getField().getType().getName().equals(Date.class.getName())
				|| event.getProperty().getField().getType().getName().equalsIgnoreCase("Date")) {
			event.getProperty().getField().setType(new Type(Date.class, true));
			event.getProperty().getField()
						.addAnnotations(new Annotation(Temporal.class, true).addAnnotationAttributes(AnnotationAttribute.defaultValue(getContext().getJpaProvider().getDateFieldTemporalType())));
		} else if (event.getProperty().getField().getType().getName().equals(Timestamp.class.getName())
				|| event.getProperty().getField().getType().getName().equalsIgnoreCase("Timestamp")) {
			event.getProperty().getField().setType(new Type(Date.class, true));
			event.getProperty().getField()
					.addAnnotations(new Annotation(Temporal.class, true).addAnnotationAttributes(AnnotationAttribute.defaultValue(TemporalType.TIMESTAMP)));
		}
		handleCustomTypeMapping(event);
		return true;
	}

	/**
	 * Handle custom Type mapping for types not directly supported by the JPA provider.<br/>
	 * 
	 * @param event
	 */
	protected void handleCustomTypeMapping(SimplePropertyEvent event) {
	}

	@Override
	protected boolean handleBasicEnumerationProperty(SimplePropertyEvent event) {
		return internalHandleBasicOrToOneEnumerationElement(event);
	}

	/**
	 * Handle a {@link Lob} type basic property
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 * @see #isLobCandidate(SimplePropertyEvent) for the guard method which is used to decide if a property is a {@link Lob}
	 */
	protected boolean handleBasicLobProperty(SimplePropertyEvent event) {
		event.getProperty().getField().addAnnotations(new Annotation(Lob.class, true));
		event.getProperty().getField().addAnnotations(new Annotation(Basic.class, true));
		return true;
	}

	/**
	 * Basic property of an {@link Embeddable} type
	 * 
	 * @param event
	 */
	protected boolean handleBasicEmbeddedProperty(SimplePropertyEvent event) {
		Annotation embedded = new Annotation(Embedded.class, true);
		event.getProperty().getField().addAnnotations(embedded);
		if (event.getMetaProperty().getMinCardinality() == 0) {
			handleBasicProperty(event);
		}
		return true;
	}

	/**
	 * Handle a collection of {@link Embeddable} types
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleCollectionOfEmbeddableElementsProperty(SimplePropertyEvent event) {
		event.getProperty().getField().addAnnotations(createCollectionOfElementsAnnotation(event));
        if (getContext().getCollectionTypeResolver().isOrdered(event)) {
            final Annotation annotation;
            final String orderByTaggedValue = event.getMetaProperty().getTaggedValue(ORDERBY_TAGNAME);
            if (orderByTaggedValue != null) {
                annotation = new Annotation(OrderBy.class, true).addAnnotationAttributes(AnnotationAttribute.defaultValue(orderByTaggedValue));
            } else {
                // No OrderBy: Use OrderColumn
                annotation = new Annotation(OrderColumn.class, true).addAnnotationAttribute("name", event.getProperty().getName() + "_index");
            }
            event.getProperty().getField().addAnnotations(annotation);
        }
		return true;
	}

	/**
	 * Handle a collection of basic types (Integer, String, Long, etc.)
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleCollectionOfBasicTypesProperty(SimplePropertyEvent event) {
		event.getProperty().getField().addAnnotations(createCollectionOfElementsAnnotation(event));
		if (getContext().getCollectionTypeResolver().isOrdered(event) || event.getMetaProperty().getType().isArray()) {
            final Annotation annotation;
            final String orderByTaggedValue = event.getMetaProperty().getTaggedValue(ORDERBY_TAGNAME);
            if (orderByTaggedValue != null) {
                annotation = new Annotation(OrderBy.class, true).addAnnotationAttributes(AnnotationAttribute.defaultValue(orderByTaggedValue));
            } else {
                // No OrderBy: Use OrderColumn
                annotation = new Annotation(OrderColumn.class, true).addAnnotationAttribute("name", event.getProperty().getName() + "_index");
            }
            event.getProperty().getField().addAnnotations(annotation);
		}
		return true;
	}

	/**
	 * Called for {@link OneToOneAssociationEvent} where our property <b>isn't the owner</b> of the association (i.e. it's the owned
	 * property).<br/>
	 * Note: This method will only be called for bidirectional OneToOne associations
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleOneToOneOwnedByAssociation(OneToOneAssociationEvent event) {
		// We're not the owner of the association
		Field field = event.getProperty().getField();
		Clazz targetEntityClazz = event.getProperty().getField().getType().getClazz();
		if (targetEntityClazz.isExtensionClazz() && targetEntityClazz.getExtendsType().getClazz().hasAnnotation(Entity.class)) {
			// Gets the base class, which is what we need for targetEntity mappedBy
			targetEntityClazz = targetEntityClazz.getExtendsType().getClazz();
		}
		Annotation oneToOneAnnotation = new Annotation(OneToOne.class, true).addAnnotationAttributes(getCascadeAnnotationAttributes(event))
				.addAnnotationAttributes(new AnnotationAttribute("mappedBy", event.getResolvedOppositeMetaPropertyName()),
						new AnnotationAttribute("targetEntity", new CodeSnippet(targetEntityClazz.asFQCN(), ".class")));
		if (event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired) {
			JpaInheritanceAndClazzesStrategy inheritanceStrategy = getContext().getInheritanceStrategy();
			InheritanceType inheritanceType = inheritanceStrategy.getInheritanceType(event.getClazz());
			if (inheritanceStrategy.isEntitySubClazz(event.getClazz()) && inheritanceType != InheritanceType.JOINED
					&& !inheritanceStrategy.isRootEntityClazz(event.getClazz())) {
				log.info("OneToOne participating property " + event.getProperty() + " (in Clazz " + event.getClazz()
						+ ") owned by the association of cardinality SingleRequired"
						+ " is in a subclass in a inheritance hierarchy with inheritanceType " + inheritanceType
						+ ", which is unsupported by JPA. Can't set optional='false'");
			} else if (shouldGenerateRequiredConstraintForProperty(event.getProperty())) {
				log.trace("Added opional='false' to OneToOne participating Required property " + event.getProperty() + " (in Clazz "
						+ event.getClazz() + ")");
				oneToOneAnnotation.addAnnotationAttribute("optional", false);
			}
		}
		field.addAnnotations(oneToOneAnnotation);
		return true;
	}

	/**
	 * Called for {@link OneToOneAssociationEvent} where our property <b>is the owner</b> of the association
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleOneToOneOwnerOfAssociation(OneToOneAssociationEvent event) {
		Field field = event.getProperty().getField();
		Annotation oneToOneAnnotation = new Annotation(OneToOne.class, true).addAnnotationAttributes(getCascadeAnnotationAttributes(event));
		applyFetchType(oneToOneAnnotation, event);
		if (event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired) {
			JpaInheritanceAndClazzesStrategy inheritanceStrategy = getContext().getInheritanceStrategy();
			InheritanceType inheritanceType = inheritanceStrategy.getInheritanceType(event.getClazz());
			if (inheritanceStrategy.isEntitySubClazz(event.getClazz()) && inheritanceType != InheritanceType.JOINED
					&& !inheritanceStrategy.isRootEntityClazz(event.getClazz())) {
				log.info("OneToOne participating property " + event.getProperty() + " (in Clazz " + event.getClazz()
						+ ") owner of the association of cardinality SingleRequired"
						+ " is in a subclass in a inheritance hierarchy with inheritanceType " + inheritanceType
						+ ", which is unsupported by JPA. Can't set optional='false'");
			} else if (shouldGenerateRequiredConstraintForProperty(event.getProperty())) {
				log.trace("Added optional='false' to OneToOne participating Required property " + event.getProperty() + " (in Clazz "
						+ event.getClazz() + ")");
				oneToOneAnnotation.addAnnotationAttribute("optional", false);
			}
		}
		field.addAnnotations(oneToOneAnnotation);
		return true;
	}

	/**
	 * Called OneToOne association to an {@link Embeddable} entity (its twin method is
	 * {@link #handleBasicEmbeddedProperty(SimplePropertyEvent)} which is called if the association is modeled as a property instead of an
	 * association)
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 * @see #handleBasicEmbeddedProperty(SimplePropertyEvent) for its twin method
	 */
	protected boolean handleOneToOneOfEmbeddableElement(OneToOneAssociationEvent event) {
		event.getProperty().getField().addAnnotations(new Annotation(Embedded.class));
		return true;
	}

	/**
	 * Called for a OneToOne association to a {@link Enumeration} style {@link Clazz}.
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleOneToOneOfEnumerationElement(OneToOneAssociationEvent event) {
		return internalHandleBasicOrToOneEnumerationElement(event);
	}

	protected boolean internalHandleBasicOrToOneEnumerationElement(PropertyEvent event) {
		Field field = event.getProperty().getField();
		field.addAnnotations(new Annotation(Enumerated.class, true).addAnnotationAttributes(AnnotationAttribute.defaultValue(EnumType.STRING)));
		return true;
	}

	/**
	 * Called for unidirectional OneToMany associations, where the OneToMany is the owner of the association
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleOneToManyOwnerOfAssociation(OneToManyAssociationEvent event) {
		Field field = event.getProperty().getField();
		Annotation oneToManyAnnotation = new Annotation(OneToMany.class, true).addAnnotationAttributes(getCascadeAnnotationAttributes(event));
		field.addAnnotations(oneToManyAnnotation);
		applyFetchType(oneToManyAnnotation, event);
		if (getContext().getCollectionTypeResolver().isOrdered(event)) {
            final Annotation annotation;
            final String orderByTaggedValue = firstNonNullOrNull(
                    event.getMetaProperty().getTaggedValue(ORDERBY_TAGNAME),
                    event.getMetaProperty().getAssociation().getTaggedValue(ORDERBY_TAGNAME));
            if (orderByTaggedValue != null) {
                annotation = new Annotation(OrderBy.class, true).addAnnotationAttributes(AnnotationAttribute.defaultValue(orderByTaggedValue));
            } else {
                // No OrderBy: Use OrderColumn
                annotation = new Annotation(OrderColumn.class, true).addAnnotationAttribute("name", event.getProperty().getName() + "_index");
            }
            event.getProperty().getField().addAnnotations(annotation);
		}
		return true;
	}

	/**
	 * Called for bidirectional OneToMany associations, where (naturally) the ManyToOne side is the owner (i.e. the OneToMany is the owned
	 * side).<br/>
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleOneToManyOwnedByAssociation(OneToManyAssociationEvent event) {
		Field field = event.getProperty().getField();
		Clazz targetEntityClazz = event.getProperty().getField().getType().getGenerics().get(0).getClazz();
		if (targetEntityClazz.isExtensionClazz() && targetEntityClazz.getExtendsType().getClazz().hasAnnotation(Entity.class)) {
			// Gets the base class, which is what we need for targetEntity mappedBy
			targetEntityClazz = targetEntityClazz.getExtendsType().getClazz();
		}

		Annotation oneToManyAnnotation = new Annotation(OneToMany.class, true).addAnnotationAttributes(getCascadeAnnotationAttributes(event))
				.addAnnotationAttributes(new AnnotationAttribute("targetEntity", new CodeSnippet(targetEntityClazz.asFQCN(), ".class")));

		// Handle Ordered bidirectionally OneToMany associations (see JPA With Hibernate page 294), where the OneToMany effectively becomes the
		// owner of the association
		if (getContext().getCollectionTypeResolver().isOrdered(event)) {
            final Annotation annotation;
            final String orderByTaggedValue = firstNonNullOrNull(
                    event.getMetaProperty().getTaggedValue(ORDERBY_TAGNAME),
                    event.getMetaProperty().getAssociation().getTaggedValue(ORDERBY_TAGNAME));
            if (orderByTaggedValue != null) {
                annotation = new Annotation(OrderBy.class, true).addAnnotationAttributes(AnnotationAttribute.defaultValue(orderByTaggedValue));
            } else {
                // No OrderBy: Use OrderColumn
                annotation = new Annotation(OrderColumn.class, true).addAnnotationAttribute("name", event.getProperty().getName() + "_index");
            }
            event.getProperty().getField().addAnnotations(annotation);
			Annotation joinColumn = new Annotation(JoinColumn.class, true).addAnnotationAttributes(new AnnotationAttribute("nullable", false));
			event.getProperty().getField().addAnnotations(joinColumn);
		} else {
			// Since we're owned by the ManyToOne we need to add a "mappedBy" attribute
			oneToManyAnnotation.addAnnotationAttributes(new AnnotationAttribute("mappedBy", Utils.uncapitalize(event
					.getResolvedOppositeMetaPropertyName())));
		}

		applyFetchType(oneToManyAnnotation, event);
		field.addAnnotations(oneToManyAnnotation);
		return true;
	}

	/**
	 * Called for a OneToMany association to {@link DefaultJpaInheritanceAndClazzesStrategy#EMBEDDED_CLAZZ} {@link Clazz}'.<br/>
	 * Its twin sister is {@link #handleCollectionOfBasicTypesProperty(SimplePropertyEvent)}
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleOneToManyOfEmbeddableElements(OneToManyAssociationEvent event) {
		Field field = event.getProperty().getField();
		field.addAnnotations(createCollectionOfElementsAnnotation(event));
        if (getContext().getCollectionTypeResolver().isOrdered(event)) {
            final Annotation annotation;
            final String orderByTaggedValue = firstNonNullOrNull(
                    event.getMetaProperty().getTaggedValue(ORDERBY_TAGNAME),
                    event.getMetaProperty().getAssociation().getTaggedValue(ORDERBY_TAGNAME));
            if (orderByTaggedValue != null) {
                annotation = new Annotation(OrderBy.class, true).addAnnotationAttributes(AnnotationAttribute.defaultValue(orderByTaggedValue));
            } else {
                // No OrderBy: Use OrderColumn
                annotation = new Annotation(OrderColumn.class, true).addAnnotationAttribute("name", event.getProperty().getName() + "_index");
            }
            event.getProperty().getField().addAnnotations(annotation);
        }
		return true;
	}

	/**
	 * Called for OneToMany associations to a {@link Enumeration} style {@link Clazz}, which should be turned into CollectionOfElements
	 * 
	 * @param event
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleOneToManyOfEnumeratedElements(OneToManyAssociationEvent event) {
		Field field = event.getProperty().getField();
		field.addAnnotations(new Annotation(Enumerated.class, true).addAnnotationAttributes(AnnotationAttribute.defaultValue(EnumType.STRING)),
				createCollectionOfElementsAnnotation(event));
		return true;
	}

	/**
	 * Handle toMany associations/properties for elements of type {@link Enum}, java built in types and {@link Embeddable} classes
     * <pre>
     * 	        {@literal @ElementCollection}
     * 			private Collection&lt;String> hobbies = new HashSet&lt;String>();
     *
     * 			The table name will be 'customer_hobbies'
     * 			The table will have two columns: 'customer_id' - the id of the owning customer and 'hobbies' with the value. A table row will be created for each element in the collection
     *
     * 			 {@literal @ElementCollection}
     * 			 {@literal @Column(name="HOBBY_NAME")}
     * 			 {@literal @CollectionTable}(name="HOBBIES", joinColumns={@literal @JoinColumn(name="CUSTID"))}
     * 			 private Collection&lt;String> hobbies = new HashSet&lt;String>();
     * </pre>
     *
	 * @param event
	 *          The event containing the property information. This will either be a {@link OneToManyAssociationEvent} or a
	 *          {@link SimplePropertyEvent} with {@link CardinalityType#Many}
	 * @return true if processing should continue, otherwise false The annotation to use. Default is {@link ElementCollection} since Hibernate
	 *         is considered the default JPA implementation
	 */
	// TODO: handle column or @CollectionTable
	protected Annotation createCollectionOfElementsAnnotation(PropertyEvent event) {
		Annotation annotation = new Annotation(ElementCollection.class);
		if (elementCollectionFetchTypeEager) {
			annotation.addAnnotationAttribute("fetch", FetchType.EAGER);
		}

		return annotation;
	}

	/**
	 * Called for bidirectional and/or unidirectionally Owning ManyToOne Associations<br/>
	 * 
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleManyToOneOwnerOfAssociation(ManyToOneAssociationEvent event) {
		Field field = event.getProperty().getField();
		Annotation manyToOneAnnotation = new Annotation(ManyToOne.class, true).addAnnotationAttributes(getCascadeAnnotationAttributes(event));
		applyFetchType(manyToOneAnnotation, event);
		if (event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired) {
			JpaInheritanceAndClazzesStrategy inheritanceStrategy = getContext().getInheritanceStrategy();
			InheritanceType inheritanceType = inheritanceStrategy.getInheritanceType(event.getClazz());
			if (inheritanceStrategy.isEntitySubClazz(event.getClazz()) && inheritanceType != InheritanceType.JOINED
					&& !inheritanceStrategy.isRootEntityClazz(event.getClazz())) {
				log.info("ManyToOne participating property " + event.getProperty() + " (in Clazz " + event.getClazz()
						+ ") owner of the association of cardinality SingleRequired"
						+ " is in a subclass in a inheritance hierarchy with inheritanceType " + inheritanceType
						+ ", which is unsupported by JPA. Can't set optional='false'");
			} else if (shouldGenerateRequiredConstraintForProperty(event.getProperty())) {
				log.trace("Added optional='false' to ManyToOne participating Required property " + event.getProperty() + " (in Clazz "
						+ event.getClazz() + ")");
				manyToOneAnnotation.addAnnotationAttribute("optional", false);
			}
		}
		field.addAnnotations(manyToOneAnnotation);

		// Handle Ordered bidirectionally OneToMany associations (see JPA With Hibernate page 294), where the OneToMany effectively becomes the
		// owner of the association
		MetaProperty oppositeMetaProperty = event.getMetaProperty().getOppositePropertyInAssociation();
		if (event.getMetaProperty().getAssociation().isBidirectional()
				&& getContext().getCollectionTypeResolver().isOrdered(oppositeMetaProperty)) {
			// @ManyToOne
			// @JoinColumn(name = "ITEM_ID", nullable = false,
			// updatable = false, insertable = false)
			Annotation joinColumn = new Annotation(JoinColumn.class, true).addAnnotationAttributes(new AnnotationAttribute("nullable", false),
					new AnnotationAttribute("updatable", false), new AnnotationAttribute("insertable", false));
			field.addAnnotations(joinColumn);
		}

		return true;
	}

	/**
	 * This method will call #getFetchType(AssociationEvent) for the given event.<b/> If this method returns a not null {@link FetchType}
	 * value, then it will be applied to the associationAnnotation.<br/>
	 * If null is returned then nothing happens, since we've decided to use JPA default as defaults (implicitly).
	 * 
	 * @param associationAnnotation
	 *          The Association Annotation ({@link OneToOne}, {@link OneToMany}, {@link ManyToOne}, {@link ManyToMany})
	 * @param event
	 *          The association event
	 * @see #getFetchType(AssociationEvent)
	 */
	protected void applyFetchType(Annotation associationAnnotation, AssociationEvent event) {
		FetchType fetchType = getFetchType(event);
		if (fetchType != null) {
			associationAnnotation.addAnnotationAttribute("fetch", fetchType);
		}
	}

	/**
	 * Is called by {@link #applyFetchType(Annotation, AssociationEvent)} to decide what {@link FetchType} an association should use.<br/>
	 * Default behaviour is to look for {@link #EAGER_LOADING_STEREOTYPE} and {@link #LAZY_LOADING_STEREOTYPE} on the association ends/roles
	 * or association and then look at {@link #defaultToLazyFetchingForAllAssociations} and in case it's set to true then return LAZY.
	 * 
	 * @param event
	 *          The association event
	 * @return The {@link FetchType} to use, or <code>null</code> to use the default JPA value (e.g. don't apply any fetching attribute)
	 */
	protected FetchType getFetchType(AssociationEvent event) {
		if (event.getMetaProperty().hasStereoType(LAZY_LOADING_STEREOTYPE)
				|| event.getMetaProperty().getAssociation().hasStereoType(LAZY_LOADING_STEREOTYPE)) {
			return FetchType.LAZY;
		} else if (event.getMetaProperty().hasStereoType(EAGER_LOADING_STEREOTYPE)
				|| event.getMetaProperty().getAssociation().hasStereoType(EAGER_LOADING_STEREOTYPE)) {
			return FetchType.EAGER;
		}
		if (defaultToLazyFetchingForAllAssociations) {
			return FetchType.LAZY;
		}
		return null;
	}

	/**
	 * Called for {@link ManyToMany} associations where the opposite side of the association is the owner of the association (i.e. this
	 * property is owned by the association)
	 * 
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleManyToManyOwnedByAssociation(ManyToManyAssociationEvent event) {
		// We're not the owner of the association
		Field field = event.getProperty().getField();
		Clazz targetEntityClazz = event.getProperty().getField().getType().getGenerics().get(0).getClazz();
		if (targetEntityClazz.isExtensionClazz() && targetEntityClazz.getExtendsType().getClazz().hasAnnotation(Entity.class)) {
			// Gets the base class, which is what we need for targetEntity mappedBy
			targetEntityClazz = targetEntityClazz.getExtendsType().getClazz();
		}

		Annotation manyToManyAnnotation = new Annotation(ManyToMany.class, true).addAnnotationAttributes(getCascadeAnnotationAttributes(event))
				.addAnnotationAttributes(new AnnotationAttribute("mappedBy", event.getResolvedOppositeMetaPropertyName()),
						new AnnotationAttribute("targetEntity", new CodeSnippet(targetEntityClazz.asFQCN(), ".class")));
		applyFetchType(manyToManyAnnotation, event);
		field.addAnnotations(manyToManyAnnotation);
		return true;
	}

	/**
	 * Called for {@link ManyToMany} associations where the property is the owner of the association
	 * 
	 * @return true if processing should continue, otherwise false
	 */
	protected boolean handleManyToManyOwnerOfAssociation(ManyToManyAssociationEvent event) {
		Field field = event.getProperty().getField();
		Annotation manyToManyAnnotation = new Annotation(ManyToMany.class, true).addAnnotationAttributes(getCascadeAnnotationAttributes(event));
		applyFetchType(manyToManyAnnotation, event);
		field.addAnnotations(manyToManyAnnotation);
		return true;
	}

	/**
	 * Get the cascade annotation attributes for this association. Default behavior. If the modeler has specified a Tagged value by name
	 * {@link #CASCADE_OPTION} on the association, then the parsing is left up to
	 * {@link #parseCascadeOptionValue(String, PropertyEvent)}<br/>
	 * TODO: Should be changed to either List&lt;CascadeType> or allow for even greater control (like adding Hibernate specific cascade
	 * options)<br/>
	 * TODO: Support tagged values on the roles instead of only on the association
	 * 
	 * @return A list {@link AnnotationAttribute}'s (with at least one called "cascade" containing the {@link CascadeType} instances)
	 * @see #parseCascadeOptionValue(String, PropertyEvent)
	 */
	protected AnnotationAttribute[] getCascadeAnnotationAttributes(PropertyEvent event) {
		List<AnnotationAttribute> result = new LinkedList<AnnotationAttribute>();

		String cascadeOption = event.getMetaProperty().getAssociation().getTaggedValue(CASCADE_OPTION);

		if (cascadeOption != null && event.getMetaProperty().isOwnerOfAssociation()) {
			// Try the simple solution for now and just select the owner of the association
			result.add(new AnnotationAttribute("cascade").addValues(parseCascadeOptionValue(cascadeOption, event).toArray()));
		} else {
			result.add(new AnnotationAttribute("cascade").addValues(CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH));
		}

		return result.toArray(new AnnotationAttribute[] {});
	}

	/**
	 * Override this method to perform customer parsing of cascade option value.<br/>
	 * Default format is {@link CascadeType#name()} [, {@link CascadeType#name()}]*
	 * <p/>
	 * Example: <br/>
	 * <code>ALL</code><br/>
	 * <code>PERSIST, MERGE, REMOVE</code>
	 * 
	 * @param cascadeOption
	 *          The associations cascade option value
	 * @param event
	 *          The property the caused this method to be called
	 * @return The list containing of {@link CascadeType} instances according to the cascadeOption value
	 */
	protected List<CascadeType> parseCascadeOptionValue(String cascadeOption, PropertyEvent event) {
		List<CascadeType> result = new LinkedList<CascadeType>();
		if (cascadeOption != null) {
			String[] cascadeValues = cascadeOption.split(",");
			for (String cascadeValue : cascadeValues) {
				cascadeValue = cascadeValue.toUpperCase().trim();
				CascadeType cascadeType = CascadeType.valueOf(cascadeValue);
				result.add(cascadeType);
			}
		}
		return result;
	}

	@Override
	protected boolean handleManyToOneOfEmbeddableElement(ManyToOneAssociationEvent event) {
		event.getProperty().getField().addAnnotations(new Annotation(Embedded.class, true));
		return true;
	}

    private static <T> T firstNonNullOrNull(T first, T second) {
        return first != null ? first : second;
    }
}
