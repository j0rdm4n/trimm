/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.jpa.configuration;

/**
 * Configuration for test settings 
 * 
 * @author Jeppe Cramon
 */
public class IntegrationTestSetup {
	
	private String testClassName;
	private String extendsClass;
	private String testPackageName;
	private boolean autoHandleTransactionsAndResources;

	public String getTestClassName() {
		return testClassName;
	}

	public void setTestClassName(String testClassName) {
		this.testClassName = testClassName;
	}

	public String getExtendsClass() {
		return extendsClass;
	}

	public void setExtendsClass(String extendsClass) {
		this.extendsClass = extendsClass;
	}

	public String getTestPackageName() {
		return testPackageName;
	}

	public void setTestPackageName(String testPackageName) {
		this.testPackageName = testPackageName;
	}

	public boolean isAutoHandleTransactionsAndResources() {
		return autoHandleTransactionsAndResources;
	}

	public void setAutoHandleTransactionsAndResources(boolean autoHandleTransactionsAndResources) {
		this.autoHandleTransactionsAndResources = autoHandleTransactionsAndResources;
	}

}