/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeElement.AccessModifier;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.persistence.util.ObjectGraphUtil;
import dk.tigerteam.trimm.util.Utils;
import org.junit.Assert;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.io.StringWriter;
import java.util.List;

/**
 * Create a JUnit test which will test all of the JPA setup (mappings, etc.).
 * This class will create a Unit testing class (using JUnit 4 annotations) where each {@link javax.persistence.Entity}
 * fed to it in the {@link #postProcessClazzes(java.util.List)}  will result in a {@literal @org.junit.Test}
 * <code>testPersist&lt;ClassName>()</code> method for each individual {@link javax.persistence.Entity} class.<br/>
 * Each Test method created will create a fully valid object (graph if needed) that is persisted, then loaded again after
 * which an object (graph) comparison will be performed to validate that persisting the object worked and didn't loose any data.<p/>
 *
 * The test class that gets generated MUST implement the  {@link dk.tigerteam.trimm.persistence.mdsd.test.JpaUnitTest} interface
 * or a subclass thereof (we recommend inheriting from {@link dk.tigerteam.trimm.persistence.mdsd.test.AbstractModelTest})
 *
 * @see dk.tigerteam.trimm.mdsd.java.generator.RuntimeMetaDataCreator
 * @see dk.tigerteam.trimm.persistence.mdsd.test.JpaUnitTest
 * @see dk.tigerteam.trimm.persistence.mdsd.test.AbstractModelTest
 * @author Jeppe Cramon
 */
public class JpaUnittestCreator {

	private static final Log logger = LogFactory.getLog(JpaUnittestCreator.class);
	
	private String testClassName;
	private String superTestClass;
	private String destinationPackageName;
	private boolean handleJPAResourcesAndTransactions = true;

    /**
     *
     * @param testClassName What should be the name of the Test class that we generate
     * @param superTestClass What class MUST it inherit from. This class MUST implement the
     *                       {@link dk.tigerteam.trimm.persistence.mdsd.test.JpaUnitTest} interface
     * @param destinationPackageName What packagename should the Testclass we generate be placed in
     * @param handleJPAResourcesAndTransactions Should the test class generate code to handle JPA resource (
     *                                          EntityManager closing and Transaction start and rollback) itself -
     *                                          or will the class that it inherits from handle this (e.g. in
     *                                          {@link @org.junit.Before} and {@link @org.junit.After} methods)?
     */
	public JpaUnittestCreator(String testClassName, Class<?> superTestClass, String destinationPackageName,
			boolean handleJPAResourcesAndTransactions) {
		this(testClassName, superTestClass.getName(),
                destinationPackageName, handleJPAResourcesAndTransactions);
	}

	public JpaUnittestCreator(String testClassName, String superTestClass, String destinationPackageName,
			boolean handleJPAResourcesAndTransactions) {
		super();
        if (superTestClass == null) throw new IllegalArgumentException("You MUST specify a superTestClass");
		this.testClassName = testClassName;
		this.superTestClass = superTestClass;
		this.destinationPackageName = destinationPackageName;
		this.handleJPAResourcesAndTransactions = handleJPAResourcesAndTransactions;
	}

	public List<ClazzOrInterface> postProcessClazzes(List<ClazzOrInterface> allClazzes) {
		CodePackage destinationPackage = new CodePackage(destinationPackageName);
		JavaGeneratorContext.getContext().getCodeModel().addChildren(destinationPackage);
		Clazz unitTestClazz = generateUnitTestForClazz(allClazzes);
		unitTestClazz.setPackage(destinationPackage);
		allClazzes.add(unitTestClazz);
		return allClazzes;
	}

	protected Clazz generateUnitTestForClazz(List<ClazzOrInterface> allClazzes) {
		Clazz unittest = new Clazz(testClassName, true);
		unittest.setTestClazz(true);
	    unittest.setExtendsType(new Type(superTestClass));
		unittest.addAccessModifiers(AccessModifier.Public);
		logger.debug("Generating unittestcase class '" + unittest.getFQCN() + "'");

		JpaInheritanceAndClazzesStrategy inheritanceStrategy = JavaGeneratorContext.getContext(JpaGeneratorContext.class)
				.getInheritanceStrategy();
		for (ClazzOrInterface clazzOrInterface : allClazzes) {
			if (!clazzOrInterface.isInterface()) {
				Clazz clazz = (Clazz) clazzOrInterface;
				if (inheritanceStrategy.isEntityClazz(clazz) && !clazz.getAccessModifiers().contains(AccessModifier.Abstract)
						&& (clazz.isExtensionClazz() || !clazz.isBaseClazz()) && shouldGenerateForClazz(clazz)) {
					Method testMethod = new Method("testPersist" + clazz.getName());
					testMethod.addAccessModifiers(AccessModifier.Public);
					testMethod.addAnnotations(new Annotation(Test.class));
					logger.debug("Generating unittest test method '" + testMethod.getName() + "' for class '" + clazz.getName());
					unittest.addChildren(testMethod);
					CodeSnippet code = new CodeSnippet();
					testMethod.addChildren(code);
					createTestMethodBody(clazz, code);
				}
			}
		}
		return unittest;
	}

	/**
	 * Guard method for turning off Unit test method generation for individual {@link Clazz}'s the might not fit the rime scheme
	 */
	protected boolean shouldGenerateForClazz(Clazz clazz) {
		return true;
	}

	protected void createTestMethodBody(Clazz clazz, CodeSnippet methodBody) {
		methodBody.printAllLn(new Type(EntityManager.class, true), " entityManager = getEntityManager();");

		String var1 = Utils.uncapitalize(clazz.getName() + "1");
		String var2 = Utils.uncapitalize(clazz.getName() + "2");
		methodBody.printAllLn(new Type(clazz), " ", var1, " = null;");
		methodBody.printAllLn(new Type(clazz), " ", var2, " = null;");

		methodBody.println("try {");
		if (handleJPAResourcesAndTransactions) {
			methodBody.println("entityManager.getTransaction().begin();");
		}

				methodBody.printAllLn(var1, " = getDataCreator().createTestData(", clazz.asFQCN().withOverrideImports(true), ".class," +
						" getTestDataSpecification(", clazz.asFQCN().withOverrideImports(true),
				".class));");
		methodBody.printAllLn("entityManager.persist(", var1, ");");
		methodBody.printAllLn("try {");
		methodBody.printAllLn("entityManager.flush();");
		methodBody.printAllLn("} catch (Exception e) {");
		methodBody.printAllLn(new Type(StringWriter.class, true), " writer = new ", new Type(StringWriter.class, true), "();");
		methodBody.printAllLn("writer.write(e.getMessage() + \"\\r\\n\");");
		methodBody.printAllLn("writer.write(\"", var1, ": \\r\\n\");");
		methodBody.printAllLn(new Type(ObjectGraphUtil.class), ".printGraphRecursively(", var1, ", writer, getObjectPrinters());");
		methodBody.printAllLn("throw new RuntimeException(\"Graph test failed for \" + ", new Type(clazz),
				".class + \"\\r\\n\" + writer.toString(), e);");
		methodBody.println("}");
		methodBody.printAllLn("entityManager.clear();");

		methodBody.printAllLn(var2, " = entityManager.find(", new Type(clazz), ".class, ", var1, ".getId());");
		methodBody.printAllLn("if (!(", new Type(ObjectGraphUtil.class), ".areTheGraphsEqual(", var1, ", ", var2, ", getProxyHandler(entityManager),");
    //methodBody.printAllLn(ReflectUtils.class, ".getField(", getRootMappedSuperClassName(), ".class, \"created\"),");
    //methodBody.printAllLn(ReflectUtils.class, ".getField(", getRootMappedSuperClassName(), ".class, \"lastUpdated\")))) {");
    methodBody.printAllLn("getIgnoreFieldsDuringGraphCompare(", new Type(clazz),".class)))) {");
    methodBody.printAllLn(new Type(StringWriter.class, true), " writer = new ", new Type(StringWriter.class, true), "();");
		methodBody.printAllLn("writer.write(\"", var1, ": \\r\\n\");");
		methodBody.printAllLn(new Type(ObjectGraphUtil.class), ".printGraphRecursively(", var1, ", writer, getObjectPrinters());");
		methodBody.printAllLn("writer.write(\"", var2, ": \\r\\n\");");
		methodBody.printAllLn(new Type(ObjectGraphUtil.class), ".printGraphRecursively(", var2, ", writer, getObjectPrinters());");
		methodBody.printAllLn(new Type(Assert.class, true), ".fail(\"Graph comparison failed for \" + ", new Type(clazz),
				".class + \"\\r\\n\" + writer.toString());");
		methodBody.printAllLn("}");
		methodBody.printAllLn("} catch (Exception e) {");

		methodBody.printAllLn(new Type(StringWriter.class, true), " writer = new ", new Type(StringWriter.class, true), "();");
		methodBody.printAllLn("writer.write(\"", var1, ": \\r\\n\");");
		methodBody.printAllLn(new Type(ObjectGraphUtil.class), ".printGraphRecursively(", var1, ", writer, getObjectPrinters());");
		methodBody.printAllLn("writer.write(\"", var2, ": \\r\\n\");");
		methodBody.printAllLn(new Type(ObjectGraphUtil.class), ".printGraphRecursively(", var2, ", writer, getObjectPrinters());");
		methodBody.printAllLn("throw new RuntimeException(\"Graph test failed for \" + ", new Type(clazz),
				".class + \"\\r\\n\" + writer.toString(), e);");
		methodBody.println("}");

		if (handleJPAResourcesAndTransactions) {
			methodBody.println("finally {");
			methodBody.println("if (entityManager.getTransaction().isActive()) {");
			methodBody.println("entityManager.getTransaction().rollback();");
			methodBody.println("}");
			methodBody.println("entityManager.close();");
			methodBody.println("}");
		}
	}
}
