/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.hibernate;

import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorContext;
import dk.tigerteam.trimm.mdsd.jpa.JpaInheritanceAndClazzesStrategy;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;

import javax.persistence.InheritanceType;
import javax.validation.constraints.NotNull;

/**
 * Support for Hibernate Validator {@link NotNull}
 * <p/>
 * Supports NotNull (specified using Stereotypes in order to avoid the Hibernate bug specified at
 * http://opensource.atlassian.com/projects/hibernate/browse/HHH-3544) and by specifically applying NotNull to required xToOne properties in
 * subclasses where the {@link InheritanceType} doesn't support making properties required
 * <p/>
 * In order to use this {@link NotNull} feature, you have to make sure that the NotNull's AREN'T transformed to the SQL Schema. This is done
 * by running Hibernate with the following setting: <code>hibernate.validator.apply_to_ddl=false</code>
 * 
 * Please check https://hibernate.onjira.com/browse/HHH-3544 for details
 * 
 * @author Jeppe Cramon
 */
public class HibernateValidatorNotNullListener extends AbstractGeneratorEventListener {
	/**
	 * Stereotype name that corresponds to the Hibernate Validator {@link NotNull} annotation
	 */
	public static final String NOT_NULL_STEREOTYPE = "NotNull";

	@Override
	protected JpaGeneratorContext getContext() {
		return super.getContext(JpaGeneratorContext.class);
	}

	@Override
	protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
		if (event.getMetaProperty().hasStereoType(NOT_NULL_STEREOTYPE)
				|| event.getMetaProperty().getAssociation().hasStereoType(NOT_NULL_STEREOTYPE)) {
			event.getProperty().getField().addAnnotations(new Annotation(NotNull.class));
		} else {
			handleNotNullOnRequiredProperty(event);
		}
		return true;
	}

	/**
	 * Apply the {@link NotNull} annotation to required properties in subclasses which are in a hierarchy which uses an
	 * {@link InheritanceType} which doesn't allow Not Nullable columns
	 * 
	 * @param event the association event
	 */
	protected void handleNotNullOnRequiredProperty(AssociationEvent event) {
		if (event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired) {
			JpaInheritanceAndClazzesStrategy inheritanceStrategy = getContext().getInheritanceStrategy();
			InheritanceType inheritanceType = inheritanceStrategy.getInheritanceType(event.getClazz());
			if (inheritanceStrategy.isEntitySubClazz(event.getClazz()) && inheritanceType != InheritanceType.JOINED
					&& !inheritanceStrategy.isRootEntityClazz(event.getClazz())) {
				event.getProperty().getField().addAnnotations(new Annotation(NotNull.class));
			}
		}
	}

	@Override
	protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
		if (event.getMetaProperty().hasStereoType(NOT_NULL_STEREOTYPE)
				|| event.getMetaProperty().getAssociation().hasStereoType(NOT_NULL_STEREOTYPE)) {
			event.getProperty().getField().addAnnotations(new Annotation(NotNull.class));
		} else {
			handleNotNullOnRequiredProperty(event);
		}
		return true;
	}

	@Override
	protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
		if (event.getMetaProperty().hasStereoType(NOT_NULL_STEREOTYPE)
				|| (event.getMetaProperty().isPartInAnAssociation() && event.getMetaProperty().getAssociation().hasStereoType(NOT_NULL_STEREOTYPE))) {
			event.getProperty().getField().addAnnotations(new Annotation(NotNull.class));
		}
		return true;
	}

}
