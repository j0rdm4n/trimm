/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.configuration;

import com.beust.jcommander.JCommander;
import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.Property;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;
import dk.tigerteam.trimm.mdsd.java.configuration.*;
import dk.tigerteam.trimm.mdsd.java.generator.RuntimeMetaDataCreator;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.CodeWritingStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.FileSystemCodeWritingStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.JavaCodeWriter;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.java.generator.extension.ImportListener;
import dk.tigerteam.trimm.mdsd.jpa.*;
import dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsListener.DefaultJpaNamedTablesAndColumnsNameResolver;
import dk.tigerteam.trimm.mdsd.jpa.hibernate.HibernateProvider;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.util.JDomWriter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom2.Document;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Java JPA style code generator - which read its configuration from a YAML file
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class JpaConfigurationBasedCodeGenerator extends AbstractConfigurationBasedCodeGenerator<JpaConfiguration, JpaGeneratorContext> {
  private static final Log log = LogFactory.getLog(JpaConfigurationBasedCodeGenerator.class);
  // --------------------------------------------------------------------------------------------------------------------------------------------------------

  public static void main(String args[]) {
      System.out.println("*****************************************");
      System.out.println("* TRIMM JPA code generator              *");
      System.out.println("*****************************************");

      PojoConfigurationBasedCodeGenerator.JavaGeneratorCommandLineArguments generatorArgs = new PojoConfigurationBasedCodeGenerator.JavaGeneratorCommandLineArguments();
      JCommander jCommander = new JCommander(generatorArgs, args);
      if (generatorArgs.help) {
          jCommander.usage();
          System.exit(-1);
      }

      if (generatorArgs.baseDir == null || !generatorArgs.baseDir.exists()) {
          System.err.println("Can't find Base directory '" + generatorArgs.baseDir + "'");
          System.exit(-1);
      }
      File outputDir = generatorArgs.baseDir;
      if (generatorArgs.outputDir != null) {
          outputDir = new File(generatorArgs.baseDir, generatorArgs.outputDir)            ;
      }
      File yamlFile = new File(generatorArgs.baseDir, generatorArgs.yamlFile);

      System.out.println("Using YAML configuration file: '" + yamlFile.getAbsolutePath() + "'");
      System.out.println("Using Base dir               : '" + generatorArgs.baseDir.getAbsolutePath() + "'");
      System.out.println("Using Output dir             : '" + outputDir.getAbsolutePath() + "'");
      if (generatorArgs.forceOverwriteGeneratedExtensionClasses) {
          System.out.println("FORCEFULLY overwriting generated Extension classes!!!");
      }
      JpaConfigurationBasedCodeGenerator.generate(yamlFile, generatorArgs.baseDir, outputDir, generatorArgs.forceOverwriteGeneratedExtensionClasses, PojoConfigurationBasedCodeGenerator.class.getClassLoader());
  }


  /**
   * Generate code based on input
   *
   * @param yamlConfigurationFile The file pointing to the YAML configuration file
   * @param baseDir               the base dir that this configuration is run in (used for calculating absolute path's)
   * @param baseOutputDir         The directory to use for output files
   * @param forceOverwriteGeneratedExtensionClasses
   *                              Extra option to allow removing ALL generated extension classes (use with care)
   * @param classLoader           The classloader to use (may be null - in which case the {@link dk.tigerteam.trimm.mdsd.java.configuration.AbstractConfigurationBasedCodeGenerator#getClassLoader()} defaults apply
   *
   * @return The classes and interfaces generated
   */
  public static List<ClazzOrInterface> generate(final File yamlConfigurationFile, final File baseDir, final File baseOutputDir, boolean forceOverwriteGeneratedExtensionClasses, final ClassLoader classLoader) {
    JpaConfiguration jpaConfiguration = null;
    try {
      jpaConfiguration = parseJpaConfiguration(yamlConfigurationFile, forceOverwriteGeneratedExtensionClasses);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return generate(jpaConfiguration, yamlConfigurationFile, baseDir, baseOutputDir, classLoader);
  }

  /**
   * Generate code based on input
   *
   * @param jpaConfiguration      The Java generator configuration - can be parsed using {@link #parseJpaConfiguration(java.io.File, boolean)}
   * @param yamlConfigurationFile The file pointing to the YAML configuration file
   * @param baseDir               the base dir that this configuration is run in (used for calculating absolute path's)
   * @param baseOutputDir         The directory to use for output files
   * @param classLoader           The classloader to use (may be null - in which case the {@link dk.tigerteam.trimm.mdsd.java.configuration.AbstractConfigurationBasedCodeGenerator#getClassLoader()} defaults apply
   * @return The classes and interfaces generated
   */
  public static List<ClazzOrInterface> generate(final JpaConfiguration jpaConfiguration, final File yamlConfigurationFile, final File baseDir, final File baseOutputDir, final ClassLoader classLoader) {
    try {
      if (PojoConfigurationBasedCodeGenerator.shouldGenerate(yamlConfigurationFile, baseDir, baseOutputDir, jpaConfiguration)) {
        return new JpaConfigurationBasedCodeGenerator(jpaConfiguration, baseDir, baseOutputDir, classLoader).generate();
      } else {
        log.info("All resource files are up to date nothing to generate");
        return null;
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Parse the {@link JavaConfiguration} from the yaml file
   *
   * @param yamlConfigurationFile The file pointing to the YAML configuration file
   * @param forceOverwriteGeneratedExtensionClasses
   *                              Extra option to allow removing ALL generated extension classes (use with care)
   * @return The Java codegeneration configuration
   * @throws FileNotFoundException
   */
  public static JpaConfiguration parseJpaConfiguration(final File yamlConfigurationFile, boolean forceOverwriteGeneratedExtensionClasses)
      throws FileNotFoundException {
    FileReader reader = null;
    try {
      reader = new FileReader(yamlConfigurationFile);
      JpaConfiguration configuration = new Yaml().loadAs(reader, JpaConfiguration.class);
      configuration.setForceOverwriteGeneratedExtensionClasses(forceOverwriteGeneratedExtensionClasses);
      return configuration;
    } finally {
      IOUtils.closeQuietly(reader);
    }
  }
  // --------------------------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Create a new configured generator, call {@link #generate()} to execute the generation
   *
   * @param jpaConfiguration The configuration
   * @param baseDir          the base dir that this configuration is run in (used for calculating absolute path'es)
   * @param baseOutputDir    The directory to use for output files
   */
  public JpaConfigurationBasedCodeGenerator(JpaConfiguration jpaConfiguration, File baseDir, File baseOutputDir) {
    super(jpaConfiguration, baseDir, baseOutputDir);
  }

  /**
   * Create a new configured generator, call {@link #generate()} to execute the generation
   *
   * @param jpaConfiguration The configuration
   * @param baseDir          the base dir that this configuration is run in (used for calculating absolute path'es)
   * @param baseOutputDir    The directory to use for output files
   * @param classLoader      The classloader to use
   */
  public JpaConfigurationBasedCodeGenerator(JpaConfiguration jpaConfiguration, File baseDir, File baseOutputDir, ClassLoader classLoader) {
    super(jpaConfiguration, baseDir, baseOutputDir, classLoader);
  }

  @Override
  protected JpaGeneratorContext createGeneratorContext(MetaModel metaModel) {
    return new JpaGeneratorContext(metaModel);
  }

  @Override
  public List<ClazzOrInterface> generateInternal(final JpaGeneratorContext generatorContext) {
    final JpaConfiguration jpaConfiguration = getConfiguration();

    JpaJavaGenerator javaGenerator = new JpaJavaGenerator();
  	
    if (jpaConfiguration.getCreateIntegrationTest() != null || getConfiguration().getCreateRuntimeMetaModel() != null) {
      generatorContext.addEventListeners(new GeneratorEventListener() {
        @Override
        public void generationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
          if (jpaConfiguration.getCreateIntegrationTest() != null) {
          	log.info("Configuring JpaUnittestCreator");
            JpaUnittestCreator jpaUnittestCreator = new JpaUnittestCreator(
                                                            jpaConfiguration.getCreateIntegrationTest().getTestClassName(),
                                                            jpaConfiguration.getCreateIntegrationTest().getExtendsClass(),
                                                            jpaConfiguration.getCreateIntegrationTest().getTestPackageName(),
                                                            jpaConfiguration.getCreateIntegrationTest().isAutoHandleTransactionsAndResources());
            jpaUnittestCreator.postProcessClazzes(allGeneratedClazzes);
          }

          if (jpaConfiguration.getCreateRuntimeMetaModel() != null) {
          	log.info("Configuring RuntimeMetaDataCreator");
            RuntimeMetaDataCreator modelMetaDataCreator = new RuntimeMetaDataCreator(
                jpaConfiguration.getCreateRuntimeMetaModel().getPackageName()
            );
            modelMetaDataCreator.create(allGeneratedClazzes);
          }
        }

        @Override
        public boolean handle(GeneratorEvent event) {
          return true;
        }

        @Override
        public void generationCommenced() {
        }
      });
    }
    if(jpaConfiguration.getCreateIntegrationTest() != null  && !jpaConfiguration.isAddExtensionAndBaseClassAnnotations()) {
  		log.warn("createIntegrationTest is enabled but addExtensionAndBaseClazzAnnotations is false, it has to be true when createIntegrationTest"
  				+ " is enabled setting addExtensionAndBaseClazzAnnotations to true");
  		generatorContext.setAddExtensionAndBaseClazzAnnotations(true);
  	}
    
    if (jpaConfiguration.getJpaSetup().getRootMappedSuperClass() != null || jpaConfiguration.getRootSuperClass() != null) {
      generatorContext.setInheritanceStrategy(new DefaultJpaInheritanceAndClazzesStrategy() {
        @Override
        public Type resolveMappedSuperClazzType(Clazz clazz) {
          if (jpaConfiguration.getJpaSetup().getRootMappedSuperClass() != null)
            return new Type(jpaConfiguration.getJpaSetup().getRootMappedSuperClass());
          else
            return null;
        }

        @Override
        public Type resolveSuperClazzType(Clazz clazz) {
          if (jpaConfiguration.getRootSuperClass() != null)
            return new Type(jpaConfiguration.getRootSuperClass());
          else
            return null;
        }

      });
    } else if (jpaConfiguration.getJpaSetup().getInheritanceStrategy() != null
        && jpaConfiguration.getJpaSetup().getInheritanceStrategy().getImplementation() != null) {
      generatorContext.setInheritanceStrategy((JpaInheritanceAndClazzesStrategy) loadExtension(new ExtensionConfiguration(jpaConfiguration.getJpaSetup()
          .getInheritanceStrategy().getImplementation())));
    } else {
      generatorContext.setInheritanceStrategy(new DefaultJpaInheritanceAndClazzesStrategy());
    }

    if (jpaConfiguration.getJpaSetup().getIdStrategy() != null
        && jpaConfiguration.getJpaSetup().getIdStrategy().getImplementation() != null) {
      generatorContext.setJpaIdStrategy((JpaIdStrategy) loadExtension(new ExtensionConfiguration(jpaConfiguration.getJpaSetup().getIdStrategy().getImplementation())));
    }
    if (jpaConfiguration.getJpaSetup().getJpaProvider() != null) {
      generatorContext.setJpaProvider((JpaProvider) loadExtension(new ExtensionConfiguration(jpaConfiguration.getJpaSetup().getJpaProvider())));
    } else {
      generatorContext.setJpaProvider(new HibernateProvider()); // Default Hibernate
    }
    log.info("Using Jpa provider '" + generatorContext.getJpaProvider().getClass().getName() + "'");
    if(jpaConfiguration.getJpaSetup().getSchema() != null) {
    	generatorContext.setSchema(jpaConfiguration.getJpaSetup().getSchema());
    }
    if(jpaConfiguration.getJpaSetup().getCatalog() != null) {
    	generatorContext.setCatalog(jpaConfiguration.getJpaSetup().getCatalog());
    }

    // Register JPA Named tables (placed first)
    String jpaNamedTablesAndColumnsListenerFactoryExtension = jpaConfiguration.getJpaSetup().getJpaNamedTablesAndColumnsListenerFactory() != null ? jpaConfiguration.getJpaSetup().getJpaNamedTablesAndColumnsListenerFactory() : DefaultJpaNamedTablesAndColumnsListenerFactory.class.getName();
    log.info("Using JpaNamedTablesAndColumnsListenerFactory: " + jpaNamedTablesAndColumnsListenerFactoryExtension);
    JpaNamedTablesAndColumnsListenerFactory jpaNamedTablesAndColumnsListenerFactory = loadExtension(new ExtensionConfiguration(jpaNamedTablesAndColumnsListenerFactoryExtension));
    JpaNamedTablesAndColumnsListener jpaNamedTablesAndColumnsListener = jpaNamedTablesAndColumnsListenerFactory.create(jpaConfiguration, generatorContext, this);
    if (jpaNamedTablesAndColumnsListener != null) {
        log.info("Registering JpaNamedTablesAndColumnsListener '" + jpaNamedTablesAndColumnsListener.getClass().getName() + "' created by '" + jpaNamedTablesAndColumnsListenerFactory.getClass().getName() + "' as first event listener");
        generatorContext.addAsFirstEventListener(jpaNamedTablesAndColumnsListener);
    }

    // Register the JPAGenerator (placed very first)
    String jpaGeneratorEventListenerFactoryExtension = jpaConfiguration.getJpaSetup().getJpaGeneratorEventListenerFactory() != null ? jpaConfiguration.getJpaSetup().getJpaGeneratorEventListenerFactory() : DefaultJpaGeneratorEventListenerFactory.class.getName();
    log.info("Using JpaGeneratorEventListenerFactoryExtension: " + jpaGeneratorEventListenerFactoryExtension);
    JpaGeneratorEventListenerFactory jpaGeneratorEventListenerFactory = loadExtension(new ExtensionConfiguration(jpaGeneratorEventListenerFactoryExtension));
    JpaGeneratorEventListener jpaGeneratorEventListener = jpaGeneratorEventListenerFactory.create(jpaConfiguration, generatorContext, this);
    jpaGeneratorEventListener.setShouldMakeBaseClazzesMappedSuperClassesIfPossible(
        jpaConfiguration.getJpaSetup().isMakeBaseClassesIntoMappedSuperClassesIfPossible()
    ).setShouldGeneratePresentFieldInEmbeddables(
        jpaConfiguration.getJpaSetup().isGeneratePresentFieldInEmbeddables()
    ).setDefaultToLazyFetchingForAllAssociations(
        jpaConfiguration.getJpaSetup().isDefaultToLazyFetchingForAllAssociations()
    ).setElementCollectionFetchTypeEager(
        jpaConfiguration.getJpaSetup().isElementCollectionFetchTypeEager());
    log.info("Registering JpaGeneratorEventListener '" + jpaGeneratorEventListener.getClass().getName() + "' created by '" + jpaGeneratorEventListenerFactory.getClass().getName() + "' as first event listener");
    generatorContext.addAsFirstEventListener(jpaGeneratorEventListener);
    
    if(getConfiguration().isUseImports()) {
    	generatorContext.addAsLastEventListener(new ImportListener(getConfiguration().getBasePackageName()));
    }

    JavaCodeWriter codeWriter = createCodeWriter();
    CodeWritingStrategy codeWritingStrategy = new FileSystemCodeWritingStrategy(codeWriter);
    List<ClazzOrInterface> allGeneratedClazzes = javaGenerator.transformAndWriteCode(generatorContext, codeWritingStrategy);

    // Generate ORM
    OrmXmlGenerator ormXmlGenerator = new OrmXmlGenerator();
    Document ormXmlDocument = ormXmlGenerator.generate(allGeneratedClazzes);

    String generateToDirectory = null;
    try {
      generateToDirectory = getBaseDir().getAbsolutePath() + File.separator + jpaConfiguration.getGenerateResourcesToPath()
          + File.separator + "META-INF";
      log.info("Writing '" + jpaConfiguration.getJpaSetup().getOrmXmlFileName() + "' to '" + generateToDirectory + "'");
      JDomWriter.write(ormXmlDocument, generateToDirectory, jpaConfiguration.getJpaSetup().getOrmXmlFileName());
    } catch (IOException e) {
      throw new RuntimeException("Failed to create JPA '" + jpaConfiguration.getJpaSetup().getOrmXmlFileName() + "' file to path '" + generateToDirectory + "'", e);
    }

    return allGeneratedClazzes;
  }

    /**
     * Default implementation. Looks at {@link JpaSetup#getResolveTableNames()} or {@link JpaSetup#isAddTableAndColumnNames()} in {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaSetup}.
     * If {@link JpaSetup#isAddTableAndColumnNames()} is true or {@link JpaSetup#getResolveTableNames()} contains any mapping it will
     * load the {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaSetup#getJpaNamedTablesAndColumnsNameResolver()} configured or use
     * {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaConfigurationBasedCodeGenerator.ConfiguredJpaNamedTablesAndColumnsResolver} instead
     */
    public static class DefaultJpaNamedTablesAndColumnsListenerFactory implements JpaNamedTablesAndColumnsListenerFactory {

        @Override
        public JpaNamedTablesAndColumnsListener create(JpaConfiguration jpaConfiguration, JpaGeneratorContext jpaGeneratorContext, ExtensibleCodeGenerator extensibleCodeGenerator) {
            if (jpaConfiguration.getJpaSetup().isAddTableAndColumnNames()
                    || (jpaConfiguration.getJpaSetup().getResolveTableNames() != null && jpaConfiguration.getJpaSetup().getResolveTableNames().size() > 0)) {
                JpaNamedTablesAndColumnsNameResolver namedTablesAndColumnsNameResolver = null;

                if (jpaConfiguration.getJpaSetup().getJpaNamedTablesAndColumnsNameResolver() == null) {
                    namedTablesAndColumnsNameResolver = new ConfiguredJpaNamedTablesAndColumnsResolver(jpaConfiguration);
                } else if (jpaConfiguration.getJpaSetup().getJpaNamedTablesAndColumnsNameResolver() != null) {
                    try {
                        namedTablesAndColumnsNameResolver = extensibleCodeGenerator.loadExtension(new ExtensionConfiguration(jpaConfiguration.getJpaSetup()
                                .getJpaNamedTablesAndColumnsNameResolver()));
                    } catch (Exception e) {
                        throw new RuntimeException("Failed to prepare JpaNamedTablesAndColumnsNameResolver extension '"
                                + jpaConfiguration.getJpaSetup().getJpaNamedTablesAndColumnsNameResolver() + "'", e);
                    }
                } else {
                    namedTablesAndColumnsNameResolver = new DefaultJpaNamedTablesAndColumnsNameResolver();
                }

                return new JpaNamedTablesAndColumnsListener(namedTablesAndColumnsNameResolver);
            }
            return null;
        }
    }

    /**
     * Default implementation. Handles any {@link JpaConfiguration#getMapUmlPropertyTypes()} by ensuring that they're
     * handled as part of {@link dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorEventListener#handleCustomTypeMapping(dk.tigerteam.trimm.mdsd.java.generator.event.SimplePropertyEvent)}
     * where the actual mapping is delegated to the {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaProvider} that's registered with the current
     * {@link dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorContext}
     */
    public static class DefaultJpaGeneratorEventListenerFactory implements JpaGeneratorEventListenerFactory {

        @Override
        public JpaGeneratorEventListener create(final JpaConfiguration jpaConfiguration, final JpaGeneratorContext jpaGeneratorContext, final ExtensibleCodeGenerator extensibleCodeGenerator) {
            return new JpaGeneratorEventListener() {
                @Override
                protected void handleCustomTypeMapping(SimplePropertyEvent event) {
                    if (jpaConfiguration.getMapUmlPropertyTypes() != null) {
                        for (Map<String, String> mapping : jpaConfiguration.getMapUmlPropertyTypes()) {
                            // Changed to be order insensitive
                            if (mapping.get("name").equals(event.getProperty().getField().getType().getName())
                                    || mapping.get("javaType").equals(event.getProperty().getField().getType().getName())) {
                                // e.g. for Hibernate =>  @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
                                JpaGeneratorContext.getContext().getJpaProvider().applyTypeConverter(event, mapping);
                                break;
                            }
                        }
                    }
                }
            };
        }
    }
  // =================================================================================================================================
  private static final class ConfiguredJpaNamedTablesAndColumnsResolver extends DefaultJpaNamedTablesAndColumnsNameResolver {
    private final JpaConfiguration jpaConfiguration;

    private ConfiguredJpaNamedTablesAndColumnsResolver(JpaConfiguration jpaConfiguration) {
      this.jpaConfiguration = jpaConfiguration;
    }

    @Override
    public String resolveTableName(ClazzEvent event, String defaultTableName) {
      for (Map<String, String> mapping : jpaConfiguration.getJpaSetup().getResolveTableNames()) {
        if (mapping.get("defaultTableName").equals(defaultTableName)) {
          String resolvedTableName = mapping.get("resolvedTableName");
          if (resolvedTableName == null) {
            throw new IllegalStateException("Can't map from defaultTableName '" + defaultTableName
                + "' to <null> resolvedTableName");
          }
          return resolvedTableName;
        }
      }
      return super.resolveTableName(event, defaultTableName);
    }

    @Override
    public String resolveColumnName(PropertyEvent event, String defaultColumnName) {
      for (Map<String, String> mapping : jpaConfiguration.getJpaSetup().getResolveColumnNames()) {
        if (mapping.get("defaultColumnName").equals(defaultColumnName)) {
          String resolvedColumnName = mapping.get("resolvedColumnName");
          if (resolvedColumnName == null) {
            throw new IllegalStateException("Can't map from defaultColumnName '" + defaultColumnName
                + "' to <null> resolvedColumnName");
          }
          return resolvedColumnName;
        }
      }
      return super.resolveColumnName(event, defaultColumnName);
    }

    @Override
    public String resolveAttributeOverrideColumnName(PropertyEvent event, Property overrideAttributeProperty,
                                                     String overrideAttributeName, String defaultAttributeOverrideColumnName) {
      for (Map<String, String> mapping : jpaConfiguration.getJpaSetup().getResolveColumnNames()) {
        if (mapping.get("defaultColumnName").equals(defaultAttributeOverrideColumnName)) {
          String resolvedColumnName = mapping.get("resolvedColumnName");
          if (resolvedColumnName == null) {
            throw new IllegalStateException("Can't map from defaultColumnName '" + defaultAttributeOverrideColumnName
                + "' to <null> resolvedColumnName");
          }
          return resolvedColumnName;
        }
      }
      return super.resolveAttributeOverrideColumnName(event, overrideAttributeProperty, overrideAttributeName,
          defaultAttributeOverrideColumnName);
    }
  }


}