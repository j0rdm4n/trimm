/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.codedom.CodeModel;
import dk.tigerteam.trimm.mdsd.java.generator.CollectionTypeResolver;
import dk.tigerteam.trimm.mdsd.java.generator.JavaCollectionTypeResolver;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.JavaInheritanceAndClazzesStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.event.GeneratorEventListener;
import dk.tigerteam.trimm.mdsd.jpa.configuration.JpaProvider;
import dk.tigerteam.trimm.mdsd.jpa.hibernate.HibernateProvider;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;

/**
 * Specialization of {@link JavaGeneratorContext} which contains JPA specifics like the {@link #inheritanceStrategy}.
 * 
 * @author Jeppe Cramon
 */
public class JpaGeneratorContext extends JavaGeneratorContext {

	private String schema;
	private String catalog;
	private JpaIdStrategy jpaIdStrategy = new DefaultJpaIdStrategy();
	private JpaProvider jpaProvider = new HibernateProvider();

  public static JpaGeneratorContext getContext() {
    return JavaGeneratorContext.getContext(JpaGeneratorContext.class);
  }

  public JpaGeneratorContext(MetaModel metaModel, GeneratorEventListener... eventListeners) {
    super(metaModel, new JavaCollectionTypeResolver(), new DefaultJpaInheritanceAndClazzesStrategy(), eventListeners);
  }

  public JpaGeneratorContext(MetaModel metaModel, CodeModel codeModel, CollectionTypeResolver collectionTypeResolver, JavaInheritanceAndClazzesStrategy inheritanceStrategy, boolean createExtensionClazzes, boolean addExtensionAndBaseClazzAnnotations, String collectionPropertyNameSuffix, boolean addDefaultConstructor, GeneratorEventListener... eventListeners) {
    super(metaModel, codeModel, collectionTypeResolver, inheritanceStrategy, createExtensionClazzes, addExtensionAndBaseClazzAnnotations, collectionPropertyNameSuffix, addDefaultConstructor, eventListeners);
  }

  public JpaIdStrategy getJpaIdStrategy() {
		return jpaIdStrategy;
	}

	public void setJpaIdStrategy(JpaIdStrategy jpaIdStrategy) {
		this.jpaIdStrategy = jpaIdStrategy;
	}
	
	public JpaProvider getJpaProvider() {
		return jpaProvider;
	}

	public void setJpaProvider(JpaProvider jpaProvider) {
		this.jpaProvider = jpaProvider;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	@Override
	public JpaInheritanceAndClazzesStrategy getInheritanceStrategy() {
		return (JpaInheritanceAndClazzesStrategy) super.getInheritanceStrategy();
	}
}
