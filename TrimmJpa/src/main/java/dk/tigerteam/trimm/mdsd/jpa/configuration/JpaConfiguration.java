/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.configuration;

import dk.tigerteam.trimm.mdsd.java.configuration.JavaConfiguration;


/**
 * JPA specific configuration - for JPA 2.0 code generation
 * @author Jeppe Cramon
 */
public class JpaConfiguration extends JavaConfiguration {

  private JpaSetup jpaSetup;
  private IntegrationTestSetup createIntegrationTest;
	
  /**
   * Specfifies the JPA specific configurations
   */
	public JpaSetup getJpaSetup() {
		return jpaSetup;
	}

	public void setJpaSetup(JpaSetup jpaSetup) {
		this.jpaSetup = jpaSetup;
	}

	/**
	 * Specifies the Integration test specific configurations
	 */
	public IntegrationTestSetup getCreateIntegrationTest() {
		return createIntegrationTest;
	}

	public void setCreateIntegrationTest(IntegrationTestSetup createIntegrationTest) {
		this.createIntegrationTest = createIntegrationTest;
	}

}
