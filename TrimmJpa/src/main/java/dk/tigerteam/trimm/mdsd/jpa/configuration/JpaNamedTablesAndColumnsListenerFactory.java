package dk.tigerteam.trimm.mdsd.jpa.configuration;

import dk.tigerteam.trimm.mdsd.java.configuration.ExtensibleCodeGenerator;
import dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorContext;
import dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsListener;

/**
 * This factory allows TRIMM-JPA users to provide their own {@link dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsListener}
 * instance together with the {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaConfigurationBasedCodeGenerator}.<br/>
 * The {@link dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsListener} will placed right after the {@link dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorEventListener}
 * which is created using an instance of the {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaGeneratorEventListenerFactory}
 *
 * @since 1.0.1
 * @author Jeppe Cramon
 */
public interface JpaNamedTablesAndColumnsListenerFactory {
    JpaNamedTablesAndColumnsListener create(JpaConfiguration jpaConfiguration, JpaGeneratorContext jpaGeneratorContext, ExtensibleCodeGenerator extensibleCodeGenerator);
}
