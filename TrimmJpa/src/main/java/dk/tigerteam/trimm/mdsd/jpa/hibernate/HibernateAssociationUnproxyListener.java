/*
 * Copyright 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.hibernate;

import dk.tigerteam.trimm.bean.bidirectional.ManyToManyCollectionWrapper;
import dk.tigerteam.trimm.bean.bidirectional.ManyToManyListWrapper;
import dk.tigerteam.trimm.bean.bidirectional.ManyToManySetWrapper;
import dk.tigerteam.trimm.bean.bidirectional.ManyToManySortedSetWrapper;
import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorContext;
import dk.tigerteam.trimm.persistence.hibernate.bean.UnproxyingWrappedCollection;
import dk.tigerteam.trimm.persistence.hibernate.bean.UnproxyingWrappedList;
import dk.tigerteam.trimm.persistence.hibernate.bean.UnproxyingWrappedSet;
import dk.tigerteam.trimm.util.Utils;
import org.hibernate.impl.SessionImpl;
import org.hibernate.proxy.HibernateProxy;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;

/**
 * Helper Listener which will allow ManyToOne and OneToOne property {@link GetterMethod}'s to perform Unproxification of Hibernate proxified
 * instance variables. And setting Jpa provider specific unproxying Collection wrappers on ManyToMany see {@link UnproxyingWrappedSet}.
 * 
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class HibernateAssociationUnproxyListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

	public HibernateAssociationUnproxyListener() {
	}

	@Override
	protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
		GetterMethod getterMethod = event.getProperty().getGetterMethod();
		if (getterMethod != null) {
			handleGetterMethod(getterMethod);
		}
		return true;
	}

	@Override
	protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
		GetterMethod getterMethod = event.getProperty().getGetterMethod();
		if (getterMethod != null) {
			handleGetterMethod(getterMethod);
		}
		return true;
	}

	@Override
	protected boolean handleManyToManyAssociationEvent(ManyToManyAssociationEvent event) {
		if (event.getClazzOrInterface().isInterface()) {
			return true;
		}
		if (!event.getMetaProperty().getAssociation().isBidirectional()) {
			return true;
		}
		GetterMethod getterMethod = event.getProperty().getGetterMethod();
		if (getterMethod != null) {
			handleManyToManyGetterMethod(getterMethod, event);
		}

		return true;
	}

	private void handleManyToManyGetterMethod(GetterMethod getterMethod, ManyToManyAssociationEvent event) {
		Clazz propertyClazz = ((Property) getterMethod.getOwner()).getField().getType().getGenerics().get(0).getClazz();
		if (!getContext(JpaGeneratorContext.class).getInheritanceStrategy().isEntityClazz(propertyClazz)) {
			return;
		}
		Type propertyCollectionType = event.getProperty().getField().getType();
		Type propertyType = propertyCollectionType.getGenerics().get(0).clone();
		String fieldName = event.getProperty().getField().getName();
		Clazz thisClazz = event.getClazz();
		if (thisClazz.isBaseClazz()) {
			thisClazz = getContext().getExtensionClazzForBaseClazz(thisClazz);
		}
		Type thisClazzType = new Type(thisClazz);
		String oppositePropertyCapitalizedName = Utils.capitalize(event.getResolvedOppositeMetaPropertyName());

		CodeSnippet snippet = new CodeSnippet();

		Class<?> wrapperClass = null;
		Class<?> unproxingWrapperClass = null;
		if (propertyCollectionType.getName().equals(java.util.Collection.class.getName())) {
			wrapperClass = ManyToManyCollectionWrapper.class;
			unproxingWrapperClass = UnproxyingWrappedCollection.class;
		} else if (propertyCollectionType.getName().equals(Set.class.getName())) {
			wrapperClass = ManyToManySetWrapper.class;
			unproxingWrapperClass = UnproxyingWrappedSet.class;
		} else if (propertyCollectionType.getName().equals(SortedSet.class.getName())) {
			wrapperClass = ManyToManySortedSetWrapper.class;
			unproxingWrapperClass = UnproxyingWrappedCollection.class;
		} else if (propertyCollectionType.getName().equals(List.class.getName())) {
			wrapperClass = ManyToManyListWrapper.class;
			unproxingWrapperClass = UnproxyingWrappedList.class;
		} else {
			throw new RuntimeException("Unsupported Many-to-Many PropertyCollectionType '" + propertyCollectionType.getName()
					+ "' for property '" + event.getProperty().getField().getMetaType() + "'");
		}

		snippet.print("return new ").print(new Type(wrapperClass).addGenerics(thisClazzType.clone(), propertyType.clone())).print("((")
				.print(thisClazzType).print(")this, ").print("new " + new Type(unproxingWrapperClass) + "(" + fieldName + ")").println(") {");
		snippet.println("@Override");
		snippet.print("protected ").print(new Type(propertyCollectionType.getName()).addGenerics(thisClazzType.clone()))
				.print(" getSourceCollectionInTarget(").print(propertyType).println(" o) {");
		snippet.print("return o.get").print(oppositePropertyCapitalizedName).println("();");
		snippet.println("}");
		snippet.println("};");

		CodeSnippet existingCodeSnippet = getterMethod.findChildOfType(CodeSnippet.class);
		if (existingCodeSnippet != null) {
			getterMethod.getChildren().remove(existingCodeSnippet);
		}
		getterMethod.getChildren().add(snippet);
	}

	private void handleGetterMethod(GetterMethod getterMethod) {
		// Insert an unproxy snippet as the first child
		Clazz propertyClazz = ((Property) getterMethod.getOwner()).getField().getType().getClazz();
		if (!getContext(JpaGeneratorContext.class).getInheritanceStrategy().isEntityClazz(propertyClazz)) {
			return;
		}
		CodeSnippet snippet = new CodeSnippet();
		String var = ((Property) getterMethod.getOwner()).getField().getName();
		snippet.printAllLn("if (", var, " instanceof ", new Type(HibernateProxy.class), ") {");

		snippet.printAllLn(" ", var, " = (", new Type(propertyClazz), ")",
				"(((", new Type(HibernateProxy.class), ")" + var
				+ ").getHibernateLazyInitializer().getSession())",
				".getPersistenceContext().unproxyAndReassociate(", var, ");");
		snippet.printAllLn("}");
		CodeSnippet existingCodeSnippet = getterMethod.findChildOfType(CodeSnippet.class);
		if (existingCodeSnippet != null) {
			snippet.getSnippets().addAll(existingCodeSnippet.getSnippets());
		} else {
			// If there are no children then the GetterMethod DOM element will write out this line of code, but since we insert elements we have
			// to perform this ourselves
			snippet.printAllLn("return ", getterMethod.getOwner().getName(), ";");
		}
		getterMethod.getChildren().remove(existingCodeSnippet);
		getterMethod.getChildren().add(snippet);
	}

	/**
	 * Return the code line which will fetch the currently active {@link SessionImpl} instance
	 */
	protected String getSessionImplInstanceCall(String variableName) {
		return "((" + new Type(SessionImpl.class) + ")((" + new Type(HibernateProxy.class) + ")" + variableName
				+ ").getHibernateLazyInitializer().getSession())";
	}
}
