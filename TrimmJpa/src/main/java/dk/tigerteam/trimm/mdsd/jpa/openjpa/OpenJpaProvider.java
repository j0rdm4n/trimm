/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.openjpa;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.generator.event.SimplePropertyEvent;
import dk.tigerteam.trimm.mdsd.jpa.configuration.JpaProvider;

import javax.persistence.InheritanceType;
import javax.persistence.TemporalType;
import java.util.List;
import java.util.Map;

public class OpenJpaProvider implements JpaProvider {

	@Override
	public void applyTypeConverter(SimplePropertyEvent event, Map<String, String> mapping) {

	}

	@Override
	public void applyProviderSpecificInheritanceStrategy(Clazz entityClazz, List<Clazz> directSubClasses, InheritanceType inheritanceType) {

	}

	@Override
	public TemporalType getDateFieldTemporalType() {
		return TemporalType.DATE;
	}

    @Override
    public void applyDiscriminatorValue(Clazz clazz, List<Clazz> subClasses, String defaultDiscriminatorValue) {

    }

}
