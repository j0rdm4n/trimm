/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.hibernate;

import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.codedom.Field;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.annotations.*;

import javax.persistence.*;

/**
 * Supports the following optimizations on associations: BatchSize: Can be used on class and collection associations: use BATCHSIZE - e.g.
 * 10 value of the batchsize
 * 
 * <pre>
 * 	If one proxy of a User must be initialized, go ahead and initialize several in the same SELECT. In
 * other words, if you already know that there are three Item instances in the persistence
 * context, and that they all have a proxy applied to their seller association,
 * you may as well initialize all the proxies instead of just one.
 * Batch fetching is often called a blind-guess optimization, because you don't know
 * how many uninitialized User proxies may be in a particular persistence context.
 *  Prefetching proxies and collections with a batch strategy is really a blind guess. It's
 *  a smart optimization that can significantly reduce the number of SQL statements
 *  that are otherwise necessary to initialize all the objects you're working with. The
 *  only downside of prefetching is, of course, that you may prefetch data you won't need in the end.
 * </pre>
 * 
 * Subselect: can be used on collections SUBSELECT - SUBSELECT
 * 
 * <pre>
 * 	Prefetching using a subselect is a powerful optimization. Subselect
 * fetching is, at the time of writing, available only for collections, not for entity
 * proxies. Also note that the original query that is rerun as a subselect is only
 * remembered by Hibernate for a particular Session.
 * </pre>
 * 
 * LazyCollection extra: use EXTRA - EXTRA
 * 
 * <pre>
 *  To be used in conjunction with Fetch lazyloading on collections for OneToMany and ManyToMany associations.
 *  LazyCollectionOption can be TRUE (the collection is lazy and will be loaded when its state is accessed), EXTRA (the collection is lazy and all operations will try to avoid the collection loading, 
 *  this is especially useful for huge collections when loading all the elements is not necessary) and FALSE (association not lazy)
 *  Note: If the collection is a Map or a List, the operations containsKey() and get() query the database directly.
 * </pre>
 * 
 * @author Lasse Cramon
 */
public class HibernateFetchOptimizationListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

	private static final Log logger = LogFactory.getLog(HibernateFetchOptimizationListener.class);
	public static String BATCHSIZE_TAGVALUE = "BATCHSIZE";
	public static String FETCH_SUBSELECT = "SUBSELECT";
	public static String FETCH_SELECT = "SELECT";
	public static String FETCH_JOIN = "JOIN";
    public static String FETCH_EXTRA_LAZY = "EXTRA_LAZY";
    /**
     * Backwards compatible for older models using Stereotypes
     */
	public static String FETCH_EXTRA = "EXTRA";
    public static String FETCHMODE_TAGGEDVALUE = "FetchMode";

	public HibernateFetchOptimizationListener() {
	}

	@Override
	protected boolean handleClazzEvent(ClazzEvent event) {
		if (getContext(JpaGeneratorContext.class).getInheritanceStrategy().isEntityClazz(event.getClazz())) {
			String value = event.getMetaClazz().getTaggedValue(BATCHSIZE_TAGVALUE);
			if (value != null) {
				logger.trace("Found tag value '" + value + "' for '" + BATCHSIZE_TAGVALUE + "' in '" + event.getClazz().getName() + "'");
				event.getClazz().addAnnotations(new Annotation(BatchSize.class).addAnnotationAttribute("size", new Integer(value)));
			}
		}
		return true;
	}

	@Override
	protected boolean handleManyToManyAssociationEvent(ManyToManyAssociationEvent event) {

        MetaProperty metaProperty = event.getMetaProperty();
        if (metaProperty.hasStereoType(FETCH_SUBSELECT) || FETCH_SUBSELECT.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_SUBSELECT + "' for ManyToManyAssociation for property '" + event.getProperty().getName()
					+ "' in '" + event.getClazzOrInterface().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Fetch.class).addAnnotationAttribute("value", FetchMode.SUBSELECT));
		}
		if (metaProperty.hasStereoType(FETCH_SELECT) || FETCH_SELECT.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_SELECT + "' for ManyToManyAssociation for property '" + event.getProperty().getName()
					+ "' in '" + event.getClazzOrInterface().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Fetch.class).addAnnotationAttribute("value", FetchMode.SELECT));
		}
		if (metaProperty.hasStereoType(FETCH_JOIN) || FETCH_JOIN.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_JOIN + "' for ManyToManyAssociation for property '" + event.getProperty().getName()
					+ "' in '" + event.getClazzOrInterface().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Fetch.class).addAnnotationAttribute("value", FetchMode.JOIN));
		}
		if (metaProperty.hasStereoType(FETCH_EXTRA) || FETCH_EXTRA_LAZY.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			if (!isFetchLazy(event)) {
				logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_EXTRA_LAZY + "' for ManyToManyAssociation for property '" + event.getProperty().getName()
						+ "' in '" + event.getClazzOrInterface().getName() + "'");
				event.getProperty().getField()
						.addAnnotations(new Annotation(LazyCollection.class).addAnnotationAttribute("value", LazyCollectionOption.EXTRA));
			} else {
				logger.warn("Cant add LazyCollectionOption Extra to association that is not already fetch=lazy");
			}
		}
		String value = metaProperty.getTaggedValue(BATCHSIZE_TAGVALUE);
		if (value != null) {
			logger.trace("Found tag value '" + value + "' for '" + BATCHSIZE_TAGVALUE + "' in '" + event.getClazz().getName() + "'");
			event.getClazz().addAnnotations(new Annotation(BatchSize.class).addAnnotationAttribute("size", new Integer(value)));
		}
		return true;
	}

	@Override
	protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
        MetaProperty metaProperty = event.getMetaProperty();
        if (metaProperty.hasStereoType(FETCH_SUBSELECT) || FETCH_SUBSELECT.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_SUBSELECT + "' for OneToManyAssociation for property '" + event.getProperty().getName()
					+ "' in '" + event.getClazzOrInterface().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Fetch.class).addAnnotationAttribute("value", FetchMode.SUBSELECT));
		}
		if (metaProperty.hasStereoType(FETCH_SELECT) || FETCH_SELECT.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_SELECT + "' for OneToManyAssociation for property '" + event.getProperty().getName()
					+ "' in '" + event.getClazzOrInterface().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Fetch.class).addAnnotationAttribute("value", FetchMode.SELECT));
		}
		if (metaProperty.hasStereoType(FETCH_JOIN) || FETCH_JOIN.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_JOIN + "' for OneToManyAssociation for property '" + event.getProperty().getName()
					+ "' in '" + event.getClazzOrInterface().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Fetch.class).addAnnotationAttribute("value", FetchMode.JOIN));
		}
		if (metaProperty.hasStereoType(FETCH_EXTRA) || FETCH_EXTRA_LAZY.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			if (!isFetchLazy(event)) {
				logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_EXTRA_LAZY + "' for OneToManyAssociation for property '" + event.getProperty().getName()
						+ "' in '" + event.getClazzOrInterface().getName() + "'");
				event.getProperty().getField()
						.addAnnotations(new Annotation(LazyCollection.class).addAnnotationAttribute("value", LazyCollectionOption.EXTRA));
			} else {
				logger.warn("Cant add LaztCollectionOption Extra to association that is not already fetch=lazy");
			}
		}
		String value = metaProperty.getTaggedValue(BATCHSIZE_TAGVALUE);
		if (value != null) {
			logger.trace("Found tag value '" + value + "' for '" + BATCHSIZE_TAGVALUE + "' in '" + event.getClazz().getName() + "'");
			event.getClazz().addAnnotations(new Annotation(BatchSize.class).addAnnotationAttribute("size", new Integer(value)));
		}
		return true;
	}

	@Override
	protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
        MetaProperty metaProperty = event.getMetaProperty();
        if (metaProperty.hasStereoType(FETCH_SELECT) || FETCH_SELECT.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_SELECT + "' for ManyToOneAssociation for property '" + event.getProperty().getName()
					+ "' in '" + event.getClazzOrInterface().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Fetch.class).addAnnotationAttribute("value", FetchMode.SELECT));
		}
		if (metaProperty.hasStereoType(FETCH_JOIN) || FETCH_JOIN.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_JOIN + "' for ManyToOneAssociation for property '" + event.getProperty().getName()
					+ "' in '" + event.getClazzOrInterface().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Fetch.class).addAnnotationAttribute("value", FetchMode.JOIN));
		}
		return true;
	}

	@Override
	protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
        MetaProperty metaProperty = event.getMetaProperty();
        if (metaProperty.hasStereoType(FETCH_SELECT) || FETCH_SELECT.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_SELECT + "' for OneToOneAssociation for property '" + event.getProperty().getName()
					+ "' in '" + event.getClazzOrInterface().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Fetch.class).addAnnotationAttribute("value", FetchMode.SELECT));
		}
		if (metaProperty.hasStereoType(FETCH_JOIN) || FETCH_JOIN.equalsIgnoreCase(metaProperty.getTaggedValue(FETCHMODE_TAGGEDVALUE))) {
			logger.trace("Found stereotype/tagged-value 'FetchMode' '" + FETCH_JOIN + "' for OneToOneAssociation for property '" + event.getProperty().getName() + "' in '"
					+ event.getClazzOrInterface().getName() + "'");
			event.getProperty().getField().addAnnotations(new Annotation(Fetch.class).addAnnotationAttribute("value", FetchMode.JOIN));
		}
		return true;
	}

	private boolean isFetchLazy(AssociationEvent event) {
		Annotation annotation = null;
        Field field = event.getProperty().getField();
        if (field.hasAnnotation(ManyToMany.class)) {
			annotation = field.getAnnotation(ManyToMany.class);
		} else if (field.hasAnnotation(OneToMany.class)) {
			annotation = field.getAnnotation(OneToMany.class);
		} else if (field.hasAnnotation(ManyToOne.class)) {
			annotation = field.getAnnotation(ManyToOne.class);
		} else if (field.hasAnnotation(OneToOne.class)) {
			annotation = field.getAnnotation(OneToOne.class);
		}
		if (annotation != null) {
			FetchType fetch = null;
			fetch = (FetchType) annotation.getAnnotationAttributeValue("fetch");
			if (fetch != null && fetch == FetchType.LAZY) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unused")
	private void removeFetchLazyAnnotation(AssociationEvent event) {
		Annotation annotation = null;
        Field field = event.getProperty().getField();
        if (field.hasAnnotation(ManyToMany.class)) {
			annotation = field.getAnnotation(ManyToMany.class);
		} else if (field.hasAnnotation(OneToMany.class)) {
			annotation = field.getAnnotation(OneToMany.class);
		} else if (field.hasAnnotation(ManyToOne.class)) {
			annotation = field.getAnnotation(ManyToOne.class);
		} else if (field.hasAnnotation(OneToOne.class)) {
			annotation = field.getAnnotation(OneToOne.class);
		}
		if (annotation != null) {
			annotation.removeAnnotationAttribute("fetch");
		}
	}

}
