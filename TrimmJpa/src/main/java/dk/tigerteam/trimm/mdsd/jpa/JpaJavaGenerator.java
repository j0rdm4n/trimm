/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.generator.JavaCodeGenerationStrategy;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator;

/**
 * {@link JavaGenerator} specialization, which is required to work with all the various subtypes of
 * {@link AbstractJpaGeneratorEventListener}.
 * 
 * @author Jeppe Cramon
 * @see AbstractJpaGeneratorEventListener
 */
public class JpaJavaGenerator extends JavaGenerator {
	/**
	 * Create a new JPA generator with the default {@link JpaCodeGenerationStrategy}
	 */
	public JpaJavaGenerator() {
        super(new JpaCodeGenerationStrategy());
	}

  /**
   * Create JPA generator with a custom JavaCodeGenerationStrategy
   * @param javaCodeGenerationStrategy the strategy to use
   */
  public JpaJavaGenerator(JavaCodeGenerationStrategy javaCodeGenerationStrategy) {
    super(javaCodeGenerationStrategy);
  }
}
