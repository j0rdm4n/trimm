package dk.tigerteam.trimm.mdsd.jpa.configuration;

import dk.tigerteam.trimm.mdsd.java.configuration.ExtensibleCodeGenerator;
import dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorContext;
import dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorEventListener;

/**
 * This factory allows TRIMM-JPA users to provide their own {@link dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorEventListener}
 * instance together with the {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaConfigurationBasedCodeGenerator}.<br/>
 * The {@link dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsListener} will placed right in front of the {@link dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsListener}
 * which is created using an instance of the {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaNamedTablesAndColumnsListenerFactory}<p/>
 *
 * After an instance has been returned the following settings will be automatically transferred from the jpaConfiguration
 * <pre>
 * jpaGeneratorEventListenerInstance.setShouldMakeBaseClazzesMappedSuperClassesIfPossible(
 *  jpaConfiguration.getJpaSetup().isMakeBaseClassesIntoMappedSuperClassesIfPossible()
 * ).setShouldGeneratePresentFieldInEmbeddables(
 *  jpaConfiguration.getJpaSetup().isGeneratePresentFieldInEmbeddables()
 * ).setDefaultToLazyFetchingForAllAssociations(
 *  jpaConfiguration.getJpaSetup().isDefaultToLazyFetchingForAllAssociations()
 * ).setElementCollectionFetchTypeEager(
 *  jpaConfiguration.getJpaSetup().isElementCollectionFetchTypeEager())
 * );
 * </pre>
 * @since 1.0.1
 * @author Jeppe Cramon
 */
public interface JpaGeneratorEventListenerFactory {
    JpaGeneratorEventListener create(JpaConfiguration jpaConfiguration, JpaGeneratorContext jpaGeneratorContext, ExtensibleCodeGenerator extensibleCodeGenerator);
}
