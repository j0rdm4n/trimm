/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.configuration;

import dk.tigerteam.trimm.mdsd.jpa.JpaIdStrategy;

/**
 * YAML configuration class for JPA Id Strategy
 * @author Jeppe Cramon
 *
 */
public class IdStrategy {
	private String implementation;

	/**
	 * This must point to a valid {@link JpaIdStrategy} implementation.<p/>
	 * The implementation can be either a Fully Qualified Class Name (FQCN) (the class will be loaded from the classpath) or a <code>.groovy</code> script which contains a Groovy implementation of said interface.
	 */
	public String getImplementation() {
		return implementation;
	}

	public void setImplementation(String implementation) {
		this.implementation = implementation;
	}
}