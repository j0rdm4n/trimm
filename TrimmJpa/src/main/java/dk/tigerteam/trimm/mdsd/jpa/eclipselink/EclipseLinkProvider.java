/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.eclipselink;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.event.SimplePropertyEvent;
import dk.tigerteam.trimm.mdsd.jpa.configuration.JpaProvider;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.StructConverter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.TemporalType;
import java.util.List;
import java.util.Map;

/**
 * EclipseLink 2.4.x JPA provider implementation
 * <p/>
 * EclipseLink {@link Converter} is defined in UmlPropertyMapping using the key: <strong>jpaTypeConverterClass</strong>.<br/>
 * Eg:
 * <pre>
 * mapUmlPropertyTypes:
 *    - name: DateTime
 *      javaType: org.joda.time.DateTime
 *      jpaTypeConverterClass: org.joda.time.???.eclipselink.DateTimeConverter
 *      eclipseLinkStructConverterClass: dk.tigerteam...??..SomeStructConverter
 *    - name: LocalDate
 *      javaType: org.joda.time.LocalDate
 *      jpaTypeConverterClass: org.joda.time.???.eclipselink.LocalDateConverter</pre>
 */
public class EclipseLinkProvider implements JpaProvider {

    public EclipseLinkProvider() {
        super();
    }

    @Override
    public void applyTypeConverter(SimplePropertyEvent event, Map<String, String> mapping) {
        if (mapping.containsKey("jpaTypeConverterClass")) {
            Field field = event
                    .getProperty()
                    .getField();
            String converterId = field.getName() + "Converter";
            field.addAnnotations(
                    new Annotation(Converter.class, true).addAnnotationAttribute("name", converterId).addAnnotationAttribute("converterClass", new Type(mapping.get("jpaTypeConverterClass")).setReference(true)));
            field.addAnnotations(
                    new Annotation(Convert.class, true).addAnnotationAttribute(AnnotationAttribute.DEFAULT_VALUE_ATTRIBUTE_NAME, converterId)
            );
        } else if (mapping.containsKey("eclipseLinkStructConverterClass")) {
            Field field = event
                    .getProperty()
                    .getField();
            String converterId = field.getName() + "Converter";
            field.addAnnotations(
                    new Annotation(StructConverter.class, true).addAnnotationAttribute("name", converterId).addAnnotationAttribute("converter", mapping.get("eclipseLinkStructConverterClass")));
            field.addAnnotations(
                    new Annotation(Convert.class, true).addAnnotationAttribute(AnnotationAttribute.DEFAULT_VALUE_ATTRIBUTE_NAME, converterId)
            );
        }

    }

    @Override
    public void applyProviderSpecificInheritanceStrategy(Clazz entityClazz, List<Clazz> directSubClasses, InheritanceType inheritanceType) {
        // ???
    }

    @Override
    public TemporalType getDateFieldTemporalType() {
        return TemporalType.TIMESTAMP;
    }

    @Override
    public void applyDiscriminatorValue(Clazz clazz, List<Clazz> subClasses, String defaultDiscriminatorValue) {
        // Mapped superclasses also for some strange reason need DiscriminatorValue annotation
        if (clazz.hasAnnotation(MappedSuperclass.class)) {
            clazz.addAnnotations(
                    new Annotation(DiscriminatorValue.class, true)
                            .withDefaultValueAnnotationAttribute(defaultDiscriminatorValue)
            );
        }
    }
}
