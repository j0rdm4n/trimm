/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.jpa.configuration;

import dk.tigerteam.trimm.mdsd.jpa.JpaInheritanceAndClazzesStrategy;

/**
 * YAML configuration class for JPA Inheritance strategy
 * @author Jeppe Cramon
 */
public class InheritanceStrategy {
	private String implementation;

	/**
	 * Must point to a valid implementation of the {@link JpaInheritanceAndClazzesStrategy} interface.<br/>
	 * The implementation can be either a Fully Qualified Class Name (FQCN) (the class will be loaded from the classpath) or a <code>.groovy</code> script which contains a Groovy implementation of said interface.
	 */
	public String getImplementation() {
		return implementation;
	}

	public void setImplementation(String implementation) {
		this.implementation = implementation;
	}
}