/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.hibernate;

import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.codedom.Enumeration;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorContext;
import dk.tigerteam.trimm.mdsd.jpa.JpaInheritanceAndClazzesStrategy;
import dk.tigerteam.trimm.mdsd.meta.MetaEnumeration;
import org.hibernate.annotations.ForeignKey;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

/**
 * Support creating <b>named</b> named {@link ForeignKey}'s.<br/>
 * Note: Hibernate doesn't support named foreignkeys for AssociationOverrides (which is basically Embeddable entities with associations to
 * real Entities).
 * 
 * @author Jeppe Cramon
 */
public class HibernateForeignKeyConstraintsListener extends AbstractGeneratorEventListener {
	@Override
	protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
		if (((event.getMetaProperty().getAssociation().isBidirectional() && event.getMetaProperty().isOwnerOfAssociation()) || !event
				.getMetaProperty().getAssociation().isBidirectional())
				&& !isEmbeddableClazz(event.getProperty().getField().getType()) && !(event.getMetaProperty().getType() instanceof MetaEnumeration)) {
			createForeignKey(event);
		}
		return true;
	}

	/**
	 * Is the type is an {@link Embeddable} entity? - Sugar for {@link JpaInheritanceAndClazzesStrategy#isEmbeddableClazz(Type)}
	 * 
	 * @param type
	 *          The type to test
	 * @return true if the type is an {@link Embeddable} entity
	 */
	protected boolean isEmbeddableClazz(Type type) {
		return getContext(JpaGeneratorContext.class).getInheritanceStrategy().isEmbeddableClazz(type);
	}

	@Override
	protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
		if (((event.getMetaProperty().getAssociation().isBidirectional() /* && event.getMetaProperty().isOwnerOfAssociation() */) || !event
				.getMetaProperty().getAssociation().isBidirectional())
				&& !isEmbeddableClazz(event.getProperty().getField().getType()) && !(event.getMetaProperty().getType() instanceof MetaEnumeration)) {
			createForeignKey(event);
		}
		return true;
	}

	@Override
	protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
		if (!event.getMetaProperty().getAssociation().isBidirectional()
				&& !(((Type) event.getProperty().getField().getType().getChildren().get(0)).getClazz() instanceof Enumeration)
				&& !isEmbeddableClazz(event.getProperty().getField().getType().getGenerics().get(0))) {
			createFkForMappingsWithJoinTable(event);
		}
		return true;
	}

	@Override
	protected boolean handleManyToManyAssociationEvent(ManyToManyAssociationEvent event) {
		if (event.getMetaProperty().isOwnerOfAssociation()) {
			createFkForMappingsWithJoinTable(event);
		}
		return true;
	}

	private void createFkForMappingsWithJoinTable(AssociationEvent event) {
		if (!event.getProperty().getField().hasAnnotation(JoinTable.class)) {
			if (event instanceof OneToManyAssociationEvent && event.getMetaProperty().isOwnerOfAssociation()
					&& !event.getMetaProperty().getAssociation().isBidirectional()) {
				// We'll allow it for OneToMany without jointable in this case
				Annotation joinColumnAnnotation = event.getProperty().getField().getAnnotation(JoinColumn.class);
				if (joinColumnAnnotation == null) {
					throw new RuntimeException("Doesn't support FK's on a unidirectional OneToMany mapping without explicit @JoinColumn");
				}
				String fkName = resolveFkName(joinColumnAnnotation);
				event.getProperty().getField().addAnnotations(new Annotation(ForeignKey.class).addAnnotationAttributes("name", fkName));
				return;
			} else {
				throw new RuntimeException("Doesn't support FK's on a unidirectional OneToMany mapping without explicit @JoinTable");
			}
		}
		Annotation joinTable = event.getProperty().getField().getAnnotation(JoinTable.class);
		Annotation joinColumn = (Annotation) joinTable.getAnnotationAttributeValues("joinColumns").get(0);
		Annotation inverseJoinColumn = (Annotation) joinTable.getAnnotationAttributeValues("inverseJoinColumns").get(0);

		String fkName = resolveFkName(joinColumn);
		String inverseFkName = resolveFkName(inverseJoinColumn);
		event.getProperty().getField()
				.addAnnotations(new Annotation(ForeignKey.class).addAnnotationAttributes("name", fkName, "inverseName", inverseFkName));
	}

	protected String resolveFkName(Annotation joinColumnAttribute) {
		String name = (String) joinColumnAttribute.getAnnotationAttributeValues("name").get(0);
		return name.substring(0, name.length() - 2) + getForeignKeySuffix();
	}

	/**
	 * Get the suffix to use for ForeignKeys. Default is "_FK"
	 */
	protected String getForeignKeySuffix() {
		return "_FK";
	}

	private void createForeignKey(AssociationEvent event) {
		String name = null;
		Annotation column = event.getProperty().getField().getAnnotation(Column.class);
		if (column != null) {
			name = (String) column.getAnnotationAttributeValues("name").get(0);
		} else {
			Annotation joinColumn = event.getProperty().getField().getAnnotation(JoinColumn.class);
			if (joinColumn != null) {
				name = (String) joinColumn.getAnnotationAttributeValues("name").get(0);
			} else {
				System.err.println("Expected to find Column for Property " + event.getClazz() + "#" + event.getProperty().getField().getName());
				name = event.getProperty().getField().getName();
			}
		}
		if (name.endsWith("Id")) {
			name = name.substring(0, name.length() - 2);
		}
		name += getForeignKeySuffix();
		event.getProperty().getField().addAnnotations(new Annotation(ForeignKey.class).addAnnotationAttributes("name", name));
	}
}
