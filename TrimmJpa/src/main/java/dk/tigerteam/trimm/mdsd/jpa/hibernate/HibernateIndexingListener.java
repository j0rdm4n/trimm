/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.hibernate;

import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.Property;
import dk.tigerteam.trimm.mdsd.jpa.BaseJpaGeneratorEventListener;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Table;

import javax.persistence.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Applies the Hibernate specific {@link Table} annotation to specify Indexes for each class carrying the {@link javax.persistence.Table} annotation and which has
 * properties with the {@link #INDEX} tagged value.<br/>
 * Applies "uniqueConstraints" annotation attribute to all classes with the {@link javax.persistence.Table} annotation and which has properties with the tagged value
 * {@link #UNIQUE_INDEX}.<br/>
 * The value of the TaggedValue must adhere to the following pattern: Name[,Order in the Index]. (Note: Name isn't supported by Hibernate for
 * Unique indexes, but must still be applied in the tagged value) <br/>
 * The optional "Order in the Index" tells in which order the columns should be listed, when defining the index (the order is 1-based).<br/>
 * If there are more than one property with the same index name, you MUST apply "Order in the Index".
 * <p/>
 * <u>Value Examples:</u>
 * <pre>
 * EstateNumber_ix
 * EstateNumber_ix,1
 * EstateNumber_ix,2
 * </pre>
 * <p/>
 * <b>Note: This listener must run after {@link dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsListener} has run, otherwise it can't fill in Column names for the
 * index!</b>
 * <p/>
 * <b>Note: This listener will reuse an existing Hibernate {@link Table} annotation if it already exists.</b>
 * <p/>
 * <b>Note: Only support indexes on simple properties (not on associations and not on embedded properties)</b>
 * <p/>
 *
 * @author Jeppe Cramon
 * @see dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsListener
 */
public class HibernateIndexingListener extends BaseJpaGeneratorEventListener {

    /**
     * Backwards compatible tagged value name for unique index
     */
    public static final String UNIQUE_INDEX = "unique_index";
    /**
     * New tagged value name for unique index
     */
    public static final String UNIQUEINDEX = "uniqueindex";
    public static final String INDEX = "index";

    public void generationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
        for (ClazzOrInterface clazzOrInterface : allGeneratedClazzes) {
            if (clazzOrInterface.isClazz()) {
                internalHandleEntityClazzEvent((Clazz) clazzOrInterface);
            }
        }
    }

    private boolean internalHandleEntityClazzEvent(Clazz clazz) {

        if (getContext().getInheritanceStrategy().isEmbeddableClazz(clazz)) {
            // Ignoring embedded clazzes
            return true;
        }

        /**
         * Key: Index name, Value: IndexInfo
         */
        Map<String, IndexInfo> indexes = resolveIndexedProperties(clazz);
        if (indexes.size() > 0) {
            List<Annotation> indexAnnotations = new LinkedList<Annotation>();
            List<Annotation> uniqueConstraintAnnotations = new LinkedList<Annotation>();
            createIndexAnnotations(clazz, indexes, indexAnnotations, uniqueConstraintAnnotations);

            applyAnnotations(clazz, resolveTableAnnotations(clazz), indexAnnotations, uniqueConstraintAnnotations);
        }

        return true;
    }

    public void createIndexAnnotations(Clazz clazz, Map<String, IndexInfo> indexes, List<Annotation> indexAnnotations,
                                       List<Annotation> uniqueConstraintAnnotations) {
        for (String indexName : indexes.keySet()) {

            if (indexes.get(indexName).isEmbedded) {
                createEmbeddedIndexAnnotations(indexes.get(indexName).propertiesInIndex.get(0), indexAnnotations, uniqueConstraintAnnotations);
            } else {
                Annotation indexOrConstraintAnnotation = null;
                boolean isUnique = indexes.get(indexName).isUnique;
                indexOrConstraintAnnotation = createAndAddIndexOrUniqueConstraintAnnotation(indexName, isUnique, indexAnnotations,
                        uniqueConstraintAnnotations);

                List<String> columnNames = new LinkedList<String>();
                for (Property property : indexes.get(indexName).propertiesInIndex) {
                    // Validate
                    if (property == null) {
                        // throw new IllegalArgumentException("Clazz " + clazz + " contains index '" + indexName + "' which has one or more its " +
                        // indexes.get(indexName).propertiesInIndex.size() +
                        // " Index values that are wrong - No property has been marked for order index " + indexCount);
                        continue;
                    }
                    // So far, we can read from either Column or JoinColumn annotations to find the column names that we want
                    Annotation annotation = null;
                    // Added support for associations that use join column
                    if (property.getField().hasAnnotation(JoinColumn.class)) {
                        annotation = property.getField().getAnnotation(JoinColumn.class);
                    } else {
                        annotation = property.getField().getAnnotation(Column.class);
                    }
                    if (annotation == null) {
                        throw new IllegalStateException("Property '" + property + "' in Entity clazz " + clazz
                                + " did not have Column or JoinColumn annotation");
                    }
                    columnNames.add((String) annotation.getAnnotationAttributeValue("name"));
                }
                indexOrConstraintAnnotation.addAnnotationAttribute("columnNames", columnNames.toArray());
            }
        }
    }

    private void createEmbeddedIndexAnnotations(Property property, List<Annotation> indexAnnotations, List<Annotation> uniqueConstraintAnnotations) {
        Clazz clazz = property.getField().getType().getClazz();
        Map<String, IndexInfo> indexes = resolveIndexedProperties(clazz);
        if (indexes.size() == 0) {
            throw new IllegalStateException("Failed to find any Indexed or UniqueConstrainted taggedValues in embedded " + clazz);
        }

        for (String indexName : indexes.keySet()) {
            Annotation indexOrConstraintAnnotation = null;
            boolean isUnique = indexes.get(indexName).isUnique;
            indexOrConstraintAnnotation = createAndAddIndexOrUniqueConstraintAnnotation(indexName, isUnique, indexAnnotations,
                    uniqueConstraintAnnotations);

            List<String> columnNames = new LinkedList<String>();
            for (Property subProperty : indexes.get(indexName).propertiesInIndex) {
                String columnName = findAttributeOverrideColumnName(property, subProperty);
                columnNames.add(columnName);
            }
            indexOrConstraintAnnotation.addAnnotationAttribute("columnNames", columnNames.toArray());
        }
    }

    @SuppressWarnings("unchecked")
    private String findAttributeOverrideColumnName(Property property, Property subProperty) {
        String attributeOverrideNameToFind = property.getField().getName() + "." + subProperty.getField().getName();

        Annotation attributeOverrides = property.getField().getRequiredAnnotation(AttributeOverrides.class);
        List<Annotation> listOfAttributeOverrides = (List<Annotation>) attributeOverrides.getDefaultValueAnnotationAttributeValues();
        for (Annotation attributeOverride : listOfAttributeOverrides) {
            if (attributeOverride.getAnnotationAttributeValue("name").equals(subProperty.getField().getName())) {
                return (String) ((Annotation) attributeOverride.getAnnotationAttributeValue("column")).getAnnotationAttributeValue("name");
            }
        }
        throw new IllegalStateException("Couldn't find AttributeOverride column name for subProperty " + attributeOverrideNameToFind + " in " + property.getField().getType());
    }

    public Annotation createAndAddIndexOrUniqueConstraintAnnotation(String indexName, boolean isUnique, List<Annotation> indexAnnotations,
                                                                    List<Annotation> uniqueConstraintAnnotations) {
        Annotation indexOrConstraintAnnotation;
        if (isUnique) {
            indexOrConstraintAnnotation = new Annotation(UniqueConstraint.class);
            uniqueConstraintAnnotations.add(indexOrConstraintAnnotation);
        } else {
            indexOrConstraintAnnotation = new Annotation(Index.class);
            indexOrConstraintAnnotation.addAnnotationAttribute("name", indexName);
            indexAnnotations.add(indexOrConstraintAnnotation);
        }
        return indexOrConstraintAnnotation;
    }

    public void applyAnnotations(Clazz clazz, TableAnnotations tableAnnotations, List<Annotation> indexAnnotations,
                                 List<Annotation> uniqueConstraintAnnotations) {
        // Apply the index annotations
        if (indexAnnotations.size() > 0) {
            tableAnnotations.hibernateTableAnnotation.addAnnotationAttribute("indexes", indexAnnotations.toArray());
            if (clazz.hasAnnotation(Entity.class)) {
                clazz.addAnnotations(tableAnnotations.hibernateTableAnnotation);
            } else if (clazz.isBaseClazz()) {
                Clazz extensionClazz = getContext().getExtensionClazzForBaseClazz(clazz);
                extensionClazz.addAnnotations(tableAnnotations.hibernateTableAnnotation);
            } else {
                throw new IllegalStateException("Don't know where to put the Hibernate Table annotation for clazz " + clazz);
            }
        }
        if (uniqueConstraintAnnotations.size() > 0) {
            tableAnnotations.tableAnnotation.addAnnotationAttribute("uniqueConstraints", uniqueConstraintAnnotations.toArray());
        }
    }

    public TableAnnotations resolveTableAnnotations(Clazz clazz) {
        TableAnnotations tableAnnotations = new TableAnnotations();
        // Get the Hibernate table annotation (if it has been applied)
        tableAnnotations.hibernateTableAnnotation = clazz.getAnnotation(Table.class);
        tableAnnotations.tableAnnotation = null;
        if (tableAnnotations.hibernateTableAnnotation == null) {
            // It wasn't applied, create one with default values
            // @Entity
            // @Table(name="ITEMS")
            // @org.hibernate.annotations.Table(
            // appliesTo = "ITEMS", indexes =
            // @org.hibernate.annotations.Index(
            // name = "IDX_INITIAL_PRICE",
            // columnNames = { "INITIAL_PRICE", "INITIAL_PRICE_CURRENCY" }
            // )
            // )
            tableAnnotations.hibernateTableAnnotation = new Annotation(Table.class);

            String tableName = null;
            Clazz currentClazz = clazz;
            if (currentClazz.isBaseClazz()) {
                // The table mapping can be put on the Extension Clazz incase we map the Base class as MappedSuperClass, so let's start looking
                // there
                currentClazz = getContext().getExtensionClazzForBaseClazz(currentClazz);
            }
            while (tableName == null && currentClazz != null && currentClazz.hasAnnotation(Entity.class)) {
                tableAnnotations.tableAnnotation = currentClazz.getAnnotation(javax.persistence.Table.class);
                if (tableAnnotations.tableAnnotation != null) {
                    tableName = (String) tableAnnotations.tableAnnotation.getAnnotationAttributeValue("name");
                } else {
                    currentClazz = currentClazz.getExtendsType().getClazz();
                }
            }
            if (tableName == null) {
                throw new IllegalStateException("Couldn't resolve the tableName that Entity " + clazz + " is mapped to");
            }
            tableAnnotations.hibernateTableAnnotation.addAnnotationAttribute("appliesTo", tableName);
        }
        return tableAnnotations;
    }

    /**
     * @param clazz for this clazz
     * @return Key: index name, Value: IndexInfo
     */
    public Map<String, IndexInfo> resolveIndexedProperties(Clazz clazz) {
        Map<String, IndexInfo> indexes = new HashMap<String, IndexInfo>();
        for (Property property : clazz.getProperties()) {
            boolean isIndexed = false;
            boolean isUnique = false;
            boolean isEmbedded = false;
            String indexValue = null;
            if (property.getField().getMetaType().hasTaggedValue(INDEX)) {
                isIndexed = true;
                indexValue = property.getField().getMetaType().getTaggedValue(INDEX);
            } else if (property.getField().getMetaType().hasTaggedValue(UNIQUE_INDEX)) {
                isIndexed = true;
                isUnique = true;
                indexValue = property.getField().getMetaType().getTaggedValue(UNIQUE_INDEX);
            } else if (property.getField().getMetaType().hasTaggedValue(UNIQUEINDEX)) {
                isIndexed = true;
                isUnique = true;
                indexValue = property.getField().getMetaType().getTaggedValue(UNIQUEINDEX);
            } else if (
                    getContext().getInheritanceStrategy().isEmbeddableClazz(property.getField().getType())
                            && property.getMetaType().getCardinalityType() != CardinalityType.Many
                            && doesEmbeddedClazzHaveIndexedProperties(property.getField().getType().getClazz())) {
                isIndexed = true;
                isEmbedded = true;
                indexValue = property.getField().getName();
            }


            if (isIndexed) {
                if (indexValue == null) {
                    throw new IllegalStateException("Property " + property + " in " + clazz
                            + " has either index or unique_index taggedvalue without any value specifying Index name or order");
                }
                String[] indexValues = indexValue.split(",");
                if (indexValues.length == 0) {
                    throw new RuntimeException("Index on property '" + property + "' in clazz '" + clazz + "' is malformed");
                }
                String indexName = indexValues[0].trim();
                int indexOrder = 1;
                if (indexValues.length > 1) {
                    try {
                        indexOrder = Integer.parseInt(indexValues[1].trim());
                    } catch (NumberFormatException e) {
                        throw new RuntimeException("Index with value '" + indexValue + "' on property '" + property + "' in clazz '" + clazz
                                + "' has a malformed order value '" + indexValues[1] + "'");
                    }
                }
                IndexInfo indexInfo = indexes.get(indexName);
                if (indexInfo == null) {
                    indexInfo = new IndexInfo();
                    indexInfo.isUnique = isUnique;
                    indexInfo.isEmbedded = isEmbedded;
                    indexes.put(indexName, indexInfo);
                }

                if (indexOrder <= 0) {
                    throw new RuntimeException("Index with value '" + indexValue + "' on property '" + property + "' in clazz '" + clazz
                            + "' has a order value zero or below. Index orders are 1-based!");
                }

                // Check if there's already a property associated with the given index
                // name and order
                if (indexInfo.propertiesInIndex.size() >= indexOrder)
                    indexInfo.propertiesInIndex.get(indexOrder - 1);
                while (indexInfo.propertiesInIndex.size() < indexOrder) {
                    indexInfo.propertiesInIndex.add(null);
                }
                indexInfo.propertiesInIndex.set(indexOrder - 1, property);
            }
        }
        return indexes;
    }

    private boolean doesEmbeddedClazzHaveIndexedProperties(Clazz propertyType) {
        for (Property property : propertyType.getProperties()) {
            if (property.getMetaType().hasTaggedValue(INDEX) || property.getMetaType().hasTaggedValue(UNIQUE_INDEX) || property.getMetaType().hasTaggedValue(UNIQUEINDEX)) {
                return true;
            }
        }
        return false;
    }

    private static final class IndexInfo {
        boolean isUnique;
        boolean isEmbedded;
        List<Property> propertiesInIndex = new LinkedList<Property>();
    }

    private static final class TableAnnotations {
        Annotation hibernateTableAnnotation;
        Annotation tableAnnotation;
    }
}
