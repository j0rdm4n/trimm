/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.codedom.Property;
import dk.tigerteam.trimm.mdsd.java.codedom.Type;
import dk.tigerteam.trimm.mdsd.java.generator.JavaInheritanceAndClazzesStrategy;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;

import javax.persistence.*;
import java.util.List;

/**
 * Strategy interface which allows several classes and methods to gather information about what inheritance type, discriminator column name,
 * discriminator value and discriminator value is used for a given entity clazz.
 * 
 * @author Jeppe Cramon
 */
public interface JpaInheritanceAndClazzesStrategy extends JavaInheritanceAndClazzesStrategy {
  /**
   * A {@link Clazz} with an accompanying {@link Clazz#getMetaType()} ({@link MetaClazz}) which carries a stereotype by this name will per
   * default marked as {@link Embeddable}.
   */
  public static final String EMBEDDED_CLAZZ = "embedded";

	/**
	 * This method returns what <b>Mapped Super Class</b> that should be used for a given Entity {@link Clazz} in case is hasn't got one
	 * already.<br/>
	 * 
	 * @param clazz
	 *          The entity clazz
	 * @return The type to use as Mapped Super class or <code>null</code> if the class shouldn't inherit from a Mapped Super class
	 */
	Type resolveMappedSuperClazzType(Clazz clazz);

	/**
	 * Is this clazz at the root level Entity clazz wize (this will typically be the top level Entity clazz which carries the
	 * {@link Inheritance} annotation).
	 * 
	 * @param clazz
	 *          The clazz to test
	 * @return true if the clazz is a root level entity clazz
	 */
	boolean isRootEntityClazz(Clazz clazz);

	/**
	 * Is this Clazz a subclass of another entity clazz
	 * 
	 * @param clazz
	 *          The clazz to test
	 * @return true if the clazz is an entity clazz and inherits from another entity clazz
	 */
	boolean isEntitySubClazz(Clazz clazz);

	/**
	 * Is the given {@link Clazz} an {@link Entity} class (by default it will be unless {@link #isEnumerationClazz(Clazz)} or
	 * {@link #isEmbeddableClazz(Clazz)})
	 * 
	 * @param clazz
	 *          The clazz to test
	 * @return true if {@link Clazz} is an entity, otherwise false
	 */
	boolean isEntityClazz(Clazz clazz);

	/**
	 * Is the given type is a JPA {@link Embeddable} class (by default it will look for a Stereotype on the {@link Type#getClazz()}'s
	 * {@link MetaClazz} with the name of {@link #EMBEDDED_CLAZZ})
	 * 
	 * @param type
	 *          The type to test
	 * @return true if the type is an embeddable entity, otherwise false
	 */
	boolean isEmbeddableClazz(Type type);

	/**
	 * Is the given type is a JPA {@link Embeddable} class (by default it will look for a Stereotype on the {@link Type#getClazz()}'s
	 * {@link MetaClazz} with the name of {@link #EMBEDDED_CLAZZ})
	 * 
	 * @param clazz
	 *          The {@link Clazz} to test
	 * @return true if the {@link Clazz} is an embeddable entity, otherwise false
	 */
	boolean isEmbeddableClazz(Clazz clazz);

	/**
	 * Is the given type is a JPA {@link Embeddable} class (by default it will look for a Stereotype on the {@link Type#getClazz()}'s
	 * {@link MetaClazz} with the name of {@link #EMBEDDED_CLAZZ})
	 * 
	 * @param metaClazz
	 *          The {@link MetaClazz} to test
	 * @return true if the {@link MetaClazz} is an embeddable entity, otherwise false
	 */
	boolean isEmbeddableClazz(MetaClazz metaClazz);

	/**
	 * What inheritance type does a given clazz use
	 * 
	 * @param entityClazz
	 *          The entity clazz
	 * @return The {@link Inheritance} annotation value
	 */
	InheritanceType getInheritanceType(Clazz entityClazz);

	/**
	 * What's the name of {@link DiscriminatorColumn}
	 * 
	 * @param entityClazz
	 *          The entity clazz
	 * @return The name of the descriminator column
	 */
	String getDiscriminatorColumnName(Clazz entityClazz);

	/**
	 * What's the value of the given inherited class {@link DiscriminatorValue} annotation
	 * 
	 * @param entityClazz
	 *          The entity clazz
	 * @return The value
	 */
	Object getDiscriminatorValue(Clazz entityClazz);

	/**
	 * What type of discriminator value is used (String, Integer, etc.)
	 * 
	 * @param entityClazz
	 *          The entity Clazz
	 * @return The discriminator value type
	 */
	DiscriminatorType getDiscriminatorValueType(Clazz entityClazz);

	/**
	 * Returns the length for the "length" attribute in the {@link DiscriminatorColumn} annotation
	 * 
	 * @param entityClazz
	 *          The entity clazz
	 * @return The length or <code>null</code> if there's no length limit specified
	 */
	Integer getDiscriminatorLength(Clazz entityClazz);

	/**
	 * This method responsible for applying the JavaDom manipulation so the JPA inheritance configuration is inserted into the JavaDom.
	 *
     * @param clazz
     *          The root Clazz in the hierarchy (excluding a mapped super class)
     * @param directSubClasses
     * @param shouldMakeBaseClazzesMappedSuperClassesIfPossible
     */
	void applyJpaInheritanceStrategy(Clazz clazz, List<Clazz> directSubClasses, boolean shouldMakeBaseClazzesMappedSuperClassesIfPossible);

	/**
	 * Should we generate a RequiredConstraint (like nullable on Columns or Optional on Associations)
	 * 
	 * @param property
	 *          The property
	 * @return true if we should generate the constraint, otherwise false
	 */
	boolean shouldGenerateRequiredConstraintForProperty(Property property);

    /**
     * Handle adding {@literal @DiscriminatorValue} annotation to
     * Entities (and for some frameworks also to MappedSuperClasses, but that's handled through
     * the specific {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaProvider})
     * @param clazz The entity (mapped superclass or real entity class) that's being mapped
     * @param subClasses All its known subclasses
     */
    void applyDiscriminatorValue(Clazz clazz, List<Clazz> subClasses);
}
