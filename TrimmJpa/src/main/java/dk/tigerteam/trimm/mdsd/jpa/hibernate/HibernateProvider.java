/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.hibernate;

import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.generator.event.SimplePropertyEvent;
import dk.tigerteam.trimm.mdsd.jpa.configuration.JpaProvider;
import org.hibernate.annotations.DiscriminatorOptions;

import javax.persistence.InheritanceType;
import javax.persistence.TemporalType;
import java.util.List;
import java.util.Map;

/**
 * Hibernate 3.6.x and 4.x compatible JpaProvider implementation
 * <p/>
 * Hibernate {@link org.hibernate.annotations.Type} is defined in UmlPropertyMapping using the key: <strong>jpaTypeConverterClass</strong>.<br/>
 * Eg:
 * <pre>
 * mapUmlPropertyTypes:
 *    - name: DateTime
 *      javaType: org.joda.time.DateTime
 *       jpaTypeConverterClass: org.joda.time.contrib.hibernate.PersistentDateTime
 *    - name: LocalDate
 *      javaType: org.joda.time.LocalDate
 *      jpaTypeConverterClass: org.joda.time.contrib.hibernate.PersistentLocalDate</pre>
 *
 * @author Jeppe Cramon
 */
public class HibernateProvider implements JpaProvider {
	
  public HibernateProvider() {
		super();
	}

	@Override
  public void applyTypeConverter(SimplePropertyEvent event, Map<String, String> mapping) {
    String typeConverterClass = mapping.get("jpaTypeConverterClass");
    if (typeConverterClass == null) {
      // Backwards compatibility
      typeConverterClass = mapping.get("hibernateCustomTypeMappingType");
    }
    if (typeConverterClass != null) {
      event
          .getProperty()
          .getField()
          .addAnnotations(
              new Annotation(org.hibernate.annotations.Type.class, true).addAnnotationAttribute("type",
                  typeConverterClass));
    }

  }

  @Override
  public void applyProviderSpecificInheritanceStrategy(Clazz entityClazz, List<Clazz> directSubClasses, InheritanceType inheritanceType) {
    if (inheritanceType == InheritanceType.SINGLE_TABLE || inheritanceType == InheritanceType.JOINED) {
      // Hibernate 3.6+ specific (for previous Hibernate versions it was ForceDiscriminator)
      entityClazz.addAnnotations(new Annotation(DiscriminatorOptions.class, true).addAnnotationAttribute("force", true));
    }

  }

  @Override
  public TemporalType getDateFieldTemporalType() {
    return TemporalType.DATE;
  }

    @Override
    public void applyDiscriminatorValue(Clazz clazz, List<Clazz> subClasses, String defaultDiscriminatorValue) {

    }
}
