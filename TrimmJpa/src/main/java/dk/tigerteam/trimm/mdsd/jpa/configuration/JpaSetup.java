/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.jpa.configuration;

import dk.tigerteam.trimm.mdsd.java.configuration.JavaConfiguration;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGenerator;
import dk.tigerteam.trimm.mdsd.jpa.JpaIdStrategy;
import dk.tigerteam.trimm.mdsd.jpa.JpaInheritanceAndClazzesStrategy;
import dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsListener.DefaultJpaNamedTablesAndColumnsNameResolver;
import dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsNameResolver;

import javax.persistence.ElementCollection;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * YAML configuration file for specifying JPA code generation
 * @author Jeppe Cramon
 */
public class JpaSetup {

    private String jpaNamedTablesAndColumnsListenerFactory;
    private String jpaGeneratorEventListenerFactory;
	private boolean makeBaseClassesIntoMappedSuperClassesIfPossible;
	private boolean generatePresentFieldInEmbeddables;
	private boolean defaultToLazyFetchingForAllAssociations;
	private String rootMappedSuperClass;
	private boolean elementCollectionFetchTypeEager = true;
	private boolean addTableAndColumnNames = false;
	private String jpaNamedTablesAndColumnsNameResolver;
	private String ormXmlFileName = "orm.xml";
	private String schema;
	private String catalog;
	private String jpaProvider;
	private InheritanceStrategy inheritanceStrategy;
	private IdStrategy idStrategy;
	private List<Map<String, String>> resolveTableNames = new LinkedList<Map<String,String>>();
	private List<Map<String, String>> resolveColumnNames = new LinkedList<Map<String,String>>();
	private List<Map<String, String>> resolveAttributeOverrideColumnNames = new LinkedList<Map<String,String>>();

    /**
     * Specifies a valid implementation of {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaNamedTablesAndColumnsListenerFactory}.<br/>
     * If none is specified the default {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaConfigurationBasedCodeGenerator.DefaultJpaNamedTablesAndColumnsListenerFactory} is used instead
     */
    public String getJpaNamedTablesAndColumnsListenerFactory() {
        return jpaNamedTablesAndColumnsListenerFactory;
    }

    public void setJpaNamedTablesAndColumnsListenerFactory(String jpaNamedTablesAndColumnsListenerFactory) {
        this.jpaNamedTablesAndColumnsListenerFactory = jpaNamedTablesAndColumnsListenerFactory;
    }

    /**
     * Specifies a valid implementation of {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaGeneratorEventListenerFactory}.<br/>
     * If none is specified the default {@link dk.tigerteam.trimm.mdsd.jpa.configuration.JpaConfigurationBasedCodeGenerator.DefaultJpaGeneratorEventListenerFactory} is used instead
     */
    public String getJpaGeneratorEventListenerFactory() {
        return jpaGeneratorEventListenerFactory;
    }

    public void setJpaGeneratorEventListenerFactory(String jpaGeneratorEventListenerFactory) {
        this.jpaGeneratorEventListenerFactory = jpaGeneratorEventListenerFactory;
    }

    /**
	 * Specify a valid implementation of the {@link JpaNamedTablesAndColumnsNameResolver} interface.<br/>
	 * This can be either a Fully Qualified Class Name (FQCN) (the class will be loaded from the classpath) or a <code>.groovy</code> script which contains a Groovy implementation of said interface.
	 */
	public String getJpaNamedTablesAndColumnsNameResolver() {
		return jpaNamedTablesAndColumnsNameResolver;
	}

	public void setJpaNamedTablesAndColumnsNameResolver(String jpaNamedTablesAndColumnsNameResolverScript) {
		this.jpaNamedTablesAndColumnsNameResolver = jpaNamedTablesAndColumnsNameResolverScript;
	}
	
	/**
	 * What file name should the Orm file have? <b>Default is <code>orm.xml</code></b><br/>
	 * <i>Note: the orm.xml will always be placed inside a META-INF subdirectory of the {@link JpaConfiguration#getGenerateResourcesToPath()}</i>
	 */
	public String getOrmXmlFileName() {
		return ormXmlFileName;
	}

	public void setOrmXmlFileName(String ormXmlFileName) {
		this.ormXmlFileName = ormXmlFileName;
	}

	/**
	 * Should the {@link JavaGenerator} use the {@link dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsListener}?<br/>
	 * In this case you can either supply your own {@link #jpaNamedTablesAndColumnsNameResolver}
	 * configuration (must be a valid instance of {@link JpaNamedTablesAndColumnsNameResolver}
	 * or use the {@link #resolveAttributeOverrideColumnNames}, {@link #resolveColumnNames} and {@link #resolveTableNames}
	 * if your mappings are simple
	 */
	public boolean isAddTableAndColumnNames() {
		return addTableAndColumnNames;
	}

	public void setAddTableAndColumnNames(boolean addTablesAndColumnNames) {
		this.addTableAndColumnNames = addTablesAndColumnNames;
	}

	/**
	 * Should {@link ElementCollection} be set to {@link FetchType#EAGER}? - default is true
	 */
	public boolean isElementCollectionFetchTypeEager() {
		return elementCollectionFetchTypeEager;
	}

	public void setElementCollectionFetchTypeEager(boolean elementCollectionFetchTypeEager) {
		this.elementCollectionFetchTypeEager = elementCollectionFetchTypeEager;
	}

	/**
	 * What {@link MappedSuperclass} should be used for our entities (in case they don't already have one)<br/>
	 * This will become the root class of the entity hierarchy, unless {@link JavaConfiguration#getRootSuperClass()} is specified
	 */
	public String getRootMappedSuperClass() {
		return rootMappedSuperClass;
	}

	public void setRootMappedSuperClass(String rootMappedSuperClass) {
		this.rootMappedSuperClass = rootMappedSuperClass;
	}

	/**
	 * Should the generator try to make base classes (according to the Base/Extension Clazz pattern) MappedSuperclass in case their only subclass is the Extension Clazz (default is true)
	 * This has the advantage of not polluting the Entity table with a discriminator column.
	 */
	public boolean isMakeBaseClassesIntoMappedSuperClassesIfPossible() {
		return makeBaseClassesIntoMappedSuperClassesIfPossible;
	}

	public void setMakeBaseClassesIntoMappedSuperClassesIfPossible(boolean makeBaseClassesIntoMappedSuperClassesIfPossible) {
		this.makeBaseClassesIntoMappedSuperClassesIfPossible = makeBaseClassesIntoMappedSuperClassesIfPossible;
	}

	/**
	 * Should we generate a boolean "present" field (and getter method) in Embeddable classes?
	 * If we do this, then we can save an entity with an embeddable property and get the property back, even though the embeddable instance didn't have any properties filled (if we don't put in present, then Hibernate et.all. will set the embeddable property to null when fetching the object from db again)
	 */
	public boolean isGeneratePresentFieldInEmbeddables() {
		return generatePresentFieldInEmbeddables;
	}

	public void setGeneratePresentFieldInEmbeddables(boolean generatePresentFieldInEmbeddables) {
		this.generatePresentFieldInEmbeddables = generatePresentFieldInEmbeddables;
	}

	/**
	 * Should all associations (OneToOne, OneToMany, ManyToOne and ManyToMany) default to LAZY fetching (can be overridden using StereoTypes LAZY_LOADING_STEREOTYPE and EAGER_LOADING_STEREOTYPE) -> Default is false
	 */
	public boolean isDefaultToLazyFetchingForAllAssociations() {
		return defaultToLazyFetchingForAllAssociations;
	}

	public void setDefaultToLazyFetchingForAllAssociations(boolean defaultToLazyFetchingForAllAssociations) {
		this.defaultToLazyFetchingForAllAssociations = defaultToLazyFetchingForAllAssociations;
	}

	/**
	 * Specify a valid {@link JpaInheritanceAndClazzesStrategy} implementation.<br/>
	 * This strategy allows several classes and methods to gather information about what inheritance type, discriminator column name,
	 * discriminator value and discriminator value is used for a given entity clazz.<p/>
	 * The implementation can be either a Fully Qualified Class Name (FQCN) (the class will be loaded from the classpath) or a <code>.groovy</code> script which contains a Groovy implementation of said interface.<p/>
	 * <b>Note: This setting doesn't have any effect if you specify a value for the {@link #rootMappedSuperClass} property</b>
	 */
	public InheritanceStrategy getInheritanceStrategy() {
		return inheritanceStrategy;
	}

	public void setInheritanceStrategy(InheritanceStrategy inheritanceStrategy) {
		this.inheritanceStrategy = inheritanceStrategy;
	}

	/**
	 * Specify a valid {@link JpaIdStrategy} implementation.<br/>
	 * This strategy interface allow for handling Simple properties which are marked with the {@link dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorEventListener#ID_STEREOTYPE} stereotype<p/>
	 * The implementation can be either a Fully Qualified Class Name (FQCN) (the class will be loaded from the classpath) or a <code>.groovy</code> script which contains a Groovy implementation of said interface.<p/>
	 * <b>Note: This setting doesn't have any effect if you specify a value for the {@link #rootMappedSuperClass} property</b>
	 */
	public IdStrategy getIdStrategy() {
		return idStrategy;
	}

	public void setIdStrategy(IdStrategy idStrategy) {
		this.idStrategy = idStrategy;
	}

	/**
	 * Specify simple mappings between name that the {@link DefaultJpaNamedTablesAndColumnsNameResolver}
	 * decides the Table.<p/>
	 * <i>Example YAML configuration:</i>
	 * <pre>
	 * resolveTableNames:
   *   - defaultTableName: User
   *     resolvedTableName: User_
   *   - defaultTableName: By
   *     resolvedTableName: By_
	 * </pre>
	 */
	public List<Map<String, String>> getResolveTableNames() {
		return resolveTableNames;
	}

	public void setResolveTableNames(List<Map<String, String>> resolveTableNames) {
		this.resolveTableNames = resolveTableNames;
	}

	/**
	 * Specify simple mappings between name that the {@link DefaultJpaNamedTablesAndColumnsNameResolver}
	 * decides the Column.<p/>
	 * <i>Example YAML configuration:</i>
	 * <pre>
	 * resolveColumnNames:
   *   - defaultColumnName: value
   *     resolvedColumnName: value_
   *   - defaultColumnName: type
   *     resolvedColumnName: type_
	 * </pre>
	 */
	public List<Map<String, String>> getResolveColumnNames() {
		return resolveColumnNames;
	}

	public void setResolveColumnNames(List<Map<String, String>> resolveColumnNames) {
		this.resolveColumnNames = resolveColumnNames;
	}

	/**
	 * Specify simple mappings between name that the {@link DefaultJpaNamedTablesAndColumnsNameResolver}
	 * decides the Column.<p/>
	 * <i>Example YAML configuration:</i>
	 * <pre>
	 * resolveAttributeOverrideColumnNames:
   *   - defaultColumnName: value_type_somethingelse
   *     resolvedColumnName: v_t_s
	 * </pre>
	 */
	public List<Map<String, String>> getResolveAttributeOverrideColumnNames() {
		return resolveAttributeOverrideColumnNames;
	}

	public void setResolveAttributeOverrideColumnNames(List<Map<String, String>> resolveAttributeOverrideColumnNames) {
		this.resolveAttributeOverrideColumnNames = resolveAttributeOverrideColumnNames;
	}

	public String getJpaProvider() {
		return jpaProvider;
	}

	public void setJpaProvider(String jpaProvider) {
		this.jpaProvider = jpaProvider;
	}

	/**
	 * Default schema name used with @Table(schema="SCHEMA_NAME")
	 * 
	 * @return schema name
	 */
	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	/**
	 * Default catalog name used with @Table(catalog="CATALOG_NAME")
	 * 
	 * @return schema name
	 */
	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}
}