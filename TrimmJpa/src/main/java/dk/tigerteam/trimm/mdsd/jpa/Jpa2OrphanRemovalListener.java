/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.generator.event.AssociationEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.OneToManyAssociationEvent;
import dk.tigerteam.trimm.mdsd.java.generator.event.OneToOneAssociationEvent;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.AssociationType;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * JPA 2.x specific extension<br/>
 * This extension will automatically investigate the MetaModel elements connected to a 1-1 or 1-m associations and
 * from their design and connection in the metamodel determine if the association can support JPA
 * <code>orphanRemoval</code>. If it does, then this extension will automatically add the necessary attribute parameter
 *  <code>orphanRemoval</code> to the {@literal @OneToMany} and {@literal @OneToOne} annotations.
 *  <p/>
 * This extension specifically ONLY supports <b>owning OneToOne unidirectional</b> or <b>owning OneToMany unidirectional</b>
 * (and not self-referencing) associations to entities which have no relationships to other entities, or which
 * have <b>owning</b> <b>unidirectional</b> OneToOne or ManyToOne associations to other entities.
 *
 * @author Jeppe Cramon
 */
public class Jpa2OrphanRemovalListener extends BaseJpaGeneratorEventListener {
	@Override
	protected boolean handleOneToOneOwnerOfAssociation(OneToOneAssociationEvent event) {
		if (isDeleteOrphanCandidate(event)) {
			Annotation annotation = event.getProperty().getField().findOrAddAnnotation(OneToOne.class);
			addOrSetOrphanRemovalAnnotationAttribute(annotation);
		}
		return true;
	}

	@Override
	protected boolean handleOneToManyOwnerOfAssociation(OneToManyAssociationEvent event) {
		if (isDeleteOrphanCandidate(event)) {
			Annotation annotation = event.getProperty().getField().findOrAddAnnotation(OneToMany.class);
			addOrSetOrphanRemovalAnnotationAttribute(annotation);
			// Remove the Set collection method since it allows for situations where Hibernate
			// (and perhaps other JPA implementations) fails, if you overwrite its internal collection wrapper
			event.getProperty().removeSetterMethod();
		}
		return true;
	}

	protected boolean isDeleteOrphanCandidate(AssociationEvent event) {
		if (event.getMetaProperty().isOwnerOfAssociation() && !event.getMetaProperty().getAssociation().isBidirectional()
				&& !event.getMetaProperty().getAssociation().isSelfReferencing()) {
			// Check the clazz of the opposite property to see what kind of associations it has
			for (MetaProperty subMetaProperty : event.getMetaProperty().getType().getProperties()) {
				if (subMetaProperty.isPartInAnAssociation()) {
					if (subMetaProperty.isOwnerOfAssociation()) {
						// One-To-One are always good for owning associations, so is Many-to-One,
						// so if we meet a ManyToMany or a OneToMany we fail it as a candidate for delete orphan
						if (subMetaProperty.getAssociationType() == AssociationType.ManyToMany
								|| subMetaProperty.getAssociationType() == AssociationType.OneToMany) {
							return false;
						}
					} else if (subMetaProperty.getAssociation().isBidirectional()) {
						// The type of the our sub property is not an owning association and we have
						// a java association in both directions (bidirectional), which hibernate doesn't handle
						return false;
					}
				}
			}
			return true;
		}
		return false;
	}
	
	private void addOrSetOrphanRemovalAnnotationAttribute(Annotation annotation) {
		if (annotation.hasAnnotationAttribute("orphanRemoval")) {
			annotation.getAnnotationAttribute("orphanRemoval").setValues(true);
		} else {
			annotation.addAnnotationAttribute("orphanRemoval", true);
		}
	}
}
