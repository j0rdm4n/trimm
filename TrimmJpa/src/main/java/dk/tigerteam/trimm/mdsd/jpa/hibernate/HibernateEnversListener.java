/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.hibernate;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorContext;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.JoinTable;
import javax.persistence.MappedSuperclass;
import java.util.ArrayList;
import java.util.List;

/**
 * Event listener to handle history/audit of artifacts that require revision history with Hibernate Envers
 * Supports most features of Envers
 * 
 * Check http://docs.jboss.org/hibernate/envers/${version}/reference/en-US/html_single/ for envers documentation
 * 
 * @author Lasse Cramon
 */
public class HibernateEnversListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

	private static final Log logger = LogFactory.getLog(HibernateEnversListener.class);
	
	public static final String HISTORY_STEREOTYPE_NAME = "history";
	public static final String AUDIT_STEREOTYPE_NAME = "audit";
	public static final String NO_HISTORY_STEREOTYPE_NAME = "nohistory";
	public static final String NO_AUDIT_STEREOTYPE_NAME = "noaudit";
	
	//	If you'd like to override auditing behaviour of some fields/properties in an embedded component,
	//	you can use the @AuditOverride(s) annotation on the place where you use the component.
	
	@SuppressWarnings("unchecked")
	@Override
	protected boolean handleClazzEvent(ClazzEvent event) {
		if(isEntityClazz(event) || event.getClazz().hasAnnotations(MappedSuperclass.class)) {
			if(hasHistoryStereotype(event.getMetaClazz())) {
				Clazz clazz = event.getClazz();
				logger.debug("Event '" + event + "' with clazz '" + clazz.getFQCN() + "' has stereotype History or Audit ");
				Annotation annotation = new Annotation(Audited.class);
//				handleAuditParents(clazz, annotation); needs to tested
				clazz.addAnnotations(annotation);
			}
		}
		return true;
	}

//	protected void handleAuditParents(Clazz clazz, Annotation annotation) {
//		List<Clazz> allSuperClazzes = clazz.getAllSuperClazzes();
//		List<Type> types = new ArrayList<Type>();
//		for (Clazz clazz_ : allSuperClazzes) {
//			types.add(new Type(clazz_));
//		}
//		if(!types.isEmpty()) {
//			annotation.addAnnotationAttribute("auditParents", types.toArray(new Type[types.size()]));
//		}
//	}
	
	@Override
	protected boolean handleSimplePropertyEvent(SimplePropertyEvent event) {
		if(hasNoHistoryStereotype(event.getMetaProperty())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' has stereotype NO History or Audit");
			property.getField().addAnnotations(new Annotation(NotAudited.class));
		}
		if(hasHistoryStereotype(event.getMetaProperty())) {
			Property property = event.getProperty();
			logger.debug("Event '" + event + "' with property '" + property.getName() + "' has stereotype History or Audit");
			boolean clazzHasAuditedAnnotation = doesPropertyClazzHaveHistoryStereotypeOrAuditedAnnotation(property);
			if(!clazzHasAuditedAnnotation) {
				property.getField().addAnnotations(new Annotation(Audited.class));
				handleClazzHierarchy((Clazz)property.getOwner()); // handle cast problem !!!
			}
		}
		return true;
	}

	@Override
	protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
		if(hasNoHistoryStereotype(event.getMetaProperty().getAssociation())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' association has stereotype NO History or Audit ");
			property.getField().addAnnotations(new Annotation(NotAudited.class));
		}
		if(hasHistoryStereotype(event.getMetaProperty().getAssociation())) {
			Property property = event.getProperty();
			logger.debug("Event '" + event +"' with property '" + property.getName() + "' association has stereotype History or Audit ");
			boolean clazzHasAuditedAnnotation = doesPropertyClazzHaveHistoryStereotypeOrAuditedAnnotation(property);
			if(!clazzHasAuditedAnnotation) {
				Annotation annotation = new Annotation(Audited.class);
				handleRelationTarget(event, annotation);
				property.getField().addAnnotations(annotation);
				handleClazzHierarchy((Clazz)property.getOwner());
			}
		}
		
		return true;
	}

	@Override
	protected boolean handleManyToManyAssociationEvent(ManyToManyAssociationEvent event) {
		if(hasNoHistoryStereotype(event.getMetaProperty().getAssociation())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' association has stereotype NO History or Audit ");
			property.getField().addAnnotations(new Annotation(NotAudited.class));
		}
		if(hasHistoryStereotype(event.getMetaProperty().getAssociation())) {
			Property property = event.getProperty();
			logger.debug("Event '" + event +"' with property '" + property.getName() + "' association has stereotype History or Audit ");
			boolean clazzHasAuditedAnnotation = doesPropertyClazzHaveHistoryStereotypeOrAuditedAnnotation(property);
			if(!clazzHasAuditedAnnotation) {
				Annotation annotation = new Annotation(Audited.class);
				handleRelationTarget(event, annotation);
				property.getField().addAnnotations(annotation);
				handleClazzHierarchy((Clazz)property.getOwner());
			}
		}
		return true;
	}

	@Override
	protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
		if(hasNoHistoryStereotype(event.getMetaProperty().getAssociation())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' association has stereotype NO History or Audit ");
			property.getField().addAnnotations(new Annotation(NotAudited.class));
		}
		if(hasHistoryStereotype(event.getMetaProperty().getAssociation())) {
			Property property = event.getProperty();
			logger.debug("Event '" + event +"' with property '" + property.getName() + "' association has stereotype History or Audit ");
			boolean clazzHasAuditedAnnotation = doesPropertyClazzHaveHistoryStereotypeOrAuditedAnnotation(property);
			if(!clazzHasAuditedAnnotation) {
				Annotation annotation = new Annotation(Audited.class);
				handleRelationTarget(event, annotation);
				property.getField().addAnnotations(annotation);
				handleClazzHierarchy((Clazz)property.getOwner());
			}
		}
		return true;
	}

//	One special case are relations mapped with  @OneToMany+@JoinColumn on the one side,
//	and  @ManyToOne+@JoinColumn(insertable=false, updatable=false) on the many side.
//	Such relations are in fact bidirectional, but the owning side is the collection (see
//	alse  here [http://docs.jboss.org/hibernate/stable/annotations/reference/en/html_single/#entityhibspec-collection-extratype]).
//	To properly audit such relations with Envers, you can use the  @AuditMappedBy annotation.
//	It enables you to specify the reverse property (using the  mappedBy element). In case of
//	indexed collections, the index column must also be mapped in the referenced entity (using
//	@Column(insertable=false, updatable=false), and specified using positionMappedBy. This
//	annotation will affect only the way Envers works. Please note that the annotation is experimental
//	and may change in the future. 
//	NB: Not something we support at the moment 
	@Override
	protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
		if(hasNoHistoryStereotype(event.getMetaProperty().getAssociation())) {
			Property property = event.getProperty();
			logger.debug("Property '" + property.getName() + "' association has stereotype NO History or Audit ");
			property.getField().addAnnotations(new Annotation(NotAudited.class));
		}
		if(hasHistoryStereotype(event.getMetaProperty().getAssociation())) {
			Property property = event.getProperty();
			logger.debug("Event '" + event +"' with property '" + property.getName() + "' association has stereotype History or Audit ");
			boolean clazzHasAuditedAnnotation = doesPropertyClazzHaveHistoryStereotypeOrAuditedAnnotation(property);
			if(!clazzHasAuditedAnnotation) {
				Annotation annotation = new Annotation(Audited.class);
				handleRelationTarget(event, annotation);
				property.getField().addAnnotations(annotation);
				handleClazzHierarchy((Clazz)property.getOwner());
				boolean hasJoinTable = hasJoinTable(property);
				if(!hasJoinTable) {
					logger.debug("History or Audit stereotype for '" + property.getName() + "' is unidirectional and does not" +
						" use JoinTable adding AuditTable to Audit annotation");
					property.getField().addAnnotations(new Annotation(AuditJoinTable.class).
							addAnnotationAttributes("name", event.getResolvedJavaFieldName() + "_" +
									event.getResolvedOppositeMetaPropertyName() + "_AUD_JOIN")); // TODO: Handle name ?? 
				}
			}
		}
		
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean hasJoinTable(Property property) {
		if(property.getField().hasAnnotations(JoinTable.class)) {
			return true;
		}
		return false;
	}

	private boolean isEntityClazz(ClazzEvent event) {
		if (getContext(JpaGeneratorContext.class).getInheritanceStrategy().isEntityClazz(event.getClazz())) {
			return true;
		}
		return false;
	}
	
	private boolean doesPropertyClazzHaveHistoryStereotypeOrAuditedAnnotation(Property property) {
		boolean isClazz = property.getOwner().isClazz();
		boolean clazzHasAuditedAnnotation = false;
		if(isClazz) {
			Clazz clazz = (Clazz) property.getOwner();
			clazzHasAuditedAnnotation = hasAuditedAnnotation(clazz) || hasHistoryStereotype(clazz.getMetaType());
			logger.debug("Property '" + property.getName() + "' belonging to clazz '" + clazz.getName() + "' has" +
					" already got audited annotation or history stereotype '" + clazzHasAuditedAnnotation + "'");
		}
		return clazzHasAuditedAnnotation;
	}
	
	private void handleRelationTarget(AssociationEvent event, Annotation annotation) {
		Property property = findPropertyFromMetaClazzByPropertyName(event.getOppositeMetaProperty().getOwnerClazz(), event.getResolvedOppositeMetaPropertyName());
		boolean hasNoHistoryStereotype = hasNoHistoryStereotype(event.getOppositeMetaProperty().getAssociation());
		boolean hasHistoryStereotype = hasHistoryStereotype(event.getOppositeMetaProperty().getOwnerClazz());
		boolean hasClazzAuditAnnotation = hasClazzAuditAnnotation(event.getOppositeMetaProperty().getOwnerClazz());
		boolean hasPropertyAuditAnnotation = property != null && (hasAuditedAnnotation(property.getField()) || hasAuditedAnnotation(property));
		boolean hasPropertyNotAuditAnnotation = property != null && (hasNotAuditedAnnotation(property.getField()) || hasNotAuditedAnnotation(property));
		if(event.getMetaProperty().getAssociation().isBidirectional() || hasPropertyAuditAnnotation ) {
			return;
		} else if(hasPropertyNotAuditAnnotation ||
				 !hasClazzAuditAnnotation ||
				 !hasHistoryStereotype ||
				 hasNoHistoryStereotype) { 
			logger.debug("Adding RelationTargetAuditMode.NOT_AUDITED for assocation oppositeside property '" + event.getResolvedOppositeMetaPropertyName() + "'");
			annotation.addAnnotationAttribute("targetAuditMode", RelationTargetAuditMode.NOT_AUDITED);
		}
	}

	// Do not use this, not we should do it this way ???
//	protected void handleBidirectionality(AssociationEvent event) {
//		boolean bidirectionalHistoryAssociation = event.getMetaProperty().getAssociation().isBidirectional() && hasHistoryStereotype(event.getOppositeMetaProperty().getAssociation());
//		logger.debug(event.getResolvedJavaFieldName() + " is a bidirectional '" + bidirectionalHistoryAssociation + "'" +
//				" with '" + event.getResolvedOppositeMetaPropertyName() + " in '" + event.getOppositeMetaProperty().getOwnerClazz().getName() + "'");
//		// should we set Audit annotation and the opposite side ???
//		if(bidirectionalHistoryAssociation) {
//			ClazzOrInterface oppositeAssociationClazz = JpaGeneratorContext.getContext().getClazzFromMetaClazz(event.getOppositeMetaProperty().getOwnerClazz());
//			Clazz clazz = (Clazz) oppositeAssociationClazz; // handle cast problems !!!
//			Property oppositeProperty = clazz.getPropertyByNameRequired(event.getResolvedOppositeMetaPropertyName());
//			logger.debug("Adding Audit annotation on opposite property '" + oppositeProperty.getName() + "' in oppositeproperty clazz" +
//					" '" + clazz.getFQCN() + "'");
//			oppositeProperty.getField().addAnnotations(new Annotation(Audited.class));
//			// handle clazz ???
//			handleClazzHierarchy(clazz);
//		}
//	}
	
	/**
	 * This is a helper method to help with the way or shortcomings of the way Envers handles
	 * class hierarchy. NB: this does not handler classes outside the generated artifacts
	 * like AbstractEntity. These classes must be handle separately with Envers annotations otherwise
	 * you may get Hibernate mapping exceptions on startup.
	 * Example:
	 * 
	 * If you have a class hierarchy AbstractEntity (handled by your self) -> User -> Customer
	 * and you have and History stereotype on Customer and non on User. You need to add Audited
	 * annotation to User and make all properties NotAudited for it to work with Envers.  
	 * 
	 * @param clazz for this clazz
	 */
	private void handleClazzHierarchy(Clazz clazz) {
		if(clazz != null) {
			List<Clazz> allSuperClazzes = clazz.getAllSuperClazzes();
			logger.debug("Clazz '" + clazz.getName() + "' has '" + allSuperClazzes.size() + "' superclazzs in it's hierarchy");
			for (Clazz superclazz : allSuperClazzes) {
				logger.debug("Checking superclazz '" + superclazz.getName() + "' for Audited annotation or History stereotype");
				if(!hasAuditedAnnotation(superclazz) && !hasHistoryStereotype(superclazz.getMetaType())) {
					logger.debug("No Audited annotation or History stereotype on superclazz '" + superclazz.getName() + "' adding one and checking properties");
					superclazz.addAnnotations(new Annotation(Audited.class));
					for(Property property : superclazz.getProperties()) {
						if(hasAuditedAnnotation(property) || hasAuditedAnnotation(property.getField())) {
							logger.debug("Property '" + property.getName() + "' has Audited annotation");
							continue;
						} else {
							logger.debug("Adding NotAudit annotation to property '" + property.getName() + "' in superclazz '" + superclazz.getName() + "'");
							if(!property.getField().hasAnnotation(NotAudited.class)) {
								property.getField().addAnnotations(new Annotation(NotAudited.class));
							}
						}
					}
				}
			}
		}
	}
	
	protected String[] auditParentsAnnotationAttributes(List<Clazz> extendsClazzes) {
		String[] parents = new String[extendsClazzes.size()];
		for (int i = 0; i < extendsClazzes.size(); i++) {
			parents[i] = extendsClazzes.get(i).getName() + ".class";
		}
		return parents;
	}
	
	private boolean hasClazzAuditAnnotation(MetaClazz metaClazz) {
		ClazzOrInterface clazzOrInterface = JpaGeneratorContext.getContext().getClazzFromMetaClazz(metaClazz);
		if(clazzOrInterface != null && clazzOrInterface.isClazz()) {
			return clazzOrInterface.hasAnnotation(Audited.class);
		}
		return false;
	}
	
	private Property findPropertyFromMetaClazzByPropertyName(MetaClazz metaClazz, String propertyName) {
		ClazzOrInterface clazzOrInterface = JpaGeneratorContext.getContext().getClazzFromMetaClazz(metaClazz);
		if(clazzOrInterface != null && clazzOrInterface.isClazz()) {
			Clazz clazz = (Clazz) clazzOrInterface;
			Property property = clazz.getPropertyByName(propertyName);
//			logger.debug("Found property '" + property != null ? property.getName() : "null " + " serching in clazz '" 
//					+ clazz != null ? clazz.getName() : "null " + " with property search name '" + propertyName + "'");
			return property;
		}
		return null;
	}
	
	private boolean hasHistoryStereotype(MetaType metaType) {
		if(metaType != null && (metaType.hasStereoType(HISTORY_STEREOTYPE_NAME) ||
				metaType.hasStereoType(AUDIT_STEREOTYPE_NAME))) {
			return true;
		}
		return false;
	}
	
	private boolean hasNoHistoryStereotype(MetaType metaType) {
		if(metaType != null && (metaType.hasStereoType(NO_HISTORY_STEREOTYPE_NAME) ||
				metaType.hasStereoType(NO_AUDIT_STEREOTYPE_NAME))) {
			return true;
		}
		return false;
	}
	
	private boolean hasAuditedAnnotation(CodeElement element) {
		return element.hasAnnotation(Audited.class);
	}
	
	protected boolean hasNotAuditedAnnotation(CodeElement element) {
		return element.hasAnnotation(NotAudited.class);
	}
	
}
