/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.generator.event.*;

/**
 * Default implementation for all processing methods in {@link AbstractJpaGeneratorEventListener}, which all returns true.
 * 
 * @author Jeppe Cramon
 */
public abstract class BaseJpaGeneratorEventListener extends AbstractJpaGeneratorEventListener {

	@Override
	protected boolean handleBasicEmbeddedProperty(SimplePropertyEvent event) {
		return true;
	}

	@Override
	protected boolean handleBasicEnumerationProperty(SimplePropertyEvent event) {
		return true;
	}

	@Override
	protected boolean handleBasicLobProperty(SimplePropertyEvent event) {
		return true;
	}

	@Override
	protected boolean handleBasicProperty(SimplePropertyEvent event) {
		return true;
	}

	@Override
	protected boolean handleCollectionOfBasicTypesProperty(SimplePropertyEvent event) {
		return true;
	}

	@Override
	protected boolean handleCollectionOfEmbeddableElementsProperty(SimplePropertyEvent event) {
		return true;
	}

	@Override
	protected boolean handleEmbeddableClazzEvent(ClazzEvent event) {
		return true;
	}

	@Override
	protected boolean handleEntityClazzEvent(ClazzEvent event) {
		return true;
	}

	@Override
	protected boolean handleManyToManyOwnedByAssociation(ManyToManyAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleManyToManyOwnerOfAssociation(ManyToManyAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleManyToOneOfEmbeddableElement(ManyToOneAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleManyToOneOwnerOfAssociation(ManyToOneAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleOneToManyOfEmbeddableElements(OneToManyAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleOneToManyOfEnumeratedElements(OneToManyAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleOneToManyOwnedByAssociation(OneToManyAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleOneToManyOwnerOfAssociation(OneToManyAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleOneToOneOfEmbeddableElement(OneToOneAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleOneToOneOfEnumerationElement(OneToOneAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleOneToOneOwnedByAssociation(OneToOneAssociationEvent event) {
		return true;
	}

	@Override
	protected boolean handleOneToOneOwnerOfAssociation(OneToOneAssociationEvent event) {
		return true;
	}
}
