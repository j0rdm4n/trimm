/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.DefaultJavaInheritanceAndClazzesStrategy;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.util.Utils;

import javax.persistence.*;
import java.util.List;

/**
 * Default implementation which should handle most needs (doesn't currently handle shifting of inheritance type within a hierarchy).
 * Default to <code>SINGLE_TABLE</code> inheritance. If you need something else you can inherit from this class and override
 * {@link #getInheritanceType(dk.tigerteam.trimm.mdsd.java.codedom.Clazz)}
 *
 * @author Jeppe Cramon
 */
public class DefaultJpaInheritanceAndClazzesStrategy extends DefaultJavaInheritanceAndClazzesStrategy implements
        JpaInheritanceAndClazzesStrategy {


    /**
     * A clazz is an Entity class if it's <b>NOT</b> an Enumeration-Clazz and not an Embeddable-Clazz
     *
     * @param clazz The clazz to test
     */
    public boolean isEntityClazz(Clazz clazz) {
        if (clazz == null)
            return false;
        return !isEnumerationClazz(clazz) && !isEmbeddableClazz(clazz);
    }

    /**
     * Is a super-clazz of the test clazz an Entity-class?
     *
     * @param clazz The clazz to test
     * @see #isEntityClazz(dk.tigerteam.trimm.mdsd.java.codedom.Clazz)
     */
    public boolean isEntitySubClazz(Clazz clazz) {
        // To be safe and support future mixed inheritance scheme with possible non entity classes inside the hierarchy will go all the way
        // until we don't have a type to extend, unless we meet an Entity class on our way up the hierarchy.
        Type currentType = clazz.getExtendsType();
        while (currentType != null) {
            Clazz currentClazz = currentType.getClazz();
            if (currentClazz == null) {
                break;
            }
            if (isEntityClazz(currentClazz)) {
                return true;
            } else {
                currentType = currentClazz.getExtendsType();
            }
        }
        return false;
    }

    /**
     * Is the test clazz the top most Entity-class
     *
     * @param clazz The clazz to test
     * @see #isEntityClazz(dk.tigerteam.trimm.mdsd.java.codedom.Clazz)
     */
    public boolean isRootEntityClazz(Clazz clazz) {
        if (clazz.getExtendsType() == null || clazz.getExtendsType().equals(resolveMappedSuperClazzType(clazz))) {
            return true;
        } else if (clazz.getExtendsType().getClazz() != null && !isEntityClazz(clazz.getExtendsType().getClazz())) {
            return true;
        }
        return false;
    }

    public Type resolveMappedSuperClazzType(Clazz clazz) {
        return null;
    }

    public boolean isEmbeddableClazz(Type type) {
        if (type == null)
            return false;
        if (type.getClazz() != null) {
            return isEmbeddableClazz(type.getClazz());
        } else if (type.getWrappedJavaClass() != null) {
            return type.getWrappedJavaClass().getAnnotation(Embeddable.class) != null;
        } else if (type.getName() != null) {
            try {
                Class<?> aClass = Class.forName(type.getName());
                return aClass.getAnnotation(Embeddable.class) != null;
            } catch (ClassNotFoundException e) {
                return false;
            }
        }
        return false;
    }

    public boolean isEmbeddableClazz(Clazz clazz) {
        if (clazz == null)
            return false;
        return isEmbeddableClazz(clazz.getMetaType());
    }

    public boolean isEmbeddableClazz(MetaClazz metaClazz) {
        if (metaClazz == null)
            return false;
        return metaClazz.hasStereoType(EMBEDDED_CLAZZ);
    }

    /**
     * Default value: "DTYPE"
     */
    public String getDiscriminatorColumnName(Clazz entityClazz) {
        return "DTYPE";
    }

    /**
     * Default value: {@link Clazz#getName()}
     */
    public String getDiscriminatorValue(Clazz entityClazz) {
        return entityClazz.getName();
    }

    /**
     * Default value: {@link InheritanceType#SINGLE_TABLE}<br/>
     * Return <code>null</code> if you don't want to have anything handled by this strategy (an e.g. specify inheritance
     * through the Root Mapped-Super-Class)
     */
    public InheritanceType getInheritanceType(Clazz entityClazz) {
        return InheritanceType.SINGLE_TABLE;
    }

    /**
     * Default value: {@link DiscriminatorType#STRING}
     */
    public DiscriminatorType getDiscriminatorValueType(Clazz entityClazz) {
        return DiscriminatorType.STRING;
    }

    public void applyJpaInheritanceStrategy(Clazz clazz, List<Clazz> directSubClasses, boolean shouldMakeBaseClazzesMappedSuperClassesIfPossible) {
        // Apply inheritance schema if we have subclasses or if we are a base class (adhering to the base/extension class pattern)
        if (directSubClasses.size() > 0) {
            InheritanceType inheritanceType = getInheritanceType(clazz);
            if (inheritanceType != null) {
                clazz.addAnnotations(new Annotation(Inheritance.class, true).addAnnotationAttributes(new AnnotationAttribute("strategy",
                        inheritanceType)));

                if (inheritanceType == InheritanceType.SINGLE_TABLE || inheritanceType == InheritanceType.JOINED) {

                    Annotation discriminatorColumn = new Annotation(DiscriminatorColumn.class, true).addAnnotationAttributes(new AnnotationAttribute("name",
                            getDiscriminatorColumnName(clazz)), new AnnotationAttribute("discriminatorType", getDiscriminatorValueType(clazz)));
                    Integer length = getDiscriminatorLength(clazz);
                    if (length != null) {
                        discriminatorColumn.addAnnotationAttribute("length", length);
                    }
                    clazz.addAnnotations(discriminatorColumn);
                    JpaGeneratorContext.getContext().getJpaProvider().applyProviderSpecificInheritanceStrategy(clazz, directSubClasses, inheritanceType);
                }
            }

        } else if (clazz.isBaseClazz() && shouldMakeBaseClazzesMappedSuperClassesIfPossible) {
            clazz.addAnnotations(new Annotation(MappedSuperclass.class, true));
            clazz.removeAnnotation(Entity.class);
        }
    }

    /**
     * Default: 255
     *
     * @param entityClazz The entity clazz
     * @return The discriminator length
     */
    public Integer getDiscriminatorLength(Clazz entityClazz) {
        return 255;
    }

    public boolean shouldGenerateRequiredConstraintForProperty(Property property) {
        if (property.getOwner().isClazz()) {
            return !isEmbeddableClazz((Clazz) property.getOwner());
        } else {
            return false;
        }

    }

    @Override
    public void applyDiscriminatorValue(Clazz clazz, List<Clazz> subClasses) {
        if (clazz.hasAnnotation(Entity.class)) {
            InheritanceType inheritanceType = getInheritanceType(clazz);
            if (inheritanceType == InheritanceType.SINGLE_TABLE || inheritanceType == InheritanceType.JOINED) {
                boolean extendsFromEntity = Utils.contains(clazz.getAllSuperClazzes(), new Utils.Contains<Clazz>() {
                    @Override
                    public boolean contains(Clazz clazz) {
                        return clazz.hasAnnotation(Entity.class);
                    }
                });
                if (subClasses.size() > 0 || extendsFromEntity) {
                    clazz.addAnnotations(
                            new Annotation(DiscriminatorValue.class, true)
                                    .withDefaultValueAnnotationAttribute(getDiscriminatorValue(clazz))
                    );
                }
            }
        }
        // Do possible JpaProvider specific handling
        JpaGeneratorContext.getContext()
                .getJpaProvider()
                .applyDiscriminatorValue(clazz, subClasses, getDiscriminatorValue(clazz));
    }
}
