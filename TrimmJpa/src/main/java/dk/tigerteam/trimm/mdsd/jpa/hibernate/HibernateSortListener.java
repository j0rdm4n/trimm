/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.hibernate;

import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.generator.event.SimplePropertyEvent;
import dk.tigerteam.trimm.mdsd.jpa.BaseJpaGeneratorEventListener;
import org.hibernate.annotations.Sort;
import org.hibernate.annotations.SortType;

/**
 * Listener that adds {@link Sort} and {@link SortType} for sorted collections of embeddable and
 * simple properties.
 * 
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class HibernateSortListener extends BaseJpaGeneratorEventListener {

	@Override
	protected boolean handleCollectionOfEmbeddableElementsProperty(SimplePropertyEvent event) {
		if (getContext().getCollectionTypeResolver().isSorted(event)) {
			// @org.hibernate.annotations.Sort(
			// type = org.hibernate.annotations.SortType.NATURAL
			// )
			event.getProperty().getField().addAnnotations(new Annotation(Sort.class, true).
					addAnnotationAttribute("type", SortType.NATURAL));
		}
		return true;
	}

	@Override
	protected boolean handleCollectionOfBasicTypesProperty(SimplePropertyEvent event) {
		if (getContext().getCollectionTypeResolver().isSorted(event)) {
			// @org.hibernate.annotations.Sort(
			// type = org.hibernate.annotations.SortType.NATURAL
			// )
			event.getProperty().getField().addAnnotations(new Annotation(Sort.class, true).addAnnotationAttribute("type", SortType.NATURAL));
		}
		return true;
	}

	
}
