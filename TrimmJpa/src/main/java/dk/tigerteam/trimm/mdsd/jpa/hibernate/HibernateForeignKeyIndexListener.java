/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.hibernate;

import dk.tigerteam.trimm.mdsd.java.codedom.Annotation;
import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.jpa.JpaGeneratorContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Table;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;

public class HibernateForeignKeyIndexListener extends AbstractGeneratorEventListener implements DeferredGeneratorEventListener {

	private static final Log logger = LogFactory.getLog(HibernateForeignKeyIndexListener.class);
	private String indexNamePrefix = "IDX_";
	private String indexNameSuffix = "_FK";

	public HibernateForeignKeyIndexListener() {
	}

	public void setIndexNamePrefix(String indexNamePrefix) {
		this.indexNamePrefix = indexNamePrefix;
	}

	public void setIndexNameSuffix(String indexNameSuffix) {
		this.indexNameSuffix = indexNameSuffix;
	}

	private void addForeignKeyIndexAnnotation(Clazz clazz, String foreignKeyColunmName) {
		Annotation hibernateTableAnnotation = clazz.getAnnotation(Table.class);
		Annotation tableAnnotation = null;
		if (hibernateTableAnnotation == null) {
			hibernateTableAnnotation = new Annotation(Table.class, true);
			String tableName = null;
			Clazz currentClazz = clazz;
			if (currentClazz.isBaseClazz()) {
				// The table mapping can be put on the Extension Clazz incase we map the Base class as MappedSuperClass, so let's start looking
				// there
				currentClazz = getContext().getExtensionClazzForBaseClazz(currentClazz);
			}
			while (tableName == null && currentClazz != null && currentClazz.hasAnnotation(Entity.class)) {
				tableAnnotation = currentClazz.getAnnotation(javax.persistence.Table.class);
				if (tableAnnotation != null) {
					tableName = (String) tableAnnotation.getAnnotationAttributeValue("name");
				} else {
					currentClazz = currentClazz.getExtendsType().getClazz();
				}
			}
			if (tableName == null) {
				throw new IllegalStateException("Couldn't resolve the tableName that Entity clazz " + clazz + " is mapped to");
			}
			hibernateTableAnnotation.addAnnotationAttribute("appliesTo", tableName);
		}

		// Add Index annotations
		Annotation indexAnnotation = new Annotation(Index.class, true);
		String foreignKeyIndexName = indexNamePrefix + foreignKeyColunmName.toUpperCase() + indexNameSuffix;
		logger.trace("Adding foreignKeyIndex '" + foreignKeyIndexName + "'");
		indexAnnotation.addAnnotationAttribute("name", foreignKeyIndexName);

		// Apply the index annotations
		hibernateTableAnnotation.addAnnotationAttribute("indexes", indexAnnotation);
		if (clazz.hasAnnotation(Entity.class)) {
			clazz.mergeAnnotations(hibernateTableAnnotation);
		} else if (clazz.isBaseClazz()) {
			Clazz extensionClazz = getContext().getExtensionClazzForBaseClazz(clazz);
			extensionClazz.mergeAnnotations(hibernateTableAnnotation);
		} else {
			throw new IllegalStateException("Don't know where to put the Hibernate Table annotation for clazz " + clazz);
		}
	}

	@Override
	protected boolean handleManyToOneAssociationEvent(ManyToOneAssociationEvent event) {
		Clazz clazz = event.getClazz();
		if (getContext(JpaGeneratorContext.class).getInheritanceStrategy().isEntityClazz(clazz)) {
			Annotation joinColumn = event.getProperty().getField().getAnnotation(JoinColumn.class);
			if (joinColumn != null) {
				String foreignKeyColunmName = (String) joinColumn.getAnnotationAttributeValue("name");
				if (foreignKeyColunmName != null) {
					logger.trace("Adding foreingkey index annotation for '" + event.getProperty().getName() + "' in '"
							+ event.getClazzOrInterface().getName() + "'");
					addForeignKeyIndexAnnotation(clazz, foreignKeyColunmName);
				}
			}
		}
		return true;
	}

	@Override
	protected boolean handleOneToManyAssociationEvent(OneToManyAssociationEvent event) {
		Clazz clazz = event.getClazz();
		if (getContext(JpaGeneratorContext.class).getInheritanceStrategy().isEntityClazz(clazz)) {
			Annotation joinColumn = event.getProperty().getField().getAnnotation(JoinColumn.class);
			if (joinColumn != null) {
				String foreignKeyColunmName = (String) joinColumn.getAnnotationAttributeValue("name");
				if (foreignKeyColunmName != null) {
					logger.trace("Adding foreingkey index annotation for '" + event.getProperty().getName() + "' in '"
							+ event.getClazzOrInterface().getName() + "'");
					addForeignKeyIndexAnnotation(clazz, foreignKeyColunmName);
				}
			}
		}
		return true;
	}

	@Override
	protected boolean handleOneToOneAssociationEvent(OneToOneAssociationEvent event) {
		Clazz clazz = event.getClazz();
		if (getContext(JpaGeneratorContext.class).getInheritanceStrategy().isEntityClazz(clazz)) {
			Annotation joinColumn = event.getProperty().getField().getAnnotation(JoinColumn.class);
			if (joinColumn != null) {
				String foreignKeyColunmName = (String) joinColumn.getAnnotationAttributeValue("name");
				if (foreignKeyColunmName != null) {
					logger.trace("Adding foreingkey index annotation for '" + event.getProperty().getName() + "' in '"
							+ event.getClazzOrInterface().getName() + "'");
					addForeignKeyIndexAnnotation(clazz, foreignKeyColunmName);
				}
			}
		}
		return true;
	}
}
