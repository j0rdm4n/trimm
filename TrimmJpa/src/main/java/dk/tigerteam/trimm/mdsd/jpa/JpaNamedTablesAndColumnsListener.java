/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;

import dk.tigerteam.trimm.mdsd.java.codedom.*;
import dk.tigerteam.trimm.mdsd.java.generator.JavaGeneratorContext;
import dk.tigerteam.trimm.mdsd.java.generator.event.*;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty.CardinalityType;
import dk.tigerteam.trimm.util.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.*;
import java.util.*;

/**
 * Handles everything related to {@link Column}, {@link JoinColumn}, {@link JoinTable}, {@link Table}, etc. naming.
 * <p/>
 * TODO: Change to reuse existing column annotation instead of replacing them (this will allow us to keep custom set annotation attributes,
 * like length, which are applied by other Listeners)
 *
 * @author Jeppe Cramon
 */
public class JpaNamedTablesAndColumnsListener extends AbstractJpaGeneratorEventListener {
  private static final Log log = LogFactory.getLog(JpaNamedTablesAndColumnsListener.class);
  private final Map<String, Clazz> tableNames = new HashMap<String, Clazz>();
  private boolean useNullableForRequiredAssociationsAndProperties = true;
  private List<String> errorMessages = new LinkedList<String>();
  private JpaNamedTablesAndColumnsNameResolver nameResolver;

  public JpaNamedTablesAndColumnsListener() {
    this(new DefaultJpaNamedTablesAndColumnsNameResolver());
  }

  public JpaNamedTablesAndColumnsListener(JpaNamedTablesAndColumnsNameResolver nameResolver) {
    super();
    this.nameResolver = nameResolver;
  }

    public void setNameResolver(JpaNamedTablesAndColumnsNameResolver nameResolver) {
        this.nameResolver = nameResolver;
    }

    /**
   * Get all error messages generated during the processing
   */
  public List<String> getErrorMessages() {
    return errorMessages;
  }

  /**
   * Add an error message which will be displayed during {@link #generationComplete(List)}
   *
   * @param errorMessage The error message
   */
  public void addErrorMessage(String errorMessage) {
    errorMessages.add(errorMessage);
  }

  /**
   * Should we use nullable="false" for simple properties, embedded xToOne properties, One-to-One, One-to-Many and Many-to-One associations
   * which are required (default is true)
   */
  public boolean isUseNullableForRequiredAssociationsAndProperties() {
    return useNullableForRequiredAssociationsAndProperties;
  }

  /**
   * Should we use nullable="false" for simple properties, embedded xToOne properties, One-to-One, One-to-Many and Many-to-One associations
   * which are required (default is true)
   */
  public void setUseNullableForRequiredAssociationsAndProperties(boolean useNullableForRequiredAssociations) {
    this.useNullableForRequiredAssociationsAndProperties = useNullableForRequiredAssociations;
  }

  @Override
  public void generationCommenced() {
    getErrorMessages().clear();
  }

  @Override
  public void generationComplete(List<ClazzOrInterface> allGeneratedClazzes) {
    for (ClazzOrInterface clazzOrInterface : allGeneratedClazzes) {
      if (!clazzOrInterface.isInterface()) {
        Clazz clazz = (Clazz) clazzOrInterface;
        if (isRootEntityClazz(clazz) && getContext().getInheritanceStrategy().getInheritanceType(clazz) != InheritanceType.TABLE_PER_CLASS) {
          validateInheritanceColumnAndJoinColumnNames(clazz);
        }
      }
    }
    validateAllJoinColumnsNames(allGeneratedClazzes);
    if (getErrorMessages().size() > 0) {
      System.err.println(this.getClass().getSimpleName() + " processing resulted in " + getErrorMessages().size() + " errors or warnings:");
      for (String errorMessage : getErrorMessages()) {
        System.err.println(errorMessage);
      }
    }
  }

  protected void validateAllJoinColumnsNames(List<ClazzOrInterface> allGeneratedClazzes) {
    Map<Clazz, Set<Property>> clazzPropertyUseageMap = new HashMap<Clazz, Set<Property>>();
    // Build the map
    for (ClazzOrInterface clazzOrInterface : allGeneratedClazzes) {
      if (!clazzOrInterface.isInterface()) {
        Clazz clazz = (Clazz) clazzOrInterface;
        for (Property property : clazz.getAllPropertiesIncludingSuperClazzProperties()) {
          Clazz propertyType = null;
          if (property.getMetaType().getCardinalityType() == CardinalityType.Many) {
            propertyType = property.getField().getType().getGenerics().get(0).getClazz();
          }
          if (propertyType != null) {
            Set<Property> properties = clazzPropertyUseageMap.get(propertyType);
            if (properties == null) {
              properties = new HashSet<Property>();
              clazzPropertyUseageMap.put(propertyType, properties);
            }
            properties.add(property);
          }
        }
      }
    }

    // Validate all properties with association to the given clazz
    for (Clazz clazz : clazzPropertyUseageMap.keySet()) {
      Map<String, Property> joinColumnNameUsedByProperty = new HashMap<String, Property>();

      for (Property property : clazzPropertyUseageMap.get(clazz)) {
        if (property.getField().hasAnnotation(JoinTable.class)) {
          // Check joinColumns and inverseJoinColumns name for collision (but skip CollectionOfElements which doesn't have an
          // inverseJoinColumn)
          Annotation joinTable = property.getField().getAnnotation(JoinTable.class);
          if (joinTable.hasAnnotationAttribute("inverseJoinColumns")) {
            String joinColumnName = (String) ((Annotation) joinTable.getAnnotationAttributeValue("joinColumns"))
                .getAnnotationAttributeValue("name");
            String inverseJoinColumnName = (String) ((Annotation) joinTable.getAnnotationAttributeValue("inverseJoinColumns"))
                .getAnnotationAttributeValue("name");
            if (joinColumnName.equalsIgnoreCase(inverseJoinColumnName)) {
              addErrorMessage("@JoinTable @JoinColumn name and inverse name collision for Property " + property + " in clazz "
                  + property.getOwner());
            }
          }
        } else if (property.getField().hasAnnotation(JoinColumn.class)) {
          String joinColumnName = (String) property.getField().getAnnotation(JoinColumn.class).getAnnotationAttributeValue("name");
          Property collidesWithProperty = joinColumnNameUsedByProperty.get(joinColumnName.toLowerCase());
          if (collidesWithProperty != null) {
            addErrorMessage("@JoinColumn name collision between Property " + property + " in clazz " + property.getOwner()
                + " and property " + collidesWithProperty + " in clazz " + collidesWithProperty.getOwner() + " - association clazz is "
                + clazz);
          } else {
            joinColumnNameUsedByProperty.put(joinColumnName.toLowerCase(), property);
          }
        }
      }
    }
  }

  protected void validateInheritanceColumnAndJoinColumnNames(Clazz rootEntityClazz) {
    List<Property> allPropertiesInHierarchy = new LinkedList<Property>();
    recursivelyCollectProperties(rootEntityClazz, allPropertiesInHierarchy);
    List<Property> excludedProperties = new LinkedList<Property>();
    for (Property property : allPropertiesInHierarchy) {
      validateInheritanceColumnAndJoinColumnName(property, allPropertiesInHierarchy, excludedProperties);
    }
  }

  @SuppressWarnings("unchecked")
  protected void validateInheritanceColumnAndJoinColumnName(Property property, List<Property> allPropertiesInHierarchy,
                                                            List<Property> excludedProperties) {
    List<Property> problematicColumnProperties = new LinkedList<Property>();
    List<Property> problematicJoinColumnProperties = new LinkedList<Property>();
    if (property.getField().hasAnnotation(Column.class)) {
      problematicColumnProperties = findPropertiesWithSameColumnName(property.getField().getAnnotation(Column.class), property,
          allPropertiesInHierarchy, excludedProperties);
    }
    if (property.getField().hasAnnotation(AttributeOverrides.class)) {
      List<Annotation> attributeOverrides = (List<Annotation>) property.getField().getAnnotation(AttributeOverrides.class)
          .getDefaultValueAnnotationAttributeValues();
      for (Annotation attributeOverride : attributeOverrides) {
        problematicColumnProperties.addAll(findPropertiesWithSameColumnName(
            (Annotation) attributeOverride.getAnnotationAttributeValue("column"), property, allPropertiesInHierarchy, excludedProperties));
      }
    }
    if (property.getField().hasAnnotation(JoinColumn.class) && property.getMetaType().getCardinalityType() != CardinalityType.Many) {
      problematicJoinColumnProperties = findPropertiesWithSameJoinColumnName(property.getField().getAnnotation(JoinColumn.class), property,
          allPropertiesInHierarchy, excludedProperties);
    }
    if (property.getField().hasAnnotation(AssociationOverrides.class)) {
      List<Annotation> associationOverrides = (List<Annotation>) property.getField().getAnnotation(AssociationOverrides.class)
          .getDefaultValueAnnotationAttributeValues();
      for (Annotation associationOverride : associationOverrides) {
        Annotation joinColumn = (Annotation) associationOverride.getAnnotationAttributeValue("joinColumns");
        problematicJoinColumnProperties.addAll(findPropertiesWithSameJoinColumnName(joinColumn, property, allPropertiesInHierarchy,
            excludedProperties));
      }
    }

    if (problematicColumnProperties != null && problematicColumnProperties.size() > 0) {
      for (Property problemProperty : problematicJoinColumnProperties) {
        addErrorMessage("@ColumnName name collision between Property " + property + " in Clazz " + property.getOwner() + " and property "
            + problemProperty + " in Clazz " + problemProperty.getOwner());
        excludedProperties.add(property);
      }
    }
    if (problematicJoinColumnProperties != null && problematicJoinColumnProperties.size() > 0) {
      for (Property problemProperty : problematicJoinColumnProperties) {
        addErrorMessage("@JoinColumn name collision between Property " + property + " in Clazz " + property.getOwner() + " and property "
            + problemProperty + " in Clazz " + problemProperty.getOwner());
        excludedProperties.add(property);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private List<Property> findPropertiesWithSameJoinColumnName(Annotation joinColumn, Property propertyBeingProcessed,
                                                              List<Property> allPropertiesInHierarchy, List<Property> excludedProperties) {
    String joinColumnName = (String) joinColumn.getAnnotationAttributeValue("name");
    List<Property> problematicProperties = new LinkedList<Property>();
    for (Property property : allPropertiesInHierarchy) {
      if (property != propertyBeingProcessed && !excludedProperties.contains(property)) {
        if (property.getField().hasAnnotation(JoinColumn.class)) {
          if (joinColumnName.equalsIgnoreCase((String) property.getField().getAnnotation(JoinColumn.class)
              .getAnnotationAttributeValue("name"))) {
            if (propertyBeingProcessed.getField().getType().equals(property.getField().getType())) {
              if (!joinColumn.equals(property.getField().getAnnotation(JoinColumn.class))) {
                problematicProperties.add(property);
              }
            } else {
              problematicProperties.add(property);
            }
          }
        } else if (property.getField().hasAnnotation(AssociationOverrides.class)) {
          List<Annotation> associationOverrides = (List<Annotation>) property.getField().getAnnotation(AssociationOverrides.class)
              .getDefaultValueAnnotationAttributeValues();
          for (Annotation associationOverride : associationOverrides) {
            Annotation associationOverrideJoinColumn = (Annotation) associationOverride.getAnnotationAttributeValue("joinColumns");
            if (joinColumnName.equalsIgnoreCase((String) associationOverrideJoinColumn.getAnnotationAttributeValue("name"))) {
              if (propertyBeingProcessed.getField().getType().equals(property.getField().getType())) {
                if (!joinColumn.equals(associationOverrideJoinColumn)) {
                  problematicProperties.add(property);
                }
              } else {
                problematicProperties.add(property);
              }
            }
          }
        }
      }
    }
    return problematicProperties;
  }

  @SuppressWarnings("unchecked")
  private List<Property> findPropertiesWithSameColumnName(Annotation column, Property propertyBeingProcessed,
                                                          List<Property> allPropertiesInHierarchy, List<Property> excludedProperties) {
    String columnName = (String) column.getAnnotationAttributeValue("name");
    List<Property> problematicProperties = new LinkedList<Property>();
    for (Property property : allPropertiesInHierarchy) {
      if (property != propertyBeingProcessed && !excludedProperties.contains(property)) {
        if (property.getField().hasAnnotation(Column.class)) {
          if (columnName.equalsIgnoreCase((String) property.getField().getAnnotation(Column.class).getAnnotationAttributeValue("name"))) {
            if (propertyBeingProcessed.getField().getType().equals(property.getField().getType())) {
              if (!column.equals(property.getField().getAnnotation(Column.class))) {
                problematicProperties.add(property);
              }
            } else {
              problematicProperties.add(property);
            }
          }
        } else if (property.getField().hasAnnotation(AttributeOverrides.class)) {
          List<Annotation> attributeOverrides = (List<Annotation>) property.getField().getAnnotation(AttributeOverrides.class)
              .getDefaultValueAnnotationAttributeValues();
          for (Annotation attributeOverride : attributeOverrides) {
            Annotation attributeOverrideColumn = (Annotation) attributeOverride.getAnnotationAttributeValue("column");
            if (columnName.equalsIgnoreCase((String) attributeOverrideColumn.getAnnotationAttributeValue("name"))) {
              if (propertyBeingProcessed.getField().getType().equals(property.getField().getType())) {
                if (!column.equals(attributeOverrideColumn)) {
                  problematicProperties.add(property);
                }
              } else {
                problematicProperties.add(property);
              }
            }
          }
        }
      }
    }
    return problematicProperties;
  }

  private void recursivelyCollectProperties(Clazz clazz, List<Property> allPropertiesInHierarchy) {
    allPropertiesInHierarchy.addAll(clazz.getProperties());
    Collection<Clazz> subClazzes = clazz.getCodeModel().findAllDirectSubClazzesOfClazz(clazz);
    for (Clazz subClazz : subClazzes) {
      recursivelyCollectProperties(subClazz, allPropertiesInHierarchy);
    }
  }

  @Override
  protected boolean handleBasicEmbeddedProperty(SimplePropertyEvent event) {
    return internalHandleSimpleOrToOneEmbeddedProperty(event);
  }

  @Override
  protected boolean handleBasicLobProperty(SimplePropertyEvent event) {
    // from our perspective a Lob is handled as a normal Basic property
    return handleBasicProperty(event);
  }

  @Override
  protected boolean handleManyToOneOfEmbeddableElement(ManyToOneAssociationEvent event) {
    return internalHandleSimpleOrToOneEmbeddedProperty(event);
  }

  @Override
  protected boolean handleBasicProperty(SimplePropertyEvent event) {
    String defaultColumnName = event.resolveNonPluralisedJavaFieldNameForMetaProperty(event.getMetaProperty());

    // Use the Column annotation value if it has been applied at an earlier
    // stage
    if (hasColumnAnnotation(event)) {
      defaultColumnName = getNameAndRemovePreviousColumnAnnotation(event);
    }

    String columnName = nameResolver.resolveColumnName(event, defaultColumnName);
    applyColumnAnnotationToEventProperty(event, columnName);
    return true;
  }

  @Override
  protected boolean handleCollectionOfBasicTypesProperty(SimplePropertyEvent event) {
    return internalHandleCollectionOfBasicOrEnumeratedTypes(event);
  }

  protected boolean internalHandleCollectionOfBasicOrEnumeratedTypes(PropertyEvent event) {
    String nonPluralisedPropertyName = event.resolveNonPluralisedJavaFieldNameForMetaProperty(event.getMetaProperty());

    // Specify a JoinTable name
    String ownerName = event.getProperty().getOwner().getName();
    if (((Clazz) event.getProperty().getOwner()).isBaseClazz()) {
      ownerName = getContext().getExtensionClazzForBaseClazz(((Clazz) event.getProperty().getOwner())).getName();
    }

    String joinTableName = ownerName + "_" + nonPluralisedPropertyName;
    joinTableName = nameResolver.resolveJoinTableName(event, joinTableName);

    // Specify a join column (currently we assume that no JoinColumn has
    // been specified)
    String joinColumn = ownerName + "_id";
    joinColumn = nameResolver.resolveJoinColumnName(event, joinColumn);

    applyJoinTableToEventProperty(event, joinTableName, joinColumn);

    // Specify a column
    // TODO: Hibernate 4/4.1, maybe Jpa 2 does not allow @Column on @ElementCollection
    String columnName = nameResolver.resolveColumnName(event, nonPluralisedPropertyName);
    applyColumnAnnotationToEventProperty(event, columnName);

    // Fix OrderColumn name abbreviations (if they're applied)
    if (event.getProperty().getField().hasAnnotation(OrderColumn.class)) {
      Annotation indexColumnAnnotation = event.getProperty().getField().getAnnotation(OrderColumn.class);
      String indexColumnName = (String) indexColumnAnnotation.getAnnotationAttributeValue("name");
      indexColumnAnnotation.getAnnotationAttribute("name").setValues(nameResolver.resolveIndexColumnName(event, indexColumnName));
    }

    return true;
  }

  @Override
  protected boolean handleOneToManyOfEmbeddableElements(OneToManyAssociationEvent event) {
    return internalHandleToManyOrCollectionOfEmbeddableElements(event);
  }

  protected boolean internalHandleToManyOrCollectionOfEmbeddableElements(final PropertyEvent event) {
    String nonPluralisedPropertyName = event.resolveNonPluralisedJavaFieldNameForMetaProperty(event.getMetaProperty());

    // Specify a JoinTable name
    String ownerName = event.getProperty().getOwner().getName();
    if (((Clazz) event.getProperty().getOwner()).isBaseClazz()) {
      ownerName = getContext().getExtensionClazzForBaseClazz(((Clazz) event.getProperty().getOwner())).getName();
    }

    String joinTableName = ownerName + "_" + nonPluralisedPropertyName;
    joinTableName = nameResolver.resolveJoinTableName(event, joinTableName);

    // Specify a join column (currently we assume that no JoinColumn has
    // been specified)
    String joinColumn = ownerName + "Id";
    joinColumn = nameResolver.resolveJoinColumnName(event, joinColumn);

    applyJoinTableToEventProperty(event, joinTableName, joinColumn);

    // Fix OrderColumn name abbreviations (if they're applied)
    if (event.getProperty().getField().hasAnnotation(OrderColumn.class)) {
      Annotation indexColumnAnnotation = event.getProperty().getField().getAnnotation(OrderColumn.class);
      String indexColumnName = (String) indexColumnAnnotation.getAnnotationAttributeValue("name");
      indexColumnAnnotation.getAnnotationAttribute("name").setValues(nameResolver.resolveIndexColumnName(event, indexColumnName));
    }

    // Handle Attribute override

    // @AttributeOverrides( {
    // @AttributeOverride(name = "element.street", column =
    // @Column(name="HOME_STREET") ),
    // @AttributeOverride(name = "element.zipcode", column =
    // @Column(name="HOME_ZIPCODE") ),
    // @AttributeOverride(name = "element.city", column =
    // @Column(name="HOME_CITY") )
    // })

    if (event.getProperty().getField().getType().getGenerics().get(0).getClazz() == null) {
      log.info("Ignoring AttributeOverride for property " + event.getMetaProperty() + " since its type is defined externally");
      return true;
    }
    JavaGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {
      public void execute() {
        Annotation attributeOverrides = new Annotation(AttributeOverrides.class);
        List<Annotation> attributeOverrideAnnotations = new LinkedList<Annotation>();
        for (Property property : event.getProperty().getField().getType().getGenerics().get(0).getClazz()
            .getAllPropertiesIncludingSuperClazzProperties()) {
          recursivelyGenerateAttributeOverrideForProperty(property, "element", event.getProperty().getName(), attributeOverrideAnnotations,
              event);
        }

        attributeOverrides.addAnnotationAttributes(AnnotationAttribute.defaultValue(attributeOverrideAnnotations));
        event.getProperty().getField().addAnnotations(attributeOverrides);
      }

      public String getDescription() {
        return "AnnotationAttributes for property " + event.getMetaProperty() + " in clazz " + event.getClazz();
      }
    });
    return true;
  }

  protected void recursivelyGenerateAttributeOverrideForProperty(Property property, String attributeOverrideNamePrefix,
                                                                 String columnNamePrefix, List<Annotation> attributeOverrideAnnotations, PropertyEvent event) {
    Clazz propertyClazzType = property.getField().getType().getClazz();
    String attributeOverrideColumnName = columnNamePrefix + "_" + property.getName();
    if (isEmbeddableClazz(propertyClazzType)) {
      for (Property subProperty : propertyClazzType.getAllPropertiesIncludingSuperClazzProperties()) {
        String namePrefix = attributeOverrideNamePrefix;
        if (property != event.getProperty()) {
          if (namePrefix.length() > 0) {
            namePrefix += ".";
          }
          namePrefix += property.getField().getName();
        }
        recursivelyGenerateAttributeOverrideForProperty(subProperty, namePrefix, attributeOverrideColumnName, attributeOverrideAnnotations,
            event);
      }
    } else {
      if (property.getMetaType().isPartInAnAssociation() && !isEnumerationClazz(propertyClazzType)) {
        // We don't create AsstributeOverride for association (we do AssociationOverride for that)
        return;
      }
      String name = attributeOverrideNamePrefix;
      if (name.length() > 0) {
        name += ".";
      }
      name += property.getField().getName();

      attributeOverrideColumnName = nameResolver.resolveAttributeOverrideColumnName(event, property, name, attributeOverrideColumnName);

      // Reuse if possible (so we can reuse setup like scale, nullable, etc.)
      boolean nullableSafeInheritanceScheme = !(getContext().getInheritanceStrategy().isEntitySubClazz(event.getClazz()) && getContext()
          .getInheritanceStrategy().getInheritanceType(event.getClazz()) == InheritanceType.SINGLE_TABLE);
      Annotation columnAnnotation = property.getField().getAnnotation(Column.class) != null ? property.getField()
          .getAnnotation(Column.class).clone() : null;
      if (columnAnnotation != null) {
        // lets remove the name attribute, so we don't have two values....
        columnAnnotation.removeAnnotationAttribute("name");
        // Make sure that we don't include a nullable if the embedded entity is used in an entity subclass
        if (!nullableSafeInheritanceScheme || event.getMetaProperty().getCardinalityType() == CardinalityType.SingleOptional
            && shouldGenerateRequiredConstraintForProperty(event.getProperty())) {
          columnAnnotation.removeAnnotationAttribute("nullable");
        }
      } else {
        // We couldn't reuse the old one, lets create a new one
        columnAnnotation = new Annotation(Column.class, true);
      }

      // Apply nullable if it's needed
      if (nullableSafeInheritanceScheme && event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired
          && property.getMetaType().getCardinalityType() == CardinalityType.SingleRequired
          && shouldGenerateRequiredConstraintForProperty(event.getProperty())) {
        if (!columnAnnotation.hasAnnotationAttribute("nullable")) {
          columnAnnotation.addAnnotationAttribute("nullable", false);
        }
      }

      Annotation attributeOverride = new Annotation(AttributeOverride.class, true).addAnnotationAttribute("name", name).addAnnotationAttribute(
          "column", columnAnnotation.addAnnotationAttribute("name", attributeOverrideColumnName));
      attributeOverrideAnnotations.add(attributeOverride);
    }
  }

  @Override
  protected boolean handleCollectionOfEmbeddableElementsProperty(SimplePropertyEvent event) {
    return internalHandleToManyOrCollectionOfEmbeddableElements(event);
  }

  @Override
  protected boolean handleOneToManyOfEnumeratedElements(OneToManyAssociationEvent event) {
    return internalHandleCollectionOfBasicOrEnumeratedTypes(event);
  }

  @Override
  protected boolean handleEmbeddableClazzEvent(ClazzEvent event) {
    // There's no special handling for an Embeddable clazz since it's column
    // mappings are performed when used
    return true;
  }

  @Override
  protected boolean handleOneToOneOfEmbeddableElement(OneToOneAssociationEvent event) {
    return internalHandleSimpleOrToOneEmbeddedProperty(event);
  }

  protected boolean internalHandleSimpleOrToOneEmbeddedProperty(final PropertyEvent event) {
    // Check if we own the type referenced (or if it's an external class, in which case we can't perform the override for sub annotations)
    if (event.getProperty().getField().getType().getClazz() == null) {
      log.info("Not processing Attribute overrides for Property " + event.getMetaProperty() + " since its type is defined externally");
      return true;
    }

    JavaGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {

      public void execute() {
        Annotation attributeOverrides = new Annotation(AttributeOverrides.class, true);
        List<Annotation> subOverrideAnnotations = new LinkedList<Annotation>();
        for (Property property : event.getProperty().getField().getType().getClazz().getAllPropertiesIncludingSuperClazzProperties()) {          /*
					 * String attributeOverrideColumnName = event.getProperty().getName() + "_" + property.getName(); attributeOverrideColumnName =
					 * resolveAttributeOverrideColumnName(event, property, attributeOverrideColumnName);
					 * 
					 * Annotation attributeOverride = new Annotation(AttributeOverride.class) .addAnnotationAttribute("name", "element." +
					 * property.getName()) .addAnnotationAttribute("column", new Annotation(Column.class) .addAnnotationAttribute("name",
					 * attributeOverrideColumnName) ); subOverrideAnnotations.add(attributeOverride);
					 */
          recursivelyGenerateAttributeOverrideForProperty(property, "", event.getProperty().getName(), subOverrideAnnotations, event);
        }

        attributeOverrides.addAnnotationAttributes(AnnotationAttribute.defaultValue(subOverrideAnnotations));
        event.getProperty().getField().addAnnotations(attributeOverrides);
      }

      public String getDescription() {
        return "AnnotationAttributes for property " + event.getMetaProperty() + " in clazz " + event.getClazz();
      }

    });


    // Handle possible association overrides
    JavaGeneratorContext.getContext().queueDeferredEventHandler(new DeferredEventHandler() {

      public void execute() {

        List<Annotation> subOverrideAnnotations = new LinkedList<Annotation>();
        for (Property property : event.getProperty().getField().getType().getClazz().getAllPropertiesIncludingSuperClazzProperties()) {
          if (isEntityClazz(property.getField().getType().getClazz())) {
            // Handle AssociationOverride,
            // TODO: Consider handling override recursively (in case
            // Hibernate supports this and IF we're going to need it)
            // @AssociationOverrides({
            // @AssociationOverride(name="valuta",
            // joinColumns=@javax.persistence.JoinColumn(name =
            // "indfrielseOmkostninger_valutaId"))
            // })
            Annotation associationOverride = new Annotation(AssociationOverride.class, true);
            associationOverride.addAnnotationAttribute("name", property.getField().getName());

            String defaultColumnName = event.resolveNonPluralisedJavaFieldNameForMetaProperty(event.getMetaProperty());
            defaultColumnName += "_" + property.getField().getName() + "Id";
            String joinColumnName = nameResolver.resolveJoinColumnName(event, defaultColumnName);

            Annotation joinColumnAnnotation = new Annotation(JoinColumn.class);
            boolean nullableSafeInheritanceScheme = !(getContext().getInheritanceStrategy().isEntitySubClazz(event.getClazz()) && getContext()
                .getInheritanceStrategy().getInheritanceType(event.getClazz()) == InheritanceType.SINGLE_TABLE);
            if (useNullableForRequiredAssociationsAndProperties
                && event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired && nullableSafeInheritanceScheme
                && shouldGenerateRequiredConstraintForProperty(event.getProperty())) {
              joinColumnAnnotation.addAnnotationAttribute("nullable", false);
            }

            associationOverride.addAnnotationAttribute("joinColumns", joinColumnAnnotation.addAnnotationAttributes("name", joinColumnName));
            subOverrideAnnotations.add(associationOverride);
          }
        }

        if (subOverrideAnnotations.size() > 0) {
          Annotation associationOverrides = new Annotation(AssociationOverrides.class, true);
          associationOverrides.addAnnotationAttributes(AnnotationAttribute.defaultValue(subOverrideAnnotations));
          event.getProperty().getField().addAnnotations(associationOverrides);
        }
      }

      public String getDescription() {
        return "AssociationOverrides for property " + event.getMetaProperty() + " in clazz " + event.getClazz();
      }

    });

    return true;
  }

  @Override
  protected boolean handleBasicEnumerationProperty(SimplePropertyEvent event) {
    String defaultColumnName = event.resolveNonPluralisedJavaFieldNameForMetaProperty(event.getMetaProperty());

    // Use the Column annotation value if it has been applied at an earlier
    // stage
    if (hasColumnAnnotation(event)) {
      defaultColumnName = getNameAndRemovePreviousColumnAnnotation(event);
    }

    String columnName = nameResolver.resolveColumnName(event, defaultColumnName);
    applyColumnAnnotationToEventProperty(event, columnName);
    return true;
  }

  @Override
  protected boolean handleEntityClazzEvent(ClazzEvent event) {
    boolean shouldGetTableMapping = false;
    if (event.getClazz().isExtensionClazz()) {
      Clazz baseClazz = event.getClazz().getExtendsType().getClazz();
      if (!baseClazz.hasAnnotation(Entity.class)) {
        shouldGetTableMapping = true;
      } else {
        shouldGetTableMapping = false;
      }
    } else if ((event.getClazz().getExtendsType() == null || extendsMappedSuperClass(event.getClazz()) || getContext().getInheritanceStrategy().getInheritanceType(event.getClazz()) != InheritanceType.SINGLE_TABLE)
        && event.getClazz().hasAnnotation(Entity.class)) {
      // Comment: We'll allow for base clazzes, since Hibernates Indexes needs an appliesToTable
      shouldGetTableMapping = true;
    }
    if (shouldGetTableMapping) {
      String defaultTableName = event.getClazz().getName();
      if (event.getClazz().isBaseClazz()) {
        defaultTableName = getContext().getExtensionClazzForBaseClazz(event.getClazz()).getName();
      }

      // Use the Table annotation value if it has been applied at an earlier
      // stage
      if (event.getClazz().hasAnnotation(Table.class)) {
        defaultTableName = (String) event.getClazz().getAndRemoveAnnotation(Table.class).getAnnotationAttributeValues("name").get(0);
      }

      String tableName = nameResolver.resolveTableName(event, defaultTableName);

      tableNames.put(tableName, event.getClazz());
      event.getClazz().addAnnotations(new Annotation(Table.class, true).addAnnotationAttributes("name", tableName));
      if (JpaGeneratorContext.getContext().getSchema() != null) {
        event.getClazz().getAnnotation(Table.class).addAnnotationAttribute("schema", JpaGeneratorContext.getContext().getSchema());
      }
      if (JpaGeneratorContext.getContext().getCatalog() != null) {
        event.getClazz().getAnnotation(Table.class).addAnnotationAttribute("catalog", JpaGeneratorContext.getContext().getCatalog());
      }

    }
    return true;
  }

  @Override
  protected boolean handleOneToOneOfEnumerationElement(OneToOneAssociationEvent event) {
    String defaultColumnName = event.resolveNonPluralisedJavaFieldNameForMetaProperty(event.getMetaProperty());
    // Use the Column annotation value if it has been applied at an earlier
    // stage
    if (hasColumnAnnotation(event)) {
      defaultColumnName = getNameAndRemovePreviousColumnAnnotation(event);
    }

    String abbreviatedName = nameResolver.resolveColumnName(event, defaultColumnName);
    // event.getProperty().getField().addAnnotations(
    // new Annotation(Column.class).addAnnotationAttributes("name",
    // abbreviatedName));
    applyColumnAnnotationToEventProperty(event, abbreviatedName);
    return true;
  }

  @Override
  protected boolean handleManyToManyOwnedByAssociation(ManyToManyAssociationEvent event) {
    // Do nothing, as everything is handled in the owning side of the ManyToMany
    // association
    return true;
  }

  @Override
  protected boolean handleManyToManyOwnerOfAssociation(ManyToManyAssociationEvent event) {
    return internalHandleToManyOwnerAssociation(event);
  }

  @Override
  protected boolean handleManyToOneOwnerOfAssociation(ManyToOneAssociationEvent event) {
    return internalHandleToOneOwnerOfAssociation(event);
  }

  private boolean internalHandleToOneOwnerOfAssociation(AssociationEvent event) {
    String defaultColumnName = event.resolveNonPluralisedJavaFieldNameForMetaProperty(event.getMetaProperty());
    defaultColumnName = defaultColumnName + "Id";

    // Use the Column annotation value if it has been applied at an earlier
    // stage
    if (hasColumnAnnotation(event)) {
      defaultColumnName = getCurrentJoinColumnName(event);
    }

    String abbreviatedName = nameResolver.resolveJoinColumnName(event, defaultColumnName);

    Annotation joinColumnAnnotation = new Annotation(JoinColumn.class, true);
    boolean nullableSafeInheritanceScheme = !(getContext().getInheritanceStrategy().isEntitySubClazz(event.getClazz()) && getContext()
        .getInheritanceStrategy().getInheritanceType(event.getClazz()) == InheritanceType.SINGLE_TABLE);
    if (useNullableForRequiredAssociationsAndProperties && event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired
        && nullableSafeInheritanceScheme && shouldGenerateRequiredConstraintForProperty(event.getProperty())) {
      joinColumnAnnotation.addAnnotationAttribute("nullable", false);
    }
    event.getProperty().getField().mergeAnnotations(joinColumnAnnotation.addAnnotationAttributes("name", abbreviatedName));
    return true;
  }

  @Override
  protected boolean handleOneToManyOwnedByAssociation(OneToManyAssociationEvent event) {
    if (getContext().getCollectionTypeResolver().isOrdered(event) && event.getMetaProperty().getAssociation().isBidirectional()) {
      Annotation joinColumnAnnotation = new Annotation(JoinColumn.class, true);
      boolean nullableSafeInheritanceScheme = !(getContext().getInheritanceStrategy().isEntitySubClazz(event.getClazz()) && getContext()
          .getInheritanceStrategy().getInheritanceType(event.getClazz()) == InheritanceType.SINGLE_TABLE);
      if (useNullableForRequiredAssociationsAndProperties && event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired
          && nullableSafeInheritanceScheme && shouldGenerateRequiredConstraintForProperty(event.getProperty())) {
        joinColumnAnnotation.addAnnotationAttribute("nullable", false);
      }
      String joinColumn = resolveNonPluralisedJoinColumnNameForMetaProperty(event.getProperty().getMetaType().getAssociation()
          .getOppositeProperty(event.getProperty().getMetaType()))
          + "_" + resolveNonPluralisedJoinColumnNameForMetaProperty(event.getProperty().getMetaType()) + "Id";
      joinColumn = nameResolver.resolveJoinColumnName(event, joinColumn);

      joinColumnAnnotation.addAnnotationAttribute("name", joinColumn);

      event.getProperty().getField().mergeAnnotations(joinColumnAnnotation);
    }
    // Do nothing as everything is specified on the owning ManyToOne side
    return true;
  }

  @Override
  protected boolean handleOneToManyOwnerOfAssociation(OneToManyAssociationEvent event) {
    if (!event.getMetaProperty().getAssociation().isBidirectional() && !shouldUseJoinTableForUnidirectionalOneToMany(event)) {
      // Specify a join column (currently we assume that no JoinColumn has
      // been specified)
      // String ownerName = event.getProperty().getOwner().getName();
      String joinColumn = resolveNonPluralisedJoinColumnNameForMetaProperty(event.getProperty().getMetaType().getAssociation()
          .getOppositeProperty(event.getProperty().getMetaType()))
          + "_" + resolveNonPluralisedJoinColumnNameForMetaProperty(event.getProperty().getMetaType()) + "Id";
      joinColumn = nameResolver.resolveJoinColumnName(event, joinColumn);

      Annotation joinColumnAnnotation = new Annotation(JoinColumn.class, true);
      boolean nullableSafeInheritanceScheme = !(getContext().getInheritanceStrategy().isEntitySubClazz(event.getClazz()) && getContext()
          .getInheritanceStrategy().getInheritanceType(event.getClazz()) == InheritanceType.SINGLE_TABLE);
      if (useNullableForRequiredAssociationsAndProperties && event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired
          && nullableSafeInheritanceScheme && shouldGenerateRequiredConstraintForProperty(event.getProperty())) {
        joinColumnAnnotation.addAnnotationAttribute("nullable", false);
      }
      event.getProperty().getField().addAnnotations(joinColumnAnnotation.addAnnotationAttribute("name", joinColumn));
      return true;
    } else {
      return internalHandleToManyOwnerAssociation(event);
    }
  }

  /**
   * Resolves the name of the JoinColumn name from the name of the MetaProperty parameter.<br/>
   * This method doesn't concern it self with the {@link MetaProperty#getCardinalityType()} value
   * <p/>
   * If the {@link MetaProperty} has a <b>name</b>, then this is used.<br/>
   * If the name is missing, the metaProperties <b>type</b>'s name is used instead.<br/>
   * Note: To ensure proper java code, this method ensures that the first letter of the name is uncapitalized.
   *
   * @param metaProperty The {@link MetaProperty} to resolve a Java field name for
   * @return The Java field name for the {@link MetaProperty} parameter
   */
  protected String resolveNonPluralisedJoinColumnNameForMetaProperty(MetaProperty metaProperty) {
    String propertyName = metaProperty.getName();
    if (propertyName != null && propertyName.trim().length() > 0) {
      propertyName = Utils.uncapitalize(propertyName);
    } else {
      propertyName = Utils.uncapitalize(metaProperty.getType().getName());
    }
    return propertyName;
  }

  /**
   * Specifies if a {@link JoinTable} should be used for a given unidirectional One-To-Many association.<br/>
   * Default is <b>true</b>
   */
  protected boolean shouldUseJoinTableForUnidirectionalOneToMany(OneToManyAssociationEvent event) {
    return event.getMetaProperty().getAssociation().getOppositeProperty(event.getMetaProperty()).getCardinalityType() == CardinalityType.SingleOptional;
  }

  @Override
  protected boolean handleOneToOneOwnedByAssociation(OneToOneAssociationEvent event) {
    // Do nothing as everything is handled on the owning side
    return true;
  }

  @Override
  protected boolean handleOneToOneOwnerOfAssociation(OneToOneAssociationEvent event) {
    return internalHandleToOneOwnerOfAssociation(event);
  }

  /**
   * Helper method to check if {@link Column} annotation has previously applied and therefore should be used before any abbreviations
   *
   */
  protected final boolean hasColumnAnnotation(PropertyEvent event) {
    return event.getProperty().getField().hasAnnotation(Column.class);
  }

  /**
   * Helper method to get the name of any previously applied {@link Column} annotation
   *
   * @see #hasColumnAnnotation(dk.tigerteam.trimm.mdsd.java.generator.event.PropertyEvent) as guard method for this method to work
   */
  protected final String getNameAndRemovePreviousColumnAnnotation(PropertyEvent event) {
    String defaultColumnName = null;
    Annotation column = event.getProperty().getField().getAndRemoveAnnotation(Column.class);
    if (column != null) {
      defaultColumnName = (String) column.getAnnotationAttributeValues("name").get(0);
    }
    return defaultColumnName;
  }

  /**
   * Helper method to check if {@link Column} annotation has previously applied and therefore should be used before any abbreviations
   *
   */
  protected final boolean hasJoinColumnAnnotation(PropertyEvent event) {
    return event.getProperty().getField().hasAnnotation(JoinColumn.class);
  }

  /**
   * Helper method to get the name of any previously applied {@link Column} annotation
   *
   * @see #hasColumnAnnotation(PropertyEvent) as guard method for this method to work
   */
  protected final String getCurrentJoinColumnName(PropertyEvent event) {
    String defaultColumnName = null;
    Annotation column = event.getProperty().getField().getAnnotation(JoinColumn.class);
    if (column != null) {
      defaultColumnName = (String) column.getAnnotationAttributeValues("name").get(0);
    }
    return defaultColumnName;
  }

  /**
   * Handles {@link ManyToManyAssociationEvent} and {@link OneToManyAssociationEvent} where the property that's being handled is the owner
   * of the association
   *
   */
  protected boolean internalHandleToManyOwnerAssociation(AssociationEvent event) {
    // Specify a JoinTable name
    String nonPluralisedPropertyName = event.resolveNonPluralisedJavaFieldNameForMetaProperty(event.getMetaProperty());

    String ownerName = event.getProperty().getOwner().getName();
    if (((Clazz) event.getProperty().getOwner()).isBaseClazz()) {
      ownerName = getContext().getExtensionClazzForBaseClazz(((Clazz) event.getProperty().getOwner())).getName();
    }

    String genericsType = event.getProperty().getField().getType().getGenerics().get(0).getClazz().getName();
    String joinTableName = ownerName + "_" + nonPluralisedPropertyName;
    joinTableName = nameResolver.resolveJoinTableName(event, joinTableName);

    // Specify a join column (currently we assume that no JoinColumn has
    // been specified)
    String joinColumn = ownerName + "Id";
    joinColumn = nameResolver.resolveJoinColumnName(event, joinColumn);

    // Specify an inverse join column (currently we assume that no
    // JoinColumn has been specified)
    String defaultColumnName = event.resolveNonPluralisedJavaFieldNameForMetaProperty(event.getMetaProperty());
    String inverseJoinColumn = defaultColumnName + "Id";
    if (inverseJoinColumn.equalsIgnoreCase(joinColumn)) {
      // We've hit a case where the Name of the Owner is the same as the name of
      // the property to which we're creating an association:
      // Example:
      // @javax.persistence.Entity
      // public class SaerligBestemmelse extends AbstractEntity {
      // @javax.persistence.OneToMany(cascade = {
      // javax.persistence.CascadeType.MERGE,
      // javax.persistence.CascadeType.PERSIST}
      // )
      // @javax.persistence.JoinTable(name = "SaerligBestemmelse_Frase",
      // joinColumns = @javax.persistence.JoinColumn(name =
      // "SaerligBestemmelse_id")
      // , inverseJoinColumns = @javax.persistence.JoinColumn(name =
      // "saerligBestemmelse_id")
      // )
      // private java.util.Set<Frase>
      // saerligBestemmelseCollection =
      // new java.util.HashSet<Frase>();

      // ... So in this case we will preappend the type name of the properties
      // type
      inverseJoinColumn = genericsType + "_" + inverseJoinColumn;
    }
    inverseJoinColumn = nameResolver.resolveInverseJoinColumnName(event, inverseJoinColumn);

    applyJoinTableToEventProperty(event, joinTableName, joinColumn, inverseJoinColumn);
    return true;
  }

  /**
   * Apply a {@link JoinTable} to the {@link Field} designated by the {@link PropertyEvent}.<br/>
   * Calls {@link #applyJoinTableToEventProperty(PropertyEvent, String, String, String)} without an inverseJoinColumnName.
   *
   * @param event          The event to process
   * @param joinTableName  The name of the {@link JoinTable}
   * @param joinColumnName The {@link JoinColumn} name
   */
  protected void applyJoinTableToEventProperty(PropertyEvent event, String joinTableName, String joinColumnName) {
    applyJoinTableToEventProperty(event, joinTableName, joinColumnName, null);
  }

  /**
   * Apply a {@link JoinTable} to the {@link Field} designated by the {@link PropertyEvent}.
   *
   * @param event                 The event to process
   * @param joinTableName         The name of the {@link JoinTable}
   * @param joinColumnName        The {@link JoinColumn} name
   * @param inverseJoinColumnName The Inverse {@link JoinColumn} name
   */
  protected void applyJoinTableToEventProperty(PropertyEvent event, String joinTableName, String joinColumnName,
                                               String inverseJoinColumnName) {
    Annotation joinTableAnnotation = new Annotation(JoinTable.class, true).addAnnotationAttribute("name", joinTableName);

    if (joinColumnName != null) {
      joinTableAnnotation.addAnnotationAttribute("joinColumns",
          new Annotation(JoinColumn.class, true).addAnnotationAttribute("name", joinColumnName));
    }
    if (inverseJoinColumnName != null) {
      joinTableAnnotation.addAnnotationAttribute("inverseJoinColumns",
          new Annotation(JoinColumn.class, true).addAnnotationAttribute("name", inverseJoinColumnName));
    }

    event.getProperty().getField().addAnnotations(joinTableAnnotation);
  }

  /**
   * Apply a {@link Column} annotation to the {@link Field} designated by the {@link Property} in the {@link PropertyEvent}.
   *
   * @param event      The event to process
   * @param columnName The {@link Column} name
   */
  protected void applyColumnAnnotationToEventProperty(PropertyEvent event, String columnName) {
    Annotation annotation = new Annotation(Column.class, true).addAnnotationAttributes("name", columnName);
    Long upperBound = ((MetaProperty) event.getProperty().getField().getMetaType()).getUpperBound();
    if (upperBound != null) {
      annotation.addAnnotationAttribute("length", upperBound);
    }
    boolean nullableSafeInheritanceScheme = !(getContext().getInheritanceStrategy().isEntitySubClazz(event.getClazz()) && getContext()
        .getInheritanceStrategy().getInheritanceType(event.getClazz()) == InheritanceType.SINGLE_TABLE);
    if (useNullableForRequiredAssociationsAndProperties && event.getMetaProperty().getCardinalityType() == CardinalityType.SingleRequired
        && nullableSafeInheritanceScheme && shouldGenerateRequiredConstraintForProperty(event.getProperty())) {
      annotation.addAnnotationAttribute("nullable", false);
    }
    event.getProperty().getField().addAnnotations(annotation);
  }

  // ---------------------- resolver methods --------------------------
  public static class DefaultJpaNamedTablesAndColumnsNameResolver implements JpaNamedTablesAndColumnsNameResolver {
    public String resolveJoinColumnName(PropertyEvent event, String defaultJoinColumnName) {
      return defaultJoinColumnName;
    }

    public String resolveColumnName(PropertyEvent event, String defaultColumnName) {
      return defaultColumnName;
    }

    public String resolveTableName(ClazzEvent event, String defaultTableName) {
      return defaultTableName;
    }

    public String resolveJoinTableName(PropertyEvent event, String defaultJoinTableName) {
      return defaultJoinTableName;
    }

    public String resolveInverseJoinColumnName(AssociationEvent event, String defaultInverseJoinColumnName) {
      return defaultInverseJoinColumnName;
    }

    public String resolveAttributeOverrideColumnName(PropertyEvent event, Property overrideAttributeProperty,
                                                     String overrideAttributeName, String defaultAttributeOverrideColumnName) {
      return defaultAttributeOverrideColumnName;
    }

    public String resolveIndexColumnName(PropertyEvent event, String defaultIndexColumnName) {
      return defaultIndexColumnName;
    }
  }
}
