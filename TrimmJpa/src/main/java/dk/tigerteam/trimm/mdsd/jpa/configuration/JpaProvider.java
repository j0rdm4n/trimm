/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.configuration;

import dk.tigerteam.trimm.mdsd.java.codedom.Clazz;
import dk.tigerteam.trimm.mdsd.java.generator.event.SimplePropertyEvent;

import javax.persistence.InheritanceType;
import javax.persistence.TemporalType;
import java.util.List;
import java.util.Map;

/**
 * Jpa provider which contains provider specific extensions to the Jpa codegenerator
 * @see dk.tigerteam.trimm.mdsd.jpa.hibernate.HibernateProvider
 * @see dk.tigerteam.trimm.mdsd.jpa.eclipselink.EclipseLinkProvider
 * @see dk.tigerteam.trimm.mdsd.jpa.openjpa.OpenJpaProvider
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public interface JpaProvider {
  /**
   * Apply Type convertion (if necessary) by looking up the TypeConverter mapping in
   * the provided Mapping map (each JpaProvider will specify its individual key under which the Jpa mapping is defined)
   * @param event The generator event that caused the Type conversion to be activated
   * @param mapping Contains the mappings that are specific for the given provider - please check the provider Java doc
   */
  void applyTypeConverter(SimplePropertyEvent event, Map<String, String> mapping);

  /**
   * Apply Provider specific Inheritance strategy code
   * @param entityClazz The entity being mapped
   * @param directSubClasses Its direct known subclasses
   * @param inheritanceType The form of JPA inheritance that we should map to
   */
  void applyProviderSpecificInheritanceStrategy(Clazz entityClazz, List<Clazz> directSubClasses, InheritanceType inheritanceType);

  /**
   * Get the {@link javax.persistence.TemporalType} that should be applied to the {@link javax.persistence.Temporal} annotation for
   * a {@link java.util.Date} field (some JPA providers insist on {@link javax.persistence.TemporalType#TIMESTAMP no matter the field type}
   */
  TemporalType getDateFieldTemporalType();

    /**
     * Handle adding additional {@literal @DiscriminatorValue} annotations to
     * Entities (and for some frameworks also to MappedSuperClasses).
     * When this method is called, the {@link dk.tigerteam.trimm.mdsd.jpa.JpaInheritanceAndClazzesStrategy}
     * has done the standard JPA 2.0 handling, so you only need to handle Provider specific <b>odd</b> handling
     * here
     * @param clazz The entity (mapped superclass or real entity class) that's being mapped
     * @param subClasses All its known subclasses
     * @param defaultDiscriminatorValue The default value for the discriminator value for this clazz (as determined
     *                                  by the active {@link dk.tigerteam.trimm.mdsd.jpa.JpaInheritanceAndClazzesStrategy}
     */
    void applyDiscriminatorValue(Clazz clazz, List<Clazz> subClasses, String defaultDiscriminatorValue);
}
