/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import dk.tigerteam.trimm.mdsd.meta.MetaClazz
import dk.tigerteam.trimm.mdsd.meta.MetaStereoType
import dk.tigerteam.trimm.mdsd.meta.MetaProperty

metaModel.each {  metaType ->
    // Swap Alias and Name in case there's an alias defined for a MetaType
	if (metaType.alias != null) {
		def old = metaType.name
		metaType.name = metaType.alias
		metaType.alias = old
	}
    // Add a special property to the consultation class
    if (metaType.name == "Consultation") {
        def prop = new MetaProperty()
        prop.name = "mySpecialProperty"
        prop.type = new MetaClazz(String.class);
        prop.add(new MetaStereoType('Transient'))
        metaType.add prop
    }
}
