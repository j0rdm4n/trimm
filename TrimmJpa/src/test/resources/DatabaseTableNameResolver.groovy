/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import dk.tigerteam.trimm.mdsd.java.codedom.Property
import dk.tigerteam.trimm.mdsd.java.generator.event.AssociationEvent
import dk.tigerteam.trimm.mdsd.java.generator.event.ClazzEvent
import dk.tigerteam.trimm.mdsd.java.generator.event.PropertyEvent
import dk.tigerteam.trimm.mdsd.jpa.JpaNamedTablesAndColumnsNameResolver

/**
 * Example JpaNamedTablesAndColumnsNameResolver script which makes all generated names
 * compatible with Oracle (e.g. no names over 30 characters) and some protected keywords renamed
 */
public class MDDExampleNameResolver implements JpaNamedTablesAndColumnsNameResolver {
	public String resolveJoinColumnName(PropertyEvent event, String defaultJoinColumnName) {
		return defaultJoinColumnName.substring(0, defaultJoinColumnName.length() > 30 ? 30 : defaultJoinColumnName.length());
	}
	
	
	
	public String resolveColumnName(PropertyEvent event, String defaultColumnName) {
		if (defaultColumnName == "by") return "by_";
		if (defaultColumnName == "key") return "key_";
		if (defaultColumnName == "value") return "value_";
		if (defaultColumnName == "type") return "type_";
		return defaultColumnName.substring(0, defaultColumnName.length() > 30 ? 30 : defaultColumnName.length());
	}
	
	
	
	public String resolveTableName(ClazzEvent event, String defaultTableName) {
		return defaultTableName.substring(0, defaultTableName.length() > 30 ? 30 : defaultTableName.length());
	}
	
	
	
	public String resolveJoinTableName(PropertyEvent event, String defaultJoinTableName) {
		return defaultJoinTableName.substring(0, defaultJoinTableName.length() > 30 ? 30 : defaultJoinTableName.length());
	}
	
	
	
	public String resolveInverseJoinColumnName(AssociationEvent event, String defaultInverseJoinColumnName) {
		return defaultInverseJoinColumnName.substring(0, defaultInverseJoinColumnName.length() > 30 ? 30 : defaultInverseJoinColumnName.length());
	}
	
	
	
	public String resolveAttributeOverrideColumnName(PropertyEvent event, Property overrideAttributeProperty,
	String overrideAttributeName, String defaultAttributeOverrideColumnName) {
		return defaultAttributeOverrideColumnName.substring(0, defaultAttributeOverrideColumnName.length() > 30 ? 30 : defaultAttributeOverrideColumnName.length());
	}
	
	
	
	public String resolveIndexColumnName(PropertyEvent event, String defaultIndexColumnName) {
		return defaultIndexColumnName.substring(0, defaultIndexColumnName.length() > 30 ? 30 : defaultIndexColumnName.length());
	}

} 