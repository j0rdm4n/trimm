/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa;


import dk.tigerteam.trimm.mdsd.java.codedom.ClazzOrInterface;
import dk.tigerteam.trimm.mdsd.java.codedom.CodeModel;
import dk.tigerteam.trimm.mdsd.java.generator.JavaCollectionTypeResolver;
import dk.tigerteam.trimm.mdsd.java.generator.codewriter.DefaultPackageRewriter;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.uml2.xmi.ea.EAXmiReader;
import org.junit.Test;
import static junit.framework.Assert.*;

import java.util.List;

public class PackageBugTest {
    @Test
    public void testThatFQCNForTheBankAccountClassIsCorrect() {
        JpaJavaGenerator jpaJavaGenerator = new JpaJavaGenerator();

        MetaModel metaModel = new EAXmiReader().read(this.getClass().getResource("/packagebug.xml"));
        JpaGeneratorContext context = new JpaGeneratorContext(metaModel, new JpaGeneratorEventListener(true));
        List<ClazzOrInterface> clazzOrInterfaces = jpaJavaGenerator.transform(context);

        for (ClazzOrInterface clazzOrInterface : clazzOrInterfaces) {
            if (clazzOrInterface.getMetaType().getId().equals("EAID_08AB517F_B33F_4ed1_BA0D_8029093A8582")) {
                assertEquals("rootpackage.Bank.BankAccount", clazzOrInterface.getFQCN());
                assertEquals("rootpackage.Bank.BankAccount", clazzOrInterface.getMetaType().getFQCN());
                return;
            }
        }
        fail("Didn't find the BankAccount class - so couldn't test FQCN");
    }
}
