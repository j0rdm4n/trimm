After installing the Archetype (using mvn install)
you can create a new project based on this Archetype by issuing the following command:

mvn archetype:generate                                  \
  -DarchetypeGroupId=dk.tigerteam.maven.archetypes      \
  -DarchetypeArtifactId=trimm.java.archetype            \
  -DarchetypeVersion=1.0.0-SNAPSHOT                     \
  -DarchetypeCatalog=http://www.tigerteam.dk/maven2/archetype-catalog.xml \
  -DgroupId=<my.groupid>                                \
  -DartifactId=<my-artifactId>

OS X / Linux Example:
  mvn archetype:generate                                  \
    -DarchetypeGroupId=dk.tigerteam.maven.archetypes      \
    -DarchetypeArtifactId=trimm.java.archetype            \
    -DarchetypeVersion=1.0.0-SNAPSHOT                     \
    -DarchetypeCatalog=http://www.tigerteam.dk/maven2/archetype-catalog.xml \
    -DgroupId=dk.tigerteam.trimm                          \
    -DartifactId=example


Windows example:
  mvn archetype:generate -DarchetypeGroupId=dk.tigerteam.maven.archetypes -DarchetypeArtifactId=trimm.java.archetype -DarchetypeVersion=1.0.0-SNAPSHOT -DarchetypeCatalog=http://www.tigerteam.dk/maven2/archetype-catalog.xml -DgroupId=dk.tigerteam.trimm -DartifactId=example