/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.jpa.maven;

import dk.tigerteam.trimm.mdsd.jpa.configuration.JpaConfiguration;
import dk.tigerteam.trimm.mdsd.jpa.configuration.JpaConfigurationBasedCodeGenerator;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

import java.io.File;

/**
 ** <pre>
 * &lt;plugin>
            	&lt;groupId>dk.tigerteam&lt;/groupId>
            	&lt;artifactId>TrimmJpaMavenPlugin&lt;/artifactId>
            	&lt;version>1.0.1&lt;/version>
            	&lt;executions>
                    &lt;execution>
                        &lt;id>generate&lt;/id>
                        &lt;phase>generate-sources&lt;/phase>
                        &lt;goals>
                        	&lt;goal>generate&lt;/goal>
                        &lt;/goals>
		            	&lt;configuration>
		            		&lt;yamlFile>JpaModel.yml&lt;/yamlFile>
		            	&lt;/configuration>
		            &lt;/execution>
		        &lt;/executions>
            &lt;dependencies>
              &lt;dependency>
                &lt;groupId>dk.tigerteam&lt;/groupId>
                &lt;artifactId>TrimmJpa&lt;/artifactId>
                &lt;version>1.0.0-SNAPSHOT&lt;/version>
                &lt;type>jar&lt;/type>
                &lt;scope>compile&lt;/scope>
                &lt;exclusions>
                  &lt;exclusion>
                    &lt;artifactId>junit&lt;/artifactId>
                    &lt;groupId>junit&lt;/groupId>
                  &lt;/exclusion>
                &lt;/exclusions>
              &lt;/dependency>
              &lt;dependency>
                &lt;groupId>junit&lt;/groupId>
                &lt;artifactId>junit&lt;/artifactId>
                &lt;version>${junit.version}&lt;/version>
              &lt;/dependency>
            &lt;/dependencies>
 * &lt;/plugin>
 * </pre>
 * <b>Note: Don't combine this plugin with <code>build-helper-maven-plugin</code> setup to add the same folders!
 * @author Lasse Cramon
 * @author Jeppe Cramon
 * @goal generate
 * @phase generate-sources
 * @requiresDependencyResolution compile
 */
public class TrimmJpaMojo extends AbstractMojo {

  private static final String MAVEN_JAVA_SRC_DIR = "src/main/java";
	private static final String MAVEN_JAVA_RESOURCES_DIR = "src/main/resources";

	/**
	 * @parameter expression="${yamlFile}"
	 * @required
	 */
	private File yamlFile;
	/**
   * @parameter expression="${project}"
   * @required
   */
  private MavenProject project;

	public TrimmJpaMojo() {}

	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
      getLog().info("Generation JPA sourcecode files from configuration file '" + yamlFile.getAbsolutePath() + "'");
      JpaConfiguration jpaConfiguration = JpaConfigurationBasedCodeGenerator.parseJpaConfiguration(yamlFile, false);

      String outputDirectory = project.getBuild().getDirectory();
      JpaConfigurationBasedCodeGenerator.generate(jpaConfiguration, yamlFile,  project.getBasedir(), project.getBasedir(), null);

			addGeneratedSourcesPathToCompileSourceRoot(jpaConfiguration, project.getBasedir());
		} catch (Exception e) {
			getLog().error(e);
			throw new MojoExecutionException("TrimmJpaMojo failed: " + e.getMessage(), e);
		}
	}

	private void addGeneratedSourcesPathToCompileSourceRoot(JpaConfiguration configuration, File baseDir) {
		if(configuration.getGenerateBaseClassesToPath() != null && !configuration.getGenerateBaseClassesToPath().startsWith(MAVEN_JAVA_SRC_DIR)) {
			File baseClassesPath = new File(configuration.getGenerateBaseClassesToPath());
			baseClassesPath = appendBaseDir(baseClassesPath, baseDir);
			getLog().info("Adding Base classes source path '" + baseClassesPath.getAbsolutePath() + "'");
			project.addCompileSourceRoot(baseClassesPath.getAbsolutePath());
		}
		if(configuration.getGenerateResourcesToPath() != null && !configuration.getGenerateResourcesToPath().startsWith(MAVEN_JAVA_RESOURCES_DIR)) {
			File resourcesPath = new File(configuration.getGenerateResourcesToPath());
			resourcesPath = appendBaseDir(resourcesPath, baseDir);
			getLog().info("Adding Main Resources source path '" + resourcesPath.getAbsolutePath() + "'");
			Resource resource = new Resource();
			resource.setDirectory(resourcesPath.getAbsolutePath());
			project.addResource(resource);
		}
		if(configuration.getGenerateExtensionClassesToPath() != null && !configuration.getGenerateExtensionClassesToPath().equals(configuration.getGenerateBaseClassesToPath()) &&
				!configuration.getGenerateExtensionClassesToPath().startsWith(MAVEN_JAVA_SRC_DIR)) {
			File extensionClassesPath = new File(configuration.getGenerateExtensionClassesToPath());
			extensionClassesPath = appendBaseDir(extensionClassesPath, baseDir);
			getLog().info("Adding Extension classes source path '" + extensionClassesPath.getAbsolutePath() + "'");
			project.addCompileSourceRoot(extensionClassesPath.getAbsolutePath());
		}
		if(configuration.getGenerateInterfacesToPath() != null && !configuration.getGenerateInterfacesToPath().equals(configuration.getGenerateBaseClassesToPath()) &&
					!configuration.getGenerateInterfacesToPath().equals(configuration.getGenerateExtensionClassesToPath())) {
			File interfaceClassesPath = new File(configuration.getGenerateInterfacesToPath());
			interfaceClassesPath = appendBaseDir(interfaceClassesPath, baseDir);
			getLog().info("Adding Interface classes source path '" + interfaceClassesPath.getAbsolutePath() + "'");
			project.addCompileSourceRoot(interfaceClassesPath.getAbsolutePath());
		}
		if(configuration.getGenerateTestClassesToPath() != null && !configuration.getGenerateTestClassesToPath().equals(configuration.getGenerateBaseClassesToPath()) &&
				!configuration.getGenerateTestClassesToPath().equals(configuration.getGenerateExtensionClassesToPath()) &&
				!configuration.getGenerateTestClassesToPath().equals(configuration.getGenerateInterfacesToPath())) {
			File testClassesPath = new File(configuration.getGenerateTestClassesToPath());
			testClassesPath = appendBaseDir(testClassesPath, baseDir);
			getLog().info("Adding Test classes source path '" + testClassesPath.getAbsolutePath() + "'");
			project.addTestCompileSourceRoot(testClassesPath.getAbsolutePath());
		}
	}

	private File appendBaseDir(File dir, File baseDir) {
		if(baseDir != null && !dir.isAbsolute()) {
			getLog().info("Appending baseDir '" + baseDir.getAbsolutePath() + "' to source dir '" + dir.getPath() + "'");
			dir = new File(baseDir.getAbsolutePath() + "/" + dir.getPath());
		}
		return dir;
	}


}
