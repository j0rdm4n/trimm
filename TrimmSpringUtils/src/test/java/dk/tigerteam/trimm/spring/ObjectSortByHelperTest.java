/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.spring;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ObjectSortByHelperTest {
	
	@Test
	public void test() {
		List<TestObject1> object1s = new ArrayList<TestObject1>();
		TestObject3 testObject3_1 = new TestObject3();
		TestObject3 testObject3_2 = new TestObject3();
		TestObject3 testObject3_3 = new TestObject3();
		object1s.add(new TestObject1(new TestObject2(testObject3_1)));
		object1s.add(new TestObject1(new TestObject2(testObject3_2)));
		object1s.add(new TestObject1(new TestObject2(testObject3_3)));
		object1s.add(new TestObject1(new TestObject2(testObject3_1)));
		object1s.add(new TestObject1(new TestObject2(testObject3_2)));
		object1s.add(new TestObject1(new TestObject2(testObject3_1)));

		Map<TestObject3, List<TestObject1>> sorted = ObjectSortByHelper.sortBy(object1s, TestObject3.class, "testObject2.testObject3");
		assertEquals(sorted.size(), 3);
		assertTrue(sorted.containsKey(testObject3_1));

		assertEquals(sorted.get(testObject3_1).size(), 3);
		assertTrue(sorted.get(testObject3_1).contains(object1s.get(0)));
		assertTrue(sorted.get(testObject3_1).contains(object1s.get(3)));
		assertTrue(sorted.get(testObject3_1).contains(object1s.get(5)));

		assertEquals(sorted.get(testObject3_2).size(), 2);
		assertTrue(sorted.get(testObject3_2).contains(object1s.get(1)));
		assertTrue(sorted.get(testObject3_2).contains(object1s.get(4)));

		assertEquals(sorted.get(testObject3_3).size(), 1);
		assertTrue(sorted.get(testObject3_3).contains(object1s.get(2)));
	}
}
