/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.spring;


import org.junit.Test;
import org.springframework.aop.ClassFilter;

import java.util.Set;

import static junit.framework.Assert.fail;

public class TestClasspathScanner {
	
	@SuppressWarnings("rawtypes")
	@Test
	public void test() {
		ClasspathScanner scanner = new ClasspathScanner();
		scanner.withIncludePackageNames("dk.tigerteam.trimm.bean", "dk.tigerteam.trimm.persistence")
			   .withExcludePackageNames("dk.tigerteam.trimm.persistence.model")
			   .withClassFilter(new ClassFilter() {

				public boolean matches(Class<?> clazz) {
					return clazz.getName().indexOf("$") == -1;
				}
				   
			   });
		Set<Class> scan = scanner.scan();
		for (Class classFound : scan) {
			if (classFound.getName().startsWith("dk.tigerteam.trimm.persistence.model")) {
				fail("Found class '" + classFound + "' in excluded package 'dk.tigerteam.trimm.persistence.model'");
			}
			System.out.println("Found '" + classFound + "'");
		}
		//assertEquals(scan.size(), 52); // Weak test that includes classes from both /src and /test source folders (TODO: maven only lets us see 4 from the test folders and 2 from the src folder)
	}
}
