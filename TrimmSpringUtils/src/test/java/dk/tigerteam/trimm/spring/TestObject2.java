/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.spring;

public class TestObject2 {
	
	private TestObject3 testObject3;

	public TestObject2(TestObject3 testObject3) {
		super();
		this.testObject3 = testObject3;
	}

	public TestObject2() {
		super();
		// TODO Auto-generated constructor stub
	}

	public final TestObject3 getTestObject3() {
		return testObject3;
	}

	public final void setTestObject3(TestObject3 testObject3) {
		this.testObject3 = testObject3;
	}

}
