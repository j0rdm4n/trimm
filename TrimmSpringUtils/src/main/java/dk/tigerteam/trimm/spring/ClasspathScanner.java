/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.spring;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.ClassFilter;
import org.springframework.asm.*;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.ClassMetadata;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.ClassUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Classpath Class scanner which supports Ant style path matching.
 * <p/>
 * Warning: This method uses {@link PathMatchingResourcePatternResolver} which is NOT guaranteed to work with wildcard search in archives -
 * which is used here. Only searches in one Jar. If you have the same package name split over more than one Jar, then this scanner will only
 * look in the first Jar found.
 * 
 * @see AntPathMatcher
 * @see PathMatchingResourcePatternResolver
 * @author Jeppe Cramon
 */
public class ClasspathScanner {
	
	private static final Log logger = LogFactory.getLog(ClasspathScanner.class);

	private AntPathMatcher excludeAntPathMatcher = new AntPathMatcher();

	/**
	 * The package names that will be recursively scanned for classes include and filter
	 */
	private Set<String> includePackageNames = new HashSet<String>();

	/**
	 * The package names that will excluded while scanning for classes
	 */
	private Set<String> excludePackageNames = new HashSet<String>();

	/**
	 * The filter that is used to determine if a class found during scanning should be included in the implementations set
	 */
	private ClassFilter classFilter;

	/**
	 * The ClassLoader to use when looking for classes. If null then the ClassLoader returned by {@link ClassUtils#getDefaultClassLoader()}
	 * will be used.
	 */
	private ClassLoader classLoader;

	// -------------------------------------------------------------------------------------------

	public ClasspathScanner() {
		super();
	}

	public ClasspathScanner(ClassFilter classFilter) {
		super();
		this.classFilter = classFilter;
	}

	public ClasspathScanner(ClassFilter classFilter, String... includePackageNames) {
		super();
		this.classFilter = classFilter;
		setIncludePackageNames(includePackageNames);
	}

	public ClasspathScanner(ClassFilter classFilter, Set<String> includePackageNames) {
		super();
		this.classFilter = classFilter;
		setIncludePackageNames(includePackageNames);
	}

	public ClasspathScanner(ClassFilter classFilter, Set<String> includePackageNames, Set<String> excludePackageNames) {
		super();
		this.classFilter = classFilter;
		setIncludePackageNames(includePackageNames);
		setExcludePackageNames(excludePackageNames);
	}

	// -------------------------------------------------------------------------------------------

	/**
	 * The package names that will be recursively scanned for classes include and filter
	 */
	public Set<String> getIncludePackageNames() {
		return includePackageNames;
	}

	/**
	 * The package names that will be recursively scanned for classes include and filter
	 */
	public void setIncludePackageNames(Collection<String> includePackageNames) {
		this.includePackageNames.clear();
		for (String packageName : includePackageNames) {
			this.includePackageNames.add(packageName.replace('.', '/'));
		}
	}

	/**
	 * The package names that will be recursively scanned for classes include and filter
	 */
	public ClasspathScanner withIncludePackageNames(Collection<String> includePackageNames) {
		setIncludePackageNames(includePackageNames);
		return this;
	}

	/**
	 * The package names that will be recursively scanned for classes include and filter
	 */
	public void setIncludePackageNames(String... includePackageNames) {
		setIncludePackageNames(Arrays.asList(includePackageNames));
	}

	/**
	 * The package names that will be recursively scanned for classes include and filter
	 */
	public ClasspathScanner withIncludePackageNames(String... includePackageNames) {
		setIncludePackageNames(includePackageNames);
		return this;
	}

	/**
	 * The package names that will excluded while scanning for classes
	 */
	public Set<String> getExcludePackageNames() {
		return excludePackageNames;
	}

	/**
	 * The package names that will excluded while scanning for classes
	 */
	public void setExcludePackageNames(Collection<String> excludePackageNames) {
		this.excludePackageNames.clear();
		for (String packageName : excludePackageNames) {
			this.excludePackageNames.add(addAntWildcardMatchIfMissing(packageName));
		}
	}

	/**
	 * The package names that will excluded while scanning for classes
	 */
	public ClasspathScanner withExcludePackageNames(Collection<String> excludePackageNames) {
		setExcludePackageNames(excludePackageNames);
		return this;
	}

	/**
	 * The package names that will excluded while scanning for classes
	 */
	public void setExcludePackageNames(String... excludePackageNames) {
		setExcludePackageNames(Arrays.asList(excludePackageNames));
	}

	/**
	 * The package names that will excluded while scanning for classes
	 */
	public ClasspathScanner withExcludePackageNames(String... excludePackageNames) {
		setExcludePackageNames(excludePackageNames);
		return this;
	}

	/**
	 * The ClassLoader to use when looking for classes. If null then the ClassLoader returned by {@link ClassUtils#getDefaultClassLoader()}
	 * will be used.
	 */
	public ClassLoader getClassLoader() {
		return classLoader == null ? ClassUtils.getDefaultClassLoader() : classLoader;
	}

	/**
	 * Sets the ClassLoader to use when looking for classes. If null then the ClassLoader returned by
	 * {@link ClassUtils#getDefaultClassLoader()} will be used.
	 * 
	 * @param classloader
	 *          a ClassLoader to use when scanning for classes
	 */
	public void setClassLoader(ClassLoader classloader) {
		this.classLoader = classloader;
	}

	/**
	 * Sets the ClassLoader to use when looking for classes. If null then the ClassLoader returned by
	 * {@link ClassUtils#getDefaultClassLoader()} will be used.
	 * 
	 * @param classloader
	 *          a ClassLoader to use when scanning for classes
	 */
	public ClasspathScanner withClassLoader(ClassLoader classloader) {
		this.classLoader = classloader;
		return this;
	}

	/**
	 * The filter that is used to determine if a class found during scanning should be included in the implementations set
	 */
	public ClassFilter getClassFilter() {
		return classFilter;
	}

	/**
	 * The filter that is used to determine if a class found during scanning should be included in the implementations set
	 */
	public void setClassFilter(ClassFilter classFilter) {
		this.classFilter = classFilter;
	}

	/**
	 * The filter that is used to determine if a class found during scanning should be included in the implementations set
	 */
	public ClasspathScanner withClassFilter(ClassFilter classFilter) {
		this.classFilter = classFilter;
		return this;
	}

	/**
	 * Scan for Classes using package pattern setup using {@link #setIncludePackageNames(Collection)} and
	 * {@link #setExcludePackageNames(Collection)} (or any of their sugar method versions).<br/>
	 * 
	 * Warning: This method uses PathMatchingResourcePatternResolver which is NOT guaranteed to work with wildcard search in archives - which
	 * is used here.
	 */
	@SuppressWarnings("rawtypes")
	public Set<Class> scan() {
		Set<Class> classesFound = new HashSet<Class>();

		for (String pkg : includePackageNames) {
			logger.trace("Scaning for classes in package '" + pkg + "'");
			scan(pkg, classesFound);
		}
		return classesFound;
	}

	/**
	 * Scan for Classes in the packages indicated with the includePackageNames parameter (sugar method).<br/>
	 * 
	 * Warning: This method uses PathMatchingResourcePatternResolver which is NOT guaranteed to work with wildcard search in archives - which
	 * is used here.
	 */
	@SuppressWarnings("rawtypes")
	public Set<Class> scan(String... includePackagenames) {
		setIncludePackageNames(Arrays.asList(includePackagenames));
		return scan();
	}

	/**
	 * Scans for classes starting at the package provided and descending into subpackages. Each class is offered up to the {@link ClassFilter}
	 * as it is discovered, and if the ClassValidator returns true the class is retained.
	 * 
	 * Warning: This method uses PathMatchingResourcePatternResolver which is NOT guaranteed to work with wildcard search in archives - which
	 * is used here.
	 * 
	 * @param searchPackageName
	 *          the name of the package from which to start scanning for classes, e.g. {@code dk.tigerteam.mappers}
	 */
	@SuppressWarnings("rawtypes")
	protected void scan(String searchPackageName, Set<Class> classesFound) {
		String pathMatchingPackageName = searchPackageName.replace('.', '/');
		ClassLoader loader = getClassLoader();
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(loader);

		try {
			String locationPattern = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + addAntWildcardMatchIfMissing(pathMatchingPackageName);
			logger.debug("Scanning using locationPattern: '" + locationPattern + "'");
			Resource[] resourcesFound = resolver.getResources(locationPattern);
			logger.trace("Found resource: " + Arrays.toString(resourcesFound));
			if (resourcesFound != null) {
				for (Resource resource : resourcesFound) {
					if (resource.isReadable()) {
						InputStream is = resource.getInputStream();
						ClassReader classReader = null;
						try {
							classReader = new ClassReader(is);
						} finally {
							is.close();
						}
						ClassMetadataReadingVisitor visitor = new ClassMetadataReadingVisitor();
						classReader.accept(visitor, ClassReader.SKIP_DEBUG);
						addIfMatching(visitor.getClassName(), classesFound);
					}
				}
			}
		} catch (IOException ioe) {
			logger.warn("Could not read package: '" + searchPackageName + "' [classpath pattern: '" + pathMatchingPackageName + "']", ioe);
			return;
		}
	}

	protected String addAntWildcardMatchIfMissing(String pathMatchingPackageName) {
		if (!pathMatchingPackageName.endsWith("/**/*.class")) {
			if (pathMatchingPackageName.endsWith("/**")) {
				pathMatchingPackageName += "/*.class";
			} else if (pathMatchingPackageName.endsWith("/")) {
				pathMatchingPackageName += "**/*.class";
			} else {
				pathMatchingPackageName += "/**/*.class";
			}
		}
		return pathMatchingPackageName;
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	private void addIfMatchingFromFileSystem(String pathToClass, String searchPackageName, Set<Class> classesFound) {
		int index = pathToClass.lastIndexOf("/");
		if (index == -1) {
			addIfMatching(pathToClass, classesFound);
			return;
		}

		index = pathToClass.indexOf(searchPackageName);
		if (index == -1) {
			throw new RuntimeException("Expected the path to the class found '" + pathToClass + "' would contain the searchPackageName '"
					+ searchPackageName + "'");
		}

		String className = pathToClass.substring(index);
		addIfMatching(className, classesFound);
	}

	protected boolean isExcluded(String packageName) {
		for (String excludePackageName : excludePackageNames) {
			if (excludeAntPathMatcher.match(excludePackageName, packageName)) {
				return true;
			}
		}
		return false;
	}

	protected String convertPathToPackageName(String packageName) {
		int endIndex = packageName.lastIndexOf("/");
		if (endIndex == -1) endIndex = packageName.lastIndexOf("\\");
		return packageName.substring(0, endIndex).replaceAll("/", ".");
	}

	/**
	 * Add the class designated by the fully qualified class name (FQCN) provided to the set of resolved classes if and only if it adheres to
	 * the rules set out in the {@link ClassFilter}
	 * 
	 * @param fqcn
	 *          the fully qualified name of a class
	 */
	@SuppressWarnings("rawtypes")
	private void addIfMatching(String fqcn, Set<Class> classesFound) {
		if (isExcluded(fqcn))
			return;

		try {
			logger.trace("Checking to see if class '" + fqcn + "' should be included");
			ClassLoader loader = ClassUtils.getDefaultClassLoader();
			//String externalName = fqcn.substring(0, fqcn.indexOf('.')).replace('/', '.').replace('\\', '.');

			Class<? extends Object> type = loader.loadClass(fqcn);
			if (classFilter.matches(type)) {
				logger.trace("Adding class '" + type.getName() + "'");
				classesFound.add(type);
			}
		} catch (Exception e) {
			logger.warn("Could not examine class '" + fqcn + "' due to a " + e.getClass().getName() + " with message: " + e.getMessage(), e);
		}
	}
	
	
	private static class ClassMetadataReadingVisitor extends ClassVisitor implements ClassMetadata {

        private final EmptyVisitor emptyVisitor;
        private String className;

		private boolean isInterface;

		private boolean isAbstract;

		private boolean isFinal;

		private String enclosingClassName;

		private boolean independentInnerClass;

		private String superClassName;

		private String[] interfaces;

        public ClassMetadataReadingVisitor() {
            super(Opcodes.V1_6);
            emptyVisitor = new EmptyVisitor();
        }

        public void visit(int version, int access, String name, String signature, String supername, String[] interfaces) {
			this.className = ClassUtils.convertResourcePathToClassName(name);
			this.isInterface = ((access & Opcodes.ACC_INTERFACE) != 0);
			this.isAbstract = ((access & Opcodes.ACC_ABSTRACT) != 0);
			this.isFinal = ((access & Opcodes.ACC_FINAL) != 0);
			if (supername != null) {
				this.superClassName = ClassUtils.convertResourcePathToClassName(supername);
			}
			this.interfaces = new String[interfaces.length];
			for (int i = 0; i < interfaces.length; i++) {
				this.interfaces[i] = ClassUtils.convertResourcePathToClassName(interfaces[i]);
			}
		}

		public void visitOuterClass(String owner, String name, String desc) {
			this.enclosingClassName = ClassUtils.convertResourcePathToClassName(owner);
		}

		public void visitInnerClass(String name, String outerName, String innerName, int access) {
			if (outerName != null && this.className.equals(ClassUtils.convertResourcePathToClassName(name))) {
				this.enclosingClassName = ClassUtils.convertResourcePathToClassName(outerName);
				this.independentInnerClass = ((access & Opcodes.ACC_STATIC) != 0);
			}
		}

		public void visitSource(String source, String debug) {
			// no-op
		}

		public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
			return emptyVisitor.visitAnnotation(desc, visible);
		}

		public void visitAttribute(Attribute attr) {
			// no-op
		}

		public FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
			// no-op
			return emptyVisitor.visitField(access, name, desc, signature, value);
		}

		public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
			// no-op
			return emptyVisitor.visitMethod(access, name, desc, signature, exceptions);
		}

		public void visitEnd() {
			// no-op
		}


		public String getClassName() {
			return this.className;
		}

		public boolean isInterface() {
			return this.isInterface;
		}

		public boolean isAbstract() {
			return this.isAbstract;
		}

		public boolean isConcrete() {
			return !(this.isInterface || this.isAbstract);
		}

		public boolean isFinal() {
			return this.isFinal;
		}

		public boolean isIndependent() {
			return (this.enclosingClassName == null || this.independentInnerClass);
		}

		public boolean hasEnclosingClass() {
			return (this.enclosingClassName != null);
		}

		public String getEnclosingClassName() {
			return this.enclosingClassName;
		}

		public boolean hasSuperClass() {
			return (this.superClassName != null);
		}

		public String getSuperClassName() {
			return this.superClassName;
		}

		public String[] getInterfaceNames() {
			return this.interfaces;
		}

		// TODO new in Spring 3.1 not sure if all still works with this???
		@Override
		public String[] getMemberClassNames() {
			return new String[]{className, enclosingClassName};
		}

	}
    static class EmptyVisitor extends ClassVisitor {

        AnnotationVisitor av = new AnnotationVisitor(Opcodes.ASM4) {

            @Override
            public AnnotationVisitor visitAnnotation(String name, String desc) {
                return this;
            }

            @Override
            public AnnotationVisitor visitArray(String name) {
                return this;
            }
        };

        public EmptyVisitor() {
            super(Opcodes.ASM4);
        }

        @Override
        public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
            return av;
        }

        @Override
        public FieldVisitor visitField(int access, String name, String desc,
                                       String signature, Object value) {
            return new FieldVisitor(Opcodes.ASM4) {

                @Override
                public AnnotationVisitor visitAnnotation(String desc,
                                                         boolean visible) {
                    return av;
                }
            };
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String desc,
                                         String signature, String[] exceptions) {
            return new MethodVisitor(Opcodes.ASM4) {

                @Override
                public AnnotationVisitor visitAnnotationDefault() {
                    return av;
                }

                @Override
                public AnnotationVisitor visitAnnotation(String desc,
                                                         boolean visible) {
                    return av;
                }

                @Override
                public AnnotationVisitor visitParameterAnnotation(
                        int parameter, String desc, boolean visible) {
                    return av;
                }
            };
        }
    }
}
