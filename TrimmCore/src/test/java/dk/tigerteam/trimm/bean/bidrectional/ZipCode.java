/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.bean.bidrectional;

import dk.tigerteam.trimm.bean.bidirectional.ManyToOneWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManySetWrapper;

public class ZipCode {
	private Address address;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		new ManyToOneWrapper<Address, ZipCode>(this) {
			private static final long serialVersionUID = 2389893134600823095L;

			@SuppressWarnings("unchecked")
			@Override
			protected void addManySideObjectToOneSideCollection(
					Address oneSide, ZipCode manySide) {
				((OneToManySetWrapper<Address, ZipCode>)oneSide.getZipCodes()).getWrappedCollection().add(manySide);
			}

			@Override
			protected Address getOneSideObjectInManySideObject(ZipCode manySide) {
				return manySide.getAddress();
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void removeManySideObjectFromOneSideCollection(
					Address oneSide, ZipCode manySide) {
				((OneToManySetWrapper<Address, ZipCode>)oneSide.getZipCodes()).getWrappedCollection().remove(manySide);
			}

			@Override
			protected void setOneSideObjectInManySideObject(ZipCode manySide,
					Address oneSide) {
				ZipCode.this.address = oneSide;
			}
		}.updateOneSideObject(address);
	}
	
}
