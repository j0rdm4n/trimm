/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.bean.bidrectional;

import org.junit.Test;

import java.util.Arrays;

import static junit.framework.Assert.*;

public class TestBidirectionality {

	@Test
	public void testOneToOne() {
		Person person1 = new Person();
		PersonName personName = new PersonName();
		
		person1.setPersonName(personName);
		assertEquals(personName, person1.getPersonName());
		assertEquals(person1, personName.getPerson());
		
		Person person2 = new Person();
		person2.setPersonName(personName);
		assertNull(person1.getPersonName());
		assertEquals(personName, person2.getPersonName());
		assertEquals(person2, personName.getPerson());
		
		// Set the relationship the opposite way
		personName.setPerson(person1);
		assertNull(person2.getPersonName());
		assertEquals(personName, person1.getPersonName());
		assertEquals(person1, personName.getPerson());
		
		// Remove relationship without setting a new
		personName.setPerson(null);
		assertNull(person1.getPersonName());
		assertNull(personName.getPerson());
	}
	
	@Test
	public void testOneToManyCollection_and_ManyToOne() {
		Person person1 = new Person();
		Person person2 = new Person();
		
		Address address1 = new Address();
		Address address2 = new Address();
		Address address3 = new Address();
		
		
		// Setup initial relations
		person1.getAddresses().add(address1);
		person1.getAddresses().add(address3);      
		person2.getAddresses().add(address2);
		
		assertEquals(2, person1.getAddresses().size());
		assertTrue(person1.getAddresses().contains(address1));
		assertTrue(person1.getAddresses().contains(address3));
		
		assertEquals(1, person2.getAddresses().size());
		assertTrue(person2.getAddresses().contains(address2));
		
		assertEquals(address1.getPerson(), person1);
		assertEquals(address3.getPerson(), person1);
		assertEquals(address2.getPerson(), person2);
		
		// Change a relation (in one direction)
		address2.setPerson(person1);
		assertEquals(3, person1.getAddresses().size());
		assertEquals(0, person2.getAddresses().size());
		assertTrue(person1.getAddresses().contains(address1));
		assertTrue(person1.getAddresses().contains(address2));
		assertEquals(person1, address2.getPerson());
		assertTrue(person1.getAddresses().contains(address3));
		
		// Change some relations (in the opposite direction)
		person2.getAddresses().add(address1);
		person2.getAddresses().add(address3);
		assertEquals(1, person1.getAddresses().size());
		assertEquals(2, person2.getAddresses().size());
		assertTrue(person1.getAddresses().contains(address2));
		assertEquals(person1, address2.getPerson());
		assertTrue(person2.getAddresses().contains(address1));
		assertTrue(person2.getAddresses().contains(address3));
		assertEquals(person2, address1.getPerson());
		assertEquals(person2, address3.getPerson());
		
		// Remove a relation (in one direction)
		address1.setPerson(null);
		assertEquals(1, person1.getAddresses().size());
		assertEquals(1, person2.getAddresses().size());
		assertTrue(person1.getAddresses().contains(address2));
		assertEquals(person1, address2.getPerson());
		assertTrue(person2.getAddresses().contains(address3));
		assertNull(address1.getPerson());
		assertEquals(person2, address3.getPerson());
		
		// Remove a relation (in the opposite direction)
		person1.getAddresses().remove(address2);
		assertEquals(0, person1.getAddresses().size());
		assertTrue(person1.getAddresses().isEmpty());
		assertEquals(1, person2.getAddresses().size());
		assertNull(address2.getPerson());
		assertTrue(person2.getAddresses().contains(address3));
		assertNull(address1.getPerson());
		assertEquals(person2, address3.getPerson());
		
		// Test addAll
		assertTrue(person1.getAddresses().addAll(Arrays.asList(address1, address2, address3)));
		assertEquals(3, person1.getAddresses().size());
		assertTrue(person1.getAddresses().contains(address1));
		assertTrue(person1.getAddresses().contains(address2));
		assertTrue(person1.getAddresses().contains(address3));
		
		assertEquals(address1.getPerson(), person1);
		assertEquals(address2.getPerson(), person1);
		assertEquals(address3.getPerson(), person1);
		
		assertEquals(0, person2.getAddresses().size());
		
		// Test retainAll
		assertTrue(person1.getAddresses().retainAll(Arrays.asList(address1, address3)));
		assertEquals(2, person1.getAddresses().size());
		assertTrue(person1.getAddresses().contains(address1));
		assertTrue(person1.getAddresses().contains(address3));
		
		assertEquals(address1.getPerson(), person1);
		assertNull(address2.getPerson());
		assertEquals(address3.getPerson(), person1);
		
		assertEquals(0, person2.getAddresses().size());
		
		// Test removeAll
		assertTrue(person1.getAddresses().removeAll(Arrays.asList(address1, address3)));
		assertEquals(0, person1.getAddresses().size());
		
		assertNull(address1.getPerson());
		assertNull(address2.getPerson());
		assertNull(address3.getPerson());
		
		assertEquals(0, person2.getAddresses().size());
		
		// Test clear
		person2.getAddresses().add(address1);
		person2.getAddresses().add(address2);
		person2.getAddresses().add(address3);
		
		assertEquals(3, person2.getAddresses().size());
		assertTrue(person2.getAddresses().contains(address1));
		assertTrue(person2.getAddresses().contains(address2));
		assertTrue(person2.getAddresses().contains(address3));
		assertEquals(address1.getPerson(), person2);
		assertEquals(address2.getPerson(), person2);
		assertEquals(address3.getPerson(), person2);
		
		assertEquals(0, person1.getAddresses().size());
		
		person2.getAddresses().clear();
		assertEquals(0, person2.getAddresses().size());
		assertNull(address1.getPerson());
		assertNull(address2.getPerson());
		assertNull(address3.getPerson());
	}
	
	@Test
	public void testOneToManySet_and_ManyToOne() {
		Address address1 = new Address();
		Address address2 = new Address();
		
		ZipCode zipCode1 = new ZipCode();
		ZipCode zipCode2 = new ZipCode();
		ZipCode zipCode3 = new ZipCode();
		
		
		// Setup initial relations
		address1.getZipCodes().add(zipCode1);
		address1.getZipCodes().add(zipCode3);      
		address2.getZipCodes().add(zipCode2);
		
		assertEquals(2, address1.getZipCodes().size());
		assertTrue(address1.getZipCodes().contains(zipCode1));
		assertTrue(address1.getZipCodes().contains(zipCode3));
		
		assertEquals(1, address2.getZipCodes().size());
		assertTrue(address2.getZipCodes().contains(zipCode2));
		
		assertEquals(zipCode1.getAddress(), address1);
		assertEquals(zipCode3.getAddress(), address1);
		assertEquals(zipCode2.getAddress(), address2);
		
		// Change a relation (in one direction)
		zipCode2.setAddress(address1);
		assertEquals(3, address1.getZipCodes().size());
		assertEquals(0, address2.getZipCodes().size());
		assertTrue(address1.getZipCodes().contains(zipCode1));
		assertTrue(address1.getZipCodes().contains(zipCode2));
		assertEquals(address1, zipCode2.getAddress());
		assertTrue(address1.getZipCodes().contains(zipCode3));
		
		// Change some relations (in the opposite direction)
		address2.getZipCodes().add(zipCode1);
		address2.getZipCodes().add(zipCode3);
		assertEquals(1, address1.getZipCodes().size());
		assertEquals(2, address2.getZipCodes().size());
		assertTrue(address1.getZipCodes().contains(zipCode2));
		assertEquals(address1, zipCode2.getAddress());
		assertTrue(address2.getZipCodes().contains(zipCode1));
		assertTrue(address2.getZipCodes().contains(zipCode3));
		assertEquals(address2, zipCode1.getAddress());
		assertEquals(address2, zipCode3.getAddress());
		
		// Remove a relation (in one direction)
		zipCode1.setAddress(null);
		assertEquals(1, address1.getZipCodes().size());
		assertEquals(1, address2.getZipCodes().size());
		assertTrue(address1.getZipCodes().contains(zipCode2));
		assertEquals(address1, zipCode2.getAddress());
		assertTrue(address2.getZipCodes().contains(zipCode3));
		assertNull(zipCode1.getAddress());
		assertEquals(address2, zipCode3.getAddress());
		
		// Remove a relation (in the opposite direction)
		address1.getZipCodes().remove(zipCode2);
		assertEquals(0, address1.getZipCodes().size());
		assertTrue(address1.getZipCodes().isEmpty());
		assertEquals(1, address2.getZipCodes().size());
		assertNull(zipCode2.getAddress());
		assertTrue(address2.getZipCodes().contains(zipCode3));
		assertNull(zipCode1.getAddress());
		assertEquals(address2, zipCode3.getAddress());
		
		// Test addAll
		assertTrue(address1.getZipCodes().addAll(Arrays.asList(zipCode1, zipCode2, zipCode3)));
		assertEquals(3, address1.getZipCodes().size());
		assertTrue(address1.getZipCodes().contains(zipCode1));
		assertTrue(address1.getZipCodes().contains(zipCode2));
		assertTrue(address1.getZipCodes().contains(zipCode3));
		
		assertEquals(zipCode1.getAddress(), address1);
		assertEquals(zipCode2.getAddress(), address1);
		assertEquals(zipCode3.getAddress(), address1);
		
		assertEquals(0, address2.getZipCodes().size());
		
		// Test addAll without an intermediate Collection
		assertTrue(address2.getZipCodes().addAll(address1.getZipCodes()));
		assertEquals(3, address2.getZipCodes().size());
		assertTrue(address2.getZipCodes().contains(zipCode1));
		assertTrue(address2.getZipCodes().contains(zipCode2));
		assertTrue(address2.getZipCodes().contains(zipCode3));
		
		assertEquals(zipCode1.getAddress(), address2);
		assertEquals(zipCode2.getAddress(), address2);
		assertEquals(zipCode3.getAddress(), address2);
		
		assertEquals(0, address1.getZipCodes().size());

		// Move it back
		assertTrue(address1.getZipCodes().addAll(address2.getZipCodes()));
		
		// Test retainAll
		assertTrue(address1.getZipCodes().retainAll(Arrays.asList(zipCode1, zipCode3)));
		assertEquals(2, address1.getZipCodes().size());
		assertTrue(address1.getZipCodes().contains(zipCode1));
		assertTrue(address1.getZipCodes().contains(zipCode3));
		
		assertEquals(zipCode1.getAddress(), address1);
		assertNull(zipCode2.getAddress());
		assertEquals(zipCode3.getAddress(), address1);
		
		assertEquals(0, address2.getZipCodes().size());
		
		// Test removeAll
		assertTrue(address1.getZipCodes().removeAll(Arrays.asList(zipCode1, zipCode3)));
		assertEquals(0, address1.getZipCodes().size());
		
		assertNull(zipCode1.getAddress());
		assertNull(zipCode2.getAddress());
		assertNull(zipCode3.getAddress());
		
		assertEquals(0, address2.getZipCodes().size());
		
		// Test clear
		address2.getZipCodes().add(zipCode1);
		address2.getZipCodes().add(zipCode2);
		address2.getZipCodes().add(zipCode3);
		
		assertEquals(3, address2.getZipCodes().size());
		assertTrue(address2.getZipCodes().contains(zipCode1));
		assertTrue(address2.getZipCodes().contains(zipCode2));
		assertTrue(address2.getZipCodes().contains(zipCode3));
		assertEquals(zipCode1.getAddress(), address2);
		assertEquals(zipCode2.getAddress(), address2);
		assertEquals(zipCode3.getAddress(), address2);
		
		assertEquals(0, address1.getZipCodes().size());
		
		address2.getZipCodes().clear();
		assertEquals(0, address2.getZipCodes().size());
		assertNull(zipCode1.getAddress());
		assertNull(zipCode2.getAddress());
		assertNull(zipCode3.getAddress());
	}
	
	@Test
	public void testOneToManyList_and_ManyToOne() {
		Person person1 = new Person();
		Person person2 = new Person();
		
		Subscription subscription1 = new Subscription();
		Subscription subscription2 = new Subscription();
		Subscription subscription3 = new Subscription();
		
		
		// Setup initial relations
		person1.getPreferredSubscriptions().add(subscription1);
		person1.getPreferredSubscriptions().add(subscription3);      
		person2.getPreferredSubscriptions().add(subscription2);
		
		assertEquals(2, person1.getPreferredSubscriptions().size());
		assertTrue(person1.getPreferredSubscriptions().contains(subscription1));
		assertTrue(person1.getPreferredSubscriptions().contains(subscription3));
		
		assertEquals(1, person2.getPreferredSubscriptions().size());
		assertTrue(person2.getPreferredSubscriptions().contains(subscription2));
		
		assertEquals(subscription1.getPerson(), person1);
		assertEquals(subscription3.getPerson(), person1);
		assertEquals(subscription2.getPerson(), person2);
		
		// Change a relation (in one direction)
		subscription2.setPerson(person1);
		assertEquals(3, person1.getPreferredSubscriptions().size());
		assertEquals(0, person2.getPreferredSubscriptions().size());
		assertTrue(person1.getPreferredSubscriptions().contains(subscription1));
		assertTrue(person1.getPreferredSubscriptions().contains(subscription2));
		assertEquals(person1, subscription2.getPerson());
		assertTrue(person1.getPreferredSubscriptions().contains(subscription3));
		
		// Change some relations (in the opposite direction)
		person2.getPreferredSubscriptions().add(subscription1);
		person2.getPreferredSubscriptions().add(subscription3);
		assertEquals(1, person1.getPreferredSubscriptions().size());
		assertEquals(2, person2.getPreferredSubscriptions().size());
		assertTrue(person1.getPreferredSubscriptions().contains(subscription2));
		assertEquals(person1, subscription2.getPerson());
		assertTrue(person2.getPreferredSubscriptions().contains(subscription1));
		assertTrue(person2.getPreferredSubscriptions().contains(subscription3));
		assertEquals(person2, subscription1.getPerson());
		assertEquals(person2, subscription3.getPerson());
		
		// Remove a relation (in one direction)
		subscription1.setPerson(null);
		assertEquals(1, person1.getPreferredSubscriptions().size());
		assertEquals(1, person2.getPreferredSubscriptions().size());
		assertTrue(person1.getPreferredSubscriptions().contains(subscription2));
		assertEquals(person1, subscription2.getPerson());
		assertTrue(person2.getPreferredSubscriptions().contains(subscription3));
		assertNull(subscription1.getPerson());
		assertEquals(person2, subscription3.getPerson());
		
		// Remove a relation (in the opposite direction)
		person1.getPreferredSubscriptions().remove(subscription2);
		assertEquals(0, person1.getPreferredSubscriptions().size());
		assertTrue(person1.getPreferredSubscriptions().isEmpty());
		assertEquals(1, person2.getPreferredSubscriptions().size());
		assertNull(subscription2.getPerson());
		assertTrue(person2.getPreferredSubscriptions().contains(subscription3));
		assertNull(subscription1.getPerson());
		assertEquals(person2, subscription3.getPerson());
		
		// Test addAll
		assertTrue(person1.getPreferredSubscriptions().addAll(Arrays.asList(subscription1, subscription2, subscription3)));
		assertEquals(3, person1.getPreferredSubscriptions().size());
		assertTrue(person1.getPreferredSubscriptions().contains(subscription1));
		assertTrue(person1.getPreferredSubscriptions().contains(subscription2));
		assertTrue(person1.getPreferredSubscriptions().contains(subscription3));
		
		assertEquals(subscription1.getPerson(), person1);
		assertEquals(subscription2.getPerson(), person1);
		assertEquals(subscription3.getPerson(), person1);
		
		assertEquals(0, person2.getPreferredSubscriptions().size());
		
		// Test retainAll
		assertTrue(person1.getPreferredSubscriptions().retainAll(Arrays.asList(subscription1, subscription3)));
		assertEquals(2, person1.getPreferredSubscriptions().size());
		assertTrue(person1.getPreferredSubscriptions().contains(subscription1));
		assertTrue(person1.getPreferredSubscriptions().contains(subscription3));
		
		assertEquals(subscription1.getPerson(), person1);
		assertNull(subscription2.getPerson());
		assertEquals(subscription3.getPerson(), person1);
		
		assertEquals(0, person2.getPreferredSubscriptions().size());
		
		// Test removeAll
		assertTrue(person1.getPreferredSubscriptions().removeAll(Arrays.asList(subscription1, subscription3)));
		assertEquals(0, person1.getPreferredSubscriptions().size());
		
		assertNull(subscription1.getPerson());
		assertNull(subscription2.getPerson());
		assertNull(subscription3.getPerson());
		
		assertEquals(0, person2.getPreferredSubscriptions().size());
		
		// Test clear
		person2.getPreferredSubscriptions().add(subscription1);
		person2.getPreferredSubscriptions().add(subscription2);
		person2.getPreferredSubscriptions().add(subscription3);
		
		assertEquals(3, person2.getPreferredSubscriptions().size());
		assertTrue(person2.getPreferredSubscriptions().contains(subscription1));
		assertTrue(person2.getPreferredSubscriptions().contains(subscription2));
		assertTrue(person2.getPreferredSubscriptions().contains(subscription3));
		assertEquals(subscription1.getPerson(), person2);
		assertEquals(subscription2.getPerson(), person2);
		assertEquals(subscription3.getPerson(), person2);
		
		assertEquals(0, person1.getPreferredSubscriptions().size());
		
		person2.getPreferredSubscriptions().clear();
		assertEquals(0, person2.getPreferredSubscriptions().size());
		assertNull(subscription1.getPerson());
		assertNull(subscription2.getPerson());
		assertNull(subscription3.getPerson());
	}
	
	@Test
	public void testManyToMany() {
		Person person1 = new Person();
		Person person2 = new Person();
		
		Company company1 = new Company();
		Company company2 = new Company();
		
		person1.getWorksForCompanies().add(company1);
		assertEquals(1, company1.getEmployees().size());
		assertEquals(1, person1.getWorksForCompanies().size());
		assertTrue(company1.getEmployees().contains(person1));
		assertTrue(person1.getWorksForCompanies().contains(company1));
		
		company1.getEmployees().add(person2);
		assertEquals(2, company1.getEmployees().size());
		assertEquals(1, person1.getWorksForCompanies().size());
		assertEquals(1, person2.getWorksForCompanies().size());
		assertTrue(company1.getEmployees().contains(person1));
		assertTrue(person1.getWorksForCompanies().contains(company1));
		assertTrue(company1.getEmployees().contains(person2));
		assertTrue(person2.getWorksForCompanies().contains(company1));
		
		company2.getEmployees().add(person1);
		assertEquals(company2.getEmployees().size(), 1);
		assertEquals(person1.getWorksForCompanies().size(), 2);
		assertTrue(company2.getEmployees().contains(person1));
		assertTrue(person1.getWorksForCompanies().contains(company1));
		assertTrue(person1.getWorksForCompanies().contains(company2));
		
		person2.getWorksForCompanies().add(company2);
		assertEquals(company2.getEmployees().size(), 2);
		assertEquals(person2.getWorksForCompanies().size(), 2);
		assertTrue(company2.getEmployees().contains(person2));
		assertTrue(company2.getEmployees().contains(person1));
		assertTrue(person2.getWorksForCompanies().contains(company1));
		assertTrue(person2.getWorksForCompanies().contains(company2));
		
		person1.getWorksForCompanies().remove(company1);
		assertEquals(1, person1.getWorksForCompanies().size());
		assertEquals(1, company1.getEmployees().size());
		assertFalse(company1.getEmployees().contains(person1));
		assertFalse(person1.getWorksForCompanies().contains(company1));
		assertTrue(company2.getEmployees().contains(person1));
		assertTrue(person1.getWorksForCompanies().contains(company2));
		assertTrue(company1.getEmployees().contains(person2));
		assertTrue(person2.getWorksForCompanies().contains(company1));
		assertTrue(company2.getEmployees().contains(person2));
		assertTrue(person2.getWorksForCompanies().contains(company2));
		
		company2.getEmployees().remove(person1);
		assertEquals(0, person1.getWorksForCompanies().size());
		assertEquals(1, company1.getEmployees().size());
		assertEquals(1, company2.getEmployees().size());
		assertFalse(company1.getEmployees().contains(person1));
		assertFalse(person1.getWorksForCompanies().contains(company1));
		assertFalse(company2.getEmployees().contains(person1));
		assertFalse(person1.getWorksForCompanies().contains(company2));
		assertTrue(company1.getEmployees().contains(person2));
		assertTrue(person2.getWorksForCompanies().contains(company1));
		assertTrue(company2.getEmployees().contains(person2));
		assertTrue(person2.getWorksForCompanies().contains(company2));
		
		company1.getEmployees().remove(person2);
		assertEquals(0, company1.getEmployees().size());
		assertEquals(1, person2.getWorksForCompanies().size());
		assertFalse(company1.getEmployees().contains(person1));
		assertFalse(person1.getWorksForCompanies().contains(company1));
		assertFalse(company2.getEmployees().contains(person1));
		assertFalse(person1.getWorksForCompanies().contains(company2));
		assertFalse(company1.getEmployees().contains(person2));
		assertFalse(person2.getWorksForCompanies().contains(company1));
		assertTrue(company2.getEmployees().contains(person2));
		assertTrue(person2.getWorksForCompanies().contains(company2));
	}
}
