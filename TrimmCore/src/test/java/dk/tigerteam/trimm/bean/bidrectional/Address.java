/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.bean.bidrectional;

import dk.tigerteam.trimm.bean.bidirectional.ManyToOneWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManySetWrapper;
import dk.tigerteam.trimm.collection.WrappedCollection;

import java.util.HashSet;
import java.util.Set;

public class Address {
	private Person person;
	private Set<ZipCode> zipCodes = new HashSet<ZipCode>();
	
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		new ManyToOneWrapper<Person, Address>(this) {
			private static final long serialVersionUID = 7586103716125954405L;

			@Override
			protected void addManySideObjectToOneSideCollection(Person oneSide,
					Address manySide) {
				((WrappedCollection<Address>)oneSide.getAddresses()).getWrappedCollection().add(manySide);
			}

			@Override
			protected Person getOneSideObjectInManySideObject(Address manySide) {
				return manySide.getPerson();
			}

			@Override
			protected void removeManySideObjectFromOneSideCollection(
					Person oneSide, Address manySide) {
				((WrappedCollection<Address>)oneSide.getAddresses()).getWrappedCollection().remove(manySide);
			}

			@Override
			protected void setOneSideObjectInManySideObject(Address manySide,
					Person oneSide) {
				Address.this.person = oneSide;
			}
		}.updateOneSideObject(person);
	}
	
	public Set<ZipCode> getZipCodes() {
		return new OneToManySetWrapper<Address, ZipCode>(this, zipCodes) {
			private static final long serialVersionUID = 3189806679650530868L;

			@Override
			protected Address getOneSideObjectInManySideObject(
					ZipCode manySideObject) {
				return manySideObject.getAddress();
			}

			@Override
			protected void setOneSideObjectInManySideObject(
					ZipCode manySideObject, Address oneSideObject) {
				manySideObject.setAddress(oneSideObject);
			}
		};
	}
}
