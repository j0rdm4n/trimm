/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.bean.bidrectional;

import dk.tigerteam.trimm.bean.bidirectional.ManyToManyCollectionWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManyCollectionWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManyListWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToOneWrapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Person {
	
	private PersonName personName;
	private Collection<Address> addresses = new ArrayList<Address>();
	private List<Subscription> preferredSubscriptions = new ArrayList<Subscription>();
 
	private Collection<Company> worksForCompanies = new LinkedList<Company>();
	
	public Collection<Company> getWorksForCompanies() {
		return new ManyToManyCollectionWrapper<Person, Company>(this, worksForCompanies) {
			private static final long serialVersionUID = -3640538189637514632L;

			@Override
			protected Collection<Person> getSourceCollectionInTarget(Company o) {
				return o.getEmployees();
			}
		};
	}
	
	public PersonName getPersonName() {
		return personName;
	}

	public void setPersonName(PersonName personName) {
		new OneToOneWrapper<PersonName>() {
			private static final long serialVersionUID = -8114342902155467017L;

			@Override
			protected PersonName getTargetObjectInSourceObject() {
				return Person.this.personName;
			}
			
			@Override
			protected void setTargetObjectInSourceObject(PersonName newTarget) {
				Person.this.personName = newTarget;
			}

			@Override
			protected void createRelationToTargetObject(PersonName newTarget) {
				newTarget.setPerson(Person.this);
			}

			@Override
			protected void removeRelationToTargetObject(PersonName currentTarget) {
				currentTarget.setPerson(null);
			}
		}.updateTargetProperty(personName);
	}

	public Collection<Address> getAddresses() {
		return new OneToManyCollectionWrapper<Person, Address>(this, addresses) {
			private static final long serialVersionUID = -449729490324113855L;

			@Override
			protected Person getOneSideObjectInManySideObject(Address manySideObject) {
				return manySideObject.getPerson();
			}

			@Override
			protected void setOneSideObjectInManySideObject(Address manySideObject, Person oneSideObject) {
				manySideObject.setPerson(oneSideObject);
			}
		};
	}
	
	public List<Subscription> getPreferredSubscriptions() {
		return new OneToManyListWrapper<Person, Subscription>(this, preferredSubscriptions) {
			private static final long serialVersionUID = -1875360666584459266L;

			@Override
			protected Person getOneSideObjectInManySideObject(
					Subscription manySideObject) {
				return manySideObject.getPerson();
			}

			@Override
			protected void setOneSideObjectInManySideObject(
					Subscription manySideObject, Person oneSideObject) {
				manySideObject.setPerson(oneSideObject);
			}
			
		};
	}
}