/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.bean.bidrectional;

import dk.tigerteam.trimm.bean.bidirectional.OneToOneWrapper;

public class PersonName {
	private Person person;

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		new OneToOneWrapper<Person>() {
			private static final long serialVersionUID = 4648650090262307323L;

			@Override
			protected void createRelationToTargetObject(Person newTarget) {
				newTarget.setPersonName(PersonName.this);
			}

			@Override
			protected Person getTargetObjectInSourceObject() {
				return PersonName.this.person;
			}

			@Override
			protected void removeRelationToTargetObject(Person currentTarget) {
				currentTarget.setPersonName(null);
			}

			@Override
			protected void setTargetObjectInSourceObject(Person newTarget) {
				PersonName.this.person = newTarget;
			}
		}.updateTargetProperty(person);
	}
}
