/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.bean.bidrectional;

import dk.tigerteam.trimm.bean.bidirectional.ManyToOneWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManyListWrapper;

public class Subscription {
	
	private Person person;

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		new ManyToOneWrapper<Person, Subscription>(this) {
			private static final long serialVersionUID = -3001918919229074576L;

			@SuppressWarnings("unchecked")
			@Override
			protected void addManySideObjectToOneSideCollection(Person oneSide,
					Subscription manySide) {
				((OneToManyListWrapper<Person, Subscription>)oneSide.getPreferredSubscriptions()).getWrappedCollection().add(manySide);
			}

			@Override
			protected Person getOneSideObjectInManySideObject(
					Subscription manySide) {
				return manySide.getPerson();
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void removeManySideObjectFromOneSideCollection(
					Person oneSide, Subscription manySide) {
				((OneToManyListWrapper<Person, Subscription>)oneSide.getPreferredSubscriptions()).getWrappedCollection().remove(manySide);				
			}

			@Override
			protected void setOneSideObjectInManySideObject(
					Subscription manySide, Person oneSide) {
				Subscription.this.person = oneSide;
			}
			
		}.updateOneSideObject(person);
	}
}
