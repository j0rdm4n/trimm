/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.uml2.xmi;

import dk.tigerteam.trimm.mdsd.meta.*;
import junit.framework.Assert;
import org.junit.Test;

import static junit.framework.Assert.*;

/**
 * Created by IntelliJ IDEA.
 * User: jeppe
 * Date: 01/01/12
 * Time: 13.29
 * To change this template use File | Settings | File Templates.
 */
public abstract class XmiReaderTest {
    public abstract MetaModel getMetaModel();

    @Test
    public void testReader() {
        assertNotNull(getMetaModel());
    }

    @Test
    public void testPackages() {
        MetaModel mm = getMetaModel();
        MetaPackage rootPackage = mm.getPackageByNameRequired("EAXmiReaderTest");
        assertEquals("EAXmiReaderTest", rootPackage.getName());
        assertTrue(rootPackage.isRootPackage());
        Assert.assertEquals(rootPackage, getMetaModel().getRootPackage());
        assertTrue(rootPackage.hasStereoType("PackageStereotype"));
        assertEquals("EAXmiReaderTest package test", rootPackage.getDocumentation());
        // Contains Interface1, Interface2, Class1, Class2, Class3 and AssociationClass (notice interfaces are also classes in our hierarchy)
        assertEquals(6, rootPackage.getClazzesInPackage().size());
        assertEquals(2, rootPackage.getInterfacesInPackage().size());

        // Test subpackage
        MetaPackage subPackage = rootPackage.getPackageByNameRequired("SubPackage");
        assertEquals("SubPackage", subPackage.getName());
        assertFalse(subPackage.isRootPackage());
        assertTrue(subPackage.hasStereoType("SubPackageStereotype"));
        assertEquals("package_tagged_value", subPackage.getTaggedValue("PackageTaggedValue"));
        // Contains Class4 and Class 5
        assertEquals(2, subPackage.getClazzesInPackage().size());

        // Test parent
        assertEquals(mm, rootPackage.getOwner());
        assertEquals(rootPackage, subPackage.getOwner());
    }

    @Test
    public void testInterfaces() {
        MetaModel mm = getMetaModel();
        MetaPackage rootPackage = mm.getRootPackage();
        assertEquals(2, rootPackage.findImmediateChildrenOfType(MetaInterface.class).size());

        // Interface1
        MetaInterface interface1 = rootPackage.getInterfaceByNameRequired("Interface1");
        {
            assertTrue(interface1.hasStereoType("Stereotype1"));
            assertEquals("interface_tagged_value", interface1.getTaggedValue("InterfaceTaggedValue"));
            assertEquals("Interface1 documentation", interface1.getDocumentation());
            // Check realization
            assertEquals(1, interface1.getRealizedBy().size());
            assertEquals("Class1", interface1.getRealizedBy().iterator().next().getName());
            assertTrue(interface1.getRealizedBy().iterator().next().getRealizations().contains(interface1));

            // Operation 1
            MetaOperation operation1 = interface1.getOperationByName("operation1");
            assertNotNull(operation1);
            assertEquals("Operation1 documentation", operation1.getDocumentation());
            assertEquals("boolean", operation1.getReturnParameter().getType().getName());
            assertEquals(true, operation1.getReturnParameter().getType().isBuiltInType());
            assertEquals(MetaParameter.Direction.Return, operation1.getReturnParameter().getDirection());

            assertEquals(2, operation1.getParameters().size());
            // Param1
            assertEquals("param1", operation1.getParameters().get(0).getName());
            assertEquals("int", operation1.getParameters().get(0).getType().getName());
            assertEquals(true, operation1.getParameters().get(0).getType().isBuiltInType());
            assertEquals("Param1 documentation", operation1.getParameters().get(0).getDocumentation());
            assertEquals(1, operation1.getParameters().get(0).getMinCardinality());
            assertEquals(1, operation1.getParameters().get(0).getMaxCardinality());
            assertEquals(MetaParameter.Direction.In, operation1.getParameters().get(0).getDirection());

            // Param2
            assertEquals("param2", operation1.getParameters().get(1).getName());
            assertEquals("boolean", operation1.getParameters().get(1).getType().getName());
            assertEquals(true, operation1.getParameters().get(1).getType().isBuiltInType());
            assertEquals("Param2 documentation", operation1.getParameters().get(1).getDocumentation());
            assertEquals(MetaParameter.Direction.In, operation1.getParameters().get(1).getDirection());
        }

        // Interface2
        MetaInterface interface2 = rootPackage.getInterfaceByNameRequired("Interface2");
        {
            // Check realization
            assertEquals(1, interface2.getRealizedBy().size());
            assertEquals("Class1", interface2.getRealizedBy().iterator().next().getName());
            assertTrue(interface2.getRealizedBy().iterator().next().getRealizations().contains(interface2));

            // Operation 2
            MetaOperation operation2 = interface2.getOperationByName("operation2");
            assertNotNull(operation2);
            assertTrue(operation2.hasStereoType("OperationStereotype"));
            assertEquals("void", operation2.getReturnParameter().getType().getName());
            assertEquals(true, operation2.getReturnParameter().getType().isBuiltInType());
            assertEquals(MetaParameter.Direction.Return, operation2.getReturnParameter().getDirection());

            assertEquals(3, operation2.getParameters().size());

            // Param1
            assertEquals("param1", operation2.getParameters().get(0).getName());
            assertEquals(interface1, operation2.getParameters().get(0).getType());
            assertEquals(false, operation2.getParameters().get(0).getType().isBuiltInType());
            assertEquals(MetaParameter.Direction.In, operation2.getParameters().get(0).getDirection());
            assertTrue(operation2.getParameters().get(0).hasStereoType("ParamStereoType"));
            assertEquals(0, operation2.getParameters().get(0).getMinCardinality());
            assertEquals(MetaParameter.CARDINALITY_UNLIMITED, operation2.getParameters().get(0).getMaxCardinality());

            // Param2
            assertEquals("outParam", operation2.getParameters().get(1).getName());
            assertEquals(interface2, operation2.getParameters().get(1).getType());
            assertEquals(false, operation2.getParameters().get(1).getType().isBuiltInType());
            assertEquals(MetaParameter.Direction.Out, operation2.getParameters().get(1).getDirection());
            assertEquals(1, operation2.getParameters().get(1).getMinCardinality());
            assertEquals(1, operation2.getParameters().get(1).getMaxCardinality());

            // Param3
            assertEquals("inOutParam", operation2.getParameters().get(2).getName());
            assertEquals("double", operation2.getParameters().get(2).getType().getName());
            assertEquals(true, operation2.getParameters().get(2).getType().isBuiltInType());
            assertEquals(MetaParameter.Direction.InOut, operation2.getParameters().get(2).getDirection());
            assertEquals(1, operation2.getParameters().get(2).getMinCardinality());
            assertEquals(1, operation2.getParameters().get(2).getMaxCardinality());
        }

        // Check association Interface1 -> Interface2
        assertEquals(1, interface1.getPropertiesPartInAnAssociation().size());
        MetaProperty metaProperty = interface1.getPropertiesPartInAnAssociation().get(0);
        assertEquals(metaProperty.getOwner(), interface1);
        assertEquals("Interface2Role", metaProperty.getName());
        assertEquals("Interface2Role documentation", metaProperty.getDocumentation());
        assertEquals(MetaProperty.CardinalityType.SingleRequired, metaProperty.getCardinalityType());
        assertEquals(1, metaProperty.getMinCardinality());
        assertEquals(1, metaProperty.getMaxCardinality());
        assertTrue(metaProperty.isPartInAnAssociation());
        assertEquals(MetaProperty.Aggregation.Shared, metaProperty.getAggregation());
        assertTrue(metaProperty.isOwnerOfAssociation());

        // Association
        assertEquals("Interface1Interface2", metaProperty.getAssociation().getName());
        assertEquals(MetaProperty.AssociationType.OneToOne, metaProperty.getAssociationType());
        assertTrue(metaProperty.getAssociation().isBidirectional());
        assertFalse(metaProperty.getAssociation().isSelfReferencing());
        assertEquals("Interface1Interface2 association documentation", metaProperty.getAssociation().getDocumentation());

        // Opposite Property
        MetaProperty oppositePropertyInAssociation = metaProperty.getOppositePropertyInAssociation();
        assertEquals(oppositePropertyInAssociation.getOwner(), interface2);
        assertFalse(oppositePropertyInAssociation.isOwnerOfAssociation());
        assertEquals("Interface1Role", oppositePropertyInAssociation.getName());
        assertEquals("Interface1Role documentation", oppositePropertyInAssociation.getDocumentation());
        assertEquals(MetaProperty.CardinalityType.SingleRequired, oppositePropertyInAssociation.getCardinalityType());
        assertEquals(1, oppositePropertyInAssociation.getMinCardinality());
        assertEquals(1, oppositePropertyInAssociation.getMaxCardinality());
        assertTrue(oppositePropertyInAssociation.isPartInAnAssociation());
        assertEquals(MetaProperty.Aggregation.None, oppositePropertyInAssociation.getAggregation());
    }

    @Test
    public void testClass1() {
        MetaModel mm = getMetaModel();
        MetaPackage rootPackage = mm.getRootPackage();
        MetaClazz class1 = rootPackage.getClazzByNameRequired("Class1");
        assertTrue(class1.hasStereoType("ClassStereoType"));

        // Test inheritance
        assertNull(class1.getSuperClazz());
        assertEquals(2, class1.getSubClazzes().size());
        assertTrue(class1.getSubClazzes().contains(rootPackage.getClazzByNameRequired("Class2")));
        assertTrue(class1.getSubClazzes().contains(rootPackage.getClazzByNameRequired("Class3")));

        // Test Realizations
        assertEquals(2, class1.getRealizations().size());
        assertTrue(class1.getRealizations().contains(rootPackage.getInterfaceByNameRequired("Interface1")));
        MetaInterface interface2 = rootPackage.getInterfaceByNameRequired("Interface2");
        assertTrue(class1.getRealizations().contains(interface2));

        // Test association Class1Interface2
        assertEquals(1, class1.getPropertiesPartInAnAssociation().size());
        MetaProperty interface2Property = class1.getPropertiesPartInAnAssociation().get(0);

        assertEquals(interface2Property.getOwner(), class1);
        assertEquals("Interface2Role", interface2Property.getName());
        assertEquals(MetaProperty.CardinalityType.SingleRequired, interface2Property.getCardinalityType());
        assertEquals(1, interface2Property.getMinCardinality());
        assertEquals(1, interface2Property.getMaxCardinality());
        assertTrue(interface2Property.isPartInAnAssociation());
        assertEquals(MetaProperty.Aggregation.Composite, interface2Property.getAggregation());
        assertTrue(interface2Property.isOwnerOfAssociation());

        // Association
        assertEquals("Class1Interface2", interface2Property.getAssociation().getName());
        assertEquals(MetaProperty.AssociationType.OneToOne, interface2Property.getAssociationType());
        assertFalse(interface2Property.getAssociation().isBidirectional());
        assertFalse(interface2Property.getAssociation().isSelfReferencing());

        // Opposite Property
        MetaProperty oppositePropertyInAssociation = interface2Property.getOppositePropertyInAssociation();
        assertEquals(oppositePropertyInAssociation.getOwner(), interface2);
        assertFalse(oppositePropertyInAssociation.isOwnerOfAssociation());
        assertEquals("Class1Role", oppositePropertyInAssociation.getName());
        assertEquals(MetaProperty.CardinalityType.SingleRequired, oppositePropertyInAssociation.getCardinalityType());
        assertEquals(1, oppositePropertyInAssociation.getMinCardinality());
        assertEquals(1, oppositePropertyInAssociation.getMaxCardinality());
        assertTrue(oppositePropertyInAssociation.isPartInAnAssociation());
        assertEquals(MetaProperty.Aggregation.None, oppositePropertyInAssociation.getAggregation());

        // Test operations
        {
            // Operation 1
            MetaOperation operation1 = class1.getOperationByName("class1Operation1");
            assertNotNull(operation1);
            assertEquals(rootPackage.getClazzByNameRequired("Class2"), operation1.getReturnParameter().getType());
            assertEquals(false, operation1.getReturnParameter().getType().isBuiltInType());
            assertEquals(MetaParameter.Direction.Return, operation1.getReturnParameter().getDirection());

            assertEquals(1, operation1.getParameters().size());
            // Param1
            assertEquals("param1", operation1.getParameters().get(0).getName());
            assertEquals(rootPackage.getClazzByNameRequired("Class3"), operation1.getParameters().get(0).getType());
            assertEquals(false, operation1.getParameters().get(0).getType().isBuiltInType());
            assertEquals(1, operation1.getParameters().get(0).getMinCardinality());
            assertEquals(1, operation1.getParameters().get(0).getMaxCardinality());

            // Operation 2
            MetaOperation operation2 = class1.getOperationByName("class1Operation2");
            assertNotNull(operation2);
            assertEquals("char", operation2.getReturnParameter().getType().getName());
            assertEquals(true, operation2.getReturnParameter().getType().isBuiltInType());
            assertEquals(MetaParameter.Direction.Return, operation2.getReturnParameter().getDirection());

            // No arguments
            assertEquals(0, operation2.getParameters().size());
        }

        // Test the simple properties
        if (canTestPropertiesWithBounds()) {
            assertEquals(7, class1.getPropertiesNotPartInAnAssociation().size());
        } else {
            assertEquals(5, class1.getPropertiesNotPartInAnAssociation().size());
        }

        MetaProperty attribute1 = class1.getPropertyByName("attribute1");
        assertFalse(attribute1.isPartInAnAssociation());
        assertEquals("Attribute1 documentation", attribute1.getDocumentation());
        assertTrue(attribute1.hasStereoType("AttributeStereoType"));
        assertEquals("attribute_tagged_value", attribute1.getTaggedValue("AttributeTaggedValue"));
        assertEquals("int", attribute1.getType().getName());
        assertEquals(true, attribute1.getType().isBuiltInType());
        assertEquals(1, attribute1.getMinCardinality());
        assertEquals(1, attribute1.getMaxCardinality());
        assertFalse(attribute1.isStatic());
        assertEquals(MetaType.Visibility.Private, attribute1.getVisibility());

        MetaProperty attribute2 = class1.getPropertyByName("attribute2");
        assertFalse(attribute2.isPartInAnAssociation());
        assertEquals("boolean", attribute2.getType().getName());
        assertEquals(true, attribute2.getType().isBuiltInType());
        assertEquals(0, attribute2.getMinCardinality());
        assertEquals(1, attribute2.getMaxCardinality());
        assertFalse(attribute2.isStatic());
        assertEquals(MetaType.Visibility.Public, attribute2.getVisibility());

        MetaProperty attribute3 = class1.getPropertyByName("attribute3");
        assertFalse(attribute3.isPartInAnAssociation());
        assertEquals(rootPackage.getInterfaceByNameRequired("Interface1"), attribute3.getType());
        assertEquals(false, attribute3.getType().isBuiltInType());
        assertEquals(1, attribute3.getMinCardinality());
        assertEquals(1, attribute3.getMaxCardinality());
        assertFalse(attribute3.isStatic());
        assertEquals(MetaType.Visibility.Protected, attribute3.getVisibility());


        MetaProperty attribute4 = class1.getPropertyByName("attribute4");
        assertFalse(attribute4.isPartInAnAssociation());
        assertEquals("long", attribute4.getType().getName());
        assertEquals(true, attribute4.getType().isBuiltInType());
        assertEquals(1, attribute4.getMinCardinality());
        assertEquals(1, attribute4.getMaxCardinality());
        assertNull(attribute4.getLowerBound());
        assertNull(attribute4.getUpperBound());
        assertTrue(attribute4.isStatic());
        assertEquals("123456", attribute4.getDefaultValue());
        assertEquals(MetaType.Visibility.Package, attribute4.getVisibility());

        MetaProperty attribute5 = class1.getPropertyByName("attribute5");
        assertFalse(attribute5.isPartInAnAssociation());
        assertEquals(rootPackage.getInterfaceByNameRequired("Interface2"), attribute5.getType());
        assertEquals(false, attribute5.getType().isBuiltInType());
        assertEquals(0, attribute5.getMinCardinality());
        assertEquals(MetaProperty.CARDINALITY_UNLIMITED, attribute5.getMaxCardinality());
        assertFalse(attribute5.isStatic());
        assertEquals(MetaType.Visibility.Private, attribute5.getVisibility());

        if (canTestPropertiesWithBounds()) {
            MetaProperty attribute6 = class1.getPropertyByName("attribute6");
            assertFalse(attribute6.isPartInAnAssociation());
            assertEquals("String", attribute6.getType().getName());
            assertEquals(true, attribute6.getType().isBuiltInType());
            assertEquals(1, attribute6.getMinCardinality());
            assertEquals(1, attribute6.getMaxCardinality());
            assertEquals(1L, attribute6.getLowerBound().longValue());
            assertEquals(255L, attribute6.getUpperBound().longValue());

            MetaProperty attribute7 = class1.getPropertyByName("attribute7");
            assertFalse(attribute7.isPartInAnAssociation());
            assertEquals("int", attribute7.getType().getName());
            assertEquals(true, attribute7.getType().isBuiltInType());
            assertEquals(1, attribute7.getMinCardinality());
            assertEquals(1, attribute7.getMaxCardinality());
            assertNull(attribute7.getLowerBound());
            assertEquals(4096L, attribute7.getUpperBound().longValue());
        }
    }

    /**
     * Does the XmiReader support properties with bounds (e.g. myProperty(1, 255) becomes a MetaProperty with  name myProperty and lowerBound 1 and upperBound 255)
     */
    protected abstract boolean canTestPropertiesWithBounds();

    @Test
    public void testClass2() {
        MetaModel mm = getMetaModel();
        MetaPackage rootPackage = mm.getRootPackage();
        MetaClazz class2 = rootPackage.getClazzByNameRequired("Class2");

        // Test inheritance and realizations
        assertEquals(rootPackage.getClazzByNameRequired("Class1"), class2.getSuperClazz());
        assertEquals(0, class2.getSubClazzes().size());
        assertEquals(0, class2.getRealizations().size());

        // Test properties and associations
        assertEquals(0, class2.getPropertiesNotPartInAnAssociation().size());
        // Class2->AssociationClass and Class2->Class3
        assertEquals(2, class2.getPropertiesPartInAnAssociation().size());

        // Test association Class2->AssociationClass
        {
            MetaProperty metaProperty = class2.getPropertyByName("AssociationClassRole");
            assertTrue(metaProperty.isPartInAnAssociation());
            assertEquals("AssociationClassRole", metaProperty.getName());
            assertEquals(MetaProperty.CardinalityType.SingleOptional, metaProperty.getCardinalityType());
            assertEquals(0, metaProperty.getMinCardinality());
            assertEquals(1, metaProperty.getMaxCardinality());
            assertEquals(MetaProperty.Aggregation.None, metaProperty.getAggregation());
            assertFalse(metaProperty.isOwnerOfAssociation());
            assertEquals(MetaProperty.AssociationType.OneToOne, metaProperty.getAssociationType());

            // Association
            assertEquals("Class2AssociationClass", metaProperty.getAssociation().getName());
            assertEquals(MetaProperty.AssociationType.OneToOne, metaProperty.getAssociationType());
            assertFalse(metaProperty.getAssociation().isBidirectional());
            assertFalse(metaProperty.getAssociation().isSelfReferencing());

            // Opposite Property
            MetaProperty oppositePropertyInAssociation = metaProperty.getOppositePropertyInAssociation();
            assertEquals(oppositePropertyInAssociation.getOwner(), rootPackage.getClazzByNameRequired("AssociationClass"));
            assertTrue(oppositePropertyInAssociation.isOwnerOfAssociation());
            assertEquals("Class2Role", oppositePropertyInAssociation.getName());
            assertEquals(MetaProperty.CardinalityType.SingleOptional, oppositePropertyInAssociation.getCardinalityType());
            assertEquals(0, oppositePropertyInAssociation.getMinCardinality());
            assertEquals(1, oppositePropertyInAssociation.getMaxCardinality());
            assertTrue(oppositePropertyInAssociation.isPartInAnAssociation());
            assertEquals(MetaProperty.Aggregation.Composite, oppositePropertyInAssociation.getAggregation());
            assertEquals(MetaProperty.AssociationType.OneToOne, oppositePropertyInAssociation.getAssociationType());
        }

        // Test association Class2->Class3
        {
            MetaProperty metaProperty = class2.getPropertyByName("Class3Role");
            assertTrue(metaProperty.isPartInAnAssociation());
            assertTrue(metaProperty.hasStereoType("RoleStereotype2"));
            assertEquals("source_tagged_value", metaProperty.getTaggedValue("SourceTaggedValue"));
            assertEquals("Class3Role", metaProperty.getName());
            assertEquals(MetaProperty.CardinalityType.Many, metaProperty.getCardinalityType());
            assertEquals(0, metaProperty.getMinCardinality());
            assertEquals(MetaProperty.CARDINALITY_UNLIMITED, metaProperty.getMaxCardinality());
            assertEquals(MetaProperty.Aggregation.Composite, metaProperty.getAggregation());
            assertTrue(metaProperty.isOwnerOfAssociation());
            assertEquals(MetaProperty.AssociationType.OneToMany, metaProperty.getAssociationType());

            // Association
            assertEquals("Class2-Class3", metaProperty.getAssociation().getName());
            assertTrue(metaProperty.getAssociation().hasStereoType("AssociationStereotype"));
            assertEquals("association_tagged_value", metaProperty.getAssociation().getTaggedValue("AssociationTaggedValue"));
            assertEquals(MetaProperty.AssociationType.OneToMany, metaProperty.getAssociationType());
            assertFalse(metaProperty.getAssociation().isBidirectional());
            assertFalse(metaProperty.getAssociation().isSelfReferencing());

            // Opposite Property
            MetaProperty oppositePropertyInAssociation = metaProperty.getOppositePropertyInAssociation();
            assertEquals(oppositePropertyInAssociation.getOwner(), rootPackage.getClazzByNameRequired("Class3"));
            assertFalse(oppositePropertyInAssociation.isOwnerOfAssociation());
            assertEquals("Class2Role", oppositePropertyInAssociation.getName());
            assertTrue(oppositePropertyInAssociation.hasStereoType("RoleStereotype1"));
            assertEquals("target_tagged_value", oppositePropertyInAssociation.getTaggedValue("TargetTaggedValue"));
            assertEquals(MetaProperty.CardinalityType.SingleOptional, oppositePropertyInAssociation.getCardinalityType());
            assertEquals(0, oppositePropertyInAssociation.getMinCardinality());
            assertEquals(1, oppositePropertyInAssociation.getMaxCardinality());
            assertTrue(oppositePropertyInAssociation.isPartInAnAssociation());
            assertEquals(MetaProperty.Aggregation.None, oppositePropertyInAssociation.getAggregation());
            assertEquals(MetaProperty.AssociationType.ManyToOne, oppositePropertyInAssociation.getAssociationType());
        }
    }

    @Test
    public void testAssociationClass() {
        MetaPackage rootPackage = getMetaModel().getRootPackage();
        MetaClazz associationClass = rootPackage.getClazzByNameRequired("AssociationClass");
        assertTrue(associationClass.hasStereoType("AssociationClassStereotype"));
        assertEquals("association_class_tagged_value", associationClass.getTaggedValue("AssociationClassTaggedValue"));
        assertEquals("AssociationClass documentation", associationClass.getDocumentation());

        // Test inheritance
        assertNull(associationClass.getSuperClazz());
        assertEquals(0, associationClass.getSubClazzes().size());

        // Test Realizations
        assertEquals(0, associationClass.getRealizations().size());

        // Test operations
        {
            // Operation 1
            MetaOperation operation1 = associationClass.getOperationByNameRequired("operation1");
            assertEquals("boolean", operation1.getReturnParameter().getType().getName());
            assertEquals(true, operation1.getReturnParameter().getType().isBuiltInType());
            assertEquals(MetaParameter.Direction.Return, operation1.getReturnParameter().getDirection());

            assertEquals(1, operation1.getParameters().size());
            // Param1
            assertEquals("param1", operation1.getParameters().get(0).getName());
            assertEquals("char", operation1.getParameters().get(0).getType().getName());
            assertEquals(true, operation1.getParameters().get(0).getType().isBuiltInType());
            assertEquals(1, operation1.getParameters().get(0).getMinCardinality());
            assertEquals(1, operation1.getParameters().get(0).getMaxCardinality());

        }

        // Test the simple properties
        assertEquals(1, associationClass.getPropertiesNotPartInAnAssociation().size());

        MetaProperty attribute1 = associationClass.getPropertyByName("attribute1");
        assertFalse(attribute1.isPartInAnAssociation());
        assertEquals("int", attribute1.getType().getName());
        assertEquals(true, attribute1.getType().isBuiltInType());
        assertEquals(1, attribute1.getMinCardinality());
        assertEquals(1, attribute1.getMaxCardinality());
        assertFalse(attribute1.isStatic());
        assertEquals(MetaType.Visibility.Private, attribute1.getVisibility());

        // Test associations (Class2->AssociationClass, Class3->AssociationClass, AssociationClass->Class5)
        // According to these rules:
        //        * ---------                ---------
        //        * |       |Role A   Role C |       |
        //        * |   A   |________________|   C   |
        //        * |       | 2     |      * |       |
        //        * ---------       |        ---------
        //        *                 |
        //        *             ---------
        //        *             |       |
        //        *             |   B   |
        //        *             |       |
        //        *             ---------
        //        *
        //        *             becomes...
        //        *
        //        * ---------                ---------                ---------
        //        * |       |Role A   Role B |       |Role B   Role C |       |
        //        * |   A   |________________|   B   |________________|   C   |
        //        * |       | 1            * |       | 2            1 |       |
        //        * ---------                ---------                ---------
        assertEquals(3, associationClass.getPropertiesPartInAnAssociation().size());
        // Association between AssociationClass and Class3 (A->B in the diagram above)
        {
            MetaProperty class3Role = associationClass.getPropertyByNameRequired("Class3Role");
            assertEquals(0, class3Role.getMinCardinality());
            assertTrue(class3Role.hasStereoType(MetaProperty.NOT_NULL_STEREOTYPE));
            assertEquals(1, class3Role.getMaxCardinality());
            assertTrue(class3Role.isPartInAnAssociation());
            assertEquals(MetaProperty.AssociationType.ManyToOne, class3Role.getAssociationType());

            MetaProperty oppositePropertyInAssociation = class3Role.getOppositePropertyInAssociation();
            assertEquals(0, oppositePropertyInAssociation.getMinCardinality());
            assertEquals(MetaProperty.CARDINALITY_UNLIMITED, oppositePropertyInAssociation.getMaxCardinality());
            assertEquals(rootPackage.getClazzByNameRequired("Class3"), oppositePropertyInAssociation.getOwnerClazz());
            assertEquals(MetaProperty.AssociationType.OneToMany, oppositePropertyInAssociation.getAssociationType());
        }
        // Association between AssociationClass and Class5 (B->C in the diagram above)
        {
            MetaProperty class5Role = associationClass.getPropertyByNameRequired("Class5Role");
            assertEquals(0, class5Role.getMinCardinality());
            assertTrue(class5Role.hasStereoType(MetaProperty.NOT_NULL_STEREOTYPE));
            assertEquals(1, class5Role.getMaxCardinality());
            assertTrue(class5Role.isPartInAnAssociation());
            assertEquals(MetaProperty.AssociationType.OneToOne, class5Role.getAssociationType());

            MetaProperty oppositePropertyInAssociation = class5Role.getOppositePropertyInAssociation();
            assertEquals(2, oppositePropertyInAssociation.getMinCardinality());
            assertEquals(2, oppositePropertyInAssociation.getMaxCardinality());
            assertEquals(rootPackage.getPackageByNameRequired("SubPackage").getClazzByNameRequired("Class5"), oppositePropertyInAssociation.getOwnerClazz());
            assertEquals(MetaProperty.AssociationType.OneToOne, oppositePropertyInAssociation.getAssociationType());
        }


        // Note: Association Class2->AssociationClass is already tested in testClass2()
    }

    @Test
    public void testClass3() {
        MetaPackage rootPackage = getMetaModel().getRootPackage();
        MetaClazz class3 = rootPackage.getClazzByNameRequired("Class3");

        // Test inheritance and realizations
        assertEquals(rootPackage.getClazzByNameRequired("Class1"), class3.getSuperClazz());
        assertEquals(0, class3.getSubClazzes().size());
        assertEquals(0, class3.getRealizations().size());

        // Test properties and associations
        assertEquals(0, class3.getPropertiesNotPartInAnAssociation().size());
        // Associations Class2->Class3, Class3->Class4 and Class3->AssociationClass->Class5
        assertEquals(3, class3.getPropertiesPartInAnAssociation().size());

        // Test Association Class3->Class4
        {
            MetaProperty class4Role = class3.getPropertyByNameRequired("Class4Role");
            assertTrue(class4Role.isPartInAnAssociation());
            assertEquals(0, class4Role.getMinCardinality());
            assertEquals(MetaProperty.CARDINALITY_UNLIMITED, class4Role.getMaxCardinality());
            assertEquals(MetaProperty.Aggregation.Shared, class4Role.getAggregation());
            assertEquals(MetaProperty.AssociationType.ManyToMany, class4Role.getAssociationType());
            // Association
            assertTrue(class4Role.getAssociation().isBidirectional());
            assertFalse(class4Role.getAssociation().isSelfReferencing());
            assertEquals("Class3-Class4", class4Role.getAssociation().getName());
            // Opposite property
            MetaProperty oppositePropertyInAssociation = class4Role.getOppositePropertyInAssociation();
            assertEquals("Class3Role", oppositePropertyInAssociation.getName());
            assertEquals(0, oppositePropertyInAssociation.getMinCardinality());
            assertEquals(MetaProperty.CARDINALITY_UNLIMITED, oppositePropertyInAssociation.getMaxCardinality());
            assertEquals(MetaProperty.Aggregation.None, oppositePropertyInAssociation.getAggregation());
            assertEquals(MetaProperty.AssociationType.ManyToMany, oppositePropertyInAssociation.getAssociationType());
        }

        // Association Class2->Class3 is tested in testClass2()
        // Association Class3->AssociationClass->Class5 is tested in testAssociationClass()
    }

    @Test
    public void testClass4() {
        MetaPackage rootPackage = getMetaModel().getRootPackage();
        // Check that Class4 can't be found in the Root package
        assertNull(rootPackage.getClazzByName("Class4"));
        MetaClazz class4 = rootPackage.getPackageByNameRequired("SubPackage").getClazzByNameRequired("Class4");

        // Test inheritance and realizations
        assertNull(class4.getSuperClazz());
        assertEquals(0, class4.getSubClazzes().size());
        assertEquals(0, class4.getRealizations().size());

        // Test properties and associations
        assertEquals(0, class4.getPropertiesNotPartInAnAssociation().size());
        // Associations Class3->Class4 and Class4->Class5
        assertEquals(2, class4.getPropertiesPartInAnAssociation().size());

        // Test Association Class4->Class5
        {
            MetaProperty class5Role = class4.getPropertyByNameRequired("Class5Role");
            assertTrue(class5Role.isPartInAnAssociation());
            assertTrue(class5Role.isOwnerOfAssociation());
            assertEquals(0, class5Role.getMinCardinality());
            assertEquals(1, class5Role.getMaxCardinality());
            assertEquals(MetaProperty.AssociationType.OneToOne, class5Role.getAssociationType());
            assertEquals(MetaProperty.Aggregation.None, class5Role.getAggregation());

            assertEquals("Class4-Class5", class5Role.getAssociation().getName());
            assertFalse(class5Role.getAssociation().isBidirectional());
            assertFalse(class5Role.getAssociation().isSelfReferencing());

            MetaProperty oppositePropertyInAssociation = class5Role.getOppositePropertyInAssociation();
            assertEquals("Class4Role", oppositePropertyInAssociation.getName());
            assertTrue(oppositePropertyInAssociation.isPartInAnAssociation());
            assertFalse(oppositePropertyInAssociation.isOwnerOfAssociation());
            assertEquals(1, oppositePropertyInAssociation.getMinCardinality());
            assertEquals(1, oppositePropertyInAssociation.getMaxCardinality());
            assertEquals(MetaProperty.AssociationType.OneToOne, oppositePropertyInAssociation.getAssociationType());
            assertEquals(MetaProperty.Aggregation.None, oppositePropertyInAssociation.getAggregation());

        }

        // Association Class3->Class4 is tested in testClass3()
    }

    @Test
    public void testClass5() {
        MetaPackage rootPackage = getMetaModel().getRootPackage();
        // Check that Class4 can't be found in the Root package
        assertNull(rootPackage.getClazzByName("Class5"));
        MetaClazz class5 = rootPackage.getPackageByNameRequired("SubPackage").getClazzByNameRequired("Class5");

        // Test inheritance and realizations
        assertNull(class5.getSuperClazz());
        assertEquals(0, class5.getSubClazzes().size());
        assertEquals(0, class5.getRealizations().size());

        // Test properties and associations
        assertEquals(0, class5.getPropertiesNotPartInAnAssociation().size());
        // Associations Class4->Class5, Class5->Class5 (which counts for two properties), Class5->AssociationClass->Class3
        assertEquals(4, class5.getPropertiesPartInAnAssociation().size());

        // Test Association Class5->Class5 (selfreferencing)
        {
            MetaProperty children = class5.getPropertyByNameRequired("children");
            MetaProperty parent = class5.getPropertyByNameRequired("parent");
            assertEquals(parent.getAssociation(), children.getAssociation());

            assertFalse(parent.getAssociation().isBidirectional());
            assertTrue(parent.getAssociation().isSelfReferencing());

            assertFalse(parent.isOwnerOfAssociation());
            assertEquals(1, parent.getMinCardinality());
            assertEquals(1, parent.getMaxCardinality());
            assertEquals(MetaProperty.CardinalityType.SingleRequired, parent.getCardinalityType());
            assertEquals(MetaProperty.Aggregation.None, parent.getAggregation());
            assertEquals(MetaProperty.AssociationType.ManyToOne, parent.getAssociationType());


            assertTrue(children.isOwnerOfAssociation());
            assertEquals(0, children.getMinCardinality());
            assertEquals(MetaProperty.CARDINALITY_UNLIMITED, children.getMaxCardinality());
            assertEquals(MetaProperty.CardinalityType.Many, children.getCardinalityType());
            assertEquals(MetaProperty.Aggregation.None, children.getAggregation());
            assertEquals(MetaProperty.AssociationType.OneToMany, children.getAssociationType());
        }

        // Associations Class4->Class5 and Class5->AssociationClass->Class3 are tested in testClass4() and testAssociationClass()
    }
}
