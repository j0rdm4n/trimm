/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.uml2.xmi.magicdraw;

import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.uml2.xmi.XmiReaderTest;

public class MagicDraw17XmiReaderTest extends XmiReaderTest {
    private static MetaModel metaModel;

    @Override
    public MetaModel getMetaModel() {
        if (metaModel == null) {
            metaModel = new MagicDraw17XmiReader().read(this.getClass().getResource("MagicDraw17XmiReaderTest.mdxml"));
        }
        return metaModel;
    }

    /**
     * Does the XmiReader support properties with bounds (e.g. myProperty(1, 255) becomes a MetaProperty with  name myProperty and lowerBound 1 and upperBound 255)
     */
    @Override
    protected boolean canTestPropertiesWithBounds() {
        return false;
    }

}