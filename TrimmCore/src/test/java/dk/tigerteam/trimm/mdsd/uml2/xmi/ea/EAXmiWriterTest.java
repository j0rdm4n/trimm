/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.uml2.xmi.ea;

import dk.tigerteam.trimm.mdsd.meta.*;
import dk.tigerteam.trimm.util.JDomWriter;
import dk.tigerteam.trimm.util.Utils;
import org.jdom2.Document;
import org.junit.AfterClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class EAXmiWriterTest {

  private static String testDir = "target/generated-sources/EAXmiWriterTest";
	EAMetaIdGenerator idGenerator = new EAMetaIdGenerator();


  @Test
	public void testSimpleClass() throws FileNotFoundException, IOException {
		MetaModel metaModel = new MetaModel("root");
		MetaClazz metaClazz1 = (MetaClazz) id(MetaClazz.class)
			.setName("Class1")
			.add(
				id(MetaProperty.class)
					.setType(
							(MetaClazz)new MetaClazz("EAJava_int")
								.setName("int")
						)
					.setName("intProperty"),
				id(MetaProperty.class)
					.setType(
							(MetaClazz)new MetaClazz("EAJava_string")
								.setName("string")
						)
					.setName("stringProperty"),
				id(MetaProperty.class)
					.setType(
							(MetaClazz)new MetaClazz("EAJava_long")
								.setName("long")
						)
					.setName("longProperty")
			);
		
		metaModel.add(
			id(MetaPackage.class)
				.setName("System")
				.add(
					metaClazz1
				)
		);
		
        EAXmiWriter xmiWriter = new EAXmiWriter();
        final Document document = xmiWriter.write(metaModel, "Test");
        JDomWriter.write(document, testDir,  "model.xml");
	}
	
	@Test
	public void testReadEaModelAndWriteItOutAgainSimple() throws FileNotFoundException, IOException {
		EAXmiReader reader = new EAXmiReader();
		MetaModel model = reader.read(this.getClass().getResource("/ea_reader_writer_test_simple.xml"));
		EAXmiWriter writer = new EAXmiWriter();
		final Document document = writer.write(model, "System");
        JDomWriter.write(document, testDir,  "writer_test_model_simple.xml");
	}
	
	@Test
	public void testReadEaModelAndWriteItOutAgain() throws FileNotFoundException, IOException {
		EAXmiReader reader = new EAXmiReader();
		MetaModel model = reader.read(this.getClass().getResource("/ea_reader_writer_test.xml"));
		EAXmiWriter writer = new EAXmiWriter();
		final Document document = writer.write(model, "System");
        JDomWriter.write(document, testDir,  "writer_test_model.xml");
	}

	private <T extends MetaType> T id(Class<T> tClass) {
		return idGenerator.createWithId(tClass);
		
	}

  @AfterClass
  public static void tearDown() throws Exception {
    File dir = new File(testDir);
    if(dir != null && dir.exists()) {
      Utils.deleteDirectory(dir);
    }
  }

}
