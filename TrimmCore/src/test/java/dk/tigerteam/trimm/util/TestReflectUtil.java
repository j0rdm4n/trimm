/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;


public class TestReflectUtil {

	@Test
	public void testGenericType() {
		Method method = ReflectUtils.getMethod(B.class, "getList", new Class[0]);
		Type genericReturnType = method.getGenericReturnType();
		if (genericReturnType instanceof ParameterizedType) {
			ParameterizedType paramReturnType = (ParameterizedType) genericReturnType;
			Type[] actualTypeArguments = paramReturnType.getActualTypeArguments();
			Type ownerType = paramReturnType.getOwnerType();
			Type rawType = paramReturnType.getRawType();
			System.out.println(Arrays.toString(actualTypeArguments));
			System.out.println(ownerType);
			System.out.println(rawType);
		}
		
		Class<?> declaringClass = method.getDeclaringClass();
		System.out.println("Declaring class: " + declaringClass);
		TypeVariable<?>[] declaringClassType = declaringClass.getTypeParameters();
		System.out.println("DeclaringClassType: " + Arrays.toString(declaringClassType));
		
		TypeVariable<?>[] actualClassTypes = B.class.getTypeParameters();
		System.out.println("ActualClassType: " + Arrays.toString(actualClassTypes));
		
		Type genericSuperclass = B.class.getGenericSuperclass();
		System.out.println("Generic Super class: " + genericSuperclass);
//		TypeVariable<?> type = actualClassTypes[0];
//		Type[] bounds = type.getBounds();
//		System.out.println("B Bounds: " + Arrays.toString(bounds));
		Type[] genericTypeResolver = ReflectUtils.genericTypeResolver(B.class, Object.class);
		System.out.println("Generic Type Resolver: " + Arrays.toString(genericTypeResolver));
		
		Type[] actualTypeArguments = ReflectUtils.getActualTypeArguments(B.class, Object.class);
		System.out.println("Actual Type Args: " + Arrays.toString(actualTypeArguments));
	}
}
