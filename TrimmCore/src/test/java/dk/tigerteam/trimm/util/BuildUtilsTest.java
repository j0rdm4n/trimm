/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import org.apache.commons.io.IOUtils;
import org.junit.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Lasse Cramon
 */
public class BuildUtilsTest {

  private static File staticFile;
  private static String staticFileName = "staticFile.txt";
  private File modifiableFile;
  private static String modifiableFileName = "modifiableFile.xml";
  private static String testResourceDir = "src/test/resources";
  private static String targetDir = "target";


  private void updateModifiableFile(String data) throws IOException {
    FileOutputStream outputStream = new FileOutputStream(modifiableFile);
    IOUtils.write(data, outputStream, "UTF-8");
    IOUtils.closeQuietly(outputStream);
    modifiableFile.setLastModified(new Date().getTime() + 100000000L);
    System.out.println("Modified file was updated '" + modifiableFile.lastModified() + "'");
  }

  public BuildUtilsTest() {}

  @BeforeClass
  public static void setUpClass() throws Exception {
    staticFile = new File(testResourceDir + File.separatorChar + staticFileName);
    System.out.println("Created staticFile '" + staticFile.getAbsolutePath() + "' lastmodfied '" + staticFile.lastModified() + "'");
    File target = new File(targetDir);
    System.out.println("Using target dir '" + target.getAbsolutePath() + "'");
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
    if(staticFile != null && staticFile.exists()) {
      staticFile.delete();
    }
    File tempDir = new File(targetDir + File.separatorChar + BuildUtils.TEMP_TIGER_DIR);
    if(tempDir.exists()) {
      Utils.deleteDirectory(tempDir);
    }
  }

  @Before
  public void setup() {
    modifiableFile = new File(testResourceDir + File.separatorChar + modifiableFileName);
    System.out.println("Created modifiableFile '" + modifiableFile.getAbsolutePath() + "' lastmodfied '" + modifiableFile.lastModified() + "'");
  }

  @After
  public void tearDown() {
    if(modifiableFile != null && modifiableFile.exists()) {
      modifiableFile.delete();
    }
  }

  /**
   * Test of shouldGenerate method, of class BuildUtils.
   */
  @Test
  public void testShouldGenerate_File_1() throws IOException {
    modifiableFile = new File(modifiableFile.getAbsolutePath());
    boolean shouldGenerate = BuildUtils.shouldGenerate(targetDir, staticFile, modifiableFile);
    assertTrue(shouldGenerate);
    shouldGenerate = BuildUtils.shouldGenerate(targetDir, staticFile, modifiableFile);
    assertFalse(shouldGenerate);
    updateModifiableFile("Hephey 666");
    shouldGenerate = BuildUtils.shouldGenerate(targetDir, staticFile, modifiableFile);
    assertTrue(shouldGenerate);
  }

}