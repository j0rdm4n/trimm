/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import java.lang.reflect.Field;

/**
 * A {@link Field} filter to be used with {@link ReflectUtils}
 * @author Jeppe Cramon
 */
public interface FieldFilter {
	/**
	 * Should the field be included?
	 * @param field The field to test
	 * @return true if the field should be included, otherwise return false
	 */
	boolean isOk(Field field);
}