/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import dk.tigerteam.trimm.mdsd.meta.MetaAssociation;
import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaType;

import java.util.*;

/**
 * Create a yUml.me diagram description from a given {@link MetaClazz}
 * 
 * @author Jeppe Cramon
 */
public class YUMLMeVisualizer {
	
	public String createYUmlMeClassDiagram(MetaClazz metaClazz) {

		final Builder builder = new Builder(metaClazz);
		recursivelyVisualizeMetaClazz(metaClazz, builder);
		builder.visualizeAssociations();
		return builder.getYUmlDiagramString();
	}

	private void recursivelyVisualizeMetaClazz(MetaClazz metaClazz, Builder builder) {
		if (builder.hasProcessedMetaClazz(metaClazz)) {
			return;
		}

		builder.startClazz(metaClazz);

		final List<MetaProperty> metaPropertyList = metaClazz.getPropertiesNotPartInAnAssociation();
		if (metaPropertyList.size() > 0) {
			builder.addClazzSectionSeparator();
			int index = 0;
			for (MetaProperty metaProperty : metaPropertyList) {
				if (index > 0) {
					builder.addSeparator();
				}
				builder.addProperty(metaProperty);
				index++;
			}
		}

		// TODO: Operations

		builder.endClazz(metaClazz);
		builder.markMetaClazzAsProcessed(metaClazz);

		// Visualize Simple classes which don't have associations
		if (metaPropertyList.size() > 0) {
			int index = 0;
			for (MetaProperty metaProperty : metaPropertyList) {
				if (!metaProperty.getType().isBuiltInType()) {
					if (index > 0) {
						builder.addSeparator();
					}
					visualizeSimpleClass(metaProperty.getType(), builder);
					index++;
				}
			}
		}

		for (MetaProperty metaProperty : metaClazz.getPropertiesPartInAnAssociation()) {
			if (metaProperty.isOwnerOfAssociation()) {
				builder.addAssociationToProcessLater(metaProperty.getAssociation());
				recursivelyVisualizeMetaClazz(metaProperty.getType(), builder);
			}
		}

		// Handle inheritance
		if (metaClazz.getSuperClazz() != null) {
			builder.addInheritance(metaClazz.getSuperClazz(), metaClazz);
			recursivelyVisualizeMetaClazz(metaClazz.getSuperClazz(), builder);
		}
		for (MetaClazz subClazz : metaClazz.getSubClazzes()) {
			builder.addInheritance(metaClazz, subClazz);
			recursivelyVisualizeMetaClazz(subClazz, builder);
		}
	}

	private void visualizeSimpleClass(MetaClazz metaClazz, Builder builder) {
		if (builder.hasProcessedMetaClazz(metaClazz)) {
			return;
		}

		builder.startClazz(metaClazz);

		final List<MetaProperty> metaPropertyList = metaClazz.getPropertiesNotPartInAnAssociation();
		if (metaPropertyList.size() > 0) {
			builder.addClazzSectionSeparator();
			int index = 0;
			for (MetaProperty metaProperty : metaPropertyList) {
				if (index > 0) {
					builder.addSeparator();
				}
				builder.addProperty(metaProperty);
				index++;
			}
		}

		// TODO: Operations

		builder.endClazz(metaClazz);
		builder.markMetaClazzAsProcessed(metaClazz);

		// Visualize Simple classes which don't have associations
		if (metaPropertyList.size() > 0) {
			int index = 0;
			for (MetaProperty metaProperty : metaPropertyList) {
				if (!metaProperty.getType().isBuiltInType()) {
					visualizeSimpleClass(metaProperty.getType(), builder);
					if (index > 0) {
						builder.addSeparator();
					}
					index++;
				}

			}
		}

	}

	private static class Builder {
		
		private StringBuilder builder = new StringBuilder();

		private Set<MetaClazz> metaClazzesProcessed = new HashSet<MetaClazz>();
		private MetaClazz rootClazz;
		private Set<MetaAssociation> associationsToProcessLater = new HashSet<MetaAssociation>();
		/**
		 * Key: Clazz, value: Set of SubClazzes alrady processed
		 */
		private Map<MetaClazz, Set<MetaClazz>> inheritancesProcessed = new HashMap<MetaClazz, Set<MetaClazz>>();

		private Builder(MetaClazz rootClazz) {
			this.rootClazz = rootClazz;
		}

		public void addInheritance(MetaClazz metaClazz, MetaClazz subClazz) {
			Set<MetaClazz> processedAlready = inheritancesProcessed.get(metaClazz);
			if (processedAlready != null && processedAlready.contains(subClazz)) {
				return;
			}

			if (processedAlready == null) {
				processedAlready = new HashSet<MetaClazz>();
				inheritancesProcessed.put(metaClazz, processedAlready);
			}
			processedAlready.add(subClazz);

			if (metaClazzesProcessed.size() > 0) {
				builder.append(", ");
			}

			builder.append("[" + metaClazz.getName() + "]^[" + subClazz.getName() + "]");

		}

		public boolean hasProcessedMetaClazz(MetaClazz metaClazz) {
			return metaClazzesProcessed.contains(metaClazz);
		}

		public void markMetaClazzAsProcessed(MetaClazz metaClazz) {
			metaClazzesProcessed.add(metaClazz);
		}

		public Builder startClazz(MetaClazz metaClazz) {
			if (metaClazzesProcessed.size() > 0) {
				builder.append(", ");
			}
			builder.append("[" + className(metaClazz));
			return this;
		}

		public Builder endClazz(MetaClazz metaClazz) {
			builder.append("]");
			return this;
		}

		public String className(MetaClazz metaClazz) {
			if (metaClazz.getPackage().equals(rootClazz.getPackage())) {
				return metaClazz.getAliasOrName();
			} else {
				return metaClazz.getPackage().getAliasOrName() + "::" + metaClazz.getAliasOrName();
			}
		}

		@SuppressWarnings("unused")
		public StringBuilder append(StringBuffer sb) {
			return builder.append(sb);
		}

		public StringBuilder append(String str) {
			return builder.append(str);
		}

		public StringBuilder append(CharSequence s) {
			return builder.append(s);
		}

		public StringBuilder append(boolean b) {
			return builder.append(b);
		}

		public StringBuilder append(char c) {
			return builder.append(c);
		}

		public StringBuilder append(int i) {
			return builder.append(i);
		}

		public StringBuilder append(long lng) {
			return builder.append(lng);
		}

		public StringBuilder append(float f) {
			return builder.append(f);
		}

		public StringBuilder append(double d) {
			return builder.append(d);
		}

		public StringBuilder append(Object obj) {
			return builder.append(obj);
		}

		public StringBuilder append(CharSequence s, int start, int end) {
			return builder.append(s, start, end);
		}

		public StringBuilder append(char[] str) {
			return builder.append(str);
		}

		public StringBuilder append(char[] str, int offset, int len) {
			return builder.append(str, offset, len);
		}

		public StringBuilder appendCodePoint(int codePoint) {
			return builder.appendCodePoint(codePoint);
		}

		public String getYUmlDiagramString() {
			return builder.toString();
		}

		public void addClazzSectionSeparator() {
			builder.append("|");
		}

		public void addProperty(MetaProperty metaProperty) {
			if (metaProperty.getVisibility() == MetaType.Visibility.Private) {
				builder.append("-");
			} else {
				builder.append("+");
			}
			builder.append(metaProperty.getAliasOrName());
			builder.append(" : ");
			builder.append(metaProperty.getType() != null ? metaProperty.getType().getAliasOrName() : "?");
			if (metaProperty.getMinCardinality() == 0) {
				builder.append(" (0..1)");
			}
		}

		public void addSeparator() {
			builder.append(";");
		}

		public Builder visualizeAssociations() {
			builder.append(", ");
			int index = 0;
			for (MetaAssociation metaAssociation : associationsToProcessLater) {
				MetaProperty metaProperty = null;
				MetaProperty oppositeProperty = null;
				if (metaAssociation.getOwnerProperty().isOwnerOfAssociation()) {
					metaProperty = metaAssociation.getOwnerProperty();
					oppositeProperty = metaAssociation.getOwnedProperty();
				} else {
					metaProperty = metaAssociation.getOwnedProperty();
					oppositeProperty = metaAssociation.getOwnerProperty();
				}
				if (index > 0) {
					builder.append(", ");
				}
				builder.append("[" + className(metaProperty.getOwnerClazz()) + "]");
				if (metaProperty.getAggregation() == MetaProperty.Aggregation.Composite) {
					builder.append("++");
				} else if (metaProperty.getAggregation() == MetaProperty.Aggregation.Shared) {
					builder.append("+");
				}
				// Opposite property
				if (oppositeProperty.getAssociation().isBidirectional()) {
					builder.append("<");
				}
				if (oppositeProperty.getAliasOrName() != null) {
					builder.append(oppositeProperty.getAliasOrName() + " ");
				}
				builder.append(cardinalityStr(oppositeProperty));

				// Property
				builder.append("-");
				if (metaProperty.getAliasOrName() != null) {
					builder.append(metaProperty.getAliasOrName() + " ");
				}
				builder.append(cardinalityStr(metaProperty));
				if (metaProperty.isOwnerOfAssociation() || metaProperty.getAssociation().isBidirectional()) {
					builder.append(">");
				}
				builder.append("[" + className(metaProperty.getType()) + "]");
				index++;
			}
			return this;
		}

		private String cardinalityStr(MetaProperty property) {
			if (property.getMinCardinality() == 0 && property.getMaxCardinality() == 1) {
				return "0..1";
			} else if (property.getMinCardinality() == 1 && property.getMaxCardinality() == 1) {
				return "1";
			} else if (property.getMinCardinality() == 0 && property.getMaxCardinality() == MetaProperty.CARDINALITY_UNLIMITED) {
				return "0..*";
			} else if (property.getMaxCardinality() == MetaProperty.CARDINALITY_UNLIMITED) {
				return Integer.toString(property.getMinCardinality()) + "..*";
			} else {
				return Integer.toString(property.getMinCardinality()) + ".." + Integer.toString(property.getMaxCardinality());
			}
		}

		public void addAssociationToProcessLater(MetaAssociation association) {
			associationsToProcessLater.add(association);
		}
	}
}
