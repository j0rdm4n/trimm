/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Encapsulates access and quering against a list of PropertyDescriptor's
 */
public class PropertyDescriptors {
	
    private List<PropertyDescriptor> descriptors;

    public PropertyDescriptors(List<PropertyDescriptor> descriptors) {
        this.descriptors = descriptors;
    }

    /**
     * Get all descriptors
     */
    public List<PropertyDescriptor> getDescriptors() {
        return descriptors;
    }

    /**
     * Get an optional PropertyDescriptor by a given Method. Checks all Descriptors read and write methods for a match
     * @param method The method for which we want a matching PropertyDescriptor
     * @return The matching PropertyDescriptor or <code>null</code> if no match was found
     */
    public PropertyDescriptor getOptionalDescriptor(Method method) {
        if (method == null) {
            throw new IllegalArgumentException("Method argument was null");
        }
        for (PropertyDescriptor descriptor : descriptors) {
            if (method.equals(descriptor.getReadMethod()) || method.equals(descriptor.getWriteMethod())) {
                return descriptor;
            }
        }

        return null;
    }

    /**
     * Get an required PropertyDescriptor by a given Method. Checks all Descriptors read and write methods for a match
     * @param method The method for which we want a matching PropertyDescriptor
     * @return The matching PropertyDescriptor
     * @throws IllegalArgumentException if no matching PropertyDescriptor was found
     */
    public PropertyDescriptor getDescriptor(Method method) {
        PropertyDescriptor propertyDescriptor = getOptionalDescriptor(method);
        if (propertyDescriptor == null) {
            throw new IllegalArgumentException("Can't find a PropertyDescriptor which has a read or write method which matches " + method);
        } else {
            return propertyDescriptor;
        }
    }

}
