/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import org.apache.commons.io.IOUtils;
import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Helper class to write out a JDom {@link Document} to a file.
 *
 * @author Jeppe Cramon
 */
public class JDomWriter {
    /**
     * Helper method to write out a JDom {@link Document} to a file.
     *
     * @param document            The JDom document
     * @param generateToDirectory The folder that we should place the file in (a missing trailing {@link File#separator} is added automatically by this method)
     * @param fileName            The name of the file to create
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String write(Document document, String generateToDirectory, String fileName) throws FileNotFoundException, IOException {
        FileOutputStream outputStream = null;
        try {
            File generatorDirectory = new File(generateToDirectory);
            if (!generatorDirectory.exists()) {
                generatorDirectory.mkdirs();
            }
            String fullPath = generatorDirectory.getAbsolutePath()
                    + (generatorDirectory.getAbsolutePath().endsWith(File.separator) ? "" : File.separator) + fileName;
            XMLOutputter serializer = new XMLOutputter();
            serializer.setFormat(Format.getPrettyFormat());
            outputStream = new FileOutputStream(fullPath);
            serializer.output(document, outputStream);
            return fullPath;
        } finally {
            IOUtils.closeQuietly(outputStream);
        }
    }
}
