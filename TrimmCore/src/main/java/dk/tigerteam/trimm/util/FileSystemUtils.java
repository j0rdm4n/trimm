/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Various File System utility methods
 * @author Jeppe Cramon
 */
public abstract class FileSystemUtils {
	
	private static final Log logger = LogFactory.getLog(FileSystemUtils.class);
	
	/**
	 * Delete Files (i.e. not directories) from the File System which are not listed in the files collection
	 * @param fileSystemRootDirectory The root directory from which we recursively will decent the file system
	 * @param files The files that shouldn't be deleted
	 * @return The files that were deleted
	 */
	public static List<File> deleteFilesFromFileSystemNotPresentInArgument(final String fileSystemRootDirectory, final Collection<File> files) {
		if (fileSystemRootDirectory == null) {
			return Collections.emptyList();
		}
		File rootDirectory = new File(fileSystemRootDirectory);
		if (!rootDirectory.exists()) {
			return Collections.emptyList();
		} else if (!rootDirectory.isDirectory()) {
			throw new IllegalArgumentException("Root directory '" + rootDirectory.getAbsolutePath() + "' either doesn't point to a directory");
		}
		
		final List<File> filesToDelete = new LinkedList<File>();
		
		recursivelyTraverseDirectory(rootDirectory, new TraversalStrategy() {
			public void execute(File file) {
				if (file.isFile() && !fileListContains(files, file) && !file.getAbsolutePath().contains("CVS") && !file.getAbsolutePath().contains(".svn")) {
					filesToDelete.add(file);
				}
			}
		});
		
		for (File file : filesToDelete) {
			logger.trace("Deleting file '" + file.getAbsolutePath() + "'");
			boolean result = file.delete();
			if(!result) {
				logger.warn("Failed to delete '" + file.getAbsolutePath() + "'");
			}
		}
		
		return filesToDelete;
	}
	
	public static boolean fileListContains(Collection<File> files, File file) {
		for (File fileInList : files) {
			if (fileInList.getAbsolutePath().equals(file.getAbsolutePath())) {
				return true;
			}
		}
		return false;
	}
	
	protected static void recursivelyTraverseDirectory(File rootDirectory,
			TraversalStrategy traversalStrategy) {
		traversalStrategy.execute(rootDirectory);
		for (File file : rootDirectory.listFiles()) {
			if (file.isDirectory()) {
				recursivelyTraverseDirectory(file, traversalStrategy);
			} else {
				traversalStrategy.execute(file);
			}
		}
	}


	/**
	 * Traversal Strategy
	 * @author Jeppe Cramon
	 */
	public static interface TraversalStrategy {
		void execute(File file);
	}
	
	/**
	 * Correct the path, so that the referencing file (typically the WSDL Document) can reference the location
	 * of the referencedFile (this typically means inserting several ../ sections to the path of the referenced file) 
	 * @param referencingFile The File the is supposed to reference the referencedFile
	 * @param referencedFile The file that is being referenced by the referencingFile
	 * @return The corrected relative path, so the referencingFile can correct reference the referencedFile (so any file readers can find the file correctly) 
	 */
	public static String correctRelativePath(String referencingFile, String referencedFile) {
		StringBuilder path = new StringBuilder();
		
		String[] referencedPathParts = referencedFile.split("/");
    int commonPathPartLength = 0;
    if (!referencingFile.equals("")) {
    	String[] pathParts = referencingFile.split("/");
	    String commonPath = null;
	    for (String pathPart : pathParts) {
	      if (commonPath == null) {
	        commonPath = pathPart;
	      } else {
	        commonPath = commonPath + "/" + pathPart;
	      }
	      int indexOfCommonPath = referencedFile.indexOf(commonPath);
	      if (indexOfCommonPath != -1) {
	        commonPathPartLength++;
	      } else {
	        break;
	      }
	    }
	
	    for (int i = 0; i < pathParts.length - commonPathPartLength; i++) { 
	      path.append("../");
	    }
    }
    
    for (int i = commonPathPartLength; i < referencedPathParts.length; i++) {
      if (!path.toString().endsWith("/") && path.length() > 0)
        path.append("/");

      path.append(referencedPathParts[i]);
    }

		return path.toString();
	}
}
