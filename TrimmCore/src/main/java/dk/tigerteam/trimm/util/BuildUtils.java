/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.util;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Build utility class that can detect if there has been changes between builds
 *
 * @author Lasse Cramon
 */
public final class BuildUtils {

  private static final Log logger = LogFactory.getLog(BuildUtils.class);

  public static final String TEMP_DIR = "java.io.tmpdir";
  public static final String TEMP_TIGER_DIR = ".TigerTrimm";
  public static final String DEFAULT_TEMP_FILENAME = "lastgenerate";
  public static final String DEFAULT_ENCODING = "UTF-8";
  public static final Character DIR_SEPERATOR = File.separatorChar;
  public static final String DEFAULT_OUTPUT_DIR = "target";
  public static final String MAVEN_JAVA_SRC_DIR = "src/main/java";
	public static final String MAVEN_JAVA_RESOURCES_DIR = "src/main/resources";

  public static File findOSTempDir() {
    String dir = System.getProperty(TEMP_DIR);
    File file = new File(dir);
    logger.debug("Using OS temp dir: '" + file.getAbsolutePath() + "'");
    return file;
  }

  public static File findTempDir(String targetPath) {
    File file = new File(targetPath);
    logger.debug("Using temp dir: '" + file.getAbsolutePath() + "'");
    return file;
  }

  public static File findTigerTempDir(File tempDir) {
    File tigerTempDir = new File(tempDir.getAbsolutePath() + DIR_SEPERATOR + TEMP_TIGER_DIR);
    if(!tigerTempDir.exists()) {
      logger.debug("Creating Trimm temp dir " + tigerTempDir.getAbsolutePath() + "'");
      tigerTempDir.mkdir();
    }
    return tigerTempDir;
  }

  public static File findTempFile(File tigerTempDir) {
    File[] files = tigerTempDir.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File file, String fileName) {
        if(fileName.equals(DEFAULT_TEMP_FILENAME)) {
          return true;
        }
        return false;
      }
    });
    if(files.length == 1) {
      return files[0];
    }
    return null;
  }
  
  private static boolean shouldGenerate(File file, String targetDir) throws FileNotFoundException, IOException {
    boolean shouldGenerate = false;
    File tempDir = findTempDir(targetDir);
    if(!tempDir.exists()) {
      return true;
    }
    File tigerTempDir = findTigerTempDir(tempDir);
    File tempFile = findTempFile(tigerTempDir);
    if(tempFile == null) {
      logger.debug("Did not find a temp file will create one and recommend that generate should proceed");
      shouldGenerate = true;
      tempFile = new File(tigerTempDir.getAbsolutePath() + DIR_SEPERATOR + DEFAULT_TEMP_FILENAME);
      String timestamp = createTimestamp();
      FileOutputStream outputStream = new FileOutputStream(tempFile);
      IOUtils.write(timestamp, outputStream, DEFAULT_ENCODING);
      IOUtils.closeQuietly(outputStream);
      logger.debug("Wrote new generate timestamp '" + timestamp + " to temp file");
    } else {
      long lastModified = file.lastModified();
      FileInputStream inputStream = new FileInputStream(tempFile);
      long fileModified = new Long(IOUtils.toString(inputStream));
      IOUtils.closeQuietly(inputStream);
      logger.debug("Found temp file checking lastModified from file resource '" + file.getName() + ": " + lastModified + "' against"
              + " temp file lastModified '" + fileModified + "'");
      if(lastModified > fileModified) {
        logger.debug("Resource file '" + file.getName() + "' has been modified sine last check against temp file last generate timestamp"
                + " recommend that generate proceed");
        shouldGenerate = true;
        String timestamp = createTimestamp();
        FileOutputStream outputStream = new FileOutputStream(tempFile);
        IOUtils.write(timestamp, outputStream, DEFAULT_ENCODING);
        IOUtils.closeQuietly(outputStream);
        logger.debug("Wrote new generate timestamp '" + timestamp + " to temp file");
      } else {
        logger.debug("Resource file '" + file.getName() + "' has not been modified since last generate");
      }
    }
    return shouldGenerate; 
  }

  public static boolean shouldGenerate(File file, String targetDir, boolean failOnError) {
    try {
      return shouldGenerate(file, targetDir);
    } catch (FileNotFoundException ex) {
      if(failOnError) {
        throw new RuntimeException(ex);
      } else {
        logger.warn("Exception occured during evaluation of generate should proceed " + ex.toString() );
      }
    } catch (IOException ex) {
      if(failOnError) {
        throw new RuntimeException(ex);
      } else {
        logger.warn("Exception occured during evaluation of generate should proceed " + ex.toString() );
      }
    }
    return true;
  }

  public static boolean shouldGenerate(String targetDir, File ... files) {
    boolean shouldgenerate = false;
    List<File> resourceFiles = Arrays.asList(files);
    for(File file : resourceFiles) {
      try {
        shouldgenerate = shouldGenerate(file, targetDir);
        if (shouldgenerate) {
          return true;
        }
      } catch (FileNotFoundException ex) {
        logger.warn("Exception occured during evaluation of generate should proceed " + ex.toString() );
      } catch (IOException ex) {
        logger.warn("Exception occured during evaluation of generate should proceed " + ex.toString());
      }
    }
    return shouldgenerate;
  }

  private static String createTimestamp() {
    String timestamp = new Long(new Date().getTime()).toString();
    return timestamp;
  }

  public static boolean doesPathsStartWithDefaultOutputDirectory(String outputDir, List<String> paths) {
    if(outputDir != null) {
      for(String path : paths) {
        if(outputDir.contains(DEFAULT_OUTPUT_DIR) && path.startsWith(DEFAULT_OUTPUT_DIR)) {
          return true;
        }
      }
    }
    return false;
  }

  public static boolean doesPathsStartWithDefaultOutputDirectory(String outputDir, String ... paths) {
    return doesPathsStartWithDefaultOutputDirectory(outputDir, Arrays.asList(paths));
  }

  public static boolean doesOutputDirectoriesExist(String ... outputDirs) {
    return doesOutputDirectoriesExist(Arrays.asList(outputDirs));
  }

  public static boolean doesOutputDirectoriesExist(List<String> outputDirs) {
    if(outputDirs != null) {
      for (String dir : outputDirs) {
        boolean exists = doesOutputDirectoryExist(dir);
        if(!exists) {
          return false;
        }
      }
    }
    return true;
  }

  public static boolean doesOutputDirectoryExist(String outputDir) {
    if(outputDir != null) {
      File outputDirectory = new File(outputDir);
      boolean exists = outputDirectory.exists();
      logger.debug("Does output directory '" + outputDirectory.getAbsolutePath() + "' exists '" + exists + "'");
      return exists;
    }
    return false;
  }

}
