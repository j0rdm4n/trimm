/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.util.*;

/**
 * Collection of general utility methods
 *
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class Utils {

    private static final Log logger = LogFactory.getLog(Utils.class);

    /**
     * Varargs to List
     *
     * @param ts
     * @return List<T>
     */
    public static <T> List<T> newList(T... ts) {
        if (ts == null || ts.length == 0) {
            return new ArrayList<T>(0);
        }
        List<T> result = new ArrayList<T>(ts.length);
        for (T t : ts) {
            result.add(t);
        }
        return result;
    }

    /**
     * Sort comparaple list
     *
     * @param source
     * @return sorted List
     */
    public static <T extends Comparable<T>> List<T> sort(List<T> source) {
        int size = source.size();
        if (size == 1) {
            return source;
        }

        List<T> result = new ArrayList<T>(size);
        int middle = size / 2;
        int left = 0;
        int right = middle;
        while (left < middle && right < size) {
            int value = source.get(left).compareTo(source.get(right));
            if (value > 0) {
                result.add(source.get(left));
                left++;
            } else if (value == 0) {
                result.add(source.get(left));
                result.add(source.get(right));
                left++;
                right++;
            } else {
                result.add(source.get(right));
                right++;
            }
        }
        while (left < middle) {
            result.add(source.get(left++));
        }
        while (right < size) {
            result.add(source.get(right++));
        }
        return result;

    }


    /**
     * Converts an Object that we know is an array to a List
     *
     * @param object
     * @return List
     */
    public static List<?> arrayObjectToList(Object object) {
        if (!object.getClass().isArray()) {
            throw new IllegalArgumentException("Parameter wasn't an array - Type '" + object.getClass() + "' with value '" + object + "'");
        }

        Class<?> componentType = object.getClass().getComponentType();
        if (componentType.isPrimitive()) {
            List<Object> result = new ArrayList<Object>();
            if (componentType == byte.class) {
                for (byte value : (byte[]) object) {
                    result.add(value);
                }
            } else if (componentType == int.class) {
                for (int value : (int[]) object) {
                    result.add(value);
                }
            } else if (componentType == short.class) {
                for (short value : (short[]) object) {
                    result.add(value);
                }
            } else if (componentType == long.class) {
                for (long value : (long[]) object) {
                    result.add(value);
                }
            } else if (componentType == char.class) {
                for (char value : (char[]) object) {
                    result.add(value);
                }
            } else if (componentType == double.class) {
                for (double value : (double[]) object) {
                    result.add(value);
                }
            } else if (componentType == float.class) {
                for (float value : (float[]) object) {
                    result.add(value);
                }
            } else if (componentType == boolean.class) {
                for (boolean value : (boolean[]) object) {
                    result.add(value);
                }
            } else {
                throw new IllegalArgumentException("Unsupported simple array type '" + componentType + "'");
            }
            return result;
        } else {
            Object[] objArray = (Object[]) object;
            return Arrays.asList(objArray);
        }
    }

    /**
     * Clean a directory recursively
     *
     * @param dir
     * @param excludeDirectories
     */
    public static void cleanDirectory(File dir, String... excludeDirectories) {
        Set<String> exclude = new HashSet<String>(Arrays.asList(excludeDirectories));
        if (dir.exists()) {
            logger.debug("Cleaning dir '" + dir.getAbsolutePath() + "'");
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory() && !exclude.contains(files[i].getName())) {
                    cleanDirectory(files[i], excludeDirectories);
                } else {
                    boolean result = files[i].delete();
                    if (!result) {
                        logger.debug("Failed to delete '" + files[i].getName() + "' in dir '" + dir.getAbsolutePath() + "'");
                    }
                }
            }
        }
    }

    /**
     * Delete a directory and all content under it
     *
     * @param dir
     * @return true if delete was successful otherwise false
     */
    public static boolean deleteDirectory(File dir) {
        if (dir.exists()) {
            logger.debug("Deleting directory '" + dir.getAbsolutePath() + "'");
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    boolean result = files[i].delete();
                    if (!result) {
                        logger.debug("Failed to delete '" + files[i].getName() + "' in dir '" + dir.getAbsolutePath() + "'");
                    }
                }
            }
        }
        return dir.delete();
    }

    /**
     * Delete a file
     *
     * @param file
     * @return true if delete was successful otherwise false
     */
    public static boolean deleteFile(File file) {
        if (file.isFile()) {
            logger.debug("Deleting file '" + file.getAbsolutePath() + "'");
            return file.delete();
        }
        return false;
    }

    /**
     * Check if a directory can be deleted
     *
     * @param dir
     * @return true if file is a directory and it is empty
     */
    public static boolean canDeleteDirectory(File dir) {
        if (dir.isDirectory() && dir.listFiles().length == 0) {
            return true;
        }
        return false;
    }

    /**
     * Read a files content and store it in a String
     *
     * @param file
     * @param encoding
     * @return String
     */
    public static String readFileToString(File file, String encoding) {
        BufferedReader reader = null;
        try {
            try {
                reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(
                        new FileInputStream(file)), encoding));
                StringBuilder builder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                return builder.toString();
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Write a String to a file
     *
     * @param file
     * @param payload
     * @param encoding
     */
    public static void writeStringToFile(File file, String payload, String encoding) {
        BufferedWriter writer = null;
        try {
            try {
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(file), encoding));
                writer.write(payload);
            } finally {
                if (writer != null) {
                    writer.close();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Check if to files String content is equal
     *
     * @param file1
     * @param file2
     * @param encoding
     * @return true if file String content is equal
     */
    public static boolean isFileStringContentEqual(File file1, File file2, String encoding) {
        String string1 = readFileToString(file1, encoding);
        String string2 = readFileToString(file2, encoding);
        if (string1.equals(string2)) {
            return true;
        }
        return false;
    }

    /**
     * Check if a file path ends with OS file separator
     *
     * @param path
     * @return true if path end with OS file separator
     */
    public static boolean doesPathEndWithFileSeperator(String path) {
        if (path.endsWith(File.separatorChar + "")) {
            return true;
        }
        return false;
    }

    /**
     * Take a List of Strings and return a comma separated String
     *
     * @param list
     * @return comma separated String
     */
    public static String listToString(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String str : list) {
            if (result.length() > 0) {
                result.append(", ");
            }
            result.append(str);
        }
        return result.toString();
    }

    /**
     * Capitalize a <code>String</code>, changing the first letter to
     * upper case as per {@link Character#toUpperCase(char)}.
     * No other letters are changed.
     *
     * @param str the String to capitalize, may be <code>null</code>
     * @return the capitalized String, <code>null</code> if null
     */
    public static String capitalize(String str) {
        return changeFirstCharacterCase(str, true);
    }

    /**
     * Uncapitalize a <code>String</code>, changing the first letter to
     * lower case as per {@link Character#toLowerCase(char)}.
     * No other letters are changed.
     *
     * @param str the String to uncapitalize, may be <code>null</code>
     * @return the uncapitalized String, <code>null</code> if null
     */
    public static String uncapitalize(String str) {
        return changeFirstCharacterCase(str, false);
    }

    private static String changeFirstCharacterCase(String str, boolean capitalize) {
        if (str == null || str.length() == 0) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str.length());
        if (capitalize) {
            sb.append(Character.toUpperCase(str.charAt(0)));
        } else {
            sb.append(Character.toLowerCase(str.charAt(0)));
        }
        sb.append(str.substring(1));
        return sb.toString();
    }

    /**
     * Helper method to insert single quotes (') around the text
     *
     * @param stringtoQuote The text to insert quotes around
     * @return The quoted string
     */
    public static String singleQuotedString(String stringtoQuote) {
        StringBuilder builder = new StringBuilder("'");
        builder.append(stringtoQuote);
        builder.append("'");
        return builder.toString();
    }


    /**
     * Helper method to insert quotes (") around the text
     *
     * @param stringtoQuote The text to insert quotes around
     * @return The quoted string
     */
    public static String quotedString(String stringtoQuote) {
        StringBuilder builder = new StringBuilder("\"");
        builder.append(stringtoQuote);
        builder.append("\"");
        return builder.toString();
    }

    /**
     * Returns a collection that applies {@code mapfunction} to each element of
     * {@code fromCollection}. The returned collection is a snapshot of {@code
     * fromCollection}; changes to one doesn't affect the other.
     */
    public static <FROM, TO> Collection<TO> map(Collection<FROM> fromCollection, Function<? super FROM, TO> mapFunction) {
        Collection<TO> result = new LinkedList<TO>();
        for (FROM from : fromCollection) {
            result.add(mapFunction.apply(from));
        }
        return result;
    }

    /**
     * Function for transforming one type of object to another
     *
     * @param <FROM>
     * @param <TO>
     * @author Jeppe cramon
     */
    public interface Function<FROM, TO> {
        TO apply(FROM from);
    }

    public static <T> Collection<T> filter(Collection<T> collection, Filter<T> filterFunction) {
        Collection<T> result = new LinkedList<T>();
        for (T t : collection) {
            if (filterFunction.include(t)) {
                result.add(t);
            }
        }
        return result;
    }

    public interface Filter<T> {
        boolean include(T t);
    }

    public static <T> T single(Collection<T> collection, Filter<T> filterFunction, String contextDescriptionForErrorHandling) {
        Collection<T> tCollection = filter(collection, filterFunction);
        if (tCollection.size() > 1) {
            throw new IllegalStateException("The collection contains " + tCollection.size() + " elements " + tCollection + " that matched the criteria. Expected 0 or 1 elements. Context: " + contextDescriptionForErrorHandling);
        }
        if (tCollection.size() == 1) return tCollection.iterator().next();
        return null;
    }

    public static <T> boolean contains(Collection<T> collection, Contains<T> containsFunction) {

        for (T t : collection) {
            if (containsFunction.contains(t)) {
                return true;
            }
        }
        return false;
    }

    public interface Contains<T> {
        boolean contains(T t);
    }

    public interface F<T> {
        void apply(T t);
    }

    public static <T> void each(Collection<T> collection, F<? super T> function) {
        for (T t : collection) {
            function.apply(t);
        }
    }
}
