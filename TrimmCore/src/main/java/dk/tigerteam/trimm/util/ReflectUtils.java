/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;

/**
 * Various reflection helper methods
 * 
 * @author Jeppe Cramon
 */
public class ReflectUtils {
	
	public static final Object[] NO_METHOD_CALL_ARGS = new Object[0];
	public static final Class<?>[] NO_METHOD_DEF_ARGS = new Class<?>[0];

	
	/**
	 * Make the given method accessible, explicitly setting it accessible if necessary.
	 * The <code>setAccessible(true)</code> method is only called when actually necessary,
	 * to avoid unnecessary conflicts with a JVM SecurityManager (if active).
	 * @param method the method to make accessible
	 * @return the method that was passed as argument (to allow for method chaining)
	 * @see java.lang.reflect.Method#setAccessible
	 */
	public static Method makeAccessible(Method method) {
		if (!Modifier.isPublic(method.getModifiers()) ||
				!Modifier.isPublic(method.getDeclaringClass().getModifiers())) {
			method.setAccessible(true);
		}

		return method;
	}
	/**
	 * Make the given field accessible, explicitly setting it accessible if necessary.
	 * The <code>setAccessible(true)</code> method is only called when actually necessary,
	 * to avoid unnecessary conflicts with a JVM SecurityManager (if active).
	 * @param field the field to make accessible
	 * @return the field that was passed as argument (to allow for method chaining)
	 * @see java.lang.reflect.Field#setAccessible
	 */
	public static Field makeAccessible(Field field) {
		if (!Modifier.isPublic(field.getModifiers()) ||
				!Modifier.isPublic(field.getDeclaringClass().getModifiers())) {
			field.setAccessible(true);
		}
        return field;
	}

    /**
     * Same as makeAccessible plus removing final modifiers (be wary of usage for other things than testing)
     *  @param field the field to make accessible and remove the FINAL modifier for
     * @return the field that was passed as argument (to allow for method chaining)
     */
    public static Field makeFieldAccessibleAndNonFinal(Field field) {
        // Try and make the field non-final
        try {
            int modifiers = field.getModifiers();
            Field modifierField = field.getClass().getDeclaredField("modifiers");
            modifiers = modifiers & ~Modifier.FINAL;
            makeAccessible(field);
            modifierField.setInt(field, modifiers);
        } catch (Exception e) {
            // Do nothing
        }
        return field;
    }

    /**
	 * Does the class have a default (taking no arguments) public constructor?
	 * @param clazz The clazz to investigate
	 * @return true if it has, otherwise false
	 */
	public static boolean hasDefaultConstructor(Class<?> clazz) {
		try {
			Constructor<?> constructor = clazz.getConstructor(new Class[] {});
			return Modifier.isPublic(constructor.getModifiers());
		} catch (SecurityException e) {
			throw new RuntimeException("Wasn't allowed to check if class '" + clazz.getName() + "' has a public default constructor");
		} catch (NoSuchMethodException e) {
			return false;
		}
	}

	 /**
     * Fetches all methods of all access types from the supplied class and super
     * classes. Methods that have been overridden in the inheritance hierachy are
     * only returned once, using the instance lowest down the hierarchy.
     *
     * @param clazz the class to inspect
     * @return a collection of methods
     */
    public static Collection<Method> getAllMethods(Class<?> clazz) {
        Collection<Method> found = new ArrayList<Method>();
        while (clazz != null) {
            for (Method m1 : clazz.getDeclaredMethods()) {
                boolean overridden = false;

                for (Method m2 : found) {
                    if ( m2.getName().equals(m1.getName()) &&
                            Arrays.deepEquals(m1.getParameterTypes(), m2.getParameterTypes())) {
                        overridden = true;
                        break;
                    }
                }

                if (!overridden) found.add(m1);
            }

            clazz = clazz.getSuperclass();
        }

        return found;
    }

    /**
     * Get a specific field by its name in a class
     * @param clazz The class from which we want the field
     * @param fieldName The name of the field
     * @return The field
     * @throws IllegalArgumentException if no such field exists in the class
     */
	public static Field getField(Class<?> clazz, String fieldName) {
		for (Field field : getFields(clazz)) {
			if (field.getName().equals(fieldName)) {
				return field;
			}
		}
		throw new IllegalArgumentException("Couldn't find a Field with name '" + fieldName + "' in class '" + clazz + "'");
	}

	/**
	 * Get the property name for a given method. Example: method "getPerson" will have the property name "person".
	 * Supports "get", "set" and "is" methods.
	 * @param method The method
	 * @return The normalized property name
	 */
	public static String getPropertyName(Method method) {
		String methodName = method.getName();
		if (methodName.startsWith("get") && isGetterPropertyMethod(method)){
			return Introspector.decapitalize(methodName.substring("get".length()));
		}
		if (methodName.startsWith("set") && isSetterPropertyMethod(method)){
			return Introspector.decapitalize(methodName.substring("set".length()));
		}
		if (methodName.startsWith("is") && isGetterPropertyMethod(method)){
			return Introspector.decapitalize(methodName.substring("is".length()));
		}
		throw new IllegalArgumentException("Method '" + method + "' isn't a property method");
	}
	
	public static boolean isSetterPropertyMethod(Method method) {
		return method.getName().startsWith("set") && method.getName().length() > 3 && Character.isUpperCase(method.getName().charAt(3)) && method.getParameterTypes().length == 1;
	}

	public static boolean isGetterPropertyMethod(Method method) {
		if (method.getName().startsWith("get")) {
			return method.getName().length() > 3 && Character.isUpperCase(method.getName().charAt(3)) && method.getReturnType() != Void.class;
		} else if (method.getName().startsWith("is")) {
			return method.getName().length() > 2 && Character.isUpperCase(method.getName().charAt(2)) && method.getReturnType() != Void.class;
		} else {
			return false;
		}
	}


	/**
	 * Find the <b>single</b> field from class 'clazz' and all its super classes, which have the given annotation.
	 * This method assumes that ONE method exists with the given annotation. If no field or more than
	 * one field is found a IllegalStateException will be thrown.<br/>
	 * Use {@link #getFieldsWithAnnotations(Class, Class...)} if you're not sure that a Field with this annotation exists.
	 * @param clazz The clazz from which we want all fields with the given annotation
	 * @param annotation The annotation
	 * @return The Field with the Annotation.
	 * @throws IllegalStateException If no field or more than one field with the annotation is found
	 * @see #getFieldsWithAnnotations(Class, Class...)
	 */
	@SuppressWarnings("unchecked")
	public static Field getFieldWithAnnotation(Class<? extends Object> clazz, Class<? extends Annotation> annotation) {
		List<Field> foundFields = getFieldsWithAnnotations(clazz, annotation);

		if (foundFields.size() == 1) {
			return foundFields.get(0);
		} else if (foundFields.size() > 1) {
			throw new IllegalStateException("Multiple fields in class '" + clazz.getName() + "' has annotation @"+annotation.getName()+". Fields found: [" + foundFields + "]");
		} else {
			throw new IllegalStateException("Annotation @"+annotation.getName()+" is not present on any field in class '" + clazz.getName() + "'");
		}
	}

	/**
	 * Find all fields from class 'clazz' and all its super classes, which have <b>one</b> or more of the given annotations.
	 * @param clazz The clazz to search for fields
	 * @param annotations The annotations
	 * @return The Fields which have one or more of the annotations
	 */
	public static List<Field> getFieldsWithAnnotations(Class<? extends Object> clazz, Class<? extends Annotation>... annotations) {
		Collection<Field> fields = getFields(clazz);
		List<Field> foundFields = new ArrayList<Field>();
		for (Field field : fields) {
			for (Class<? extends Annotation> annotation : annotations) {
				if (field.isAnnotationPresent(annotation)){
					foundFields.add(field);
				}
			}
		}

		return foundFields;
	}

	/**
	 * Get the <b>single</b> method which has the annotation. This method will search the class 'clazz' and all it's superclasses (taking
	 * overridden methods into consideration (only includes the most specific version)).
	 * @param clazz The class to search for methods
	 * @param annotation The annotation
	 * @return The <b>single</b> method which has the annotation.
	 * @throws IllegalStateException in case none or more than one Method has the annotation
	 */
	public static Method getMethodWithAnnotation(Class<? extends Object> clazz, Class<? extends Annotation> annotation) {
		List<Method> foundMethods = getAllMethodsWithAnnotation(clazz, annotation);

		if (foundMethods.size() == 1) {
			return foundMethods.get(0);
		} else if (foundMethods.size() > 1) {
			throw new IllegalStateException("Multiple method in class '" + clazz.getName() + "' has annotation @"+annotation.getName()+". Methods found: [" + foundMethods + "]");
		} else {
			throw new IllegalStateException("Annotation @"+annotation.getName()+" is not present on any method in class '" + clazz.getName() + "'");
		}
	}

	/**
	 * Find the Setter property method for property with name specified by the propertyName parameter
	 * @param clazz The clazz to investigate (all super class methods will be included in the search)
	 * @param propertyName The property name
	 * @return The setter property method if found or <code>null</code> if a match couldn't be found
	 */
	public static Method getOptionalSetterPropertyMethod(Class<? extends Object> clazz, String propertyName) {
		Collection<Method> foundMethods = getAllMethods(clazz);
		
		for (Method method : foundMethods) {
			if (isSetterPropertyMethod(method) && getPropertyName(method).equals(propertyName)) {
				return method;
			}
		}
		
		return null;
	}

	
	/**
	 * Find the Setter property method for property with name specified by the propertyName parameter, but which also carries the given annotation
	 * @param clazz The clazz to investigate (all super class methods will be included in the search)
	 * @param propertyName The property name
	 * @param annotation The annotation that the setter method must be annotated with
	 * @return The setter property method if found or <code>null</code> if a match couldn't be found
	 */
	public static Method getSetterPropertyMethodWithAnnotation(Class<? extends Object> clazz, String propertyName, Class<? extends Annotation> annotation) {
		Method setterMethod = getOptionalSetterPropertyMethod(clazz, propertyName);
		if (setterMethod != null && setterMethod.isAnnotationPresent(annotation)) {
			return setterMethod;
		} 
		
		return null;
	}
	
	/**
	 * Get <b>all</b> the method which has the annotation. This method will search the class 'clazz' and all it's superclasses (taking
	 * overridden methods into consideration (only includes the most specific version)).
	 * @param clazz The class to search for methods
	 * @param annotation The annotation
	 * @return The <b>all</b> the methods which has the annotation.
	 */
	public static List<Method> getAllMethodsWithAnnotation(Class<? extends Object> clazz, Class<? extends Annotation> annotation) {
		Collection<Method> methods = getAllMethods(clazz);
		List<Method> foundMethods = new ArrayList<Method>();
		for (Method method : methods) {
			if (method.isAnnotationPresent(annotation)){
				foundMethods.add(method);
			}
		}

		return foundMethods;
	}
	
	/**
	 * Check if a class has annotations
	 * @param c the Class
	 * @param annotations The Annotations
	 * @return true if class c has annotations
	 */
	public static boolean hasAnnotations(Class<?> c, Class<? extends Annotation>... annotations) {
		for (Class<? extends Annotation> annotation : annotations) {
			boolean result = hasAnnotation(c, annotation);
			if(!result) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Check if a class has a given annotation
	 * @param c The Class
	 * @param annotation The Annotation
	 * @return true if class c has annotation
	 */
	public static boolean hasAnnotation(Class<?> c, Class<? extends Annotation> annotation) {
		return c.isAnnotationPresent(annotation);
	}

	/**
	 * Get the public fields from the source object which have the given value
	 * @param source The object to test
	 * @param value The value that the fields must have (using equals to test)
	 * @return The list of fields that have the given value
	 */
	public static List<Field> getFieldsWithValue(Object source, Object value){
		Collection<Field> fields = getFields(source.getClass());
		List<Field> foundFields = new ArrayList<Field>();
		for (Field field : fields) {
			try {
				if (Modifier.isPublic(field.getModifiers()) && value.equals(field.get(source))){
					foundFields.add(field);
				}
			} catch (IllegalAccessException e) {
				throw new RuntimeException("Read access to field value denied on '" + field.getName() + "'");
			}
		}
		return foundFields;
	}


  /**
   * Fetches all fields of all access types from the supplied class and super
   * classes. Fields that have been overridden in the inheritance hierachy are
   * only returned once, using the instance lowest down the hierarchy.
   *
   * @param clazz the class to inspect
   * @return a collection of fields
   */
	public static Collection<Field> getFields(Class<?> clazz) {
		return getFields(clazz, null);
	}

  /**
   * Fetches all fields of all access types from the supplied class and super
   * classes. Fields that have been overridden in the inheritance hierachy are
   * only returned once, using the instance lowest down the hierarchy.
   * Fields are only returned, if filter requirements are satisfied or filter
   * is null.
   *
   * @param clazz the class to inspect
   * @param filter filter The field filter to use when selecting fields from the class
   * @return a collection of fields
   */
	public static Collection<Field> getFields(Class<?> clazz, FieldFilter filter) {
    Map<String,Field> fields = new HashMap<String, Field>();
		while(clazz != null){
			for(Field field : clazz.getDeclaredFields())
  			if (!fields.containsKey(field.getName()) && (filter == null || filter.isOk(field)))
  				fields.put(field.getName(), field);
			clazz = clazz.getSuperclass();
		}
		return fields.values();
	}

	public static Collection<Field> getAllFieldsWithAnnotation(Class<?> c, final Class<? extends Annotation> annotation) {
		FieldFilter filter = new FieldFilter() {
			public boolean isOk(Field field) {
				return field.isAnnotationPresent(annotation);
			}
		};
		return getFields(c, filter);
	}
	
	public static Collection<Field> getAllFieldsWithAnnotations(Class<?> c, Class<? extends Annotation>... annotations) {
		Set<Field> fields = new HashSet<Field>();
		for (Class<? extends Annotation> annotation : annotations) {
			fields.addAll(getAllFieldsWithAnnotation(c, annotation));
		}
		return fields;
	}
	
	public static void setFieldValue(Field field, Object onObject, Object value) {
		try {
			field.set(onObject, value);
		} catch (Exception e) {
			throw new RuntimeException("Failed to set value for field '" + field + "' on object '" + onObject + "' using value '" + value + "'", e);
		}
	}

	
	public static Object getFieldValue(Field field, Object onObject) {
		try {
			return field.get(onObject);
		} catch (Exception e) {
			throw new RuntimeException("Failed to get value for field '" + field + "' on object '" + onObject + "'", e);
		}
	}

	public static Object getValueUsingReadMethodForField(Object o, Field field) {
		try {
			return getReadMethod(field).invoke(o);
		} catch (Exception e) {
			throw new RuntimeException("Failed to get field value '" + field + "' using ReadMethod", e);
		}
	}

	public static void setValueUsingWriteMethodForField(Object o, Field field, Object value) {
		try {
			getWriteMethod(field).invoke(o, value);
		} catch (Exception e) {
			throw new RuntimeException("Failed to update field value '" + field + "' using WriteMethod", e);
		}
	}

	public static Method getWriteMethod(Field field) {
		String methodName = "set" + Utils.capitalize(field.getName());
		try {
			return field.getDeclaringClass().getMethod(methodName, field.getType());
		} catch (Exception e) {
			throw new RuntimeException("Failed to find write method '" + methodName + "' for field '" + field + "'", e);
		}
	}

	public static Method getReadMethod(Field field) {
		String methodName = null;
		if (field.getType().equals(boolean.class)){
			methodName = "is" + Utils.capitalize(field.getName());
		} else {
			methodName = "get" + Utils.capitalize(field.getName());
		}
		try {
			return field.getDeclaringClass().getMethod(methodName);
		} catch (Exception e) {
			throw new RuntimeException("Failed to find read method '" + methodName + "' for field '" + field + "'", e);
		}
	}
  /**
   * Get the property descriptor, from the propertyName parameter on the given class
   * @param clazz
   * @param propertyName
   * @return the property descriptor or <code>null</code> if no matching property was found
   * @throws RuntimeException in case of an introspection exception
   */
  public static PropertyDescriptor getOptionalPropertyDescriptor(Class<? extends Object> clazz, String propertyName) {
    try {
      for (PropertyDescriptor	propertyDescriptor : Introspector.getBeanInfo(clazz).getPropertyDescriptors()) {
        if (propertyDescriptor.getName().equals(propertyName)) {
          return propertyDescriptor;
        }
      }
      return null;
    } catch (IntrospectionException e) {
      throw new RuntimeException("Failed to find property '" + propertyName + "' in class '" + clazz.getName() + "'", e);
    }
  }

  /**
   * Get the property descriptor, from the propertyName parameter on the given class
   * @param clazz
   * @param propertyName
   * @return The property descriptor matching the propertyName in the given clazz
   * @throws IllegalStateException if the property can't be found
   * @throws RuntimeException in case of an introspection exception
   */
	public static PropertyDescriptor getPropertyDescriptor(Class<? extends Object> clazz, String propertyName) {
    PropertyDescriptor optionalPropertyDescriptor = getOptionalPropertyDescriptor(clazz, propertyName);
    if (optionalPropertyDescriptor == null) {
      throw new IllegalStateException("Failed to find property '" + propertyName + "' in class '" + clazz.getName() + "'");
    } else {
      return optionalPropertyDescriptor;
    }

	}

  public static PropertyDescriptors getPropertyDescriptors(Class<? extends Object> clazz) {
    List<PropertyDescriptor> result = new LinkedList<PropertyDescriptor>();
		try {
			for (PropertyDescriptor	propertyDescriptor : Introspector.getBeanInfo(clazz).getPropertyDescriptors()) {
				result.add(propertyDescriptor);
			}
      return new PropertyDescriptors(result);
		} catch (IntrospectionException e) {
			throw new RuntimeException("Failed to get PropertyDescriptors for class '" + clazz.getName() + "'", e);
		}
	}


	/**
	 * <p>
	 * Check whether the given class implements the given generic superclass with the given generic types
	 * </p>
	 * @param currentClass
	 * @param genericSuperClass
	 * @param genericTypes
	 * @return True if the class implements the generic interface
	 */
	public static boolean isGenericImplementation(Class<? extends Object> currentClass, Class<? extends Object> genericSuperClass, Class<? extends Object> ...genericTypes) {
		return Arrays.equals(genericTypes, genericTypeResolver(currentClass, genericSuperClass));
	}

	/**
	 * <p>
	 * Find the generic type parameter at the given position for the given class in the given superclass.
	 * </p>
	 * @param currentClass
	 * @param parent
	 * @param genericArgumentNumber
	 * @return The generic type parameter at the given position
	 * @throws IllegalStateException In case the type cant be resolved
	 */
	public static Type getGenericType(Class<? extends Object> currentClass, Class<? extends Object> parent, int genericArgumentNumber) {
		Type[] genericTypes = genericTypeResolver(currentClass, parent);

		if (genericArgumentNumber >= genericTypes.length || genericTypes[genericArgumentNumber] == null) {
			throw new IllegalStateException("Can't read parameterized type number " + genericArgumentNumber + " for the '" + currentClass + "' specialization of '" + parent + "'");
		}
		return genericTypes[genericArgumentNumber];
	}

	/**
	 * <p>
	 * Return the type parameter at the given position for the given class in the given superclass as a Class.<br>
	 * The method returns null if the class cant be resolved.
	 * </p>
	 * @param currentClass
	 * @param parent
	 * @param genericArgumentNumber
	 * @return The generic type parameter at the given position as a class or null.
	 */
	public static Class<? extends Object> getGenericClass(Class<? extends Object> currentClass, Class<? extends Object> parent, int genericArgumentNumber) {
		Type[] genericTypes = genericTypeResolver(currentClass, parent);
		Type type = genericArgumentNumber < genericTypes.length ? genericTypes[genericArgumentNumber] : null;

		if (type instanceof Class<?>)
			return (Class<?>)type;
		return null;
	}

	/**
	 * <p>
	 * Resolve the types in the given generic superclass, the given class is declared with.<br>
	 * If the types can't be resolved an empty array is returned.<br>
	 * If any of the generic types from the parent can't be resolved it is set to null.
	 * <p>
	 * @param currentClass The current class that we should look for Generics in
	 * @param parent Search upwards in the hierarchy until we pass this parent Class (which may go up all the way until {@link Object})
	 * @return List of types used in the declaration of currentClass.
	 */
	public static Type[] genericTypeResolver(Class<? extends Object> currentClass, Class<? extends Object> parent) {
		Type[] actualTypeArguments = null;
		int[] genericTypesMap = null;

		while (!currentClass.isAssignableFrom(parent)) {
			if (parent.isInterface())
				for (Type iType : currentClass.getGenericInterfaces())
					if (iType instanceof ParameterizedType) {
						ParameterizedType pType = (ParameterizedType) iType;
						if (parent.equals(pType.getRawType())) {
							if (actualTypeArguments == null)
								return pType.getActualTypeArguments();

							Type[] actualTypes = new Type[genericTypesMap.length];
							for (int i = 0; i < genericTypesMap.length; i++)
								actualTypes[i] = (genericTypesMap[i] < actualTypeArguments.length) ? actualTypeArguments[genericTypesMap[i]] : null;
							return actualTypes;
						}
					}

            //
			Type type = currentClass.getGenericSuperclass();
			if (type instanceof ParameterizedType) {
				if (actualTypeArguments == null) {
					actualTypeArguments = ((ParameterizedType)type).getActualTypeArguments();
					genericTypesMap = new int[actualTypeArguments.length];
					for (int i = 0; i < actualTypeArguments.length; i++)
						genericTypesMap[i] = i;
				}
				else {
					TypeVariable<?>[] typeParameters = currentClass.getTypeParameters();
					Type[] actualTypes = ((ParameterizedType)type).getActualTypeArguments();
					int[] newGenericTypesMap = new int[actualTypes.length];
					for (int i = 0; i < actualTypes.length; i++) {
						newGenericTypesMap[i] = actualTypeArguments.length;
						for (int j = 0; j < typeParameters.length; j++) {
							if (actualTypes[i].equals(typeParameters[genericTypesMap[j]])) {
								newGenericTypesMap[i] = j;
								break;
							}
						}
					}
					genericTypesMap = newGenericTypesMap;
				}
			} else
				break;
			currentClass = currentClass.getSuperclass();
		}
		if (genericTypesMap == null) {
			return new Type[0];
		}
          ///
		Type[] genericTypes = new Type[genericTypesMap.length];
		for (int i = 0; i < genericTypesMap.length; i++)
			genericTypes[i] = (genericTypesMap[i] < actualTypeArguments.length) ? actualTypeArguments[genericTypesMap[i]] : null;
		return genericTypes;
	}
	
	/**
     * Returns a set of all interfaces implemented by class supplied. This includes all
     * interfaces directly implemented by this class as well as those implemented by
     * superclasses or interface superclasses.
     * 
     * @param clazz
     * @return all interfaces implemented by this class
     */
    public static Set<Class<?>> getImplementedInterfaces(Class<?> clazz) {
      Set<Class<?>> interfaces = new HashSet<Class<?>>();
      
      if (clazz.isInterface())
          interfaces.add(clazz);

      while (clazz != null) {
          for (Class<?> iface : clazz.getInterfaces())
              interfaces.addAll(getImplementedInterfaces(iface));
          clazz = clazz.getSuperclass();
      } 

      return interfaces;
    }

    /**
     * Returns an array of Type objects representing the actual type arguments
     * to targetType used by clazz.
     * 
     * @param clazz the implementing class (or subclass)
     * @param targetType the implemented generic class or interface - <b>Note: The targetType must be a Parameterized class, i.e. one using Generics :)</b>
     * @return an array of Type objects or null
     */
    public static Type[] getActualTypeArguments(Class<?> clazz, Class<?> targetType) {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        classes.add(clazz);
        
       /* if (targetType.isInterface())
            classes.addAll(getImplementedInterfaces(clazz));
        else {
            Class<?> superClass = clazz.getSuperclass();
            while (superClass != null) {
                classes.add(superClass);
                superClass = superClass.getSuperclass();
            }
        }
        
            
        for (Class<?> search : classes) {
            for (Type type : (targetType.isInterface() ? search.getGenericInterfaces()
                    : new Type[] { search.getGenericSuperclass() })) {
                if (type instanceof ParameterizedType) {
                    ParameterizedType parameterizedType = (ParameterizedType) type;
                    if (targetType.equals(parameterizedType.getRawType()))
                        return parameterizedType.getActualTypeArguments();
                }
            }
        }*/
        
        if (targetType.isInterface())
            classes.addAll(getImplementedInterfaces(clazz));
        
        Class<?> superClass = clazz.getSuperclass();
        while (superClass != null && targetType.isAssignableFrom(superClass)) {
            classes.add(superClass);
            superClass = superClass.getSuperclass();
        }
        
            
        for (Class<?> search : classes) {
            for (Type type : (targetType.isInterface() ? search.getGenericInterfaces()
                    : new Type[] { search.getGenericSuperclass() })) {
                if (type instanceof ParameterizedType) {
                    ParameterizedType parameterizedType = (ParameterizedType) type;
                    if (targetType.equals(parameterizedType.getRawType()))
                        return parameterizedType.getActualTypeArguments();
                }
            }
        }

        return null;
    }

  /**
   * Look for a Method, with the given methodName and the given parameterTypes, in this clazz and upwards until we reach {@link Object} 
   * @param clazz The clazz to look for the method in
   * @param methodName The method name
   * @param methodParameterTypes The method arguments (may be null or omitted)
   * @return The method found or <code>null</code> if no method matching these requirements can be found
   * @see #getMethod(Class, String, Class...) which required the method to be found 
   */  
	public static Method findMethod(Class<?> clazz, String methodName, Class<?>... methodParameterTypes) {
		Class<?> currentClazz = clazz;
		while (currentClazz != null && !currentClazz.equals(Object.class)) {
			Method[] methods = currentClazz.isInterface() ? currentClazz.getMethods() : currentClazz.getDeclaredMethods();
			for (int index = 0; index < methods.length; index++) {
				Method method = methods[index];
				if (methodName.equals(method.getName()) && Arrays.equals(methodParameterTypes, method.getParameterTypes())) {
					return method;
				}
			}
			currentClazz = currentClazz.getSuperclass();
		}
		return null;	
	}
	
	/**
   * Get the Method, with the given methodName and the given parameterTypes, in this clazz and upwards until we reach {@link Object} 
   * @param clazz The clazz to look for the method in
   * @param methodName The method name
   * @param methodParameterTypes The method arguments (may be null or omitted)
   * @return The method found
   * @throws IllegalArgumentException in case the method couldn't be found
   */  
	public static Method getMethod(Class<?> clazz, String methodName, Class<?>... methodParameterTypes) {
		Method method = findMethod(clazz, methodName, methodParameterTypes);
		if (method != null) {
			return method;
		} else {
			throw new IllegalArgumentException("Couldn't find method '" + methodName + "(" + Arrays.toString(methodParameterTypes) + ")' in class '" + clazz.getName() + "'");	
		}
	}
	
	/**
	 * Invoke the specified {@link Method} against the supplied target object
	 * with the supplied arguments. The target object can be <code>null</code>
	 * when invoking a static {@link Method}.
	 * @param method the method to invoke
	 * @param target the target object to invoke the method on
	 * @param args the invocation arguments (may be <code>null</code> or omitted)
	 * @return the invocation result, if any
	 */
	public static Object invokeMethod(Method method, Object target, Object... args) {
		try {
			return method.invoke(target, args);
		}
		catch (Exception ex) {
			throw new IllegalArgumentException("Failed to invoke method '" + method + "' with arguments '" + Arrays.toString(args) + "' on target object '" + target + "'", ex);
		}
	}

    public static boolean canSetPropertyValue(Object entity, String propertyName) {
      return canSetPropertyValue(entity.getClass(), propertyName);
    }

    public static boolean canGetPropertyValue(Object entity, String propertyName) {
      return canGetPropertyValue(entity.getClass(), propertyName);
    }

    public static boolean canSetPropertyValue(Class<?> clazz, String propertyName) {
      return getPropertyDescriptor(clazz, propertyName).getWriteMethod() != null;
    }

    public static boolean canGetPropertyValue(Class<?> clazz, String propertyName) {
      return getPropertyDescriptor(clazz, propertyName).getReadMethod() != null;
    }

    public static void setPropertyValue(Object entity, String propertyName, Object value) {
      try {
          getPropertyDescriptor(entity.getClass(), propertyName).getWriteMethod().invoke(entity, value);
      } catch (Exception e) {
          throw new RuntimeException("Failed to set property '" + propertyName + "' on class '" + entity.getClass().getName() + "'", e);
      }
    }

    public static Object getPropertyValue(Object entity, String propertyName) {
      try {
          return getPropertyDescriptor(entity.getClass(), propertyName).getReadMethod().invoke(entity, new Object[0]);
      } catch (Exception e) {
          throw new RuntimeException("Failed to get property '" + propertyName + "' on class '" + entity.getClass().getName() + "'", e);
      }
    }

		@SuppressWarnings("rawtypes")
		public static boolean hasNoArgConstructor(Class<?> type) {
      for (Constructor constructor : type.getConstructors()) {
          if (constructor.getParameterTypes().length == 0) {
              return true;
          }
      }
      return false;
    }
    
		public static Object getFieldValue(String fieldName, Object onObject) {
			Field field = getField(onObject.getClass(), fieldName);
			makeAccessible(field);
			return getFieldValue(field, onObject);
		}
		
		public static <T> Constructor<T> getConstructorIfAvailable(Class<T> clazz, Class<?>... paramTypes) {
			assert clazz != null;
			try {
				return clazz.getConstructor(paramTypes);
			}
			catch (NoSuchMethodException ex) {
				return null;
			}
		}

  public static boolean hasSetterProperty(Class<?> _class, String propertyName) {
    PropertyDescriptor propertyDescriptor = getOptionalPropertyDescriptor(_class, propertyName);
    if (propertyDescriptor == null) return false;
    return (propertyDescriptor.getWriteMethod() != null);
  }

  public static boolean hasGetterProperty(Class<?> _class, String propertyName) {
    PropertyDescriptor propertyDescriptor = getOptionalPropertyDescriptor(_class, propertyName);
    if (propertyDescriptor == null) return false;
    return (propertyDescriptor.getReadMethod() != null);
  }

}
