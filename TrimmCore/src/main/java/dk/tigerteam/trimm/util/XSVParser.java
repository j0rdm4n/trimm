/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import java.util.*;
import java.util.Map.Entry;

/**
 * Xsv format parser @see parseKeyValue method
 * 
 * @author Jeppe Cramon
 */
public class XSVParser {
	
	/**
	 * Parse key value token stream. Example <code>alias=Test;name=TestFullName;somethingElse=Something</code>
	 * 
	 * @param text
	 *          The text to parse
	 * @param separatorToken
	 *          The separator between keyValue sets (<code>;</code> in the example)
	 * @param keyValueAssignmentToken
	 *          The separator between key/value assignment (<code>=</code> in the example)
	 * @return A map where the keys are key and a List of the values (in case the same key appears more than once)
	 */
	public static Map<String, List<String>> parseKeyValue(String text, String separatorToken, String keyValueAssignmentToken) {
		Map<String, List<String>> result = new HashMap<String, List<String>>();
		if (text == null) {
			return result;
		}
		StringTokenizer tokenizer = new StringTokenizer(text, separatorToken);
		while (tokenizer.hasMoreTokens()) {
			String keyValue = tokenizer.nextToken().trim();
			int indexOfAssignmentToken = keyValue.indexOf(keyValueAssignmentToken);
			if (indexOfAssignmentToken == -1) {
				throw new IllegalArgumentException("KeyValue: '" + keyValue + "' doesn't contain keyValueAssignmentToken '"
						+ keyValueAssignmentToken + "'. Other information separatorToken '" + separatorToken + "' & text '" + text + "'");
			}
			String key = keyValue.substring(0, indexOfAssignmentToken).trim();
			String value = keyValue.substring(indexOfAssignmentToken + 1).trim();

			List<String> values = result.get(key);
			if (values == null) {
				values = new ArrayList<String>();
				result.put(key, values);
			}
			values.add(value);
		}
		return result;
	}

	/**
	 * Parse key value token stream. Example <code>alias=Test;name=TestFullName;somethingElse=Something</code>.
	 * <p/>
	 * <b>This method expects that each Key only appears once in the text!</b>
	 * 
	 * @param text
	 *          The text to parse
	 * @param separatorToken
	 *          The separator between keyValue sets (<code>;</code> in the example)
	 * @param keyValueAssignmentToken
	 *          The separator between key/value assignment (<code>=</code> in the example)
	 * @return A map where the keys are key and and the value is the value
	 * @throws IllegalArgumentException
	 *           in case the keyValueAssignmentToken can't be found inside a key/value or if the same key appears more than once
	 */
	public static Map<String, String> parseKeyValueToFlatMap(String text, String separatorToken, String keyValueAssignmentToken) {
		Map<String, List<String>> parsedKeyValue = parseKeyValue(text, separatorToken, keyValueAssignmentToken);
		Map<String, String> result = new HashMap<String, String>();
		for (Entry<String, List<String>> entry : parsedKeyValue.entrySet()) {
			if (entry.getValue().size() > 1) {
				throw new IllegalArgumentException("Key '" + entry.getKey()
						+ "' appears more than once!. Other information keyValueAssignmentToken '" + keyValueAssignmentToken + "', separatorToken '"
						+ separatorToken + "' & text '" + text + "'");
			}
			result.put(entry.getKey(), entry.getValue().size() == 1 ? entry.getValue().get(0) : null);
		}
		return result;
	}
}
