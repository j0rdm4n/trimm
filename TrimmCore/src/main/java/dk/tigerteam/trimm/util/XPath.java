package dk.tigerteam.trimm.util;

import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Helper class to ease the transition from JDOM 1 to JDOM 2 (work in progress)
 */
public class XPath {
    private String xPathExpression;
    private List<Namespace> namespaces = new LinkedList<Namespace>();
    XPathExpression<Element> xPathExpressionObject;

    public static XPath newInstance(String xPathExpression) {
        return new XPath(xPathExpression);
    }

    private XPath(String xPathExpression) {
        this.xPathExpression = xPathExpression;
    }

    public XPath addNamespace(Namespace namespace) {
        this.namespaces.add(namespace);
        return this;
    }

    public Element selectSingleNode(Object context) {
        return buildXPathExpress().evaluateFirst(context);
    }

    public List<Element> selectNodes(Object context) {
        return buildXPathExpress().evaluate(context);
    }

    private XPathExpression<Element> buildXPathExpress() {
        if (xPathExpressionObject == null) {
           xPathExpressionObject = XPathFactory.instance().compile("xPathExpression", Filters.element(), Collections.EMPTY_MAP, namespaces.toArray(new Namespace[0]));
        }
        return xPathExpressionObject;
    }

    @Override
    public String toString() {
        return xPathExpression;
    }
}
