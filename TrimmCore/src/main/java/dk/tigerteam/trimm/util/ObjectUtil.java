/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

/**
 * General object related utility methods
 * 
 * @author Jeppe Cramon
 * @author Lasse Cramon
 */
public class ObjectUtil {

	/**
	 * Performs a null safe {@link Object#equals(Object)} between two objects
	 */
	public static boolean equals(Object o1, Object o2) {
		return (o1 == null) ? o2 == null : o1.equals(o2);
	}
	
	/**
	 * Performs a null safe {@link Object#equals(Object)} between two objects
	 */
	public static boolean equalsIgnoreCase(String o1, String o2) {
		return (o1 == null) ? o2 == null : o1.equalsIgnoreCase(o2);
	}
	
//	public static <SORT_BY_TYPE, TYPE> Map<SORT_BY_TYPE, List<TYPE>> sortBy(Collection<TYPE> objects, Class<SORT_BY_TYPE> sortByType, String sortByExpression) {
//		return sortBy(objects, sortByType, sortByExpression, 0);
//	}


//	@SuppressWarnings("unchecked")
//	public static <SORT_BY_TYPE, TYPE> Map<SORT_BY_TYPE, List<TYPE>>  sortBy(Collection<TYPE> objects, Class<SORT_BY_TYPE> sortByType, String sortByExpression, int maxResultsPerSortType) {
//		Map<SORT_BY_TYPE, List<TYPE>> result = new HashMap<SORT_BY_TYPE, List<TYPE>>();
//		for (TYPE obj : objects) {
//			BeanWrapper entityWrapper = new BeanWrapperImpl(obj);
//			SORT_BY_TYPE value = (SORT_BY_TYPE) entityWrapper.getPropertyValue(sortByExpression);
//			if (!result.containsKey(value)) {
//				result.put(value, new ArrayList<TYPE>());
//			}
//			List<TYPE> list = result.get(value);
//			if (maxResultsPerSortType == 0) {
//				list.add(obj);
//			} else if (list.size() < maxResultsPerSortType) {
//				list.add(obj);
//			}
//		}
//		return result;
//	}
	
}
