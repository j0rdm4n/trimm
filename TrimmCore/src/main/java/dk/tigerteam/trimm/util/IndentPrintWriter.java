/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.util;

import java.io.*;
import java.util.Locale;

public class IndentPrintWriter extends PrintWriter {
	
	private int indentLevel;
	
	/**
	 * Flag that's used to specify if a call to the underlying PrintWriter should indent for
	 * this line or if indentation already has occurred for this line
	 */
	boolean shouldIndent;
	private String indentation = "  ";

	public IndentPrintWriter(Writer out) {
		super(out);
	}

	public IndentPrintWriter(OutputStream out) {
		super(out);
	}
	
	public IndentPrintWriter(Writer out, String indentation) {
		super(out);
		this.indentation = indentation;
	}

	public IndentPrintWriter(OutputStream out, String indentation) {
		super(out);
		this.indentation = indentation;
	}

	public IndentPrintWriter(String fileName) throws FileNotFoundException {
		super(fileName);
	}

	public IndentPrintWriter(File file) throws FileNotFoundException {
		super(file);
	}

	public IndentPrintWriter(Writer out, boolean autoFlush) {
		super(out, autoFlush);
	}

	public IndentPrintWriter(OutputStream out, boolean autoFlush) {
		super(out, autoFlush);
	}

	public IndentPrintWriter(String fileName, String csn)
			throws FileNotFoundException, UnsupportedEncodingException {
		super(fileName, csn);
	}

	public IndentPrintWriter(File file, String csn)
			throws FileNotFoundException, UnsupportedEncodingException {
		super(file, csn);
	}

	public IndentPrintWriter indent() {
		indentLevel++;
		return this;
	}

	public IndentPrintWriter outdent() {
		indentLevel--;
		return this;
	}

	

	// ----------------- Overriding methods -----------------

	@Override
	public PrintWriter append(char c) {
		checkIndentation();
		return super.append(c);
	}

	@Override
	public PrintWriter append(CharSequence csq, int start, int end) {
		checkIndentation();
		return super.append(csq, start, end);
	}

	@Override
	public PrintWriter append(CharSequence csq) {
		checkIndentation();
		return super.append(csq);
	}

	@Override
	public PrintWriter format(Locale l, String format, Object... args) {
		checkIndentation();
		return super.format(l, format, args);
	}

	@Override
	public PrintWriter format(String format, Object... args) {
		checkIndentation();
		return super.format(format, args);
	}

	@Override
	public void print(boolean b) {
		checkIndentation();
		super.print(b);
	}

	@Override
	public void print(char c) {
		checkIndentation();
		super.print(c);
	}

	@Override
	public void print(char[] s) {
		checkIndentation();
		super.print(s);
	}

	@Override
	public void print(double d) {
		checkIndentation();
		super.print(d);
	}

	@Override
	public void print(float f) {
		checkIndentation();
		super.print(f);
	}

	@Override
	public void print(int i) {
		checkIndentation();
		super.print(i);
	}

	@Override
	public void print(long l) {
		checkIndentation();
		super.print(l);
	}

	@Override
	public void print(Object obj) {
		checkIndentation();
		super.print(obj);
	}

	@Override
	public void print(String s) {
		checkIndentation();
		super.print(s);
	}

	@Override
	public PrintWriter printf(Locale l, String format, Object... args) {
		checkIndentation();
		return super.printf(l, format, args);
	}

	@Override
	public PrintWriter printf(String format, Object... args) {
		checkIndentation();
		return super.printf(format, args);
	}

	@Override
	public void println() {
		checkIndentation();
		super.println();
		shouldIndent = true;
	}

	@Override
	public void println(boolean x) {
		checkIndentation();
		super.println(x);
		shouldIndent = true;
	}

	@Override
	public void println(char x) {
		checkIndentation();
		super.println(x);
		shouldIndent = true;
	}

	@Override
	public void println(char[] x) {
		checkIndentation();
		super.println(x);
		shouldIndent = true;
	}

	@Override
	public void println(double x) {
		checkIndentation();
		super.println(x);
		shouldIndent = true;
	}

	@Override
	public void println(float x) {
		checkIndentation();
		super.println(x);
		shouldIndent = true;
	}

	@Override
	public void println(int x) {
		checkIndentation();
		super.println(x);
		shouldIndent = true;
	}

	@Override
	public void println(long x) {
		checkIndentation();
		super.println(x);
		shouldIndent = true;
	}

	@Override
	public void println(Object x) {
		checkIndentation();
		super.println(x);
		shouldIndent = true;
	}

	@Override
	public void println(String x) {
		checkIndentation();
		super.println(x);
		shouldIndent = true;
	}


	@Override
	public void write(char[] buf, int off, int len) {
		checkIndentation();
		super.write(buf, off, len);
	}

	@Override
	public void write(char[] buf) {
		checkIndentation();
		super.write(buf);
	}

	@Override
	public void write(int c) {
		checkIndentation();
		super.write(c);
	}

	@Override
	public void write(String s, int off, int len) {
		checkIndentation();
		super.write(s, off, len);
	}

	@Override
	public void write(String s) {
		checkIndentation();
		super.write(s);
	}

	private void checkIndentation() {
		if (shouldIndent) {
			shouldIndent = false;
			for (int indent = 0; indent < indentLevel; indent++) {
				super.write(indentation);
			}
		}
	}


}
