/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.proxy;

import dk.tigerteam.trimm.util.ReflectUtils;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * A Method call recording proxy, useful for recording (potentially) chained calls to JavaBeans getter property methods. 
 * It records each method call to instances returned by {@link #createProxy(Class)}<br/>
 * Each time a new call is created on the root proxy (ie. the proxy returned by {@link #createProxy(Class)} then a new recording is commenced.<br/>
 * Return values from a method call are automatically proxied and are included in the current recording, so method chaining is possible<p/>
 * <b>!Warning! - Don't hold on to intermediate proxies for reuse purposes. Only the rootProxy returned by {@link #createProxy(Class)} can be called more than once!</b><p/>
 * <b>Note: Simple types, final classes are not proxied and will result in {@link NullPointerException}'s in case methods are called on the return value (which will be null)</b>
 * <b>Note: Method arguments are not recorded, so this is only useful for calling JavaBeans style (although no restrictions are
 * put into ensuring this, as some classes have getter method that deviate from standard JabaBeans naming style's all though still
 * being valid for this type of recording)</b><p/>
 * <b>To get access to the {@link MethodCallRecordings} you need to keep a handle to one of the proxies (preferably the root proxy). You can then cast it to interface
 * {@link MethodCallRecordingProxyMarker} and call its {@link MethodCallRecordingProxyMarker#getRecording()} method to get access to the recording.</b> 
 * @author Jeppe Cramon
 */
public class MethodCallRecordingProxy {
  private static final Log logger = LogFactory.getLog(MethodCallRecordingProxy.class);
	
	public static <T> T createProxy(Class<T> tClassOrInterface) {
		return createProxy(tClassOrInterface, null, true);
	}
	
	@SuppressWarnings("unchecked")
	static <T> T createProxy(Class<T> tClassOrInterface, MethodCallRecordings recording, boolean rootProxy) {
		Enhancer enhancer = new Enhancer();
		if (tClassOrInterface.isInterface()) {
			enhancer.setInterfaces(new Class[] { tClassOrInterface, MethodCallRecordingProxyMarker.class });
			enhancer.setSuperclass(Object.class);
		} else {
			enhancer.setInterfaces(new Class[] { MethodCallRecordingProxyMarker.class });
			enhancer.setSuperclass(tClassOrInterface);
		}
		enhancer.setCallback(new MethodCallInterceptor(tClassOrInterface, recording, rootProxy));
		 try {
		    return (T) enhancer.create();
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Failed to create a proxy for class '" + tClassOrInterface.getName() + "'", e);
        }
	}
	
	/**
	 * Marker interface so that we can spot our own proxies
	 */ 
	public static interface MethodCallRecordingProxyMarker {
		/**
		 * Get the recording
		 */
		MethodCallRecordings getRecording();
	}
	
	/**
	 * Method calling interceptor, which handles the recording and proxification of return values
	 */
	private static class MethodCallInterceptor extends CglibIdentitySupport implements MethodInterceptor {
		private MethodCallRecordings recording;
		private boolean rootProxy;
		
		public MethodCallInterceptor(Class<?> wrappedClass, MethodCallRecordings recording, boolean rootProxy) {
			super(wrappedClass);
			this.recording = recording;
			this.rootProxy = rootProxy;
		}
		
		public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
			if (method.getDeclaringClass().equals(MethodCallRecordingProxyMarker.class)) {
				if (method.getName().equals("getRecording")) {
					return recording;
				} else {
					throw new IllegalArgumentException("Internal error: Unsupported method '" + method + "'");
				}
			}
			
			if (rootProxy) {
				// Okay, we have a method call to our rootProxy. This means that the user is starting a new recording, so we'll create
				// a new one
				recording = new MethodCallRecordings(getWrappedClass());
			}
			
			recording.addMethodCall(method);
			
			Class<?> returnType = method.getReturnType();
			if (returnType.isPrimitive() || Modifier.isFinal(returnType.getModifiers()) || returnType == Void.class
                || !ReflectUtils.hasNoArgConstructor(returnType)) {
				// We can't proxy these classes, so further recording will be impossible
				return null;
			} else {
                try {
				    return createProxy(returnType);
                } catch (IllegalArgumentException e) {
                    logger.debug("Failed to create a Proxy for result of method call '" + method + "'", e);
                    return null;
                }
			}
		}
	}
}
