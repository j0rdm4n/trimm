/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.proxy;

import dk.tigerteam.trimm.util.ReflectUtils;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * A threadsafe Method call recording proxy, useful for recording (potentially) chained calls to JavaBeans getter property methods. 
 * It records each method call to instances returned by {@link #startRecording(Class)}.
 * Return values from a method call are automatically proxied and are included in the current recording, so method chaining is possible<p/>
 * <b>Note: Simple types, final classes are not proxied and will result in {@link NullPointerException}'s in case methods are called on the return value (which will be null)</b>
 * <b>Note: The current recording is associated with the current thread (because some return type proxies will result in nulls, so we effectively loose track of our recording)
 * , so even though we can record on many threads, we can only have one recording active at the same time on a single thread.</b><p/>
 * <b>Note: Method arguments are not recorded, so this is only useful for calling JavaBeans style (although no restrictions are
 * put into ensuring this, as some classes have getter method that deviate from standard JavaBeans naming style's all though still
 * being valid for this type of recording)</b><p/> 
 * @author Jeppe Cramon
 */
public class ThreadScopedMethodCallRecordingProxy {
    private static final Log log = LogFactory.getLog(ThreadScopedMethodCallRecordingProxy.class);
	private static final ThreadLocal<MethodCallRecordings> currentRecording = new ThreadLocal<MethodCallRecordings>();
	
	@SuppressWarnings("unchecked")
	static <T> T createProxy(Class<T> tClassOrInterface) {
		Enhancer enhancer = new Enhancer();
		if (tClassOrInterface.isInterface()) {
			enhancer.setInterfaces(new Class[] { tClassOrInterface, ThreadScopedMethodCallRecordingProxyMarker.class });
			enhancer.setSuperclass(Object.class);
		} else {
			enhancer.setInterfaces(new Class[] { ThreadScopedMethodCallRecordingProxyMarker.class });
			enhancer.setSuperclass(tClassOrInterface);
		}
		enhancer.setCallback(new MethodCallInterceptor(tClassOrInterface));
        try {
		    return (T) enhancer.create();
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Failed to create a proxy for class '" + tClassOrInterface.getName() + "'", e);
        }
	}
	
	/**
	 * Start recording method calls to the Class or interface. The recording sample can be read using
	 * {@link #getCurrentRecording()}. <br/>
	 * Use {@link #stopAndClearRecording()} to stop recording again. 
	 * @param <T> The type of Class or interface
	 * @param tClassOrInterface The Class or Interface to start recording method calls to
	 * @return A recording proxy
	 */
	public static <T> T startRecording(Class<T> tClassOrInterface) {
		setCurrentRecording(new MethodCallRecordings(tClassOrInterface));
		return createProxy(tClassOrInterface);
	}
	
	/**
	 * Is the instance provided as argument a recording proxy 
	 * @param instance The instance to test
	 * @return true if the instance is an instance of a recording proxy, otherwise false
	 */
	public static boolean isMethodCallRecordingProxyInstance(Object instance) {
		return instance instanceof ThreadScopedMethodCallRecordingProxyMarker;
	}
	
	/**
	 * Get the current recording. Use guard method {@link #hasACurrentRecording()} to test if a recording is in progress
	 * @return The current recording
	 * @throws IllegalStateException if there isn't a current recording
	 */
	public static MethodCallRecordings getCurrentRecording() {
		if (currentRecording.get() == null) {
			throw new IllegalStateException("There isn't a current recording. Did you call createProxy() ?");
		}
		return currentRecording.get();
	}
	
	/**
	 * Is a recording in progress?
	 * @return true if we're currently recording in this thread, otherwise false
	 */
	public static boolean hasACurrentRecording() {
		return currentRecording.get() != null;
	}
	
	/**
	 * Set the current recording
	 * @param recordings The recording to set on the thread
	 */
	static void
  setCurrentRecording(MethodCallRecordings recordings) {
		if (hasACurrentRecording()) {
			throw new IllegalStateException("Can't set a new Recording while a current recording still exists. Did you call removeCurrentRecording() prior to creating a new Recording ?");
		}
        currentRecording.set(recordings);
	}
	
	/**
	 * Stops the recording. Sets the current recording to null and returns 
	 * recording that was just finished.
	 */
	public static MethodCallRecordings stopAndClearRecording() {
		MethodCallRecordings recordings = getCurrentRecording();
		currentRecording.set(null);
		return recordings;
	}
	
	/**
	 * Stops the current recording and creates a new blank recording as the new current recording
	 */
	public static MethodCallRecordings resetCurrentRecording() {
		MethodCallRecordings recordings = getCurrentRecording();
		stopAndClearRecording();
		setCurrentRecording(new MethodCallRecordings(recordings.getRootType()));
		return recordings;
	}	
	
	/**
	 * Marker interface so that we can spot our own proxies
	 */ 
	public static interface ThreadScopedMethodCallRecordingProxyMarker {}
	
	/**
	 * Method calling interceptor, which handles the recording and proxification of return values
	 */
	private static class MethodCallInterceptor extends CglibIdentitySupport implements MethodInterceptor {

		public MethodCallInterceptor(Class<?> wrappedClass) {
			super(wrappedClass);
		}

		public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
			ThreadScopedMethodCallRecordingProxy.getCurrentRecording().addMethodCall(method);
			Class<?> returnType = method.getReturnType();
			if (returnType.isPrimitive() || Modifier.isFinal(returnType.getModifiers()) || returnType == Void.class
                || !ReflectUtils.hasNoArgConstructor(returnType)) {
				// We can't proxy these classes, so further recording will be impossible
				return null;
			} else {
                try {
				    return createProxy(returnType);
                } catch (IllegalArgumentException e) {
                    log.error("Failed to create a Proxy for result of method call '" + method + "'", e);
                    return null;
                }
			}
		}
	}
}
