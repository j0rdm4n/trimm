/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.net;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;


public class SoapUtil {

	private static final String newLine = System.getProperty("line.separator");
	public static final int DEFAULT_TIMEOUT = 10*1000;
	
	public String doSoapRequest(String url, String requestPayload, Map<String, String> urlParameters, Map<String, String> headers, Map<String, String> inputFormatting) {
		HttpURLConnection connection = null;
		try {
			try {
				if(headers != null) {
					headers.put("Content-type", "text/xml");
					headers.put("charset", "UTF-8");
				} else {
					headers = new HashMap<String, String>();
					headers.put("Content-type", "text/xml");
					headers.put("charset", "UTF-8");
				}
				connection = HttpUtil.getConnection(url, urlParameters, headers, "POST", true, true, false, DEFAULT_TIMEOUT);
				HttpUtil.doOutput(connection.getOutputStream(), true, wrapWithSoapEnvelope(requestPayload));
				return HttpUtil.doInput(connection.getInputStream(), inputFormatting);
			} finally {
				if(connection != null) HttpUtil.disconnect(connection);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public String doSoapRequest(String url, String requestPayload, Map<String, String> urlParameters, Map<String, String> headers, Map<String, String> inputFormatting,
			String proxyIp, int proxyPort) {
		HttpURLConnection connection = null;
		try {
			try {
				if(headers != null) {
					headers.put("Content-type", "text/xml");
					headers.put("charset", "UTF-8");
				} else {
					headers = new HashMap<String, String>();
					headers.put("Content-type", "text/xml");
					headers.put("charset", "UTF-8");
				}
				connection = HttpUtil.getConnection(url, urlParameters, headers, "POST", true, true, false, DEFAULT_TIMEOUT, proxyIp, proxyPort);
				HttpUtil.doOutput(connection.getOutputStream(), true, wrapWithSoapEnvelope(requestPayload));
				return HttpUtil.doInput(connection.getInputStream(), inputFormatting);
			} finally {
				if(connection != null) HttpUtil.disconnect(connection);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private String wrapWithSoapEnvelope(String request) {
		StringBuilder builder = new StringBuilder();
    builder.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
    builder.append(newLine);
    builder.append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
    builder.append(newLine);
    builder.append("<soap:Body>");
    builder.append(newLine);
    builder.append(request);
    builder.append(newLine);
    builder.append("</soap:Body>");
    builder.append(newLine);
    builder.append("</soap:Envelope>");
    return builder.toString();
	}
	
}
