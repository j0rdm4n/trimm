/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.net;

import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import java.util.Map;

public class RestUtil {

	public static final int DEFAULT_TIMEOUT = 10*1000;
	public static final String GET = "GET";
	public static final String POST = "POST";
	public static final String PUT = "PUT";
	public static final String DELETE = "DELETE";
	
	/**
	 * 
	 * @return server response as string
	 */
	public String doGet(String url, Map<String, String> urlParameters, Map<String, String> headers, Map<String, String> inputFormatting) {
		HttpURLConnection connection = null;
		try {
			try {
				connection = HttpUtil.getConnection(url, urlParameters, headers, GET, false, true, false, DEFAULT_TIMEOUT);
				String response = HttpUtil.doInput(connection.getInputStream(), inputFormatting); 
				return response;
			} finally {
				if(connection != null) HttpUtil.disconnect(connection);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 
	 * @return server response as string
	 */
	public String doSSLGet(String url, Map<String, String> urlParameters, Map<String, String> headers, Map<String, String> inputFormatting) {
		HttpURLConnection connection = null;
		try {
			try {
				connection = HttpUtil.getSSLConnection(url, urlParameters, headers, GET, false, true, false, DEFAULT_TIMEOUT);
				String response = HttpUtil.doInput(connection.getInputStream(), inputFormatting); 
				return response;
			} finally {
				if(connection != null) HttpUtil.disconnect(connection);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public String doGet(String url, Map<String, String> urlParameters, Map<String, String> headers, Map<String, String> inputFormatting, String proxyIp, int proxyPort) {
		HttpURLConnection connection = null;
		try {
			try {
				connection = HttpUtil.getConnection(url, urlParameters, headers, GET, false, true, false, DEFAULT_TIMEOUT);
				String response = HttpUtil.doInput(connection.getInputStream(), inputFormatting); 
				return response;
			} finally {
				if(connection != null) HttpUtil.disconnect(connection);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public String doPost(String url, String requestPayload, Map<String, String> urlParameters, Map<String, String> headers, Map<String, String> inputFormatting) {
		HttpURLConnection connection = null;
		try {
			try {
				if(requestPayload != null) {
					connection = HttpUtil.getConnection(url, urlParameters, headers, POST, true, true, false, DEFAULT_TIMEOUT);
					HttpUtil.doOutput(connection.getOutputStream(), true, requestPayload);
				} else {
					connection = HttpUtil.getConnection(url, urlParameters, headers, POST, false, true, false, DEFAULT_TIMEOUT);
				}
				String response = HttpUtil.doInput(connection.getInputStream(), inputFormatting);
				System.out.println(response);
				return response;
			} finally {
				if(connection != null) HttpUtil.disconnect(connection);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public String doSSLPost(String url, String requestPayload, Map<String, String> urlParameters, Map<String, String> headers, Map<String, String> inputFormatting) {
		HttpURLConnection connection = null;
		try {
			try {
				if(requestPayload != null) {
					connection = HttpUtil.getSSLConnection(url, urlParameters, headers, POST, true, true, false, DEFAULT_TIMEOUT);
					HttpUtil.doOutput(connection.getOutputStream(), true, requestPayload);
				} else {
					connection = HttpUtil.getSSLConnection(url, urlParameters, headers, POST, false, true, false, DEFAULT_TIMEOUT);
				}
				String response = HttpUtil.doInput(connection.getInputStream(), inputFormatting);
				return response;
			} finally {
				if(connection != null) HttpUtil.disconnect(connection);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public String doPut(String url, String requestPayload, Map<String, String> urlParameters, Map<String, String> headers, Map<String, String> inputFormatting) {
		HttpURLConnection connection = null;
		try {
			try {
				if(requestPayload != null) {
					connection = HttpUtil.getConnection(url, urlParameters, headers, PUT, true, true, false, DEFAULT_TIMEOUT);
					HttpUtil.doOutput(connection.getOutputStream(), true, requestPayload);
				} else {
					connection = HttpUtil.getConnection(url, urlParameters, headers, PUT, false, true, false, DEFAULT_TIMEOUT);
				}
				String response = HttpUtil.doInput(connection.getInputStream(), inputFormatting); 
				return response;
			} finally {
				if(connection != null) HttpUtil.disconnect(connection);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public String doDelete(String url, String requestPayload, Map<String, String> urlParameters, Map<String, String> headers, Map<String, String> inputFormatting) {
		HttpURLConnection connection = null;
		try {
			try {
				if(requestPayload != null) {
					connection = HttpUtil.getConnection(url, urlParameters, headers, DELETE, true, true, false, DEFAULT_TIMEOUT);
					HttpUtil.doOutput(connection.getOutputStream(), true, requestPayload);
				} else {
					connection = HttpUtil.getConnection(url, urlParameters, headers, DELETE, false, true, false, DEFAULT_TIMEOUT);
				}
				String response = HttpUtil.doInput(connection.getInputStream(), inputFormatting); 
				return response;
			} finally {
				if(connection != null) HttpUtil.disconnect(connection);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	protected Charset getCharset(String encoding) {
		return Charset.forName(encoding);
	}
	
	
}
