/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.net;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

public class HttpUtil {

	private static final String newLine = System.getProperty("line.separator");
	public static final String DEFAULT_ENCODING = "UTF-8";
	public static final String DEFAULT_CHARSET = "UTF-8";
	public static final int DEFAULT_TIMEOUT = 10*1000;
	
	private HttpUtil() {}
	
	public static URL getURL(String url, Map<String, String> urlParameters) {
		try {
			String parameters = prepareUrl(urlParameters);
			if(parameters != null) {
//				url = url + URLEncoder.encode(parameters, DEFAULT_ENCODING);
				url = url + parameters;
			}
			return new URL(url);
		} catch (Exception e) {
			throw new RuntimeException("Get url failed " + e.getMessage(), e);
		}
	}
	
	public static void doOutput(OutputStream stream, boolean flush, String payload) {
		doOutput(stream, flush, payload, DEFAULT_ENCODING);
	}
	
	public static void doOutput(OutputStream stream, boolean flush, String payload, String encoding) {
		BufferedOutputStream outputStream = null;
		try {
			try {
				outputStream = new BufferedOutputStream(stream);
				outputStream.write(payload.getBytes(encoding));
				if(flush) outputStream.flush();
			} finally {
				if(outputStream != null) outputStream.close();
			}
		} catch (Exception e) {
			throw new RuntimeException("Doing output failed " + e.getMessage(), e);
		}
	}
	
	public static String doInput(InputStream stream, Map<String, String> formatting) {
		BufferedReader reader = null;
		try {
			try {
				reader = new BufferedReader(new InputStreamReader(stream));
				String line = null;
				StringBuilder builder = new StringBuilder();
				while((line = reader.readLine()) != null) {
					if(formatting != null) {
						builder.append(escapeFormatting(line, formatting)).append(newLine);
					} else {
						builder.append(line).append(newLine);
					}
				}
				return builder.toString();
			} finally {
				if(reader != null) reader.close();
			}
		} catch (Exception e) {
			throw new RuntimeException("Doing input failed " + e.getMessage(), e);
		}
	}
	
	public static String simpleGetRequest(String url, Map<String, String> urlParameters) {
		try {
			return doInput(getURL(url, urlParameters).openStream(), null);
		} catch (IOException e) {
			throw new RuntimeException("Simple GetRequest failed " + e.getMessage(), e);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static HttpURLConnection getConnection(String url, Map<String, String> urlParameters, Map<String, String> headers, String httpMethod, boolean doOutput,
			boolean doInput, boolean useCache, int timeout) {
		HttpURLConnection connection = null;
		try {
			String parameters = prepareUrl(urlParameters);
			if(parameters != null) {
//				url = url + URLEncoder.encode(parameters, DEFAULT_ENCODING);
				url = url + parameters;
			}
			URL url_ = new URL(url);
			connection = (HttpURLConnection) url_.openConnection();
			connection.setDoInput(doInput);
			connection.setDoOutput(doOutput);
			connection.setUseCaches(useCache);
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);
			connection.setRequestMethod(httpMethod);
			if(headers != null) {
				for (Iterator it = headers.entrySet().iterator(); it.hasNext();) {
					Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
					connection.setRequestProperty(entry.getKey(), entry.getValue());
				}
			}
			return connection;
		} catch (Exception e) {
			throw new RuntimeException("Get connection failed '" + url + "' " + e.getMessage(), e);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static HttpURLConnection getSSLConnection(String url, Map<String, String> urlParameters, Map<String, String> headers, String httpMethod, boolean doOutput,
			boolean doInput, boolean useCache, int timeout) {
		try {
			System.setProperty( "java.protocol.handler.pkgs" , "javax.net.ssl" );
	//    java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
	    TrustManager[] trustAllCerts = new TrustManager[] {
	          new X509TrustManager(){
	                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                      return null;
	                }
	          public void checkClientTrusted( java.security.cert.X509Certificate[] certs, String authType ) { }
	                public void checkServerTrusted( java.security.cert.X509Certificate[] certs, String authType ) { }
	          }
	    };
	    SSLContext sc = SSLContext.getInstance("SSL");
	    sc.init( null, trustAllCerts, new java.security.SecureRandom() );
	    HttpsURLConnection.setDefaultSSLSocketFactory( sc.getSocketFactory() ); 
			HttpsURLConnection connection = null;
			String parameters = prepareUrl(urlParameters);
			if(parameters != null) {
//				url = url + URLEncoder.encode(parameters, DEFAULT_ENCODING);
				url = url + parameters;
			}
			URL url_ = new URL(url);
			connection = (HttpsURLConnection) url_.openConnection();
			connection.setDoInput(doInput);
			connection.setDoOutput(doOutput);
			connection.setUseCaches(useCache);
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);
			connection.setRequestMethod(httpMethod);
			if(headers != null) {
				for (Iterator it = headers.entrySet().iterator(); it.hasNext();) {
					Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
					connection.setRequestProperty(entry.getKey(), entry.getValue());
				}
			}
			return connection;
		} catch (Exception e) {
			throw new RuntimeException("Get ssl connection failed '" + url + "' " + e.getMessage(), e);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static HttpURLConnection getConnection(String url, Map<String, String> urlParameters, Map<String, String> headers, String httpMethod, boolean doOutput,
			boolean doInput, boolean useCache, int timeout, String proxyIp, int proxyPort) {
		HttpURLConnection connection = null;
		try {
			String parameters = prepareUrl(urlParameters);
			if(parameters != null) {
//				url = url + URLEncoder.encode(parameters, DEFAULT_ENCODING);
				url = url + parameters;
			}
			URL url_ = new URL(url);
			connection = (HttpURLConnection) url_.openConnection(createHttpProxy(proxyIp, proxyPort));
			connection.setDoInput(doInput);
			connection.setDoOutput(doOutput);
			connection.setUseCaches(useCache);
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);
			connection.setRequestMethod(httpMethod);
			if(headers != null) {
				for (Iterator it = headers.entrySet().iterator(); it.hasNext();) {
					Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
					connection.setRequestProperty(entry.getKey(), entry.getValue());
				}
			}
			return connection;
		} catch (Exception e) {
			throw new RuntimeException("Get connection failed '" + url + "' " + e.getMessage(), e);
		}
	}
	
	public static void disconnect(HttpURLConnection connection) {
		if(connection != null) connection.disconnect();
	}
	
	protected static Proxy createHttpProxy(String ip, int port) {
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ip, port));
		return proxy;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static String prepareUrl(Map<String, String> urlParameters) {
		if (urlParameters != null ) {
			StringBuilder builder =  new StringBuilder();
			builder.append("?");
			boolean first = true;
			for (Iterator it = urlParameters.entrySet().iterator(); it.hasNext();) {
				Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
				if(first) {
					builder.append(entry.getKey());
					first = false;
				} else {
					builder.append("&" + entry.getKey());
				}
				if(entry.getValue() != null && !entry.getValue().equals("")) {
					builder.append("=" + entry.getValue());
				}
			}
			return builder.toString();
		}
		return null;
	}
	
	/**
	 * Could be use to remove &lt with < or &gt with >
	 * @param line the text being escaped
	 * @param formatting the keys represent text strings that will be matched against line and replaced with their matching value
	 * @return line with escaped formatting
	 */
	@SuppressWarnings({"unchecked"})
	private static String escapeFormatting(String line, Map<String, String> formatting) {
		if(formatting != null) {
			for (@SuppressWarnings("rawtypes")
			Iterator it = formatting.entrySet().iterator(); it.hasNext();) {
				Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
				line = line.replaceAll(entry.getKey(), entry.getValue());
			}
		}
		return line;
	}
	
}
