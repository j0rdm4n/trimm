/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.bean.bidirectional;

import dk.tigerteam.trimm.collection.WrappedList;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * A OneToMany bidirectional wrapper for {@link List}' (the implementation on the opposite side is a {@link ManyToOneWrapper}).<br/>
 * Example (where Person is One Side and Address is Many side):
 * <pre>
 +------------------+               +-------------------+
 |     Person       |               |     Address       |
 |------------------|               |-------------------|
 |                  |1             *|                   |
 |                  +---------------+                   |
 |                  |               |                   |
 +------------------+               +-------------------+
 *
 * <pre>
 * public class Person {
 * 	private List&lt;Address> addresses = new ArrayList&lt;Address>();
 * 	
 * 	public List&lt;Address> getAddresses() {
 * 	  return new OneToManyCollectionWrapper&lt;Person, Address>(this, addresses) {
 * 			{@literal @Override}
 * 			protected Person getOneSideObjectInManySideObject(Address manySideObject) {
 * 				return manySideObject.getPerson();
 * 			} 
 * 
 * 			{@literal @Override}
 * 			protected void setOneSideObjectInManySideObject(Address manySideObject, Person oneSideObject) {
 * 				manySideObject.setPerson(oneSideObject);
 * 			}
 * 		};
 * 	  }
 * 	....
 * 	}
 * 	---------------------------------------------------------
 * 	public class Address {
 * 	  private Person person;
 * 
 * 	  public Person getPerson() {
 * 	 	return person;
 * 	  }
 * 
 * 	  public void setPerson(Person person) {
 * 		new ManyToOneWrapper&lt;Person, Address>(this) {
 * 			{@literal @Override}
 * 			protected void addManySideObjectToOneSideCollection(Person oneSide,
 * 					Address manySide) {
 * 				((OneToManyListWrapper&lt;Person, Address>)oneSide.getAddresses()).getWrappedCollection().add(manySide);
 * 			}
 * 
 * 			{@literal @Override}
 * 			protected Person getOneSideObjectInManySideObject(Address manySide) {
 * 				return manySide.getPerson();
 * 			}
 * 
 * 			{@literal @Override}
 * 			protected void removeManySideObjectFromOneSideCollection(
 * 					Person oneSide, Address manySide) {
 * 				((OneToManyListWrapper&lt;Person, Address>)oneSide.getAddresses()).getWrappedCollection().remove(manySide);
 * 			}
 * 
 * 			{@literal @Override}
 * 			protected void setOneSideObjectInManySideObject(Address manySide,
 * 					Person oneSide) {
 * 				Address.this.person = oneSide;
 * 			}
 * 		}.updateOneSideObject(person);
 * 	  }
 * 	....
 * 	}
 * </pre>
 * @see ManyToOneWrapper
 * @author Jeppe Cramon
 * @param <ONE_SIDE> The type of object on the ONE side of the one-to-many relationship 
 * @param <MANY_SIDE> The type of objects on the MANY side of the one-to-many relationship
 */
public abstract class OneToManyListWrapper<ONE_SIDE, MANY_SIDE> extends WrappedList<MANY_SIDE> {
	
	private static final long serialVersionUID = -2312369363108674219L;
	
	private ONE_SIDE oneSideObject;

	public OneToManyListWrapper(ONE_SIDE oneSideObject,
			List<MANY_SIDE> manySideCollection) {
		super(manySideCollection);
		this.oneSideObject = oneSideObject;
	}

	@Override
	public boolean add(MANY_SIDE manySideObject) {
		if (getOneSideObjectInManySideObject(manySideObject) == oneSideObject) {
			return false;
		}
		setOneSideObjectInManySideObject(manySideObject, oneSideObject);
		return true;
	}
	
	@Override
	public boolean addAll(Collection<? extends MANY_SIDE> c) {
		boolean modified = false;
		List<MANY_SIDE> tmpSet = new LinkedList<MANY_SIDE>(c);
		
		Iterator<MANY_SIDE> e = tmpSet.iterator();
		while (e.hasNext()) {
		    MANY_SIDE objectToAdd = e.next();
			if (add( objectToAdd)) {
		    	modified = true;
		    }
		}
		return modified;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean remove(Object o) {
		boolean changed = super.remove(o);
		if (changed) {
			MANY_SIDE manySideObject = (MANY_SIDE)o;
			boolean oneSideObjectHasRelationShipToManySideObject = getOneSideObjectInManySideObject(manySideObject) == oneSideObject;
			if (oneSideObjectHasRelationShipToManySideObject) {
				setOneSideObjectInManySideObject(manySideObject, null);
			}
		}
		return changed;
	}

	
	@Override
	public void add(int index, MANY_SIDE manySideObject) {
		throw new UnsupportedOperationException("Only add() is supported");
	}

	@Override
	public boolean addAll(int index, Collection<? extends MANY_SIDE> c) {
		throw new UnsupportedOperationException("Only addAll() is supported");
	}

	@Override
	public MANY_SIDE remove(int index) {
		MANY_SIDE removedManySideObject = super.remove(index);
		boolean oneSideObjectHasRelationShipToManySideObject = getOneSideObjectInManySideObject(removedManySideObject) == oneSideObject;
		if (oneSideObjectHasRelationShipToManySideObject) {
			setOneSideObjectInManySideObject(removedManySideObject, null);
		}
		return removedManySideObject;
	}

	@Override
	public MANY_SIDE set(int index, MANY_SIDE element) {
		throw new UnsupportedOperationException("set() isn't supported");
	}

	/**
	 * Get the reference to the oneSideObject in the manySideObject
	 */
	protected abstract ONE_SIDE getOneSideObjectInManySideObject(MANY_SIDE manySideObject);
	
	/**
	 * Get the reference to the oneSideObject in the manySideObject
	 */
	protected abstract void setOneSideObjectInManySideObject(MANY_SIDE manySideObject, ONE_SIDE oneSideObject);
}
