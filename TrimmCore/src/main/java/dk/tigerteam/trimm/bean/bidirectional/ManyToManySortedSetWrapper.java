/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.bean.bidirectional;

import dk.tigerteam.trimm.collection.WrappedSortedSet;

import java.util.*;

/**
 * A ManyToManyCollectionWrapper for use in Many-to-Many associations (i.e. using a {@link ManyToManySortedSetWrapper} on both sides of the association).<br/>
 * Example:
 * <pre>
 * public class Person {
 * 	private SortedSet&lt;Company> worksForCompanies = new TreeSet&lt;Company>();
 * 	
 * 	public SortedSet&lt;Company> getWorksForCompanies() {
 * 		return new ManyToManySortedSetWrapper&lt;Person, Company>(this, worksForCompanies) {
 * 			{@literal @Override}
 * 			protected Collection&lt;Person> getSourceCollectionInTarget(Company o) {
 * 				return o.getEmployees();
 * 			}
 * 		};
 * 	}
 * }
 * ------------------------------------------------------------
 * public class Company {
 * 	private SortedSet&lt;Person> employees = new TreeSet&lt;Person>();
 * 	
 * 	public SortedSet&lt;Person> getEmployees() {
 * 		return new ManyToManySortedSetWrapper&lt;Company, Person>(this, employees) {
 * 			{@literal @Override}
 * 			protected Collection&lt;Company> getSourceCollectionInTarget(Person o) {
 * 				return o.getWorksForCompanies();
 * 			}
 * 		};
 * 	}
 * } 	
 * </pre>
 * @author Jeppe Cramon
 *
 * @param <SOURCE_TYPE> The type of the Source side
 * @param <TARGET_TYPE> The type of the Target side
 */
public abstract class ManyToManySortedSetWrapper<SOURCE_TYPE, TARGET_TYPE> extends WrappedSortedSet<TARGET_TYPE> {
	
	private static final long serialVersionUID = -3183524752075491262L;
	
	private SOURCE_TYPE source;
	public ManyToManySortedSetWrapper(SOURCE_TYPE source, SortedSet<TARGET_TYPE> targetCollectionInSource) {
		super(targetCollectionInSource);
		this.source = source;
	}

	@Override
	public boolean add(TARGET_TYPE o) {
		boolean changed = false;
		if (!getWrappedCollection().contains(o)) {
			changed |= getWrappedCollection().add(o);
			Collection<SOURCE_TYPE> sourceCollection = getSourceCollectionInTarget(o);
			sourceCollection.add(source);
		}
		return changed;
	}
	
	@Override
	public boolean addAll(Collection<? extends TARGET_TYPE> c) {
		boolean modified = false;
		Set<TARGET_TYPE> tmpSet = new HashSet<TARGET_TYPE>(c);
		
		Iterator<TARGET_TYPE> e = tmpSet.iterator();
		while (e.hasNext()) {
			TARGET_TYPE objectToAdd = e.next();
			if (add( objectToAdd)) {
		    	modified = true;
		    }
		}
		return modified;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean remove(Object o) {
		boolean changed = false;
		if (getWrappedCollection().contains(o)) {
			changed |= getWrappedCollection().remove(o);
			Collection<SOURCE_TYPE> sourceCollection = getSourceCollectionInTarget((TARGET_TYPE) o);
			sourceCollection.remove(source);
		}
		return changed;
	}

    /**
     * Get the opposite Collection (which contains Source instances) in the target object
     * @param o the target object
     * @return The source collection
     */
	protected abstract Collection<SOURCE_TYPE> getSourceCollectionInTarget(TARGET_TYPE o);
}
