/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.bean.bidirectional;

import dk.tigerteam.trimm.collection.WrappedCollection;

import java.io.Serializable;

/**
 * A ManyToOne bidirectional wrapper for One side of the relationship (the implementation on the opposite side is typically a 
 * {@link OneToManyCollectionWrapper}, {@link OneToManySetWrapper} or {@link OneToManyListWrapper}).<p/>
 * Example (where Address is Many side and Person is One Side):
 * <pre>
 +------------------+               +-------------------+
 |     Person       |               |     Address       |
 |------------------|               |-------------------|
 |                  |1             *|                   |
 |                  +---------------+                   |
 |                  |               |                   |
 +------------------+               +-------------------+
 *
 * public class Address {
 * 	private Person person;
 * 	private Set&lt;ZipCode> zipCodes = new HashSet&lt;ZipCode>();
 * 	
 * 	public Person getPerson() {
 * 		return person;
 * 	}
 * 
 * 	public void setPerson(Person person) {
 * 		new ManyToOneWrapper&lt;Person, Address>(this) {
 * 			{@literal @Override}
 * 			protected void addManySideObjectToOneSideCollection(Person oneSide,
 * 					Address manySide) {
 * 				((OneToManyCollectionWrapper<Person, Address>)oneSide.getAddresses()).getWrappedCollection().add(manySide);
 * 			} 
 * 
 * 			{@literal @Override}
 * 			protected Person getOneSideObjectInManySideObject(Address manySide) {
 * 				return manySide.getPerson();
 * 			}
 * 
 * 			{@literal @Override}
 * 			protected void removeManySideObjectFromOneSideCollection(
 * 					Person oneSide, Address manySide) {
 * 				((OneToManyCollectionWrapper<Person, Address>)oneSide.getAddresses()).getWrappedCollection().remove(manySide);
 * 			}
 * 
 * 			{@literal @Override}
 * 			protected void setOneSideObjectInManySideObject(Address manySide,
 * 					Person oneSide) {
 * 				Address.this.person = oneSide;
 * 			}
 * 		}.updateOneSideObject(person);
 * 	  }
 * 	  ....
  * }
 * </pre>
 * @see OneToManyCollectionWrapper
 * @see OneToManySetWrapper
 * @see OneToManyListWrapper
 * @author Jeppe Cramon
 * @param <ONE_SIDE> The type of object on the ONE side of the one-to-many relationship 
 * @param <MANY_SIDE> The type of objects on the MANY side of the one-to-many relationship
 */
public abstract class ManyToOneWrapper<ONE_SIDE, MANY_SIDE> implements Serializable {
	
	private static final long serialVersionUID = -6447107518071982599L;
	
	private final MANY_SIDE manySideObject;

	public ManyToOneWrapper(MANY_SIDE manySideObject) {
		super();
		this.manySideObject = manySideObject;
	}
	
	/**
	 * Update the oneSide object relationship
	 * @param newOneSide The new oneside object
	 */
	public void updateOneSideObject(ONE_SIDE newOneSide) {
		ONE_SIDE currentOneSide = getOneSideObjectInManySideObject(manySideObject);
		if (currentOneSide == newOneSide) return;
		if (currentOneSide == null) {
			if (newOneSide != null) {
				setOneSideObjectInManySideObject(manySideObject, newOneSide);
				addManySideObjectToOneSideCollection(newOneSide, manySideObject);
			}
		} else {
			setOneSideObjectInManySideObject(manySideObject, newOneSide);
			removeManySideObjectFromOneSideCollection(currentOneSide, manySideObject);
			if (newOneSide != null) {
				addManySideObjectToOneSideCollection(newOneSide, manySideObject);
			}
		}
	}

	/**
	 * Remove a manySide instance from the oneSide's manySide collection.<br/>
	 * To avoid endless recursive calls, this removal to the Collection MUST be direct and go directly through
	 * the {@link WrappedCollection#getWrappedCollection()}<p/>
	 * Example:
	 * <pre>
	 * {@literal @Override}
	 * protected void removeManySideObjectFromOneSideCollection(Person oneSide, Address manySide) {
	 *  	((OneToManyCollectionWrapper<Person, Address>)oneSide.getAddresses()).getWrappedCollection().remove(manySide);
	 * }
	 * </pre>
	 */
	protected abstract void removeManySideObjectFromOneSideCollection(ONE_SIDE oneSide, MANY_SIDE manySide);

	/**
	 * Add a manySide instance to the oneSide's manySide collection. <br/>
	 * To avoid endless recursive calls, this addition to the Collection MUST be direct and go directly through
	 * the {@link WrappedCollection#getWrappedCollection()}<p/>
	 * Example:
	 * <pre>
	 * protected void addManySideObjectToOneSideCollection(Person oneSide, Address manySide) {
	 * 	 ((OneToManyCollectionWrapper<Person, Address>)oneSide.getAddresses()).getWrappedCollection().add(manySide);
	 * } 
	 * </pre>
	 */
	protected abstract void addManySideObjectToOneSideCollection(ONE_SIDE oneSide, MANY_SIDE manySide);

	/**
	 * Update the manySide's reference to the oneSide.<br/>
	 * To avoid endless recursive call, this reference setting should be direct and not go thorugh a property setter method.<br/>
	 * Example:
	 * <pre>
	 * {@literal @Override}
	 * protected void setOneSideObjectInManySideObject(Address manySide, Person oneSide) {
	 *  	Address.this.person = oneSide; // or manySide.person = oneSide;
	 * }
	 * </pre>
	 */
	protected abstract void setOneSideObjectInManySideObject(MANY_SIDE manySide, ONE_SIDE oneSide);

	/**
	 * Get the manySide's reference to the oneSide.<p/>
	 * Example:
	 * <pre>
	 *  {@literal @Override}
	 * 	protected Person getOneSideObjectInManySideObject(Address manySide) {
	 *  	return manySide.getPerson();
	 * }
	 * </pre>
	 */
	protected abstract ONE_SIDE getOneSideObjectInManySideObject(MANY_SIDE manySide);
	
	/**
	 * Get access to the wrapped manySide object
	 * @return The wrapped manySide object
	 */
	protected MANY_SIDE getManySideObject(){
		return manySideObject;
	}
}
