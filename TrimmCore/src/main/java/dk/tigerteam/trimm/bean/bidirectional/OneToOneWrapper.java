/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.bean.bidirectional;

import java.io.Serializable;

/**
 * Works with the concepts SOURCE and TARGET objects. The SOURCE object is the object that owns the target property.
 * <p>
 * Example: If we have a OneToOne relationship between Person and PersonName and we're currently working with the Person object
 * then the SOURCE object will be Person and TARGET object will be PersonName (and vice versa).<br/>
 * <pre>
 * public class Person {
 * 	private PersonName personName;
 * 
 * 	public PersonName getPersonName() {
 * 		return personName;
 * 	}
 * 
 * 	public void setPersonName(PersonName personName) {
 * 		new OneToOneWrapper&lt;PersonName>() {
 * 			{@literal @Override}
 * 			protected PersonName getTargetObjectInSourceObject() {
 * 				return Person.this.personName;
 * 			}
 * 			
 * 			{@literal @Override}
 * 			protected void setTargetObjectInSourceObject(PersonName newTarget) {
 * 				Person.this.personName = newTarget;
 * 			}
 * 
 * 			{@literal @Override}
 * 			protected void createRelationToTargetObject(PersonName newTarget) {
 * 				newTarget.setPerson(Person.this);
 * 			} 
 * 
 * 			{@literal @Override}
 * 			protected void removeRelationToTargetObject(PersonName currentTarget) {
 * 				currentTarget.setPerson(null);
 * 			}
 * 		}.updateTargetProperty(personName);
 * 	  }
 * }
 * -----------------------------------------------------------------------------------
 * public class PersonName {
 * 	private Person person;
 * 
 * 	public Person getPerson() {
 * 		return person;
 * 	}
 * 
 * 	public void setPerson(Person person) {
 * 		new OneToOneWrapper<Person>() {
 * 			{@literal @Override}
 * 			protected void createRelationToTargetObject(Person newTarget) {
 * 				newTarget.setPersonName(PersonName.this);
 * 			} 
 * 
 * 			{@literal @Override}
 * 			protected Person getTargetObjectInSourceObject() {
 * 				return PersonName.this.person;
 * 			}
 * 
 * 			{@literal @Override}
 * 			protected void removeRelationToTargetObject(Person currentTarget) {
 * 				currentTarget.setPersonName(null);
 * 			}
 * 
 * 			{@literal @Override}
 * 			protected void setTargetObjectInSourceObject(Person newTarget) {
 * 				PersonName.this.person = newTarget;
 * 			}
 * 		}.updateTargetProperty(person);
 * 	  }
 * }
 * </pre>
 * </p> 
 * Note: This wrapper is always implemented inside the SOURCE object (typically within the setXxxx(target) property method).
 * @author Jeppe Cramon
 *
 * @param <TARGET> The type of the target object
 */
public abstract class OneToOneWrapper<TARGET> implements Serializable {
	
	private static final long serialVersionUID = 8324675723723125652L;

	/**
	 * Update the target property in our source object to the value of the newTarget argument
	 * @param newTarget The new value for the target property in our source object
	 * @return The new value of the target property in our source object
	 */
	public TARGET updateTargetProperty(TARGET newTarget) {
		TARGET currentTarget = getTargetObjectInSourceObject();
		if (currentTarget == newTarget) return getTargetObjectInSourceObject();

		setTargetObjectInSourceObject(newTarget);
		
		boolean hasRelationToCurrentTarget = (currentTarget != null);
		if (hasRelationToCurrentTarget) {
			removeRelationToTargetObject(currentTarget);
		}
		
		boolean shouldCreateRelationToNewTarget = newTarget != null;
		if (shouldCreateRelationToNewTarget) {
			createRelationToTargetObject(newTarget);
		}
		return getTargetObjectInSourceObject();
	}
	
	/**
	 * Set the value of the target property in the source object
	 * @param newTarget The new value for the target property in the source object
	 */
	protected abstract void setTargetObjectInSourceObject(TARGET newTarget);

	/**
	 * Return the value of the target property in the source object.
	 * @return The value of the target property
	 */
	protected abstract TARGET getTargetObjectInSourceObject();
	
	/**
	 * Create a relationship to our source object in the newTarget object
	 * @param newTarget The target object which should establish a relationship to our source object
	 */
	protected abstract void createRelationToTargetObject(TARGET newTarget);

	/**
	 * Remove the relation shop to our source object on the target side, so 
	 * that the old relationship is broken
	 * @param currentTarget The current target object, which has a relation shop with our source object
	 */
	protected abstract void removeRelationToTargetObject(TARGET currentTarget);
}
