/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.collection;

import java.util.HashMap;
import java.util.Map;

/**
 * TMap implementation which allows wrapping over existing Map instances
 */
public class WrappedMap<KEY, VALUE> implements TMap<KEY, VALUE> {
  private Map<KEY, VALUE> wrappedMap;
  private TSet<KEY> keySet;
  private TCollection<VALUE> valueCollection;
  private TSet<Entry<KEY, VALUE>> entrySet;

  public WrappedMap(Map<KEY, VALUE> wrappedMap) {
    this.wrappedMap = wrappedMap;
  }

  /**
   * Create a new instance with a wrapped HashMap
   *
   * @param <KEY>
   * @param <VALUE>
   * @return an initialized WrappedMap
   */
  public static <KEY, VALUE> TMap<KEY, VALUE> newInstance() {
    return new WrappedMap<KEY, VALUE>(new HashMap<KEY, VALUE>());
  }

  public int size() {
    return wrappedMap.size();
  }

  public boolean isEmpty() {
    return wrappedMap.isEmpty();
  }

  public boolean containsKey(Object key) {
    return wrappedMap.containsKey(key);
  }

  public boolean containsValue(Object value) {
    return wrappedMap.containsValue(value);
  }

  public VALUE get(Object key) {
    return wrappedMap.get(key);
  }

  public VALUE put(KEY key, VALUE value) {
    return wrappedMap.put(key, value);
  }

  public VALUE remove(Object key) {
    return wrappedMap.remove(key);
  }

  public void putAll(Map<? extends KEY, ? extends VALUE> m) {
    wrappedMap.putAll(m);
  }

  public void clear() {
    wrappedMap.clear();
  }

  public TSet<KEY> keySet() {
    if (keySet == null) {
      keySet = new WrappedSet<KEY>(wrappedMap.keySet());
    }
    return keySet;
  }

  public TCollection<VALUE> values() {
    if (valueCollection == null) {
      valueCollection = new WrappedCollection<VALUE>(wrappedMap.values());
    }
    return valueCollection;
  }

  public TSet<Entry<KEY, VALUE>> entrySet() {
    if (entrySet == null) {
      entrySet = new WrappedSet<Entry<KEY, VALUE>>(wrappedMap.entrySet());
    }
    return entrySet;
  }

  public boolean equals(Object o) {
    return wrappedMap.equals(o);
  }

  public int hashCode() {
    return wrappedMap.hashCode();
  }
}
