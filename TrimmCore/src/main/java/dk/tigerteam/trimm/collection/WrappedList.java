/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.collection;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Helper class which makes it easy to wrap a {@link List} and apply extra functionality to it
 * @author Jeppe Cramon
 *
 * @param <T> The type of objects in the List
 */
public class WrappedList<T> extends WrappedCollection<T> implements TList<T> {
	private static final long serialVersionUID = 1273205644034136281L;

	/**
	 * Default constructor for deferred wrapping. Call {@link #setWrappedList(List)} to supply the List
	 */
	public WrappedList() {}

	/**
	 * Default constructor for immediate wrapping
	 * @param wrappedList The List to wrap
	 */
	public WrappedList(List<T> wrappedList) {
		super(wrappedList);
	}

    /**
   * Create a new instance with a prewrapped LinkedList
   * @param <T>
   * @return an initialized WrappedList
   */
  public static <T> TList<T> newInstance() {
    return new WrappedList<T>(new LinkedList<T>());
  }
	
	/**
	 * For subclass access to the wrapped List.<br/>
	 * Sugar method for calling {@link #getWrappedCollection()} and performing a cast to List&lt;T>.
	 * @return The wrapped List
	 */
	protected final List<T> getWrappedList() {
		return (List<T>)getWrappedCollection();
	}
	
	/**
	 * For subclasses which needs to set the wrappedList later than at construction time.<br/>
	 * Sugar method for calling {@link #setWrappedCollection(java.util.Collection)}.
	 * @param list The list to wrap
	 */
	protected void setWrappedList(List<T> list) {
		setWrappedCollection(list);
	}

	public T get(int index) {
		return getWrappedList().get(index);
	}

	public T remove(int index) {
		return getWrappedList().remove(index);
	}
	
	public ListIterator<T> listIterator() {
		return getWrappedList().listIterator();
	}
	
	public ListIterator<T> listIterator(int index) {
		return getWrappedList().listIterator(index);
	}
	
	public void add(int index, T element) {
		getWrappedList().add(index, element);
	}

	public boolean addAll(int index, Collection<? extends T> c) {
		return getWrappedList().addAll(index, c);
	}

	public int indexOf(Object o) {
		return getWrappedList().indexOf(o);
	}

	public int lastIndexOf(Object o) {
		return getWrappedList().lastIndexOf(o);
	}

	public T set(int index, T element) {
		return getWrappedList().set(index, element);
	}

	public List<T> subList(int fromIndex, int toIndex) {
		return getWrappedList().subList(fromIndex, toIndex);
	}
}
