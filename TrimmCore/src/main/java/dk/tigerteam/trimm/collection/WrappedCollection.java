/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.collection;

import dk.tigerteam.trimm.util.Utils;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Helper class which makes it easy to wrap a {@link Collection} and apply extra functionality to it
 * @author Jeppe Cramon
 * @param <T> The type of items in the Collection
 */
public class WrappedCollection<T> implements TCollection<T>, Serializable {
	private static final long serialVersionUID = -5624568397197869124L;
	private Collection<T> wrappedCollection;
	
	/**
	 * Default constructor for deferred wrapping. Call {@link #setWrappedCollection(Collection)} to supply the collection
	 */
	public WrappedCollection() {}
	
	/**
	 * Default constructor for immediate wrapping
	 * @param wrappedCollection The collection to wrap
	 */
	public WrappedCollection(Collection<T> wrappedCollection){
		this.wrappedCollection = wrappedCollection;
	}

  /**
   * Factory method to create a new WrappedCollection
   * @param <T>  The type of objects to contain in the collection
   * @return The Wrapped collection instance
   */
  public static <T> TCollection<T> newInstance() {
    return new WrappedCollection<T>(new LinkedList<T>());
  }
	
	/**
	 * For subclass access to the wrapped collection
	 * @return The wrapped collection (the real internal collection)
	 */
	public final Collection<T> getWrappedCollection() {
		return wrappedCollection;
	}

	/**
 	 * For subclasses which needs to set the wrappedCollection later than at construction time
	 * @param wrappedCollection The collection to wrap
	 */
	public void setWrappedCollection(Collection<T> wrappedCollection) {
		this.wrappedCollection = wrappedCollection;
	}

  public void each(Utils.F<T> function) {
    Utils.each(this, function);
  }

  public <TO> Collection<TO> map(Utils.Function<? super T, TO> mapFunction) {
    return Utils.map(this, mapFunction);
  }
	
	public boolean add(T o) {
		return wrappedCollection.add(o);
	}

	public boolean addAll(Collection<? extends T> c) {
		boolean modified = false;
		Iterator<? extends T> e = c.iterator();
		while (e.hasNext()) {
		    if (add(e.next()))
			modified = true;
		}
		return modified;
	}

	public boolean retainAll(Collection<?> c) {
		boolean changed = false;
		List<T> objectsToDelete = new LinkedList<T>();
		for (T t : wrappedCollection) {
			if (!c.contains(t)) {
				objectsToDelete.add(t);
			}
		}
		for (Object object : objectsToDelete) {
			changed |= remove(object);
		}
		return changed;
	}

	public boolean remove(Object o) {
		return wrappedCollection.remove(o);
	}
	
	public boolean removeAll(Collection<?> c) {
		boolean changed = false;
		for (Object object : c) {
			changed |= remove(object);
		}
		return changed;
	}

	public void clear() {
		Collection<T> objectsToDelete = new LinkedList<T>();
        // Double looped to avoid concurrent modifications
		for (Iterator<T> i = iterator(); i.hasNext();) {
			objectsToDelete.add(i.next());
		}
		for (Iterator<T> i = objectsToDelete.iterator(); i.hasNext();) {
			remove(i.next());
		}
	}
	
	public boolean contains(Object o) {
		return wrappedCollection.contains(o);
	}

	public boolean containsAll(Collection<?> c) {
		return wrappedCollection.containsAll(c);
	}
	
	public boolean isEmpty() {
		return wrappedCollection.isEmpty();
	}
	
	public int size() {
		return wrappedCollection.size();
	}

	public Object[] toArray() {
		return wrappedCollection.toArray();
	}


	public <T> T[] toArray(T[] a) {
		return wrappedCollection.toArray(a);
	}

	public Iterator<T> iterator() {
		final Iterator<T> wrappedIterator = wrappedCollection.iterator();
		return new Iterator<T>() {

			public boolean hasNext() {
				return wrappedIterator.hasNext();
			}

			public T next() {
				return wrappedIterator.next();
			}

			public void remove() {
				throw new UnsupportedOperationException("Use Collection#remove or Collection#removeAll instead");
			}
		};
	}
	
	@Override
	public boolean equals(Object o) {
		return wrappedCollection.equals(o);
	}
	
	@Override
	public int hashCode() {
		return wrappedCollection.hashCode();
	}
	
	@Override
	public String toString() {
		return wrappedCollection.toString();
	}
	
}
