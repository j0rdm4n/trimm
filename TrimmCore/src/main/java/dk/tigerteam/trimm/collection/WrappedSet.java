/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.collection;

import java.util.HashSet;
import java.util.Set;

/**
 * Helper class which makes it easy to wrap a {@link Set} and apply extra functionality to it
 * @author Jeppe Cramon
 * @param <T> The type of objects in the Set
 */
public class WrappedSet<T> extends WrappedCollection<T> implements TSet<T> {
	private static final long serialVersionUID = 286168363591245668L;

	/**
	 * Default constructor for deferred wrapping. Call {@link #setWrappedSet(Set)} to supply the Set
	 */
	public WrappedSet() {}

	/**
	 * Default constructor for immediate wrapping
	 * @param wrappedSet The Set to wrap
	 */
	public WrappedSet(Set<T> wrappedSet) {
		super(wrappedSet);
	}

  /**
   * Create a new instance with a prewrapped HashSet
   * @param <T>
   * @return an initialized WrappedSet
   */
  public static <T> TSet<T> newInstance() {
    return new WrappedSet<T>(new HashSet<T>());
  }
	
	/**
	 * For subclass access to the wrapped Set<br/>
	 * Sugar method for calling {@link #getWrappedCollection()} and performing a cast to Set&lt;T>.
	 * @return The wrapped Set
	 */
	protected final Set<T> getWrappedSet() {
		return (Set<T>)getWrappedCollection();
	}
	
	/**
	 * For subclasses which needs to set the wrappedSet later than at construction time.<br/>
	 * Sugar method for calling {@link #setWrappedCollection(java.util.Collection)}.
	 * @param set The Set to wrap
	 */
	protected void setWrappedSet(Set<T> set) {
		setWrappedCollection(set);
	}
}
