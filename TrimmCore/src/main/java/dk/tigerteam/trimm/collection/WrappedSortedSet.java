/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.collection;

import java.util.Comparator;
import java.util.Set;
import java.util.SortedSet;

/**
 * Helper class which makes it easy to wrap a {@link Set} and apply extra functionality to it
 * @author Jeppe Cramon
 * @param <T> The type of objects in the Set
 */
public class WrappedSortedSet<T> extends WrappedCollection<T> implements TSortedSet<T> {
	private static final long serialVersionUID = -8198801741925162733L;

	/**
	 * Default constructor for deferred wrapping. Call {@link #setWrappedSet(SortedSet)} to supply the Set
	 */
	public WrappedSortedSet() {}

	/**
	 * Default constructor for immediate wrapping
	 * @param wrappedSet The Set to wrap
	 */
	public WrappedSortedSet(SortedSet<T> wrappedSet) {
		super(wrappedSet);
	}

	/**
	 * For subclass access to the wrapped Set<br/>
	 * Sugar method for calling {@link #getWrappedCollection()} and performing a cast to Set&lt;T>.
	 * @return The wrapped SortedSet
	 */
	protected final SortedSet<T> getWrappedSet() {
		return (SortedSet<T>)getWrappedCollection();
	}
	
	/**
	 * For subclasses which needs to set the wrappedSet later than at construction time.<br/>
	 * Sugar method for calling {@link #setWrappedCollection(java.util.Collection)}.
	 * @param set The SortedSet to wrap
	 */
	protected void setWrappedSet(SortedSet<T> set) {
		setWrappedCollection(set);
	}

	public Comparator<? super T> comparator() {
		return getWrappedSet().comparator();
	}

	public T first() {
		return getWrappedSet().first();
	}

	public SortedSet<T> headSet(T toElement) {
		return getWrappedSet().headSet(toElement);
	}

	public T last() {
		return getWrappedSet().last();
	}

	public SortedSet<T> subSet(T fromElement, T toElement) {
		return getWrappedSet().subSet(fromElement, toElement);
	}

	public SortedSet<T> tailSet(T fromElement) {
		return getWrappedSet().tailSet(fromElement);
	}
}
