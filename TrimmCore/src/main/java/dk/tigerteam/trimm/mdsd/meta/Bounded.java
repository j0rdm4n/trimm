/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

/**
 * Interface for all MetaTypes that allows to define Upper and Lower bounds for their value(s)
 * 
 * @see MetaParameter
 * @see MetaProperty
 * @author Jeppe Cramon
 */
public interface Bounded {
	/**
	 * The lower bound for the property (for Strings this is typically minimum length and for Numbers the smallest number allowed)
	 */
	Long getLowerBound();

	/**
	 * The lower bound for the property (for Strings this is typically minimum length and for Numbers the smallest number allowed)
	 */
	MetaType setLowerBound(Long lowerBound);

	/**
	 * The upper bound for the property (for Strings this is typically maximum length and for Numbers the largest number allowed)
	 */
	Long getUpperBound();

	/**
	 * The upper bound for the property (for Strings this is typically maximum length and for Numbers the largest number allowed)
	 */
	MetaType setUpperBound(Long upperBound);

}
