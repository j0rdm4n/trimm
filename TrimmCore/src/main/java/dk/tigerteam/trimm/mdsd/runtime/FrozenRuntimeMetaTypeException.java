/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

/**
 * Exception thrown if a {@link RuntimeMetaType} is tried changed (using set, add, etc.) after it has been frozen
 * using the {@link RuntimeMetaType#freeze()} 
 * @author Jeppe Cramon
 */
public class FrozenRuntimeMetaTypeException extends RuntimeException {
	
	private static final long serialVersionUID = -6405449597811657317L;
	
	private RuntimeMetaType metaType;

	/**
	 * @param metaType The {@link RuntimeMetaType} that was tried modified after it was frozen
	 */
	public FrozenRuntimeMetaTypeException(RuntimeMetaType metaType) {
		super();
		this.metaType = metaType;
	}

	/**
	 * The {@link RuntimeMetaType} that was tried modified after it was frozen
	 */
	public RuntimeMetaType getMetaType() {
		return metaType;
	}
}
