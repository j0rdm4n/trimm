/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi.ea;

import dk.tigerteam.trimm.mdsd.meta.*;
import dk.tigerteam.trimm.util.Utils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static dk.tigerteam.trimm.mdsd.uml2.xmi.ea.EAXmiWriterModel.xmiNs;

/**
 * Produces an EA 7.x-8.x XMI document from a MetaModel - Work in Progress (e.g. Enumerations, inheritance/specialization are unsupported at the moment)
 *
 * @author Jeppe Cramon
 */
public class EAXmiWriter {
    private EAMetaIdGenerator idGenerator = new EAMetaIdGenerator();

    public Document write(MetaModel metaModel, String importPackageName) {
        EAXmiWriterModel xmiWriterModel = new EAXmiWriterModel(metaModel);
        processModel(xmiWriterModel, importPackageName);
        return xmiWriterModel.getDocument();
    }

    private void processModel(EAXmiWriterModel xmiWriterModel, String importPackageName) {
        MetaPackage root = idGenerator.createWithId(MetaPackage.class);
        root.setName(importPackageName);
        while (xmiWriterModel.getMetaModel().getChildren().size() > 0) {
            root.add(xmiWriterModel.getMetaModel().getChildren().get(0));
        }
        xmiWriterModel.getMetaModel().add(root);
        processMetaTypeRecursively(root, xmiWriterModel.getUmlModel(), xmiWriterModel);
        processPrimitiveTypes(xmiWriterModel.getUmlModel(), xmiWriterModel);
        // Finish up and place umlModelAppliedStereoTypes into the right context
    }

    protected void processPrimitiveTypes(Element umlModel,
                                         EAXmiWriterModel xmiWriterModel) {
        SAXBuilder builder = new SAXBuilder();
        Document doc;
        try {
            doc = builder.build(this.getClass().getResourceAsStream("/ea_xmi_std_primitivetypes.xml"));
        } catch (JDOMException e) {
            throw new RuntimeException("Parse Exception while reading from classpath resource 'ea_xmi_std_primitivetypes.xml'", e);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't find or read from classpath resource 'ea_xmi_std_primitivetypes.xml'", e);
        }
        xmiWriterModel.getXmiExtensionPrimitiveTypes().addContent(
                doc.getRootElement().cloneContent()
        );

    }

    private void processMetaTypeRecursively(MetaType metaType, Element currentUmlModelParent, EAXmiWriterModel xmiWriterModel) {
        if (metaType instanceof MetaPackage) {
            processMetaPackage((MetaPackage) metaType, currentUmlModelParent, xmiWriterModel);
        } else if (metaType instanceof MetaClazz) {
            processMetaClazz((MetaClazz) metaType, currentUmlModelParent, xmiWriterModel);
        }
    }

    private void processMetaClazz(MetaClazz metaClazz, Element currentUmlModelParent, EAXmiWriterModel xmiWriterModel) {
        // UML model
        Element packagedElement = new Element("packagedElement")
                .setAttribute("type", "uml:" + metaClazzSubTypeToEaType(metaClazz), xmiNs)
                .setAttribute("id", metaClazz.getId(), xmiNs)
                .setAttribute("name", metaClazz.getName())
                .setAttribute("visibility", metaClazz.getVisibility().name().toLowerCase());
        if (metaClazz.isInterface() || metaClazz.isAbstract()) {
            packagedElement.setAttribute("isAbstract", "true");
        }
        currentUmlModelParent.addContent(packagedElement);

        // XMI extension model
        Element xmiExtensionElement = new Element("element")
                .setAttribute("type", "uml:" + metaClazzSubTypeToEaType(metaClazz), xmiNs)
                .setAttribute("idref", metaClazz.getId(), xmiNs)
                .setAttribute("name", metaClazz.getName())
                .setAttribute("scope", metaClazz.getVisibility().name().toLowerCase());
        xmiWriterModel.getXmiExtensionElements().addContent(xmiExtensionElement);


        xmiExtensionElement.addContent(Utils.newList(
                new Element("model")
                        .setAttribute("package", (metaClazz.getOwner() instanceof MetaPackage) ? metaClazz.getOwner().getId() : "")
                        .setAttribute("tpos", "0")
                        .setAttribute("ea_localid", xmiWriterModel.newLocalIdAsString())
                        .setAttribute("ea_eleType", "element"),
                new Element("properties")
                        .setAttribute("documentation", metaClazz.getDocumentation() != null ? metaClazz.getDocumentation() : "")
                        .setAttribute("isSpecification", "false")
                        .setAttribute("sType", metaClazzSubTypeToEaType(metaClazz))
                        .setAttribute("nType", "0")
                        .setAttribute("scope", metaClazz.getVisibility().name().toLowerCase())
                        .setAttribute("stereotype", resolveStereoTypesAsCommaDelimitedString(metaClazz))
                        .setAttribute("isRoot", "false")
                        .setAttribute("isLeaf", "false")
                        .setAttribute("isAbstract", ((metaClazz.isInterface() || metaClazz.isAbstract()) ? "true" : "false"))
                        .setAttribute("isActive", "false"),
                new Element("project")
                        .setAttribute("author", "TigerMDSDCore")
                        .setAttribute("version", "1.0")
                        .setAttribute("phase", "1.0")
                        .setAttribute("complexity", "1")
                        .setAttribute("status", "Proposed")
                        .setAttribute("created", xmiWriterModel.createTimstampAsString()),
                new Element("code")
                        .setAttribute("gentype", "Java"),
                new Element("style")
                        .setAttribute("appearance", "BackColor=-1;BorderColor=-1;BorderWidth=-1;FontColor=-1;VSwimLanes=1;HSwimLanes=1;BorderStyle=0;"),
                new Element("modelDocument"),
                new Element("tags")
                        .addContent(processXmiExtensionTags(metaClazz, xmiWriterModel)),
                new Element("xrefs"), // This should be safe to keep empty, EA will reconstruct it at import time
                new Element("extendedProperties")
                        .setAttribute("tagged", "0")
                        .setAttribute("package_name", (metaClazz.getOwner() instanceof MetaPackage) ? ((MetaPackage) metaClazz.getOwner()).getName() : "")
        ));

        // Attributes/Properties
        processMetaProperties(metaClazz, currentUmlModelParent, packagedElement, xmiExtensionElement, xmiWriterModel);
        processMetaOperations(metaClazz, currentUmlModelParent, packagedElement, xmiExtensionElement, xmiWriterModel);
    }

    private void processMetaOperations(MetaClazz metaClazz, Element currentUmlModelParent, Element packagedElement, Element xmiExtensionElement, EAXmiWriterModel xmiWriterModel) {
        Element operations = new Element("operations");
        xmiExtensionElement.addContent(operations);

        for (MetaOperation metaOperation : metaClazz.getOperations()) {
            // Owned operation
            Element ownedOperation = new Element("ownedOperation")
                    .setAttribute("id", metaOperation.getId(), xmiNs)
                    .setAttribute("name", metaOperation.getName())
                    .setAttribute("visibility", metaOperation.getVisibility().name().toLowerCase())
                    .setAttribute("concurrency", "sequential");
            packagedElement.addContent(ownedOperation);

            // XMI Extension element operation
            Element operation = new Element("operation")
                    .setAttribute("idref", metaOperation.getId(), xmiNs)
                    .setAttribute("name", metaOperation.getName())
                    .setAttribute("scope", metaOperation.getVisibility().name())
                    .addContent(Utils.newList(
                            new Element("properties")
                                    .setAttribute("position", "0"),
                            new Element("stereotype")
                                    .setAttribute("stereotype", resolveStereoTypesAsCommaDelimitedString(metaOperation)),
                            new Element("model")
                                    .setAttribute("ea_guid", idGenerator.getEaGuid(metaOperation.getId()))
                                    .setAttribute("ea_localid", xmiWriterModel.newLocalIdAsString()),
                            new Element("type")
                                    .setAttribute("type", metaOperation.getReturnParameter() != null ? metaOperation.getReturnParameter().getType().getName() : "void")
                                    .setAttribute("const", "false")
                                    .setAttribute("static", Boolean.toString(metaOperation.isStatic()))
                                    .setAttribute("stereotype", resolveStereoTypesAsCommaDelimitedString(metaOperation))
                                    .setAttribute("synchronised", "0")
                                    .setAttribute("concurrency", "Sequential")
                                    .setAttribute("returnarray", "0")
                                    .setAttribute("pure", "0")
                                    .setAttribute("isQuery", "false"),
                            new Element("behavior"),
                            new Element("code"), // TODO: Maybe later include CodeSnippets?
                            new Element("style")
                                    .setAttribute("value", metaOperation.getAlias() != null ? metaOperation.getAlias() : ""),
                            new Element("styleex"),
                            new Element("documentation")
                                    .setAttribute("value", metaOperation.getDocumentation() != null ? metaOperation.getDocumentation() : ""),
                            new Element("tags")
                                    .addContent(processXmiExtensionTags(metaOperation, xmiWriterModel))
                    ));
            operations.addContent(operation);
            Element parameters = new Element("parameters");
            operation.addContent(parameters);

            int parametersPosition = 0;
            for (MetaParameter metaParameter : metaOperation.getAllParameters()) {
                // XMI Extension parameter
                int parameterPositionToUse = parametersPosition;
                if (metaParameter.getDirection() == MetaParameter.Direction.Return) {
                    if (metaParameter.getType().getName().equals("void")) {
                        continue;
                    }
                    parameterPositionToUse = 0;
                } else {
                    parametersPosition++;
                }

                final Element properties = new Element("properties")
                        .setAttribute("pos", Integer.toString(parameterPositionToUse))
                        .setAttribute("type", metaParameter.getType().getName())
                        .setAttribute("const", "false")
                        .setAttribute("ea_guid", idGenerator.getEaGuid(metaParameter.getId()));
                if (!metaParameter.getType().isBuiltInType()) {
                    properties.setAttribute("classifier", metaParameter.getType().getId());
                }

                parameters.addContent(
                        new Element("parameter")
                                .setAttribute("idref", metaParameter.getId(), xmiNs)
                                .setAttribute("visibility", metaParameter.getVisibility().name().toLowerCase())
                                .addContent(Utils.newList(
                                        properties,
                                        new Element("style"),
                                        new Element("styleex"),
                                        new Element("documentation")
                                                .setAttribute("value", metaParameter.getDocumentation() != null ? metaParameter.getDocumentation() : ""),
                                        new Element("tags")
                                                .addContent(processXmiExtensionTags(metaOperation, xmiWriterModel)),
                                        new Element("xrefs")
                                ))

                );

                // Owned parameter
                ownedOperation.addContent(
                        new Element("ownedParameter")
                                .setAttribute("id", metaParameter.getId(), xmiNs)
                                .setAttribute("name", metaParameter.getDirection() == MetaParameter.Direction.Return ? "return" : metaParameter.getName())
                                .setAttribute("type", metaParameter.getType().isBuiltInType() ? resolveBuiltInTypeForPropertyOrParameter(metaParameter.getType()) : metaParameter.getType().getId())
                                .setAttribute("direction", metaParameter.getDirection().name().toLowerCase())
                );
            }

        }
    }

    private void processMetaProperties(MetaClazz metaClazz, Element currentUmlModelParent, Element umlPackagedElement, Element xmiExtensionElement, EAXmiWriterModel xmiWriterModel) {
        Element attributesElement = new Element("attributes");
        xmiExtensionElement.addContent(attributesElement);
        List<Element> links = new LinkedList<Element>();

        int attributePosition = 0;
        for (MetaProperty metaProperty : metaClazz.getProperties()) {

            if (metaProperty.isPartInAnAssociation()) {
                final MetaProperty oppositeProperty = metaProperty.getAssociation().getOppositeProperty(metaProperty);
                //if (metaProperty.getAssociation().getOwnerProperty() != metaProperty) {
                if (!metaProperty.isOwnerOfAssociation()) {
                    Element association = new Element("packagedElement")
                            .setAttribute("type", "uml:Association", xmiNs)
                            .setAttribute("id", metaProperty.getAssociation().getId(), xmiNs)
                            .setAttribute("name", metaProperty.getAssociation().getName() != null ? metaProperty.getAssociation().getName() : "")
                            .addContent(Utils.newList(
                                    new Element("memberEnd")
                                            .setAttribute("idref", metaProperty.getId(), xmiNs),
                                    new Element("memberEnd")
                                            .setAttribute("idref", oppositeProperty.getId(), xmiNs),
                                    new Element("ownedEnd")
                                            .setAttribute("type", "uml:Property", xmiNs)
                                            .setAttribute("id", metaProperty.getId(), xmiNs)
                                            .setAttribute("name", metaProperty.getName() != null ? metaProperty.getName() : "")
                                            .setAttribute("association", metaProperty.getAssociation().getId(), xmiNs)
                                            .setAttribute("aggregation", metaProperty.getAggregation().name().toLowerCase())
                                            .addContent(Utils.newList(
                                                    new Element("type")
                                                            .setAttribute("idref", metaProperty.getType().getId(), xmiNs),
                                                    new Element("lowerValue")
                                                            .setAttribute("type", "uml:LiteralInteger", xmiNs)
                                                            .setAttribute("id", idGenerator.createId(MetaParameter.class), xmiNs)
                                                            .setAttribute("value", Integer.toString(metaProperty.getMinCardinality())),
                                                    new Element("upperValue")
                                                            .setAttribute("type", metaProperty.getMaxCardinality() == MetaProperty.CARDINALITY_UNLIMITED ? "uml:LiteralUnlimitedNatural" : "uml:LiteralInteger", xmiNs)
                                                            .setAttribute("id", idGenerator.createId(MetaParameter.class), xmiNs)
                                                            .setAttribute("value", Integer.toString(metaProperty.getMaxCardinality()))
                                            ))
                            ));
                    currentUmlModelParent.addContent(association);

                    // Xmi Extensions element links <Aggregation>
                    links.add(
                            new Element("Aggregation")
                                    .setAttribute("id", metaProperty.getAssociation().getId(), xmiNs)
                                    .setAttribute("start", metaProperty.getType().getId())
                                    .setAttribute("end", oppositeProperty.getType().getId())
                    );


                    // Create the connector for this association
                    Element connector = new Element("connector")
                            .setAttribute("idref", metaProperty.getAssociation().getId(), xmiNs)
                            .addContent(Utils.newList(
                                    new Element("source")
                                            .setAttribute("idref", metaProperty.getType().getId(), xmiNs)
                                            .addContent(Utils.newList(
                                                    new Element("model")
                                                            .setAttribute("ea_localid", xmiWriterModel.newLocalIdAsString())
                                                            .setAttribute("type", metaClazzSubTypeToEaType(metaProperty.getType()))
                                                            .setAttribute("name", metaProperty.getType().getName()),
                                                    new Element("role")
                                                            .setAttribute("name", metaProperty.getName() != null ? metaProperty.getName() : "")
                                                            .setAttribute("visibility", metaProperty.getVisibility().name())
                                                            .setAttribute("targetScope", "instance"),
                                                    new Element("type")
                                                            .setAttribute("multiplicity", metaProperty.getCardinalityType().umlNotation())
                                                            .setAttribute("aggregation", oppositeProperty.getAggregation().name().toLowerCase()) // NOTE: EA shows the aggregation as it is shown on the diagram,
                                                                    // NOT as it's interpreted in the Metamodel
                                                            .setAttribute("containment", "Unspecified"),

                                                    new Element("style")
                                                            .setAttribute("value", "Union=0;Derived=0;AllowDuplicates=0;Owned=0;Navigable=Non-Navigable;"), // Used to be Navigable

                                                    new Element("constraints"),
                                                    new Element("modifiers")
                                                            .setAttribute("isOrdered", "false")
                                                            .setAttribute("changeable", "none")
                                                            .setAttribute("isNavigable", "false"), // Used to be true

                                                    new Element("documentation")
                                                            .setAttribute("value", metaProperty.getDocumentation() != null ? metaProperty.getDocumentation() : ""),
                                                    new Element("xrefs"),
                                                    new Element("tags")
                                                            .addContent(processXmiExtensionTags(metaProperty, xmiWriterModel))
                                            )),
                                    new Element("target")
                                            .setAttribute("idref", oppositeProperty.getType().getId(), xmiNs)
                                            .addContent(Utils.newList(
                                                    new Element("model")
                                                            .setAttribute("ea_localid", xmiWriterModel.newLocalIdAsString())
                                                            .setAttribute("type", metaClazzSubTypeToEaType(oppositeProperty.getType()))
                                                            .setAttribute("name", oppositeProperty.getType().getName()),
                                                    new Element("role")
                                                            .setAttribute("name", oppositeProperty.getName() != null ? oppositeProperty.getName() : "")
                                                            .setAttribute("visibility", oppositeProperty.getVisibility().name())
                                                            .setAttribute("targetScope", "instance"),
                                                    new Element("type")
                                                            .setAttribute("multiplicity", oppositeProperty.getCardinalityType().umlNotation())
                                                            .setAttribute("aggregation", metaProperty.getAggregation().name().toLowerCase()) // NOTE: EA shows the aggregation as it is shown on the diagram,
                                                                    // NOT as it's interpreted in the Metamodel
                                                            .setAttribute("containment", "Unspecified"),
                                                    new Element("style")
                                                            //.setAttribute("value", "Union=0;Derived=0;AllowDuplicates=0;Owned=0;Navigable=" + ((metaProperty.getAssociation().isBidirectional()) ? "Navigable;" : "Non-Navigable;")),
                                                            .setAttribute("value", "Union=0;Derived=0;AllowDuplicates=0;Owned=0;Navigable=Navigable;"),

                                                    new Element("constraints"),
                                                    new Element("modifiers")
                                                            .setAttribute("isOrdered", "false")
                                                            .setAttribute("changeable", "none")
                                                                    //.setAttribute("isNavigable", Boolean.toString(metaProperty.getAssociation().isBidirectional())),
                                                            .setAttribute("isNavigable", "true"),

                                                    new Element("documentation")
                                                            .setAttribute("value", oppositeProperty.getDocumentation() != null ? oppositeProperty.getDocumentation() : ""),
                                                    new Element("xrefs"),
                                                    new Element("tags")
                                                            .addContent(processXmiExtensionTags(metaProperty, xmiWriterModel))
                                            )),
                                    new Element("model")
                                            .setAttribute("ea_localid", xmiWriterModel.newLocalIdAsString()),
                                    new Element("properties")
                                            .setAttribute("ea_type", "Aggregation")
                                            .setAttribute("subtype", "Strong")
                                            .setAttribute("direction", "Source -> Destination"),
                                    new Element("modifiers")
                                            .setAttribute("isRoot", "false")
                                            .setAttribute("isLeaf", "false"),
                                    new Element("documentation")
                                            .setAttribute("value", metaProperty.getAssociation().getDocumentation() != null ? metaProperty.getAssociation().getDocumentation() : ""),
                                    new Element("appearance")
                                            .setAttribute("linemode", "3")
                                            .setAttribute("linecolor", "-1")
                                            .setAttribute("seqno", "0")
                                            .setAttribute("headStyle", "0")
                                            .setAttribute("lineStyle", "0"),
                                    new Element("labels")
                                            .setAttribute("lb", metaProperty.getCardinalityType().umlNotation())
                                            .setAttribute("lt", "+" + metaProperty.getName())
                                            .setAttribute("mt", metaProperty.getAssociation().getName() != null ? metaProperty.getAssociation().getName() : "")
                                            .setAttribute("rb", oppositeProperty.getCardinalityType().umlNotation())
                                            .setAttribute("rt", oppositeProperty.getName() != null ? ("+" + oppositeProperty.getName()) : ""),
                                    new Element("extendedProperties")
                                            .setAttribute("virtualInheritance", "0"),
                                    new Element("style"),
                                    new Element("xrefs"),
                                    new Element("tags")
                                            .addContent(processXmiExtensionTags(metaProperty.getAssociation(), xmiWriterModel))
                            ));
                    xmiWriterModel.getXmiExtensionConnectors().addContent(connector);

                } else {
                    // Owned attribute
                    Element ownedAttribute = new Element("ownedAttribute")
                            .setAttribute("type", "uml:Property", xmiNs)
                            .setAttribute("id", metaProperty.getId(), xmiNs)
                            .setAttribute("name", metaProperty.getName() != null ? metaProperty.getName() : "")
                            .setAttribute("association", metaProperty.getAssociation().getId(), xmiNs)
                            .setAttribute("visibility", metaProperty.getVisibility().name().toLowerCase())
                            .setAttribute("aggregation", metaProperty.getAggregation().name().toLowerCase())
                            .addContent(Utils.newList(
                                    new Element("lowerValue")
                                            .setAttribute("type", "uml:LiteralInteger", xmiNs)
                                            .setAttribute("id", idGenerator.createId(MetaParameter.class), xmiNs)
                                            .setAttribute("value", Integer.toString(metaProperty.getMinCardinality())),
                                    new Element("upperValue")
                                            .setAttribute("type", metaProperty.getMaxCardinality() == MetaProperty.CARDINALITY_UNLIMITED ? "uml:LiteralUnlimitedNatural" : "uml:LiteralInteger", xmiNs)
                                            .setAttribute("id", idGenerator.createId(MetaParameter.class), xmiNs)
                                            .setAttribute("value", Integer.toString(metaProperty.getMaxCardinality())),
                                    new Element("type")
                                            .setAttribute("idref", metaProperty.getType().getId(), xmiNs)
                            ));
                    umlPackagedElement.addContent(ownedAttribute);

                    links.add(
                            new Element("Aggregation")
                                    .setAttribute("id", metaProperty.getAssociation().getId(), xmiNs)
                                    .setAttribute("start", oppositeProperty.getType().getId())
                                    .setAttribute("end", metaProperty.getType().getId())
                    );
                }
            } else {
                // Owned attribute
                Element ownedAttribute = new Element("ownedAttribute")
                        .setAttribute("type", "uml:Property", xmiNs)
                        .setAttribute("id", metaProperty.getId(), xmiNs)
                        .setAttribute("name", metaProperty.getName())
                        .setAttribute("visibility", metaProperty.getVisibility().name().toLowerCase())
                        .setAttribute("isDerived", "false")
                        .setAttribute("isOrdered", "false")
                        .setAttribute("isUnique", "true")
                        .addContent(Utils.newList(
                                new Element("lowerValue")
                                        .setAttribute("type", "uml:LiteralInteger", xmiNs)
                                        .setAttribute("id", idGenerator.createId(MetaParameter.class), xmiNs)
                                        .setAttribute("value", Integer.toString(metaProperty.getMinCardinality())),
                                new Element("upperValue")
                                        .setAttribute("type", metaProperty.getMaxCardinality() == MetaProperty.CARDINALITY_UNLIMITED ? "uml:LiteralUnlimitedNatural" : "uml:LiteralInteger", xmiNs)
                                        .setAttribute("id", idGenerator.createId(MetaParameter.class), xmiNs)
                                        .setAttribute("value", Integer.toString(metaProperty.getMaxCardinality())),
                                new Element("type")
                                        .setAttribute("idref", metaProperty.getType().isBuiltInType() ? resolveBuiltInTypeForPropertyOrParameter(metaProperty.getType()) : metaProperty.getType().getId(), xmiNs)
                        ));
                umlPackagedElement.addContent(ownedAttribute);

                // Attribute (XMI Extension element child)
                attributesElement.addContent(
                        new Element("attribute")
                                .setAttribute("idref", metaProperty.getId(), xmiNs)
                                .setAttribute("name", metaProperty.getName())
                                .setAttribute("scope", metaProperty.getVisibility().name())
                                .addContent(Utils.newList(
                                        new Element("initial"),
                                        new Element("documentation")
                                                .setAttribute("value", metaProperty.getDocumentation() != null ? metaProperty.getDocumentation() : ""),
                                        new Element("model")
                                                .setAttribute("ea_localid", xmiWriterModel.newLocalIdAsString())
                                                .setAttribute("ea_guid", idGenerator.getEaGuid(metaProperty.getId())),
                                        new Element("properties")
                                                .setAttribute("type", metaProperty.getType().getName())
                                                .setAttribute("derived", "0")
                                                .setAttribute("collection", "false") // TODO: Should this be dependent on max cardinality??
                                                .setAttribute("duplicates", "0")
                                                .setAttribute("changeability", "changeable"),
                                        new Element("coords")
                                                .setAttribute("ordered", "0"),
                                        new Element("containment")
                                                .setAttribute("containment", "Not Specified")
                                                .setAttribute("position", Integer.toString(attributePosition)),
                                        new Element("stereotype")
                                                .setAttribute("stereotype", resolveStereoTypesAsCommaDelimitedString(metaProperty)),
                                        new Element("bounds")
                                                .setAttribute("lower", Integer.toString(metaProperty.getMinCardinality()))
                                                .setAttribute("upper", Integer.toString(metaProperty.getMaxCardinality())),
                                        new Element("options"),
                                        new Element("style"),
                                        new Element("styleex")
                                                .setAttribute("value", "volatile=0;IsLiteral=0;"),
                                        new Element("tags")
                                                .addContent(processXmiExtensionTags(metaProperty, xmiWriterModel)),
                                        new Element("xrefs")
                                ))
                );
                attributePosition++;
            }
        }

        if (links.size() > 0) {
            xmiExtensionElement.addContent(
                    new Element("links")
                            .addContent(links)
            );
        }
    }

    private String metaClazzSubTypeToEaType(MetaClazz type) {
        // TODO: Er der andre muligheder
        if (type.isInterface()) {
            return "Interface";
        } else if (type.isEnumerationClazz()) {
            return "Enumeration"; // TODO: Correct?
        } else {
            return "Class";
        }
    }

    /**
     * Return &lt;tag xmi:id="EAID_B18C9E01_428B_6b42_92F5_A82AAD95C640" name="MaxSize" value="1"/> elements for this MetaType
     *
     * @param metaType       metatype
     * @param xmiWriterModel the xmi writer model
     */
    protected List<Element> processXmiExtensionTags(MetaType metaType, EAXmiWriterModel xmiWriterModel) {
        return new LinkedList<Element>();
    }

    protected String resolveBuiltInTypeForPropertyOrParameter(MetaClazz metaClazz) {
        return "EAJava_" + metaClazz.getName();
    }

    protected String resolveStereoTypesAsCommaDelimitedString(MetaType metaType) {
        return "";
    }

    private void processMetaPackage(MetaPackage metaPackage, Element currentUmlModelParent, EAXmiWriterModel xmiWriterModel) {
        // UML model
        Element packagedElement = new Element("packagedElement")
                .setAttribute("type", "uml:Package", xmiNs)
                .setAttribute("id", metaPackage.getId(), xmiNs)
                .setAttribute("name", metaPackage.getName())
                .setAttribute("visibility", metaPackage.getVisibility().name().toLowerCase());
        currentUmlModelParent.addContent(packagedElement);

        // XMI extension model
        Element element = new Element("element")
                .setAttribute("type", "uml:Package", xmiNs)
                .setAttribute("idref", metaPackage.getId(), xmiNs)
                .setAttribute("name", metaPackage.getName())
                .setAttribute("scope", metaPackage.getVisibility().name().toLowerCase());
        xmiWriterModel.getXmiExtensionElements().addContent(element);


        element.addContent(Utils.newList(
                new Element("model")
                        .setAttribute("package2", metaPackage.getId())
                        .setAttribute("package", (metaPackage.getOwner() instanceof MetaPackage) ? metaPackage.getOwner().getId() : "")
                        .setAttribute("tpos", "0")
                        .setAttribute("ea_localid", xmiWriterModel.newLocalIdAsString())
                        .setAttribute("ea_eleType", "package"),
                new Element("properties")
                        .setAttribute("documentation", metaPackage.getDocumentation() != null ? metaPackage.getDocumentation() : "")
                        .setAttribute("isSpecification", "false")
                        .setAttribute("sType", "Package")
                        .setAttribute("nType", "0")
                        .setAttribute("scope", metaPackage.getVisibility().name().toLowerCase())
                        .setAttribute("stereotype", resolveStereoTypesAsCommaDelimitedString(metaPackage)),
                new Element("project")
                        .setAttribute("author", "TigerMDSDCore")
                        .setAttribute("version", "1.0")
                        .setAttribute("phase", "1.0")
                        .setAttribute("complexity", "1")
                        .setAttribute("status", "Proposed")
                        .setAttribute("created", xmiWriterModel.createTimstampAsString()),
                new Element("code")
                        .setAttribute("gentype", "Java"),
                new Element("style")
                        .setAttribute("appearance", "BackColor=-1;BorderColor=-1;BorderWidth=-1;FontColor=-1;VSwimLanes=1;HSwimLanes=1;BorderStyle=0;"),
                new Element("modelDocument"),
                new Element("tags")
                        .addContent(processXmiExtensionTags(metaPackage, xmiWriterModel)),
                new Element("xrefs"), // This should be safe to keep empty, EA will reconstruct it at import time
                new Element("extendedProperties")
                        .setAttribute("tagged", "0")
                        .setAttribute("package_name", metaPackage.getName()),
                new Element("packageProperties")
                        .setAttribute("version", "1.0"),
                new Element("paths"),
                new Element("times")
                        .setAttribute("created", xmiWriterModel.createTimstampAsString())
                        .setAttribute("modified", xmiWriterModel.createTimstampAsString()),
                new Element("flags")
                        .setAttribute("iscontrolled", "FALSE")
                        .setAttribute("isprotected", "FALSE")
                        .setAttribute("usedtd", "FALSE")
                        .setAttribute("logxml", "FALSE")
        ));

        // Diagram for the package
//        Element diagram = new Element("diagram")
//                            .setAttribute("id", idGenerator.createId(MetaClazz.class), xmiNs)
//                            .addContent(Utils.newList(
//                                new Element("model")
//                                    .setAttribute("package", metaPackage.getId())
//                                    .setAttribute("owner", metaPackage.getId())
//                                    .setAttribute("localId", xmiWriterModel.newLocalIdAsString()),
//                                new Element("properties")
//                                    .setAttribute("name", metaPackage.getName())
//                                    .setAttribute("type", "Logical"),
//                                new Element("project")
//                                    .setAttribute("author", "TigerMDSD")
//                                    .setAttribute("version", "1.0")
//                                    .setAttribute("created", xmiWriterModel.createTimstampAsString())
//                                    .setAttribute("modified", xmiWriterModel.createTimstampAsString()),
//                                new Element("style1")
//                                    .setAttribute("value", "ShowPrivate=1;ShowProtected=1;ShowPublic=1;HideRelationships=0;Locked=0;Border=1;HighlightForeign=1;PackageContents=1;SequenceNotes=0;ScalePrintImage=0;PPgs.cx=0;PPgs.cy=0;DocSize.cx=815;DocSize.cy=1067;ShowDetails=0;Orientation=P;Zoom=100;ShowTags=0;OpParams=1;VisibleAttributeDetail=0;ShowOpRetType=1;ShowIcons=1;CollabNums=0;HideProps=0;ShowReqs=0;ShowCons=0;PaperSize=1;HideParents=0;UseAlias=0;HideAtts=0;HideOps=0;HideStereo=0;HideElemStereo=0;ShowTests=0;ShowMaint=0;ConnectorNotation=UML 2.1;ExplicitNavigability=0;AdvancedElementProps=1;AdvancedFeatureProps=1;AdvancedConnectorProps=1;ShowNotes=0;SuppressBrackets=0;SuppConnectorLabels=0;PrintPageHeadFoot=0;ShowAsList=0;"),
//                                new Element("style2"),
//                                new Element("swimlanes")
//                                    .setAttribute("value", "locked=false;orientation=0;width=0;inbar=false;names=false;color=0;bold=false;fcol=0;;cls=0;"),
//                                new Element("matrixitems")
//                                    .setAttribute("value", "locked=false;matrixactive=false;swimlanesactive=true;width=1;"),
//                                new Element("extendedProperties")
//
//                            ));
//        xmiWriterModel.getXmiExtensionDiagrams().addContent(diagram);


        for (MetaType metaType : metaPackage.getChildren()) {
            processMetaTypeRecursively(metaType, packagedElement, xmiWriterModel);
        }

    }
}
