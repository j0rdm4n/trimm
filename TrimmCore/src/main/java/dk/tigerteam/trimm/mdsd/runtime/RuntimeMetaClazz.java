/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

import dk.tigerteam.trimm.bean.bidirectional.ManyToManySetWrapper;
import dk.tigerteam.trimm.bean.bidirectional.ManyToOneWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManySetWrapper;

import java.util.*;

/**
 * Metadata about a Class. Associations to {@link #subClazzes} and {@link #superClazz} is maintained bidirectionally for you :)
 * @author Jeppe Cramon
 */
public class RuntimeMetaClazz extends RuntimeNameableMetaType {
	private static final long serialVersionUID = 3001586979524617221L;
	private RuntimeMetaClazz superClazz;
	private Set<RuntimeMetaClazz> subClazzes = new HashSet<RuntimeMetaClazz>();
	/**
	 * A set of all properties which are of this type
	 */
	private Set<RuntimeMetaProperty> propertiesOfThisType = new HashSet<RuntimeMetaProperty>();
	private boolean builtInType = false;
	private boolean isExtensionClazz = false;
	private boolean isBaseClazz = false;
	private Class<?> _class;
	/**
	 * What RuntimeMetaInterfaces does this class realize.<br/>
	 * The inverse side of this is {@link RuntimeMetaInterface#getRealizedBy()} 
	 */
	private Set<RuntimeMetaInterface> realizations = new HashSet<RuntimeMetaInterface>();
	
	/**
	 * Create a {@link RuntimeMetaClazz} by supplying its unique id and a fabrication information
	 * @param id The unique id
	 * @param fabricated Is this MetaType a result of a fabrication or defined in the original model
	 * @param _class The java class this MetaClazz represents
	 */
	public RuntimeMetaClazz(String id, boolean fabricated, Class<?> _class) {
		super(id, fabricated);
		this._class = _class;
		setName(_class.getName());
	}

	public RuntimeMetaClazz(String id, boolean fabricated, String name) {
		super(id, fabricated, name);
	}
	
	/**
	 * Is this clazz functioning as Extension Class for another class (aka. the base Class)
	 */
	public boolean isExtensionClazz() {
		return isExtensionClazz;
	}

	/**
	 * Is this clazz functioning as Extension Class for another class (aka. the base Class)
	 */
	public RuntimeMetaClazz setExtensionClazz(boolean isExtensionClazz) {
		checkCanModify();
		this.isExtensionClazz = isExtensionClazz;
		return this;
	}

	/**
	 * Is this clazz functioning as Base Class for another class (aka. the Extension Class)
	 */
	public boolean isBaseClazz() {
		return isBaseClazz;
	}

	/**
	 * Is this clazz functioning as Base Class for another class (aka. the Extension Class)
	 */
	public RuntimeMetaClazz setBaseClazz(boolean isBaseClazz) {
		checkCanModify();
		this.isBaseClazz = isBaseClazz;
		return this;
	}
	
	/**
	 * What RuntimeMetaInterfaces does this class realize.<br/>
	 * The inverse side of this is {@link RuntimeMetaInterface#getRealizedBy()} 
	 */
	@SuppressWarnings("serial")
	public Set<RuntimeMetaInterface> getRealizations() {
		return new ManyToManySetWrapper<RuntimeMetaClazz, RuntimeMetaInterface>(this, realizations) {
			@Override
			protected Collection<RuntimeMetaClazz> getSourceCollectionInTarget(
					RuntimeMetaInterface o) {
				return o.getRealizedBy();
			}
		};
	}

	/**
	 * Does this Clazz represent an Interface (sugar for checking against the sub type of MetaClazz being used)
	 */
	public boolean isInterface() {
		return false;
	}
	
	/**
	 * Does this Clazz represent an Association Clazz  (sugar for checking against the sub type of MetaClazz being used)
	 */
	public boolean isAssociationClazz() {
		return false;
	}
	
	/**
	 * Does this Clazz represent an Enumeration Clazz  (sugar for checking against the sub type of MetaClazz being used)
	 */
	public boolean isEnumerationClazz() {
		return false;
	}


	/**
	 * The java class this MetaClazz represents
	 */
	@SuppressWarnings("rawtypes")
	public Class getJavaClass() {
		if (_class == null) {
			try {
				_class = Class.forName(getName());
			} catch (ClassNotFoundException e) {
				throw new IllegalStateException("Couldn't resolve Java Class for meta clazz '" + getName() + "'", e);
			}
		}
		return _class;
	}

	public boolean isBuiltInType() {
		return builtInType;
	}
	
	public RuntimeMetaClazz setBuiltInType(boolean builtInType) {
		checkCanModify();
		this.builtInType = builtInType;
		return this;
	}

	/**
	 * This method will return all subClasses of this classes and their subclasses recursively
	 */
	public Set<RuntimeMetaClazz> getAllSubClazzes() {
		Set<RuntimeMetaClazz> allSubClazzes = new LinkedHashSet<RuntimeMetaClazz>();
		for (RuntimeMetaClazz runtimeMetaClazz : getSubClazzes()) {
			allSubClazzes.add(runtimeMetaClazz);
			allSubClazzes.addAll(runtimeMetaClazz.getAllSubClazzes());
		}
		return allSubClazzes;
	}
		
	public Set<RuntimeMetaClazz> getSubClazzes() {
		return new OneToManySetWrapper<RuntimeMetaClazz, RuntimeMetaClazz>(this, subClazzes) {
			private static final long serialVersionUID = 7801407073213296270L;

			@Override
			protected RuntimeMetaClazz getOneSideObjectInManySideObject(
					RuntimeMetaClazz manySideObject) {
				return manySideObject.getSuperClazz();
			}

			@Override
			protected void setOneSideObjectInManySideObject(
					RuntimeMetaClazz manySideObject, RuntimeMetaClazz oneSideObject) {
				manySideObject.checkCanModify();
				manySideObject.setSuperClazz(oneSideObject);
			}
		};
	}
	
	
	/**
	 * Get all properties of this RuntimeMetaClazz and then its super RuntimeMetaClazz, and its super clazz'
	 * super clazz, etc. they are ordered from more specific class (this class) to the least 
	 * specific (the top most super class)
	 * @return list of all properties
	 */
	public List<RuntimeMetaProperty> getAllPropertiesIncludingSuperClazzProperties() {
		List<RuntimeMetaProperty> properties = new LinkedList<RuntimeMetaProperty>();
		RuntimeMetaClazz currentClazz = this;
		while (currentClazz != null) {
			properties.addAll(currentClazz.getProperties());
			currentClazz = currentClazz.getSuperClazz() != null ? currentClazz.getSuperClazz() : null;
		}
		return properties;
	}
	
	/**
	 * Sugar method that returns all {@link RuntimeMetaProperty}'s in {@link #getChildren()} set<br/>
	 * Note: Unmodifiable
	 */
	public Set<RuntimeMetaProperty> getProperties() {
		return Collections.unmodifiableSet(findImmediateChildrenOfType(RuntimeMetaProperty.class));
	}
	
	/**
	 * Sugar method that returns all {@link RuntimeMetaOperation}'s in {@link #getChildren()} set<br/>
	 * Note: Unmodifiable
	 */
	public Set<RuntimeMetaOperation> getOperations() {
		return Collections.unmodifiableSet(findImmediateChildrenOfType(RuntimeMetaOperation.class));
	}

	public RuntimeMetaClazz getSuperClazz() {
		return superClazz;
	}
	
	public RuntimeMetaClazz setSuperClazz(RuntimeMetaClazz superClazz) {
		new ManyToOneWrapper<RuntimeMetaClazz, RuntimeMetaClazz>(this) {
			private static final long serialVersionUID = 26992835872857531L;

			@SuppressWarnings("unchecked")
			@Override
			protected void addManySideObjectToOneSideCollection(
					RuntimeMetaClazz oneSide, RuntimeMetaClazz manySide) {
				oneSide.checkCanModify();
				 ((OneToManySetWrapper<RuntimeMetaClazz, RuntimeMetaClazz>)oneSide.getSubClazzes()).getWrappedCollection().add(manySide);
			} 

			@Override
			protected RuntimeMetaClazz getOneSideObjectInManySideObject(RuntimeMetaClazz manySide) {
				return manySide.superClazz;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void removeManySideObjectFromOneSideCollection(
					RuntimeMetaClazz oneSide, RuntimeMetaClazz manySide) {
				oneSide.checkCanModify();
				 ((OneToManySetWrapper<RuntimeMetaClazz, RuntimeMetaClazz>)oneSide.getSubClazzes()).getWrappedCollection().remove(manySide);
			}

			@Override
			protected void setOneSideObjectInManySideObject(RuntimeMetaClazz manySide,
					RuntimeMetaClazz oneSide) {
				manySide.checkCanModify();
				manySide.superClazz = oneSide;
			}
		}.updateOneSideObject(superClazz);
		return this;
	}
	
	/**
	 * A set of all properties which are of this type
	 */
	public Set<RuntimeMetaProperty> getPropertiesOfThisType() {
		return new OneToManySetWrapper<RuntimeMetaClazz, RuntimeMetaProperty>(this, propertiesOfThisType) {
			private static final long serialVersionUID = 7729899197663960058L;

			@Override
			protected RuntimeMetaClazz getOneSideObjectInManySideObject(
					RuntimeMetaProperty manySideObject) {
				return manySideObject.getType();
			}

			@Override
			protected void setOneSideObjectInManySideObject(
					RuntimeMetaProperty manySideObject, RuntimeMetaClazz oneSideObject) {
				manySideObject.checkCanModify();
				manySideObject.setType(oneSideObject);
			}
		};
	}

	public RuntimeMetaPackage getPackage() {
		return findOwnerOfTypeOrNull(RuntimeMetaPackage.class);
	}

	/**
	 * Get the Fully Qualified Class Name (Java style)
	 */
	public String getFQCN() {
		return getName();
	}
	
	@Override
	public String toString() {
		return getName() + "#" + getId();
	}

    public RuntimeMetaProperty getPropertyByName(String propertyName) {
        for (RuntimeMetaProperty metaProperty : getProperties()) {
            if (propertyName.equals(metaProperty.getName())) {
                return metaProperty;
            }
        }
        throw new IllegalArgumentException("Couldn't find RuntimeMetaProperty with name '" + propertyName + "' in RuntimeMetaClazz " + this.getFQCN());
    }
}
