/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi.ea;

import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Encapsulates all relevant data holders for the XMI document
 *
 * @author Jeppe Cramon
 * @see EAXmiWriter
 */
public class EAXmiWriterModel {
    private Element xmi;
    private Element umlModel;
    private Element xmiExtension;
    private Element xmiExtensionElements;
    private Element xmiExtensionConnectors;
    private Element xmiExtensionPrimitiveTypes;
    private Element xmiExtensionProfiles;
    private Element xmiExtensionDiagrams;
    // Is added to the umlModel element at the very end when all packagedElements have been written to the doc
    private List<Element> umlModelAppliedStereoTypes = new LinkedList<Element>();
    private Document document;
    private MetaModel metaModel;
    private int nextEaLocalId = 0;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static Namespace xmiNs = Namespace.getNamespace("xmi", "http://schema.omg.org/spec/XMI/2.1");
    public static Namespace umlNs = Namespace.getNamespace("uml", "http://schema.omg.org/spec/UML/2.1");


    public EAXmiWriterModel(MetaModel metaModel) {
        this.metaModel = metaModel;
        // <xmi:Documentation exporter="Enterprise Architect" exporterVersion="6.5"/>
        xmi = new Element("XMI", xmiNs)
                .setAttribute("version", "2.1", xmiNs)
                .addContent(
                        new Element("Documentation", xmiNs)
                                .setAttribute("exporter", "Enterprise Architect")
                                .setAttribute("exporterVersion", "6.5")
                );

        umlModel = new Element("Model", umlNs);
        umlModel.setAttribute("type", "uml:Model", xmiNs)
                .setAttribute("name", "EA_Model")
                .setAttribute("visibility", "public");
        xmi.addContent(umlModel);

        xmiExtension = new Element("Extension", xmiNs)
                .setAttribute("extender", "Enterprise Architect").setAttribute("extenderID", "6.5");
        xmi.addContent(xmiExtension);

        xmiExtensionElements = new Element("elements");
        xmiExtension.addContent(xmiExtensionElements);

        xmiExtensionConnectors = new Element("connectors");
        xmiExtension.addContent(xmiExtensionConnectors);

        xmiExtensionPrimitiveTypes = new Element("primitivetypes");
        xmiExtension.addContent(xmiExtensionPrimitiveTypes);

        xmiExtensionProfiles = new Element("profiles");
        xmiExtension.addContent(xmiExtensionProfiles);

        xmiExtensionDiagrams = new Element("diagrams");
        xmiExtension.addContent(xmiExtensionDiagrams);


        document = new Document(xmi);
    }

    public int newLocalId() {
        return nextEaLocalId++;
    }

    public MetaModel getMetaModel() {
        return metaModel;
    }

    public Namespace getXmiNS() {
        return xmiNs;
    }

    public Namespace getUmlNS() {
        return umlNs;
    }

    public Element getXmi() {
        return xmi;
    }

    public Element getUmlModel() {
        return umlModel;
    }

    public Element getXmiExtension() {
        return xmiExtension;
    }

    public Element getXmiExtensionElements() {
        return xmiExtensionElements;
    }

    public Element getXmiExtensionConnectors() {
        return xmiExtensionConnectors;
    }

    public Element getXmiExtensionPrimitiveTypes() {
        return xmiExtensionPrimitiveTypes;
    }

    public Element getXmiExtensionProfiles() {
        return xmiExtensionProfiles;
    }

    public Element getXmiExtensionDiagrams() {
        return xmiExtensionDiagrams;
    }

    public List<Element> getUmlModelAppliedStereoTypes() {
        return umlModelAppliedStereoTypes;
    }

    public Document getDocument() {
        return document;
    }

    public String newLocalIdAsString() {
        return Integer.toString(newLocalId());
    }

    public String createTimstampAsString() {
        return dateFormat.format(new Date());
    }
}
