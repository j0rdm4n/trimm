/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

import java.util.Set;

/**
 * Models a package. The package compositely contains all {@link RuntimeMetaClazz} and {@link RuntimeMetaPackage}' the are defined within it.
 * @author Jeppe Cramon
 */
public class RuntimeMetaPackage extends RuntimeNameableMetaType {
	private static final long serialVersionUID = 5396053811605956505L;
	private transient Package javaPackage;
	
	public RuntimeMetaPackage(String id, boolean fabricated, Package javaPackage) {
		super(id, fabricated);
		this.javaPackage = javaPackage;
		setName(javaPackage.getName());
	}
	
	public RuntimeMetaPackage(String id, boolean fabricated, String name) {
		super(id, fabricated, name);
	}



	public Set<RuntimeMetaClazz> getClazzesInPackage() {
		return findImmediateChildrenOfType(RuntimeMetaClazz.class);
	}
	
	public Set<RuntimeMetaPackage> getSubPackages() {
		return findImmediateChildrenOfType(RuntimeMetaPackage.class);
	}
	
	public Package getJavaPackage() {
		if (javaPackage == null) {
			javaPackage = Package.getPackage(getName());
			if (javaPackage == null) {
				throw new IllegalStateException("Couldn't resolve the Java Package for meta package '" + getName() + "'");
			}
		}
		return javaPackage;
	}
}
