/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

import java.util.UUID;

/**
 * Models an UML stereotype (meta data about a {@link MetaType}') 
 * @author Jeppe Cramon
 */
public class MetaStereoType extends NameableMetaType {
	private static final long serialVersionUID = 7128391931573703872L;

	public MetaStereoType(String name) {
		this(UUID.randomUUID().toString(), name);
	}

  public MetaStereoType(String id, String name) {
    super(id);
    setName(name);
  }

  @Override
	public MetaStereoType setName(String name) {
		return (MetaStereoType) super.setName(name);
	}

    public MetaType cloneStereotype() {
        MetaStereoType clone = new MetaStereoType(this.getName());
        return clone;
    }
}
