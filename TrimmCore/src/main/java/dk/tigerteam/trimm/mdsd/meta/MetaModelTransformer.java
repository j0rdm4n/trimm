package dk.tigerteam.trimm.mdsd.meta;

/**
 * Code the transforms/changes the metamodel can implement this interface and register with a code generator to preprocess
 * the metamodel BEFORE code generation begins.
 */
public interface MetaModelTransformer {
    void transform(MetaModel metaModel);
}

