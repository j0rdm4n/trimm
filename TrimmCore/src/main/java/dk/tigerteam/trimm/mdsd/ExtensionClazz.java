/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd;

import java.lang.annotation.*;

/**
 * Denotes that the Class this annotation is applied to plays the role of an Extension Class (according to the TRIMM Base/Extension Clazz [generator gap] pattern).
 * It's counterpart is {@link BaseClazz}  
 * @author Jeppe Cramon
 */
@Retention( RetentionPolicy.RUNTIME )
@Target( ElementType.TYPE )
@Documented
public @interface ExtensionClazz {
	/**
	 * The base Clazz counterpart for this extension clazz
	 */
	Class<?> baseClazz();
}
