/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.uml2.xmi.magicdraw;

import dk.tigerteam.trimm.mdsd.meta.*;
import dk.tigerteam.trimm.mdsd.meta.MetaParameter.Direction;
import dk.tigerteam.trimm.mdsd.uml2.xmi.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPath;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * XMI reader for MagicDraw XMI project files (.mdzip or .mdxml)
 *
 * @author Jeppe Cramon
 */
public abstract class MagicDrawXmiReader extends AbstractXmiReader {
  protected static final String METATYPE_RESOLVED = "resolved";
  private static final Log log = LogFactory.getLog(MagicDrawXmiReader.class);

  /**
   * XMI reader for MagicDraw XMI project files (.mdzip or .mdxml)
   */
  public MagicDrawXmiReader(Namespace umlNs, Namespace xmiNs) {
    super(umlNs, xmiNs);

    registerXmiElementParser(new PackageXmiElementParser(), new ClazzXmiElementParser(), new PropertyXmiElementParser(),
        new AssociationXmiElementParser(), new EnumerationXmiElementParser(), new OperationXmiElementParser(),
        new ParameterXmiElementParser());
    registerMetaTypeResolvers(new ClazzResolver(), new PropertyResolver(), new ParameterTypeResolver());
  }

  public MetaModel read(File file) {
    if (file == null) {
      throw new IllegalArgumentException("Can't read from a <null> File");
    }
    return read(file.getAbsolutePath());
  }

  public MetaModel read(URL url) {
    if (url == null) {
      throw new IllegalArgumentException("Can't read from a <null> Url");
    }
    return read(url.getPath());
  }

  public MetaModel read(String path) {
    if (path == null) {
      throw new IllegalArgumentException("Can't read from a <null> path");
    }
    log.info("Reading MagicDraw version XMI model file : '" + path + "'");
    try {
      if (path.startsWith("file:/")) {
        path = path.substring("file:/".length());
      }
      // Calculate meta model name from the last part of the file name
      int lastSeparator = path.lastIndexOf("/");
      if (lastSeparator == -1) {
        lastSeparator = path.lastIndexOf("\\");
      }
      String metaModelName = path;
      if (lastSeparator != -1) {
        metaModelName = metaModelName.substring(lastSeparator + 1);
      }
      String projectDir = path.substring(0, lastSeparator);

      Document doc = loadDocument(path);
      MetaModel model = createAndInitializeMetaModel(metaModelName);

      loadAnyReferencedModules(model, doc, projectDir);

      firstPassParse(model, doc);
      secondPassParse(model, doc);
      fixManyToManyOwnership(model);
      validateModel(model);
      return model;
    } catch (JDOMException e) {
      throw new XmiReaderException(this.getClass().getName() + " - Failed while parsing Xmi Model file '" + path + "': " + e.getMessage(),
          e);
    } catch (IOException e) {
      throw new XmiReaderException(this.getClass().getName() + " - Failed to read Xmi Model file '" + path + "': " + e.getMessage(), e);
    } catch (Exception e) {
      throw new XmiReaderException(this.getClass().getName() + " - Failed while parsing Xmi Model file '" + path + "': " + e.getMessage(),
          e);
    } finally {
      log.info("Done with XMI parsing");
    }
  }

  protected void loadAnyReferencedModules(MetaModel model, Document document, String projectDir) throws JDOMException {
    XPath xpath = XPath.newInstance("//xmi:XMI/xmi:Extension/mountTable");
    xpath.addNamespace(getXmiNs());
    xpath.addNamespace(getUmlNs());

    XmiElement mountTableElement = new XmiElement((Element) xpath.selectSingleNode(document));
    if (mountTableElement.getWrappedElement() == null) {
      return;
    }
    for (XmiElement moduleElement : mountTableElement.getChildrenAsXmiElements()) {
      String resourceUrl = moduleElement.getAttributeValue("resource");
      if (resourceUrl.endsWith(".mdzip")) {
        resourceUrl = resourceUrl.replaceAll("%3Cproject.dir%3E", projectDir);
        try {
          log.info("Loading referenced Module '" + resourceUrl + "'");
          MetaModel referencedMetaModel = read(resourceUrl);
          model.addReferencedMetaModel(referencedMetaModel);
        } catch (Exception e) {
          throw new XmiReaderException("Failed to load referenced module '" + resourceUrl + "'", e);
        }
      }
    }

  }

  protected MetaModel createAndInitializeMetaModel(String metaModelName) {
    MetaModel model = new MetaModel(UUID.randomUUID().toString());
    model.setName(metaModelName);
    // Initialize Interface inheritance data collector
    model.getAttributes().put(InterfaceInheritance.class.getName(), new InterfaceInheritance());
    return model;
  }

  protected Document loadDocument(String path) throws IOException, JDOMException {
    SAXBuilder builder = new SAXBuilder();
    Document doc = null;
    if (path.endsWith(".mdzip")) {
      ZipFile zipFile = new ZipFile(path);
      try {
        if (zipFile.entries().hasMoreElements()) {
          ZipEntry zipFileEntry = zipFile.entries().nextElement();
          if (zipFileEntry.getName().endsWith(".mdxml")) {
            log.info("Reading from MDXML file '" + zipFileEntry.getName() + "' inside the Zip file '" + path + "'");
            doc = builder.build(zipFile.getInputStream(zipFileEntry));
          } else {
            throw new RuntimeException("Zip file '" + path + "' doesn't contain a single entry which ends with .mdxml");
          }
        } else {
          throw new RuntimeException("Zip file '" + path + "' doesn't contain any entries!");
        }
      } finally {
        zipFile.close();
      }
    } else {
      doc = builder.build(new File(path));
    }
    return doc;
  }

  protected void secondPassParse(MetaModel model, Document document) throws JDOMException {
    log.info("Resolving MetaType dependencies");
    boolean restartChildLoop = true;
    while (restartChildLoop)
      restartChildLoop = recursivelyResolveMetaTypes(model, model);

    parseStereoTypesAndTaggedValues(model, document);
    resolveAssociationClazzesAssociations(model);
    resolveRealizations(model, document);
    parseComments(model, document);
  }

  private void parseComments(MetaModel model, Document document) throws JDOMException {
    XPath realizationsXPath = XPath.newInstance("//ownedComment[@xmi:type='uml:Comment']");
    realizationsXPath.addNamespace(getXmiNs());
    realizationsXPath.addNamespace(getUmlNs());

    List<Element> ownedComments = (List<Element>) realizationsXPath.selectNodes(document);
    for (Element ownedComment : ownedComments) {
      Element annotatedElement = ownedComment.getChild("annotatedElement");
      // Check if it's a comment for another element (or if it's a real comment/note)
      if (annotatedElement != null) {
        String idref = annotatedElement.getAttributeValue("idref", getXmiNs());
        try {
          MetaType metaType = model.getById(idref);
          metaType.setDocumentation(ownedComment.getAttributeValue("body"));
        } catch (MetaTypeNotFoundException e) {
          // Ignore - sometimes there's also comments for profiles which we don't support
        }
      }
    }
  }

  /**
   * Resolves all realization elements so {@link MetaInterface#getRealizedBy()} and {@link MetaClazz#getRealizations()} gets filled with
   * content<br/>
   * Also translates any previously recorded {@link InterfaceInheritance} into realizations<br/>
   * Example of a realization element:
   * <p/>
   * <pre>
   *  &lt;packagedElement xmi:type="uml:Realization" xmi:id="EAID_DF10B060_FDE6_4219_AC74_1C2EF9753BE7"
   *                     visibility="public" supplier="EAID_6638EC9A_6C1C_49a0_88D5_91C91567FA9D"
   *                     realizingClassifier="EAID_6638EC9A_6C1C_49a0_88D5_91C91567FA9D"
   *                     client="EAID_4AD473B5_854B_4ae7_BABC_BC55BA75203A"/>
   * </pre>
   * <p/>
   * The <i>"supplier"</i> is the <b>interface</b> that the <i>"client"</i> (ie. the <b>class</b>) implements/realizes
   *
   * @param document The JDOM document
   * @throws JDOMException
   */
  @SuppressWarnings("unchecked")
  protected void resolveRealizations(MetaModel model, Document document) throws JDOMException {
    XPath realizationsXPath = XPath.newInstance("//packagedElement[@xmi:type='uml:Realization']");
    realizationsXPath.addNamespace(getXmiNs());
    realizationsXPath.addNamespace(getUmlNs());

    List<Element> realizations = (List<Element>) realizationsXPath.selectNodes(document);
    for (Element realization : realizations) {
      if (!(realization.getParentElement().getName().equals("packagedElement") || realization.getParentElement().getName()
          .equals("nestedClassifier"))) {
        continue;
      }
      MetaInterface supplyingInterface = null;
      String interfaceId = realization.getChild("supplier").getAttributeValue("idref", getXmiNs());
      if (interfaceId == null) {
        interfaceId = realization.getChild("supplier").getAttributeValue("href");
        if (!interfaceId.contains("#")) {
          throw new XmiReaderException("Couldn't resolve supplier id");
        }
        int index = interfaceId.indexOf('#');
        String referencedMetaModelName = interfaceId.substring(0, index);
        String interfaceIdFixed = interfaceId.substring(index + 1);
        supplyingInterface = model.getReferencedMetaModel(referencedMetaModelName).getByIdAndType(interfaceIdFixed, MetaInterface.class);
      } else {
        supplyingInterface = model.getByIdAndType(interfaceId, MetaInterface.class);
      }
      String realizedBy = realization.getChild("client").getAttributeValue("idref", getXmiNs());
      MetaClazz realizingClazz = model.getByIdAndType(realizedBy, MetaClazz.class);
      supplyingInterface.getRealizedBy().add(realizingClazz);
    }

    // resolve previously recorded interface inheritance, that we will translate into realizations
    InterfaceInheritance interfaceInheritance = (InterfaceInheritance) model.getAttribute(InterfaceInheritance.class.getName());
    for (String interfaceWhichInheritsId : interfaceInheritance.getAllInterfacesWhichInheritsFromOthers()) {
      MetaInterface interfaceWhichInherits = model.getByIdAndType(interfaceWhichInheritsId, MetaInterface.class);
      for (String inheritsFromId : interfaceInheritance.getInterfaceInheritsFrom(interfaceWhichInheritsId)) {
        MetaInterface inheritsFromInterface = null;
        if (inheritsFromId.contains("#")) {
          int index = inheritsFromId.indexOf('#');
          String referencedMetaModel = inheritsFromId.substring(0, index);
          String inheitsFromIdFixed = inheritsFromId.substring(index + 1);
          inheritsFromInterface = model.getReferencedMetaModel(referencedMetaModel)
              .getByIdAndType(inheitsFromIdFixed, MetaInterface.class);
        } else {
          inheritsFromInterface = model.getByIdAndType(inheritsFromId, MetaInterface.class);
        }
        inheritsFromInterface.getRealizedBy().add(interfaceWhichInherits);
      }
    }
  }


  protected void firstPassParse(MetaModel model, Document document) throws JDOMException {
    log.info("Parsing XMI");

    XPath xpath = XPath.newInstance("/xmi:XMI/uml:Model");
    xpath.addNamespace(getXmiNs());
    xpath.addNamespace(getUmlNs());

    XmiElement modelNode = new XmiElement((Element) xpath.selectSingleNode(document));
    if (modelNode.getWrappedElement() == null) {
      throw new XmiReaderException("Failed to find the uml:Model element, according to XPath query " + xpath);
    }
    for (XmiElement xmiElement : modelNode.getChildrenAsXmiElements()) {
      if (xmiElement.getName().equals("packagedElement")) {
        recursivelyFirstPassParseElement(xmiElement, model);
      }
    }
  }

  /**
   * Parses all &lt;uml:Model> subelements which are not of type &lt;packagedElement> and not in the uml namespace but has an attribute
   * named "base_???".
   *
   * @param model    The model node (used for searching the hierarchy)
   * @param document The XMI document
   * @throws JDOMException
   */
  @SuppressWarnings("unchecked")
  protected void parseStereoTypesAndTaggedValues(MetaModel model, Document document) throws JDOMException {
    log.info("Resolving StereoTypes");

    List<Element> stereotypeElements = document.getRootElement().getChildren();

    for (Element element : stereotypeElements) {
      XmiElement stereotypeElement = new XmiElement(element);
      String stereoTypeTargetId = null;
      for (Attribute attribute : (List<Attribute>) stereotypeElement.getAttributes()) {
        if (attribute.getName().startsWith("base_") && !"base_Diagram".equals(attribute.getName())) {
          stereoTypeTargetId = attribute.getValue();
          break;
        }
      }
      if (stereoTypeTargetId == null) {
        // It wasn't a stereotype element
        continue;
        // throw new IllegalStateException("Couldn't find target id for StereoType with id '" + stereotypeElement.getAttributeValue("id",
        // xmiNs));
      }

      String stereoTypeName = stereotypeElement.getName();
      if (ignoredStereotypes(stereoTypeName)) {
        continue;
      }
      try {
        MetaType targetUmlType = model.getById(stereoTypeTargetId);
        MetaType metaStereotype = new MetaStereoType(stereotypeElement.getXmiId(getXmiNs()), stereoTypeName);
        targetUmlType.add(metaStereotype);

        // Tagged values (even though tagged values belong to the stereotype, then other tools support defining tagged values without
        // binding them to stereotypes,
        // so we'll apply the the tagged values to both the stereotype and to the target type)
        for (Attribute attribute : (List<Attribute>) stereotypeElement.getAttributes()) {
          if (attribute.getName().equals("base_Element") || (attribute.getName().equals("id") && attribute.getNamespace().equals(getXmiNs()))) {
            continue;
          }
          targetUmlType.addTaggedValue(attribute.getName(), attribute.getValue().trim());
          metaStereotype.addTaggedValue(attribute.getName(), attribute.getValue().trim());
        }
      } catch (MetaTypeNotFoundException e) {
        // Ignore, we have just discovered a StereoType for an element that we have ignored during parsing...
        // throw new XmiReaderException("Failed while parsing stereotype " + stereotypeElement, e);
      }
    }
  }

  protected boolean ignoredStereotypes(String stereoTypeName) {
    if (stereoTypeName.equals("property_get") || stereoTypeName.equals("property_set") || stereoTypeName.equals("attribute_name")
        || stereoTypeName.equals("throws")) {
      return true;
    }
    return false;
  }

  protected void recursivelyFirstPassParseElement(XmiElement xmiElement, MetaType parent) {
    List<XmiElementParser> xmiElementParsers = getXmiElementParser(xmiElement, parent);
    MetaType currentParent = parent;
    for (XmiElementParser xmiElementParser : xmiElementParsers) {
      try {
        currentParent = xmiElementParser.parse(xmiElement, currentParent);
      } catch (Exception e) {
        throw new XmiReaderException("Failed during parsing of element '" + xmiElement + "' (" + xmiElement.getXmiId(getXmiNs()) + ")", e);
      }
    }

    for (Element childElement : (List<Element>) xmiElement.getWrappedElement().getChildren()) {
      recursivelyFirstPassParseElement(new XmiElement(childElement), currentParent);
    }
  }

  /**
   * Handle bounds (embedded in the type information) parsing, which is sometime used for simple types (String, int, long, etc.).<br/>
   * E.g. <code>String(1, 255)</code> where <code>String</code> is the <b>type</b> and 1 is the <b>lowerbound</b>
   * and 255 is the <b>upperbound</b>. <em>Lower bound is optional, so String(255) resolves to type String and upperbound 255.</em><p/>
   * The property will be updated with the bounds if they're found.<br/>
   * The property type will be stored in the property's attribute bag under {@link MetaProperty#TYPE_ATTR_KEY} for later processing /retrieval

   * <p/>
   * <pre>
   * &lt;ownedAttribute xmi:type='uml:Property' xmi:id='_15_0_1_6400215_1228159557091_67955_580' name='age' visibility='private' isStatic='true' isReadOnly='true'>
   *  	&lt;type xmi:type='uml:DataType' href='UML_Standard_Profile.xml#eee_1045467100323_917313_65'>
   * 	   &lt;xmi:Extension extender='MagicDraw UML 15.0' extenderID='MagicDraw UML 15.0'>
   * 			&lt;referenceExtension referentPath='UML Standard Profile::MagicDraw Profile::datatypes::int' referentType='DataType'/>
   * 	   &lt;/xmi:Extension>
   *     &lt;/type>
   *      ....
   * &lt;ownedAttribute>
   * </pre>
   *
   * @param propertyOrParameter
   * @param typeElement
   * @return The type name/id
   */
  public static String handlePropertyOrParameterTypeWithPossibleBounds(MetaType propertyOrParameter, XmiElement typeElement, XmiReader xmiReader) {
    if (typeElement.hasXmiType("uml:Class", xmiReader.getXmiNs())) {      /*
			 * Example: <ownedAttribute xmi:type='uml:Property' xmi:id='_16_0_6400215_1228808162867_691467_413' name='baseEntities'
			 * visibility='private' aggregation='composite' association='_16_0_6400215_1228808162867_134437_412'> <type xmi:type='uml:Class'
			 * href='BaseModel.mdzip#_15_0_1_6400215_1228803949255_365032_241'> <xmi:Extension extender='MagicDraw UML 16.0' extenderID='MagicDraw
			 * UML 16.0'> <referenceExtension referentPath='model::base::BaseEntity' referentType='Class'/> </xmi:Extension> </type>
			 * </ownedAttribute>
			 */
      String href = typeElement.getAttributeValue("href");
      int index = href.indexOf('#');
      if (index == -1) {
        throw new XmiReaderException("Invalid reference for " + propertyOrParameter + " from href value: '" + href + "'");
      }
      propertyOrParameter.getAttributes().put(MetaProperty.TYPE_IS_REFERENCED_ATTR_KEY, Boolean.TRUE);
      propertyOrParameter.getAttributes().put(MetaProperty.TYPE_ATTR_KEY, href);
      propertyOrParameter.getAttributes().put(MetaProperty.TYPE_IS_BUILTIN_ATTR_KEY, Boolean.TRUE);
      return href;
    }

    XmiElement extensionElement = typeElement.getChild("Extension", xmiReader.getXmiNs());
    if (extensionElement == null) {
      throw new XmiReaderException("Failed to read type information for " + propertyOrParameter
          + ". Reason: Missing xmi:Extension element under the type element");
    }
    XmiElement referenceExtension = extensionElement.getChild("referenceExtension");
    if (referenceExtension == null) {
      throw new XmiReaderException("Failed to read type information for " + propertyOrParameter
          + ". Reason: Missing referenceExtension element under the xmi:Reference element");
    }

    String typeValue = referenceExtension.getAttributeValue("referentPath");
    // Check for type value bounds
    int index = typeValue.indexOf('(');
    if (index == -1) {
      typeValue = resolveBuiltInType(typeValue, propertyOrParameter);
      propertyOrParameter.getAttributes().put(MetaProperty.TYPE_ATTR_KEY, typeValue);
      propertyOrParameter.getAttributes().put(MetaProperty.TYPE_IS_BUILTIN_ATTR_KEY, Boolean.TRUE);
      return typeValue;
    } else {
      Bounded boundedMetaType = (Bounded) propertyOrParameter;
      // --- Type ---
      String type = typeValue.substring(0, index).trim();
      type = resolveBuiltInType(type, propertyOrParameter);
      propertyOrParameter.getAttributes().put(MetaProperty.TYPE_ATTR_KEY, type);
      propertyOrParameter.getAttributes().put(MetaProperty.TYPE_IS_BUILTIN_ATTR_KEY, Boolean.TRUE);

      // --- Bounds ---
      int lastIndex = typeValue.indexOf(')');
      String boundsValue = typeValue.substring(index + 1, lastIndex);
      // Split bounds using a comma, to read upper and lower bounds.
      String[] bounds = boundsValue.split(",");
      if (bounds.length < 1 || bounds.length > 2) {
        throw new XmiReaderException("Property '" + propertyOrParameter + "' with type value '" + typeValue
            + "' has invalid defined bounds. Allowed bound definitions 'Type (lowerBound, upperBound)', 'Type(upperBound)'");
      }
      if (bounds.length == 1) {
        // Upperbound
        String upperBoundValue = bounds[0].trim();
        try {
          boundedMetaType.setUpperBound(Long.parseLong(upperBoundValue));
        } catch (NumberFormatException e) {
          throw new XmiReaderException("Invalid upperbound value '" + upperBoundValue + "' for property '" + propertyOrParameter
              + "' with type value '" + typeValue + "'");
        }
      } else if (bounds.length == 2) {
        // LowerBound, UpperBound
        String lowerBoundValue = bounds[0].trim();
        String upperBoundValue = bounds[1].trim();
        try {
          boundedMetaType.setLowerBound(Long.parseLong(lowerBoundValue));
        } catch (NumberFormatException e) {
          throw new XmiReaderException("Invalid lowerbound value '" + lowerBoundValue + "' for property '" + propertyOrParameter
              + "' with type value '" + typeValue + "'");
        }
        try {
          boundedMetaType.setUpperBound(Long.parseLong(upperBoundValue));
        } catch (NumberFormatException e) {
          throw new XmiReaderException("Invalid upperbound value '" + upperBoundValue + "' for property '" + propertyOrParameter
              + "' with type value '" + typeValue + "'");
        }
      }
      return type;
    }
  }

  /**
   * Magic draw uses types such as "UML Standard Profile::UML2 Metamodel::AuxiliaryConstructs::PrimitiveTypes::String" for built in types so
   * we will just skip the first part of it
   *
   * @param typeValue
   * @param propertyOrParameter
   */
  public static String resolveBuiltInType(String typeValue, MetaType propertyOrParameter) {
    int lastIndex = typeValue.lastIndexOf("::");
    if (lastIndex == -1) {
      return typeValue;
    } else {
      return typeValue.substring(lastIndex + "::".length());
    }
  }

  // ------------------------------------- EA XMI Element parsers -------------------------------------
  public static class PackageXmiElementParser extends AbstractXmiElementParser {
    public MetaPackage parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
      MetaPackage _package = new MetaPackage(xmiElement.getXmiId(getXmiNs()));
      _package.setName(xmiElement.getUmlName());
      _package.setVisibility(getVisibility(xmiElement));
      _package.setOwner(parent);
      _package.setRootPackage(parent instanceof MetaModel);
      return _package;
    }

    public boolean supports(XmiElement xmiElement, MetaType parent) {
      return xmiElement.hasXmiType("uml:Package", getXmiNs()) && xmiElement.getXmiId(getXmiNs()) != null;
    }
  }

  public static class ClazzXmiElementParser extends AbstractXmiElementParser {
    private final Log log = LogFactory.getLog(ClazzXmiElementParser.class);

    public MetaClazz parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
      if (xmiElement.getUmlName() != null && xmiElement.getUmlName().startsWith("$")) {
        return null;
      }
      MetaClazz clazz = null;
      if (xmiElement.hasXmiType("uml:Class", getXmiNs())) {
        clazz = new MetaClazz(xmiElement.getXmiId(getXmiNs()));
      } else if (xmiElement.hasXmiType("uml:AssociationClass", getXmiNs())) {
        clazz = new MetaAssociationClazz(xmiElement.getXmiId(getXmiNs()));
      } else if (xmiElement.hasXmiType("uml:Interface", getXmiNs())) {
        clazz = new MetaInterface(xmiElement.getXmiId(getXmiNs()));
      }
      clazz.setName(xmiElement.getUmlName());
      clazz.setVisibility(getVisibility(xmiElement));
      clazz.setOwner(parent);
      clazz.setAbstract(getAbstract(xmiElement));
      // Check for Generalization element(s)
      List<XmiElement> generalizations = xmiElement.getChildrenAsXmiElements("generalization");
      if (generalizations.size() > 0) {
        if (clazz.isInterface()) {
          InterfaceInheritance interfaceInheritance = (InterfaceInheritance) parent.getMetaModel().getAttribute(
              InterfaceInheritance.class.getName());
          for (XmiElement generalizationElement : generalizations) {
            String generalId = generalizationElement.getAttributeValue("general");
            if (generalId == null) {
              XmiElement generalElement = generalizationElement.getChild("general");
              if (generalElement == null) {
                throw new XmiReaderException("Couldn't find general id for generalizations for " + clazz);
              }
              generalId = generalElement.getAttributeValue("href");
            }
            interfaceInheritance.addInterfaceInheritance(clazz.getId(), generalId);
          }
        } else {
          if (generalizations.size() != 1) {
            throw new XmiReaderException("Class '" + clazz.getName() + " with id '" + clazz.getId()
                + "' has more than 1 generalization. Clazz multiple inheritance isn't supported - use interfaces instead :-)");
          }
          XmiElement generalizationElement = generalizations.get(0);
          String generalId = generalizationElement.getAttributeValue("general");
          if (generalId == null) {
            XmiElement generalElement = generalizationElement.getChild("general");
            if (generalElement == null) {
              throw new XmiReaderException("Couldn't find general id for generalizations for " + clazz);
            }
            generalId = generalElement.getAttributeValue("href");
          }
          clazz.addAttribute(MetaClazz.GENERALIZATION_ID_ATTR_KEY, generalId);
        }
      }

      XmiElement templateBinding = xmiElement.getChild("templateBinding");
      if (templateBinding != null) {
        List<TemplateParameterId> templateParameters = new ArrayList<TemplateParameterId>();
        for (XmiElement templateSubstitution : templateBinding.getChildrenAsXmiElements("parameterSubstitution")) {
          String actualTypeId = templateSubstitution.getAttributeValue("actual");
          if (actualTypeId == null) {
            XmiElement actualElement = templateSubstitution.getChild("actual");
            if (actualElement != null) {
              actualTypeId = actualElement.getAttributeValue("href");
              if (actualTypeId == null) {
                throw new XmiReaderException("Missing href value for 'actual' child element in templateBinding under " + clazz.toString());
              }
              boolean isPrimitiveType = "uml:PrimitiveType".equals(actualElement.getAttributeValue("type", getXmiNs()));
              templateParameters.add(new TemplateParameterId(actualTypeId, isPrimitiveType));
            }
          } else {
            templateParameters.add(new TemplateParameterId(actualTypeId));
          }
        }

        clazz.setAttribute(MetaClazz.TEMPLATE_PARAMETERS_ATTR_KEY, templateParameters);
      }

      log.debug("Parsed MetaClazz '" + clazz + "'");
      return clazz;
    }

    protected boolean getAbstract(XmiElement xmiElement) throws DataConversionException {
      Attribute isAbstract = xmiElement.getAttribute("isAbstract");
      if (isAbstract != null) {
        return isAbstract.getBooleanValue();
      } else {
        return false;
      }
    }

    public boolean supports(XmiElement xmiElement, MetaType parent) {

      return (xmiElement.hasXmiType("uml:Class", getXmiNs()) || xmiElement.hasXmiType("uml:AssociationClass", getXmiNs()) || xmiElement.hasXmiType("uml:Interface", getXmiNs()))
          && (xmiElement.getName().equals("packagedElement") || xmiElement.getName().equals("nestedClassifier"));
    }
  }

  public static class EnumerationXmiElementParser extends AbstractXmiElementParser {
    public MetaEnumeration parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
      MetaEnumeration enumeration = new MetaEnumeration(xmiElement.getXmiId(getXmiNs()));
      enumeration.setName(xmiElement.getUmlName());
      enumeration.setVisibility(getVisibility(xmiElement));
      enumeration.setOwner(parent);

      return enumeration;
    }

    public boolean supports(XmiElement xmiElement, MetaType parent) {
      return xmiElement.hasXmiType("uml:Enumeration", getXmiNs());
    }
  }

  public static class PropertyXmiElementParser extends AbstractXmiElementParser {
    public MetaProperty parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
      MetaProperty property = new MetaProperty(xmiElement.getXmiId(getXmiNs()));
      property.setName(xmiElement.getUmlName());
      property.setVisibility(getVisibility(xmiElement));
      property.setOwner(parent);
      property.setStatic(xmiElement.isUmlStatic());
      property.setOrdered(xmiElement.isUmlOrdered());
      property.setWriteable(!xmiElement.isUmlReadOnly());

      // Default value
      XmiElement defaultValueElement = xmiElement.getChild("defaultValue");
      if (defaultValueElement != null) {
        property.setDefaultValue(defaultValueElement.getAttributeValue("value"));
      }

      if (property.isStatic() && property.getDefaultValue() == null) {
        log.warn("Property '" + property + "' is marked as Static, but doesn't specify a default/initial value!");
      }

      // Association
      String association = xmiElement.getUmlAssociation();
      if (association != null) {
        property.getAttributes().put(MetaProperty.ASSOCIATION_ATTR_KEY, association);
        // Check the element name to deduce the ownership
        if (xmiElement.getName().equals("ownedEnd")) {
          property.getAttributes().put(MetaProperty.IS_ASSOCIATION_OWNER_ATTR_KEY, Boolean.FALSE);
        } else {
          property.getAttributes().put(MetaProperty.IS_ASSOCIATION_OWNER_ATTR_KEY, Boolean.TRUE);
        }
      }

      // Aggregation
      property.setAggregation(getAggregation(xmiElement));

      // Type
      XmiElement typeElement = xmiElement.getChild("type");
      if (typeElement != null) {
        handlePropertyOrParameterTypeWithPossibleBounds(property, typeElement, getXmiReader());
      } else if (xmiElement.getAttributeValue("type") != null) {
        property.getAttributes().put(MetaProperty.TYPE_ATTR_KEY, xmiElement.getAttributeValue("type"));
        property.getAttributes().put(MetaProperty.TYPE_IS_BUILTIN_ATTR_KEY, Boolean.FALSE);
      } else if (xmiElement.getAttributeValue("type") == null && xmiElement.hasXmiType("uml:Property", getXmiNs())) {
        // EnumerationLiteral doens't have a type, but Property elements MUST have one
        throw new XmiReaderException("MetaProperty doesn't have a type specified!. MetaProperty: " + property);
      }

      // Max Cardinality
      {
        Element upperValueElement = findChildElementInSubTree(xmiElement.getWrappedElement(), "upperValue");
        if (upperValueElement != null) {
          String upperValue = upperValueElement.getAttributeValue("value");
          if (upperValue.equals("*")) {
            property.setMaxCardinality(MetaParameter.CARDINALITY_UNLIMITED);
          } else {
            property.setMaxCardinality(Integer.parseInt(upperValue));
          }
        }
      }
      // Min Cardinality
      {
        Element lowerValueElement = findChildElementInSubTree(xmiElement.getWrappedElement(), "lowerValue");
        if (lowerValueElement != null) {
          String lowerValue = lowerValueElement.getAttributeValue("value");
          if (lowerValue == null || lowerValue.equals("")) {
            property.setMinCardinality(0);
          } else {
            property.setMinCardinality(Integer.parseInt(lowerValue));
          }
        }
      }

      // Finally, if we're dealing with an enumeration, let's apply the enum stereotype
      if (xmiElement.hasXmiType("uml:EnumerationLiteral", getXmiNs())) {
        property.setEnumerationLiteral(true);
      }

      return property;
    }

    public boolean supports(XmiElement xmiElement, MetaType parent) {
      boolean supports = xmiElement.hasXmiType("uml:Property", getXmiNs()) || xmiElement.hasXmiType("uml:EnumerationLiteral", getXmiNs());
      if (supports && xmiElement.getParentXmiElement().getXmiType(getXmiNs()).equals("uml:Stereotype")) {
        supports = false;
      }
      return supports;
    }
  }

  public static class AssociationXmiElementParser extends AbstractXmiElementParser {
    public MetaAssociation parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
      MetaAssociation association = new MetaAssociation(xmiElement.getXmiId(getXmiNs()));
      association.setName(xmiElement.getUmlName());
      association.setVisibility(getVisibility(xmiElement));
      association.setOwner(parent);

      // Save the memberEnds
      List<XmiElement> memberEnds = xmiElement.getChildrenAsXmiElements("memberEnd");
      if (memberEnds.size() != 2) {
        throw new XmiReaderException("Malformed XMI - " + association + "' only has '" + memberEnds.size()
            + "' memberEnds, expected 2 memberEnds");
      }
      association.getAttributes().put(MetaAssociation.MEMBEREND_DESTINATION_ATTR_KEY, memberEnds.get(0).getXmiIdRef(getXmiNs()));
      association.getAttributes().put(MetaAssociation.MEMBEREND_SOURCE_ATTR_KEY, memberEnds.get(1).getXmiIdRef(getXmiNs()));

      // Check for bidirectionality
      int numberOfOwnedEnds = xmiElement.getChildrenAsXmiElements("ownedEnd").size();
      if (numberOfOwnedEnds == 0 || numberOfOwnedEnds == 2) {
        association.setBidirectional(true);
        if (numberOfOwnedEnds == 2) {
          association.addAttribute(MetaAssociation.NO_OWNED_ENDS, Boolean.TRUE);
        }
      }


      return association;
    }

    public boolean supports(XmiElement xmiElement, MetaType parent) {
      return xmiElement.hasXmiType("uml:Association", getXmiNs()) || xmiElement.hasXmiType("uml:AssociationClass", getXmiNs());
    }
  }

  /**
   * &lt;ownedOperation xmi:id="EAID_7B9A2794_E93C_418b_8208_B6D985849451" name="bla" visibility="public" concurrency="sequential">
   * &lt;ownedParameter xmi:id="EAID_722FFBD9_E5D3_4541_B84A_531C9543F9A5" name="param3" type="EAID_3EDF711A_D32A_4172_8A10_B0DA7526DAFB"
   * direction="in"/> &lt;ownedParameter xmi:id="EAID_DFECD69C_43EA_4766_8F43_98F99C361585" name="param2String" direction="in"
   * type="EAJava_String"/> &lt;ownedParameter xmi:id="EAID_4F7EE0D3_9F2F_4b86_9A57_C149C676AF02" name="param1Bool" direction="in"
   * type="EAJava_boolean"/> &lt;ownedParameter xmi:id="EAID_RT000000_E93C_418b_8208_B6D985849451" name="return" direction="return"
   * type="EAJava_void"/> &lt;/ownedOperation>
   */
  public static class OperationXmiElementParser extends AbstractXmiElementParser {
    public MetaType parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
      MetaOperation operation = new MetaOperation(xmiElement.getXmiId(getXmiNs()));
      operation.setName(xmiElement.getUmlName());
      operation.setVisibility(getVisibility(xmiElement));
      operation.setOwner(parent);
      operation.setStatic(xmiElement.isUmlStatic());
      operation.setAbstract(xmiElement.isUmlAbstract());

      return operation;
    }

    public boolean supports(XmiElement xmiElement, MetaType parent) {
      return xmiElement.getName().equals("ownedOperation");
    }
  }


  /**
   * &lt;ownedOperation xmi:id="EAID_7B9A2794_E93C_418b_8208_B6D985849451" name="bla" visibility="public" concurrency="sequential">
   * &lt;ownedParameter xmi:id="EAID_722FFBD9_E5D3_4541_B84A_531C9543F9A5" name="param3" type="EAID_3EDF711A_D32A_4172_8A10_B0DA7526DAFB"
   * direction="in"/> &lt;ownedParameter xmi:id="EAID_DFECD69C_43EA_4766_8F43_98F99C361585" name="param2String" direction="in"
   * type="EAJava_String"/> &lt;ownedParameter xmi:id="EAID_4F7EE0D3_9F2F_4b86_9A57_C149C676AF02" name="param1Bool" direction="in"
   * type="EAJava_boolean"/> &lt;ownedParameter xmi:id="EAID_RT000000_E93C_418b_8208_B6D985849451" name="return" direction="return"
   * type="EAJava_void"/> &lt;/ownedOperation>
   */
  public static class ParameterXmiElementParser extends AbstractXmiElementParser {

    protected Direction mapDirection(String directionFromXmi) {
      if ("in".equalsIgnoreCase(directionFromXmi)) return Direction.In;
      if ("inout".equalsIgnoreCase(directionFromXmi)) return Direction.InOut;
      if ("out".equalsIgnoreCase(directionFromXmi)) return Direction.Out;
      if ("return".equalsIgnoreCase(directionFromXmi)) return Direction.Return;
      throw new IllegalArgumentException("Unsupported Direction value '" + directionFromXmi + "'");
    }

    public MetaType parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
      MetaOperation operation = (MetaOperation) parent;
      MetaParameter parameter = new MetaParameter(xmiElement.getXmiId(getXmiNs()));
      parameter.setName(xmiElement.getUmlName());
      parameter.setOwner(operation); // Add as children as well, so we allow the ParameterTypeResolver to resolve types transparently
      parameter.setDirection(xmiElement.getAttributeValue("direction") != null ? mapDirection(xmiElement
          .getAttributeValue("direction")) : Direction.In);


      // Max Cardinality
      {
        Element upperValueElement = findChildElementInSubTree(xmiElement.getWrappedElement(), "upperValue");
        if (upperValueElement != null) {
          String upperValue = upperValueElement.getAttributeValue("value");
          if (upperValue.equals("*")) {
            parameter.setMaxCardinality(MetaParameter.CARDINALITY_UNLIMITED);
          } else {
            parameter.setMaxCardinality(Integer.parseInt(upperValue));
          }
        }
      }
      // Min Cardinality
      {
        Element lowerValueElement = findChildElementInSubTree(xmiElement.getWrappedElement(), "lowerValue");
        if (lowerValueElement != null) {
          String lowerValue = lowerValueElement.getAttributeValue("value");
          if (lowerValue == null || lowerValue.equals("")) {
            parameter.setMinCardinality(0);
          } else {
            parameter.setMinCardinality(Integer.parseInt(lowerValue));
          }
        }
      }

      // Type
      XmiElement typeElement = xmiElement.getChild("type");
      if (typeElement != null) {
        handlePropertyOrParameterTypeWithPossibleBounds(parameter, typeElement, getXmiReader());
      } else {
        parameter.getAttributes().put(MetaProperty.TYPE_ATTR_KEY, xmiElement.getAttributeValue("type").trim());
        parameter.getAttributes().put(MetaProperty.TYPE_IS_BUILTIN_ATTR_KEY, Boolean.FALSE);
      }

      if (parameter.getDirection() == Direction.Return) {
        operation.setReturnParameter(parameter);
      } else {
        operation.addParameters(parameter);
      }

      return parameter;
    }

    public boolean supports(XmiElement xmiElement, MetaType parent) {
      return xmiElement.getName().equals("ownedParameter") && xmiElement.getParentXmiElement().getName().equals("ownedOperation");
    }
  }

  public static Element findChildElementInSubTree(Element context, String elementName) {
    for (Object child : context.getChildren()) {
      Element childElem = (Element) child;
      if (childElem.getName().equals(elementName)) {
        return childElem;
      } else {
        Element result = findChildElementInSubTree(childElem, elementName);
        if (result != null) {
          return result;
        }
      }
    }
    return null;
  }

  // ------------------------------ UmlTypeResolvers -------------------------------
  public static class ClazzResolver implements MetaTypeResolver<MetaClazz> {
    public boolean resolve(MetaType metaType, MetaModel model) {
      MetaClazz clazz = (MetaClazz) metaType;
      if (clazz.getAttribute(METATYPE_RESOLVED) != null) {
        return false;
      }
      if (clazz.hasAttribute(MetaClazz.GENERALIZATION_ID_ATTR_KEY)) {
        String superClazzId = (String) clazz.getAttribute(MetaClazz.GENERALIZATION_ID_ATTR_KEY);
        MetaClazz superClazz = null;
        if (superClazzId.contains("#")) {
          int index = superClazzId.indexOf('#');
          String referencedMetaModelName = superClazzId.substring(0, index);
          superClazzId = superClazzId.substring(index + 1);
          superClazz = (MetaClazz) model.getReferencedMetaModel(referencedMetaModelName).getById(superClazzId);
        } else {
          superClazz = (MetaClazz) model.getById(superClazzId);
        }
        clazz.setSuperClazz(superClazz);
      }

      if (clazz.hasAttribute(MetaClazz.TEMPLATE_PARAMETERS_ATTR_KEY)) {
        @SuppressWarnings("unchecked")
        List<TemplateParameterId> templateParameters = (List<TemplateParameterId>) clazz
            .getAttribute(MetaClazz.TEMPLATE_PARAMETERS_ATTR_KEY);
        for (TemplateParameterId templateParameter : templateParameters) {
          MetaClazz templateClazz;
          String actualTypeId = templateParameter.getActualTypeId();
          if (templateParameter.isPrimitiveType()) {
            int index = actualTypeId.indexOf('#');
            actualTypeId = actualTypeId.substring(index + 1);
            templateClazz = new MetaClazz(UUID.randomUUID().toString());
            templateClazz.setBuiltInType(true).setName(actualTypeId);
          } else {
            if (actualTypeId.contains("#")) {
              int index = actualTypeId.indexOf('#');
              String referencedMetaModelName = actualTypeId.substring(0, index);
              actualTypeId = actualTypeId.substring(index + 1);
              templateClazz = (MetaClazz) model.getReferencedMetaModel(referencedMetaModelName).getById(actualTypeId);
            } else {
              // Since we currently don't support nested classes, then we may lack some types
              templateClazz = (MetaClazz) model.findById(actualTypeId);
            }
          }
          // Since we currently don't support nested classes, then we may lack some types
          if (templateClazz != null) {
            clazz.addTemplateParameters(templateClazz);
          }
        }
      }
      clazz.getAttributes().put(METATYPE_RESOLVED, Boolean.TRUE);
      return false;
    }

    public boolean supports(MetaType metaType) {
      return metaType.getClass().equals(MetaClazz.class);
    }
  }

  public static class ParameterTypeResolver implements MetaTypeResolver<MetaParameter> {
    public boolean resolve(MetaType metaType, MetaModel model) {
      MetaParameter parameter = (MetaParameter) metaType;
      if (parameter.getAttribute(METATYPE_RESOLVED) != null) {
        return false;
      }
      if (parameter.hasAttribute(MetaProperty.TYPE_ATTR_KEY)) {
        String typeRefId = (String) parameter.getAttribute(MetaProperty.TYPE_ATTR_KEY);
        MetaClazz typeClazz = null;
        if (parameter.getAttribute(MetaProperty.TYPE_IS_BUILTIN_ATTR_KEY) != null
            && parameter.getAttribute(MetaProperty.TYPE_IS_BUILTIN_ATTR_KEY).equals(Boolean.TRUE)) {
          typeClazz = new MetaClazz(UUID.randomUUID().toString());
          typeClazz.setBuiltInType(true).setName(typeRefId);
        } else {
          boolean isReferenced = parameter.getAttribute(MetaProperty.TYPE_IS_REFERENCED_ATTR_KEY) != null
              && parameter.getAttribute(MetaProperty.TYPE_IS_REFERENCED_ATTR_KEY).equals(Boolean.TRUE);
          if (isReferenced) {
            int index = typeRefId.indexOf('#');
            String metaModelName = typeRefId.substring(0, index);
            String refId = typeRefId.substring(index + 1);
            typeClazz = model.getReferencedMetaModel(metaModelName).getByIdAndType(refId, MetaClazz.class);
          } else {
            typeClazz = model.getByIdAndType(typeRefId, MetaClazz.class);
          }
        }
        parameter.setType(typeClazz);
      }
      parameter.getAttributes().put(METATYPE_RESOLVED, Boolean.TRUE);
      return false;
    }

    public boolean supports(MetaType metaType) {
      return metaType instanceof MetaParameter;
    }
  }

  public static class PropertyResolver implements MetaTypeResolver<MetaProperty> {
    public boolean resolve(MetaType metaType, MetaModel model) {
      MetaProperty property = (MetaProperty) metaType;
      if (property.getAttribute(METATYPE_RESOLVED) != null) {
        return false;
      }

      if (property.hasAttribute(MetaProperty.ASSOCIATION_ATTR_KEY)) {
        // MetaAssociation association = (MetaAssociation) model.getById((String)
        // property.getAttribute(MetaProperty.ASSOCIATION_ATTR_KEY));
        MetaAssociation association = (MetaAssociation) model.getByIdAndType(
            (String) property.getAttribute(MetaProperty.ASSOCIATION_ATTR_KEY), MetaAssociation.class);

        // Fix ownership
        if (property.getOwner() == association) {
          // Move the ownership to the MetaClazz, which should home the property
          String oppositePropertyId = null;
          if (property.getId().equals(association.getAttribute(MetaAssociation.MEMBEREND_DESTINATION_ATTR_KEY))) {
            oppositePropertyId = (String) association.getAttribute(MetaAssociation.MEMBEREND_SOURCE_ATTR_KEY);
          } else {
            oppositePropertyId = (String) association.getAttribute(MetaAssociation.MEMBEREND_DESTINATION_ATTR_KEY);
          }
          MetaProperty oppositeProperty = (MetaProperty) model.getById(oppositePropertyId);
          MetaClazz oppositePropertyType = null;
          String metaClazzId = (String) oppositeProperty.getAttribute(MetaProperty.TYPE_ATTR_KEY);
          if (metaClazzId.contains("#")) {
            int index = metaClazzId.indexOf('#');
            String metaModelName = metaClazzId.substring(0, index);
            String refId = metaClazzId.substring(index + 1);
            oppositePropertyType = model.getReferencedMetaModel(metaModelName).getByIdAndType(refId, MetaClazz.class);
          } else {
            oppositePropertyType = (MetaClazz) model.getByIdAndType(metaClazzId, MetaClazz.class);
          }
          if (oppositePropertyType == null) {
            throw new IllegalStateException("Failed to find type for property with id '" + oppositeProperty.getId() + "'");
          }
          property.setOwner(oppositePropertyType);
          return true;
        }

        if (association.isBidirectional()) {
          if (association.getOwnerProperty() == null || association.getOwnerProperty() == property) {
            association.setOwnerProperty(property);
          } else {
            association.setOwnedProperty(property);
          }
        } else {
          boolean isOwner = true;
          if (property.hasAttribute(MetaProperty.IS_ASSOCIATION_OWNER_ATTR_KEY)) {
            isOwner = (Boolean) property.getAttribute(MetaProperty.IS_ASSOCIATION_OWNER_ATTR_KEY);
          }
          // TODO: What about wrongly placed ownership in relation to shared and composition?
          if (isOwner) {
            // Sanity check
            if (association.getOwnerProperty() != null && association.getOwnerProperty() != property) {
              throw new XmiReaderException("Model validation error. Association '" + association.getId() + "' already has owner property '"
                  + association.getOwnerProperty() + "', but property '" + property.getId()
                  + "' is also marked as owner of the association");
            }
            property.setAsOwnerOfAssociation(association);
          } else {
            // Sanity check
            if (association.getOwnedProperty() != null && association.getOwnedProperty() != property) {
              throw new XmiReaderException("Model validation error. Association '" + association.getId() + "' already has owned property '"
                  + association.getOwnedProperty().getId() + "', but property '" + property.getId()
                  + "' is also marked as owned by the association");
            }
            property.setAsOwnedByAssociation(association);
          }
        }
      }
      if (property.hasAttribute(MetaProperty.TYPE_ATTR_KEY)) {
        String typeRefId = (String) property.getAttribute(MetaProperty.TYPE_ATTR_KEY);
        MetaClazz typeClazz = null;
        if (property.getAttribute(MetaProperty.TYPE_IS_BUILTIN_ATTR_KEY) != null
            && property.getAttribute(MetaProperty.TYPE_IS_BUILTIN_ATTR_KEY).equals(Boolean.TRUE)) {
          typeClazz = new MetaClazz(UUID.randomUUID().toString());
          typeClazz.setBuiltInType(true).setName(typeRefId);
        } else {
          boolean isReferenced = property.getAttribute(MetaProperty.TYPE_IS_REFERENCED_ATTR_KEY) != null
              && property.getAttribute(MetaProperty.TYPE_IS_REFERENCED_ATTR_KEY).equals(Boolean.TRUE);
          if (isReferenced) {
            int index = typeRefId.indexOf('#');
            String metaModelName = typeRefId.substring(0, index);
            String refId = typeRefId.substring(index + 1);
            typeClazz = model.getReferencedMetaModel(metaModelName).getByIdAndType(refId, MetaClazz.class);
          } else {
            typeClazz = model.getByIdAndType(typeRefId, MetaClazz.class);
          }
        }
        property.setType(typeClazz);
      }
      property.getAttributes().put(METATYPE_RESOLVED, Boolean.TRUE);
      return false;
    }

    public boolean supports(MetaType metaType) {
      return metaType.getClass().equals(MetaProperty.class);
    }
  }

  // ------------------------------------------------------------

  /**
   * Collects all interface inheritances which are translated into realizations during
   * {@link MagicDrawXmiReader#resolveRealizations(MetaModel, Document)}<br/>
   * Is stored in the {@link MetaModel}'s attribute map under {@link InterfaceInheritance} FQCN.
   */
  public static class InterfaceInheritance implements Serializable {
    private Map<String, Set<String>> inheritsFrom = new HashMap<String, Set<String>>();

    /**
     * Get all the interfaces that this interface inherits from
     *
     * @param id The id of the interface
     * @return The interfaces this interface inherits from (empty set if there are no realizations)
     */
    public Set<String> getInterfaceInheritsFrom(String id) {
      Set<String> interfaceInheritsFrom = inheritsFrom.get(id);
      if (interfaceInheritsFrom == null) {
        interfaceInheritsFrom = new HashSet<String>();
        inheritsFrom.put(id, interfaceInheritsFrom);
      }
      return interfaceInheritsFrom;
    }

    public Set<String> getAllInterfacesWhichInheritsFromOthers() {
      return inheritsFrom.keySet();
    }

    /**
     * Add an interface that this interface inherits from
     *
     * @param id             The id of the interface that inherits from the "inheritsFrom" interface
     * @param inheritsFromId The id of the interface that the interface inherits from
     */
    public void addInterfaceInheritance(String id, String inheritsFromId) {
      Set<String> interfaceInheritsFrom = getInterfaceInheritsFrom(id);
      interfaceInheritsFrom.add(inheritsFromId);
    }
  }

  @Override
  protected Log getLogger() {
    return log;
  }

}
