/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi;

import dk.tigerteam.trimm.mdsd.meta.*;
import org.apache.commons.logging.Log;
import org.jdom2.Namespace;

import java.util.*;

/**
 * Convenience implementation of {@link XmiReader} which supports all method related to storing and fetching
 * {@link XmiElementParser} and {@link MetaTypeResolver}'
 *
 * @author Jeppe Cramon
 */
public abstract class AbstractXmiReader implements XmiReader {
    private Namespace umlNs;
    private Namespace xmiNs;

    private List<XmiElementParser> xmiElementParsers = new ArrayList<XmiElementParser>();
    private List<MetaTypeResolver<?>> metaTypeResolvers = new ArrayList<MetaTypeResolver<?>>();

    /**
     * Set the UML and XMI namespaces to use during parsing.<p/>
     * Note: Namespace <u>prefix</u> <strong>SHOULD</strong> be <strong>uml</strong><br/>
     * Note: Namespace <u>prefix</u> <strong>SHOULD</strong> be <strong>xmi</strong><p/>
     *
     * @param umlNs The UML namespace to use during reading/parsing/xpath
     * @param xmiNs The XMI namespace to use during reading/parsing/xpath
     */
    protected AbstractXmiReader(Namespace umlNs, Namespace xmiNs) {
        assert (umlNs != null);
        assert (xmiNs != null);
        this.umlNs = umlNs;
        this.xmiNs = xmiNs;
    }

    @Override
    public Namespace getUmlNs() {
        return umlNs;
    }

    @Override
    public Namespace getXmiNs() {
        return xmiNs;
    }

    public AbstractXmiReader registerXmiElementParser(XmiElementParser... xmiElementParserArray) {
        for (XmiElementParser xmiElementParser : xmiElementParserArray) {
            xmiElementParser.setXmiReader(this);
            xmiElementParsers.add(xmiElementParser);
        }
        return this;
    }

    public List<XmiElementParser> getXmiElementParsers() {
        return Collections.unmodifiableList(xmiElementParsers);
    }

    public AbstractXmiReader setXmiElementParsers(List<XmiElementParser> xmiElementParsers) {
        this.xmiElementParsers.clear();
        registerXmiElementParser(xmiElementParsers.toArray(new XmiElementParser[0]));
        return this;
    }

    public AbstractXmiReader registerMetaTypeResolvers(MetaTypeResolver<?>... typeResolvers) {
        for (MetaTypeResolver<?> metaTypeResolver : typeResolvers) {
            metaTypeResolvers.add(metaTypeResolver);
        }
        return this;
    }

    public List<MetaTypeResolver<?>> getMetaTypeResolvers() {
        return metaTypeResolvers;
    }

    public AbstractXmiReader setMetaTypeResolvers(List<MetaTypeResolver<?>> metaTypeResolvers) {
        this.metaTypeResolvers = metaTypeResolvers;
        return this;
    }

    protected boolean recursivelyResolveMetaTypes(MetaType metaType, MetaModel model) {
        MetaTypeResolver<?> modelTypeResolver = getMetaTypeResolver(metaType);
        if (modelTypeResolver != null) {
            boolean restartChildLoop = modelTypeResolver.resolve(metaType, model);
            if (restartChildLoop)
                return true;
        }
        for (MetaType childType : metaType.getChildren()) {
            boolean restartChildLoop = recursivelyResolveMetaTypes(childType, model);
            if (restartChildLoop)
                return true;
        }
        return false;
    }

    protected List<XmiElementParser> getXmiElementParser(XmiElement xmiElement, MetaType parent) {
        List<XmiElementParser> result = new LinkedList<XmiElementParser>();
        for (XmiElementParser xmiElementParser : xmiElementParsers) {
            // TODO: Possibly support specialization by inheritance (so we can support more XmiElementParsers
            // which supports the same XmiElement. The most specific (the deepest subclass) in the inheritance hierarchy wins
            if (xmiElementParser.supports(xmiElement, parent)) {
                result.add(xmiElementParser);
            }
        }
        return result;
    }

    protected MetaTypeResolver<?> getMetaTypeResolver(MetaType metaType) {
        for (MetaTypeResolver<?> typeResolver : metaTypeResolvers) {
            if (typeResolver.supports(metaType)) {
                return typeResolver;
            }
        }
        return null;
    }

    /**
     * Fix MetaAssociationClazzes so that they are put back into the associations between the two clazzes associations - Should perhaps be
     * moved out of the XmiReader and into the JavaGenerator Given a two classes A and C with a association between them, and associationclazz
     * called D attached to the association, then the MetaAssociation will be between A and C. It must be replaced by an Association between A
     * and B and one between B and C.
     * Note:  The association class adds an extra constraint, in that there can be only one instance of the association class between any two participating objects.
     * <p/>
     * Note: This is typically done during the 2ndParse phase
     * <p/>
     * <pre>
     * ---------                ---------
     * |       |Role A   Role C |       |
     * |   A   |________________|   C   |
     * |       | 2..*  |      * |       |
     * ---------       |        ---------
     *                 |
     *             ---------
     *             |       |
     *             |   B   |
     *             |       |
     *             ---------
     *
     *             becomes...
     *
     * ---------                ---------                ---------
     * |       |Role A   Role B |       |Role B   Role C |       |
     * |   A   |________________|   B   |________________|   C   |
     * |       | 1            * |       | 2..*         1 |       |
     * ---------                ---------                ---------
     * </pre>
     *
     * @param model our meta model
     */
    protected void resolveAssociationClazzesAssociations(MetaModel model) {
        List<MetaAssociationClazz> allAssociationClazzes = model.findAllChildrenOfType(MetaAssociationClazz.class);
        for (MetaAssociationClazz metaAssociationClazz : allAssociationClazzes) {
            // Find the matching MetaAssociation
            MetaAssociation metaAssociation = model.getByIdAndType(metaAssociationClazz.getId(), MetaAssociation.class);

            // Transfer any internal properties/operations in the MetaAssociation to the MetaAssociationClazz
            for (MetaType metaType : new ArrayList<MetaType>(metaAssociation.getChildren())) {
                if (metaType instanceof MetaProperty || metaType instanceof MetaOperation) {
                    metaAssociationClazz.add(metaType);
                }
            }

            // Grab on to original owners, properties
            MetaProperty origOwnerProperty = metaAssociation.getOwnerProperty();
            MetaProperty origOwnedProperty = metaAssociation.getOwnedProperty();
            if (origOwnedProperty == null || origOwnerProperty == null) {
                getLogger().warn("Ignoring Association with a null property - Association Id: " + metaAssociation.getId());
                continue;
            }
            MetaType origOwnerPropertyOwner = origOwnerProperty.getOwner();
            MetaType metaAssociationOwner = metaAssociation.getOwner();
            MetaType origOwnedPropertyOwner = origOwnedProperty.getOwner();

            // Remove the old MetaAssociation and their MetaProperties
            metaAssociation.setOwner(null);
            origOwnerProperty.getOwner().getChildren().remove(origOwnerProperty);
            origOwnedProperty.getOwner().getChildren().remove(origOwnedProperty);

            // From A to B
            // TODO: Handle aggregation, tagged value and stereotype info
            MetaProperty ownerToAssociationClazzProperty = new MetaProperty(UUID.randomUUID().toString(), true);
            ownerToAssociationClazzProperty.setOwner(origOwnerPropertyOwner);
            ownerToAssociationClazzProperty.setType(metaAssociationClazz);
            ownerToAssociationClazzProperty.setMinCardinality(origOwnerProperty.getMinCardinality());
            ownerToAssociationClazzProperty.setMaxCardinality(origOwnerProperty.getMaxCardinality());

            MetaProperty associationClazzToOwnerProperty = new MetaProperty(UUID.randomUUID().toString(), true);
            associationClazzToOwnerProperty.setName(origOwnedProperty.getName());
            associationClazzToOwnerProperty.setOwner(metaAssociationClazz);

            associationClazzToOwnerProperty.setType((MetaClazz) origOwnerPropertyOwner);
            associationClazzToOwnerProperty.setMinCardinality(0);
            // Hibernate has a
            // tendency to fail
            associationClazzToOwnerProperty.add(new MetaStereoType(MetaProperty.NOT_NULL_STEREOTYPE));
            associationClazzToOwnerProperty.setMaxCardinality(1);

            MetaAssociation origOwnerToAssociationClazzAssociation = new MetaAssociation(UUID.randomUUID().toString(), true);
            origOwnerToAssociationClazzAssociation.setBidirectional(true);

            origOwnerToAssociationClazzAssociation.setOwner(metaAssociationOwner);
            origOwnerToAssociationClazzAssociation.setOwnerProperty(ownerToAssociationClazzProperty);
            origOwnerToAssociationClazzAssociation.setOwnedProperty(associationClazzToOwnerProperty);

            // Handle if A and C is the same class (in which case we would end up with two properties with the same name in the same class)
            if (!origOwnerProperty.getType().equals(origOwnedProperty.getType())) {

                // From B to C
                // TODO: Handle aggregation, tagged value and stereotype info
                MetaProperty ownedToAssociationClazzProperty = new MetaProperty(UUID.randomUUID().toString(), true);
                ownedToAssociationClazzProperty.setOwner(origOwnedPropertyOwner);
                ownedToAssociationClazzProperty.setType(metaAssociationClazz);
                ownedToAssociationClazzProperty.setMinCardinality(origOwnedProperty.getMinCardinality());
                ownedToAssociationClazzProperty.setMaxCardinality(origOwnedProperty.getMaxCardinality());

                MetaProperty associationClazzToOwnedProperty = new MetaProperty(UUID.randomUUID().toString(), true);
                associationClazzToOwnedProperty.setOwner(metaAssociationClazz);
                associationClazzToOwnedProperty.setType((MetaClazz) origOwnedPropertyOwner);
                associationClazzToOwnedProperty.setName(origOwnerProperty.getName());
                associationClazzToOwnedProperty.setMinCardinality(0);

                // Hibernate has a
                // tendency to fail
                associationClazzToOwnedProperty.add(new MetaStereoType(MetaProperty.NOT_NULL_STEREOTYPE));
                associationClazzToOwnedProperty.setMaxCardinality(1);
                MetaAssociation origOwnedToAssociationClazzAssociation = new MetaAssociation(UUID.randomUUID().toString(), true);
                origOwnedToAssociationClazzAssociation.setBidirectional(true);
                origOwnedToAssociationClazzAssociation.setOwner(metaAssociationOwner);
                origOwnedToAssociationClazzAssociation.setOwnerProperty(ownedToAssociationClazzProperty);
                origOwnedToAssociationClazzAssociation.setOwnedProperty(associationClazzToOwnedProperty);
            }
        }
    }

    /**
     * Some UML tools, like Enterprise Architect, sometimes changes the order by which we choose ManyToMany ownership of associations. To
     * remedy this and ensure that the JoinTables, etc. is chosen consistently, we decide the ownership by the hashcode value of the two
     * properties names
     * <br/>
     * Note: This is typically done during the 2ndParse phase
     *
     * @param metaModel The meta model
     */
    protected void fixManyToManyOwnership(MetaModel metaModel) {
        for (MetaClazz metaClazz : metaModel.findAllChildrenOfType(MetaClazz.class)) {
            for (MetaProperty metaProperty : metaClazz.getProperties()) {
                if (metaProperty.isPartInAnAssociation() && metaProperty.getAssociationType() == MetaProperty.AssociationType.ManyToMany
                        && metaProperty.getAssociation().isBidirectional()) {
                    MetaProperty ownerProperty = metaProperty.getAssociation().getOwnerProperty();
                    MetaProperty ownedProperty = metaProperty.getAssociation().getOwnedProperty();
                    if (ownerProperty.getName() != null) {
                        if (ownedProperty.getName() != null) {
                            int ownerHashcode = ownerProperty.getName().hashCode();
                            int ownedHashcode = ownedProperty.getName().hashCode();
                            if (ownerHashcode < ownedHashcode) {
                                // Swap around so that the owner is the one with the largest
                                // hashcode
                                metaProperty.getAssociation().swapOwnerAndOwnedProperties();
                            }
                        }
                    } else {
                        if (ownedProperty.getName() != null) {
                            // Swap around so that the only named property is the owner
                            metaProperty.getAssociation().swapOwnerAndOwnedProperties();
                        } else {
                            // use the Owners clazz names to decide the order
                            int ownerHashcode = ((NameableMetaType) ownerProperty.getOwner()).getName().hashCode();
                            int ownedHashcode = ((NameableMetaType) ownedProperty.getOwner()).getName().hashCode();
                            if (ownerHashcode < ownedHashcode) {
                                // Swap around so that the owner is the one with the largest
                                // hashcode
                                metaProperty.getAssociation().swapOwnerAndOwnedProperties();
                            }
                        }
                    }
                }
            }
        }
    }

    protected void validateModel(MetaModel model) {
        List<MetaAssociation> allMetaAssociations = model.findAllChildrenOfType(MetaAssociation.class);
        for (MetaAssociation metaAssociation : allMetaAssociations) {
            if (metaAssociation.hasAttribute(MetaAssociation.NO_OWNED_ENDS)) {
                MetaProperty property1 = metaAssociation.getOwnerProperty();
                MetaProperty property2 = metaAssociation.getOwnedProperty();
                StringBuilder builder = new StringBuilder();
                builder.append("! MetaAssociation '").append(metaAssociation.getId()).append("' has unspecified directionality. ");
                builder.append("MetaProperty1 '").append(property1).append("' in MetaClazz '").append(property1.getOwnerClazz())
                        .append("' and property2 '").append(property2).append("' in MetaClazz '").append(property1.getOwnerClazz()).append("'");
                getLogger().info(builder.toString());
            }
        }
    }

    protected abstract Log getLogger();

}