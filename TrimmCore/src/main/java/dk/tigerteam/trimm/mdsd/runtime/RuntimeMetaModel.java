/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

import dk.tigerteam.trimm.mdsd.ExtensionClazz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The root object in a {@link RuntimeMetaType} instance hierarchy.
 * @author Jeppe Cramon
 */
public class RuntimeMetaModel extends RuntimeMetaType {
	private static final long serialVersionUID = -1899169853526893804L;
    private Map<Class<?>, RuntimeMetaClazz> metaClazzForClassCache = new HashMap<Class<?>, RuntimeMetaClazz>();

	public RuntimeMetaModel(String id) {
		super(id);
	}

    /**
     * Find the RuntimeMetaClazz which represent a given Java class (uses caching internally for will be very fast
     * for consequtive calls with the same Clazz parameters)
     * @param clazz The Java class for which we want to find the corresponding RuntimeMetaClazz
     * @return The matching RuntimeMetaClass or <code>null</code> if no matching RuntimeMetaClazz is found
     * @throws IllegalStateException if more than 1 mathing RuntimeMetaClazz is found
     */
    public RuntimeMetaClazz findMetaClazzForClass(Class<?> clazz) {
        ExtensionClazz extensionClazz = clazz.getAnnotation(ExtensionClazz.class);
        if (extensionClazz != null) {
            clazz = extensionClazz.baseClazz();
        }

        if (metaClazzForClassCache.containsKey(clazz)) {
            return metaClazzForClassCache.get(clazz);
        }

        final String fqcnToFind = clazz.getName();

        List<RuntimeMetaClazz> classesFound = find(new RuntimeMetaCriteria<RuntimeMetaClazz>(RuntimeMetaClazz.class) {
            public boolean isOk(RuntimeMetaClazz metaClazz) {
                if (metaClazz.getFQCN().equals(fqcnToFind)) {
                    return true;
                } 
                return false;
            }
        });
        if (classesFound.size() == 0) {
            metaClazzForClassCache.put(clazz, null);
            return null;
        } else if (classesFound.size() == 1) {
            metaClazzForClassCache.put(clazz, classesFound.get(0));
            return classesFound.get(0);
        } else {
            throw new IllegalStateException("Found two RuntimeMetaClazz's (" + classesFound + ") which have " +
                    "the same FQCN as the Class (" + clazz.getName() + ") we're looking for!");
        }
    }
}
