/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

/**
 * A {@link MetaCriteria} is used when searching the {@link MetaModel} for element(s) that match the criteria that you've setup using the
 * type supplied to the constructor and the implementation of {@link #isOk(MetaType)}.
 * 
 * @author Jeppe Cramon
 * 
 * @param <T>
 *          The type of {@link MetaType} that the criteria returns (note must match the constructor type)
 */
public abstract class MetaCriteria<T extends MetaType> {
	
	private Class<T> type;

	/**
	 * New criteria instance.
	 * 
	 * @param type
	 *          The type of {@link MetaType} that the criteria returns (note must match the T type).<br/>
	 *          Only {@link MetaType}' that are of type "type" (or a subtype thereof) are considered and fed to {@link #isOk(MetaType)})
	 * @see #getType()
	 */
	public MetaCriteria(Class<T> type) {
		super();
		this.type = type;
	}

	/**
	 * The criteria guard method which determines which elements that match the criteria.<br/>
	 * Only {@link MetaType}' that are of type (or a subtype thereof) as returned by {@link #getType()} are considered and fed to
	 * {@link #isOk(MetaType)})
	 * 
	 * @param element
	 *          The element to test
	 * @return true if the element mathces the criteria, otherwise false
	 * @see #getType()
	 */
	public abstract boolean isOk(T element);

	/**
	 * Only the {@link MetaType}' that are of the same type (or a subtype thereof) as returned by this method are considered and fed to
	 * {@link #isOk(MetaType)})
	 */
	public Class<T> getType() {
		return type;
	}
}
