/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi.ea;

import dk.tigerteam.trimm.mdsd.meta.*;
import dk.tigerteam.trimm.mdsd.meta.MetaParameter.Direction;
import dk.tigerteam.trimm.mdsd.uml2.xmi.*;
import dk.tigerteam.trimm.util.Utils;
import dk.tigerteam.trimm.util.XSVParser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom2.*;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPath;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.*;

/**
 * XMI reader for Enterprise Architect 8.x/9.x/10.x - XMI version 2.1 exports<br/>
 *
 * @author Jeppe Cramon
 */
public class EAXmiReader extends AbstractXmiReader {

    private static final Log logger = LogFactory.getLog(EAXmiReader.class);
    public static final String RESOLVED = "resolved";

    /**
     * XMI reader for Enterprise Architect 8.x/9.x/10.x - XMI version 2.1 exports
     */
    public EAXmiReader() {
        super(Namespace.getNamespace("uml", "http://schema.omg.org/spec/UML/2.1"), Namespace.getNamespace("xmi", "http://schema.omg.org/spec/XMI/2.1"));
        registerXmiElementParser(new PackageXmiElementParser(), new ClazzXmiElementParser(), new PropertyXmiElementParser(),
                new AssociationXmiElementParser(), new EnumerationXmiElementParser(), new OperationXmiElementParser(),
                new ParameterXmiElementParser());
        registerMetaTypeResolvers(new ClazzResolver(), new PropertyResolver(), new ParameterTypeResolver());
    }

    public MetaModel read(File file) {
        if (file == null) {
            throw new IllegalArgumentException("Can't parse from a <null> File");
        }
        SAXBuilder builder = new SAXBuilder();
        logger.info("Reading Enterprise Architect 8.x/9.x/10.x XMI model file : '" + file.getAbsolutePath() + "'");
        try {
            Document doc = builder.build(file);
            return parseDocument(doc);
        } catch (JDOMException e) {
            throw new XmiReaderException(this.getClass().getSimpleName() + " - Failed while parsing Xmi Model file '" + file.getAbsolutePath()
                    + "': " + e.getMessage(), e);
        } catch (IOException e) {
            throw new XmiReaderException(this.getClass().getSimpleName() + " - Failed to read Xmi Model file '" + file.getAbsolutePath() + "': "
                    + e.getMessage(), e);
        } catch (Exception e) {
            throw new XmiReaderException(this.getClass().getSimpleName() + " - Failed while parsing Xmi Model file '" + file.getAbsolutePath()
                    + "': " + e.getMessage(), e);
        } finally {
            logger.info("Done with XMI parsing");
        }
    }

    public MetaModel read(URL url) {
        if (url == null) {
            throw new IllegalArgumentException("Can't parse from a <null> Url");
        }
        SAXBuilder builder = new SAXBuilder();
        logger.info("Reading Enterprise Architect 8.x/9.x/10.x XMI model file : '" + url + "'");
        try {
            Document doc = builder.build(url);
            return parseDocument(doc);
        } catch (JDOMException e) {
            throw new XmiReaderException(this.getClass().getSimpleName() + " - Failed while parsing Xmi Model file '" + url + "': "
                    + e.getMessage(), e);
        } catch (IOException e) {
            throw new XmiReaderException(this.getClass().getSimpleName() + " - Failed to read Xmi Model file '" + url + "': " + e.getMessage(), e);
        } catch (Exception e) {
            throw new XmiReaderException(this.getClass().getSimpleName() + " - Failed while parsing Xmi Model file '" + url + "': "
                    + e.getMessage(), e);
        } finally {
            logger.info("Done with XMI parsing");
        }
    }

    private MetaModel parseDocument(Document doc) throws JDOMException {
        XPath xpath = XPath.newInstance("/xmi:XMI/uml:Model");
        xpath.addNamespace(getXmiNs());
        xpath.addNamespace(getUmlNs());

        XmiElement modelNode = new XmiElement((Element) xpath.selectSingleNode(doc));

        if (modelNode.getWrappedElement() == null) {
            throw new XmiReaderException("Failed to find the uml:Model element, according to XPath query " + xpath);
        }

        MetaModel model = firstPassParse(modelNode);
        secondPassParse(model, doc);
        fixManyToManyOwnership(model);
        return model;
    }

    protected void secondPassParse(MetaModel model, Document document) throws JDOMException {
        logger.info("Resolving MetaType dependencies");
        boolean restartChildLoop = true;
        while (restartChildLoop)
            restartChildLoop = recursivelyResolveMetaTypes(model, model);

        resolveHiddenEATypes(model, document);
        parseTaggedValues(model, document);
        resolveAssociationClazzesAssociations(model);
        validateModel(model);
        resolveRealizations(model, document);
    }

    /**
     * Resolves all realization elements so {@link MetaInterface#getRealizedBy()} and {@link MetaClazz#getRealizations()} gets filled with
     * content<br/>
     * Also translates any previously recorded {@link InterfaceInheritance} into realizations<br/>
     * Example of a realization element:
     * <p/>
     * <pre>
     *  &lt;packagedElement xmi:type="uml:Realization" xmi:id="EAID_DF10B060_FDE6_4219_AC74_1C2EF9753BE7"
     *                     visibility="public" supplier="EAID_6638EC9A_6C1C_49a0_88D5_91C91567FA9D"
     *                     realizingClassifier="EAID_6638EC9A_6C1C_49a0_88D5_91C91567FA9D"
     *                     client="EAID_4AD473B5_854B_4ae7_BABC_BC55BA75203A"/>
     * </pre>
     * <p/>
     * The <i>"supplier"</i> is the <b>interface</b> that the <i>"client"</i> (ie. the <b>class</b>) implements/realizes
     *
     * @param document The JDOM document
     * @throws JDOMException
     */
    @SuppressWarnings("unchecked")
    protected void resolveRealizations(MetaModel model, Document document) throws JDOMException {
        XPath realizationsXPath = XPath.newInstance("//packagedElement[@xmi:type='uml:Realization']");
        realizationsXPath.addNamespace(getXmiNs());
        realizationsXPath.addNamespace(getUmlNs());

        List<Element> realizations = (List<Element>) realizationsXPath.selectNodes(document);
        for (Element realization : realizations) {
            String interfaceId = realization.getAttributeValue("supplier");
            String realizedBy = realization.getAttributeValue("client");
            MetaInterface supplyingInterface = model.getByIdAndType(interfaceId, MetaInterface.class);
            MetaClazz realizingClazz = model.getByIdAndType(realizedBy, MetaClazz.class);
            supplyingInterface.getRealizedBy().add(realizingClazz);
        }

        // resolve previously recorded interface inheritance, that we will translate
        // into realizations
        InterfaceInheritance interfaceInheritance = (InterfaceInheritance) model.getAttribute(InterfaceInheritance.class.getName());
        for (String interfaceWhichInheritsId : interfaceInheritance.getAllInterfacesWhichInheritsFromOthers()) {
            MetaInterface interfaceWhichInherits = model.getByIdAndType(interfaceWhichInheritsId, MetaInterface.class);
            for (String inheritsFromId : interfaceInheritance.getInterfaceInheritsFrom(interfaceWhichInheritsId)) {
                MetaInterface inheritsFromInterface = model.getByIdAndType(inheritsFromId, MetaInterface.class);
                inheritsFromInterface.getRealizedBy().add(interfaceWhichInherits);
            }
        }
    }

    protected void parseTaggedValues(MetaModel model, Document document) throws JDOMException {
        for (MetaPackage metaPackage : model.findAllChildrenOfType(MetaPackage.class)) {
            // Search in XMI extensions for the type information
            XPath metaPackageTagsXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + metaPackage.getId()
                    + "']/tags");
            metaPackageTagsXPath.addNamespace(getXmiNs());
            metaPackageTagsXPath.addNamespace(getUmlNs());

            XmiElement metaPackageTagsElement = new XmiElement((Element) metaPackageTagsXPath.selectSingleNode(document));
            if (metaPackageTagsElement.getWrappedElement() == null) {
                continue;
                //throw new XmiReaderException("Failed to find the tags element, according to XPath query " + metaPackageTagsXPath);
            }
            for (XmiElement singleTagElement : metaPackageTagsElement.getChildrenAsXmiElements()) {

                String value = singleTagElement.getAttributeValue("value") != null ? singleTagElement.getAttributeValue("value").trim() : null;
                // Note: Could be avoided if we read TaggedValues directly from the Stereotype
                if (value != null && value.contains("#NOTES#")) {
                    value = value.substring(0, value.indexOf("#NOTES#"));
                }
                /*
				 * TODO: Why did we allow notes? if (singleTagElement.getAttribute("notes") != null) { value =
				 * singleTagElement.getAttributeValue("notes") != null ? singleTagElement.getAttributeValue("notes").trim() : null; }
				 */
                metaPackage.addTaggedValue(singleTagElement.getAttributeValue("name").trim(), normalizeTaggedValue(value));
            }
        }
        for (MetaClazz metaClazz : model.findAllChildrenOfType(MetaClazz.class)) {
            // Search in XMI extensions for the type information
            XPath metaClazzTagsXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + metaClazz.getId() + "']/tags");
            metaClazzTagsXPath.addNamespace(getXmiNs());
            metaClazzTagsXPath.addNamespace(getUmlNs());

            XmiElement metaClazzTagsElement = new XmiElement((Element) metaClazzTagsXPath.selectSingleNode(document));
            if (metaClazzTagsElement.getWrappedElement() == null) {
                throw new XmiReaderException("Failed to find the tags element, according to XPath query " + metaClazzTagsXPath);
            }
            for (XmiElement singleTagElement : metaClazzTagsElement.getChildrenAsXmiElements()) {

                String value = singleTagElement.getAttributeValue("value") != null ? singleTagElement.getAttributeValue("value").trim() : null;
                // Note: Could be avoided if we read TaggedValues directly from the Stereotype
                if (value != null && value.contains("#NOTES#")) {
                    value = value.substring(0, value.indexOf("#NOTES#"));
                }
				/*
				 * TODO: Why did we allow Notes earlier - EA will provide notes for Tagged Value Types the are Enums... so this fails! if
				 * (singleTagElement.getAttribute("notes") != null) { value = singleTagElement.getAttributeValue("notes") != null ?
				 * singleTagElement.getAttributeValue("notes").trim() : null; }
				 */
                metaClazz.addTaggedValue(singleTagElement.getAttributeValue("name").trim(), normalizeTaggedValue(value));
            }

            // Parse Tagged values for operations
            for (MetaOperation metaOperation : metaClazz.getOperations()) {
                XPath metaOperationTagsXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element/operations/operation[@xmi:idref='"
                        + metaOperation.getId() + "']/tags");
                metaOperationTagsXPath.addNamespace(getXmiNs());
                metaOperationTagsXPath.addNamespace(getUmlNs());

                XmiElement metaOperationTagsElement = new XmiElement((Element) metaOperationTagsXPath.selectSingleNode(metaClazzTagsElement
                        .getParent()));
                if (metaOperationTagsElement.getWrappedElement() != null) {
                    for (XmiElement singleTagElement : metaOperationTagsElement.getChildrenAsXmiElements()) {
                        if (singleTagElement.getAttributes().size() > 0) {
                            String name = singleTagElement.getAttributeValue("name");

                            String value = singleTagElement.getAttributeValue("value") != null ? singleTagElement.getAttributeValue("value").trim()
                                    : null;
                            // Note: Could be avoided if we read TaggedValues directly from the Stereotype
                            if (value != null && value.contains("#NOTES#")) {
                                value = value.substring(0, value.indexOf("#NOTES#"));
                            }
							/*
							 * TODO: Why did we allow notes?if (singleTagElement.getAttribute("notes") != null) { value =
							 * singleTagElement.getAttributeValue("notes") != null ? singleTagElement.getAttributeValue("notes").trim() : null; }
							 */
                            if (name == null) {
                                throw new XmiReaderException("Found null name for tagged value (id: " + metaOperationTagsElement.getXmiId(getXmiNs())
                                        + ") belonging to " + metaOperation);
                            }
                            metaOperation.addTaggedValue(name.trim(), normalizeTaggedValue(value));
                        }
                    }
                }
            }

            // Parse tagged values for attributes
            for (MetaProperty metaProperty : metaClazz.getProperties()) {
                XPath metaPropertyTagsXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element/attributes/attribute[@xmi:idref='"
                        + metaProperty.getId() + "']/tags");
                metaPropertyTagsXPath.addNamespace(getXmiNs());
                metaPropertyTagsXPath.addNamespace(getUmlNs());

                XmiElement metaPropertyTagsElement = new XmiElement((Element) metaPropertyTagsXPath.selectSingleNode(metaClazzTagsElement
                        .getParent()));
                if (metaPropertyTagsElement.getWrappedElement() != null) {
                    for (XmiElement singleTagElement : metaPropertyTagsElement.getChildrenAsXmiElements()) {
                        if (singleTagElement.getAttributes().size() > 0) {
                            String name = singleTagElement.getAttributeValue("name");

                            String value = singleTagElement.getAttributeValue("value") != null ? singleTagElement.getAttributeValue("value").trim()
                                    : null;
                            // Note: Could be avoided if we read TaggedValues directly from the Stereotype
                            if (value != null && value.contains("#NOTES#")) {
                                value = value.substring(0, value.indexOf("#NOTES#"));
                            }
							/*
							 * TODO: Why did we allow notes? if (singleTagElement.getAttribute("notes") != null) { value =
							 * singleTagElement.getAttributeValue("notes") != null ? singleTagElement.getAttributeValue("notes").trim() : null; }
							 */
                            if (name == null) {
                                throw new XmiReaderException("Found null name for tagged value (id: " + metaPropertyTagsElement.getXmiId(getXmiNs())
                                        + ") belonging to " + metaProperty);
                            }
                            metaProperty.addTaggedValue(name.trim(), normalizeTaggedValue(value));
                        }
                    }
                }

                if (metaProperty.isPartInAnAssociation()) {
                    MetaAssociation association = metaProperty.getAssociation();

                    // Read the properties tagged value
                    XPath associationXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/connectors/connector[@xmi:idref='" + association.getId()
                            + "']");
                    associationXPath.addNamespace(getXmiNs());
                    associationXPath.addNamespace(getUmlNs());

                    Element connectorElement = (Element) associationXPath.selectSingleNode(metaClazzTagsElement
                            .getParent());
                    if (connectorElement != null && metaProperty.getOwner() instanceof MetaClazz) {
                        Element connectorChild = null;
                        if (connectorElement.getChild("source").getAttributeValue("idref", getXmiNs()).equals(metaProperty.getOwnerClazz().getId())) {
                            connectorChild = connectorElement.getChild("target");
                        } else {
                            connectorChild = connectorElement.getChild("source");
                        }
                        if (connectorChild != null) {
                            for (Element singleTagElement : (List<Element>) connectorChild.getChild("tags").getChildren()) {
                                String name = singleTagElement.getAttributeValue("name");

                                String value = singleTagElement.getAttributeValue("value") != null ? singleTagElement.getAttributeValue("value").trim()
                                        : null;
                                // Note: Could be avoided if we read TaggedValues directly from the Stereotype
                                if (value != null && value.contains("#NOTES#")) {
                                    value = value.substring(0, value.indexOf("#NOTES#"));
                                }
                                metaProperty.addTaggedValue(name.trim(), normalizeTaggedValue(value));
                            }
                        }
                    }

                    // Read the associations Tagged Value -  get's read twice but we will survive (from both sides of the association) ;)
                    XPath associationTagsXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/connectors/connector[@xmi:idref='" + association.getId()
                            + "']/tags");
                    associationTagsXPath.addNamespace(getXmiNs());
                    associationTagsXPath.addNamespace(getUmlNs());

                    XmiElement associationTagsElement = new XmiElement((Element) associationTagsXPath.selectSingleNode(metaClazzTagsElement
                            .getParent()));
                    if (associationTagsElement.getWrappedElement() != null) {
                        for (XmiElement singleTagElement : associationTagsElement.getChildrenAsXmiElements()) {
                            String name = singleTagElement.getAttributeValue("name");

                            String value = singleTagElement.getAttributeValue("value") != null ? singleTagElement.getAttributeValue("value").trim()
                                    : null;
                            // Note: Could be avoided if we read TaggedValues directly from the Stereotype
                            if (value != null && value.contains("#NOTES#")) {
                                value = value.substring(0, value.indexOf("#NOTES#"));
                            }
                            association.addTaggedValue(name.trim(), normalizeTaggedValue(value));
                        }
                    }
                }
            }
        }
    }

    private String normalizeTaggedValue(String value) {
        if (value == null) return null;
        return value.replace("&amp;", "&");
    }

    protected MetaModel firstPassParse(XmiElement modelNode) throws JDOMException {
        logger.info("Parsing XMI");
        MetaModel model = new MetaModel("root");
        // Intialize Interface inheritance data collector
        model.getAttributes().put(InterfaceInheritance.class.getName(), new InterfaceInheritance());

        for (XmiElement xmiElement : modelNode.getChildrenAsXmiElements()) {
            if (xmiElement.getName().equals("packagedElement")) {
                recursivelyFirstPassParseElement(xmiElement, model);
            }
        }

        parseStereoTypes(model, modelNode);
        return model;
    }

    /**
     * resolveHiddenEATypes
     *
     * @param model    the metamodel
     * @param document the XML document
     * @throws JDOMException
     */
    protected void resolveHiddenEATypes(MetaModel model, Document document) throws JDOMException {
        logger.info("Resolving hidden EA property types");
        List<MetaProperty> allProperties = model.findAllChildrenOfType(MetaProperty.class);
        for (MetaProperty metaProperty : allProperties) {
            if (metaProperty.getType() == null) {
                // Search in XMI extensions for the type information
                XPath xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element/attributes/attribute[@xmi:idref='" + metaProperty.getId()
                        + "']/properties");
                xpath.addNamespace(getXmiNs());
                xpath.addNamespace(getUmlNs());

                XmiElement attributeElement = new XmiElement((Element) xpath.selectSingleNode(document));
                if (attributeElement.getWrappedElement() == null) {
                    throw new XmiReaderException("Failed to find the properties element, according to XPath query " + xpath);
                }
                String builtInType = attributeElement.getAttributeValue("type");
                if (builtInType != null) {
                    // log.info("BuiltIn Type: " + builtInType + " for property " +
                    // metaProperty.getId());
                    MetaClazz metaClazz = new MetaClazz(UUID.randomUUID().toString());
                    metaClazz.setBuiltInType(true);
                    // This is a bit of a hack, as
                    // PropertyXmiElementParser.handlePropertyTypeWithPossibleBounds
                    // return the type name (for our hidden built in types)
                    // but also updates the bounds properties of our metaProperty...
                    metaClazz.setName(handlePropertyTypeWithPossibleBounds(metaProperty, builtInType));
                    metaProperty.setType(metaClazz);
                } else {
                    // Sometimes types for <<Enum>> entries are null, but in reality they
                    // are of type "int"
                    if (metaProperty.getOwnerClazz().isEnumerationClazz()) {
                        // log.info("BuiltIn Type: (resolved) int for enum property " +
                        // metaProperty.getId());
                        MetaClazz metaClazz = new MetaClazz(UUID.randomUUID().toString());
                        metaClazz.setBuiltInType(true).setName("int");
                        metaProperty.setType(metaClazz);
                    } else {
                        System.err.println("UNKNOWN BuiltIn Type: " + builtInType + " for property " + metaProperty.getId());
                    }
                }
            }
        }
    }

    /**
     * Parses all &lt;uml:Model> subelements which are not of type &lt;packagedElement> and not in the uml namespace but has an attribute
     * named "base_???".
     *
     * @param model     The model node (used for searching the hierarchy)
     * @param modelNode The &lt;uml:Model> XmiElement node
     */
    @SuppressWarnings("unchecked")
    protected void parseStereoTypes(MetaModel model, XmiElement modelNode) {
        logger.info("Resolving StereoTypes");
        for (XmiElement xmiElement : modelNode.getChildrenAsXmiElements()) {
            if (!"packagedElement".equals(xmiElement.getName()) && !xmiElement.getNamespace().equals(getUmlNs())) {
                for (Attribute attribute : (List<Attribute>) xmiElement.getAttributes()) {
                    if (attribute.getName().startsWith("base_")) {
                        String stereoTypeTargetId = attribute.getValue();
                        String stereoTypeName = xmiElement.getName();
                        if (ignoredStereotypes(attribute.getName(), xmiElement.getNamespace(), stereoTypeName)) {
                            continue;
                        }
                        try {
                            MetaType targetUmlType = model.getById(stereoTypeTargetId);
                            targetUmlType.add(new MetaStereoType(stereoTypeName));
                        } catch (MetaTypeNotFoundException e) {
                            // TODO: With the current parser implementation we unfortunately can't parse stereotypes placed directly on the association for an Association class (needs to change the parsing scheme to only read EA specific schema)
                            throw new XmiReaderException("Failed while parsing stereotype " + xmiElement + ". For Associations Classes: Make sure the Stereotype isn't placed directly on the Association for an Association Class. Place it on the Association class instead ", e);
                        }
                        break;
                    }
                }
            }
        }
    }

    protected boolean ignoredStereotypes(String attributeName, Namespace stereoTypeNamespace, String stereoTypeName) {
        if (stereoTypeName.equals("property_get") || stereoTypeName.equals("property_set") || stereoTypeName.equals("attribute_name")
                || stereoTypeName.equals("throws")) {
            return true;
        } else if ("BPMN".equals(stereoTypeNamespace.getPrefix())) {
            return true;
        }
        return false;
    }

    protected void recursivelyFirstPassParseElement(XmiElement xmiElement, MetaType parent) {
        List<XmiElementParser> xmiElementParsers = getXmiElementParser(xmiElement, parent);
        MetaType currentParent = parent;
        for (XmiElementParser xmiElementParser : xmiElementParsers) {
            try {
                MetaType newParent = xmiElementParser.parse(xmiElement, currentParent);
                currentParent = newParent != null ? newParent : currentParent;
            } catch (Exception e) {
                e.printStackTrace();
                throw new XmiReaderException("Failed during parsing of element '" + xmiElement + "' (" + xmiElement.getXmiId(getXmiNs()) + "): "
                        + e.getMessage(), e);
            }
        }

        for (XmiElement childElement : xmiElement.getChildrenAsXmiElements()) {
            recursivelyFirstPassParseElement(childElement, currentParent);
        }
    }

    /**
     * Handle bounds (embedded in the type information) parsing, which is sometime used for simple types (String, int, long, etc.).<br/>
     * E.g. <code>String(1, 255)</code> where <code>String</code> is the <b>type</b> and 1 is the <b>lowerbound</b>
     * and 255 is the <b>upperbound</b>. <em>Lower bound is optional, so String(255) resolves to type String and upperbound 255.</em><p/>
     * The property will be updated with the bounds if they're found.<br/>
     * The property type will be stored in the property's attribute bag under {@link MetaProperty#TYPE_ATTR_KEY} for later processing /retrieval
     *
     * @param property  The meta property
     * @param typeValue The meta property's type value declaration
     * @return The type name (e.g. String)
     */
    public static String handlePropertyTypeWithPossibleBounds(MetaProperty property, String typeValue) {
        // Check for type value bounds
        int index = typeValue.indexOf('(');
        if (index == -1) {
            property.getAttributes().put(MetaProperty.TYPE_ATTR_KEY, typeValue);
            return typeValue;
        } else {
            // --- Type ---
            String type = typeValue.substring(0, index).trim();
            property.getAttributes().put(MetaProperty.TYPE_ATTR_KEY, type);

            // --- Bounds ---
            int lastIndex = typeValue.indexOf(')');
            String boundsValue = typeValue.substring(index + 1, lastIndex);
            // Split bounds using a comma, to read upper and lower bounds.
            String[] bounds = boundsValue.split(",");
            if (bounds.length < 1 || bounds.length > 2) {
                throw new XmiReaderException("Property '" + property + "' with type value '" + typeValue
                        + "' has invalid defined bounds. Allowed bound definitions 'Type (lowerBound, upperBound)', 'Type(upperBound)'");
            }
            if (bounds.length == 1) {
                // Upperbound
                String upperBoundValue = bounds[0].trim();
                try {
                    property.setUpperBound(Long.parseLong(upperBoundValue));
                } catch (NumberFormatException e) {
                    throw new XmiReaderException("Invalid upperbound value '" + upperBoundValue + "' for property '" + property
                            + "' with type value '" + typeValue + "'");
                }
            } else if (bounds.length == 2) {
                // LowerBound, UpperBound
                String lowerBoundValue = bounds[0].trim();
                String upperBoundValue = bounds[1].trim();
                try {
                    property.setLowerBound(Long.parseLong(lowerBoundValue));
                } catch (NumberFormatException e) {
                    throw new XmiReaderException("Invalid lowerbound value '" + lowerBoundValue + "' for property '" + property
                            + "' with type value '" + typeValue + "'");
                }
                try {
                    property.setUpperBound(Long.parseLong(upperBoundValue));
                } catch (NumberFormatException e) {
                    throw new XmiReaderException("Invalid upperbound value '" + upperBoundValue + "' for property '" + property
                            + "' with type value '" + typeValue + "'");
                }
            }
            return type;
        }
    }

    // ------------------------------------- EA XMI Element parsers
    // -------------------------------------
    public static class PackageXmiElementParser extends AbstractXmiElementParser {
        public PackageXmiElementParser() {
        }

        public MetaPackage parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
            MetaPackage _package = new MetaPackage(xmiElement.getXmiId(getXmiNs()));
            _package.setName(xmiElement.getUmlName());
            _package.setVisibility(getVisibility(xmiElement));
            _package.setOwner(parent);

            // Alias
            XPath propXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']/properties");
            propXPath.addNamespace(getXmiNs());
            propXPath.addNamespace(getUmlNs());
            Element propertiesElement = (Element) propXPath.selectSingleNode(xmiElement.getDocument());
            if (propertiesElement != null) {
                _package.setAlias(propertiesElement.getAttributeValue("alias"));
            }

            // Namespace root (i.e. Root Package)
            XPath flagsXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']/flags");
            flagsXPath.addNamespace(getXmiNs());
            flagsXPath.addNamespace(getUmlNs());
            Element flagsElement = (Element) flagsXPath.selectSingleNode(xmiElement.getDocument());
            _package.setRootPackage(parent instanceof MetaModel);


            // Documentation
            XPath xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']/modelDocument");
            xpath.addNamespace(getXmiNs());
            xpath.addNamespace(getUmlNs());
            Element modelDocument = (Element) xpath.selectSingleNode(xmiElement.getDocument());
            if (modelDocument != null) {
                _package.setDocumentation(modelDocument.getTextTrim());
            }
            if (_package.getDocumentation() == null || _package.getDocumentation().equals("")) {
                // Alternative documentation in the properties section
                if (propertiesElement != null) {
                    _package.setDocumentation(propertiesElement.getAttributeValue("documentation"));
                }
            }

            return _package;
        }

        public boolean supports(XmiElement xmiElement, MetaType parent) {
            return xmiElement.
                    hasXmiType("uml:Package",
                            getXmiNs()
                    );
        }
    }

    public static class ClazzXmiElementParser extends AbstractXmiElementParser {
        private final Log log = LogFactory.getLog(ClazzXmiElementParser.class);

        public MetaClazz parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
            if (xmiElement.getUmlName() != null && xmiElement.getUmlName().startsWith("$")) {
                return null;
            }
            MetaClazz clazz = null;
            if (xmiElement.hasXmiType("uml:Class", getXmiNs())) {
                clazz = new MetaClazz(xmiElement.getXmiId(getXmiNs()));
            } else if (xmiElement.hasXmiType("uml:AssociationClass", getXmiNs())) {
                clazz = new MetaAssociationClazz(xmiElement.getXmiId(getXmiNs()));
            } else if (xmiElement.hasXmiType("uml:Interface", getXmiNs())) {
                clazz = new MetaInterface(xmiElement.getXmiId(getXmiNs()));
            }
            clazz.setName(xmiElement.getUmlName());
            clazz.setVisibility(getVisibility(xmiElement));
            clazz.setOwner(parent);
            clazz.setAbstract(getAbstract(xmiElement));
            // Check for Generalization element(s)
            List<XmiElement> generalizations = xmiElement.getChildrenAsXmiElements("generalization");
            if (generalizations.size() > 0) {
                if (clazz.isInterface()) {
                    InterfaceInheritance interfaceInheritance = (InterfaceInheritance) parent.getMetaModel().getAttribute(
                            InterfaceInheritance.class.getName());
                    for (XmiElement generalizationElement : generalizations) {
                        interfaceInheritance.addInterfaceInheritance(clazz.getId(), generalizationElement.getAttributeValue("general"));
                    }
                } else {
                    if (generalizations.size() != 1) {
                        throw new XmiReaderException("Class '" + clazz.getName() + " with id '" + clazz.getId()
                                + "' has more than 1 generalization. Clazz multiple inheritance isn't supported - use interfaces instead :-)");
                    }
                    XmiElement generalizationElement = generalizations.get(0);
                    clazz.addAttribute(MetaClazz.GENERALIZATION_ID_ATTR_KEY, generalizationElement.getAttributeValue("general"));
                }
            }

            // Documentation
            XPath xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']/modelDocument");
            xpath.addNamespace(getXmiNs());
            xpath.addNamespace(getUmlNs());
            Element modelDocument = (Element) xpath.selectSingleNode(xmiElement.getDocument());
            if (modelDocument != null) {
                clazz.setDocumentation(modelDocument.getTextTrim());
            }
            if (clazz.getDocumentation() == null || clazz.getDocumentation().equals("")) {
                // Alternative documentation
                xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']/properties");
                xpath.addNamespace(getXmiNs());
                xpath.addNamespace(getUmlNs());
                Element propertiesElement = (Element) xpath.selectSingleNode(xmiElement.getDocument());
                if (propertiesElement != null) {
                    clazz.setDocumentation(propertiesElement.getAttributeValue("documentation"));
                }
            }

            // Alias
            XPath propXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']/properties");
            propXPath.addNamespace(getXmiNs());
            propXPath.addNamespace(getUmlNs());
            Element propertiesElement = (Element) propXPath.selectSingleNode(xmiElement.getDocument());
            clazz.setAlias(propertiesElement.getAttributeValue("alias"));

            log.debug("Parsed MetaClazz '" + clazz + "'");
            return clazz;
        }

        protected boolean getAbstract(XmiElement xmiElement) throws DataConversionException {
            Attribute isAbstract = xmiElement.getAttribute("isAbstract");
            if (isAbstract != null) {
                return isAbstract.getBooleanValue();
            } else {
                return false;
            }
        }

        public boolean supports(XmiElement xmiElement, MetaType parent) {
            try {
                if (xmiElement.hasXmiType("uml:Class", getXmiNs()) || xmiElement.hasXmiType("uml:AssociationClass", getXmiNs()) || xmiElement.hasXmiType("uml:Interface", getXmiNs())) {
                    // Verify that the elements sections has the same XMI type
                    XPath xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']");
                    xpath.addNamespace(getXmiNs());
                    xpath.addNamespace(getUmlNs());
                    XmiElement extensionElement = new XmiElement((Element) xpath.selectSingleNode(xmiElement.getDocument()));
                    return extensionElement.hasXmiType("uml:Class", getXmiNs()) || extensionElement.hasXmiType("uml:AssociationClass", getXmiNs()) || extensionElement.hasXmiType("uml:Interface", getXmiNs());
                } else {
                    return false;
                }
            } catch (Exception e) {
                throw new RuntimeException("Failed to resolve 'supports' for xmi element " + xmiElement.getXmiId(getXmiNs()), e);
            }
        }
    }

    public static class EnumerationXmiElementParser extends AbstractXmiElementParser {
        public MetaEnumeration parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
            MetaEnumeration enumeration = new MetaEnumeration(xmiElement.getXmiId(getXmiNs()));
            enumeration.setName(xmiElement.getUmlName());
            enumeration.setVisibility(getVisibility(xmiElement));
            enumeration.setOwner(parent);

            // Documentation
            XPath xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']/modelDocument");
            xpath.addNamespace(getXmiNs());
            xpath.addNamespace(getUmlNs());
            Element modelDocument = (Element) xpath.selectSingleNode(xmiElement.getDocument());
            if (modelDocument != null) {
                enumeration.setDocumentation(modelDocument.getTextTrim());
            }

            // Alias
            XPath propXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']/properties");
            propXPath.addNamespace(getXmiNs());
            propXPath.addNamespace(getUmlNs());
            Element propertiesElement = (Element) propXPath.selectSingleNode(xmiElement.getDocument());
            enumeration.setAlias(propertiesElement.getAttributeValue("alias"));

            return enumeration;
        }

        public boolean supports(XmiElement xmiElement, MetaType parent) {
            return xmiElement.hasXmiType("uml:Enumeration", getXmiNs());
        }
    }

    public static class PropertyXmiElementParser extends AbstractXmiElementParser {
        public MetaProperty parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
            MetaProperty property = new MetaProperty(xmiElement.getXmiId(getXmiNs()));
            property.setVisibility(getVisibility(xmiElement));
            property.setOwner(parent);
            property.setStatic(xmiElement.isUmlStatic());
            property.setOrdered(xmiElement.isUmlOrdered());
            property.setWriteable(!xmiElement.isUmlReadOnly());

            // Default value
            XmiElement defaultValueElement = xmiElement.getChild("defaultValue");
            if (defaultValueElement != null) {
                property.setDefaultValue(defaultValueElement.getAttributeValue("value"));
            }

            if (property.isStatic() && property.getDefaultValue() == null) {
                throw new XmiReaderException("Property '" + property + "' is marked as Static, but doesn't specify a default/initial value!");
            }

            // Aggregation
            property.setAggregation(getAggregation(xmiElement));

            // Type
            XmiElement typeElement = xmiElement.getChild("type");
            if (typeElement != null) {
                if (typeElement.getXmiIdRef(getXmiNs()) != null) {
                    handlePropertyTypeWithPossibleBounds(property, typeElement.getXmiIdRef(getXmiNs()).trim());
                } else {
                    // Simple type (e.g. when no Language has been set in EA for a given class)?
                    if ((getUmlNs().getPrefix() + ":" + "PrimitiveType").equals(typeElement.getXmiType(getXmiNs()))) {
                        if (typeElement.getAttribute("href") == null) {
                            throw new XmiReaderException(
                                    "Found type child element with XmiType 'PrimitiveType' but no href, while parsing MetaProperty: " + property.toString());
                        }
                        String href = typeElement.getAttributeValue("href");
                        int indexOfHash = href.indexOf('#');
                        if (indexOfHash == -1) {
                            throw new XmiReaderException(
                                    "Found type child element with XmiType 'PrimitiveType' and href BUT no # mark in href, while parsing MetaProperty: "
                                            + property.toString());
                        }
                        String type = href.substring(indexOfHash + 1);
                        MetaClazz typeClazz = new MetaClazz(UUID.randomUUID().toString());
                        typeClazz.setBuiltInType(true).setName(type);
                        property.setType(typeClazz);
                    } else {
                        throw new XmiReaderException(
                                "Found type child element, but no XmiIdRef & the XmiType wasn't 'PrimitiveType', while parsing MetaProperty: "
                                        + property.toString());
                    }
                }
            }
            boolean alreadyHasType = property.getType() != null && property.getType().isBuiltInType();
            String valueTypeValue = (String) property.getAttributes().get(MetaProperty.TYPE_ATTR_KEY);
            boolean isSimpleType = valueTypeValue != null && !valueTypeValue.startsWith("EAID_");
            if (alreadyHasType || isSimpleType) {
                XPath xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + parent.getId() + "']/attributes/attribute[@xmi:idref='" +  xmiElement.getXmiId(getXmiNs()) + "']");
                xpath.addNamespace(getXmiNs());
                xpath.addNamespace(getUmlNs());
                Element attributeElement = (Element) xpath.selectSingleNode(xmiElement.getDocument());
                if (attributeElement == null) {
                    throw new RuntimeException("Couldn't find Extension attribute for the attribute with id " + xmiElement.getXmiId(getXmiNs()) + " in MetaClazz " + parent.toString());
                }
                if (!alreadyHasType) {
                    MetaClazz typeClazz = new MetaClazz(UUID.randomUUID().toString());
                    typeClazz.setBuiltInType(true);
                    property.setType(typeClazz);
                }
                property.getType().setName(attributeElement.getChild("properties").getAttributeValue("type"));
                property.setAttribute(RESOLVED, true);
            }


            // Handle name AFTER we've resolved the type
            property.setName(handlePropertyNameWithPossibleBounds(property, xmiElement.getUmlName()));

            // Check upper and lower values (cardinality)
            XmiElement lowerValue = xmiElement.getChild("lowerValue");
            if (lowerValue != null) {
                property.setMinCardinality(lowerValue.getAttributeValue("value", Integer.class));
            }
            XmiElement upperValue = xmiElement.getChild("upperValue");
            if (upperValue != null) {
                property.setMaxCardinality(upperValue.getAttributeValue("value", Integer.class));
            }

            // Association
            String association = xmiElement.getUmlAssociation();
            if (association != null) {
                property.getAttributes().put(MetaProperty.ASSOCIATION_ATTR_KEY, association);
                // Check the element name to deduce the ownership
                if (xmiElement.getName().equals("ownedEnd")) {
                    property.getAttributes().put(MetaProperty.IS_ASSOCIATION_OWNER_ATTR_KEY, Boolean.FALSE);
                } else {
                    property.getAttributes().put(MetaProperty.IS_ASSOCIATION_OWNER_ATTR_KEY, Boolean.TRUE);
                }

                // Documentation
                XPath associationConnectorXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/connectors/connector[@xmi:idref='" + association
                        + "']");
                associationConnectorXPath.addNamespace(getXmiNs());
                associationConnectorXPath.addNamespace(getUmlNs());
                Element connectorElement = (Element) associationConnectorXPath.selectSingleNode(xmiElement.getDocument());
                // Association Classes don't apply to this rule, hence the null and MetaClazz check
                if (connectorElement != null && property.getOwner() instanceof MetaClazz) {
                    Element documentationElement = null;
                    if (connectorElement.getChild("source").getAttributeValue("idref", getXmiNs()).equals(property.getOwnerClazz().getId())) {
                        documentationElement = connectorElement.getChild("target");
                    } else {
                        documentationElement = connectorElement.getChild("source");
                    }
                    if (documentationElement != null) {
                        property.setDocumentation(documentationElement.getChild("documentation").getAttributeValue("value"));
                    }
                }

            }


            // Documentation
            XPath xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element/attributes/attribute[@xmi:idref='" + property.getId()
                    + "']/documentation");
            xpath.addNamespace(getXmiNs());
            xpath.addNamespace(getUmlNs());
            Element documentationElement = (Element) xpath.selectSingleNode(xmiElement.getDocument());
            if (documentationElement != null) {
                property.setDocumentation(documentationElement.getAttributeValue("value"));
            }

            // Alias
            XPath propXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element/attributes/attribute[@xmi:idref='" + property.getId()
                    + "']/style");
            propXPath.addNamespace(getXmiNs());
            propXPath.addNamespace(getUmlNs());
            Element propertiesElement = (Element) propXPath.selectSingleNode(xmiElement.getDocument());
            if (propertiesElement != null) {
                property.setAlias(propertiesElement.getAttributeValue("value"));
            }

            // Finally, if we're dealing with an enumeration, let's apply the enum
            // stereotype
            if (xmiElement.hasXmiType("uml:EnumerationLiteral", getXmiNs())) {
                property.setEnumerationLiteral(true);
            }

            return property;
        }

        private String handlePropertyNameWithPossibleBounds(MetaProperty property, String propertyName) {
            if (propertyName == null) {
                return null;
            }

            int index = propertyName.indexOf('(');
            if (index == -1) {
                return propertyName;
            } else {
                String realPropertyName = propertyName.substring(0, index).trim();

                // --- Bounds ---
                int lastIndex = propertyName.indexOf(')');
                String boundsValue = propertyName.substring(index + 1, lastIndex);
                // Split bounds using a comma, to read upper and lower bounds.
                String[] bounds = boundsValue.split(",");
                if (bounds.length < 1 || bounds.length > 2) {
                    throw new XmiReaderException("Property '" + property + "' with name '" + propertyName
                            + "' has invalid defined bounds. Allowed bound definitions 'name (lowerBound, upperBound)', 'name(upperBound)'");
                }
                if (bounds.length == 1) {
                    // Upperbound
                    String upperBoundValue = bounds[0].trim();
                    if (upperBoundValue.equalsIgnoreCase("MAX")) {
                        upperBoundValue = resolveMaxUpperBoundValue(property, upperBoundValue);
                    }
                    try {
                        property.setUpperBound(Long.parseLong(upperBoundValue));
                    } catch (NumberFormatException e) {
                        throw new XmiReaderException("Invalid upperbound value '" + upperBoundValue + "' for property '" + property
                                + "' with name '" + propertyName + "'");
                    }
                } else if (bounds.length == 2) {
                    // LowerBound, UpperBound
                    String lowerBoundValue = bounds[0].trim();
                    if (lowerBoundValue.equalsIgnoreCase("MIN")) {
                        lowerBoundValue = resolveMinLowerBoundValue(property, lowerBoundValue);
                    }
                    String upperBoundValue = bounds[1].trim();
                    if (upperBoundValue.equalsIgnoreCase("MAX")) {
                        upperBoundValue = resolveMaxUpperBoundValue(property, upperBoundValue);
                    }
                    try {
                        property.setLowerBound(Long.parseLong(lowerBoundValue));
                    } catch (NumberFormatException e) {
                        throw new XmiReaderException("Invalid lowerbound value '" + lowerBoundValue + "' for property '" + property
                                + "' with name '" + propertyName + "'");
                    }
                    try {
                        property.setUpperBound(Long.parseLong(upperBoundValue));
                    } catch (NumberFormatException e) {
                        throw new XmiReaderException("Invalid upperbound value '" + upperBoundValue + "' for property '" + property
                                + "' with name '" + propertyName + "'");
                    }
                }
                return realPropertyName;
            }
        }

        private String resolveMinLowerBoundValue(MetaProperty property, String lowerBoundValue) {
            String propertyType = property.getAttribute("type").toString().toLowerCase();
            if (propertyType.contains("long"))
                return new Long(Long.MIN_VALUE).toString();
            if (propertyType.contains("int"))
                return new Integer(Integer.MIN_VALUE).toString();
            if (propertyType.contains("float"))
                return new Float(Float.MIN_VALUE).toString();
            if (propertyType.contains("double"))
                return new Double(Double.MIN_VALUE).toString();
            if (propertyType.contains("short"))
                return new Short(Short.MIN_VALUE).toString();
            return lowerBoundValue;
        }

        private String resolveMaxUpperBoundValue(MetaProperty property, String upperBoundValue) {
            String propertyType = property.getAttribute("type").toString().toLowerCase();
            if (propertyType.contains("long"))
                return new Long(Long.MAX_VALUE).toString();
            if (propertyType.contains("int"))
                return new Integer(Integer.MAX_VALUE).toString();
            if (propertyType.contains("float"))
                return new Float(Float.MAX_VALUE).toString();
            if (propertyType.contains("double"))
                return new Double(Double.MAX_VALUE).toString();
            if (propertyType.contains("short"))
                return new Short(Short.MAX_VALUE).toString();
            return upperBoundValue;
        }


        public boolean supports(XmiElement xmiElement, MetaType parent) {
            return xmiElement.hasXmiType("uml:Property", getXmiNs()) || xmiElement.hasXmiType("uml:EnumerationLiteral", getXmiNs());
        }
    }

    /**
     * <pre>
     * ---------                ---------
     * |       |Role A   Role B |       |
     * |   A   |/#\_____________|   B   |
     * |       |\#/             |       |
     * ---------                ---------
     * </pre>
     */
    public static class AssociationXmiElementParser extends AbstractXmiElementParser {
        public MetaAssociation parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
            MetaAssociation association = new MetaAssociation(xmiElement.getXmiId(getXmiNs()));
            association.setName(xmiElement.getUmlName());
            association.setVisibility(getVisibility(xmiElement));
            association.setOwner(parent);

            // Save the memberEnds
            List<XmiElement> memberEnds = xmiElement.getChildrenAsXmiElements("memberEnd");
            if (memberEnds.size() == 0) {
                System.err.println("Ignoring malformed XMI - " + association + "' only has '" + memberEnds.size()
                        + "' memberEnds, expected 2 memberEnds");
                return null;
            } else if (memberEnds.size() == 1) {
                throw new XmiReaderException("Malformed XMI - " + association + "' only has '" + memberEnds.size()
                        + "' memberEnds, expected 2 memberEnds");
            }
            association.getAttributes().put(MetaAssociation.MEMBEREND_DESTINATION_ATTR_KEY, memberEnds.get(0).getXmiIdRef(getXmiNs()));
            association.getAttributes().put(MetaAssociation.MEMBEREND_SOURCE_ATTR_KEY, memberEnds.get(1).getXmiIdRef(getXmiNs()));

            // Check for bidirectionality
            int numberOfOwnedEnds = xmiElement.getChildrenAsXmiElements("ownedEnd").size();
            if (numberOfOwnedEnds == 0 || numberOfOwnedEnds == 2) {
                association.setBidirectional(true);
                if (numberOfOwnedEnds == 2) {
                    association.addAttribute(MetaAssociation.NO_OWNED_ENDS, Boolean.TRUE);
                }
            }

            // Documentation
            XPath associationConnectorXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/connectors/connector[@xmi:idref='" + association.getId()
                    + "']/documentation");
            associationConnectorXPath.addNamespace(getXmiNs());
            associationConnectorXPath.addNamespace(getUmlNs());
            Element connectorDocumentationElement = (Element) associationConnectorXPath.selectSingleNode(xmiElement.getDocument());
            // Association Classes don't apply to the same rule, hence the null check
            if (connectorDocumentationElement != null) {
                association.setDocumentation(connectorDocumentationElement.getAttributeValue("value"));
            }

            if (logger.isDebugEnabled()) {
                logger.debug("Parsed MetaAssociation: " + association);
            }

            return association;
        }

        public boolean supports(XmiElement xmiElement, MetaType parent) {
            return xmiElement.hasXmiType("uml:Association", getXmiNs()) || xmiElement.hasXmiType("uml:AssociationClass", getXmiNs());
        }
    }

    /**
     * &lt;ownedOperation xmi:id="EAID_7B9A2794_E93C_418b_8208_B6D985849451" name="bla" visibility="public" concurrency="sequential">
     * &lt;ownedParameter xmi:id="EAID_722FFBD9_E5D3_4541_B84A_531C9543F9A5" name="param3" type="EAID_3EDF711A_D32A_4172_8A10_B0DA7526DAFB"
     * direction="in"/> &lt;ownedParameter xmi:id="EAID_DFECD69C_43EA_4766_8F43_98F99C361585" name="param2String" direction="in"
     * type="EAJava_String"/> &lt;ownedParameter xmi:id="EAID_4F7EE0D3_9F2F_4b86_9A57_C149C676AF02" name="param1Bool" direction="in"
     * type="EAJava_boolean"/> &lt;ownedParameter xmi:id="EAID_RT000000_E93C_418b_8208_B6D985849451" name="return" direction="return"
     * type="EAJava_void"/> &lt;/ownedOperation>
     */
    public static class OperationXmiElementParser extends AbstractXmiElementParser {
        public MetaType parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
            MetaOperation operation = new MetaOperation(xmiElement.getXmiId(getXmiNs()));
            operation.setName(xmiElement.getUmlName());
            operation.setVisibility(getVisibility(xmiElement));
            operation.setOwner(parent);
            operation.setStatic(xmiElement.isUmlStatic());
            operation.setAbstract(xmiElement.isUmlAbstract());

            XPath xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element/operations/operation[@xmi:idref='" + xmiElement.getXmiId(getXmiNs())
                    + "']");
            xpath.addNamespace(getXmiNs());
            xpath.addNamespace(getUmlNs());
            Element operationElement = (Element) xpath.selectSingleNode(xmiElement.getDocument());
            if (operationElement == null) {
                throw new IllegalStateException("Couldn't find <operation> element for operation " + operation);
            }
            Element behaviour = operationElement.getChild("behaviour");
            if (behaviour != null && behaviour.getAttribute("value") != null) {
                operation.setBehaviour(behaviour.getAttributeValue("value"));
            }

            Element code = operationElement.getChild("code");
            if (code != null && code.getAttribute("value") != null) {
                operation.setCode(code.getAttributeValue("value"));
            }

            String documentationContent = operationElement.getChildTextTrim("documentation");
            if (documentationContent != null && !"".equals(documentationContent.trim())) {
                operation.setDocumentation(documentationContent);
            } else {
                String docAttributeValue = operationElement.getChild("documentation").getAttributeValue("value");
                if (docAttributeValue != null) {
                    operation.setDocumentation(docAttributeValue);
                }
            }

            // Alias
            XPath styleXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element/operations/operation[@xmi:idref='"
                    + operation.getId() + "']/style");
            styleXPath.addNamespace(getXmiNs());
            styleXPath.addNamespace(getUmlNs());
            Element styleElement = (Element) styleXPath.selectSingleNode(xmiElement.getDocument());
            operation.setAlias(styleElement.getAttributeValue("value"));

            return operation;
        }

        public boolean supports(XmiElement xmiElement, MetaType parent) {
            return xmiElement.getName().equals("ownedOperation");
        }
    }

    /**
     * &lt;ownedOperation xmi:id="EAID_7B9A2794_E93C_418b_8208_B6D985849451" name="bla" visibility="public" concurrency="sequential">
     * &lt;ownedParameter xmi:id="EAID_722FFBD9_E5D3_4541_B84A_531C9543F9A5" name="param3" type="EAID_3EDF711A_D32A_4172_8A10_B0DA7526DAFB"
     * direction="in"/> &lt;ownedParameter xmi:id="EAID_DFECD69C_43EA_4766_8F43_98F99C361585" name="param2String" direction="in"
     * type="EAJava_String"/> &lt;ownedParameter xmi:id="EAID_4F7EE0D3_9F2F_4b86_9A57_C149C676AF02" name="param1Bool" direction="in"
     * type="EAJava_boolean"/> &lt;ownedParameter xmi:id="EAID_RT000000_E93C_418b_8208_B6D985849451" name="return" direction="return"
     * type="EAJava_void"/> &lt;/ownedOperation>
     */
    public static class ParameterXmiElementParser extends AbstractXmiElementParser {
        public MetaType parse(XmiElement xmiElement, MetaType parent) throws JDOMException {
            MetaOperation operation = (MetaOperation) parent;
            MetaParameter parameter = new MetaParameter(xmiElement.getXmiId(getXmiNs()));
            parameter.setName(xmiElement.getUmlName());
            parameter.setOwner(operation); // Add as children as well, so we allow the
            // ParameterTypeResolver to resolve types
            // transparently
            // Direction
            String direction = Utils.capitalize(xmiElement.getAttributeValue("direction"));
            if ("Inout".equals(direction)) {
                direction = "InOut";
            }
            parameter.setDirection(Direction.valueOf(direction));
            // Return param
            if (parameter.getDirection() == Direction.Return) {
                operation.setReturnParameter(parameter);
            } else {
                operation.addParameters(parameter);
            }

            // Type
            String type = xmiElement.getAttributeValue("type").trim();
            parameter.getAttributes().put(MetaProperty.TYPE_ATTR_KEY, type);
            boolean isSimpleType = !type.startsWith("EAID_");
            if (isSimpleType && parameter.getDirection() != Direction.Return) {
                XPath xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + ((MetaOperation) parent).getOwnerClazz().getId() + "']/operations/operation[@xmi:idref='" +  parent.getId() + "']/parameters/parameter[@xmi:idref='" +  xmiElement.getXmiId(getXmiNs()) + "']");
                xpath.addNamespace(getXmiNs());
                xpath.addNamespace(getUmlNs());
                Element parameterElement = (Element) xpath.selectSingleNode(xmiElement.getDocument());
                if (parameterElement == null) {
                    throw new RuntimeException("Couldn't find Extension element for the parameter with id " + xmiElement.getXmiId(getXmiNs()) + " in Operation " + parent.toString());
                }
                MetaClazz typeClazz = new MetaClazz(UUID.randomUUID().toString());
                typeClazz.setBuiltInType(true);
                typeClazz.setName(parameterElement.getChild("properties").getAttributeValue("type"));
                parameter.setType(typeClazz);
                parameter.setAttribute(RESOLVED, Boolean.TRUE);
            } else if (isSimpleType && parameter.getDirection() == Direction.Return) {
                XPath xpath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element[@xmi:idref='" + ((MetaOperation) parent).getOwnerClazz().getId() + "']/operations/operation[@xmi:idref='" +  parent.getId() + "']/parameters/parameter[starts-with(@xmi:idref, 'EAID_RETURNID_')]");
                xpath.addNamespace(getXmiNs());
                xpath.addNamespace(getUmlNs());
                Element parameterElement = (Element) xpath.selectSingleNode(xmiElement.getDocument());
                if (parameterElement == null) {
                    throw new RuntimeException("Couldn't find Extension element for the parameter which starts with id EAID_RETURNID_ in Operation " + parent.toString());
                }
                MetaClazz typeClazz = new MetaClazz(UUID.randomUUID().toString());
                typeClazz.setBuiltInType(true);
                typeClazz.setName(parameterElement.getChild("properties").getAttributeValue("type"));
                parameter.setType(typeClazz);
                parameter.setAttribute(RESOLVED, Boolean.TRUE);
            }

            // Cardinality
            if (xmiElement.getChild("lowerValue") != null) {
                parameter.setMinCardinality(Integer.parseInt(xmiElement.getChild("lowerValue").getAttributeValue("value")));
            }
            if (xmiElement.getChild("upperValue") != null) {
                parameter.setMaxCardinality(Integer.parseInt(xmiElement.getChild("upperValue").getAttributeValue("value")));
            }

            // Alias
            if (parameter.getDirection() != Direction.Return) {
                XPath styleexXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element/operations/operation[@xmi:idref='"
                        + operation.getId() + "']/parameters/parameter[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']/styleex");
                styleexXPath.addNamespace(getXmiNs());
                styleexXPath.addNamespace(getUmlNs());
                Element styleexElement = (Element) styleexXPath.selectSingleNode(xmiElement.getDocument());
                String styleexValue = styleexElement.getAttributeValue("value");
                parameter.setAlias(XSVParser.parseKeyValueToFlatMap(styleexValue, ";", "=").get("alias"));
            }

            // Documentation
            XPath documentationXPath = XPath.newInstance("/xmi:XMI/xmi:Extension/elements/element/operations/operation[@xmi:idref='"
                    + operation.getId() + "']/parameters/parameter[@xmi:idref='" + xmiElement.getXmiId(getXmiNs()) + "']/documentation");
            documentationXPath.addNamespace(getXmiNs());
            documentationXPath.addNamespace(getUmlNs());
            Element documentationElement = (Element) documentationXPath.selectSingleNode(xmiElement.getDocument());
            if (documentationElement != null) {
                String documentation = documentationElement.getAttributeValue("value");
                parameter.setDocumentation(documentation);
            }

            return parameter;
        }

        public boolean supports(XmiElement xmiElement, MetaType parent) {
            return xmiElement.getName().equals("ownedParameter");
        }
    }

    // ------------------------------ EA UmlTypeResolvers
    // -------------------------------
    public static class ClazzResolver implements MetaTypeResolver<MetaClazz> {
        public boolean resolve(MetaType metaType, MetaModel model) {
            MetaClazz clazz = (MetaClazz) metaType;
            if (clazz.hasAttribute(MetaClazz.GENERALIZATION_ID_ATTR_KEY)) {
                MetaClazz superClazz = (MetaClazz) model.getById((String) clazz.getAttribute(MetaClazz.GENERALIZATION_ID_ATTR_KEY));
                clazz.setSuperClazz(superClazz);
            }
            return false;
        }

        public boolean supports(MetaType metaType) {
            return metaType.getClass().equals(MetaClazz.class);
        }
    }

    public static class ParameterTypeResolver implements MetaTypeResolver<MetaParameter> {
        public boolean resolve(MetaType metaType, MetaModel model) {

            MetaParameter parameter = (MetaParameter) metaType;
            if (parameter.getAttributes().get(RESOLVED) != null) return false;

            if (parameter.hasAttribute(MetaProperty.TYPE_ATTR_KEY)) {
                String typeRefId = (String) parameter.getAttribute(MetaProperty.TYPE_ATTR_KEY);
                MetaClazz typeClazz = null;
                if (typeRefId.startsWith("EAJava_") || typeRefId.startsWith("EAnone_")) {
                    typeClazz = new MetaClazz(UUID.randomUUID().toString());
                    typeClazz.setBuiltInType(true).setName(typeRefId.substring("EAJava_".length()));
                } else {
                    typeClazz = (MetaClazz) model.getByIdAndType(typeRefId, MetaClazz.class);
                }
                parameter.setType(typeClazz);
            }
            parameter.getAttributes().put(RESOLVED, Boolean.TRUE);
            return false;
        }

        public boolean supports(MetaType metaType) {
            return metaType instanceof MetaParameter;
        }
    }

    public static class PropertyResolver implements MetaTypeResolver<MetaProperty> {
        public boolean resolve(MetaType metaType, MetaModel model) {
            MetaProperty property = (MetaProperty) metaType;
            if (property.getAttribute(RESOLVED) != null) {
                return false;
            }

            if (property.hasAttribute(MetaProperty.ASSOCIATION_ATTR_KEY)) {
                MetaAssociation association = (MetaAssociation) model.getByIdAndType(
                        (String) property.getAttribute(MetaProperty.ASSOCIATION_ATTR_KEY), MetaAssociation.class);

                // Fix ownership
                if (property.getOwner() == association) {
                    // Move the ownership to the MetaClazz, which should home the property
                    String oppositePropertyId = null;
                    if (property.getId().equals(association.getAttribute(MetaAssociation.MEMBEREND_DESTINATION_ATTR_KEY))) {
                        oppositePropertyId = (String) association.getAttribute(MetaAssociation.MEMBEREND_SOURCE_ATTR_KEY);
                    } else {
                        oppositePropertyId = (String) association.getAttribute(MetaAssociation.MEMBEREND_DESTINATION_ATTR_KEY);
                    }
                    MetaProperty oppositeProperty = (MetaProperty) model.getById(oppositePropertyId);
                    MetaClazz oppositePropertyType = (MetaClazz) model.getByIdAndType(
                            (String) oppositeProperty.getAttribute(MetaProperty.TYPE_ATTR_KEY), MetaClazz.class);
                    if (oppositePropertyType == null) {
                        throw new IllegalStateException("Failed to find type for property with id '" + oppositeProperty.getId() + "'");
                    }
                    property.setOwner(oppositePropertyType);
                    return true;
                }

                if (association.isBidirectional()) {
                    if (association.getOwnerProperty() == null || association.getOwnerProperty() == property) {
                        association.setOwnerProperty(property);
                    } else {
                        association.setOwnedProperty(property);
                    }
                } else {
                    boolean isOwner = true;
                    if (property.hasAttribute(MetaProperty.IS_ASSOCIATION_OWNER_ATTR_KEY)) {
                        isOwner = (Boolean) property.getAttribute(MetaProperty.IS_ASSOCIATION_OWNER_ATTR_KEY);
                    }
                    // TODO: What about wrongly placed ownership in relation to shared and composition?
                    if (isOwner) {
                        // Sanity check
                        if (association.getOwnerProperty() != null && association.getOwnerProperty() != property) {
                            throw new XmiReaderException("Model validation error. Association '" + association.getId() + "' already has owner property '"
                                    + association.getOwnerProperty() + "', but property '" + property.getId()
                                    + "' is also marked as owner of the association");
                        }
                        property.setAsOwnerOfAssociation(association);
                    } else {
                        // Sanity check
                        if (association.getOwnedProperty() != null && association.getOwnedProperty() != property) {
                            throw new XmiReaderException("Model validation error. Association '" + association.getId() + "' already has owned property '"
                                    + association.getOwnedProperty().getId() + "', but property '" + property.getId()
                                    + "' is also marked as owned by the association");
                        }
                        property.setAsOwnedByAssociation(association);
                    }
                }
            }
            if (property.hasAttribute(MetaProperty.TYPE_ATTR_KEY)) {
                String typeRefId = (String) property.getAttribute(MetaProperty.TYPE_ATTR_KEY);
                MetaClazz typeClazz = null;
                if (typeRefId.startsWith("EAJava_") || typeRefId.startsWith("EAnone_")) {
                    typeClazz = new MetaClazz(UUID.randomUUID().toString());
                    typeClazz.setBuiltInType(true).setName(typeRefId.substring("EAJava_".length()));
                } else {
                    typeClazz = (MetaClazz) model.getByIdAndType(typeRefId, MetaClazz.class);
                }
                property.setType(typeClazz);
            }


            property.getAttributes().put(RESOLVED, Boolean.TRUE);
            return false;
        }

        public boolean supports(MetaType metaType) {
            return metaType.getClass().equals(MetaProperty.class);
        }
    }

    // ------------------------------------------------------------

    /**
     * Collects all interface inheritances which are translated into realizations during
     * {@link EAXmiReader#resolveRealizations(MetaModel, Document)}<br/>
     * Is stored in the {@link MetaModel}'s attribute map under {@link InterfaceInheritance} FQCN.
     */
    public static class InterfaceInheritance implements Serializable {
        private Map<String, Set<String>> inheritsFrom = new HashMap<String, Set<String>>();

        /**
         * Get all the interfaces that this interface inherits from
         *
         * @param id The id of the interface
         * @return The interfaces this interface inherits from (empty set if there are no realizations)
         */
        public Set<String> getInterfaceInheritsFrom(String id) {
            Set<String> interfaceInheritsFrom = inheritsFrom.get(id);
            if (interfaceInheritsFrom == null) {
                interfaceInheritsFrom = new HashSet<String>();
                inheritsFrom.put(id, interfaceInheritsFrom);
            }
            return interfaceInheritsFrom;
        }

        public Set<String> getAllInterfacesWhichInheritsFromOthers() {
            return inheritsFrom.keySet();
        }

        /**
         * Add an interface that this interface inherits from
         *
         * @param id             The id of the interface that inherits from the "inheritsFrom" interface
         * @param inheritsFromId The id of the interface that the interface inherits from
         */
        public void addInterfaceInheritance(String id, String inheritsFromId) {
            Set<String> interfaceInheritsFrom = getInterfaceInheritsFrom(id);
            interfaceInheritsFrom.add(inheritsFromId);
        }
    }

    @Override
    protected Log getLogger() {
        return logger;
    }
}
