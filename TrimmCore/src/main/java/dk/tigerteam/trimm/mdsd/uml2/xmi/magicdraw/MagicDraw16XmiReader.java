/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.uml2.xmi.magicdraw;

import org.jdom2.Namespace;

/**
 * MagicDraw 16.6 XmiReader (might support 16.7, 16.8 and 16.9 but this is untested)
 */
public class MagicDraw16XmiReader extends MagicDrawXmiReader {
    /**
     * XMI reader for MagicDraw version 16.6 - XMI version 2.1 native project files (.mdzip or .mdxml)
     */
    public MagicDraw16XmiReader() {
        super(Namespace.getNamespace("uml", "http://schema.omg.org/spec/UML/2.2"), Namespace.getNamespace("xmi", "http://schema.omg.org/spec/XMI/2.1"));
    }
}
