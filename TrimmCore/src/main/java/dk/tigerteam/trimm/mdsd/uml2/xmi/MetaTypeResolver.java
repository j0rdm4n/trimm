/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaType;

import java.util.ConcurrentModificationException;

/**
 * A {@link MetaTypeResolver} is responsible for reconstructing the MetaModel with dependencies. The Xmi parsing in {@link XmiReader}
 * typically happens in two phases. One where the core {@link MetaType} are parsed to the extent that's possible (for instance it isn't
 * possible to set the {@link MetaClazz} another {@link MetaClazz} extends from, if the super {@link MetaClazz} hasn't been parsed yet).
 * During second phase of parsing the {@link MetaTypeResolver} are responsible for resolving dependencies between MetaTypes. These
 * dependencies are typically stored in the {@link MetaType#getAttributes()} map as symbolic references. The {@link MetaTypeResolver} is
 * will replace this symbolic reference with an in memory dependency between real {@link MetaType}'s.
 * 
 * @author Jeppe Cramon
 * 
 * @param <T>
 *          The type supported by the MetaType
 */
public interface MetaTypeResolver<T extends MetaType> {
	/**
	 * Resolve all dependencies, etc. for this metatype TODO: Use the generic parameter (T) in the {@link #resolve(MetaType, MetaModel)}
	 * method
	 * 
	 * @param metaType
	 *          The meta type to resolve dependencies for
	 * @return true if the child looping to begin again from start, otherwise false and the child loop should continue (needed for when the
	 *         child list is modified during second phase iteration over all parsed MetaTypes). This can typically happen regarding ownership.
	 *         In for instance Enterprise Architect, the ownership of a property lies with the association in which it takes part ( in case it
	 *         takes part in an association). In our {@link MetaModel} this owner ship naturally lies with the {@link MetaClazz} to which
	 *         {@link MetaProperty} belongs. This changes to the overall structure of the {@link MetaModel} requires the iteation is
	 *         restarted, otherwise we will get a {@link ConcurrentModificationException}.
	 */
	boolean resolve(MetaType metaType, MetaModel model);

	/**
	 * Checks if the MetaType is of the same type as specified in the generics parameter (T). TODO: Necessary for now. Replace with reflection
	 * which reads the generics type specified on the class
	 */
	boolean supports(MetaType metaType);
}
