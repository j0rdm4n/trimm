/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents an operation in a UML model
 * @author Jeppe Cramon
 */
public class RuntimeMetaOperation extends RuntimeNameableMetaType {
	
	private static final long serialVersionUID = -2626579553614799571L;
	
	private RuntimeMetaParameter returnParameter;
	private List<RuntimeMetaParameter> parameters = new LinkedList<RuntimeMetaParameter>();
	private String behaviour;
	private String code;
	private boolean _static;
	private boolean _abstract;
	private boolean hasCorrespondingJavaMethod;
	private transient Method javaMethod; // Lazy loaded
	
	public RuntimeMetaOperation(String id, boolean fabricated, String name, boolean hasCorrespondingJavaMethod) {
		super(id, fabricated);
		setName(name);
		this.hasCorrespondingJavaMethod = hasCorrespondingJavaMethod;
	}
	
	public boolean isHasCorrespondingJavaMethod() {
		return hasCorrespondingJavaMethod;
	}
	
	public boolean isStatic() {
		return _static;
	}
	
	public boolean isAbstract() {
		return _abstract;
	}

	public RuntimeMetaOperation setAbstract(boolean _abstract) {
		checkCanModify();
		this._abstract = _abstract;
		return this;
	}

	public RuntimeMetaOperation setStatic(boolean _static) {
		checkCanModify();
		this._static = _static;
		return this;
	}

	public RuntimeMetaParameter getReturnParameter() {
		return returnParameter;
	}

	public RuntimeMetaOperation setReturnParameter(RuntimeMetaParameter returnParameter) {
		checkCanModify();
		this.returnParameter = returnParameter;
		return this;
	}
	
	public List<RuntimeMetaParameter> getParameters() {
		return Collections.unmodifiableList(parameters);
	}

	public RuntimeMetaOperation addParameters(RuntimeMetaParameter...metaParameters) {
		checkCanModify();
		parameters.addAll(Arrays.asList(metaParameters));
		return this;
	}

	public String getBehaviour() {
		return behaviour;
	}

	public RuntimeMetaOperation setBehaviour(String behaviour) {
		checkCanModify();
		this.behaviour = behaviour;
		return this;
	}

	public String getCode() {
		return code;
	}

	public RuntimeMetaOperation setCode(String code) {
		checkCanModify();
		this.code = code;
		return this;
	}
	
	@Override
	public RuntimeMetaClazz getOwner() {
		return (RuntimeMetaClazz) super.getOwner();
	}
	
	/**
	 * Get the Java {@link Method} which corresponds to this {@link RuntimeMetaOperation}.
	 * Check guard method {@link #isHasCorrespondingJavaMethod()} before calling as this method just returns null if there's
	 * no corresponding java method.
	 */
	public Method getJavaMethod() {
		if (hasCorrespondingJavaMethod) {
			if (javaMethod == null) {
				List<Class<?>> parameters = new LinkedList<Class<?>>();
				for (RuntimeMetaParameter metaParameter : getParameters()) {
					parameters.add(metaParameter.getType().getJavaClass());
				}
				
				Class<?> ownerClass = getOwner().getJavaClass();
				
				try {
					javaMethod = ownerClass.getMethod(getName(), parameters.toArray(new Class<?>[0]));
				} catch (Exception e) {
					throw new IllegalStateException("Failed to find Java method for MetaOperation. Looked in Class '" + ownerClass.getName()
							+ "' for method with name '" + getName() + "' and parameter list " + parameters.toString() + ". MetaOperation description: " 
							+ getDescription(), e);
				}
			}
			return javaMethod;
		}
		return null;
	}

	@Override
	public String getDescription() {
		StringBuilder builder = new StringBuilder()
			.append(super.getDescription()).append(newline)
			.append("Static = '").append(_static).append(newline)
			.append("Abstract = '").append(_abstract).append(newline);
		
		for (RuntimeMetaParameter parameter : getParameters()) {
			builder.append("@param ").append(parameter.getName()).append(" ").append(parameter.getDescription());
			builder.append(newline);
		}
		
		builder.append("@return ").append(returnParameter.getDescription());
		
		return builder.toString();
	}
	
}
