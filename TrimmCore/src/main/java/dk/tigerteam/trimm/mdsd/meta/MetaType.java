/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

import dk.tigerteam.trimm.bean.bidirectional.ManyToOneWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManyListWrapper;
import groovy.lang.Closure;

import java.io.Serializable;
import java.util.*;

/**
 * Base class for all our meta model data.<br/>
 * Follows a common composite pattern where leaf and composites are the same (a
 * leaf just doesn't have any children when asking {@link #getChildren()}).
 * <p/>
 * <b>All associations between {@link #owner} and {@link #children} are
 * maintained bi-directionally for you :)</b>
 * <p/>
 * <b>Note:</b> <i>All searches in the {@link MetaModel} are done using
 * {@link #id}'s, so it is therefore a requirement that all {@link #id}'s are
 * unique.</i>
 * <p/>
 * <i>To support storage of various utility/temporary data during
 * parsing/transformation you can use the various {@link #getAttributes()},
 * {@link #addAttribute(String, Object)}, {@link #removeAttribute(String)},
 * {@link #hasAttribute(String)}, etc.</i>
 * 
 * @author Jeppe Cramon
 */
public class MetaType implements Serializable {
	private static final long serialVersionUID = 765616357281054876L;
	/**
	 * New lines as specified by the Operating System (read of the "line.separator" system property)
	 */
	public static String newline = System.getProperty("line.separator");


    public <T extends MetaType> T getOwnerOfType(Class<T> metaTypeClass) {
        MetaType owner = this.getOwner();
        while (owner != null && !metaTypeClass.isAssignableFrom(owner.getClass())) {
            owner = owner.getOwner();
        }
        if (owner == null) throw new IllegalStateException("Couldn't find an owner of Type " + metaTypeClass.getName() + " for " + this);
        return (T) owner;
    }

    /**
	 * Supported Visibility modes
	 * @author Jeppe Cramon
	 *
	 */
	public enum Visibility {
		Public, Private, Protected, Package
	}

	private boolean fabricated = false;
	private boolean _abstract = false;
	private String id;
	private MetaType owner;
	private List<MetaType> children = new ArrayList<MetaType>();
	private Visibility visibility = Visibility.Public;
	private Map<String, Object> attributes = new HashMap<String, Object>();
	private String documentation;
	private Map<String, String> taggedValues = new HashMap<String, String>();
	

	/**
	 * Short hand for creating a fabricated MetaType with a randomly generated id 
	 */
	protected MetaType() {
		super();
		setFabricated(true);
		id = UUID.randomUUID().toString();
	}

	/**
	 * Create a {@link MetaType} by supplying its unique id
	 * 
	 * @param id
	 *          The unique id
	 */
	public MetaType(String id) {
		super();
		if (id == null) throw new IllegalArgumentException("Id must not be null");
		this.id = id;
	}

	/**
	 * Create a {@link MetaType} by supplying its unique id and a fabrication
	 * information
	 * 
	 * @param id
	 *          The unique id
	 * @param fabricated
	 *          Is this MetaType a result of a fabrication or defined in the
	 *          original model
	 */
	public MetaType(String id, boolean fabricated) {
		this(id);
		this.fabricated = true;
	}

	/**
	 * Is this {@link MetaType} defined from the original Model or is it
	 * constructed by fabrication
	 */
	public boolean isFabricated() {
		return fabricated;
	}

	/**
	 * Is this {@link MetaType} defined from the original Model or is it
	 * constructed by fabrication
	 */
	public MetaType setFabricated(boolean artificial) {
		this.fabricated = artificial;
		return this;
	}

	/**
	 * The unique id for a meta type
	 */
	public String getId() {
		return id;
	}

	/**
	 * Is this type abstract?
	 * 
	 * @return true if the type is abstract, otherwise false
	 */
	public boolean isAbstract() {
		return _abstract;
	}

	/**
	 * Set if this type is abstract or not
	 * 
	 * @param _abstract
	 *          true if the type is abstract, otherwise false
	 * @return this meta type instance
	 */
	public MetaType setAbstract(boolean _abstract) {
		this._abstract = _abstract;
		return this;
	}

	/**
	 * Any documentation about the meta type
	 */
	public String getDocumentation() {
		return documentation;
	}

	/**
	 * Any documentation about the meta type
	 * 
	 * @param documentation
	 *          Any documentation about the meta type
	 * @return This meta type instance
	 */
	public MetaType setDocumentation(String documentation) {
		this.documentation = documentation;
		return this;
	}

	/**
	 * Any tagged values (UML) which is a combination of name/value (or key/value
	 * if you like) pairs - returns an immutable Map
	 */
	public Map<String, String> getTaggedValues() {
		return Collections.unmodifiableMap(taggedValues);
	}

	/**
	 * Any tagged values (UML) which is a combination of name/value (or key/value
	 * if you like) pairs - this will override any previous Tagged values
	 * 
	 * @param taggedValues
	 *          Any tagged values (UML)
	 * @return This meta type instance
	 */
	public MetaType setTaggedValues(Map<String, String> taggedValues) {
		this.taggedValues.clear();
		this.taggedValues.putAll(taggedValues);
		return this;
	}

	/**
	 * Add a tagged value (UML)
	 * 
	 * @param name
	 *          The name of the tagged value
	 * @param value
	 *          The value of the tagged value
	 * @return This meta type instance
	 */
	public MetaType addTaggedValue(String name, String value) {
		this.taggedValues.put(name, value);
		return this;
	}

	/**
	 * Do we have a tagged value by the given name (case insensitive)
	 * 
	 * @param name
	 *          The name of the tagged value
	 * @return true if we have a tagged value by the given name, otherwise false
	 */
	public boolean hasTaggedValue(String name) {
		for (String taggedValueName : taggedValues.keySet()) {
			if (taggedValueName.equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Do we have a tagged value by the given name (case insensitive) and is its
	 * value different from <code>null</code>
	 * 
	 * @param name
	 *          The name of the tagged value
	 * @return true if we have a tagged value by the given name and its value is
	 *         different from <code>null</code>, otherwise false
	 */
	public boolean hasNotNullTaggedValue(String name) {
		return getTaggedValue(name) != null;
	}

	/**
	 * Get the tagged value by its name (case insensitive)
	 * 
	 * @param name
	 *          The name of the tagged value
	 * @return The tagged value or <code>null</code> if we don't have a tagged
	 *         value by the given name
	 */
	public String getTaggedValue(String name) {
		for (String taggedValueName : taggedValues.keySet()) {
			if (taggedValueName.equalsIgnoreCase(name)) {
				return taggedValues.get(taggedValueName);
			}
		}
		return null;
	}

	/**
	 * Add a child to this meta type<br/>
	 * <b>Note:</b> <i> the bi-directional relationship to the owner is automatically maintained</i>
	 * @param children
	 *          The children to add
	 * @return This meta type
	 */
	public MetaType add(MetaType... children) {
		for (MetaType child : children) {
			getChildren().add(child);

		}
		return this;
	}

	/**
	 * Get all children of this meta type - note this list is <b>Mutable</b><br/>
	 * <b>Note:</b> <i>the bi-directional relationship to the owner is automatically maintained</i>
	 */
	public List<MetaType> getChildren() {
		return new OneToManyListWrapper<MetaType, MetaType>(this, children) {
			private static final long serialVersionUID = -782855107551392868L;

			@Override
			protected MetaType getOneSideObjectInManySideObject(MetaType manySideObject) {
				return manySideObject.getOwner();
			}

			@Override
			protected void setOneSideObjectInManySideObject(MetaType manySideObject, MetaType oneSideObject) {
				manySideObject.setOwner(oneSideObject);
			}

			@Override
			public boolean contains(Object o) {
				if (o instanceof MetaType) {
					MetaType mt = (MetaType) o;
					return mt.getOwner().equals(this);
				}
				return false;
			}
		};
	}

	/**
	 * Get all stereotypes applied to this meta type - returns an immutable list
	 */
	public List<MetaStereoType> getStereoTypes() {
		return findImmediateChildrenOfType(MetaStereoType.class);
	}

	/**
	 * Remove all Stereotypes from this meta type and return the removed
	 * stereotypes
	 * 
	 * @return The stereotypes removed
	 */
	public List<MetaStereoType> removeAllStereoTypes() {
		List<MetaStereoType> stereoTypes = findImmediateChildrenOfType(MetaStereoType.class);
		for (MetaStereoType stereoType : stereoTypes) {
			this.getChildren().remove(stereoType);
		}
		return stereoTypes;
	}

	/**
	 * Do we have a {@link MetaStereoType} applied by the given name (case
	 * insensitive)
	 * 
	 * @param name
	 *          The name of the stereotype value
	 * @return true if we have a stereotype by the given name, otherwise false
	 */
	public boolean hasStereoType(String name) {
		for (MetaStereoType stereoType : getStereoTypes()) {
			if (stereoType.getName() != null && stereoType.getName().equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if this MetaType or any of its parents (Owners) has the given stereotype applied
	 * @param stereoTypeName The name of the stereotype
	 * @return true if the type or any of its parents has the stereotype applied
	 */
	public boolean hasStereotypeUpwardsRecursively(String stereoTypeName) {
		if (hasStereoType(stereoTypeName)) {
			return true;
		} else {
			if (getOwner() != null) {
				return getOwner().hasStereotypeUpwardsRecursively(stereoTypeName);
			}
		}
		return false;
	}

	/**
	 * Get the owner of this meta type (is null for {@link MetaModel})<br/>
	 * <b>Note:</b> <i>bi-directional relationships to the children are automatically maintained</i>
	 */
	@SuppressWarnings("unchecked")
	public <T extends MetaType> T getOwner() {
		return (T) owner;
	}

	/**
	 * Set the owner of this meta type<br/>
	 * <b>Note:</b> <i>bi-directional relationships to the children are automatically maintained</i>
	 * 
	 * @param owner
	 *          The new owner of this meta type
	 * @return current meta type instance
	 */
	public MetaType setOwner(MetaType owner) {
		new ManyToOneWrapper<MetaType, MetaType>(this) {
			private static final long serialVersionUID = -6402624532142931737L;

			@SuppressWarnings("unchecked")
			@Override
			protected void addManySideObjectToOneSideCollection(MetaType oneSide, MetaType manySide) {
				((OneToManyListWrapper<MetaType, MetaType>) oneSide.getChildren()).getWrappedCollection().add(manySide);
			}

			@Override
			protected MetaType getOneSideObjectInManySideObject(MetaType manySide) {
				return manySide.owner;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void removeManySideObjectFromOneSideCollection(MetaType oneSide, MetaType manySide) {
				((OneToManyListWrapper<MetaType, MetaType>) oneSide.getChildren()).getWrappedCollection().remove(manySide);
			}

			@Override
			protected void setOneSideObjectInManySideObject(MetaType manySide, MetaType oneSide) {
				manySide.owner = oneSide;
			}
		}.updateOneSideObject(owner);
		return this;
	}

	/**
	 * Get the {@link Visibility} of this meta type
	 */
	public Visibility getVisibility() {
		return visibility;
	}

	/**
	 * Set the {@link Visibility} of this meta type
	 * 
	 * @param visibility
	 * @return This meta type instance
	 */
	public MetaType setVisibility(Visibility visibility) {
		this.visibility = visibility;
		return this;
	}

	/**
	 * Get all attributes applied (attributes are used for storing various value
	 * that can be useful during parsing or transformation) 
	 */
	public Map<String, Object> getAttributes() {
		return attributes;
	}

	/**
	 * Add an attribute (attributes are used for storing various value that can be
	 * useful during parsing or transformation) - Same as
	 * {@link #setAttribute(String, Object)}
	 * 
	 * @param key
	 *          The name of the attribute
	 * @param value
	 *          The attribute value
	 * @return This meta type instance
	 */
	public MetaType addAttribute(String key, Object value) {
		attributes.put(key, value);
		return this;
	}

	/**
	 * Sets an attribute (attributes are used for storing various value that can
	 * be useful during parsing or transformation) - Same as
	 * {@link #addAttribute(String, Object)}
	 * 
	 * @param key
	 *          The name of the attribute
	 * @param value
	 *          The attribute value
	 * @return This meta type instance
	 */
	public MetaType setAttribute(String key, Object value) {
		attributes.put(key, value);
		return this;
	}

	/**
	 * Remove an attribute (attributes are used for storing various value that can
	 * be useful during parsing or transformation)
	 * 
	 * @param key
	 *          The name of the attribute
	 * @return This meta type instance
	 */
	public MetaType removeAttribute(String key) {
		attributes.remove(key);
		return this;
	}

	/**
	 * Do we have the given attribute applied? (attributes are used for storing
	 * various value that can be useful during parsing or transformation)
	 * 
	 * @param key
	 *          The name of the attribute
	 * @return true if we have the attribute applied, otherwise false
	 */
	public boolean hasAttribute(String key) {
		return attributes.containsKey(key);
	}

	/**
	 * Get the attribute (attributes are used for storing various value that can
	 * be useful during parsing or transformation)
	 * 
	 * @param key
	 *          The name of the attribute
	 * @return The attribute value if it was applied or <code>null</code> if it
	 *         wasn't applied
	 */
	public Object getAttribute(String key) {
		return attributes.get(key);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final MetaType other = (MetaType) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "#" + id;
	}

	/**
	 * JavaDoc friendly description of this {@link MetaType}.
	 */
	public String getDescription() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Meta id = '" + this.getId()).append("'<br/>").append(newline).append("Visibility = '")
				.append(visibility.toString()).append("'<br/>").append(newline).append("TaggedValues = '").append(taggedValues.toString())
				.append("'<br/>").append(newline).append("Fabricated = '").append(fabricated).append("'<br/>").append(newline)
				.append("StereoTypes = '");
		int index = 0;
		for (MetaStereoType stereoType : getStereoTypes()) {
			if (index > 0) {
				stringBuilder.append(", ");
			}
			stringBuilder.append(stereoType.getName());
			index++;
		}
		stringBuilder.append("'");
		return stringBuilder.toString();
	}

	// ----------------------------------------------
	/**
	 * Find a {@link MetaType} by it's id. This method will search this instance
	 * and all its children recursively (in a breadth first search)
	 * 
	 * @throws MetaTypeNotFoundException
	 *           if a {@link MetaType} by the given id can't be found
	 */
	public MetaType getById(String id) {
		MetaType match = internalFindById(id);
		if (match == null) {
			throw new MetaTypeNotFoundException(id);
		} else {
			return match;
		}
	}

	/**
	 * Find an optional (may not exist because of parser exclusion)
	 * {@link MetaType} by it's id. This method will search this instance and all
	 * its children recursively (in a breadth first search)
	 */
	public MetaType findById(String id) {
		return internalFindById(id);
	}

	/**
	 * Helper method for {@link #getById}
	 * 
	 * @param id
	 * @return The MetaType with the id (or null if not found)
	 */
	private MetaType internalFindById(String id) {
		if (this.id.equals(id)) {
			return this;
		}
		return recursivelyWideSearch(id, this);
	}

	/**
	 * Find a {@link MetaType} by it's id. This method will search this instance
	 * and all its children recursively (in a breadth first search)
	 * 
	 * @throws MetaTypeNotFoundException
	 *           if a {@link MetaType} by the given id can't be found
	 */
	public <T extends MetaType> T getByIdAndType(String id, Class<T> type) {
		if (id == null) {
			throw new NullPointerException("Id was null");
		}
		T match = internalFindByIdAndType(id, type);
		if (match == null) {
			throw new MetaTypeNotFoundException(id);
		} else {
			return match;
		}
	}

	/**
	 * Helper method for {@link #getById}
	 * 
	 * @param id
	 * @return The MetaType with the id and type or null if not found
	 */
	@SuppressWarnings("unchecked")
	private <T extends MetaType> T internalFindByIdAndType(String id, Class<T> type) {
		if (this.id.equals(id) && type.isAssignableFrom(this.getClass())) {
			return (T) this;
		}
		return recursivelyWideSearchByType(id, type, this);
	}

	/**
	 * Helper method for {@link #internalFindById(String)} -> Search breadth first
	 * (first all children) and then afterwards each child recursively
	 * 
	 * @param id
	 * @param parent
	 * @return The MetaType with the id and type or null if not found
	 */
	@SuppressWarnings("unchecked")
	private <T extends MetaType> T recursivelyWideSearchByType(String id, Class<T> type, MetaType parent) {
		for (MetaType childType : parent.getChildren()) {
			if (childType.id.equals(id) && type.isAssignableFrom(childType.getClass())) {
				return (T) childType;
			}
		}

		for (MetaType childType : parent.getChildren()) {
			T match = recursivelyWideSearchByType(id, type, childType);
			if (match != null) {
				return match;
			}
		}
		return null;
	}

	/**
	 * Helper method for {@link #internalFindById(String)} -> Search breadth first
	 * (first all children) and then afterwards each childs children recursively
	 * 
	 * @param id
	 * @param parent
	 * @return  The MetaType with the id or null if not found
	 */
	private MetaType recursivelyWideSearch(String id, MetaType parent) {
		for (MetaType childType : parent.getChildren()) {
			if (childType.id.equals(id)) {
				return childType;
			}
		}

		for (MetaType childType : parent.getChildren()) {
			MetaType match = recursivelyWideSearch(id, childType);
			if (match != null) {
				return match;
			}
		}
		return null;
	}

	/**
	 * Search for a child of the given {@link MetaType} class. Only searches the
	 * immediate/direct children of this instance
	 * 
	 * @param <T>
	 *          The type of children to find (or subtypes thereof)
	 * @param metaTypeClass
	 *          The type of children to find (or subtypes thereof)
	 * @return The immediate children found
	 */
	@SuppressWarnings("unchecked")
	public <T extends MetaType> List<T> findImmediateChildrenOfType(Class<T> metaTypeClass) {
		List<T> result = new LinkedList<T>();
		for (MetaType child : children) {
			if (metaTypeClass.isAssignableFrom(child.getClass())) {
				result.add((T) child);
			}
		}
		return Collections.unmodifiableList(result);
	}

    /**
     * Search for a child of the given {@link MetaType} class. Only searches the
     * immediate/direct children of this instance
     *
     * @param <T>
     *          The type of children to find (or subtypes thereof)
     * @param metaTypeClass
     *          The type of children to find (or subtypes thereof)
     * @return The immediate child found or <code>null</code> if no match found
     * @throws MetaTypeNotFoundException if we found more than 1 match
     */
    @SuppressWarnings("unchecked")
    public <T extends MetaType> T findImmediateChildOfType(Class<T> metaTypeClass) {
        List<T> result = new LinkedList<T>();
        for (MetaType child : children) {
            if (metaTypeClass.isAssignableFrom(child.getClass())) {
                result.add((T) child);
            }
        }
        if (result.size() == 1) {
            return result.get(0);
        } else if (result.size() == 0) {
            return null;    
        } else {
            throw new MetaTypeNotFoundException("Found " + result.size() + " immediate children of type '" + metaTypeClass.getSimpleName() + "' - expected 0 or 1");
        }
    }

    /**
     * Search for a child of the given {@link MetaType} class. Only searches the
     * immediate/direct children of this instance
     *
     * @param <T>
     *          The type of children to find (or subtypes thereof)
     * @param metaTypeClass
     *          The type of children to find (or subtypes thereof)
     * @return The immediate child found
     * @throws MetaTypeNotFoundException if we found 0 or more than 1 match
     */
    @SuppressWarnings("unchecked")
    public <T extends MetaType> T findImmediateChildOfTypeRequired(Class<T> metaTypeClass) {
        List<T> result = new LinkedList<T>();
        for (MetaType child : children) {
            if (metaTypeClass.isAssignableFrom(child.getClass())) {
                result.add((T) child);
            }
        }
        if (result.size() == 1) {
            return result.get(0);
        } else if (result.size() == 0) {
            throw new MetaTypeNotFoundException("Found " + 0 + " immediate children of type '" + metaTypeClass.getSimpleName() + "' - expected 1");
        } else {
            throw new MetaTypeNotFoundException("Found " + result.size() + " immediate children of type '" + metaTypeClass.getSimpleName() + "' - expected 1");
        }
    }

    /**
     * Search for a child of the given {@link MetaType} class. Only searches the
     * immediate/direct children of this instance
     *
     * @param <T>
     *          The type of children to find (or subtypes thereof)
     * @param name The name of the Meta type
     * @param metaTypeClass
     *          The type of children to find (or subtypes thereof)
     * @return The immediate child found or <code>null</code> if no match found
     * @throws MetaTypeNotFoundException if we found more than 1 match
     */
    @SuppressWarnings("unchecked")
    public <T extends NameableMetaType> T findImmediateChildWithNameAndType(String name, Class<T> metaTypeClass) {
        List<T> result = new LinkedList<T>();
        for (MetaType child : children) {
            if (metaTypeClass.isAssignableFrom(child.getClass()) && name.equals(((NameableMetaType)child).getName())) {
                result.add((T) child);
            }
        }
        if (result.size() == 1) {
            return result.get(0);
        } else if (result.size() == 0) {
            return null;
        } else {
            throw new MetaTypeNotFoundException("Found " + result.size() + " immediate children with name '" + name + "' and type '" + metaTypeClass.getSimpleName() + "' - excepted 0 or 1");
        }
    }

    /**
     * Search for a child of the given {@link MetaType} class. Only searches the
     * immediate/direct children of this instance
     *
     * @param <T>
     *          The type of children to find (or subtypes thereof)
     * @param name The name of the Meta type
     * @param metaTypeClass
     *          The type of children to find (or subtypes thereof)
     * @return The immediate child found
     * @throws MetaTypeNotFoundException if we found 0 or more than 1 match
     */
    @SuppressWarnings("unchecked")
    public <T extends NameableMetaType> T findImmediateChildWithNameAndTypeRequired(String name, Class<T> metaTypeClass) {
        List<T> result = new LinkedList<T>();
        for (MetaType child : children) {
            if (metaTypeClass.isAssignableFrom(child.getClass()) && name.equals(((NameableMetaType)child).getName())) {
                result.add((T) child);
            }
        }
        if (result.size() == 1) {
            return result.get(0);
        } else if (result.size() == 0) {
            throw new MetaTypeNotFoundException("Found " + 0 + " immediate children with name '" + name + "' and type '" + metaTypeClass.getSimpleName() + "'- expected 1");
        } else {
            throw new MetaTypeNotFoundException("Found " + result.size() + " immediate children with name '" + name + "' and type '" + metaTypeClass.getSimpleName() + "'- expected 1");
        }
    }    

	/**
	 * Recursively search this instance children, for children of the given
	 * {@link MetaType}
	 * 
	 * @param <T>
	 *          The type of children to find (or subtypes thereof)
	 * @param metaTypeClass
	 *          The type of children to find (or subtypes thereof)
	 * @return All the children found
	 */
	public <T extends MetaType> List<T> findAllChildrenOfType(Class<T> metaTypeClass) {
		List<T> result = new LinkedList<T>();
		recursivelyFindAllChildrenOfType(this, metaTypeClass, result);
		return Collections.unmodifiableList(result);
	}

	/**
	 * Helper method for {@link #findAllChildrenOfType(Class)}
	 * 
	 * @param <T>
	 *          The type of children to find (or subtypes thereof)
	 * @param metaType
	 *          The metatype which will have all its children recursively searched
	 * @param metaTypeClass
	 *          The type of children to find (or subtypes thereof)
	 * @param childrenFound
	 *          Collector for all the children found during the search
	 */
	@SuppressWarnings("unchecked")
	private <T extends MetaType> void recursivelyFindAllChildrenOfType(MetaType metaType, Class<T> metaTypeClass, List<T> childrenFound) {
		for (MetaType childType : metaType.getChildren()) {
			if (metaTypeClass.isAssignableFrom(childType.getClass())) {
				childrenFound.add((T) childType);
			}
			recursivelyFindAllChildrenOfType(childType, metaTypeClass, childrenFound);
		}
	}

	/**
	 * Searches upwards in the hierarchy to find an owner of a given type.
	 * 
	 * @param <T>
	 *          The type of owner to find (or a subtype thereof)
	 * @param typeOfOwner
	 *          The type of owner to find (or a subtype thereof)
	 * @return The owner found
	 * @throws MetaTypeNotFoundException
	 *           in case a an owner of the given type can't be found
	 */
	public <T extends MetaType> T findOwnerOfType(Class<T> typeOfOwner) {
		T parent = findOwnerOfTypeOrNull(typeOfOwner);
		if (parent != null) {
			return parent;
		}
		throw new MetaTypeNotFoundException("Couldn't find an owner of type " + typeOfOwner);
	}

	/**
	 * Searches upwards in the hierarchy to find an owner of a given type. <T> The
	 * type of owner to find (or a subtype thereof)
	 * 
	 * @param typeOfOwner
	 *          The type of owner to find (or a sub-type thereof)
	 * @return the owner found or <code>null</code> if an owner of the given type
	 *         can't be found
	 */
	@SuppressWarnings("unchecked")
	public <T extends MetaType> T findOwnerOfTypeOrNull(Class<T> typeOfOwner) {
		MetaType parent = getOwner();
		while (parent != null) {
			if (typeOfOwner.isAssignableFrom(parent.getClass())) {
				return (T) parent;
			}
			parent = parent.getOwner();
		}
		return null;
	}

	/**
	 * Search <b>this</b> {@link MetaType}'s children recursively using the
	 * {@link MetaCriteria} for a single matching element
	 * 
	 * @param <T>
	 *          The type of elements to find (or sub-types thereof)
	 * @param criteria
	 *          The criteria to use
	 * @return The matching element found or <code>null</code> if no matching
	 *         elements are found
	 * @throws MetaTypeNotFoundException
	 *           In case there are more than 1 matching elements
	 */
	public <T extends MetaType> T findSingleMatch(MetaCriteria<T> criteria) {
		List<T> result = find(criteria);
		if (result.size() == 1) {
			return result.get(0);
		} else if (result.size() == 0) {
			return null;
		} else {
			throw new MetaTypeNotFoundException("Found " + result.size() + " matching elements - excepted 0 or 1 match");
		}
	}

	/**
	 * Search <b>this</b> {@link MetaType}'s children recursively using the
	 * {@link MetaCriteria} for a required single matching element
	 * 
	 * @param <T>
	 *          The type of elements to find (or subtypes thereof)
	 * @param criteria
	 *          The criteria to use
	 * @return The matching element found
	 * @throws MetaTypeNotFoundException
	 *           In case there is 0 or more than 1 matching elements
	 */
	public <T extends MetaType> T findSingleMatchRequired(MetaCriteria<T> criteria) {
		List<T> result = find(criteria);
		if (result.size() == 1) {
			return result.get(0);
		} else {
			throw new MetaTypeNotFoundException("Found " + result.size() + " matching elements - excepted 1 match");
		}
	}

	/**
	 * Search <b>this</b> {@link MetaType}'s children using the
	 * {@link MetaCriteria} for a single matching element
	 * 
	 * @param <T>
	 *          The type of elements to find (or subtypes thereof)
	 * @param criteria
	 *          The criteria to use
	 * @return The matching element found or <code>null</code> if no matching
	 *         elements are found
	 * @throws MetaTypeNotFoundException
	 *           In case there are more than 1 matching elements
	 */
	public <T extends MetaType> T findSingleMatchInImmediateChildren(MetaCriteria<T> criteria) {
		List<T> result = findInImmediateChildren(criteria);
		if (result.size() == 1) {
			return result.get(0);
		} else if (result.size() == 0) {
			return null;
		} else {
			throw new MetaTypeNotFoundException("Found " + result.size() + " matching elements - excepted 0 or 1 match");
		}
	}

	/**
	 * Search <b>this</b> {@link MetaType}'s children using the
	 * {@link MetaCriteria}.
	 * 
	 * @param <T>
	 *          The type of elements to find (or subtypes thereof)
	 * @param criteria
	 *          The criteria to use
	 * @return The elements found that matches the criteria
	 */
	@SuppressWarnings("unchecked")
	public <T extends MetaType> List<T> findInImmediateChildren(MetaCriteria<T> criteria) {
		Class<?> tClass = (Class<?>) criteria.getType();
		List<T> result = new LinkedList<T>();
		for (MetaType element : this.getChildren()) {
			if (tClass.isAssignableFrom(element.getClass()) && criteria.isOk((T) element)) {
				result.add((T) element);
			}
		}
		return Collections.unmodifiableList(result);
	}

	/**
	 * This will recursively iterate all this MetaType's children (excluding the
	 * MetaType on which it's called) and all it's children
	 * 
	 * @param forEach
	 */
	public void forEach(ForEach forEach) {
		recursivelyForEach(this, forEach);
	}

	private void recursivelyForEach(MetaType metaType, ForEach forEach) {
		for (MetaType element : metaType.getChildren()) {
			forEach.each(element);
			recursivelyForEach(element, forEach);
		}

	}

	/**
	 * Groovy friendly forEach supporting Closures.<br/>
	 * This will recursively iterate all this MetaType's children (excluding the
	 * MetaType on which it's called) and all it's children
	 * 
	 * @param closure
	 */
	@SuppressWarnings("rawtypes")
	public void each(Closure closure) {
		recursivelyGroovyEach(this, closure);
	}

	@SuppressWarnings("rawtypes")
	private void recursivelyGroovyEach(MetaType metaType, Closure closure) {
		for (MetaType element : metaType.getChildren()) {
			closure.call(element);
			recursivelyGroovyEach(element, closure);
		}
	}

	/**
	 * Search <b>this</b> {@link MetaType} element and all its children
	 * recursively using the {@link MetaCriteria}.
	 * 
	 * @param <T>
	 *          The type of elements to find (or sub-types thereof)
	 * @param criteria
	 *          The criteria to use
	 * @return The elements found
	 */
	@SuppressWarnings("unchecked")
	public <T extends MetaType> List<T> find(MetaCriteria<T> criteria) {
		Class<?> tClass = (Class<?>) criteria.getType();
		List<T> result = new LinkedList<T>();
		if (tClass.isAssignableFrom(this.getClass()) && criteria.isOk((T) this)) {
			result.add((T) this);
		}
		recursivelyFind(this, criteria, result, tClass);
		return Collections.unmodifiableList(result);
	}

	/**
	 * Internal helper method for {@link #find(MetaCriteria)}
	 */
	@SuppressWarnings("unchecked")
	private <T extends MetaType> void recursivelyFind(MetaType metaType, MetaCriteria<T> criteria, List<T> elementsFound, Class<?> tClass) {
		for (MetaType element : metaType.getChildren()) {
			if (tClass.isAssignableFrom(element.getClass()) && criteria.isOk((T) element)) {
				elementsFound.add((T) element);
			}
			recursivelyFind(element, criteria, elementsFound, tClass);
		}
	}

	public MetaModel getMetaModel() {
		return findOwnerOfType(MetaModel.class);
	}
}
