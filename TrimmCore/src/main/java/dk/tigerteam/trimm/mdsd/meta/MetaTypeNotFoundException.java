/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

/**
 * Exception thrown if a MetaType can't be found during a search
 * @author Jeppe Cramon
 */
public class MetaTypeNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 5690162181229407263L;

	public MetaTypeNotFoundException() {
	}

	public MetaTypeNotFoundException(String id) {
		super(id);
	}

	public MetaTypeNotFoundException(Throwable cause) {
		super(cause);
	}

	public MetaTypeNotFoundException(String id, Throwable cause) {
		super(id, cause);
	}
	
	public String getId() {
		return getMessage();
	}
	
	@Override
	public String toString() {
		return "Couldn't find MetaType with id '" + getMessage() + "'";
	}

}
