/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * The root object in a {@link MetaType} instance hierarchy.<p/>
 * Supports referenced Meta Models - which e.g. is supported by MagicDraw
 * 
 * @author Jeppe Cramon
 */
public class MetaModel extends NameableMetaType {
	private static final long serialVersionUID = -1899169853526893804L;
	/**
	 * Key: Referenced meta model name
	 */
	private Map<String, MetaModel> referencedMetaModels = new HashMap<String, MetaModel>();

	public MetaModel(String id) {
		super(id);
	}

	public MetaModel addReferencedMetaModel(String name, MetaModel metaModel) {
		referencedMetaModels.put(name, metaModel);
		return this;
	}

	/**
	 * Add the {@link MetaModel} using its {@link MetaModel#getName()} property as refrecened meta model name
	 * 
	 * @param metaModel
	 * @return this
	 */
	public MetaModel addReferencedMetaModel(MetaModel metaModel) {
		referencedMetaModels.put(metaModel.getName(), metaModel);
		return this;
	}

	public MetaModel removeReferencedMetaModel(String name) {
		referencedMetaModels.remove(name);
		return this;
	}

	public MetaModel getReferencedMetaModel(String name) {
		MetaModel referencedMetaModel = referencedMetaModels.get(name);
		if (referencedMetaModel == null) {
			throw new MetaTypeNotFoundException("Couldn't find Referenced MetaModel with name '" + name + "'");
		}
		return referencedMetaModel;
	}

	public Map<String, MetaModel> getReferencedMetaModels() {
		return referencedMetaModels;
	}

	public Set<String> getReferencedMetaModelNames() {
		return referencedMetaModels.keySet();
	}

    /**
     * Searches inside the meta models first level of children for a MetaPackage with the given name
     * @param packageName The name of the package
     * @return The package
     * @throws    MetaTypeNotFoundException In case it couldn't find a MetaPackage with the given name
     */
    public MetaPackage getPackageByNameRequired(String packageName) {
        return findImmediateChildWithNameAndTypeRequired(packageName, MetaPackage.class);
    }

    /**
     * Searches inside the meta models first level of children for a MetaPackage with the given name
     * @param packageName The name of the package
     * @return The package or <code>null</code> if no package matching the name was found
     */
    public MetaPackage getPackageByName(String packageName) {
        return findImmediateChildWithNameAndType(packageName, MetaPackage.class);
    }


    public MetaPackage getRootPackage() {
        return findImmediateChildOfTypeRequired(MetaPackage.class);
    }
}
