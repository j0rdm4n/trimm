/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

import dk.tigerteam.trimm.bean.bidirectional.ManyToOneWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManySetWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToOneWrapper;
import dk.tigerteam.trimm.util.ReflectUtils;
import dk.tigerteam.trimm.util.Utils;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Models a Property (name, type, cardinality and association).<p/>
 * <h3>Cardinality</h3>
 * Cardinality indicates how many instances of the given property can exist at one time. This is specified using the {@link #minCardinality} and
 * {@link #maxCardinality} properties (unlimited has value -1 aka {@link #UNLIMITED}). A user friendly way to read the cardinality is to use {@link #getCardinalityType()}
 * {@link CardinalityType#Many}. Example:
 * <table>
 * 	<tr>
 * 		<th>Min cardinality</th><th>Max cardinality</th><th>Cardinality type</th>
 *  </tr>
* 	<tr><td>0</td><td>1</td><td>{@link CardinalityType#SingleOptional}</td></tr>
 *    <tr><td>1</td><td>1</td><td>{@link CardinalityType#SingleRequired}</td></tr>
 *    <tr><td>0</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
 *    <tr><td>1</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
 * </table>
 * <h3>Bounds</h3>Car
 * Bounds are not like cardinality. {@link #upperBound} and {@link #lowerBound} indicate the value range for a property, 
 * for instance the min and max length for a String.<br/>
 * The bound is specified after the type name inside a parenthesis.<br/>
 * Example: Max string length is 60: <code>String (60)</code> or min length is 3 and max is 255 <code>String (3, 255)</code>
 * @author Jeppe Cramon
 */
public class RuntimeMetaProperty extends RuntimeNameableMetaType {
	private static final long serialVersionUID = -5188669990498570213L;
	/**
	 * Value for an unlimited (unbounded) cardinality
	 */
	public static int UNLIMITED = -1;
	/**
	 * The different types of Aggregations 
	 * @author Jeppe Cramon
	 */
	public enum Aggregation {
		/**
		 * Aka. Aggregated
		 */
		Shared, 
		Composite, 
		None 
	}
	public enum AssociationType {
		OneToOne, OneToMany, ManyToOne, ManyToMany
	}
	/**
	 * And Object interpretation of #minCardinality and #maxCardinality
	 */
	public enum CardinalityType { SingleRequired, SingleOptional, Many }
	private Aggregation aggregation = Aggregation.None;
	private int minCardinality = 1;
	private int maxCardinality = 1;
	private boolean derived = false;
	private boolean ordered = false;
	private RuntimeMetaClazz type;
	private RuntimeMetaAssociation association;
	private boolean _static;
	private String defaultValue;
	private Long lowerBound;
	private Long upperBound;
	private boolean readable = true;
	private boolean writeable = true;
	private boolean hasCorrespondingJavaProperty = true;
	private transient PropertyDescriptor propertyDescriptor;
	private transient Field javaField;
	
	/**
	 * Create a {@link RuntimeMetaProperty} by supplying its unique id and a fabrication information
	 * @param id The unique id
	 * @param fabricated Is this MetaType a result of a fabrication or defined in the original model
	 */
	public RuntimeMetaProperty(String id, boolean fabricated, PropertyDescriptor propertyDescriptor) {
		super(id, fabricated);
		this.propertyDescriptor = propertyDescriptor;
		setName(propertyDescriptor.getName());
	}
	
	public RuntimeMetaProperty(String id, boolean fabricated, String name) {
		super(id, fabricated, name);
	}
	
	public RuntimeMetaProperty(String id, boolean fabricated, String name, boolean hasCorrespondingJavaProperty) {
		super(id, fabricated, name);
		this.hasCorrespondingJavaProperty = hasCorrespondingJavaProperty;
	}
	
	/**
	 * Do we have created corresponding Java property getter/setter methods?<br/>
	 * Guard method for {@link #getPropertyDescriptor()}
	 */
	public boolean isHasCorrespondingJavaProperty() {
		return hasCorrespondingJavaProperty;
	}

	public RuntimeMetaProperty setHasCorrespondingJavaProperty(boolean hasCorrespondingJavaProperty) {
		checkCanModify();
		this.hasCorrespondingJavaProperty = hasCorrespondingJavaProperty;
		return this;
	}
	
	public Field getJavaField() {
		if (javaField == null && getName() != null) {
			javaField = ReflectUtils.getField(getOwnerClazz().getJavaClass(), getName());
		}
		return javaField;
	}

	/**
	 * The property this metatype represents
	 */
	public PropertyDescriptor getPropertyDescriptor() {
		if (propertyDescriptor == null && getName() != null) {
			try {
				PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(getOwnerClazz().getJavaClass()).getPropertyDescriptors();
				for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
					if (propertyDescriptor.getName().equals(getName()) || propertyDescriptor.getName().equals(Utils.capitalize(getName()))) {
						this.propertyDescriptor = propertyDescriptor;
						break;
					}
				}
			} catch (IntrospectionException e) {
				throw new IllegalStateException("Failed to resolve PropertyDescriptor for meta property '" + getName() + "' in Class '" + getOwnerClazz().getName() + "'", e);
			}
			if (propertyDescriptor == null) {
				throw new IllegalStateException("Failed to resolve PropertyDescriptor for meta property '" + getName() + "' in Class '" + getOwnerClazz().getName() + "'");
			}
		}
		return propertyDescriptor;
	}
	
	/**
	 * Does the {@link PropertyDescriptor}, that this {@link RuntimeMetaProperty} represents,
	 * contain the {@link Method} as either the Read or Write method?
	 * @param method
	 */
	public boolean containsMethod(Method method) {
		if (method.equals(getPropertyDescriptor() != null ? getPropertyDescriptor().getReadMethod() : null)) {
			return true;
		} else if (method.equals(getPropertyDescriptor() != null ? getPropertyDescriptor().getWriteMethod() : null)) {
			return true;
		}
		return false;
	}

	/**
	 * Is this Meta property readable (default is true) 
	 */
	public boolean isReadable() {
		return readable;
	}

	/**
	 * Set if this Meta property is readable (default is true) 
	 */
	public RuntimeMetaProperty setReadable(boolean readable) {
		checkCanModify();
		this.readable = readable;
		return this;
	}


	/**
	 * Is this Meta property writeable (default is true) 
	 */
	public boolean isWriteable() {
		return writeable;
	}

	/**
	 * Set if this Meta property is writeable (default is true) 
	 */
	public RuntimeMetaProperty setWriteable(boolean writeable) {
		checkCanModify();
		this.writeable = writeable;
		return this;
	}


	/**
	 * The lower bound for the property (for Strings this is typically minimum length and for Numbers the smallest number allowed) 
	 */
	public Long getLowerBound() {
		return lowerBound;
	}

	/**
	 * The lower bound for the property (for Strings this is typically minimum length and for Numbers the smallest number allowed) 
	 */
	public RuntimeMetaProperty setLowerBound(Long lowerBound) {
		checkCanModify();
		this.lowerBound = lowerBound;
		return this;
	}

	/**
	 * The upper bound for the property (for Strings this is typically maximum length and for Numbers the largest number allowed) 
	 */
	public Long getUpperBound() {
		return upperBound;
	}

	/**
	 * The upper bound for the property (for Strings this is typically maximum length and for Numbers the largest number allowed) 
	 */
	public RuntimeMetaProperty setUpperBound(Long upperBound) {
		checkCanModify();
		this.upperBound = upperBound;
		return this;
	}



	public int getMinCardinality() {
		return minCardinality;
	}

	public RuntimeMetaProperty setMinCardinality(int minCardinality) {
		checkCanModify();
		this.minCardinality = minCardinality;
		return this;
	}

	public int getMaxCardinality() {
		return maxCardinality;
	}

	public RuntimeMetaProperty setMaxCardinality(int maxCardinality) {
		checkCanModify();
		this.maxCardinality = maxCardinality;
		return this;
	}

	public boolean isDerived() {
		return derived;
	}

	public RuntimeMetaProperty setDerived(boolean derived) {
		checkCanModify();
		this.derived = derived;
		return this;
	}

	public boolean isOrdered() {
		return ordered;
	}

	public RuntimeMetaProperty setOrdered(boolean ordered) {
		checkCanModify();
		this.ordered = ordered;
		return this;
	}

	public RuntimeMetaClazz getOwnerClazz() {
		return (RuntimeMetaClazz) getOwner();
	}
		
	public Aggregation getAggregation() {
		return aggregation;
	}

	public RuntimeMetaProperty setAggregation(Aggregation aggregation) {
		checkCanModify();
		this.aggregation = aggregation;
		return this;
	}
	
	public RuntimeMetaAssociation getAssociation() {
		return association;
	}
	
	
	public boolean isStatic() {
		return _static;
	}

	public RuntimeMetaProperty setStatic(boolean _static) {
		checkCanModify();
		this._static = _static;
		return this;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public RuntimeMetaProperty setDefaultValue(String defaultValue) {
		checkCanModify();
		this.defaultValue = defaultValue;
		return this;
	}

	public RuntimeMetaProperty setAsOwnerOfAssociation(RuntimeMetaAssociation association) {
		new OneToOneWrapper<RuntimeMetaAssociation>() {
			private static final long serialVersionUID = 6095139358596729143L;

			@Override
			protected void createRelationToTargetObject(RuntimeMetaAssociation newTarget) {
				newTarget.checkCanModify();
				newTarget.setOwnerProperty(RuntimeMetaProperty.this);
			}

			@Override
			protected RuntimeMetaAssociation getTargetObjectInSourceObject() {
				return RuntimeMetaProperty.this.association;
			}

			@Override
			protected void removeRelationToTargetObject(
					RuntimeMetaAssociation currentTarget) {
				currentTarget.checkCanModify();
				currentTarget.setOwnerProperty(null);
			}

			@Override
			protected void setTargetObjectInSourceObject(RuntimeMetaAssociation newTarget) {
				RuntimeMetaProperty.this.checkCanModify();
				RuntimeMetaProperty.this.association = newTarget;
			}
		}.updateTargetProperty(association);
		return this;
	}
	
	public RuntimeMetaProperty setAsOwnedByAssociation(RuntimeMetaAssociation association) {
		new OneToOneWrapper<RuntimeMetaAssociation>() {
			private static final long serialVersionUID = -4971432702754240192L;

			@Override
			protected void createRelationToTargetObject(RuntimeMetaAssociation newTarget) {
				newTarget.checkCanModify();
				newTarget.setOwnedProperty(RuntimeMetaProperty.this);
			}

			@Override
			protected RuntimeMetaAssociation getTargetObjectInSourceObject() {
				return RuntimeMetaProperty.this.association;
			}

			@Override
			protected void removeRelationToTargetObject(
					RuntimeMetaAssociation currentTarget) {
				currentTarget.checkCanModify();
				currentTarget.setOwnedProperty(null);
			}

			@Override
			protected void setTargetObjectInSourceObject(RuntimeMetaAssociation newTarget) {
				RuntimeMetaProperty.this.checkCanModify();
				RuntimeMetaProperty.this.association = newTarget;
			}
		}.updateTargetProperty(association);
		return this;
	}
	
	public boolean isPartInAnAssociation() {
		return association != null;
	}

	public RuntimeMetaClazz getType() {
		return type;
	}

	public RuntimeMetaProperty setType(RuntimeMetaClazz type) {
		new ManyToOneWrapper<RuntimeMetaClazz, RuntimeMetaProperty>(this) {
			private static final long serialVersionUID = 4808255763486308091L;

			@SuppressWarnings("unchecked")
			@Override
			protected void addManySideObjectToOneSideCollection(RuntimeMetaClazz oneSide,
					RuntimeMetaProperty manySide) {
				oneSide.checkCanModify();
				 ((OneToManySetWrapper<RuntimeMetaClazz, RuntimeMetaProperty>)oneSide.getPropertiesOfThisType()).getWrappedCollection().add(manySide);
			}

			@Override
			protected RuntimeMetaClazz getOneSideObjectInManySideObject(RuntimeMetaProperty manySide) {
				return manySide.type;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void removeManySideObjectFromOneSideCollection(
					RuntimeMetaClazz oneSide, RuntimeMetaProperty manySide) {
				oneSide.checkCanModify();
				 ((OneToManySetWrapper<RuntimeMetaClazz, RuntimeMetaProperty>)oneSide.getPropertiesOfThisType()).getWrappedCollection().remove(manySide);
			}

			@Override
			protected void setOneSideObjectInManySideObject(RuntimeMetaProperty manySide,
					RuntimeMetaClazz oneSide) {
				manySide.checkCanModify();
				manySide.type = oneSide;
			}
			
		}.updateOneSideObject(type);
		return this;
	}
	
	public CardinalityType getCardinalityType() {
		if (maxCardinality == UNLIMITED) {
			return CardinalityType.Many;
		} else {
			int minMaxDifference = maxCardinality-minCardinality;
			if (minMaxDifference == 0) {
				return CardinalityType.SingleRequired;
			} else if (minMaxDifference == 1) {
				return CardinalityType.SingleOptional;
			} else {
				return CardinalityType.Many;
			}
		}
	}

	public boolean isOwnerOfAssociation() {
		if (isPartInAnAssociation()) {
			if (getAssociation().isBidirectional()) {
				if (getAggregation() == Aggregation.None) {
					if (getAssociation().getOppositeProperty(this).getAggregation() != Aggregation.None) {
						return false;
					} else {
						return getAssociation().getOwnerProperty() == this;
					}
				} else if (getAggregation() == Aggregation.Composite) {
					return true;
				} else if (getAggregation() == Aggregation.Shared) {
					return true;
				}
			} else if (getAssociation().getOwnerProperty() == this) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Translate the {@link CardinalityType}'s for this and the opposite property in the Association into
	 * an {@link AssociationType} <i>(taking into consideration that we here are looking at it from the opposite direction
     * so that a ManyToOne association between this MetaProperty and the Opposite MetaProperty in the assocation,
	 * will read as a One to Many in memory because of UML's way of specifying cardinality - this difference is translated using
	 * this method :)</i>
	 * @return The {@link CardinalityType} or <code>null</code> if the {@link RuntimeMetaProperty} isn't a part in an association (check using {@link #isPartInAnAssociation()})
	 */
	public AssociationType getAssociationType() {
		if (!isPartInAnAssociation()) {
			return null;
		}
		
		// Note: Remember that we here are looking at it from the opposite direction
		// so a ManyToOne association between our MetaProperty and the Opposite MetaProperty,
		// will read as a One to Many because of UML's way of specifying cardinality
		RuntimeMetaProperty oppositeProperty = this.getAssociation().getOppositeProperty(this);
		if (this.getCardinalityType() == CardinalityType.SingleOptional || this.getCardinalityType() == CardinalityType.SingleRequired) {
			if (oppositeProperty.getCardinalityType() == CardinalityType.Many) {
				// m-1
				return AssociationType.ManyToOne;
			} else {
				// 1-1
				return AssociationType.OneToOne;
			}
		} else {
			if (oppositeProperty.getCardinalityType() == CardinalityType.Many) {
				// m-m
				return AssociationType.ManyToMany;
			} else {
				// 1-m
				return AssociationType.OneToMany;
			}
		}
		
	}
	
	@Override
	public String getDescription() {
		return new StringBuilder()
		.append(super.getDescription()).append(newline)
		.append("Cardinality = '").append(minCardinality).append("; ").append(maxCardinality).append("'").append(newline)
		.append("Ordered = '").append(ordered).append("'").append(newline)
		.append("Static = '").append(_static).append("'").append(newline)
		.append("Bounds = '").append(lowerBound != null ? lowerBound : "n/a").append("; ").append(upperBound != null ? upperBound : "n/a").append("'").append(newline)
		.append("Aggregation = '").append(aggregation.toString()).append("'").append(newline)
		.append("IsPartInAssocation = '").append(isPartInAnAssociation()).append("'").append(newline)
		.append("Meta association id = '").append(isPartInAnAssociation() ? getAssociation().getId()  : "n/a").append("'").append(newline)
		.append("IsOwnerOfAssocation = '").append(isOwnerOfAssociation()).append("'").append(newline)
		.append("IsAssocationBidirectional = '").append(isPartInAnAssociation() ? getAssociation().isBidirectional() : "n/a").append("'").append(newline)
		.append("OppositePropertyInAssociation = '").append(isPartInAnAssociation() ? getAssociation().getOppositeProperty(this).getId() : "n/a").append("'")
		.toString();
	} 
}
