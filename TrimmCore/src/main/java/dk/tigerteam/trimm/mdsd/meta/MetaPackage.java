/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

import java.util.List;

/**
 * Models a package. The package compositely contains all {@link MetaClazz} and {@link MetaPackage}' the are defined within it.
 * 
 * @author Jeppe Cramon
 */
public class MetaPackage extends NameableMetaType {
	private static final long serialVersionUID = 5396053811605956505L;

	/**
	 * Some UML tools, such as Enterprise Architect, support marking certain packages as Namespace Roots, which means that the designer
	 * intends for this package to form the Root of the package name
	 */
	private boolean rootPackage;

	public MetaPackage(String id) {
		super(id);
	}

	public boolean isRootPackage() {
		return rootPackage;
	}

	public MetaPackage setRootPackage(boolean rootPackage) {
		this.rootPackage = rootPackage;
		return this;
	}

    /**
     * Searches inside the packages first level of children for a MetaPackage with the given name
     * @param packageName The name of the package
     * @return The package
     * @throws MetaTypeNotFoundException In case it couldn't find a MetaPackage with the given name
     */
    public MetaPackage getPackageByNameRequired(String packageName) {
        return findImmediateChildWithNameAndTypeRequired(packageName, MetaPackage.class);
    }

    /**
     * Searches inside the packages first level of children for a MetaPackage with the given name
     * @param packageName The name of the package
     * @return The package or <code>null</code> if no package was found matching the name
     */
    public MetaPackage getPackageByName(String packageName) {
        return findImmediateChildWithNameAndType(packageName, MetaPackage.class);
    }

    /**
     * Searches inside the meta models first level of children for a MetaClazz with the given name
     * @param clazzName The name of the clazz
     * @return The clazz
     * @throws MetaTypeNotFoundException In case it couldn't find a MetaClazz with the given name
     */
    public MetaClazz getClazzByNameRequired(String clazzName) {
        return findImmediateChildWithNameAndTypeRequired(clazzName, MetaClazz.class);
    }

    /**
     * Searches inside the meta models first level of children for a MetaClazz with the given name
     * @param clazzName The name of the clazz
     * @return The clazz or <code>null</code> if no class was found matching the name
     */
    public MetaClazz getClazzByName(String clazzName) {
        return findImmediateChildWithNameAndType(clazzName, MetaClazz.class);
    }


    /**
     * Searches inside the meta models first level of children for a MetaInterface with the given name
     * @param interfaceName The name of the clazz
     * @return The interface
     * @throws MetaTypeNotFoundException In case it couldn't find a MetaInterface with the given name
     */
    public MetaInterface getInterfaceByNameRequired(String interfaceName) {
        return findImmediateChildWithNameAndTypeRequired(interfaceName, MetaInterface.class);
    }

    /**
     * Searches inside the meta models first level of children for a MetaInterface with the given name
     * @param interfaceName The name of the clazz
     * @return The interface or <code>null</code> if no interface was found matching the name
     */
    public MetaInterface getInterfaceByName(String interfaceName) {
        return findImmediateChildWithNameAndType(interfaceName, MetaInterface.class);
    }


	public List<MetaClazz> getClazzesInPackage() {
		return findImmediateChildrenOfType(MetaClazz.class);
	}

    public List<MetaInterface> getInterfacesInPackage() {
        return findImmediateChildrenOfType(MetaInterface.class);
    }

	public String getFullPackageName() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getName());
		MetaPackage parent = findOwnerOfTypeOrNull(MetaPackage.class);
		while (parent != null) {
			builder.insert(0, ".");
			builder.insert(0, ((MetaPackage) parent).getName());
			if (parent.isRootPackage()) {
				break;
			}

			parent = parent.findOwnerOfTypeOrNull(MetaPackage.class);
		}

		return builder.toString();
	}

	public String getFullPackageNameUsingAliasIfAvailable() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getAliasOrName());
		MetaPackage parent = findOwnerOfTypeOrNull(MetaPackage.class);
		while (parent != null) {
			builder.insert(0, ".");
			builder.insert(0, parent.getAliasOrName());
			if (parent.isRootPackage()) {
				break;
			}

			parent = parent.findOwnerOfTypeOrNull(MetaPackage.class);
		}

		return builder.toString();
	}

}
