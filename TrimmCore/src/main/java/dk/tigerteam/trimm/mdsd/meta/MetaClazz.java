/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

import dk.tigerteam.trimm.bean.bidirectional.ManyToManyListWrapper;
import dk.tigerteam.trimm.bean.bidirectional.ManyToOneWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManySetWrapper;

import java.util.*;

/**
 * Metadata about a Class. Associations to {@link #subClazzes} and {@link #superClazz} is maintained bi-directionally for you :)
 * 
 * @author Jeppe Cramon
 */
public class MetaClazz extends NameableMetaType {
	private static final long serialVersionUID = 3001586979524617221L;
	/**
	 * The key in the Attribute map where templateParameter id's are stored in a List&lt;TemplateParameterId>
	 */
	public static final String TEMPLATE_PARAMETERS_ATTR_KEY = "templateParameters";
	/**
	 * The key in the Attribute map to save the super class id's
	 */
	public static final String GENERALIZATION_ID_ATTR_KEY = "generalization";
	private MetaClazz superClazz;
	private MetaClazz outerClazz;
	private Set<MetaClazz> subClazzes = new HashSet<MetaClazz>();
	private Set<MetaClazz> innerClazzes = new HashSet<MetaClazz>();
	/**
	 * Template binding parameters
	 */
	private List<MetaClazz> templateParameters = new LinkedList<MetaClazz>();
	/**
	 * What MetaInterfaces does this class realize.<br/>
	 * The inverse side of this is {@link MetaInterface#getRealizedBy()}
	 */
	private List<MetaInterface> realizations = new ArrayList<MetaInterface>();

	/**
	 * A set of all properties which are of this type
	 */
	private Set<MetaProperty> propertiesOfThisType = new HashSet<MetaProperty>();
	private boolean builtInType = false;
    private Class<?> wrappedJavaClass;

    /**
	 * Create a {@link MetaClazz} by supplying its unique id
	 * 
	 * @param id
	 *          The unique id
	 */
	public MetaClazz(String id) {
		super(id);
	}

	/**
	 * Define a new {@link MetaClazz} from a built in {@link Class}<br/>
	 * Handles setting the right properties in MetaClazz for you (id, name, fabricated, builtInType)
	 * 
	 * @param clazz
	 *          The class that defined this MetaClazz
	 */
	public MetaClazz(Class<?> clazz) {
		super(UUID.randomUUID().toString());
        wrappedJavaClass = clazz;
        if (clazz.isArray()) {
            setName(clazz.getCanonicalName());
        } else {
		    setName(clazz.getName());
        }
		setFabricated(true);
		builtInType = true;
	}

    public Class<?> getWrappedJavaClass() {
        return wrappedJavaClass;
    }

    /**
	 * Create a {@link MetaClazz} by supplying its unique id and a fabrication information
	 * 
	 * @param id
	 *          The unique id
	 * @param fabricated
	 *          Is this MetaType a result of a fabrication or defined in the original model
	 */
	public MetaClazz(String id, boolean fabricated) {
		super(id, fabricated);
	}

	@Override
	public MetaClazz setName(String name) {
		return (MetaClazz) super.setName(name);
	}

	/**
	 * What MetaInterfaces does this class realize.<br/>
	 * The inverse side of this is {@link MetaInterface#getRealizedBy()}
	 */
	public List<MetaInterface> getRealizations() {
		return new ManyToManyListWrapper<MetaClazz, MetaInterface>(this, realizations) {
			private static final long serialVersionUID = 4869119795837169291L;

			@Override
			protected List<MetaClazz> getSourceCollectionInTarget(MetaInterface o) {
				return o.getRealizedBy();
			}
		};
	}

	/**
	 * Does this Clazz represent an Interface (sugar for checking against the sub type of MetaClazz being used)
	 */
	public boolean isInterface() {
		return false;
	}

	/**
	 * Does this Clazz represent an Association Clazz (sugar for checking against the sub type of MetaClazz being used)
	 */
	public boolean isAssociationClazz() {
		return false;
	}

	/**
	 * Does this Clazz represent an Enumeration Clazz (sugar for checking against the sub type of MetaClazz being used)
	 */
	public boolean isEnumerationClazz() {
		return false;
	}

	public boolean isBuiltInType() {
		return builtInType;
	}

	public MetaClazz setBuiltInType(boolean builtInType) {
		this.builtInType = builtInType;
		return this;
	}

	public boolean isOuterClazz() {
		return this.outerClazz != null;
	}

	public boolean hasInnerClazzes() {
		if (!this.innerClazzes.isEmpty()) {
			return true;
		}
		return false;
	}

	public List<MetaClazz> getTemplateParameters() {
		return templateParameters;
	}

	public MetaClazz addTemplateParameters(MetaClazz... templateParameterClazzes) {
		templateParameters.addAll(Arrays.asList(templateParameterClazzes));
		return this;
	}

	/**
	 * Does this MetaClazz have any Template parameters (generics)?
	 */
	public boolean isTemplateParameterized() {
		return templateParameters.size() > 0;
	}

	public Set<MetaClazz> getSubClazzes() {
		return new OneToManySetWrapper<MetaClazz, MetaClazz>(this, subClazzes) {
			private static final long serialVersionUID = 7801407073213296270L;

			@Override
			protected MetaClazz getOneSideObjectInManySideObject(MetaClazz manySideObject) {
				return manySideObject.getSuperClazz();
			}

			@Override
			protected void setOneSideObjectInManySideObject(MetaClazz manySideObject, MetaClazz oneSideObject) {
				manySideObject.setSuperClazz(oneSideObject);
			}
		};
	}

	public Set<MetaClazz> getInnerClazzes() {
		return new OneToManySetWrapper<MetaClazz, MetaClazz>(this, innerClazzes) {
			private static final long serialVersionUID = 7801407073213296270L;

			@Override
			protected MetaClazz getOneSideObjectInManySideObject(MetaClazz manySideObject) {
				return manySideObject.getOuterClazz();
			}

			@Override
			protected void setOneSideObjectInManySideObject(MetaClazz manySideObject, MetaClazz oneSideObject) {
				manySideObject.setSuperClazz(oneSideObject);
			}
		};
	}

	/**
	 * Sugar - Get all properties of this MetaClazz and then its super MetaClazz, and its super clazz' super clazz, etc. they are ordered from
	 * more specific class (this class) to the least specific (the top most super class)<br/>
	 * Note: Unmodifiable
	 * 
	 * @return list of all properties
	 */
	public List<MetaProperty> getAllPropertiesIncludingSuperClazzProperties() {
		List<MetaProperty> properties = new LinkedList<MetaProperty>();
		MetaClazz currentClazz = this;
		while (currentClazz != null) {
			properties.addAll(currentClazz.getProperties());
			currentClazz = currentClazz.getSuperClazz() != null ? currentClazz.getSuperClazz() : null;
		}
		return Collections.unmodifiableList(properties);
	}

	/**
	 * Sugar method that returns all {@link MetaProperty}'s in {@link #getChildren()} set<br/>
	 * Note: Unmodifiable
	 */
	public List<MetaProperty> getProperties() {
		return findImmediateChildrenOfType(MetaProperty.class);
	}

	/**
	 * Sugar method that returns all {@link MetaOperation}'s in {@link #getChildren()} set<br/>
	 * Note: Unmodifiable
	 */
	public List<MetaOperation> getOperations() {
		return findImmediateChildrenOfType(MetaOperation.class);
	}

	public MetaClazz getSuperClazz() {
		return superClazz;
	}

	public MetaClazz getOuterClazz() {
		return outerClazz;
	}

	public MetaClazz setSuperClazz(MetaClazz superClazz) {
		new ManyToOneWrapper<MetaClazz, MetaClazz>(this) {
			private static final long serialVersionUID = 26992835872857531L;

			@SuppressWarnings("unchecked")
			@Override
			protected void addManySideObjectToOneSideCollection(MetaClazz oneSide, MetaClazz manySide) {
				((OneToManySetWrapper<MetaClazz, MetaClazz>) oneSide.getSubClazzes()).getWrappedCollection().add(manySide);
			}

			@Override
			protected MetaClazz getOneSideObjectInManySideObject(MetaClazz manySide) {
				return manySide.superClazz;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void removeManySideObjectFromOneSideCollection(MetaClazz oneSide, MetaClazz manySide) {
				((OneToManySetWrapper<MetaClazz, MetaClazz>) oneSide.getSubClazzes()).getWrappedCollection().remove(manySide);
			}

			@Override
			protected void setOneSideObjectInManySideObject(MetaClazz manySide, MetaClazz oneSide) {
				manySide.superClazz = oneSide;
			}
		}.updateOneSideObject(superClazz);
		return this;
	}

	public MetaClazz setOuterClazz(MetaClazz outerClazz) {
		new ManyToOneWrapper<MetaClazz, MetaClazz>(this) {
			private static final long serialVersionUID = 26992835872857531L;

			@SuppressWarnings("unchecked")
			@Override
			protected void addManySideObjectToOneSideCollection(MetaClazz oneSide, MetaClazz manySide) {
				((OneToManySetWrapper<MetaClazz, MetaClazz>) oneSide.getInnerClazzes()).getWrappedCollection().add(manySide);
			}

			@Override
			protected MetaClazz getOneSideObjectInManySideObject(MetaClazz manySide) {
				return manySide.outerClazz;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void removeManySideObjectFromOneSideCollection(MetaClazz oneSide, MetaClazz manySide) {
				((OneToManySetWrapper<MetaClazz, MetaClazz>) oneSide.getInnerClazzes()).getWrappedCollection().remove(manySide);
			}

			@Override
			protected void setOneSideObjectInManySideObject(MetaClazz manySide, MetaClazz oneSide) {
				manySide.outerClazz = oneSide;
			}
		}.updateOneSideObject(outerClazz);
		return this;
	}

	/**
	 * A set of all properties which are of this type
	 */
	public Set<MetaProperty> getPropertiesOfThisType() {
		return new OneToManySetWrapper<MetaClazz, MetaProperty>(this, propertiesOfThisType) {
			private static final long serialVersionUID = 7729899197663960058L;

			@Override
			protected MetaClazz getOneSideObjectInManySideObject(MetaProperty manySideObject) {
				return manySideObject.getType();
			}

			@Override
			protected void setOneSideObjectInManySideObject(MetaProperty manySideObject, MetaClazz oneSideObject) {
				manySideObject.setType(oneSideObject);
			}
		};
	}

	public MetaPackage getPackage() {
		return findOwnerOfTypeOrNull(MetaPackage.class);
	}

	/**
	 * Get the Fully Qualified Class Name (Java style)
	 */
	public String getFQCN() {
		StringBuilder fqcn = new StringBuilder();

		MetaPackage package_ = getPackage();
		if (package_ != null) {
			fqcn.append(package_.getFullPackageName());
		}

		LinkedList<MetaClazz> clazzes = new LinkedList<MetaClazz>();
		clazzes.add(this);
		MetaType owner = this.getOwner();
		while (owner instanceof MetaClazz) {
			clazzes.add((MetaClazz) owner);
			owner = owner.getOwner();
		}

		ListIterator<MetaClazz> listIterator = clazzes.listIterator(clazzes.size());
		while (listIterator.hasPrevious()) {
			MetaClazz clazz = listIterator.previous();

			if (fqcn.length() > 0) {
				fqcn.append(".");
			}
			fqcn.append(clazz.getName());
		}

		return fqcn.toString();
	}

	/**
	 * Get the Fully Qualified Class Name (Java style)
	 */
	public String getFQCNUsingAliasIfAvailable() {
		StringBuilder fqcn = new StringBuilder();

		MetaPackage package_ = getPackage();
		if (package_ != null) {
			fqcn.append(package_.getFullPackageNameUsingAliasIfAvailable());
		}

		LinkedList<MetaClazz> clazzes = new LinkedList<MetaClazz>();
		clazzes.add(this);
		MetaType owner = this.getOwner();
		while (owner instanceof MetaClazz) {
			clazzes.add((MetaClazz) owner);
			owner = owner.getOwner();
		}

		ListIterator<MetaClazz> listIterator = clazzes.listIterator(clazzes.size());
		while (listIterator.hasPrevious()) {
			MetaClazz clazz = listIterator.previous();

			if (fqcn.length() > 0) {
				fqcn.append(".");
			}
			fqcn.append(clazz.getAliasOrName());
		}

		return fqcn.toString();
	}

	public boolean isArray() {
		return getName().endsWith("[]");
	}

	public List<MetaProperty> getPropertiesNotPartInAnAssociation() {
		List<MetaProperty> result = new LinkedList<MetaProperty>();
		for (MetaProperty metaProperty : getProperties()) {
			if (!metaProperty.isPartInAnAssociation()) {
				result.add(metaProperty);
			}
		}
		return result;
	}

	public List<MetaProperty> getPropertiesPartInAnAssociation() {
		List<MetaProperty> result = new LinkedList<MetaProperty>();
		for (MetaProperty metaProperty : getProperties()) {
			if (metaProperty.isPartInAnAssociation()) {
				result.add(metaProperty);
			}
		}
		return result;
	}
	
	/**
	 * Find a property by its name. If you need an exception if it's not found use {@link #getPropertyByNameRequired(String)}
	 * @param propertyByName The name of the property
	 * @return The property instance, if found, or <code>null</code> if not found
	 */
	public MetaProperty getPropertyByName(String propertyByName) {
		for (MetaProperty property : getProperties()) {
			if (propertyByName.equals(property.getName())) {
				return property;
			}
		}
		return null;
	}
	
	/**
	 * Find a property by its name. If you don't want an exception if it's not found use {@link #getPropertyByName(String)}
	 * @param propertyByName The name of the property
	 * @return The property instance, if found
	 * @throws IllegalArgumentException if the property can't be found
	 */
	public MetaProperty getPropertyByNameRequired(String propertyByName) {
		MetaProperty property = getPropertyByName(propertyByName);
		if (property != null) {
			return property;
		} else {
			throw new IllegalArgumentException("Couldn't find property by name '" + propertyByName + "' in " + this.toString());
		}
	}
	
	/**
	 * Find an Operation by its name
	 * @param operationName The name of the operation
	 * @return The operation instance if found, otherwise returns <code>null</code>
	 */
	public MetaOperation getOperationByName(String operationName) {
		for (MetaOperation operation : getOperations()) {
			if (operationName.equals(operation.getName())) {
				return operation;
			}
		}
		return null;
	}

	/**
	 * Find an Operation by its name
	 * @param operationName The name of the operation
	 * @return The operation instance if found
	 * @throws IllegalArgumentException If the operation couldn't be found
	 */
	public MetaOperation getOperationByNameRequired(String operationName) {
		MetaOperation operation = getOperationByName(operationName);
		if (operation != null) {
			return operation;
		} else {
			throw new IllegalArgumentException("Couldn't find Operation by name '" + operationName + "'");
		}
	}

    public static MetaClazz Void() {
        MetaClazz voidClazz = new MetaClazz(UUID.randomUUID().toString(), true);
        voidClazz.setName("void");
        return voidClazz;
    }

    public boolean doesInheritStereoType(String stereotypeName) {
        MetaClazz currentSuperClazz = this.getSuperClazz();
        while (currentSuperClazz != null) {
            if (currentSuperClazz.hasStereoType(stereotypeName)) return true;
            currentSuperClazz = currentSuperClazz.getSuperClazz();
        }
        return false;
    }
}
