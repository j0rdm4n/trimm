/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

import dk.tigerteam.trimm.bean.bidirectional.ManyToOneWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManySetWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToOneWrapper;

import java.util.UUID;

/**
 * Models a Property (name, type, cardinality and association).<p/>
 * <h3>Cardinality</h3>
 * Cardinality indicates how many instances of the given property can exist at one time. This is specified using the {@link #minCardinality} and
 * {@link #maxCardinality} properties (unlimited has value -1 {@link #CARDINALITY_UNLIMITED}). A user friendly way to read the cardinality is to use {@link #getCardinalityType()}
 * {@link CardinalityType#Many}. Example:
 * <table>
 * 	<tr>
 * 		<th>Min cardinality</th><th>Max cardinality</th><th>Cardinality type</th>
 *  </tr>
* 	<tr><td>0</td><td>1</td><td>{@link CardinalityType#SingleOptional}</td></tr>
 *    <tr><td>1</td><td>1</td><td>{@link CardinalityType#SingleRequired}</td></tr>
 *    <tr><td>0</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
 *    <tr><td>1</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
 * </table>
 * <h3>Bounds</h3>Car
 * Bounds are not like cardinality. {@link #upperBound} and {@link #lowerBound} indicate the value range for a property, 
 * for instance the min and max length for a String.<br/>
 * The bound is specified after the type name inside a parenthesis.<br/>
 * Example: Max string length is 60: <code>String (60)</code> or min length is 3 and max is 255 <code>String (3, 255)</code>
 * 
 * @author Jeppe Cramon
 */
public class MetaProperty extends NameableMetaType implements Bounded {
	private static final long serialVersionUID = -5188669990498570213L;
	/**
	 * The Attribute map key for storing information about association idref in properties 
	 */
	public static final String ASSOCIATION_ATTR_KEY = "association";
	/**
	 * The Attribute map key for storing information about the type idref in properties 
	 */
	public static final String TYPE_ATTR_KEY = "type";
	/**
	 * The Attribute map key for storing information about if the type referenced by TYPE_ATTR_KEY is a built in type (ie. doesn't have a corresponding MetaClazz) 
	 */
	public static final String TYPE_IS_BUILTIN_ATTR_KEY = "typeBuiltIn";
	/**
	 * The Attribute map key for storing information about the boolean value ownership of an eventual association in properties
	 */
	public static final String IS_ASSOCIATION_OWNER_ATTR_KEY = "associationOwnership";
	/**
	 * Value for an unlimited (unbounded) cardinality
	 */
	public static int CARDINALITY_UNLIMITED = -1;

	/**
	 * The name of stereotype that can be applied to a role/association-end/property to specify that it's the owner of the association
	 */
	public static final String OWNER_OF_ASSOCIATION_STEREOTYPE = "owner";
	/**
	 * The name of the key, that contains information about whether the MetaClazz is referenced (i.e. is defined in another MetaModel -> See
	 * {@link MetaModel#getReferencedMetaModel(String)}) <br/>
	 * Note: The format of the value, pointed to by {@link #TYPE_ATTR_KEY} is Xmi tool specific (e.g. MagicDraw uses format
	 * <code>ReferencedMetaModelName#idref</code>)
	 */
	public static final String TYPE_IS_REFERENCED_ATTR_KEY = "isReferenced";

	/**
	 * Stereotype that can be applied to properties to indicate that the property is immutable (must be initialized at Object creation time
	 * and not be writeable)
	 */
	public static final String IMMUTABLE_STEREOTYPE_NAME = "Immutable";
	/**
	 * The tagged value that can be used with Enumeration Literals to indicate a construction time property values (being passed to a
	 * fabricated constructor)
	 */
	public static final String ENUM_LITERAL_VALUE_TAGGEDVALUE_NAME = "literalInitValue";

	/**
	 * Stereotype name that corresponds to the Hibernate Validator NotNull
	 * annotation
	 */
	public static final String NOT_NULL_STEREOTYPE = "NotNull";
	/**
	 * Is this MetaProperty an Enumeration Literal ?
	 */
	private boolean enumerationLiteral;

    /**
	 * The different types of Aggregations
	 * 
	 * @author Jeppe Cramon
	 */
	public enum Aggregation {
		/**
		 * Also known as "Aggregated"
		 */
		Shared, Composite, None
	}
	public enum AssociationType {
		OneToOne, OneToMany, ManyToOne, ManyToMany
	}
	/**
	 * And Object interpretation of #minCardinality and #maxCardinality
	 */
	public enum CardinalityType {
        // We don't care if many is required or optional at this level
		SingleRequired("1..1"), SingleOptional("0..1"), Many("0..*");

		private String umlNotation;

		private CardinalityType(String umlNotation) {
			this.umlNotation = umlNotation;
		}

		public String umlNotation() {
			return umlNotation;
		}
	}
	
	private Aggregation aggregation = Aggregation.None;
	private int minCardinality = 1;
	private int maxCardinality = 1;
	private boolean derived = false;
	private boolean ordered = false;
	private MetaClazz type;
	private MetaAssociation association;
	private boolean _static;
	private String defaultValue;
	private Long lowerBound;
	private Long upperBound;
	private boolean readable = true;
	private boolean writeable = true;

    /**
     * Copies all primitive values including the Type information. Doesn't clone Association value, owner and attribute values.<br/>
     * The children of type MetaStereotype are copied (so you end up with two child collections that contain the same stereootypes)<br/>
     * The property is assigned a new unique id
     */
    public MetaProperty cloneSimplePropertyValues() {
        MetaProperty clone = new MetaProperty();
        clone.setName(this.getName());
        clone.setAlias(this.getAlias());
        clone.setDocumentation(this.getDocumentation());
        clone.setAbstract(this.isAbstract());
        clone.setVisibility(this.getVisibility());
        clone.setTaggedValues(this.getTaggedValues());
        for (MetaType child : this.getChildren()) {
            if (child instanceof MetaStereoType)
                clone.add(((MetaStereoType)child).cloneStereotype());
        }

        clone.aggregation = this.aggregation;
        clone.minCardinality = this.minCardinality;
        clone.maxCardinality = this.maxCardinality;
        clone.derived = this.derived;
        clone.ordered = this.ordered;
        clone.setType(this.getType());
        clone._static = this._static;
        clone.defaultValue = this.defaultValue;
        clone.lowerBound = this.lowerBound;
        clone.upperBound = this.upperBound;
        clone.readable = this.readable;
        clone.writeable = this.writeable;
        return clone;
    }


    /**
	 * Create a {@link MetaProperty} by supplying its unique id
	 * @param id The unique id
	 */
	public MetaProperty(String id) {
		super(id);
	}
	
	/**
	 * Create a {@link MetaProperty} by supplying its unique id and a fabrication information
	 * @param id The unique id
	 * @param fabricated Is this MetaType a result of a fabrication or defined in the original model
	 */
	public MetaProperty(String id, boolean fabricated) {
		super(id, fabricated);
	}

    /**
     * Creates a fabricated MetaProperty with a generated id
     */
    public MetaProperty() {
        super();
    }

	/**
	 * Checks if the Property is an enumeration literal
	 */
	public boolean isEnumerationLiteral() {
		return enumerationLiteral;
	}

	/**
	 * Set if the Property is an enumeration literal
	 */
	public void setEnumerationLiteral(boolean enumerationLiteral) {
		this.enumerationLiteral = enumerationLiteral;
	}

	/**
	 * Is this Meta property readable (default is true)
	 */
	public boolean isReadable() {
		return readable;
	}

	/**
	 * Set if this Meta property is readable (default is true) 
	 */
	public MetaProperty setReadable(boolean readable) {
		this.readable = readable;
		return this;
	}


	/**
	 * Is this Meta property writeable (default is true) 
	 */
	public boolean isWriteable() {
		return writeable;
	}

	/**
	 * Set if this Meta property is writeable (default is true) 
	 */
	public MetaProperty setWriteable(boolean writeable) {
		this.writeable = writeable;
		return this;
	}


	/**
	 * The lower bound for the property (for Strings this is typically minimum length and for Numbers the smallest number allowed) 
	 */
	public Long getLowerBound() {
		return lowerBound;
	}

	/**
	 * The lower bound for the property (for Strings this is typically minimum length and for Numbers the smallest number allowed) 
	 */
	public MetaProperty setLowerBound(Long lowerBound) {
		this.lowerBound = lowerBound;
		return this;
	}

	/**
	 * The upper bound for the property (for Strings this is typically maximum length and for Numbers the largest number allowed) 
	 */
	public Long getUpperBound() {
		return upperBound;
	}

	/**
	 * The upper bound for the property (for Strings this is typically maximum length and for Numbers the largest number allowed) 
	 */
	public MetaProperty setUpperBound(Long upperBound) {
		this.upperBound = upperBound;
		return this;
	}



  /**
   * <h3>Cardinality</h3>
   * Cardinality indicates how many instances of the given property can exist at one time. This is specified using the {@link #minCardinality} and
   * {@link #maxCardinality} properties (unlimited has value -1 aka {@link #CARDINALITY_UNLIMITED}). A user friendly way to read the cardinality is to use {@link #getCardinalityType()}
   * {@link CardinalityType#Many}. Example:
   * <table>
   * 	<tr>
   * 		<th>Min cardinality</th><th>Max cardinality</th><th>Cardinality type</th>
   *  </tr>
   * 	<tr><td>0</td><td>1</td><td>{@link CardinalityType#SingleOptional}</td></tr>
   *    <tr><td>1</td><td>1</td><td>{@link CardinalityType#SingleRequired}</td></tr>
   *    <tr><td>0</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   *    <tr><td>1</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   * </table>
   */
	public int getMinCardinality() {
		return minCardinality;
	}

  /**
   * <h3>Cardinality</h3>
   * Cardinality indicates how many instances of the given property can exist at one time. This is specified using the {@link #minCardinality} and
   * {@link #maxCardinality} properties (unlimited has value -1 aka {@link #CARDINALITY_UNLIMITED}). A user friendly way to read the cardinality is to use {@link #getCardinalityType()}
   * {@link CardinalityType#Many}. Example:
   * <table>
   * 	<tr>
   * 		<th>Min cardinality</th><th>Max cardinality</th><th>Cardinality type</th>
   *  </tr>
   * 	<tr><td>0</td><td>1</td><td>{@link CardinalityType#SingleOptional}</td></tr>
   *    <tr><td>1</td><td>1</td><td>{@link CardinalityType#SingleRequired}</td></tr>
   *    <tr><td>0</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   *    <tr><td>1</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   * </table>
   */
	public MetaProperty setMinCardinality(int minCardinality) {
		this.minCardinality = minCardinality;
		return this;
	}

  /**
   * Null safe<br/>
   * <h3>Cardinality</h3>
   * Cardinality indicates how many instances of the given property can exist at one time. This is specified using the {@link #minCardinality} and
   * {@link #maxCardinality} properties (unlimited has value -1 aka {@link #CARDINALITY_UNLIMITED}). A user friendly way to read the cardinality is to use {@link #getCardinalityType()}
   * {@link CardinalityType#Many}. Example:
   * <table>
   * 	<tr>
   * 		<th>Min cardinality</th><th>Max cardinality</th><th>Cardinality type</th>
   *  </tr>
   * 	<tr><td>0</td><td>1</td><td>{@link CardinalityType#SingleOptional}</td></tr>
   *    <tr><td>1</td><td>1</td><td>{@link CardinalityType#SingleRequired}</td></tr>
   *    <tr><td>0</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   *    <tr><td>1</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   * </table>
   */
	public MetaProperty setMinCardinality(Integer minCardinality) {
		if (minCardinality != null) {
			setMinCardinality(minCardinality.intValue());
		}
		return this;
	}

  /**
   * <h3>Cardinality</h3>
   * Cardinality indicates how many instances of the given property can exist at one time. This is specified using the {@link #minCardinality} and
   * {@link #maxCardinality} properties (unlimited has value -1 aka {@link #CARDINALITY_UNLIMITED}). A user friendly way to read the cardinality is to use {@link #getCardinalityType()}
   * {@link CardinalityType#Many}. Example:
   * <table>
   * 	<tr>
   * 		<th>Min cardinality</th><th>Max cardinality</th><th>Cardinality type</th>
   *  </tr>
   * 	<tr><td>0</td><td>1</td><td>{@link CardinalityType#SingleOptional}</td></tr>
   *    <tr><td>1</td><td>1</td><td>{@link CardinalityType#SingleRequired}</td></tr>
   *    <tr><td>0</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   *    <tr><td>1</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   * </table>
   */
	public int getMaxCardinality() {
		return maxCardinality;
	}

  /**
   * <h3>Cardinality</h3>
   * Cardinality indicates how many instances of the given property can exist at one time. This is specified using the {@link #minCardinality} and
   * {@link #maxCardinality} properties (unlimited has value -1 aka {@link #CARDINALITY_UNLIMITED}). A user friendly way to read the cardinality is to use {@link #getCardinalityType()}
   * {@link CardinalityType#Many}. Example:
   * <table>
   * 	<tr>
   * 		<th>Min cardinality</th><th>Max cardinality</th><th>Cardinality type</th>
   *  </tr>
   * 	<tr><td>0</td><td>1</td><td>{@link CardinalityType#SingleOptional}</td></tr>
   *    <tr><td>1</td><td>1</td><td>{@link CardinalityType#SingleRequired}</td></tr>
   *    <tr><td>0</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   *    <tr><td>1</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   * </table>
   */
	public MetaProperty setMaxCardinality(int maxCardinality) {
		this.maxCardinality = maxCardinality;
		return this;
	}

  /**
   * Null safe<br/>
   * <h3>Cardinality</h3>
   * Cardinality indicates how many instances of the given property can exist at one time. This is specified using the {@link #minCardinality} and
   * {@link #maxCardinality} properties (unlimited has value -1 aka {@link #CARDINALITY_UNLIMITED}). A user friendly way to read the cardinality is to use {@link #getCardinalityType()}
   * {@link CardinalityType#Many}. Example:
   * <table>
   * 	<tr>
   * 		<th>Min cardinality</th><th>Max cardinality</th><th>Cardinality type</th>
   *  </tr>
   * 	<tr><td>0</td><td>1</td><td>{@link CardinalityType#SingleOptional}</td></tr>
   *    <tr><td>1</td><td>1</td><td>{@link CardinalityType#SingleRequired}</td></tr>
   *    <tr><td>0</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   *    <tr><td>1</td><td>-1</td><td>{@link CardinalityType#Many}</td></tr>
   * </table>
   */
	public MetaProperty setMaxCardinality(Integer maxCardinality) {
		if (maxCardinality != null) {
			setMaxCardinality(maxCardinality.intValue());
		}
		return this;
	}

	public boolean isDerived() {
		return derived;
	}

	public MetaProperty setDerived(boolean derived) {
		this.derived = derived;
		return this;
	}

	public boolean isOrdered() {
		return ordered;
	}

	public MetaProperty setOrdered(boolean ordered) {
		this.ordered = ordered;
		return this;
	}

	public MetaClazz getOwnerClazz() {
		return (MetaClazz) getOwner();
	}
		
	public Aggregation getAggregation() {
		return aggregation;
	}

	public MetaProperty setAggregation(Aggregation aggregation) {
		this.aggregation = aggregation;
		return this;
	}
	
	public MetaAssociation getAssociation() {
		return association;
	}
	
	
	public boolean isStatic() {
		return _static;
	}

	public MetaProperty setStatic(boolean _static) {
		this._static = _static;
		return this;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public MetaProperty setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
		return this;
	}

	public MetaProperty setAsOwnerOfAssociation(MetaAssociation association) {
		new OneToOneWrapper<MetaAssociation>() {
			private static final long serialVersionUID = 6095139358596729143L;

			@Override
			protected void createRelationToTargetObject(MetaAssociation newTarget) {
				newTarget.setOwnerProperty(MetaProperty.this);
			}

			@Override
			protected MetaAssociation getTargetObjectInSourceObject() {
				return MetaProperty.this.association;
			}

			@Override
			protected void removeRelationToTargetObject(MetaAssociation currentTarget) {
				currentTarget.setOwnerProperty(null);
			}

			@Override
			protected void setTargetObjectInSourceObject(MetaAssociation newTarget) {
				MetaProperty.this.association = newTarget;
			}
		}.updateTargetProperty(association);
		return this;
	}
	
	public MetaProperty setAsOwnedByAssociation(MetaAssociation association) {
		new OneToOneWrapper<MetaAssociation>() {
			private static final long serialVersionUID = -4971432702754240192L;

			@Override
			protected void createRelationToTargetObject(MetaAssociation newTarget) {
				newTarget.setOwnedProperty(MetaProperty.this);
			}

			@Override
			protected MetaAssociation getTargetObjectInSourceObject() {
				return MetaProperty.this.association;
			}

			@Override
			protected void removeRelationToTargetObject(MetaAssociation currentTarget) {
				currentTarget.setOwnedProperty(null);
			}

			@Override
			protected void setTargetObjectInSourceObject(MetaAssociation newTarget) {
				MetaProperty.this.association = newTarget;
			}
		}.updateTargetProperty(association);
		return this;
	}
	
	public boolean isPartInAnAssociation() {
		return association != null;
	}

	public MetaClazz getType() {
		return type;
	}

	public MetaProperty setType(MetaClazz type) {
		new ManyToOneWrapper<MetaClazz, MetaProperty>(this) {
			private static final long serialVersionUID = 4808255763486308091L;

			@SuppressWarnings("unchecked")
			@Override
			protected void addManySideObjectToOneSideCollection(MetaClazz oneSide, MetaProperty manySide) {
				((OneToManySetWrapper<MetaClazz, MetaProperty>) oneSide.getPropertiesOfThisType()).getWrappedCollection().add(manySide);
			}

			@Override
			protected MetaClazz getOneSideObjectInManySideObject(MetaProperty manySide) {
				return manySide.type;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void removeManySideObjectFromOneSideCollection(MetaClazz oneSide, MetaProperty manySide) {
				((OneToManySetWrapper<MetaClazz, MetaProperty>) oneSide.getPropertiesOfThisType()).getWrappedCollection().remove(manySide);
			}

			@Override
			protected void setOneSideObjectInManySideObject(MetaProperty manySide, MetaClazz oneSide) {
				manySide.type = oneSide;
			}

		}.updateOneSideObject(type);
		return this;
	}
	
	public CardinalityType getCardinalityType() {
		if (maxCardinality == CARDINALITY_UNLIMITED) {
			return CardinalityType.Many;
		} else {
			int minMaxDifference = maxCardinality - minCardinality;
			if (minMaxDifference == 0) {
				return CardinalityType.SingleRequired;
			} else if (minMaxDifference == 1) {
				return CardinalityType.SingleOptional;
			} else {
				return CardinalityType.Many;
			}
		}
	}

	/**
	 * A specialized method for deciding which property should own the association. Allows for overriding
	 * using a Stereotype by name of {@link #OWNER_OF_ASSOCIATION_STEREOTYPE}.<br/>
	 * This method interprets on the type of {@link Aggregation} used in the model and falls back to the raw
	 * UML decided owner which can be read using {@link MetaAssociation#getOwnerProperty()}.
	 */
	public boolean isOwnerOfAssociation() {
		if (isPartInAnAssociation()) {
			if (this.hasStereoType(OWNER_OF_ASSOCIATION_STEREOTYPE)) {
				return true;
			} else if (association.getOppositeProperty(this).hasStereoType(OWNER_OF_ASSOCIATION_STEREOTYPE)) {
				return false;
			}

			if (getAssociation().isBidirectional()) {
				if (getAggregation() == Aggregation.None) {
					if (getAssociation().getOppositeProperty(this).getAggregation() != Aggregation.None) {
						return false;
					} else {
						return getAssociation().getOwnerProperty() == this;
					}
				} else if (getAggregation() == Aggregation.Composite) {
					return true;
				} else if (getAggregation() == Aggregation.Shared) {
					return true;
				}
			} else if (getAggregation() == Aggregation.Composite) {
				return true;
			} else if (getAggregation() == Aggregation.Shared) {
				return true;
			} else if (getAssociation().getOppositeProperty(this).getAggregation() == Aggregation.None
					&& getAssociation().getOwnerProperty() == this) {
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * Translate the {@link CardinalityType}'s for this and the opposite property in the Association into
	 * an {@link AssociationType} <i>(taking into consideration that we here are looking at it from the opposite direction
     * so that a ManyToOne association between this MetaProperty and the Opposite MetaProperty in the assocation,
	 * will read as a One to Many in memory because of UML's way of specifying cardinality - this difference is translated using
	 * this method :)</i>
	 * @return The {@link CardinalityType} or <code>null</code> if the {@link MetaProperty} isn't a part in an association (check using {@link #isPartInAnAssociation()})
	 */
	public AssociationType getAssociationType() {
		if (!isPartInAnAssociation()) {
			return null;
		}
		
		// Note: Remember that we here are looking at it from the opposite direction
		// so a ManyToOne association between our MetaProperty and the Opposite MetaProperty,
		// will read as a One to Many because of UML's way of specifying cardinality
		MetaProperty oppositeProperty = this.getAssociation().getOppositeProperty(this);
		if (this.getCardinalityType() == CardinalityType.SingleOptional || this.getCardinalityType() == CardinalityType.SingleRequired) {
			if (oppositeProperty.getCardinalityType() == CardinalityType.Many) {
				// m-1
				return AssociationType.ManyToOne;
			} else {
				// 1-1
				return AssociationType.OneToOne;
			}
		} else {
			if (oppositeProperty.getCardinalityType() == CardinalityType.Many) {
				// m-m
				return AssociationType.ManyToMany;
			} else {
				// 1-m
				return AssociationType.OneToMany;
			}
		}
		
	}
	
	@Override
	public String getDescription() {
		return new StringBuilder()
		.append(super.getDescription()).append("<br/>").append(newline)
		.append("Cardinality = '").append(minCardinality).append("; ").append(maxCardinality).append("'<br/>").append(newline)
		.append("Ordered = '").append(ordered).append("'<br/>").append(newline)
		.append("Static = '").append(_static).append("'<br/>").append(newline)
		.append("Bounds = '").append(lowerBound != null ? lowerBound : "n/a").append("; ").append(upperBound != null ? upperBound : "n/a").append("'<br/>").append(newline)
		.append("Aggregation = '").append(aggregation.toString()).append("'<br/>").append(newline)
		.append("IsPartInAssocation = '").append(isPartInAnAssociation()).append("'<br/>").append(newline)
		.append("Meta association id = '").append(isPartInAnAssociation() ? getAssociation().getId()  : "n/a").append("'<br/>").append(newline)
		.append("IsOwnerOfAssocation = '").append(isOwnerOfAssociation()).append("'<br/>").append(newline)
		.append("IsAssocationBidirectional = '").append(isPartInAnAssociation() ? getAssociation().isBidirectional() : "n/a").append("'<br/>").append(newline)
		.append("AssociationType = '").append(isPartInAnAssociation() ? getAssociationType().toString(): "n/a").append("'<br/>").append(newline)
		.append("OppositePropertyInAssociation = '").append(isPartInAnAssociation() ? getAssociation().getOppositeProperty(this).getId() : "n/a").append("'")
		.toString();
	}

	/**
	 * Get the Opposite {@link MetaProperty} in this {@link MetaProperty}'s {@link MetaAssociation}<br/>
	 * A shorthand method for calling <code>this.getAssociation().getOppositeProperty(this);</code>
	 * 
	 * @return The Opposite {@link MetaProperty}
	 * @throws IllegalStateException
	 *           In case this {@link MetaProperty} isn't part in an Association (use {@link #isPartInAnAssociation()} as a guard method before
	 *           calling)
	 */
	public MetaProperty getOppositePropertyInAssociation() {
		if (this.isPartInAnAssociation()) {
			return this.getAssociation().getOppositeProperty(this);
		} else {
			throw new IllegalStateException("MetaProperty " + this + " isn't part in an association");
		}
	}
}
