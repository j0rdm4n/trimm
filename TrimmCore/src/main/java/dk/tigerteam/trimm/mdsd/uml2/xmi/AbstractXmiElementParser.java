/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi;

import dk.tigerteam.trimm.mdsd.meta.MetaProperty.Aggregation;
import dk.tigerteam.trimm.mdsd.meta.MetaType.Visibility;
import dk.tigerteam.trimm.util.Utils;
import org.jdom2.Namespace;

/**
 * Convenience implementation of {@link XmiElementParser} which gives a few utility methods which are useful during parsing
 *
 * @author Jeppe Cramon
 */
public abstract class AbstractXmiElementParser implements XmiElementParser {
    private XmiReader xmiReader;

    @Override
    public XmiReader getXmiReader() {
        return xmiReader;
    }

    @Override
    public void setXmiReader(XmiReader xmiReader) {
        assert xmiReader != null;
        this.xmiReader = xmiReader;
    }

    /**
     * Sugar method for reading the UmlNs from the XmiReader
     */
    protected Namespace getUmlNs() {
        return xmiReader.getUmlNs();
    }

    /**
     * Sugar method for reading the XmiNs from the XmiReader
     */
    protected Namespace getXmiNs() {
        return xmiReader.getXmiNs();
    }

    /**
     * Get the {@link Visibility} of the {@link XmiElement}
     *
     * @param xmiElement The xmi element
     * @return The {@link Visibility} or <code>null</code> if no visibility has been specified
     */
    protected Visibility getVisibility(XmiElement xmiElement) {
        String umlVisibility = xmiElement.getUmlVisibility();
        if (umlVisibility != null)
            return Visibility.valueOf(Utils.capitalize(umlVisibility));
        else
            return null;
    }

    /**
     * Get the {@link Aggregation} for the {@link XmiElement}
     *
     * @param xmiElement The xmi element
     * @return The aggregation or {@link Aggregation#None} if unspecified
     */
    protected Aggregation getAggregation(XmiElement xmiElement) {
        String umlAggregation = xmiElement.getUmlAggregation();
        if (umlAggregation != null)
            return Aggregation.valueOf(Utils.capitalize(umlAggregation));
        else
            return Aggregation.None;
    }
}
