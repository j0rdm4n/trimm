/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

/**
 * Represents a method parameter (or return value) in UML
 * @author Jeppe Cramon
 */
public class MetaParameter extends NameableMetaType implements Bounded {
	
	private static final long serialVersionUID = 1816761405984798122L;


    /**
	 * The parameter direction
	 * @author Jeppe Cramon
	 */
	public enum Direction { In, Out, InOut, Return };
    /**
     * Value for an unlimited (unbounded) cardinality
     */
    public static int CARDINALITY_UNLIMITED = -1;

	private Direction direction = Direction.In;
	private MetaClazz type;
	private Long lowerBound;
	private Long upperBound;
    private int minCardinality = 1;
    private int maxCardinality = 1;

    public static MetaParameter VoidReturnParameter() {
        MetaParameter metaParameter = new MetaParameter();
        metaParameter.setType(MetaClazz.Void());
        metaParameter.direction = Direction.Return;
        return metaParameter;
    }

    /**
     * Creates a fabricated MetaParameter with a generated id (UUID based)
     */
    public MetaParameter() {
    }

    public MetaParameter(String id) {
		super(id);
	}

	public MetaParameter(String id, boolean fabricated) {
		super(id, fabricated);
	}

    public MetaParameter(String name, MetaClazz type) {
        super();
        this.setName(name);
        this.type = type;
    }

	/**
	 * The lower bound for the property (for Strings this is typically minimum length and for Numbers the smallest number allowed) 
	 */
	public Long getLowerBound() {
		return lowerBound;
	}

	/**
	 * The lower bound for the property (for Strings this is typically minimum length and for Numbers the smallest number allowed) 
	 */
	public MetaParameter setLowerBound(Long lowerBound) {
		this.lowerBound = lowerBound;
		return this;
	}

	/**
	 * The upper bound for the property (for Strings this is typically maximum length and for Numbers the largest number allowed) 
	 */
	public Long getUpperBound() {
		return upperBound;
	}

	/**
	 * The upper bound for the property (for Strings this is typically maximum length and for Numbers the largest number allowed) 
	 */
	public MetaParameter setUpperBound(Long upperBound) {
		this.upperBound = upperBound;
		return this;
	}

    /**
     * Min Cardinality for the parameter - default 1
     */
    public int getMinCardinality() {
        return minCardinality;
    }

    /**
     * Min Cardinality for the parameter - default 1
     * @param minCardinality
     */
    public void setMinCardinality(int minCardinality) {
        this.minCardinality = minCardinality;
    }

    /**
     * Max Cardinality for the parameter - default 1
     * @see #CARDINALITY_UNLIMITED
     */
    public int getMaxCardinality() {
        return maxCardinality;
    }

    /**
     * Max Cardinality for the parameter - default 1
     * @see #CARDINALITY_UNLIMITED
     * @param maxCardinality
     */
    public void setMaxCardinality(int maxCardinality) {
        this.maxCardinality = maxCardinality;
    }

    /**
	 * The parameter direction
	 */
	public Direction getDirection() {
		return direction;
	}

	/**
	 * The parameter direction
	 * @return this
	 */
	public MetaParameter setDirection(Direction direction) {
		this.direction = direction;
		return this;
	}

	public MetaClazz getType() {
		return type;
	}

	public MetaParameter setType(MetaClazz type) {
		this.type = type;
		return this;
	}
	
	@Override
	public String getDescription() {
		StringBuilder builder = new StringBuilder()
		.append(getDocumentation() != null ? getDocumentation() : "No documentation").append("<br/>").append(newline)
		.append(super.getDescription()).append("<br/>").append(newline)
		.append("Direction = '").append(direction);
		
		return builder.toString();	
	}

    @Override
    public String toString() {
        return "MetaParameter{" +
                "name=" + getName() +
                ", direction=" + direction +
                ", type=" + type +
                ", lowerBound=" + lowerBound +
                ", upperBound=" + upperBound +
                ", minCardinality=" + minCardinality +
                ", maxCardinality=" + maxCardinality +
                '}';
    }
}
