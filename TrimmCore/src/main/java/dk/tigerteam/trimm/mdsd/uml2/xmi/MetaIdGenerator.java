/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi;

import dk.tigerteam.trimm.mdsd.meta.MetaType;

/**
 * UML tool specific MetaType Id generator
 */
public interface MetaIdGenerator {
  /**
   * Create a new id for the given type of MetaType
   *
   * @param metaTypeClass
   * @return The created id
   */
  String createId(Class<? extends MetaType> metaTypeClass);

  /**
   * Create an instance of the given metaType (as indicated by the metaTypeClass parameter) and give the instance an id (as
   * created by {@link #createId(Class)}
   *
   * @param metaTypeClass The type of MetaType subclass to create an instanceof (with an id)
   * @param <T>           The meta type class
   * @return The metatype class instance (with an id set)
   */
  <T extends MetaType> T createWithId(Class<T> metaTypeClass);
}
