/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

import dk.tigerteam.trimm.bean.bidirectional.ManyToManySetWrapper;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a Meta interface, which is just a specialization of MetaClazz
 * @author Jeppe Cramon
 */
public class RuntimeMetaInterface extends RuntimeMetaClazz {
	
	private static final long serialVersionUID = -7913043599524150210L;
	
	/**
	 * What RuntimeMetaClazzes are realizations this Interface.<br/>
	 * The inverse side of this is {@link RuntimeMetaClazz#getRealizations()} 
	 */
	private Set<RuntimeMetaClazz> realizedBy = new HashSet<RuntimeMetaClazz>();
	
	public RuntimeMetaInterface(String id, boolean fabricated, Class<?> _class) {
		super(id, fabricated, _class);
	}

	public RuntimeMetaInterface(String id, boolean fabricated, String name) {
		super(id, fabricated, name);
	}

	@Override
	public boolean isInterface() {
		return true;
	}
	
	/**
	 * What RuntimeMetaClazzes are realizations this Interface.<br/>
	 * The inverse side of this is {@link RuntimeMetaClazz#getRealizations()} 
	 */
	@SuppressWarnings("serial")
	public Set<RuntimeMetaClazz> getRealizedBy() {
		return new ManyToManySetWrapper<RuntimeMetaInterface, RuntimeMetaClazz>(this, realizedBy) {
			@Override
			protected Collection<RuntimeMetaInterface> getSourceCollectionInTarget(
					RuntimeMetaClazz o) {
				return o.getRealizations();
			}
		};
	}
}
