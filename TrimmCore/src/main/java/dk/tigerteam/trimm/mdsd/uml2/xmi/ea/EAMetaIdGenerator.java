/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi.ea;

import dk.tigerteam.trimm.mdsd.meta.MetaPackage;
import dk.tigerteam.trimm.mdsd.meta.MetaType;
import dk.tigerteam.trimm.mdsd.uml2.xmi.MetaIdGenerator;

import java.util.UUID;

/**
 * Enterprise Architect specific Meta Id generator
 */
public class EAMetaIdGenerator implements MetaIdGenerator {

	public String createId(Class<? extends MetaType> metaTypeClass) {
		String preFix = "EAID_";
		if (MetaPackage.class.isAssignableFrom(metaTypeClass)) {
			preFix = "EAPK_";
		}
		String uuid = UUID.randomUUID().toString();
		String[] uuidParts = uuid.split("-");
		return preFix + uuidParts[0].toUpperCase() + "_" + uuidParts[1].toUpperCase() + "_" + uuidParts[2] + "_" + uuidParts[3].toUpperCase()
				+ "_" + uuidParts[4].toUpperCase();
	}

	public <T extends MetaType> T createWithId(Class<T> metaTypeClass) {
		try {
			return metaTypeClass.getConstructor(String.class).newInstance(createId(metaTypeClass));
		} catch (Exception e) {
			throw new RuntimeException("Couldn't create a new instance of " + metaTypeClass, e);
		}
	}

	public String getEaGuid(String id) {
		return "{" + id.substring("EAID_".length()).replace('_', '-') + "}";
	}
}
