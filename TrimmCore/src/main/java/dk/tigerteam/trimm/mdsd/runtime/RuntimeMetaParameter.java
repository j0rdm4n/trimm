/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

/**
 * Represents a method parameter (or return value) in UML
 * @author Jeppe Cramon
 */
public class RuntimeMetaParameter extends RuntimeNameableMetaType {
	
	private static final long serialVersionUID = 8003511310121402398L;

	/**
	 * The parameter direction
	 * @author Jeppe Cramon
	 */
	public enum Direction { In, Out, InOut, Return };
	private Direction direction = Direction.In;
	private RuntimeMetaClazz type;
	
	public RuntimeMetaParameter(String id) {
		super(id);
	}

	public RuntimeMetaParameter(String id, boolean fabricated, String name) {
		super(id, fabricated);
		setName(name);
	}

	/**
	 * The parameter direction
	 */
	public Direction getDirection() {
		return direction;
	}

	/**
	 * The parameter direction
	 * @return this
	 */
	public RuntimeMetaParameter setDirection(Direction direction) {
		this.direction = direction;
		return this;
	}

	public RuntimeMetaClazz getType() {
		return type;
	}

	public RuntimeMetaParameter setType(RuntimeMetaClazz type) {
		this.type = type;
		return this;
	}
	
	@Override
	public String getDescription() {
		StringBuilder builder = new StringBuilder()
		.append(getDocumentation() != null ? getDocumentation() : "No documentation").append(newline)
		.append(super.getDescription()).append(newline)
		.append("Direction = '").append(direction);
		
		return builder.toString();	
	}
}
