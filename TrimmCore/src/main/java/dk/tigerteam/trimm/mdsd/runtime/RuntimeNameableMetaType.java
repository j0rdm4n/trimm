/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

import dk.tigerteam.trimm.util.ObjectUtil;

/**
 * A {@link RuntimeMetaType} with a name
 * @author Jeppe Cramon
 */
public class RuntimeNameableMetaType extends RuntimeMetaType {
	private static final long serialVersionUID = -4974772564182526490L;

	/**
	 * Create a {@link RuntimeNameableMetaType} by supplying its unique id
	 * @param id The unique id
	 */
	public RuntimeNameableMetaType(String id) {
		super(id);
	}

	/**
	 * Create a {@link RuntimeNameableMetaType} by supplying its unique id and a fabrication information
	 * @param id The unique id
	 * @param fabricated Is this MetaType a result of a fabrication or defined in the original model
	 */
	public RuntimeNameableMetaType(String id, boolean fabricated) {
		super(id, fabricated);
	}
	
	/**
	 * Create a {@link RuntimeNameableMetaType} by supplying its unique id and a fabrication information
	 * @param id The unique id
	 * @param fabricated Is this MetaType a result of a fabrication or defined in the original model
	 * @param name The name of the type (could be the package name, class name, property name)
	 */
	public RuntimeNameableMetaType(String id, boolean fabricated, String name) {
		super(id, fabricated);
		this.name = name;
	}

	private String name;

	public String getName() {
		return name;
	}

	public RuntimeNameableMetaType setName(String name) {
		checkCanModify();
		this.name = name;
		return this;
	} 
	
	/**
	 * An equals method that only checks the {@link #name} values
	 * @param other The other {@link RuntimeNameableMetaType} to compare with
	 * @return true if the two object have the same name
	 */
	public boolean hasSameName(RuntimeNameableMetaType other) {
		assert other != null;
		return ObjectUtil.equals(this.getName(), other.getName());
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ": " + name + "#" + getId();
	}
	
	/**
	 * Debug friendly description of this {@link RuntimeNameableMetaType}. 
	 */
	public String getDescription() {
		return super.getDescription() + RuntimeMetaType.newline + "Name = '" + name + "'";
	}
	
}