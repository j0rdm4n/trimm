/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.uml2.xmi;

/**
 * Represents an individual TemplateParameter in a MetaClazz' TemplateBinding
 */
public class TemplateParameterId {
    private String actualTypeId;
    private boolean primitiveType;

    public TemplateParameterId(String actualTypeId) {
        this.actualTypeId = actualTypeId;
    }

    public TemplateParameterId(String actualTypeId, boolean primitiveType) {
        this.actualTypeId = actualTypeId;
        this.primitiveType = primitiveType;
    }

    public String getActualTypeId() {
        return actualTypeId;
    }

    public boolean isPrimitiveType() {
        return primitiveType;
    }
}
