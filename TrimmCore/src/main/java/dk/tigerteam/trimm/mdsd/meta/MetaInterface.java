/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

import dk.tigerteam.trimm.bean.bidirectional.ManyToManyListWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Meta interface, which is just a specialization of MetaClazz
 * 
 * @author Jeppe Cramon
 */
public class MetaInterface extends MetaClazz {

	private static final long serialVersionUID = 9062707001250923055L;

	/**
	 * What MetaClazzes are realizations this Interface.<br/>
	 * The inverse side of this is {@link MetaClazz#getRealizations()}
	 */
	private List<MetaClazz> realizedBy = new ArrayList<MetaClazz>();

	public MetaInterface(String id) {
		super(id);
	}

	public MetaInterface(String id, boolean fabricated) {
		super(id, fabricated);
	}

	@Override
	public boolean isInterface() {
		return true;
	}

	/**
	 * What MetaClazzes are realizations this Interface.<br/>
	 * The inverse side of this is {@link MetaClazz#getRealizations()}
	 */
	public List<MetaClazz> getRealizedBy() {
		return new ManyToManyListWrapper<MetaInterface, MetaClazz>(this, realizedBy) {
			private static final long serialVersionUID = -7894096104066040749L;

			@Override
			protected List<MetaInterface> getSourceCollectionInTarget(MetaClazz o) {
				return o.getRealizations();
			}
		};
	}
}
