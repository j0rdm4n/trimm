/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package dk.tigerteam.trimm.mdsd.runtime;

/**
 * Interface which defines that this {@link Class} it has been applied to has {@link RuntimeMetaClazz} data available
 * and this Runtime meta data can be accessed through the {@link #getMetaType()} method
 * @author Jeppe Cramon
 */
public interface RuntimeMeta {
	/**
	 * Get the {@link RuntimeMetaClazz} for this instance
	 */
	RuntimeMetaClazz getMetaType();
}
