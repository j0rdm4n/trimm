/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

import java.util.List;

/**
 * Models a {@link RuntimeMetaClazz} which is in fact an enumeration of several properties (like a Java {@link Enum}):
 * @author Jeppe Cramon
 */
public class RuntimeMetaEnumeration extends RuntimeMetaClazz {
	
	private static final long serialVersionUID = 5856839037753320912L;

	public RuntimeMetaEnumeration(String id, boolean fabricated, Class<?> _class) {
		super(id, fabricated, _class);
	}

	public RuntimeMetaEnumeration(String id, boolean fabricated, String name) {
		super(id, fabricated, name);
	}
	
	@Override
	public boolean isEnumerationClazz() {
		return true;
	}
	
	public List<RuntimeMetaProperty> getEnumValues() {
		return this.findInImmediateChildren(new RuntimeMetaCriteria<RuntimeMetaProperty>(RuntimeMetaProperty.class) {
			@Override
			public boolean isOk(RuntimeMetaProperty element) {
				return !element.isStatic() && !element.isPartInAnAssociation();
			}
		});
	}
}
