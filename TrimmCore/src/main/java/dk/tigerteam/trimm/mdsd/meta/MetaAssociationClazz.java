/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

/**
 * Models a {@link MetaAssociation} which is defined by its two {@link MetaProperty}'s and an association {@link MetaClazz}.
 * 
 * @author Jeppe Cramon
 */
public class MetaAssociationClazz extends MetaClazz {
	private static final long serialVersionUID = 7036217436216384018L;

	public MetaAssociationClazz(String id) {
		super(id);
	}

	@Override
	public boolean isAssociationClazz() {
		return true;
	}
}
