/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

import dk.tigerteam.trimm.bean.bidirectional.OneToOneWrapper;

/**
 * Models meta data associated with an association between two {@link RuntimeMetaProperty}'.
 * @author Jeppe Cramon
 */
public class RuntimeMetaAssociation extends RuntimeMetaType {
	
	private static final long serialVersionUID = 8122094220857269806L;
	
	private RuntimeMetaProperty ownerProperty;
	private RuntimeMetaProperty ownedProperty;
	private boolean bidirectional;
	
	/**
	 * Create a {@link RuntimeMetaAssociation} by supplying its unique id and a fabrication information
	 * @param id The unique id
	 * @param fabricated Is this MetaType a result of a fabrication or defined in the original model
	 */
	public RuntimeMetaAssociation(String id, boolean fabricated) {
		super(id, fabricated);
	}

	/**
	 * Check if this association is between a MetaClazz and itself
	 */
	public boolean isSelfReferencing() {
		return ownerProperty.getOwner() == ownedProperty.getOwner();
	}
	
	public boolean isBidirectional() {
		return bidirectional;
	}

	public RuntimeMetaAssociation setBidirectional(boolean bidirectional) {
		checkCanModify();
		this.bidirectional = bidirectional;
		return this;
	}

	public RuntimeMetaProperty getOwnerProperty() {
		return ownerProperty;
	}
	

	public RuntimeMetaAssociation setOwnerProperty(RuntimeMetaProperty ownerProperty) {
		new OneToOneWrapper<RuntimeMetaProperty>() {
			private static final long serialVersionUID = -8353383097975834348L;

			@Override
			protected void createRelationToTargetObject(RuntimeMetaProperty newTarget) {
				newTarget.checkCanModify();
				newTarget.setAsOwnerOfAssociation(RuntimeMetaAssociation.this);
			}

			@Override
			protected RuntimeMetaProperty getTargetObjectInSourceObject() {
				return RuntimeMetaAssociation.this.ownerProperty;
			}

			@Override
			protected void removeRelationToTargetObject(RuntimeMetaProperty currentTarget) {
				currentTarget.checkCanModify();
				currentTarget.setAsOwnerOfAssociation(null);
			}

			@Override
			protected void setTargetObjectInSourceObject(RuntimeMetaProperty newTarget) {
				RuntimeMetaAssociation.this.checkCanModify();
				RuntimeMetaAssociation.this.ownerProperty = newTarget;
			}
		}.updateTargetProperty(ownerProperty);
		return this;
	}

	public RuntimeMetaProperty getOwnedProperty() {
		return ownedProperty;
	}

	public RuntimeMetaAssociation setOwnedProperty(RuntimeMetaProperty ownedProperty) {
		new OneToOneWrapper<RuntimeMetaProperty>() {
			private static final long serialVersionUID = 2849109383315489452L;

			@Override
			protected void createRelationToTargetObject(RuntimeMetaProperty newTarget) {
				newTarget.checkCanModify();
				newTarget.setAsOwnedByAssociation(RuntimeMetaAssociation.this);
			}

			@Override
			protected RuntimeMetaProperty getTargetObjectInSourceObject() {
				return RuntimeMetaAssociation.this.ownedProperty;
			}

			@Override
			protected void removeRelationToTargetObject(RuntimeMetaProperty currentTarget) {
				currentTarget.checkCanModify();
				currentTarget.setAsOwnedByAssociation(null);
			}

			@Override
			protected void setTargetObjectInSourceObject(RuntimeMetaProperty newTarget) {
				RuntimeMetaAssociation.this.checkCanModify();
				RuntimeMetaAssociation.this.ownedProperty = newTarget;
			}
		}.updateTargetProperty(ownedProperty);
		return this;
	}

	/**
	 * Get the property (owner or owned) that is opposite the metaProperty parameter
	 * @param metaProperty The property
	 * @return The property that is opposite the metaProperty parameter
	 */
	public RuntimeMetaProperty getOppositeProperty(RuntimeMetaProperty metaProperty) {
		if (ownedProperty == metaProperty) {
			return ownerProperty;
		} else if (ownerProperty == metaProperty) {
			return ownedProperty;
		} else {
			throw new IllegalArgumentException("MetaProperty '"  + metaProperty + "' isn't a part of this Association '" + this + "'");
		}
	}

	
}
