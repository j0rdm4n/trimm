/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

import dk.tigerteam.trimm.bean.bidirectional.OneToOneWrapper;

/**
 * Models meta data associated with an association between two {@link MetaProperty}'.
 * 
 * @author Jeppe Cramon
 */
public class MetaAssociation extends NameableMetaType {
	private static final long serialVersionUID = 8122094220857269806L;
	/**
	 * The Attribute map key for storing information about association member destination
	 */
	public static final String MEMBEREND_DESTINATION_ATTR_KEY = "memberEndDestination";
	/**
	 * The Attribute map key for storing information about association member source
	 */
	public static final String MEMBEREND_SOURCE_ATTR_KEY = "memberEndSource";
	/**
	 * Attribute map key which stores information about unspecified owned ends (aka unspecified directionality)
	 */
	public static final String NO_OWNED_ENDS = "noOwnedEnds";

	private MetaProperty ownerProperty;
	private MetaProperty ownedProperty;
	private boolean bidirectional;

	/**
	 * Create a {@link MetaAssociation} by supplying its unique id
	 * 
	 * @param id
	 *          The unique id
	 */
	public MetaAssociation(String id) {
		super(id);
	}

	/**
	 * Create a {@link MetaAssociation} by supplying its unique id and a fabrication information
	 * 
	 * @param id
	 *          The unique id
	 * @param fabricated
	 *          Is this MetaType a result of a fabrication or defined in the original model
	 */
	public MetaAssociation(String id, boolean fabricated) {
		super(id, fabricated);
	}

	/**
	 * Check if this association is between a MetaClazz and itself
	 */
	public boolean isSelfReferencing() {
		return ownerProperty.getOwner() == ownedProperty.getOwner();
	}

	public boolean isBidirectional() {
		return bidirectional;
	}

	public MetaAssociation setBidirectional(boolean bidirectional) {
		this.bidirectional = bidirectional;
		return this;
	}

	/**
	 * Get the raw UML representation of which property is assigned as owner of theac association. To allow for specialization and overriding,
	 * you should always use {@link MetaProperty#isOwnerOfAssociation()}
	 */
	public MetaProperty getOwnerProperty() {
		return ownerProperty;
	}

	public MetaAssociation setOwnerProperty(MetaProperty ownerProperty) {
		new OneToOneWrapper<MetaProperty>() {
			private static final long serialVersionUID = -8353383097975834348L;

			@Override
			protected void createRelationToTargetObject(MetaProperty newTarget) {
				newTarget.setAsOwnerOfAssociation(MetaAssociation.this);
			}

			@Override
			protected MetaProperty getTargetObjectInSourceObject() {
				return MetaAssociation.this.ownerProperty;
			}

			@Override
			protected void removeRelationToTargetObject(MetaProperty currentTarget) {
				currentTarget.setAsOwnerOfAssociation(null);
			}

			@Override
			protected void setTargetObjectInSourceObject(MetaProperty newTarget) {
				MetaAssociation.this.ownerProperty = newTarget;
			}
		}.updateTargetProperty(ownerProperty);
		return this;
	}

	public MetaProperty getOwnedProperty() {
		return ownedProperty;
	}

	public MetaAssociation setOwnedProperty(MetaProperty ownedProperty) {
		new OneToOneWrapper<MetaProperty>() {
			private static final long serialVersionUID = 2849109383315489452L;

			@Override
			protected void createRelationToTargetObject(MetaProperty newTarget) {
				newTarget.setAsOwnedByAssociation(MetaAssociation.this);
			}

			@Override
			protected MetaProperty getTargetObjectInSourceObject() {
				return MetaAssociation.this.ownedProperty;
			}

			@Override
			protected void removeRelationToTargetObject(MetaProperty currentTarget) {
				currentTarget.setAsOwnedByAssociation(null);
			}

			@Override
			protected void setTargetObjectInSourceObject(MetaProperty newTarget) {
				MetaAssociation.this.ownedProperty = newTarget;
			}
		}.updateTargetProperty(ownedProperty);
		return this;
	}

	/**
	 * Get the property (owner or owned) that is opposite the metaProperty parameter
	 * 
	 * @param metaProperty
	 *          The property
	 * @return The property that is opposite the metaProperty parameter
	 */
	public MetaProperty getOppositeProperty(MetaProperty metaProperty) {
		if (ownedProperty == metaProperty) {
			return ownerProperty;
		} else if (ownerProperty == metaProperty) {
			return ownedProperty;
		} else {
			throw new IllegalArgumentException("MetaProperty '" + metaProperty + "' isn't a part of this Association '" + this + "'");
		}
	}

	public void swapOwnerAndOwnedProperties() {
		MetaProperty previousOwner = ownerProperty;
		ownerProperty = ownedProperty;
		ownedProperty = previousOwner;
	}

}
