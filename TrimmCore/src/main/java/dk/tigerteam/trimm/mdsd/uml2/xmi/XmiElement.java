/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi;

import org.jdom2.*;
import org.jdom2.filter.Filter;
import org.jdom2.util.IteratorIterable;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Xmi specialized extension of the standard Jdom {@link Element}
 *
 * @author Jeppe Cramon
 */
@SuppressWarnings({"unchecked"})
public class XmiElement extends Element {
    private static final long serialVersionUID = 512665689467741100L;

    private Element element;

    public XmiElement(Element element) {
        super();
        this.element = element;
    }

    public <T> T getAttributeValue(String attributeName, Class<T> returnType) {
        String value = getAttributeValue(attributeName);
        if (value == null) return null;
        if (Integer.class.isAssignableFrom(returnType)) {
            return (T) new Integer(value);
        } else {
            throw new IllegalArgumentException("Can't convert value '" + value + "' to type " + returnType);
        }
    }

    public Element getWrappedElement() {
        return element;
    }

    public String getXmiIdRef(Namespace xmiNs) {
        return getAttributeValue("idref", xmiNs);
    }

    public String getXmiId(Namespace xmiNs) {
        String xmiId = getAttributeValue("id", xmiNs);
        return xmiId;
    }

    public String getXmiType(Namespace xmiNs) {
        return getAttributeValue("type", xmiNs);
    }

    public boolean hasXmiType(String xmiType, Namespace xmiNs) {
        String thisElementsXmiType = getXmiType(xmiNs);
        if (thisElementsXmiType != null && thisElementsXmiType.equals(xmiType)) return true;
        return false;
    }

    public String getUmlName() {
        String name = getAttributeValue("name");
        if (name != null) {
            return name.trim();
        } else {
            return null;
        }
    }

    public String getUmlVisibility() {
        return getAttributeValue("visibility");
    }

    public boolean isUmlReadOnly() {
        return "true".equals(getAttributeValue("isAbstract")) || "1".equals(getAttributeValue("isAbstract")) || "true".equals(getAttributeValue("isReadOnly")) || "1".equals(getAttributeValue("isReadOnly"));
    }

    public String getUmlIsOrdered() {
        return getAttributeValue("isOrdered");
    }

    public String getUmlIsDerived() {
        return getAttributeValue("isDerived");
    }

    public String getUmlIsDerivedUnion() {
        return getAttributeValue("isDerivedUnion");
    }

    public String getUmlAssociation() {
        return getAttributeValue("association");
    }

    public String getUmlAggregation() {
        return getAttributeValue("aggregation");
    }

    public boolean isUmlStatic() {
        return "true".equals(getAttributeValue("isStatic")) || "1".equals(getAttributeValue("isStatic"));
    }

    public boolean isUmlOrdered() {
        return "true".equals(getAttributeValue("isOrdered")) || "1".equals(getAttributeValue("isOrdered"));
    }

    public boolean isUmlAbstract() {
        return "true".equals(getAttributeValue("isAbstract")) || "1".equals(getAttributeValue("isAbstract"));
    }


    public boolean addNamespaceDeclaration(Namespace additionalNamespace) {
        return element.addNamespaceDeclaration(additionalNamespace);
    }

    /**
     * Removes an additional namespace declarations from this element. This
     * should <i>not</i> be used to remove the declaration for this element
     * itself; that should be handled in the construction of the element.
     * Instead, this is for removing namespace declarations on the element not
     * relating directly to itself. If the declaration is not present, this
     * method does nothing.
     *
     * @param additionalNamespace namespace to remove. A null Namespace does nothing.
     */
    @Override
    public void removeNamespaceDeclaration(Namespace additionalNamespace) {
        element.removeNamespaceDeclaration(additionalNamespace);
    }

    /**
     * Returns a list of the additional namespace declarations on this element.
     * This includes only additional namespace, not the namespace of the element
     * itself, which can be obtained through {@link #getNamespace()}. If there
     * are no additional declarations, this returns an empty list. Note, the
     * returned list is unmodifiable.
     *
     * @return                     a List of the additional namespace
     *                             declarations
     */
    @Override
    public List<Namespace> getAdditionalNamespaces() {
        return element.getAdditionalNamespaces();
    }

    /**
     * Returns the XPath 1.0 string value of this element, which is the
     * complete, ordered content of all text node descendants of this element
     * (i&#46;e&#46; the text that's left after all references are resolved
     * and all other markup is stripped out.)
     *
     * @return a concatentation of all text node descendants
     */
    @Override
    public String getValue() {
        return element.getValue();
    }

    /**
     * Returns whether this element is a root element. This can be used in
     * tandem with {@link #getParent} to determine if an element has any
     * "attachments" to a parent element or document.
     * <p>
     * An element is a root element when it has a parent and that parent is a
     * Document. In particular, this means that detatched Elements are <b>not</b>
     * root elements.
     *
     * @return                     whether this is a root element
     */
    @Override
    public boolean isRootElement() {
        return element.isRootElement();
    }

    @Override
    public int getContentSize() {
        return element.getContentSize();
    }

    @Override
    public int indexOf(Content child) {
        return element.indexOf(child);
    }

    /**
     * Returns the textual content directly held under this element as a string.
     * This includes all text within this single element, including whitespace
     * and CDATA sections if they exist. It's essentially the concatenation of
     * all {@link org.jdom2.Text} and {@link org.jdom2.CDATA} nodes returned by {@link #getContent}.
     * The call does not recurse into child elements. If no textual value exists
     * for the element, an empty string is returned.
     *
     * @return                     text content for this element, or empty
     *                             string if none
     */
    @Override
    public String getText() {
        return element.getText();
    }

    /**
     * Returns the textual content of this element with all surrounding
     * whitespace removed. If no textual value exists for the element, or if
     * only whitespace exists, the empty string is returned.
     *
     * @return                     trimmed text content for this element, or
     *                             empty string if none
     */
    @Override
    public String getTextTrim() {
        return element.getTextTrim();
    }

    /**
     * Returns the textual content of this element with all surrounding
     * whitespace removed and internal whitespace normalized to a single space.
     * If no textual value exists for the element, or if only whitespace exists,
     * the empty string is returned.
     *
     * @return                     normalized text content for this element, or
     *                             empty string if none
     */
    @Override
    public String getTextNormalize() {
        return element.getTextNormalize();
    }

    /**
     * Returns the textual content of the named child element, or null if
     * there's no such child. This method is a convenience because calling
     * <code>getChild().getText()</code> can throw a NullPointerException.
     *
     * @param  cname                the name of the child
     * @return                     text content for the named child, or null if
     *                             no such child
     */
    @Override
    public String getChildText(String cname) {
        return element.getChildText(cname);
    }

    /**
     * Returns the trimmed textual content of the named child element, or null
     * if there's no such child. See <code>{@link #getTextTrim()}</code> for
     * details of text trimming.
     *
     * @param  cname                the name of the child
     * @return                     trimmed text content for the named child, or
     *                             null if no such child
     */
    @Override
    public String getChildTextTrim(String cname) {
        return element.getChildTextTrim(cname);
    }

    /**
     * Returns the normalized textual content of the named child element, or
     * null if there's no such child. See <code>{@link
     * #getTextNormalize()}</code> for details of text normalizing.
     *
     * @param  cname                the name of the child
     * @return                     normalized text content for the named child,
     *                             or null if no such child
     */
    @Override
    public String getChildTextNormalize(String cname) {
        return element.getChildTextNormalize(cname);
    }

    /**
     * Returns the textual content of the named child element, or null if
     * there's no such child.
     *
     * @param cname
     *        the name of the child
     * @param ns
     *        the namespace of the child. A null implies Namespace.NO_NAMESPACE.
     * @return text content for the named child, or null if no such child
     */
    @Override
    public String getChildText(String cname, Namespace ns) {
        return element.getChildText(cname, ns);
    }

    /**
     * Returns the trimmed textual content of the named child element, or null
     * if there's no such child.
     *
     * @param cname
     *        the name of the child
     * @param ns
     *        the namespace of the child. A null implies Namespace.NO_NAMESPACE.
     * @return trimmed text content for the named child, or null if no such
     *         child
     */
    @Override
    public String getChildTextTrim(String cname, Namespace ns) {
        return element.getChildTextTrim(cname, ns);
    }

    /**
     * Returns the normalized textual content of the named child element, or
     * null if there's no such child.
     *
     * @param cname
     *        the name of the child
     * @param ns
     *        the namespace of the child. A null implies Namespace.NO_NAMESPACE.
     * @return normalized text content for the named child, or null if no such
     *         child
     */
    @Override
    public String getChildTextNormalize(String cname, Namespace ns) {
        return element.getChildTextNormalize(cname, ns);
    }

    /**
     * Sets the content of the element to be the text given. All existing text
     * content and non-text context is removed. If this element should have both
     * textual content and nested elements, use <code>{@link #setContent}</code>
     * instead. Setting a null text value is equivalent to setting an empty
     * string value.
     *
     * @param  text                 new text content for the element
     * @return                      the target element
     * @throws org.jdom2.IllegalDataException if the assigned text contains an illegal
     *                              character such as a vertical tab (as
     *                              determined by {@link
     *                              org.jdom2.Verifier#checkCharacterData})
     */
    @Override
    public Element setText(String text) {
        return element.setText(text);
    }

    /**
     * Adjacent Text content is merged into the first Text in document order,
     * and the redundant Text items are removed (including any empty Text).
     *
     * @param recursively
     *        true if you want the text of child elements coalesced too. False
     *        if you only want to coalesce this Element's Text.
     * @return true if any content was changed by this operation.
     */
    @Override
    public boolean coalesceText(boolean recursively) {
        return element.coalesceText(recursively);
    }

    /**
     * This returns the full content of the element as a List which
     * may contain objects of type <code>Text</code>, <code>Element</code>,
     * <code>Comment</code>, <code>ProcessingInstruction</code>,
     * <code>CDATA</code>, and <code>EntityRef</code>.
     * The List returned is "live" in document order and modifications
     * to it affect the element's actual contents.  Whitespace content is
     * returned in its entirety.
     *
     * <p>
     * Sequential traversal through the List is best done with an Iterator
     * since the underlying implement of List.size() may require walking the
     * entire list.
     * </p>
     *
     * @return a <code>List</code> containing the mixed content of the
     *         element: may contain <code>Text</code>,
     *         <code>{@link org.jdom2.Element}</code>, <code>{@link org.jdom2.Comment}</code>,
     *         <code>{@link org.jdom2.ProcessingInstruction}</code>,
     *         <code>{@link org.jdom2.CDATA}</code>, and
     *         <code>{@link org.jdom2.EntityRef}</code> objects.
     */
    @Override
    public List<Content> getContent() {
        return element.getContent();
    }

    /**
     * Return a filter view of this <code>Element</code>'s content.
     *
     * <p>
     * Sequential traversal through the List is best done with a Iterator
     * since the underlying implement of List.size() may require walking the
     * entire list.
     * </p>
     *
     * @param filter <code>Filter</code> to apply
     * @return <code>List</code> - filtered Element content
     */
    @Override
    public <E extends Content> List<E> getContent(Filter<E> filter) {
        return element.getContent(filter);
    }

    /**
     * Removes all child content from this parent.
     *
     * @return list of the old children detached from this parent
     */
    @Override
    public List<Content> removeContent() {
        return element.removeContent();
    }

    /**
     * Remove all child content from this parent matching the supplied filter.
     *
     * @param filter filter to select which content to remove
     * @return list of the old children detached from this parent
     */
    @Override
    public <F extends Content> List<F> removeContent(Filter<F> filter) {
        return element.removeContent(filter);
    }

    /**
     * This sets the content of the element.  The supplied List should
     * contain only objects of type <code>Element</code>, <code>Text</code>,
     * <code>CDATA</code>, <code>Comment</code>,
     * <code>ProcessingInstruction</code>, and <code>EntityRef</code>.
     *
     * <p>
     * When all objects in the supplied List are legal and before the new
     * content is added, all objects in the old content will have their
     * parentage set to null (no parent) and the old content list will be
     * cleared. This has the effect that any active list (previously obtained
     * with a call to {@link #getContent} or {@link #getChildren}) will also
     * change to reflect the new content.  In addition, all objects in the
     * supplied List will have their parentage set to this element, but the
     * List itself will not be "live" and further removals and additions will
     * have no effect on this elements content. If the user wants to continue
     * working with a "live" list, then a call to setContent should be
     * followed by a call to {@link #getContent} or {@link #getChildren} to
     * obtain a "live" version of the content.
     * </p>
     *
     * <p>
     * Passing a null or empty List clears the existing content.
     * </p>
     *
     * <p>
     * In event of an exception the original content will be unchanged and
     * the objects in the supplied content will be unaltered.
     * </p>
     *
     * @param newContent <code>Collection</code> of content to set
     * @return this element modified
     * @throws org.jdom2.IllegalAddException if the List contains objects of
     *         illegal types or with existing parentage.
     */
    @Override
    public Element setContent(Collection<? extends Content> newContent) {
        return element.setContent(newContent);
    }

    /**
     * Replace the current child the given index with the supplied child.
     * <p>
     * In event of an exception the original content will be unchanged and
     * the supplied child will be unaltered.
     * </p>
     *
     * @param index - index of child to replace.
     * @param child - child to add.
     * @return element on which this method was invoked
     * @throws org.jdom2.IllegalAddException if the supplied child is already attached
     *                             or not legal content for this parent.
     * @throws IndexOutOfBoundsException if index is negative or greater
     *         than the current number of children.
     */
    @Override
    public Element setContent(int index, Content child) {
        return element.setContent(index, child);
    }

    /**
     * Replace the child at the given index whith the supplied
     * collection.
     * <p>
     * In event of an exception the original content will be unchanged and
     * the content in the supplied collection will be unaltered.
     * </p>
     *
     * @param index - index of child to replace.
     * @param newContent - <code>Collection</code> of content to replace child.
     * @return object on which this method was invoked
     * @throws org.jdom2.IllegalAddException if the collection contains objects of
     *         illegal types.
     * @throws IndexOutOfBoundsException if index is negative or greater
     *         than the current number of children.
     */
    @Override
    public Parent setContent(int index, Collection<? extends Content> newContent) {
        return element.setContent(index, newContent);
    }

    /**
     * This adds text content to this element.  It does not replace the
     * existing content as does <code>setText()</code>.
     *
     * @param str <code>String</code> to add
     * @return this element modified
     * @throws org.jdom2.IllegalDataException if <code>str</code> contains an
     *         illegal character such as a vertical tab (as determined
     *         by {@link org.jdom2.Verifier#checkCharacterData})
     */
    @Override
    public Element addContent(String str) {
        return element.addContent(str);
    }

    /**
     * Appends the child to the end of the element's content list.
     *
     * @param child   child to append to end of content list
     * @return        the element on which the method was called
     * @throws org.jdom2.IllegalAddException if the given child already has a parent.     */
    @Override
    public Element addContent(Content child) {
        return element.addContent(child);
    }

    /**
     * Appends all children in the given collection to the end of
     * the content list.  In event of an exception during add the
     * original content will be unchanged and the objects in the supplied
     * collection will be unaltered.
     *
     * @param newContent <code>Collection</code> of content to append
     * @return           the element on which the method was called
     * @throws org.jdom2.IllegalAddException if any item in the collection
     *         already has a parent or is of an inappropriate type.
     */
    @Override
    public Element addContent(Collection<? extends Content> newContent) {
        return element.addContent(newContent);
    }

    /**
     * Inserts the child into the content list at the given index.
     *
     * @param index location for adding the collection
     * @param child      child to insert
     * @return           the parent on which the method was called
     * @throws IndexOutOfBoundsException if index is negative or beyond
     *         the current number of children
     * @throws org.jdom2.IllegalAddException if the given child already has a parent.
     */
    @Override
    public Element addContent(int index, Content child) {
        return element.addContent(index, child);
    }

    /**
     * Inserts the content in a collection into the content list
     * at the given index.  In event of an exception the original content
     * will be unchanged and the objects in the supplied collection will be
     * unaltered.
     *
     * @param index location for adding the collection
     * @param newContent  <code>Collection</code> of content to insert
     * @return            the parent on which the method was called
     * @throws IndexOutOfBoundsException if index is negative or beyond
     *         the current number of children
     * @throws org.jdom2.IllegalAddException if any item in the collection
     *         already has a parent or is of an inappropriate type.
     */
    @Override
    public Element addContent(int index, Collection<? extends Content> newContent) {
        return element.addContent(index, newContent);
    }

    @Override
    public List<Content> cloneContent() {
        return element.cloneContent();
    }

    @Override
    public Content getContent(int index) {
        return element.getContent(index);
    }

    @Override
    public boolean removeContent(Content child) {
        return element.removeContent(child);
    }

    @Override
    public Content removeContent(int index) {
        return element.removeContent(index);
    }

    /**
     * Set this element's content to be the supplied child.
     * <p>
     * If the supplied child is legal content for this parent and before
     * it is added, all content in the current content list will
     * be cleared and all current children will have their parentage set to
     * null.
     * <p>
     * This has the effect that any active list (previously obtained with
     * a call to one of the {@link #getContent} methods will also change
     * to reflect the new content.  In addition, all content in the supplied
     * collection will have their parentage set to this parent.  If the user
     * wants to continue working with a <b>"live"</b> list of this parent's
     * child, then a call to setContent should be followed by a call to one
     * of the {@link #getContent} methods to obtain a <b>"live"</b>
     * version of the children.
     * <p>
     * Passing a null child clears the existing content.
     * <p>
     * In event of an exception the original content will be unchanged and
     * the supplied child will be unaltered.
     *
     * @param child new content to replace existing content
     * @return           the parent on which the method was called
     * @throws org.jdom2.IllegalAddException if the supplied child is already attached
     *                             or not legal content for an Element
     */
    @Override
    public Element setContent(Content child) {
        return element.setContent(child);
    }

    /**
     * Determines if this element is the ancestor of another element.
     *
     * @param element <code>Element</code> to check against
     * @return <code>true</code> if this element is the ancestor of the
     *         supplied element
     */
    @Override
    public boolean isAncestor(Element element) {
        return this.element.isAncestor(element);
    }

    /**
     * Indicate whether this Element has any attributes.
     * Where possible you should call this method before calling getAttributes()
     * because calling getAttributes() will create the necessary Attribute List
     * memory structures, even if there are no Attributes attached to the
     * Element. Calling hasAttributes() first can save memory.
     * @return true if this Element has attributes.
     */
    @Override
    public boolean hasAttributes() {
        return element.hasAttributes();
    }

    /**
     * Indicate whether this Element has any additional Namespace declarations.
     * Where possible you should call this method before calling
     * {@link #getAdditionalNamespaces()} because calling getAttributes() will
     * create an unnecessary List even if there are no Additional Namespaces
     * attached to the Element. Calling this method first can save memory and
     * time.
     * @return true if this Element has additional Namespaces.
     */
    @Override
    public boolean hasAdditionalNamespaces() {
        return element.hasAdditionalNamespaces();
    }

    /**
     * <p>
     * This returns the complete set of attributes for this element, as a
     * <code>List</code> of <code>Attribute</code> objects in no particular
     * order, or an empty list if there are none.
     * The returned list is "live" and changes to it affect the
     * element's actual attributes.
     * </p>
     *
     * @return attributes for the element
     */
    @Override
    public List<Attribute> getAttributes() {
        return element.getAttributes();
    }

    /**
     * <p>
     * This returns the attribute for this element with the given name
     * and within no namespace, or null if no such attribute exists.
     * </p>
     *
     * @param attname name of the attribute to return
     * @return attribute for the element
     */
    @Override
    public Attribute getAttribute(String attname) {
        return element.getAttribute(attname);
    }

    /**
     * <p>
     * This returns the attribute for this element with the given name
     * and within the given Namespace, or null if no such attribute exists.
     * </p>
     *
     * @param attname name of the attribute to return
     * @param ns <code>Namespace</code> to search within. A null implies Namespace.NO_NAMESPACE.
     * @return attribute for the element
     */
    @Override
    public Attribute getAttribute(String attname, Namespace ns) {
        return element.getAttribute(attname, ns);
    }

    /**
     * <p>
     * This returns the attribute value for the attribute with the given name
     * and within no namespace, null if there is no such attribute, and the
     * empty string if the attribute value is empty.
     * </p>
     *
     * @param attname name of the attribute whose value to be returned
     * @return the named attribute's value, or null if no such attribute
     */
    @Override
    public String getAttributeValue(String attname) {
        return element.getAttributeValue(attname);
    }

    /**
     * <p>
     * This returns the attribute value for the attribute with the given name
     * and within no namespace, or the passed-in default if there is no
     * such attribute.
     * </p>
     *
     * @param attname name of the attribute whose value to be returned
     * @param def a default value to return if the attribute does not exist
     * @return the named attribute's value, or the default if no such attribute
     */
    @Override
    public String getAttributeValue(String attname, String def) {
        return element.getAttributeValue(attname, def);
    }

    /**
     * <p>
     * This returns the attribute value for the attribute with the given name
     * and within the given Namespace, null if there is no such attribute, and
     * the empty string if the attribute value is empty.
     * </p>
     *
     * @param attname name of the attribute whose valud is to be returned
     * @param ns <code>Namespace</code> to search within. A null implies Namespace.NO_NAMESPACE.
     * @return the named attribute's value, or null if no such attribute
     */
    @Override
    public String getAttributeValue(String attname, Namespace ns) {
        return element.getAttributeValue(attname, ns);
    }

    /**
     * <p>
     * This returns the attribute value for the attribute with the given name
     * and within the given Namespace, or the passed-in default if there is no
     * such attribute.
     * </p>
     *
     * @param attname name of the attribute whose valud is to be returned
     * @param ns <code>Namespace</code> to search within. A null implies Namespace.NO_NAMESPACE.
     * @param def a default value to return if the attribute does not exist
     * @return the named attribute's value, or the default if no such attribute
     */
    @Override
    public String getAttributeValue(String attname, Namespace ns, String def) {
        return element.getAttributeValue(attname, ns, def);
    }

    /**
     * <p>
     * This sets the attributes of the element.  The supplied Collection should
     * contain only objects of type <code>Attribute</code>.
     * </p>
     *
     * <p>
     * When all objects in the supplied List are legal and before the new
     * attributes are added, all old attributes will have their
     * parentage set to null (no parent) and the old attribute list will be
     * cleared. This has the effect that any active attribute list (previously
     * obtained with a call to {@link #getAttributes}) will also change to
     * reflect the new attributes.  In addition, all attributes in the supplied
     * List will have their parentage set to this element, but the List itself
     * will not be "live" and further removals and additions will have no
     * effect on this elements attributes. If the user wants to continue
     * working with a "live" attribute list, then a call to setAttributes
     * should be followed by a call to {@link #getAttributes} to obtain a
     * "live" version of the attributes.
     * </p>
     *
     * <p>
     * Passing a null or empty List clears the existing attributes.
     * </p>
     *
     * <p>
     * In cases where the List contains duplicate attributes, only the last
     * one will be retained.  This has the same effect as calling
     * {@link #setAttribute(org.jdom2.Attribute)} sequentially.
     * </p>
     *
     * <p>
     * In event of an exception the original attributes will be unchanged and
     * the attributes in the supplied attributes will be unaltered.
     * </p>
     *
     * @param newAttributes <code>Collection</code> of attributes to set
     * @return this element modified
     * @throws org.jdom2.IllegalAddException if the List contains objects
     *         that are not instances of <code>Attribute</code>,
     *         or if any of the <code>Attribute</code> objects have
     *         conflicting namespace prefixes.
     */
    @Override
    public Element setAttributes(Collection<? extends Attribute> newAttributes) {
        return element.setAttributes(newAttributes);
    }

    /**
     * <p>
     * This sets an attribute value for this element.  Any existing attribute
     * with the same name and namespace URI is removed.
     * </p>
     *
     * @param name name of the attribute to set
     * @param value value of the attribute to set
     * @return this element modified
     * @throws org.jdom2.IllegalNameException if the given name is illegal as an
     *         attribute name.
     * @throws org.jdom2.IllegalDataException if the given attribute value is
     *         illegal character data (as determined by
     *         {@link org.jdom2.Verifier#checkCharacterData}).
     */
    @Override
    public Element setAttribute(String name, String value) {
        return element.setAttribute(name, value);
    }

    /**
     * <p>
     * This sets an attribute value for this element.  Any existing attribute
     * with the same name and namespace URI is removed.
     * </p>
     *
     * @param name name of the attribute to set
     * @param value value of the attribute to set
     * @param ns namespace of the attribute to set. A null implies Namespace.NO_NAMESPACE.
     * @return this element modified
     * @throws org.jdom2.IllegalNameException if the given name is illegal as an
     *         attribute name, or if the namespace is an unprefixed default
     *         namespace
     * @throws org.jdom2.IllegalDataException if the given attribute value is
     *         illegal character data (as determined by
     *         {@link org.jdom2.Verifier#checkCharacterData}).
     * @throws org.jdom2.IllegalAddException if the attribute namespace prefix
     *         collides with another namespace prefix on the element.
     */
    @Override
    public Element setAttribute(String name, String value, Namespace ns) {
        return element.setAttribute(name, value, ns);
    }

    /**
     * <p>
     * This sets an attribute value for this element.  Any existing attribute
     * with the same name and namespace URI is removed.
     * </p>
     *
     * @param attribute <code>Attribute</code> to set
     * @return this element modified
     * @throws org.jdom2.IllegalAddException if the attribute being added already has a
     *   parent or if the attribute namespace prefix collides with another
     *   namespace prefix on the element.
     */
    @Override
    public Element setAttribute(Attribute attribute) {
        return element.setAttribute(attribute);
    }

    /**
     * <p>
     * This removes the attribute with the given name and within no
     * namespace. If no such attribute exists, this method does nothing.
     * </p>
     *
     * @param attname name of attribute to remove
     * @return whether the attribute was removed
     */
    @Override
    public boolean removeAttribute(String attname) {
        return element.removeAttribute(attname);
    }

    /**
     * <p>
     * This removes the attribute with the given name and within the
     * given Namespace.  If no such attribute exists, this method does
     * nothing.
     * </p>
     *
     * @param attname name of attribute to remove
     * @param ns namespace URI of attribute to remove. A null implies Namespace.NO_NAMESPACE.
     * @return whether the attribute was removed
     */
    @Override
    public boolean removeAttribute(String attname, Namespace ns) {
        return element.removeAttribute(attname, ns);
    }

    /**
     * <p>
     * This removes the supplied Attribute should it exist.
     * </p>
     *
     * @param attribute Reference to the attribute to be removed.
     * @return whether the attribute was removed
     */
    @Override
    public boolean removeAttribute(Attribute attribute) {
        return element.removeAttribute(attribute);
    }

    /**
     * <p>
     *  This returns a <code>String</code> representation of the
     *    <code>Element</code>, suitable for debugging. If the XML
     *    representation of the <code>Element</code> is desired,
     *    {@link org.jdom2.output.XMLOutputter#outputString(org.jdom2.Element)}
     *    should be used.
     * </p>
     *
     * @return <code>String</code> - information about the
     *         <code>Element</code>
     */
    @Override
    public String toString() {
        return element.toString();
    }

    public XmiElement clone() {
        return new XmiElement(element.clone());
    }

    /**
     * Returns an iterator that walks over all descendants in document order.
     *
     * @return an iterator to walk descendants
     */
    @Override
    public IteratorIterable<Content> getDescendants() {
        return element.getDescendants();
    }

    /**
     * Returns an iterator that walks over all descendants in document order
     * applying the Filter to return only content that match the filter rule.
     * With filters you can match only Elements, only Comments, Elements or
     * Comments, only Elements with a given name and/or prefix, and so on.
     *
     * @param filter filter to select which descendants to see
     * @return an iterator to walk descendants within a filter
     */
    @Override
    public <F extends Content> IteratorIterable<F> getDescendants(Filter<F> filter) {
        return element.getDescendants(filter);
    }

    /**
     * This returns a <code>List</code> of all the child elements
     * nested directly (one level deep) within this element, as
     * <code>Element</code> objects.  If this target element has no nested
     * elements, an empty List is returned.  The returned list is "live"
     * in document order and changes to it affect the element's actual
     * contents.
     *
     * <p>
     * Sequential traversal through the List is best done with a Iterator
     * since the underlying implement of List.size() may not be the most
     * efficient.
     * </p>
     *
     * <p>
     * No recursion is performed, so elements nested two levels deep
     * would have to be obtained with:
     * <pre>
     * <code>
     *   Iterator itr = (currentElement.getChildren()).iterator();
     *   while(itr.hasNext()) {
     *     Element oneLevelDeep = (Element)itr.next();
     *     List twoLevelsDeep = oneLevelDeep.getChildren();
     *     // Do something with these children
     *   }
     * </code>
     * </pre>
     * </p>
     *
     * @return list of child <code>Element</code> objects for this element
     */
    @Override
    public List<Element> getChildren() {
        return element.getChildren();
    }

    /**
     * This returns a <code>List</code> of all the child elements
     * nested directly (one level deep) within this element with the given
     * local name and belonging to no namespace, returned as
     * <code>Element</code> objects.  If this target element has no nested
     * elements with the given name outside a namespace, an empty List
     * is returned.  The returned list is "live" in document order
     * and changes to it affect the element's actual contents.
     * <p>
     * Please see the notes for <code>{@link #getChildren}</code>
     * for a code example.
     * </p>
     *
     * @param cname local name for the children to match
     * @return all matching child elements
     */
    @Override
    public List<Element> getChildren(String cname) {
        return element.getChildren(cname);
    }

    /**
     * This returns a <code>List</code> of all the child elements
     * nested directly (one level deep) within this element with the given
     * local name and belonging to the given Namespace, returned as
     * <code>Element</code> objects.  If this target element has no nested
     * elements with the given name in the given Namespace, an empty List
     * is returned.  The returned list is "live" in document order
     * and changes to it affect the element's actual contents.
     * <p>
     * Please see the notes for <code>{@link #getChildren}</code>
     * for a code example.
     * </p>
     *
     * @param cname local name for the children to match
     * @param ns <code>Namespace</code> to search within. A null implies Namespace.NO_NAMESPACE.
     * @return all matching child elements
     */
    @Override
    public List<Element> getChildren(String cname, Namespace ns) {
        return element.getChildren(cname, ns);
    }


    public XmiElement getChild(String name, Namespace ns) {
        return toXmiElement(element.getChild(name, ns));
    }

    private XmiElement toXmiElement(Element child) {
        if (child == null)
            return null;
        else
            return new XmiElement(child);
    }

    public XmiElement getChild(String name) {
        return toXmiElement(element.getChild(name));
    }

    /**
     * <p>
     * This removes the first child element (one level deep) with the
     * given local name and belonging to no namespace.
     * Returns true if a child was removed.
     * </p>
     *
     * @param cname the name of child elements to remove
     * @return whether deletion occurred
     */
    @Override
    public boolean removeChild(String cname) {
        return element.removeChild(cname);
    }

    /**
     * <p>
     * This removes the first child element (one level deep) with the
     * given local name and belonging to the given namespace.
     * Returns true if a child was removed.
     * </p>
     *
     * @param cname the name of child element to remove
     * @param ns <code>Namespace</code> to search within. A null implies Namespace.NO_NAMESPACE.
     * @return whether deletion occurred
     */
    @Override
    public boolean removeChild(String cname, Namespace ns) {
        return element.removeChild(cname, ns);
    }

    /**
     * <p>
     * This removes all child elements (one level deep) with the
     * given local name and belonging to no namespace.
     * Returns true if any were removed.
     * </p>
     *
     * @param cname the name of child elements to remove
     * @return whether deletion occurred
     */
    @Override
    public boolean removeChildren(String cname) {
        return element.removeChildren(cname);
    }

    /**
     * <p>
     * This removes all child elements (one level deep) with the
     * given local name and belonging to the given namespace.
     * Returns true if any were removed.
     * </p>
     *
     * @param cname the name of child elements to remove
     * @param ns <code>Namespace</code> to search within. A null implies Namespace.NO_NAMESPACE.
     * @return whether deletion occurred
     */
    @Override
    public boolean removeChildren(String cname, Namespace ns) {
        return element.removeChildren(cname, ns);
    }

    /**
     * Get the Namespaces that are in-scope on this Element. Element has the
     * most complex rules for the namespaces-in-scope.
     * <p>
     * The scope is built up from a number of sources following the rules of
     * XML namespace inheritence as follows:
     * <ul>
     * <li>The {@link org.jdom2.Namespace#XML_NAMESPACE} is added
     * <li>The element's namespace is added (commonly
     * {@link org.jdom2.Namespace#NO_NAMESPACE})
     * <li>All the attributes are inspected and their Namespaces are included
     * <li>All Namespaces declared on this Element using
     * {@link #addNamespaceDeclaration(org.jdom2.Namespace)} are included.
     * <li>If the element has a parent then the parent's Namespace scope is
     * inspected, and any prefixes in the parent scope that are not yet bound
     * in this Element's scope are included.
     * <li>If the default Namespace (the no-prefix namespace) has not been
     * encountered for this Element then {@link org.jdom2.Namespace#NO_NAMESPACE} is
     * included.
     * </ul>
     * The Element's Namespace scope consist of it's inherited Namespaces and
     * any modifications to that scope derived from the Element itself. If the
     * element is detached then it's inherited scope consists of just
     * If an element has no parent then
     * <p>
     * Note that the Element's Namespace will always be reported first.
     * <p>
     * <strong>Description copied from</strong>
     * {@link org.jdom2.NamespaceAware#getNamespacesInScope()}:
     * <p>
     * {@inheritDoc}
     *
     * @see org.jdom2.NamespaceAware
     */
    @Override
    public List<Namespace> getNamespacesInScope() {
        return element.getNamespacesInScope();
    }

    @Override
    public List<Namespace> getNamespacesInherited() {
        return element.getNamespacesInherited();
    }

    @Override
    public List<Namespace> getNamespacesIntroduced() {
        return element.getNamespacesIntroduced();
    }

    @Override
    public Element detach() {
        return element.detach();
    }

    @Override
    public void canContainContent(Content child, int index, boolean replace) throws IllegalAddException {
        element.canContainContent(child, index, replace);
    }

    /**
     * Sort the contents of this Element using a mechanism that is safe for JDOM
     * content. See the notes on {@link #sortContent(org.jdom2.filter.Filter, java.util.Comparator)} for
     * how the algorithm works.
     * <p>
     * {@link java.util.Collections#sort(java.util.List, java.util.Comparator)} is not appropriate for sorting
     * the Lists returned from {@link org.jdom2.Element#getContent()} because those are
     * 'live' lists, and the Collections.sort() method uses an algorithm that
     * adds the content in the new location before removing it from the old.
     * That creates validation issues with content attempting to attach to a
     * parent before detaching first.
     * <p>
     * This method provides a safe means to conveniently sort the content.
     *
     * @param comparator The Comparator to use for the sorting.
     */
    @Override
    public void sortContent(Comparator<? super Content> comparator) {
        element.sortContent(comparator);
    }

    /**
     * Sort the child Elements of this Element using a mechanism that is safe
     * for JDOM content. Other child content will be unaffected. See the notes
     * on {@link #sortContent(org.jdom2.filter.Filter, java.util.Comparator)} for how the algorithm works.
     * <p>
     * {@link java.util.Collections#sort(java.util.List, java.util.Comparator)} is not appropriate for sorting
     * the Lists returned from {@link org.jdom2.Element#getContent()} because those are
     * 'live' lists, and the Collections.sort() method uses an algorithm that
     * adds the content in the new location before removing it from the old.
     * This creates validation issues with content attempting to attach to a
     * parent before detaching first.
     * <p>
     * This method provides a safe means to conveniently sort the content.
     *
     * @param comparator The Comparator to use for the sorting.
     */
    @Override
    public void sortChildren(Comparator<? super Element> comparator) {
        element.sortChildren(comparator);
    }

    /**
     * Sort the Attributes of this Element using a mechanism that is safe
     * for JDOM. Other child content will be unaffected. See the notes
     * on {@link #sortContent(org.jdom2.filter.Filter, java.util.Comparator)} for how the algorithm works.
     * <p>
     * {@link java.util.Collections#sort(java.util.List, java.util.Comparator)} is not appropriate for sorting
     * the Lists returned from {@link org.jdom2.Element#getContent()} because those are
     * 'live' lists, and the Collections.sort() method uses an algorithm that
     * adds the content in the new location before removing it from the old.
     * This creates validation issues with content attempting to attach to a
     * parent before detaching first.
     * <p>
     * This method provides a safe means to conveniently sort the content.
     *
     * @param comparator The Comparator to use for the sorting.
     */
    @Override
    public void sortAttributes(Comparator<? super Attribute> comparator) {
        element.sortAttributes(comparator);
    }

    /**
     * Sort the child content of this Element that matches the Filter, using a
     * mechanism that is safe for JDOM content. Other child content (that does
     * not match the filter) will be unaffected.
     * <p>
     * The algorithm used for sorting affects the child content in the following
     * ways:
     * <ol>
     * <li>Items not matching the Filter will be unchanged. This includes the
     * absolute position of that content in this Element. i.e. if child content
     * <code>cc</code> does not match the Filter, then <code>indexOf(cc)</code>
     * will not be changed by this sort.
     * <li>Items matching the Filter will be reordered according to the
     * comparator. Only the relative order of the Filtered data will change.
     * <li>Items that compare as 'equals' using the comparator will keep the
     * the same relative order as before the sort.
     * </ol>
     * <p>
     * {@link java.util.Collections#sort(java.util.List, java.util.Comparator)} is not appropriate for sorting
     * the Lists returned from {@link org.jdom2.Element#getContent()} because those are
     * 'live' lists, and the Collections.sort() method uses an algorithm that
     * adds the content in the new location before removing it from the old.
     * This creates validation issues with content attempting to attach to a
     * parent before detaching first.
     * <p>
     * This method provides a safe means to conveniently sort the content.
     * @param <E> The generic type of the Filter used to select the content to
     * sort.
     * @param filter The Filter used to select which child content to sort.
     * @param comparator The Comparator to use for the sorting.
     */
    @Override
    public <E extends Content> void sortContent(Filter<E> filter, Comparator<? super E> comparator) {
        element.sortContent(filter, comparator);
    }

    /**
     * Calculate the XMLBase URI for this Element using the rules defined in the
     * XMLBase specification, as well as the values supplied in the xml:base
     * attributes on this Element and its ancestry.
     * <p>
     * This method assumes that all values in <code>xml:base</code> attributes
     * are valid URI values according to the <code>java.net.URI</code>
     * implementation. The same implementation is used to resolve relative URI
     * values, and thus this code follows the assumptions in java.net.URI.
     * <p>
     * This technically deviates from the XMLBase spec because to fully support
     * legacy HTML the xml:base attribute could contain what is called a 'LIERI'
     * which is a superset of true URI values, but for practical purposes JDOM
     * users should never encounter such values because they are not processing
     * raw HTML (but xhtml maybe).
     *
     * @return a URI representing the XMLBase value for the supplied Element, or
     *         null if one could not be calculated.
     * @throws java.net.URISyntaxException
     *         if it is not possible to create java.net.URI values from the data
     *         in the <code>xml:base</code> attributes.
     */
    @Override
    public URI getXMLBaseURI() throws URISyntaxException {
        return element.getXMLBaseURI();
    }



    /**
     * Return this child's parent, or null if this child is currently
     * not attached. The parent can be either an {@link org.jdom2.Element}
     * or a {@link org.jdom2.Document}.
     * <p>
     * This method can be overridden by particular Content subclasses to return
     * a specific type of Parent (co-variant return type). All overriding
     * subclasses <b>must</b> call <code>super.getParent()</code>;
     *
     * @return this child's parent or null if none
     */
    @Override
    public Parent getParent() {
        return element.getParent();
    }

    /**
     * Return this child's owning document or null if the branch containing
     * this child is currently not attached to a document.
     *
     * @return this child's owning document or null if none
     */
    @Override
    public Document getDocument() {
        return element.getDocument();
    }

    public List<XmiElement> getChildrenAsXmiElements() {
        return toXmiElementList((List<Element>) element.getChildren());
    }

    public List<XmiElement> getChildrenAsXmiElements(String name, Namespace ns) {
        return toXmiElementList((List<Element>) element.getChildren(name, ns));
    }

    public List<XmiElement> getChildrenAsXmiElements(String name) {
        return toXmiElementList((List<Element>) element.getChildren(name));
    }

    protected List<XmiElement> toXmiElementList(List<Element> elements) {
        List<XmiElement> result = new LinkedList<XmiElement>();
        for (Element element : elements) {
            result.add(new XmiElement(element));
        }
        return result;
    }


    public XmiElement getParentXmiElement() {
        Element parentElement = element.getParentElement();
        if (parentElement != null) {
            return new XmiElement(parentElement);
        } else {
            return null;
        }
    }

    /**
     * Returns the (local) name of the element (without any namespace prefix).
     *
     * @return                     local element name
     */
    @Override
    public String getName() {
        return element.getName();
    }

    /**
     * Sets the (local) name of the element.
     *
     * @param  name                 the new (local) name of the element
     * @return                      the target element
     * @throws org.jdom2.IllegalNameException if the given name is illegal as an Element
     *                              name
     */
    @Override
    public Element setName(String name) {
        return element.setName(name);
    }

    /**
     * Returns the element's {@link org.jdom2.Namespace}.
     *
     * @return                     the element's namespace
     */
    @Override
    public Namespace getNamespace() {
        return element.getNamespace();
    }

    /**
     * Sets the element's {@link org.jdom2.Namespace}. If the provided namespace is null,
     * the element will have no namespace.
     *
     * @param  namespace           the new namespace. A null implies Namespace.NO_NAMESPACE.
     * @return                     the target element
     * @throws org.jdom2.IllegalAddException if there is a Namespace conflict
     */
    @Override
    public Element setNamespace(Namespace namespace) {
        return element.setNamespace(namespace);
    }

    /**
     * Returns the namespace prefix of the element or an empty string if none
     * exists.
     *
     * @return                     the namespace prefix
     */
    @Override
    public String getNamespacePrefix() {
        return element.getNamespacePrefix();
    }

    /**
     * Returns the namespace URI mapped to this element's prefix (or the
     * in-scope default namespace URI if no prefix). If no mapping is found, an
     * empty string is returned.
     *
     * @return                     the namespace URI for this element
     */
    @Override
    public String getNamespaceURI() {
        return element.getNamespaceURI();
    }

    /**
     * Returns the {@link org.jdom2.Namespace} corresponding to the given prefix in scope
     * for this element. This involves searching up the tree, so the results
     * depend on the current location of the element. Returns null if there is
     * no namespace in scope with the given prefix at this point in the
     * document.
     *
     * @param  prefix              namespace prefix to look up
     * @return                     the Namespace for this prefix at this
     *                             location, or null if none
     */
    @Override
    public Namespace getNamespace(String prefix) {
        return element.getNamespace(prefix);
    }

    /**
     * Returns the full name of the element, in the form
     * [namespacePrefix]:[localName]. If the element does not have a namespace
     * prefix, then the local name is returned.
     *
     * @return                     qualified name of the element (including
     *                             namespace prefix)
     */
    @Override
    public String getQualifiedName() {
        return element.getQualifiedName();
    }
}