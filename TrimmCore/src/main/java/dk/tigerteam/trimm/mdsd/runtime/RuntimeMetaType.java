/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dk.tigerteam.trimm.mdsd.runtime;

import dk.tigerteam.trimm.bean.bidirectional.ManyToOneWrapper;
import dk.tigerteam.trimm.bean.bidirectional.OneToManySetWrapper;

import java.io.Serializable;
import java.util.*;

/**
 * Base class for all our runtime meta data.<br/>
 * Follows a common composite pattern where leaf and composites are the same (a leaf just doesn't have any children when asking {@link #getChildren()}).<p/>
 * <b>All assocications between {@link #owner} and {@link #children} are maintained bidirectionally for you :)</b><p/>
 * <b>Note:</b> <i>All searches in the {@link RuntimeMetaModel} are done using {@link #id}'s, so it is therefore a requirement that all {@link #id}'s are unique.</i>
 * <p/>
 * To support storage of various utility/temporary data during runtime you can use the various
 * {@link #getAttributes()}, {@link #addAttribute(String, Object)}, {@link #removeAttribute(String)}, {@link #hasAttribute(String)}, etc. (these will not be affected by the frozen status)
 * @author Jeppe Cramon
 */
public abstract class RuntimeMetaType implements Serializable {
	private static final long serialVersionUID = 765616357281054876L;
	public static String newline = System.getProperty("line.separator");

	private boolean fabricated = false;
	private String id;
	private RuntimeMetaType owner;
	private Set<RuntimeMetaType> children = new HashSet<RuntimeMetaType>();
	private String documentation;
	private Map<String, String> taggedValues = new HashMap<String, String>();
	private Map<String, Object> attributes = new HashMap<String, Object>();
	private boolean frozen = false;
	
	/**
	 * Create a {@link RuntimeMetaType} by supplying its unique id
	 * @param id The unique id
	 */
	public RuntimeMetaType(String id) {
		super();
		assert id != null;
		this.id = id;
	}
	
	/**
	 * Create a {@link RuntimeMetaType} by supplying its unique id and a fabrication information
	 * @param id The unique id
	 * @param fabricated Is this MetaType a result of a fabrication or defined in the original model
	 */
	public RuntimeMetaType(String id, boolean fabricated) {
		this(id);
		this.fabricated = true;
	}
	
	/**
	 * Freeze this Meta types configuration
	 */
	public void freeze() {
		frozen = true;
	}
	
	/**
	 * Has this metatypes configuration been frozen?
	 */
	public boolean isFrozen() {
		return frozen;
	}
	
	/**
	 * Checks and validates if the meta type is frozen for modification or not. If it frozen a {@link FrozenRuntimeMetaTypeException} is thrown
	 * @throws FrozenRuntimeMetaTypeException if the meta type is frozen
	 */
	protected void checkCanModify() {
		if (frozen) {
			throw new FrozenRuntimeMetaTypeException(this);
		}
	}
	
	/**
	 * Is this {@link RuntimeMetaType} defined from the original Model or is it constructed by fabrication
	 */
	public boolean isFabricated() {
		return fabricated;
	}

	/**
	 * Is this {@link RuntimeMetaType} defined from the original Model or is it constructed by fabrication
	 */
	public void setFabricated(boolean artificial) {
		checkCanModify();
		this.fabricated = artificial;
	}

	/**
	 * The unique id for a meta type
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Any documentation about the meta type
	 */
	public String getDocumentation() {
		return documentation;
	}

	/**
	 * Any documentation about the meta type
	 * @param documentation Any documentation about the meta type
	 * @return This meta type instance
	 */
	public RuntimeMetaType setDocumentation(String documentation) {
		checkCanModify();
		this.documentation = documentation;
		return this;
	}

	/**
	 * Any tagged values (UML) which is a combination of name/value (or key/value if you like) pairs
	 */
	public Map<String, String> getTaggedValues() {
		return taggedValues;
	}

	/**
	 * Any tagged values (UML) which is a combination of name/value (or key/value if you like) pairs
	 * @param taggedValues Any tagged values (UML)
	 * @return This meta type instance
	 */
	public RuntimeMetaType setTaggedValues(Map<String, String> taggedValues) {
		checkCanModify();
		this.taggedValues = taggedValues;
		return this;
	}
	
	/**
	 * Add a tagged value (UML)
	 * @param name The name of the tagged value
	 * @param value The value of the tagged value
	 * @return This meta type instance
	 */
	public RuntimeMetaType addTaggedValue(String name, String value) {
		checkCanModify();
		getTaggedValues().put(name, value);
		return this;
	}
	
	/**
	 * Do we have a tagged value by the given name 
	 * @param name The name of the tagged value
	 * @return true if we have a tagged value by the given name, otherwise false
	 */
	public boolean hasTaggedValue(String name) {
		return taggedValues.containsKey(name);
	}

	/**
	 * Get the tagged value by its name
	 * @param name The name of the tagged value
	 * @return The tagged value or <code>null</code> if we don't have a tagged value by the given name
	 */
	public String getTaggedValue(String name) {
		return taggedValues.get(name);
	}
	
	/**
	 * Add a child to this meta type<br/>
	 * <b>Note:</b> <i> relationship to the owner is automatically maintained bidirectionally</i>
	 * @param children The children to add
	 * @return This meta type
	 */
	public RuntimeMetaType add(RuntimeMetaType... children) {
		checkCanModify();
		for (RuntimeMetaType child : children) {
			getChildren().add(child);
		}
		return this;
	}
	
	/**
	 * Get all children of this meta type<br/>
	 * <b>Note:</b> <i> relationship to the owner is automatically maintained bidirectionally</i>
	 */
	public Set<RuntimeMetaType> getChildren() {
		return new OneToManySetWrapper<RuntimeMetaType, RuntimeMetaType>(this, children) {
			private static final long serialVersionUID = -782855107551392868L;

			@Override
			protected RuntimeMetaType getOneSideObjectInManySideObject(
					RuntimeMetaType manySideObject) {
				return manySideObject.getOwner();
			}

			@Override
			protected void setOneSideObjectInManySideObject(
					RuntimeMetaType manySideObject, RuntimeMetaType oneSideObject) {
				manySideObject.checkCanModify();
				manySideObject.setOwner(oneSideObject);
			}
		};
	}
	
	/**
	 * Get all stereotypes applied to this meta type
	 */
	public Set<RuntimeMetaStereoType> getStereoTypes() {
		return findImmediateChildrenOfType(RuntimeMetaStereoType.class);
	}
	
	/**
	 * Do we have a {@link RuntimeMetaStereoType} applied by the given name
	 *@param name The name of the stereotype value
	 * @return true if we have a stereotype by the given name, otherwise false
	 */
	public boolean hasStereoType(String name) {
		for (RuntimeMetaStereoType stereoType : getStereoTypes()) {
			if (name.equals(stereoType.getName())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Get the owner of this meta type (is null for {@link RuntimeMetaModel})<br/>
	 * <b>Note:</b> <i>relationships to the children is automatically maintained bidirectionally</i>
	 */
	public RuntimeMetaType getOwner() {
		return owner;
	}
	
	/**
	 * Set the owner of this meta type<br/>
	 * <b>Note:</b> <i>relationships to the children is automatically maintained bidirectionally</i>
	 * @param owner The new owner of this meta type
	 * @return This metatype instance
	 */
	public RuntimeMetaType setOwner(RuntimeMetaType owner) {
		new ManyToOneWrapper<RuntimeMetaType, RuntimeMetaType>(this) {
			private static final long serialVersionUID = -6402624532142931737L;

			@SuppressWarnings("unchecked")
			@Override
			protected void addManySideObjectToOneSideCollection(
					RuntimeMetaType oneSide, RuntimeMetaType manySide) {
				oneSide.checkCanModify();
				 ((OneToManySetWrapper<RuntimeMetaType, RuntimeMetaType>)oneSide.getChildren()).getWrappedCollection().add(manySide);
			} 

			@Override
			protected RuntimeMetaType getOneSideObjectInManySideObject(RuntimeMetaType manySide) {
				return manySide.owner;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void removeManySideObjectFromOneSideCollection(
					RuntimeMetaType oneSide, RuntimeMetaType manySide) {
				oneSide.checkCanModify();
				 ((OneToManySetWrapper<RuntimeMetaType, RuntimeMetaType>)oneSide.getChildren()).getWrappedCollection().remove(manySide);
			}

			@Override
			protected void setOneSideObjectInManySideObject(RuntimeMetaType manySide,
					RuntimeMetaType oneSide) {
				checkCanModify();
				manySide.owner = oneSide;
			}
		}.updateOneSideObject(owner);
		return this;
	}

	/**
	 * Get all attributes applied (attributes are used for storing various value that can be useful during parsing or transformation)
	 */
	public Map<String, Object> getAttributes() {
		return attributes;
	}
	
	/**
	 * Add an attribute (attributes are used for storing various value that can be useful during parsing or transformation)
	 * @param key The name of the attribute
	 * @param value The attribute value
	 * @return This meta type instance
	 */
	public RuntimeMetaType addAttribute(String key, Object value) {
		attributes.put(key, value);
		return this;
	}
	
	/**
	 * Remove an attribute (attributes are used for storing various value that can be useful during parsing or transformation)
	 * @param key The name of the attribute
	 * @return This meta type instance
	 */
	public RuntimeMetaType removeAttribute(String key) {
		attributes.remove(key);
		return this;
	}
	
	/**
	 * Do we have the given attribute applied? (attributes are used for storing various value that can be useful during parsing or transformation)
	 * @param key The name of the attribute
	 * @return true if we have the attribute applied, otherwise false
	 */
	public boolean hasAttribute(String key) {
		return attributes.containsKey(key);
	}
	
	/**
	 * Get the attribute (attributes are used for storing various value that can be useful during parsing or transformation)
	 * @param key The name of the attribute
	 * @return The attribute value if it was applied or <code>null</code> if it wasn't applied
	 */
	public Object getAttribute(String key) {
		return attributes.get(key);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final RuntimeMetaType other = (RuntimeMetaType) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		//return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
		return this.getClass().getSimpleName() + "#" + id;
	}
	
	/**
	 * Debug friendly description of this {@link RuntimeMetaType}. 
	 */
	public String getDescription() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder
					.append("Meta id = '" + this.getId()).append(newline)
					.append("TaggedValues = '").append(taggedValues.toString()).append(newline)
					.append("Fabricated = '").append(fabricated).append(newline)
					.append("StereoTypes = '");
		int index = 0;
		for (RuntimeMetaStereoType	stereoType : getStereoTypes()) {
			if (index > 0) {
				stringBuilder.append(", ");
			}
			stringBuilder.append(stereoType.getName());
			index++;
		}
		stringBuilder.append("'");
		return stringBuilder.toString();
	}
	
	// ----------------------------------------------
	/**
	 * Find a {@link RuntimeMetaType} by it's id. This method will search this instance and all its children recursively (in a breadth first search)
	 * @throws RuntimeMetaTypeNotFoundException if a {@link RuntimeMetaType} by the given id can't be found
	 */
	public RuntimeMetaType getById(String id) {
		RuntimeMetaType match = findById(id);
		if (match == null) {
			throw new RuntimeMetaTypeNotFoundException(id);
		} else {
			return match;
		}
	}
	
	/**
   * Find a {@link RuntimeMetaType} by it's id. This method will search this instance and all its children recursively (in a breadth first search)
	 * @param id
	 * @return The runtime meta type or <code>null</code> if a match wasn't found
	 */
	public RuntimeMetaType findById(String id) {
		if (this.id.equals(id)) {
			return this;
		}
		return recursivelyWideSearch(id, this);
	}

	/**
	 * Find a {@link RuntimeMetaType} by it's id. This method will search this instance and all its children recursively (in a breadth first search)
	 * @throws RuntimeMetaTypeNotFoundException if a {@link RuntimeMetaType} by the given id can't be found
	 */
	public <T extends RuntimeMetaType> T getByIdAndType(String id, Class<T> type) {
		T match = findByIdAndType(id, type);
		if (match == null) {
			throw new RuntimeMetaTypeNotFoundException(id);
		} else {
			return match;
		}
	}
	
	/**
	 * Find a {@link RuntimeMetaType} by it's id. This method will search this instance and all its children recursively (in a breadth first search)
	 * @param id
	 * @return The runtime meta type or <code>null</code> if a match wasn't found
	 */
	@SuppressWarnings("unchecked")
	public <T extends RuntimeMetaType> T findByIdAndType(String id, Class<T> type) {
		if (this.id.equals(id) && type.isAssignableFrom(this.getClass())) {
			return (T) this;
		}
		return recursivelyWideSearchByType(id, type, this);
	}

	/**
	 * Helper method for {@link #findById} -> Search breadth first (first all children) and then afterwards
	 * each childs children recursively
	 * @param id
	 * @param parent
	 * @return  the metatype or null if not found
	 */
	@SuppressWarnings("unchecked")
	private <T extends RuntimeMetaType> T recursivelyWideSearchByType(String id, Class<T> type, RuntimeMetaType parent) {
		for (RuntimeMetaType childType : parent.getChildren()) {
			if (childType.id.equals(id) && type.isAssignableFrom(childType.getClass())) {
				return (T) childType;
			}
		}
		
		for (RuntimeMetaType childType : parent.getChildren()) {
			T match = recursivelyWideSearchByType(id, type, childType);
			if (match != null) {
				return match;
			}
		}
		return null;
	}

	/**
	 * Helper method for {@link #findById} -> Search breadth first (first all children) and then afterwards
	 * each childs children recursively
	 * @param id
	 * @param parent
	 * @return the metatype or null if not found
	 */
	private RuntimeMetaType recursivelyWideSearch(String id, RuntimeMetaType parent) {
		for (RuntimeMetaType childType : parent.getChildren()) {
			if (childType.id.equals(id)) {
				return childType;
			}
		}
		
		for (RuntimeMetaType childType : parent.getChildren()) {
			RuntimeMetaType match = recursivelyWideSearch(id, childType);
			if (match != null) {
				return match;
			}
		}
		return null;
	}
	
	/**
	 * Search for a child of the given {@link RuntimeMetaType} class. Only searches the immediate/direct children of this instance 
	 * @param <T> The type of children to find (or subtypes thereof)
	 * @param metaTypeClass The type of children to find (or subtypes thereof)
	 * @return The immediate children found
	 */
	@SuppressWarnings("unchecked")
	public <T extends RuntimeMetaType> Set<T> findImmediateChildrenOfType(Class<T> metaTypeClass) {
		Set<T> result = new HashSet<T>();
		for (RuntimeMetaType child : children) {
			if (metaTypeClass.isAssignableFrom(child.getClass())) {
				result.add((T) child);
			}
		}
		return result;
	}

	/**
	 * Recursively search this instance children, for children of the given {@link RuntimeMetaType}
	 * @param <T> The type of children to find (or subtypes thereof)
	 * @param metaTypeClass The type of children to find (or subtypes thereof)
	 * @return All the children found
	 */
	public <T extends RuntimeMetaType> Set<T> findAllChildrenOfType(Class<T> metaTypeClass) {
		Set<T> result = new HashSet<T>();
		recursivelyFindAllChildrenOfType(this, metaTypeClass, result);
		return result;
	}
	
	/**
	 * Helper method for {@link #findAllChildrenOfType(Class)}
	 * @param <T> The type of children to find (or subtypes thereof)
	 * @param metaType The metatype which will have all its children recursively searched
	 * @param metaTypeClass The type of children to find (or subtypes thereof)
	 * @param childrenFound Collector for all the children found during the search
	 */
	@SuppressWarnings("unchecked")
	private <T extends RuntimeMetaType> void recursivelyFindAllChildrenOfType(RuntimeMetaType metaType, Class<T> metaTypeClass, Set<T> childrenFound) {
		for (RuntimeMetaType childType : metaType.getChildren()) {
			if (metaTypeClass.isAssignableFrom(childType.getClass())) {
				childrenFound.add((T) childType);
			}
			recursivelyFindAllChildrenOfType(childType, metaTypeClass, childrenFound);
		}
	}
	
	/**
	 * Searches upwards in the hierarchy to find an owner of a given type.
	 * @param <T> The type of owner to find (or a subtype thereof)
	 * @param typeOfOwner The type of owner to find (or a subtype thereof)
	 * @return The owner found 
	 * @throws RuntimeMetaTypeNotFoundException in case a an owner of the given type can't be found
	 */
	public <T extends RuntimeMetaType> T findOwnerOfType(Class<T> typeOfOwner) {
		T parent = findOwnerOfTypeOrNull(typeOfOwner);
		if (parent != null) {
			return parent;
		}
		throw new RuntimeMetaTypeNotFoundException("Coulnd't find an owner of type " + typeOfOwner);
	}
	
	/**
	 * Searches upwards in the hierarchy to find an owner of a given type.
	 * <T> The type of owner to find (or a subtype thereof)
	 * @param typeOfOwner The type of owner to find (or a subtype thereof)
	 * @return the owner found or <code>null</code> if an owner of the given type can't be found
	 */
	@SuppressWarnings("unchecked")
	public <T extends RuntimeMetaType> T findOwnerOfTypeOrNull(Class<T> typeOfOwner) {
		RuntimeMetaType parent = getOwner();
		while (parent != null) {
			if (typeOfOwner.isAssignableFrom(parent.getClass())) {
				return (T) parent;
			}
			parent = parent.getOwner();
		}
		return null;
	}
	
	/**
	 * Search for a children that match the criteria. Only searches the immediate/direct children of this instance 
	 * @param <T> The type of children to find (or subtypes thereof)
	 * @param criteria The criteria to find using
	 * @return The immediate children found
	 */
	@SuppressWarnings("unchecked")
	public <T extends RuntimeMetaType> List<T> findInImmediateChildren(RuntimeMetaCriteria<T> criteria) {
		List<T> result = new LinkedList<T>();
		Class<?> tClass = (Class<?>) criteria.getType();
		for (RuntimeMetaType element : this.getChildren()) {
			if (tClass.isAssignableFrom(element.getClass()) && criteria.isOk((T)element)) {
				result.add((T) element);
			}
		}
		return result;
	}
	
	/**
	 * Search <b>this</b> {@link RuntimeMetaType} element and all its children recursively using the {@link RuntimeMetaCriteria}.
	 * @param <T> The type of elements to find (or subtypes thereof)
	 * @param criteria The criteria to use
	 * @return The elements found
	 */
	@SuppressWarnings("unchecked")
	public <T extends RuntimeMetaType> List<T> find(RuntimeMetaCriteria<T> criteria) {
		Class<?> tClass = (Class<?>) criteria.getType();
		List<T> result = new LinkedList<T>();
		if (tClass.isAssignableFrom(this.getClass()) && criteria.isOk((T)this)) {
			result.add((T) this);
		}
		recursivelyFind(this, criteria, result, tClass);
		return result;
	}

	/**
	 * Internal helper method for {@link #find(RuntimeMetaCriteria)}
	 */
	@SuppressWarnings("unchecked")
	private <T extends RuntimeMetaType> void recursivelyFind(RuntimeMetaType runtimeMetaType, RuntimeMetaCriteria<T> criteria, List<T> elementsFound, Class<?> tClass) {
		for (RuntimeMetaType element : runtimeMetaType.getChildren()) {
			if (tClass.isAssignableFrom(element.getClass()) && criteria.isOk((T)element)) {
				elementsFound.add((T) element);
			}
			recursivelyFind(element, criteria, elementsFound, tClass);
		}
	}
	
	public RuntimeMetaModel getMetaModel() {
		return findOwnerOfType(RuntimeMetaModel.class);
	}
}
