/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi;

import dk.tigerteam.trimm.mdsd.meta.MetaClazz;
import dk.tigerteam.trimm.mdsd.meta.MetaProperty;
import dk.tigerteam.trimm.mdsd.meta.MetaType;
import org.jdom2.JDOMException;

/**
 * XMI Parser interface. The {@link XmiElementParser} is responsible for parsing specific XMI element sections ({@link XmiElement}), which
 * are indicated using the {@link #supports(XmiElement, MetaType)} method.
 *
 * @author Jeppe Cramon
 */
public interface XmiElementParser {
    /**
     * Parse the {@link XmiElement}
     *
     * @param xmiElement The xmi element to parse
     * @param parent     The parent (owning) MetaType (could for instance be the {@link MetaClazz} which owns the {@link MetaProperty})
     * @return The MetaType representation of the {@link XmiElement}
     * @throws JDOMException In case there's a parsing error
     */
    MetaType parse(XmiElement xmiElement, MetaType parent) throws JDOMException;

    /**
     * Does this {@link XmiElementParser} support parsing the given {@link XmiElement}
     *
     * @param xmiElement The xmi element to parse
     * @param parent     The parent (owning) MetaType (could for instance be the {@link MetaClazz} which owns the {@link MetaProperty})
     * @return true if it supports parsing the {@link XmiElement} otherwise false
     */
    boolean supports(XmiElement xmiElement, MetaType parent);

    /**
     * The XmiReader that this parser is attached to - is typically called by {@link XmiReader#registerXmiElementParser(XmiElementParser...)}
     *
     * @param xmiReader
     */
    void setXmiReader(XmiReader xmiReader);

    /**
     * The XmiReader that this parser is attached to
     */
    XmiReader getXmiReader();
}
