/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

import dk.tigerteam.trimm.util.ObjectUtil;

/**
 * A {@link MetaType} with a name
 * 
 * @author Jeppe Cramon
 */
public class NameableMetaType extends MetaType {
	private static final long serialVersionUID = -4974772564182526490L;

	/**
	 * Global value that's used to determine if {@link #getAliasOrName()} returns
	 * the alias (if available) or the name. Default value is false
	 */
	public static boolean UseAliasIfAvailable = false;

	/**
	 * Create a {@link NameableMetaType} by supplying its unique id
	 * 
	 * @param id
	 *          The unique id
	 */
	public NameableMetaType(String id) {
		super(id);
	}

	/**
	 * Create a {@link NameableMetaType} by supplying its unique id and a
	 * fabrication information
	 * 
	 * @param id
	 *          The unique id
	 * @param fabricated
	 *          Is this MetaType a result of a fabrication or defined in the
	 *          original model
	 */
	public NameableMetaType(String id, boolean fabricated) {
		super(id, fabricated);
	}

	private String name;
	private String alias;

    /**
     * Creates a fabricated NameableMetaType with a generated id (UUID based)
     */
    public NameableMetaType() {
        super();
    }

    public String getName() {
		return name;
	}

	public NameableMetaType setName(String name) {
		this.name = name;
		return this;
	}

	public String getAlias() {
		return alias;
	}

	public NameableMetaType setAlias(String alias) {
		this.alias = alias;
		return this;
	}

	/**
	 * If this type has an alias it is used, otherwise the name is used
	 */
	public String getAliasOrName() {
		if (UseAliasIfAvailable && alias != null)
			return alias;
		else
			return name;
	}

	/**
	 * An equals method that only checks the {@link #name} values
	 * 
	 * @param other
	 *          The other {@link NameableMetaType} to compare with
	 * @return true if the two object have the same name
	 */
	public boolean hasSameName(NameableMetaType other) {
		assert other != null;
		return ObjectUtil.equals(this.getName(), other.getName());
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ": " + name + "#" + getId();
	}

	/**
	 * JavaDoc friendly description of this {@link NameableMetaType}.
	 */
	public String getDescription() {
		return super.getDescription() + "<br/>" + MetaType.newline + "Name = '" + name + "'";
	}
}
