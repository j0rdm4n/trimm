/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.meta;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents an operation in a UML model
 * 
 * @author Jeppe Cramon
 */
public class MetaOperation extends NameableMetaType {

	private static final long serialVersionUID = 514329369490503188L;

	private MetaParameter returnParameter;
	private List<MetaParameter> parameters = new LinkedList<MetaParameter>();
	private String behaviour;
	private String code;
	private boolean _static;
	private boolean _abstract;

    /**
     * Creates a fabricated MetaOperation with a generated id (UUID based)
     */
    public MetaOperation() {
    }

    /**
     * Creates a fabricated MetaOperation with a generated id (UUID based)
     */
    public MetaOperation(String name, boolean _abstract, MetaParameter returnParameter, MetaParameter...metaParameters) {
        super();
        setName(name);
        this._abstract = _abstract;
        this.returnParameter = returnParameter;
        this.parameters.addAll(Arrays.asList(metaParameters));
    }


    public MetaOperation(String id) {
		super(id);
	}

	public MetaOperation(String id, boolean fabricated) {
		super(id, fabricated);
	}

	@SuppressWarnings("unchecked")
	public MetaClazz getOwnerClazz() {
		return (MetaClazz) super.getOwnerOfType(MetaClazz.class);
	}

	public boolean isStatic() {
		return _static;
	}

	public boolean isAbstract() {
		return _abstract;
	}

	public MetaOperation setAbstract(boolean _abstract) {
		this._abstract = _abstract;
		return this;
	}

	public MetaOperation setStatic(boolean _static) {
		this._static = _static;
		return this;
	}

	public MetaParameter getReturnParameter() {
		return returnParameter;
	}

	public MetaOperation setReturnParameter(MetaParameter returnParameter) {
		this.returnParameter = returnParameter;
		return this;
	}

	/**
	 * Get all parameters EXCEPT the Return Parameter
	 */
	public List<MetaParameter> getParameters() {
		return parameters;
	}

	/**
	 * Get all parameters INCLUDING the Return Parameter (which will be the first parameter if it exists)
	 */
	public List<MetaParameter> getAllParameters() {
		final LinkedList<MetaParameter> allParams = new LinkedList<MetaParameter>(parameters);
		if (returnParameter != null) {
			allParams.add(0, returnParameter);
		}
		return allParams;
	}

	public MetaOperation addParameters(MetaParameter... metaParameters) {
		getParameters().addAll(Arrays.asList(metaParameters));
		return this;
	}

	public String getBehaviour() {
		return behaviour;
	}

	public MetaOperation setBehaviour(String behaviour) {
		this.behaviour = behaviour;
		return this;
	}

	public String getCode() {
		return code;
	}

	public MetaOperation setCode(String code) {
		this.code = code;
		return this;
	}

	@Override
	public String getDescription() {
		StringBuilder builder = new StringBuilder().append(super.getDescription()).append("<br/>").append(newline).append("Static = '")
				.append(_static).append("'<br/>").append(newline).append("Abstract = '").append(_abstract).append("'<br/>").append(newline);

		for (MetaParameter parameter : getParameters()) {
			builder.append("@param ").append(parameter.getName()).append(" ").append(parameter.getDescription());
			builder.append("<br/>").append(newline);
		}

		builder.append("@return ").append(returnParameter.getDescription());

		return builder.toString();
	}

}
