/*
 * Copyright 2008-2013 TigerTeam ApS - http://www.tigerteam.dk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.tigerteam.trimm.mdsd.uml2.xmi;

import dk.tigerteam.trimm.mdsd.meta.MetaModel;
import org.jdom2.Namespace;

import java.io.File;
import java.net.URL;
import java.util.List;

/**
 * General interface for all XmiReaders
 *
 * @author Jeppe Cramon
 */
public interface XmiReader {
    /**
     * Read the {@link MetaModel} from the given Url
     *
     * @param file the XMI file
     * @return A {@link MetaModel} representation of the XMI contents
     * @throws XmiReaderException in case of an error while parsing and processing the XMI and {@link MetaModel}
     */
    MetaModel read(File file);

    /**
     * Read the {@link MetaModel} from the given Url
     *
     * @param url the location of the XMI file
     * @return A {@link MetaModel} representation of the XMI contents
     * @throws XmiReaderException in case of an error while parsing and processing the XMI and {@link MetaModel}
     */
    MetaModel read(URL url);

    /**
     * Register {@link XmiElementParser} instances, by adding to the Set of already registered parsers.
     * This method also sets the {@link XmiElementParser} {@link XmiElementParser#setXmiReader(XmiReader)} property
     *
     * @param xmiElementParserArray The {@link XmiElementParser} parsers
     * @return this
     */
    XmiReader registerXmiElementParser(XmiElementParser... xmiElementParserArray);

    /**
     * Get the registered {@link XmiElementParser}' as an immutable List
     */
    List<XmiElementParser> getXmiElementParsers();

    /**
     * Set the registered {@link XmiElementParser}' (overwrites previously registered parsers and performs a
     * {@link #registerXmiElementParser(XmiElementParser...)} for each {@link XmiElementParser})
     *
     * @param xmiElementParsers The {@link XmiElementParser}' to register
     * @return this
     */
    XmiReader setXmiElementParsers(List<XmiElementParser> xmiElementParsers);

    /**
     * The UML namespace that's used during parsing<br/>
     * Eg: <code>Namespace.getNamespace("uml", "http://schema.omg.org/spec/UML/2.1")</code>
     */
    Namespace getUmlNs();

    /**
     * The XMI namespace that's used during parsing<br/>
     * Eg: <code>Namespace.getNamespace("xmi", "http://schema.omg.org/spec/XMI/2.1")</code>
     */
    Namespace getXmiNs();
}
