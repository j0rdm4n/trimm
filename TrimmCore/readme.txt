TigerTeam Trimm Core
====================

What:
-----
Trimm is the code generation swizz army knife for UML based Models.
The Core module contains the MetaModel (which represents UML class models), XmiReaders (for reading UML models
from XMI files) and supporting code that supports the rest of the Trimm code base.

Getting started:
----------------


Who:
----
TigerTeam Trimm was originally developed by TigerTeam in Denmark under the name TigerMDSD.
TigerMDSD was renamed to Trimm in 2012 and released under Apache 2 license.
Homepage: http://www.tigerteam.dk/trimm

License:
--------
 Copyright 2008-2012 the original author or authors.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
